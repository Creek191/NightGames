package status;

import characters.Character;

public class Braced extends Status {

	public Braced(Character affected) {
		super("Braced", affected);
		duration = 4;
		flag(Stsflag.braced);
		tooltip = "Resist knockdowns";
		this.affected = affected;
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public int value() {
		return 20 + (25 * duration);
	}

	@Override
	public Status copy(Character target) {
		return new Braced(target);
	}
}
