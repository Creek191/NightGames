package status;

import characters.Character;
import characters.*;
import combat.Combat;

/**
 * Defines a status where a character's spirit burns brightly, reducing mojo use at the cost of stamina
 */
public class FireStance extends Status {

	public FireStance(Character affected) {
		super("Fire Form", affected);
		duration = 10;
		flag(Stsflag.form);
		tooltip = "Mojo skills cost half, increased Mojo gain, lose Stamina each turn";
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			duration = 15;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "Your spirit burns in you, feeding your power";
		}
		else {
			return affected.name() + " is still all fired up.";
		}
	}

	@Override
	public int regen() {
		return -Math.min(affected.getEffective(Attribute.Ki), 30) / 5;
	}

	@Override
	public int gain(Pool res, int x) {
		if (res == Pool.MOJO) {
			return x * Math.min(affected.getEffective(Attribute.Ki), 30) / 6;
		}
		else {
			return 0;
		}
	}

	@Override
	public int spend(Pool res, int x) {
		if (res == Pool.MOJO) {
			return -x / 2;
		}
		else {
			return 0;
		}
	}

	@Override
	public Status copy(Character target) {
		return new FireStance(target);
	}

	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident, 5);
		affected.emote(Emotion.dominant, 5);
		decay(c);
	}
}
