package status;

import characters.Character;
import characters.*;
import combat.Combat;

/**
 * Defines a status where a character is very self-conscious and ashamed
 */
public class Shamed extends Status {
	public Shamed(Character affected) {
		super("Shamed", affected);
		magnitude = 2;
		stacking = true;
		flag(Stsflag.shamed);
		flag(Stsflag.mindaffecting);
		this.duration = 4;
		tooltip = "Cunning and Seduction penalty, deal less pleasure, bonus to Submissive";
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			this.duration = 6;
		}
		this.affected = affected;
	}

	public Shamed(Character affected, int magnitude) {
		this(affected);
		this.magnitude = magnitude;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "You're a little distracted by self-consciousness, and it's throwing you off your game.";
		}
		else {
			return affected.name() + " is red faced from embarrassment as much as arousal.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if (a == Attribute.Seduction) {
			return -magnitude;
		}
		else if (a == Attribute.Cunning) {
			return -magnitude;
		}
		else if (a == Attribute.Submissive && affected.getPure(Attribute.Submissive) > 0) {
			return magnitude;
		}
		else {
			return 0;
		}
	}

	@Override
	public int weakened(int x) {
		return magnitude / 2;
	}

	@Override
	public int tempted(int x) {
		return magnitude;
	}

	@Override
	public int escape() {
		return -magnitude;
	}

	@Override
	public float proficiency(Anatomy using) {
		return Math.max(1.0f - (.05f * magnitude), 0);
	}

	@Override
	public Status copy(Character target) {
		return new Shamed(target);
	}

	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous, 30);
		affected.emote(Emotion.desperate, 30);
		decay(c);
	}
}
