package status;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;

/**
 * Defines a status where a character retaliates to any non-soul damage taken
 */
public class Punisher extends Status {
    private final Character opponent;
    private boolean triggered;

    public Punisher(Character affected, Character target) {
        super("Punisher", affected);
        flags.add(Stsflag.punisher);
        duration = affected.getEffective(Attribute.Discipline) / 5;
        tooltip = "Any physical damage taken results in immediate retaliation.";
        opponent = target;
        triggered = false;
    }

    @Override
    public String describe() {
        if (affected.human()) {
            return "You keep your riding crop at ready to counter attack.";
        }
        else {
            return affected.name() + " is still holding her crop threateningly";
        }
    }

    @Override
    public int damage(int x, Anatomy area) {
        if (affected.canAct() && area != Anatomy.soul) {
            opponent.pain(x, Anatomy.soul);
            triggered = true;
            opponent.emote(Emotion.nervous, 30);
        }

        return 0;
    }

    @Override
    public void turn(Combat c) {
        decay(c);
        if (!triggered) {
            return;
        }

        if (affected.human()) {
            c.write(affected, "You react in an instant to " + opponent.name() + "'s attack, lashing out with "
                    + "your riding crop to strike back at her and make sure she knows better than to act up again.");
        }
        else {
            c.write(affected, "A sharp slash of " + affected.name() + "'s riding crop against your flesh makes "
                    + "it clear to you how much of a mistake you just made by trying to attack her. You wince in pain "
                    + "as you pull back from her, and almost find yourself apologizing before you manage to stop "
                    + "yourself.");
        }
        triggered = false;
    }

    @Override
    public Status copy(Character target) {
        return new Punisher(target, opponent);
    }
}
