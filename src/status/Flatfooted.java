package status;

import characters.Character;
import characters.Emotion;
import combat.Combat;

/**
 * Defines a status where a character was caught off-guard
 */
public class Flatfooted extends Status {

	public Flatfooted(Character affected, int duration) {
		super("Flat-Footed", affected);
		flag(Stsflag.distracted);
		flag(Stsflag.mindaffecting);
		this.duration = duration;
		tooltip = "Unable to act";
		this.affected = affected;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "You are caught off-guard.";
		}
		else {
			return affected.name() + " is flat-footed and not ready to fight.";
		}
	}

	@Override
	public int evade() {
		return -10;
	}

	@Override
	public int escape() {
		return -20;
	}

	@Override
	public int counter() {
		return -3;
	}

	@Override
	public Status copy(Character target) {
		return new Flatfooted(target, duration);
	}

	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous, 5);
		decay(c);
	}
}
