package status;

import characters.Character;

/**
 * Defines a status where a character has primed a number of time charges
 */
public class Primed extends Status {
	public Primed(Character affected, int charges) {
		super("Primed", affected);
		magnitude = charges;
		stacking = true;
		tooltip = "Time charges available";
		decaying = false;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "You have " + magnitude + " time charges primed.";
		}
		else {
			return "";
		}
	}

	@Override
	public Status copy(Character target) {
		return new Primed(target, magnitude);
	}
}
