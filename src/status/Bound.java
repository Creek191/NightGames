package status;

import characters.Character;
import characters.Emotion;
import combat.Combat;

/**
 * Defines a status preventing a character from moving
 */
public class Bound extends Status {
	private final String binding;
	private int toughness;

	public Bound(Character affected, int dc, String binding) {
		super("Bound", affected);
		toughness = dc;
		this.binding = binding;
		flag(Stsflag.bound);
		tooltip = "Unable to move. Escape difficulty: " + dc;
		this.affected = affected;
		decaying = false;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "Your hands are bound by " + binding + ".";
		}
		else {
			return "Her hands are restrained by " + binding + ".";
		}
	}

	@Override
	public int evade() {
		return -15;
	}

	@Override
	public int escape() {
		var dc = toughness;
		toughness -= 5;
		return -dc;
	}

	@Override
	public int counter() {
		return -10;
	}

	@Override
	public String toString() {
		return binding;
	}

	@Override
	public Status copy(Character target) {
		return new Bound(target, toughness, binding);
	}

	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.desperate, 10);
		affected.emote(Emotion.nervous, 10);
	}
}
