package status;

import characters.Anatomy;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;

/**
 * Defines a status where a character's body part is more or less sensitive
 */
public class Sensitive extends Status {
	private final Anatomy part;
	private final float value;

	public Sensitive(Character affected, int duration, Anatomy part, float value) {
		super("Sensitive: " + part, affected);
		this.duration = duration;
		this.part = part;
		this.value = value;
		this.flag(Stsflag.sensitive);
		lingering = true;
		tooltip = "Increased pleasure to " + part + " (" + (value * 100) + "%)";
		if (value < 1.0f) {
			this.name = "Numb: " + part;
			tooltip = "Decreased pleasure to " + part + " (" + (value * 100) + "%)";
		}
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			this.duration = 3 * duration / 2;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public int value() {
		return (int) value;
	}

	@Override
	public float sensitive(Anatomy targeted) {
		if (part == targeted) {
			return value;
		}
		return 1.0f;
	}

	@Override
	public Status copy(Character target) {
		return new Sensitive(target, duration, part, value);
	}

	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.horny, 10);
		decay(c);
	}
}
