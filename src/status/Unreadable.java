package status;

import characters.Character;
import characters.Trait;

/**
 * Defines a status where a character's status can't be read by their opponents anymore
 */
public class Unreadable extends Status {

	public Unreadable(Character affected) {
		super("Unreadable", affected);
		this.duration = 3;
		this.affected = affected;
		flag(Stsflag.unreadable);
		tooltip = "Hides arousal and stamina from opponent";
		if (affected.has(Trait.PersonalInertia)) {
			this.duration = 5;
		}
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public Status copy(Character target) {
		return new Unreadable(target);
	}
}
