package status;

import characters.Anatomy;
import characters.Character;
import characters.Trait;

/**
 * Defines a status where a character has increased proficiency with fingers/mouth/genitals
 */
public class ProfessionalMomentum extends Status {
	public ProfessionalMomentum(Character affected, int percent) {
		super("Professional Momentum", affected);
		this.magnitude = percent;
		stacking = true;
		lingering = false;
		decaying = false;
		tooltip = "Bonus finger, mouth, and intercourse proficiency (" + magnitude + "%)";
		duration = 3;
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			this.duration = 3 * duration / 2;
		}
	}

	@Override
	public String describe() {
		return null;
	}

	public float proficiency(Anatomy using) {
		if (using == Anatomy.fingers || using == Anatomy.mouth || using == Anatomy.genitals) {
			return 1.0f + (magnitude / 100f);
		}
		return 1.0f;
	}

	@Override
	public Status copy(Character target) {
		return new ProfessionalMomentum(affected, magnitude);
	}
}
