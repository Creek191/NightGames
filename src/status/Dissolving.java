package status;

import characters.Character;
import characters.Trait;
import combat.Combat;

/**
 * Defines a status where a character's clothes are dissolving
 */
public class Dissolving extends Status {

	public Dissolving(Character affected) {
		super("Dissolving", affected);
		duration = 2;
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			duration = 3;
		}
		flag(Stsflag.dissolving);
		tooltip = "An article of clothing dissolves each turn";
		this.affected = affected;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "The corrosive solution continues to eat away at your clothes.";
		}
		else {
			return affected.name() + "'s clothes are rapidly deteriorating.";
		}
	}

	@Override
	public Status copy(Character target) {
		return new Dissolving(target);
	}

	@Override
	public void turn(Combat c) {
		var lost = affected.shredRandom();
		if (lost != null) {
			if (affected.human()) {
				c.write("Your " + lost.getName() + " dissolves away.");
			}
			else {
				c.write("Her " + lost.getName() + " dissolves away.");
			}
		}
		if (affected.nude()) {
			affected.removeStatus(this, c);
			return;
		}
		decay(c);
	}
}
