package status;

import characters.Character;
import characters.*;
import combat.Combat;

/**
 * Defines a status where a character becomes frigid, massively reducing pleasure taken, at the cost of mojo
 */
public class IceStance extends Status {
	public IceStance(Character affected) {
		super("Ice Form", affected);
		duration = 10;
		flag(Stsflag.form);
		tooltip = "2% pleasure resistance per point of Ki, no Mojo gain";
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			duration = 15;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "You're as frigid as a glacier.";
		}
		else {
			return affected.name() + " is cool as ice.";
		}
	}

	@Override
	public int pleasure(int x, Anatomy area) {
		return -x * (Math.min((affected.getEffective(Attribute.Ki) * 2), 80) / 100);
	}

	@Override
	public int gain(Pool res, int x) {
		if (res == Pool.MOJO) {
			return -x;
		}
		return 0;
	}

	@Override
	public Status copy(Character target) {
		return new IceStance(target);
	}

	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident, 5);
		affected.emote(Emotion.dominant, 5);
		decay(c);
	}
}
