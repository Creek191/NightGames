package status;

import characters.Character;
import characters.Trait;

/**
 * Defines a status that gives a bonus to evasion and counter attacks
 */
public class Alert extends Status {
	public Alert(Character affected) {
		super("Alert", affected);
		this.magnitude = 5;
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			duration = 5;
		}
		else {
			duration = 3;
		}
		flag(Stsflag.alert);
		tooltip = "Evasion and Counter bonus";
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public int evade() {
		return magnitude;
	}

	@Override
	public int counter() {
		return magnitude * 3;
	}

	@Override
	public Status copy(Character target) {
		return new Alert(target);
	}
}
