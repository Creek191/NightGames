package status;

import characters.Anatomy;
import characters.Character;

/**
 * Defines a status where a character is oiled up, making escape easier
 */
public class Oiled extends Status {

	public Oiled(Character affected) {
		super("Oiled", affected);
		lingering = true;
		flag(Stsflag.oiled);
		tooltip = "-25% pleasure resistance, easier to escape pins";
		this.affected = affected;
		decaying = false;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "Your skin is slick with oil and kinda feels weird.";
		}
		else {
			return affected.name() + " is shiny with lubricant, making you more tempted to touch and rub her skin.";
		}
	}

	@Override
	public int escape() {
		return 10;
	}

	@Override
	public int pleasure(int x, Anatomy area) {
		return x / 4;
	}

	@Override
	public Status copy(Character target) {
		return new Oiled(target);
	}
}
