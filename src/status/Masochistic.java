package status;

import characters.Anatomy;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;

/**
 * Defines a status where a character gets aroused by pain
 */
public class Masochistic extends Status {

	public Masochistic(Character affected) {
		super("Masochism", affected);
		flag(Stsflag.masochism);
		flag(Stsflag.mindaffecting);
		duration = 10;
		tooltip = "Gain Arousal whenever you are hurt (150% of damage taken)";
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			duration = 15;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "Arousing fantasies of being hurt continue to tempt you.";
		}
		else {
			return affected.name() + " is still flushed with arousal at the idea of being struck.";
		}
	}

	@Override
	public int damage(int x, Anatomy area) {
		if (area != Anatomy.soul) {
			affected.getArousal().restore(3 * x / 2);
		}
		return 0;
	}

	@Override
	public Status copy(Character target) {
		return new Masochistic(target);
	}

	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous, 15);
		decay(c);
	}
}
