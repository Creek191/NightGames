package status;

import characters.Anatomy;
import characters.Character;
import characters.Emotion;
import combat.Combat;

/**
 * Defines a status where a character has mastered their penis, increasing pleasure dealt and taking less from it
 */
public class GoldenCock extends Status {

	public GoldenCock(Character affected) {
		super("Golden Cock", affected);
		this.flag(Stsflag.goldencock);
		this.duration = 20;
		this.affected = affected;
		tooltip = "Deal double pleasure with penis, receive half pleasure on penis.";
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "Your cock glows with incredible power";
		}
		else {
			return affected.name() + "'s penis glows intimidatingly.";
		}
	}

	@Override
	public float proficiency(Anatomy using) {
		if (using == Anatomy.genitals) {
			return 2.0f;
		}
		return 1.0f;
	}

	@Override
	public float sensitive(Anatomy targeted) {
		if (targeted == Anatomy.genitals) {
			return .5f;
		}
		return 1.0f;
	}

	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.dominant, 10);
		decay(c);
	}

	@Override
	public Status copy(Character target) {
		return new GoldenCock(target);
	}
}
