package status;

import characters.Attribute;
import characters.Character;
import characters.Trait;

/**
 * Defines a status that buffs a single attribute
 */
public class Abuff extends Status {
	private final Attribute modded;

	public Abuff(Character affected, Attribute att, int value, int duration) {
		super(att + " buff", affected);
		this.modded = att;
		this.magnitude = value;
		if (value < 0) {
			this.name = att + " debuff";
		}
		stacking = true;
		lingering = true;
		tooltip = att + "buff (" + value + ")";
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			this.duration = 3 * duration / 2;
		}
		else {
			this.duration = duration;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public int mod(Attribute a) {
		if (a == modded) {
			return magnitude;
		}
		return 0;
	}

	@Override
	public Status copy(Character target) {
		return new Abuff(target, modded, magnitude, duration);
	}
}
