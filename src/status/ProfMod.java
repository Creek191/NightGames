package status;

import characters.Anatomy;
import characters.Character;
import characters.Trait;

/**
 * Defines a status where a character gains a bonus to their proficiency with a specific body part
 */
public class ProfMod extends Status {
	private final Anatomy part;

	public ProfMod(String name, Character affected, Anatomy part, int percent) {
		super(name, affected);
		this.magnitude = percent;
		this.part = part;
		stacking = true;
		tooltip = "Bonus " + part.name() + " proficiency";
		duration = 3;
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			this.duration = 3 * duration / 2;
		}
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public float proficiency(Anatomy using) {
		if (using == part) {
			return 1.0f + (magnitude / 100f);
		}
		return 1.0f;
	}

	@Override
	public Status copy(Character target) {
		return new ProfMod(name, affected, part, magnitude);
	}
}
