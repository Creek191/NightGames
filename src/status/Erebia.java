package status;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;

/**
 * Defines a status where a character gains power from recently picked up magic
 */
public class Erebia extends Status {

	public Erebia(Character affected, int duration) {
		super("Mana Fortification", affected);
		this.duration = duration;
		flag(Stsflag.erebia);
		tooltip = "Bonus to Power, Seduction, Speed, and Stamina regen";
		this.magnitude = affected.getEffective(Attribute.Arcane) / 3;
		if (affected.has(Trait.PersonalInertia)) {
			this.duration = 3 * duration / 2;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "Magic is still flowing through your body, giving you tremendous speed and power";
		}
		else {
			return affected.name() + " is glowing with powerful magic.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if (a == Attribute.Power || a == Attribute.Seduction) {
			return magnitude;
		}
		else if (a == Attribute.Speed) {
			return magnitude / 2;
		}
		return 0;
	}

	@Override
	public int regen() {
		return 3;
	}

	@Override
	public int evade() {
		return 5;
	}

	@Override
	public int escape() {
		return 5;
	}

	@Override
	public int value() {
		return magnitude;
	}

	@Override
	public Status copy(Character target) {
		return new Erebia(target, duration);
	}

	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident, 25);
		affected.emote(Emotion.dominant, 25);
		decay(c);
	}
}
