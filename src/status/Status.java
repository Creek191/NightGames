package status;

import characters.Character;
import characters.*;
import combat.Combat;
import items.Clothing;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Base class for all status
 */
public abstract class Status {
	protected String name;
	protected String tooltip;
	protected Character affected;
	protected HashSet<Stsflag> flags;
	protected ArrayList<Trait> traits;
	protected int magnitude;
	protected int duration;
	protected boolean stacking;
	protected boolean lingering;
	protected boolean decaying;
	protected boolean fixed;

	public Status() {

	}

	public Status(String name, Character affected) {
		this.name = name;
		flags = new HashSet<>();
		traits = new ArrayList<>();
		this.magnitude = 0;
		this.duration = 0;
		stacking = false;
		lingering = false;
		decaying = true;
		this.affected = affected;
		fixed = false;
	}

	/**
	 * Gets the name of the status
	 */
	public String toString() {
		return name;
	}

	/**
	 * Gets a description of the status from the player's perspective
	 */
	public abstract String describe();

	/**
	 * Gets a tooltip for the status, including magnitude and remaining duration
	 */
	public String getTooltip() {
		var tip = tooltip;
		if (magnitude != 0) {
			tip += " (" + mag() + ")";
		}
		if (duration > 0) {
			tip += "; " + duration + " turns";
		}
		return tip;
	}

	/**
	 * Gets the modifier for the specified attribute applied by the status
	 *
	 * @param a The attribute to check
	 */
	public int mod(Attribute a) {
		return 0;
	}

	/**
	 * Gets the amount of stamina regeneration the status gives per turn
	 */
	public int regen() {
		return 0;
	}

	/**
	 * Gets extra damage to the specified area based on the status
	 *
	 * @param x    The base damage
	 * @param area The area affected
	 * @return The bonus damage
	 */
	public int damage(int x, Anatomy area) {
		return 0;
	}

	/**
	 * Gets extra pleasure to the specified area based on the status
	 *
	 * @param x    The base pleasure
	 * @param area The area affected
	 * @return The bonus pleasure
	 */
	public int pleasure(int x, Anatomy area) {
		return 0;
	}

	/**
	 * Gets extra weakness to the specified area based on the status
	 *
	 * @param x The base weakness
	 * @return The bonus weakness
	 */
	public int weakened(int x) {
		return 0;
	}

	/**
	 * Gets extra temptation to the specified area based on the status
	 *
	 * @param x The base temptation
	 * @return The bonus temptation
	 */
	public int tempted(int x) {
		return 0;
	}

	/**
	 * Gets extra evasion provided by the status
	 *
	 * @return The bonus evasion
	 */
	public int evade() {
		return 0;
	}

	/**
	 * Gets extra escape chance provided by the status
	 *
	 * @return The bonus escape chance
	 */
	public int escape() {
		return 0;
	}

	/**
	 * Gets extra gains for the specified pool provided by the status
	 *
	 * @param res The resource gaining a value
	 * @param x   The gained value
	 * @return The bonus gains
	 */
	public int gain(Pool res, int x) {
		return 0;
	}

	/**
	 * Gets extra cost for the specified pool provided by the status
	 *
	 * @param res The resource being spent
	 * @param x   The spent amount
	 * @return The bonus cost
	 */
	public int spend(Pool res, int x) {
		return 0;
	}

	/**
	 * Gets extra counter chance provided by the status
	 *
	 * @return The bonus counter chance
	 */
	public int counter() {
		return 0;
	}

	/**
	 * Gets the value or magnitude of the status
	 */
	public int value() {
		return 0;
	}

	/**
	 * Gets extra strip chance provided by the status
	 *
	 * @param article The clothing that is being stripped
	 * @return The bonus strip chance
	 */
	public int stripAttempt(Clothing article) {
		return 0;
	}

	/**
	 * Checks whether the status provides the specified trait
	 *
	 * @param bestows The trait to check for
	 */
	public boolean tempTraits(Trait bestows) {
		return traits.contains(bestows);
	}

	/**
	 * Passes a turn, applying any continuous effects of the status
	 *
	 * @param c A reference to the ongoing combat
	 */
	public void turn(Combat c) {
		if (decaying) {
			decay(c);
		}
	}

	/**
	 * Creates a copy of the status
	 *
	 * @param target The character the status is applied to
	 */
	public abstract Status copy(Character target);

	/**
	 * Checks whether the status lingers after combat
	 */
	public boolean lingering() {
		return lingering || fixed;
	}

	/**
	 * Adds the specified flag to the status
	 *
	 * @param status The flag to add
	 */
	public void flag(Stsflag status) {
		flags.add(status);
	}

	/**
	 * Gets the flags applied to the status
	 */
	public HashSet<Stsflag> flags() {
		return this.flags;
	}

	/**
	 * Checks whether multiple instances of the status can stack
	 */
	public boolean stacking() {
		return stacking;
	}

	/**
	 * Gets the magnitude of the status
	 */
	public int mag() {
		return magnitude;
	}

	/**
	 * Gets the remaining duration of the status
	 */
	public int duration() {
		return duration;
	}

	/**
	 * Gets the proficiency modifier when using the specified body part
	 *
	 * @param using The body part being used
	 */
	public float proficiency(Anatomy using) {
		return 1.0f;
	}

	/**
	 * Gets the sensitivity modifier when the specified body part is being targeted
	 *
	 * @param targeted The body part being targeted
	 */
	public float sensitive(Anatomy targeted) {
		return 1.0f;
	}

	/**
	 * Gets the soreness modifier when the specified body part is being targeted
	 *
	 * @param targeted The body part being targeted
	 */
	public float sore(Anatomy targeted) {
		return 1.0f;
	}

	/**
	 * Applies another stack of the status to an already existing one
	 *
	 * @param other The status that is stacked onto the existing one
	 */
	public void stack(Status other) {
		this.magnitude += other.mag();
		if (other.duration > this.duration) {
			this.duration = other.duration();
		}
		if (this.magnitude == 0) {
			affected.removeStatus(this);
			if (flags().contains(Stsflag.mindaffecting)) {
				affected.add(new Cynical(affected));
			}
		}
	}

	/**
	 * Checks whether the other status is the same as this one
	 *
	 * @param other The status to compare
	 */
	public boolean equals(Status other) {
		return this.name.equals(other.name);
	}

	/**
	 * Decays the status, removing it when it runs out
	 *
	 * @param c A reference to the ongoing combat
	 */
	public void decay(Combat c) {
		if (!fixed) {
			if (duration <= 0) {
				affected.removeStatus(this, c);
				if (flags().contains(Stsflag.mindaffecting)) {
					affected.add(new Cynical(affected), c);
				}
			}
			duration--;
		}
	}

	/**
	 * Decays the status, removing it when it runs out
	 */
	public void decay() {
		if (!fixed) {
			if (duration <= 0) {
				affected.removeStatus(this);
				if (flags().contains(Stsflag.mindaffecting)) {
					affected.add(new Cynical(affected));
				}
			}
			duration--;
		}
	}

	/**
	 * Checks whether the specified status can be removed
	 */
	public boolean removable() {
		return !fixed;
	}

	/**
	 * Marks the specified status as fixed and non-removable
	 */
	public void fix() {
		fixed = true;
	}
}
