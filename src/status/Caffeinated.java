package status;

import characters.Character;
import characters.Trait;
import combat.Combat;

/**
 * Defines a status where the character regenerates mojo from caffeine
 */
public class Caffeinated extends Status {
	public Caffeinated(Character affected, int duration, int magnitude) {
		super("Caffeinated", affected);
		this.duration = duration;
		this.magnitude = magnitude;
		stacking = true;
		lingering = true;
		tooltip = "Regenerate Stamina each turn";
		if (affected != null && affected.has(Trait.PersonalInertia)) {
			this.duration = 3 * duration / 2;
		}
		this.affected = affected;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "You feel the effects of the energy drink filling you with energy.";
		}
		else {
			return affected.name() + " looks a little hyper.";
		}
	}

	@Override
	public int regen() {
		return magnitude;
	}

	@Override
	public Status copy(Character target) {
		return new Caffeinated(target, duration, magnitude);
	}

	@Override
	public void turn(Combat c) {
		affected.buildMojo(magnitude);
		decay(c);
	}
}
