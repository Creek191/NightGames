package status;

import characters.Attribute;
import characters.Character;

/**
 * Defines a status where a character is tied up, reducing their evasion and increasing their arousal
 */
public class Tied extends Status {

	public Tied(Character affected) {
		super("Tied Up", affected);
		flag(Stsflag.tied);
		tooltip = "Penalty to evasion, trigger arousal effect from Bondage";
		this.affected = affected;
		decaying = false;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "The rope wrapped around you digs into your body, but only slows you down a bit.";
		}
		else {
			return affected.name() + " squirms against the rope, but you know you tied it well.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if (a == Attribute.Speed) {
			return -1;
		}
		return 0;
	}

	@Override
	public int evade() {
		return -10;
	}

	@Override
	public Status copy(Character target) {
		return new Tied(target);
	}
}
