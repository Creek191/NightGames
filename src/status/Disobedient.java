package status;

import characters.Anatomy;
import characters.Character;
import characters.Pool;
import characters.Trait;

/**
 * Defines a status where a character cannot evade anymore after disobeying an order
 */
public class Disobedient extends Status {
	public Disobedient(Character affected) {
		super("Disobedient", affected);
		flag(Stsflag.disobedient);
		flag(Stsflag.mindaffecting);
		if (affected.has(Trait.PersonalInertia)) {
			this.duration = 9;
		}
		else {
			this.duration = 6;
		}
		tooltip = "Receive double damage and unable to evade, gain no Mojo";
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "You disobeyed your master's command and await punishment";
		}
		else {
			return affected.name() + " has disobeyed your order and deserves punishment";
		}
	}

	@Override
	public int damage(int x, Anatomy area) {
		return x;
	}

	@Override
	public int weakened(int x) {
		return x;
	}

	@Override
	public int evade() {
		return -99;
	}

	@Override
	public int gain(Pool res, int x) {
		if (res == Pool.MOJO) {
			return -x;
		}
		return 0;
	}

	@Override
	public Status copy(Character target) {
		return new Disobedient(affected);
	}
}
