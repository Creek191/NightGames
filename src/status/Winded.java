package status;

import characters.Character;
import characters.*;
import combat.Combat;

/**
 * Defines a status where a character is exhausted and can't act until they recover
 */
public class Winded extends Status {
	private final Character opponent;

	public Winded(Character affected, Character opponent) {
		this(affected, opponent, 3);
	}

	public Winded(Character affected, Character opponent, int duration) {
		super("Winded", affected);
		this.opponent = opponent;
		flag(Stsflag.stunned);
		var d = duration;
		if (affected.has(Trait.rapidrecovery)) {
			d--;
		}
		if (affected.has(Trait.juggernaut)) {
			d--;
		}
		if (opponent.has(Trait.staydown)) {
			d++;
		}
		if (opponent.has(Trait.foulqueen)) {
			d++;
		}
		this.duration = d;
		tooltip = "Unable to act or evade, Stamina recovers quickly, take reduced Temptation damage";
		this.affected = affected;
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "You need a moment to catch your breath";
		}
		else {
			return affected.name() + " is panting and trying to recover";
		}
	}

	@Override
	public int mod(Attribute a) {
		if (a == Attribute.Power || a == Attribute.Speed) {
			return -2;
		}
		else {
			return 0;
		}
	}

	@Override
	public int regen() {
		return affected.getStamina().max() / 4;
	}

	@Override
	public int pleasure(int x, Anatomy area) {
		if (opponent.has(Trait.opportunist)) {
			return x / 5;
		}
		else {
			return 0;
		}
	}

	@Override
	public int tempted(int x) {
		return -x / 2;
	}

	@Override
	public int evade() {
		return -99;
	}

	@Override
	public int counter() {
		return -99;
	}

	@Override
	public Status copy(Character target) {
		Status copy = new Winded(target, opponent);
		copy.duration = duration;
		return copy;
	}

	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous, 15);
		affected.emote(Emotion.angry, 10);
		decay(c);
	}
}
