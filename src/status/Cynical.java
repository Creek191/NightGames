package status;

import characters.Character;
import characters.Pool;
import characters.Trait;

/**
 * Defines a status where the character cannot be affected by mind-altering status anymore
 */
public class Cynical extends Status {

	public Cynical(Character affected) {
		super("Cynical", affected);
		flag(Stsflag.cynical);
		tooltip = "Immune to mind-affecting statuses, gain no Mojo";
		this.affected = affected;
		if (affected.has(Trait.PersonalInertia)) {
			duration = 5;
		}
		else {
			duration = 3;
		}
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "You're feeling more cynical than usual and won't fall for any mind games.";
		}
		else {
			return affected.name() + " has a cynical edge in her eyes.";
		}
	}

	@Override
	public int tempted(int x) {
		return -x / 4;
	}

	@Override
	public int gain(Pool p, int x) {
		if (p == Pool.MOJO) {
			return -x;
		}
		else {
			return 0;
		}
	}

	@Override
	public Status copy(Character target) {
		return new Cynical(target);
	}
}
