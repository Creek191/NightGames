package status;

import characters.Character;
import characters.Trait;

/**
 * Defines a status where a character cannot summon pets
 */
public class Isolated extends Status {
	public Isolated(Character affected) {
		super("Isolated", affected);
		flag(Stsflag.isolated);
		duration = 20;
		tooltip = "Unable to summon pets";
		traits.add(Trait.summonblock);
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "You feel cut off from anything you would try to summon.";
		}
		else {
			return affected.name() + " is cut off from any pets.";
		}
	}

	@Override
	public Status copy(Character target) {
		return new Isolated(target);
	}
}
