package status;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;

/**
 * Defines a status where a character gains pleasure resistance through being calm
 */
public class Tranquil extends Status {

	public Tranquil(Character affected) {
		super("Tranquil", affected);
		flag(Stsflag.tranquil);
		this.duration = 4;
		if (affected.has(Trait.PersonalInertia)) {
			this.duration = 6;
		}
		magnitude = Math.min(70, affected.getEffective(Attribute.Spirituality) * 5);
		tooltip = magnitude + "% Pleasure resistance";
	}

	@Override
	public String describe() {
		if (affected.human()) {
			return "Your aura of tranquility resists pleasure";
		}
		else {
			return affected.name() + " resists pleasure with impressive tranquility.";
		}
	}

	@Override
	public int pleasure(int x, Anatomy area) {
		return Math.round(x * -.01f);
	}

	@Override
	public Status copy(Character target) {
		return new Tranquil(target);
	}
}
