package scenes;

import java.util.HashMap;

/**
 * Defines an event played during daytime
 */
public interface Event {

	/**
	 * Advances the scene with the specified response
	 *
	 * @param response The response to send
	 */
	void respond(String response);

	/**
	 * Plays the scene with the specified name
	 *
	 * @param response The name of the scene
	 * @return True if an event requiring user input was started, otherwise False
	 */
	boolean play(String response);

	/**
	 * Gets the name of the event that must be played in the morning
	 *
	 * @return The name of the event, or an empty string if there is none
	 */
	String morning();

	/**
	 * Gets the name of a mandatory event that should be played when possible
	 *
	 * @return The name of the event, or an empty string if there is none
	 */
	String mandatory();

	/**
	 * Adds any available scene to the specified list
	 * <p>
	 * The integer passed along with the scene defines its weight when picking a random one
	 *
	 * @param available The list to add available scenes to
	 */
	void addAvailable(HashMap<String, Integer> available);
}
