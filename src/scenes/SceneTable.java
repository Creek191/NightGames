package scenes;

/**
 * Defines a container for a character's scenes
 */
public abstract class SceneTable {

	/**
	 * Gets the scene matching the specified flag
	 *
	 * @param flag The flag of the scene
	 * @return The scene matching the flag, or an error scene if none was found
	 */
	public Scene getScene(SceneFlag flag) {
		switch (flag.getType()) {
			case VICTORY:
				return getVictoryScene(flag);
			case DEFEAT:
				return getDefeatScene(flag);
			case DRAW:
				return getDrawScene(flag);
			case INTERVENTION:
				return getInterventionScene(flag);
			case EVENT:
				return getEventScene(flag);
			case DAYTIME:
				return getDayScene(flag);
		}
		return new Scene("Error! Scene " + flag.getLabel() + " not found.");
	}

	/**
	 * Gets the victory scene matching the specified flag
	 *
	 * @param flag The flag of the scene
	 */
	protected abstract Scene getVictoryScene(SceneFlag flag);

	/**
	 * Gets the defeat scene matching the specified flag
	 *
	 * @param flag The flag of the scene
	 */
	protected abstract Scene getDefeatScene(SceneFlag flag);

	/**
	 * Gets the draw scene matching the specified flag
	 *
	 * @param flag The flag of the scene
	 */
	protected abstract Scene getDrawScene(SceneFlag flag);

	/**
	 * Gets the intervention scene matching the specified flag
	 *
	 * @param flag The flag of the scene
	 */
	protected abstract Scene getInterventionScene(SceneFlag flag);

	/**
	 * Gets the event scene matching the specified flag
	 *
	 * @param flag The flag of the scene
	 */
	protected abstract Scene getEventScene(SceneFlag flag);

	/**
	 * Gets the daytime scene matching the specified flag
	 *
	 * @param flag The flag of the scene
	 */
	protected abstract Scene getDayScene(SceneFlag flag);
}
