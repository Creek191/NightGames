package scenes;

import characters.Character;
import global.Global;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Defines a scene
 */
public class Scene {
    private final List<List<String>> images;
    private List<String> text;

    public Scene() {
        this.text = new ArrayList<>();
        this.images = new ArrayList<>();
    }

    public Scene(String text) {
        this();
        this.text.add(text);
    }

    public Scene(String text, List<List<String>> images) {
        this(text);
        this.images.addAll(images);
    }

    public Scene(List<String> text) {
        this();
        this.text.addAll(text);
    }

    public Scene(List<String> text, List<List<String>> images) {
        this(text);
        this.images.addAll(images);
    }

    /**
     * Adds the specified text as a new line to a scene
     *
     * @param text The text to add to the scene
     */
    public void addText(String text) {
        this.text.add(text);
    }

    /**
     * Adds an image to the next page without one
     *
     * @param path   The path to the image asset
     * @param author The author of the image
     */
    public void addImage(String path, String author) {
        this.images.add(Arrays.asList(path, author));
    }

    /**
     * Gets the number of pages the scene has
     */
    public int getPages() {
        return Math.min(1, text.size() - images.size());
    }

    /**
     * Plays the specified page of the scene
     *
     * @param page The page of the scene
     */
    public void play(int page) {
        var index = page + Math.min(page, images.size());
        if (text.size() > index) {
            Global.gui().message(text.get(index));
            if (images.size() > page) {
                Global.gui().displayImage(images.get(page).get(0), images.get(page).get(1));
            }
            index++;
            if (text.size() > index) {
                Global.gui().message(text.get(index));
            }
        }
        else {
            Global.gui().message("Error in scene: page " + index + " of " + text.size() + " requested.");
        }
    }

    /**
     * Parses a scene involving only the player
     *
     * @param player The player character
     */
    public void parse(Character player) {
        parse(player, null);
    }

    /**
     * Parses a scene involving the player and an NPC
     *
     * @param player The player
     * @param npc    The NPC
     */
    public void parse(Character player, Character npc) {
        parse(player, npc, null);
    }

    /**
     * Parses a scene involving the player and two NPCs
     *
     * @param player The player
     * @param npc    The first NPC
     * @param npc2   The second NPC
     */
    public void parse(Character player, Character npc, Character npc2) {
        List<String> parsed = new ArrayList<>();
        for (var raw : text) {
            // Replace player references
            raw = raw.replaceAll("\\[PLAYER\\]", player.name());
            raw = raw.replaceAll("\\[Player\\]", player.name());
            if (npc != null) {
                // Replace first NPC references
                raw = raw.replaceAll("\\[NPC\\]", npc.name());
                raw = raw.replaceAll("\\[OPPONENT\\]", npc.name());
                raw = raw.replaceAll("\\[heshe\\]", npc.pronounSubject(false));
                raw = raw.replaceAll("\\[HeShe\\]", npc.pronounSubject(true));
                raw = raw.replaceAll("\\[hisher\\]", npc.pronounSubject(false));
                raw = raw.replaceAll("\\[HisHer\\]", npc.pronounSubject(true));
                raw = raw.replaceAll("\\[himher\\]", npc.pronounTarget(false));
                raw = raw.replaceAll("\\[HimHer\\]", npc.pronounTarget(true));
            }
            if (npc2 != null) {
                // Replace second NPC references
                raw = raw.replaceAll("\\[NPC2\\]", npc2.name());
                raw = raw.replaceAll("\\[heshe2\\]", npc2.pronounSubject(false));
                raw = raw.replaceAll("\\[HeShe2\\]", npc2.pronounSubject(true));
                raw = raw.replaceAll("\\[hisher2\\]", npc2.pronounSubject(false));
                raw = raw.replaceAll("\\[HisHer2\\]", npc2.pronounSubject(true));
                raw = raw.replaceAll("\\[himher2\\]", npc2.pronounTarget(false));
                raw = raw.replaceAll("\\[HimHer2\\]", npc2.pronounTarget(true));
            }

            // Turn quotes into italics
            raw = raw.replaceAll(" \\\"", " <i>\\\"");
            raw = raw.replaceAll("\\\" ", "\\\"</i> ");
            raw = raw.replaceAll("<p>\\\"", "<p><i>\\\"");
            raw = raw.replaceAll("\\\"<p>", "\\\"</i><p>");
            raw = raw.replaceAll("\\n", "<p>");
            raw = raw.replaceAll("“", "<i>\\\"");
            raw = raw.replaceAll("”", "\\\"</i>");
            raw = raw.replaceAll("…", "...");
            parsed.add(raw);
        }
        text = parsed;
    }
}
