package scenes;

import global.Flag;
import global.Global;
import global.Scheduler;

import java.time.LocalTime;
import java.util.HashMap;

/**
 * Defines events involving Angel
 */
public class AngelEvent implements Event {
    public AngelEvent() {
    }

    @Override
    public void respond(String response) {
        Global.gui().clearText();
        Scheduler.advanceTime(LocalTime.of(1, 0));
        Scheduler.getDay().plan();
    }

    @Override
    public boolean play(String response) {
        if (response.equalsIgnoreCase("Sarah's Flower")) {
            SceneManager.play(SceneFlag.AngelSarahFlower);
            Global.flag(Flag.CorruptSarah);
        }
        Global.current = this;
        Global.gui().choose("Next");
        return true;
    }

    @Override
    public String morning() {
        return "";
    }

    @Override
    public String mandatory() {
        return "";
    }

    @Override
    public void addAvailable(HashMap<String, Integer> available) {
        if (Global.checkFlag(Flag.CorruptCaroline) && !Global.checkFlag(Flag.CorruptSarah)) {
            available.put("Sarah's Flower", 10);
        }
    }
}
