package scenes;

import characters.ID;

/**
 * Defines all scene flags
 */
public enum SceneFlag {
	//CASSIE
	CassieAfterMatch("After Match", SceneType.EVENT, ID.CASSIE),

	CassieForeplayDefeatBasic("Foreplay Basic", SceneType.DEFEAT, ID.CASSIE),
	CassieForeplayDefeatAlt("Foreplay Alternate", SceneType.DEFEAT, ID.CASSIE),
	CassieForeplayDefeatManaDrain("Mana Drain", SceneType.DEFEAT, ID.CASSIE),
	CassieForeplayDefeatEasy("Easy", SceneType.DEFEAT, ID.CASSIE),
	CassieForeplayDefeatClothed("Clothed", SceneType.DEFEAT, ID.CASSIE),
	CassieForeplayDefeatRoleplay("Roleplay", SceneType.DEFEAT, ID.CASSIE),
	CassieSexDefeat("Sex", SceneType.DEFEAT, ID.CASSIE),
	CassieAnalDefeat("Anal", SceneType.DEFEAT, ID.CASSIE),

	CassieForeplayVictoryBasic("Foreplay Basic", SceneType.VICTORY, ID.CASSIE),
	CassieForeplayVictoryAlt("Foreplay Alt", SceneType.VICTORY, ID.CASSIE),
	CassieForeplayVictoryEasy("Easy", SceneType.VICTORY, ID.CASSIE),
	CassieForeplayVictoryEasyAlt("Easy Alternate", SceneType.VICTORY, ID.CASSIE),
	CassieMagicVictory("Magic", SceneType.VICTORY, ID.CASSIE),
	CassieHornyVictory("Horny", SceneType.VICTORY, ID.CASSIE),
	CassieFaerieVictory("Faerie", SceneType.VICTORY, ID.CASSIE),
	CassieSexVictory("Sex", SceneType.VICTORY, ID.CASSIE),
	CassiePeggingVictory("Pegging", SceneType.VICTORY, ID.CASSIE),

	CassieForeplayDraw("Foreplay", SceneType.DRAW, ID.CASSIE),
	CassieSexDraw("Sex", SceneType.DRAW, ID.CASSIE),
	CassieMagicDraw("Magic", SceneType.DRAW, ID.CASSIE),

	CassieWatch("Watch", SceneType.INTERVENTION, ID.CASSIE),
	CassieWatchMagic("Magic", SceneType.INTERVENTION, ID.CASSIE),
	CassieWatchPenis("Magic (versus penis)", SceneType.INTERVENTION, ID.CASSIE),

	CassieSparring3("Sparring 3", SceneType.DAYTIME, ID.CASSIE),

	//ANGEL
	AngelAfterMatch("After Match", SceneType.EVENT, ID.ANGEL),

	AngelForeplayDefeat("Foreplay", SceneType.DEFEAT, ID.ANGEL),
	AngelSexDefeat("Sex", SceneType.DEFEAT, ID.ANGEL),
	AngelForeplayDefeatAlt("Foreplay Alt", SceneType.DEFEAT, ID.ANGEL),
	AngelSuccubusDefeat("Succubus", SceneType.DEFEAT, ID.ANGEL),
	AngelMasochismDefeat("Masochism", SceneType.DEFEAT, ID.ANGEL),
	AngelImpDefeat("Imp", SceneType.DEFEAT, ID.ANGEL),
	AngelAnalDefeat("Anal", SceneType.DEFEAT, ID.ANGEL),
	AngelKissDefeat("Kiss", SceneType.DEFEAT, ID.ANGEL),

	AngelForeplayVictory("Foreplay", SceneType.VICTORY, ID.ANGEL),
	AngelForeplayVictoryAlt("Foreplay Alt", SceneType.VICTORY, ID.ANGEL),
	AngelSexVictory("Sex", SceneType.VICTORY, ID.ANGEL),
	AngelImpVictory("Imp", SceneType.VICTORY, ID.ANGEL),
	AngelSuccubusVictory("Succubus", SceneType.VICTORY, ID.ANGEL),
	AngelHornyVictory("Horny", SceneType.VICTORY, ID.ANGEL),
	AngelFlyingVictory("Flying", SceneType.VICTORY, ID.ANGEL),
	AngelPeggingVictoryFirst("Pegging First", SceneType.VICTORY, ID.ANGEL),
	AngelPeggingVictory("Angel Pegging", SceneType.VICTORY, ID.ANGEL),
	AngelPeggingVictorySubmission("Pegging Submission", SceneType.VICTORY, ID.ANGEL),

	AngelForeplayDraw("Foreplay", SceneType.DRAW, ID.ANGEL),
	AngelSexDraw("Sex", SceneType.DRAW, ID.ANGEL),
	AngelSuccubusDraw("Succubus", SceneType.DRAW, ID.ANGEL),

	AngelWatch("Watch", SceneType.INTERVENTION, ID.ANGEL),
	AngelWatchDark("Watch Dark", SceneType.INTERVENTION, ID.ANGEL),

	AngelMeiPleasure("Mei's Pleasure", SceneType.DAYTIME, ID.ANGEL),
	AngelCarolineKiss("Caroline's Kiss", SceneType.DAYTIME, ID.ANGEL),
	AngelSarahFlower("Sarah's Flower", SceneType.DAYTIME, ID.ANGEL),
	AngelRevelation("Angel's Revelation", SceneType.DAYTIME, ID.ANGEL),

	//MARA
	MaraAfterMatch("After Match", SceneType.EVENT, ID.MARA),
	MaraSickDayAfterMatch("Day Off After Match", SceneType.EVENT, ID.MARA),
	MaraSickDayNight("Day Off Night", SceneType.EVENT, ID.MARA),
	MaraSickDayMorning("Day Off Morning", SceneType.EVENT, ID.MARA),
	MaraSickDayShower("Day Off Shower", SceneType.EVENT, ID.MARA),
	MaraSickDayInterrogation("Day Off Interrogation", SceneType.EVENT, ID.MARA),
	MaraSickDayLunch("Day Off Lunch", SceneType.EVENT, ID.MARA),
	MaraSickDayFinale("Day Off Finale", SceneType.EVENT, ID.MARA),

	MaraForeplayDefeat("Foreplay", SceneType.DEFEAT, ID.MARA),
	MaraForeplayDefeatAlt("Foreplay Alt", SceneType.DEFEAT, ID.MARA),
	MaraForeplayDefeatSensitive("Sensitive", SceneType.DEFEAT, ID.MARA),
	MaraHornyDefeat("Horny", SceneType.DEFEAT, ID.MARA),
	MaraForeplayDefeatEasy("Easy", SceneType.DEFEAT, ID.MARA),
	MaraTickleDefeat("Tickle", SceneType.DEFEAT, ID.MARA),
	MaraSexDefeat("Sex", SceneType.DEFEAT, ID.MARA),
	MaraPinDefeat("Pin", SceneType.DEFEAT, ID.MARA),
	MaraBoundDefeat("Bound", SceneType.DEFEAT, ID.MARA),
	MaraAnalDefeat("Anal", SceneType.DEFEAT, ID.MARA),

	MaraForeplayVictory("Foreplay", SceneType.VICTORY, ID.MARA),
	MaraSexVictory("Sex", SceneType.VICTORY, ID.MARA),
	MaraPeggingVictory("Pegging", SceneType.VICTORY, ID.MARA),
	MaraPeggingVirginity("Pegging Virginity", SceneType.VICTORY, ID.MARA),
	MaraSlimeVictory("Slime", SceneType.VICTORY, ID.MARA),
	MaraShockVictory("Shock", SceneType.VICTORY, ID.MARA),
	MaraFootjobVictory("Footjob", SceneType.VICTORY, ID.MARA),
	MaraLubeVictory("Lube", SceneType.VICTORY, ID.MARA),
	MaraOnaholeVictory("Onahole", SceneType.VICTORY, ID.MARA),

	MaraSexDraw("Sex", SceneType.DRAW, ID.MARA),
	MaraForeplayDraw("Foreplay", SceneType.DRAW, ID.MARA),
	MaraAphrodisiacDraw("Aphrodisiac", SceneType.DRAW, ID.MARA),
	MaraTemporalDraw("Temporal", SceneType.DRAW, ID.MARA),

	MaraWatch("Watch", SceneType.INTERVENTION, ID.MARA),
	MaraWatchScience("Science", SceneType.INTERVENTION, ID.MARA),

	//JEWEL
	JewelAfterMatch("After Match", SceneType.EVENT, ID.JEWEL),

	JewelForeplayVictory("Easy", SceneType.VICTORY, ID.JEWEL),
	JewelForeplayVictoryAlt("Foreplay", SceneType.VICTORY, ID.JEWEL),
	JewelFireVictory("Fire Form", SceneType.VICTORY, ID.JEWEL),
	JewelPinVictory("Pin", SceneType.VICTORY, ID.JEWEL),
	JewelHornyVictory("Horny", SceneType.VICTORY, ID.JEWEL),
	JewelUltVictory("Pleasure Bomb", SceneType.VICTORY, ID.JEWEL),
	JewelSexVictory("Sex", SceneType.VICTORY, ID.JEWEL),
	JewelPeggingVictory("Pegging", SceneType.VICTORY, ID.JEWEL),
	JewelPeggingVirginity("Pegging Virginity", SceneType.VICTORY, ID.JEWEL),

	JewelForeplayDefeat("Foreplay", SceneType.DEFEAT, ID.JEWEL),
	JewelForeplayDefeatAlt("Foreplay Alt", SceneType.DEFEAT, ID.JEWEL),
	JewelForeplayDefeatEasy("Easy", SceneType.DEFEAT, ID.JEWEL),
	JewelChallengeDefeat("Challenge", SceneType.DEFEAT, ID.JEWEL),
	JewelSexDefeat("Sex", SceneType.DEFEAT, ID.JEWEL),
	JewelReversalDefeat("Sex Reversal", SceneType.DEFEAT, ID.JEWEL),
	JewelAnalDefeat("Anal", SceneType.DEFEAT, ID.JEWEL),
	JewelMasochismDefeat("Masochism", SceneType.DEFEAT, ID.JEWEL),
	JewelPinDefeat("Pin", SceneType.DEFEAT, ID.JEWEL),
	JewelHornyDefeat("Horny", SceneType.DEFEAT, ID.JEWEL),

	JewelForeplayDraw("Foreplay", SceneType.DRAW, ID.JEWEL),
	JewelSexDraw("Sex", SceneType.DRAW, ID.JEWEL),

	JewelWatch("Watch", SceneType.INTERVENTION, ID.JEWEL),

	//REYKA
	ReykaAfterMatch("After Match", SceneType.EVENT, ID.REYKA),

	ReykaForeplayVictory("Foreplay", SceneType.VICTORY, ID.REYKA),
	ReykaSexVictory("Sex", SceneType.VICTORY, ID.REYKA),
	ReykaPeggingVictory("Pegging", SceneType.VICTORY, ID.REYKA),
	ReykaImpVictory("Imp", SceneType.VICTORY, ID.REYKA),
	ReykaEntralledVictory("Enthralled", SceneType.VICTORY, ID.REYKA),
	ReykaFrustratedVictory("Frustrated", SceneType.VICTORY, ID.REYKA),

	ReykaForeplayDefeat("Foreplay", SceneType.DEFEAT, ID.REYKA),
	ReykaForeplayDefeatAlt("Frustrated", SceneType.DEFEAT, ID.REYKA),
	ReykaSexDefeat("Sex", SceneType.DEFEAT, ID.REYKA),
	ReykaAnalDefeat("Anal", SceneType.DEFEAT, ID.REYKA),

	ReykaForeplayDraw("Foreplay", SceneType.DRAW, ID.REYKA),
	ReykaSexDraw("Sex", SceneType.DRAW, ID.REYKA),

	ReykaWatch("Watch", SceneType.INTERVENTION, ID.REYKA),

	//YUI
	YuiAfterMatch("After Match", SceneType.EVENT, ID.YUI),
	YuiFirstAfterMatch("First After Match", SceneType.EVENT, ID.YUI),
	YuiFirstUndress("First Undressing", SceneType.EVENT, ID.YUI),
	YuiFirstService("First Service", SceneType.EVENT, ID.YUI),
	YuiFirstLick("First Eat Out", SceneType.EVENT, ID.YUI),
	YuiFirstSex("First Sex", SceneType.EVENT, ID.YUI),

	YuiForeplayVictory("Foreplay", SceneType.VICTORY, ID.YUI),
	YuiSexVictory("Sex", SceneType.VICTORY, ID.YUI),
	YuiBunshinVictory("Bunshin", SceneType.VICTORY, ID.YUI),
	YuiBoundVictory("Bound", SceneType.VICTORY, ID.YUI),
	YuiPeggingVictory("Pegging", SceneType.VICTORY, ID.YUI),

	YuiForeplayDefeat("Foreplay", SceneType.DEFEAT, ID.YUI),
	YuiForeplayDefeatAlt("Foreplay Alt", SceneType.DEFEAT, ID.YUI),
	YuiSexDefeat("Sex", SceneType.DEFEAT, ID.YUI),
	YuiAnalDefeat("Anal", SceneType.DEFEAT, ID.YUI),
	YuiClothedDefeat("Clothed", SceneType.DEFEAT, ID.YUI),
	YuiShamedDefeat("Shamed", SceneType.DEFEAT, ID.YUI),

	YuiForeplayDraw("Foreplay", SceneType.DRAW, ID.YUI),
	YuiSexDraw("Sex", SceneType.DRAW, ID.YUI),

	YuiWatch("Watch", SceneType.INTERVENTION, ID.YUI),

	//KAT
	KatAfterMatch("After Match", SceneType.EVENT, ID.KAT),

	KatForeplayVictory("Foreplay", SceneType.VICTORY, ID.KAT),
	KatForeplayVictoryEasy("Easy", SceneType.VICTORY, ID.KAT),
	KatFootjobVictory("Footjob", SceneType.VICTORY, ID.KAT),
	KatSexVictory("Sex", SceneType.VICTORY, ID.KAT),
	KatSexVictoryAlt("Alternate Sex", SceneType.VICTORY, ID.KAT),
	KatTailjobVictory("Tailjob", SceneType.VICTORY, ID.KAT),
	KatPeggingVictory("Pegging", SceneType.VICTORY, ID.KAT),

	KatForeplayDefeat("Foreplay", SceneType.DEFEAT, ID.KAT),
	KatForeplayDefeatAlt("Foreplay Alternate", SceneType.DEFEAT, ID.KAT),
	KatBoundDefeat("Bound", SceneType.DEFEAT, ID.KAT),
	KatSexDefeat("Sex", SceneType.DEFEAT, ID.KAT),
	KatSexDefeatCarry("Sex Carry", SceneType.DEFEAT, ID.KAT),
	KatFeralDefeat("Feral", SceneType.DEFEAT, ID.KAT),
	KatAnalDefeat("Anal", SceneType.DEFEAT, ID.KAT),

	KatForeplayDraw("Foreplay", SceneType.DRAW, ID.KAT),
	KatSexDraw("Sex", SceneType.DRAW, ID.KAT),

	KatWatch("Watch", SceneType.INTERVENTION, ID.KAT),
	KatWatchPenis("Watch (versus penis)", SceneType.INTERVENTION, ID.KAT),

	//EVE
	EveInititation("Initiation", SceneType.DAYTIME, ID.EVE),
	EveZoeLowWager("Zoe Low Stakes Wager", SceneType.DAYTIME, ID.EVE),
	EveZoeLowWin("Zoe Low Stakes Win", SceneType.DAYTIME, ID.EVE),
	EveZoeLowLoss("Zoe Low Stakes Loss", SceneType.DAYTIME, ID.EVE),
	EveZoeHighWager("Zoe High Stakes Wager", SceneType.DAYTIME, ID.EVE),
	EveZoeHighWin("Zoe High Stakes Win", SceneType.DAYTIME, ID.EVE),
	EveZoeHighLoss("Zoe High Stakes Loss", SceneType.DAYTIME, ID.EVE),

	EveForeplayVictory("Foreplay", SceneType.VICTORY, ID.EVE),
	EveForeplayVictoryAlt("Alternate Foreplay", SceneType.VICTORY, ID.EVE),
	EvePeggingVictory("Pegging", SceneType.VICTORY, ID.EVE),

	EveForeplayDefeat("Foreplay", SceneType.DEFEAT, ID.EVE),
	EveForeplayDefeatAlt("Foreplay Alt", SceneType.DEFEAT, ID.EVE),
	EveSexDefeat("Sex", SceneType.DEFEAT, ID.EVE),
	EveAnalDefeat("Anal", SceneType.DEFEAT, ID.EVE),

	EveForeplayDraw("Foreplay", SceneType.DRAW, ID.EVE),
	EveSexDraw("Sex", SceneType.DRAW, ID.EVE),

	EveWatch("Watch", SceneType.INTERVENTION, ID.EVE),

	//SAMANTHA
	SamanthaAfterMatch("After Match", SceneType.EVENT, ID.SAMANTHA),

	SamanthaForeplayVictory("Foreplay", SceneType.VICTORY, ID.SAMANTHA),
	SamanthaSexVictory("Sex", SceneType.VICTORY, ID.SAMANTHA),
	SamanthaPeggingVictory("Pegging", SceneType.VICTORY, ID.SAMANTHA),
	SamanthaHornyVictory("Horny", SceneType.VICTORY, ID.SAMANTHA),

	SamanthaForeplayDefeat("Foreplay", SceneType.DEFEAT, ID.SAMANTHA),
	SamanthaForeplayDefeatAlt("Foreplay Alt", SceneType.DEFEAT, ID.SAMANTHA),
	SamanthaSexDefeat("Sex", SceneType.DEFEAT, ID.SAMANTHA),
	SamanthaAnalDefeat("Anal", SceneType.DEFEAT, ID.SAMANTHA),

	SamanthaForeplayDraw("Foreplay", SceneType.DRAW, ID.SAMANTHA),
	SamanthaSexDraw("Sex", SceneType.DRAW, ID.SAMANTHA),
	SamanthaSexDraw2("Alternate Sex", SceneType.DRAW, ID.SAMANTHA),

	SamanthaWatch("Watch", SceneType.INTERVENTION, ID.SAMANTHA),
	SamanthaPoker("Strip Poker", SceneType.DAYTIME, ID.SAMANTHA),

	//VALERIE
	ValerieAfterMatch("After Match", SceneType.EVENT, ID.VALERIE),
	ValerieAfterMatchSub("After Match Submissive", SceneType.EVENT, ID.VALERIE),

	ValerieComposedForeplayVictory("Composed Foreplay", SceneType.VICTORY, ID.VALERIE),
	ValerieComposedForeplayVictoryAlt("Composed Alternate Foreplay", SceneType.VICTORY, ID.VALERIE),
	ValerieComposedSexVictory("Composed Sex", SceneType.VICTORY, ID.VALERIE),
	ValerieBrokenForeplayVictory("Broken Foreplay", SceneType.VICTORY, ID.VALERIE),
	ValerieBrokenSexVictory("Broken Sex", SceneType.VICTORY, ID.VALERIE),
	ValeriePeggingVictory("Pegging", SceneType.VICTORY, ID.VALERIE),

	ValerieComposedForeplayDefeat("Composed Foreplay", SceneType.DEFEAT, ID.VALERIE),
	ValerieComposedForeplayDefeatAlt("Composed Alternate Foreplay", SceneType.DEFEAT, ID.VALERIE),
	ValerieComposedSexDefeat("Composed Sex", SceneType.DEFEAT, ID.VALERIE),
	ValerieComposedAnalDefeat("Composed Anal", SceneType.DEFEAT, ID.VALERIE),
	ValerieBrokenForeplayDefeat("Broken Foreplay", SceneType.DEFEAT, ID.VALERIE),
	ValerieBrokenSexDefeat("Broken Sex", SceneType.DEFEAT, ID.VALERIE),
	ValerieBrokenAnalDefeat("Broken Anal", SceneType.DEFEAT, ID.VALERIE),

	ValerieComposedForeplayDraw("Composed Foreplay", SceneType.DRAW, ID.VALERIE),
	ValerieComposedSexDraw("Composed Sex", SceneType.DRAW, ID.VALERIE),
	ValerieBrokenForeplayDraw("Foreplay", SceneType.DRAW, ID.VALERIE),
	ValerieBrokenSexDraw("Sex", SceneType.DRAW, ID.VALERIE),

	ValerieWatch("Watch", SceneType.INTERVENTION, ID.VALERIE),

	//SOFIA
	SofiaAfterMatch("After Match", SceneType.EVENT, ID.SOFIA),

	SofiaForeplayVictory("Foreplay", SceneType.VICTORY, ID.SOFIA),
	SofiaSexVictory("Sex", SceneType.VICTORY, ID.SOFIA),

	SofiaForeplayDefeat("Foreplay", SceneType.DEFEAT, ID.SOFIA),
	SofiaForeplayDefeatAlt("Foreplay Alt", SceneType.DEFEAT, ID.SOFIA),
	SofiaSexDefeat("Sex", SceneType.DEFEAT, ID.SOFIA),
	SofiaAnalDefeat("Anal", SceneType.DEFEAT, ID.SOFIA),

	SofiaForeplayDraw("Foreplay", SceneType.DRAW, ID.SOFIA),
	SofiaSexDraw("Sex", SceneType.DRAW, ID.SOFIA),

	SofiaWatch("Watch", SceneType.INTERVENTION, ID.SOFIA),

	//MAYA
	MayaVictory("Victory", SceneType.VICTORY, ID.MAYA),
	MayaPeggingVictory("Pegging", SceneType.VICTORY, ID.MAYA),
	MayaPeggingVirginity("Pegging Virginity", SceneType.VICTORY, ID.MAYA),

	MayaDefeat("Defeat", SceneType.DEFEAT, ID.MAYA),
	MayaAnalDefeat("Anal Defeat", SceneType.DEFEAT, ID.MAYA),

	MayaDraw("Draw", SceneType.DRAW, ID.MAYA),

	MayaWatch("Watch", SceneType.INTERVENTION, ID.MAYA),

	//Other
	CassieMaraThreesome("Cassie Mara 3P", SceneType.DAYTIME, ID.PLAYER),
	CassieJewelThreesome("Cassie Jewel 3P", SceneType.DAYTIME, ID.PLAYER),
	CassieAngelThreesome("Cassie Angel 3P", SceneType.DAYTIME, ID.PLAYER),
	CassieYuiThreesome("Cassie Yui 3P", SceneType.DAYTIME, ID.PLAYER),
	MaraJewelThreesome("Mara Jewel 3P", SceneType.DAYTIME, ID.PLAYER),
	AngelMaraThreesome("Angel Mara 3P", SceneType.DAYTIME, ID.PLAYER),
	AngelReykaThreesome("Angel Reyka 3P", SceneType.DAYTIME, ID.PLAYER),
	AngelReykaThreesome2("Angel Reyka 2nd 3P", SceneType.DAYTIME, ID.PLAYER),
	AngelReykaThreesome2a("Angel Reyka 2nd 3P alt", SceneType.DAYTIME, ID.PLAYER),
	MaraKatThreesome("Mara Kat 3P", SceneType.DAYTIME, ID.PLAYER),
	CassieKatThreesome("Cassie Kat 3P", SceneType.DAYTIME, ID.PLAYER),
	AngelJewelThreesome("Angel Jewel 3P", SceneType.DAYTIME, ID.PLAYER);

	private final String label;
	private final SceneType type;
	private final ID star;

	SceneFlag(String label, SceneType type, ID star) {
		this.label = label;
		this.type = type;
		this.star = star;
	}

	/**
	 * Gets the name of the scene
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Gets the type of the scene
	 */
	public SceneType getType() {
		return type;
	}

	/**
	 * Gets the ID of the character that is the 'star' of the scene
	 */
	public ID getStar() {
		return star;
	}
}
