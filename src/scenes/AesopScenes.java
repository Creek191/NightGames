package scenes;

/**
 * Defines a container for Aesop's scenes
 */
public class AesopScenes extends SceneTable {
	@Override
	public Scene getVictoryScene(SceneFlag flag) {
		return null;
	}

	@Override
	public Scene getDefeatScene(SceneFlag flag) {
		return null;
	}

	@Override
	public Scene getDrawScene(SceneFlag flag) {
		return null;
	}

	@Override
	public Scene getInterventionScene(SceneFlag flag) {
		return null;
	}

	@Override
	public Scene getEventScene(SceneFlag flag) {
		return null;
	}

	@Override
	public Scene getDayScene(SceneFlag flag) {
		return null;
	}
}
