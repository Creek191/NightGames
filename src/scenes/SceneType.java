package scenes;

/**
 * Defines the types of scenes
 */
public enum SceneType {
	VICTORY,
	DEFEAT,
	DRAW,
	INTERVENTION,
	DAYTIME,
	EVENT,
	THREESOME,
}
