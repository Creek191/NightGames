package scenes;

import characters.Character;
import characters.Dummy;
import characters.Emotion;
import global.Flag;
import global.Global;
import global.Scheduler;

import java.time.LocalTime;
import java.util.HashMap;

/**
 * Defines events involving Mara
 */
public class MaraEvent implements Event {
	private final Character player;
	private int scene;

	public MaraEvent(Character player) {
		this.player = player;
		scene = 1;
	}

	@Override
	public void respond(String response) {
		if (response.equalsIgnoreCase("Next")) {
			scene++;
			play("");
		}
		else if (response.equalsIgnoreCase("Leave")) {
			Scheduler.setTime(LocalTime.of(15, 0));
			Global.gui().clearText();
			Scheduler.getDay().plan();
		}
	}

	@Override
	public boolean play(String response) {
		Global.current = this;
		Global.gui().clearText();
		Global.gui().clearCommand();
		var mara = new Dummy("Mara");
		var cassie = new Dummy("Cassie");
		switch (scene) {
			case 1:
				mara.undress();
				mara.setBlush(1);
				Global.gui().loadPortrait(player, mara);
				SceneManager.play(SceneFlag.MaraSickDayMorning);
				Global.gui().choose("Next");
				break;
			case 2:
				mara.undress();
				mara.setBlush(3);
				mara.setMood(Emotion.desperate);
				Global.gui().loadPortrait(player, mara);
				SceneManager.play(SceneFlag.MaraSickDayShower);
				Global.gui().choose("Next");
				break;
			case 3:
				mara.undress();
				mara.setBlush(1);
				mara.setMood(Emotion.angry);
				Global.gui().loadPortrait(player, mara);
				SceneManager.play(SceneFlag.MaraSickDayInterrogation);
				Global.gui().choose("Next");
				break;
			case 4:
				mara.undress();
				mara.setBlush(2);
				mara.setMood(Emotion.horny);
				cassie.undress();
				cassie.setBlush(3);
				cassie.setMood(Emotion.desperate);
				Global.gui().loadTwoPortraits(mara, cassie);
				SceneManager.play(SceneFlag.MaraSickDayLunch);
				Global.gui().choose("Next");
				break;
			case 5:
				mara.undress();
				mara.setBlush(1);
				mara.setMood(Emotion.confident);
				cassie.undress();
				cassie.setBlush(1);
				cassie.setMood(Emotion.confident);
				Global.gui().loadTwoPortraits(cassie, mara);
				SceneManager.play(SceneFlag.MaraSickDayFinale);
				Global.gui().choose("Leave");
				Global.flag(Flag.MaraTemporal);
		}
		return true;
	}

	@Override
	public String morning() {
		if (Global.checkFlag(Flag.MaraDayOff) && !Global.checkFlag(Flag.MaraTemporal)) {
			return "DayOff";
		}
		else {
			return "";
		}
	}

	@Override
	public String mandatory() {
		return "";
	}

	@Override
	public void addAvailable(HashMap<String, Integer> available) {
	}
}
