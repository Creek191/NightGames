package scenes;

import characters.Character;
import characters.ID;
import global.Global;
import global.Roster;

import java.util.HashMap;

/**
 * Defines the scene manager used to play a scene
 */
public class SceneManager {
	public static SceneManager instance;
	private static HashMap<ID, SceneTable> sceneDb;

	public SceneManager() {
		sceneDb = new HashMap<>();
		sceneDb.put(ID.CASSIE, new CassieScenes());
		sceneDb.put(ID.MARA, new MaraScenes());
		sceneDb.put(ID.ANGEL, new AngelScenes());
		sceneDb.put(ID.JEWEL, new JewelScenes());
		sceneDb.put(ID.YUI, new YuiScenes());
		sceneDb.put(ID.KAT, new KatScenes());
		sceneDb.put(ID.REYKA, new ReykaScenes());
		sceneDb.put(ID.EVE, new EveScenes());
		sceneDb.put(ID.VALERIE, new ValerieScenes());
		sceneDb.put(ID.SAMANTHA, new SamanthaScenes());
		sceneDb.put(ID.SOFIA, new SofiaScenes());
		sceneDb.put(ID.SELENE, new SeleneScenes());
		sceneDb.put(ID.PLAYER, new MiscScenes());
		sceneDb.put(ID.MAYA, new MayaScenes());
		sceneDb.put(ID.AESOP, new AesopScenes());
	}

	private static SceneManager getInstance() {
		if (instance == null) {
			instance = new SceneManager();
		}
		return instance;
	}

	/**
	 * Plays the specified scene
	 *
	 * @param scene The flag of the scene to play
	 */
	public static void play(SceneFlag scene) {
		play(scene, null);
	}

	/**
	 * Plays the specified scene
	 *
	 * @param scene The flag of the scene to play
	 * @param npc   The character that takes part in the scene, used to fill placeholders
	 */
	public static void play(SceneFlag scene, Character npc) {
		SceneManager.getInstance().playScene(scene, npc);
	}

	/**
	 * Plays the specified scene
	 *
	 * @param scene The flag of the scene to play
	 * @param npc   The character that takes part in the scene, used to fill placeholders
	 */
	public void playScene(SceneFlag scene, Character npc) {
		if (sceneDb.containsKey(scene.getStar())) {
			var playing = sceneDb.get(scene.getStar()).getScene(scene);
			playing.parse(Roster.get(ID.PLAYER), npc);
			playing.play(0);
		}
		else {
			Global.gui().message("Error: scene table not found for ID: " + scene.getStar());
		}
		Global.watched(scene);
	}

	/**
	 * Gets the total number of scenes matching the specified filters
	 *
	 * @param star The ID of the character that is the "star" of the scene
	 * @param type The type of the scene
	 */
	public static int getTotalCount(ID star, SceneType type) {
		var count = 0;
		for (var scene : SceneFlag.values()) {
			if (scene.getStar() == star && scene.getType() == type) {
				count++;
			}
		}
		return count;
	}
}
