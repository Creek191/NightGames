package stance;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;

/**
 * Defines a position where one person is pinning the other down while grinding into their crotch
 */
public class PleasurePin extends Position {

	public PleasurePin(Character top, Character bottom) {
		super(top, bottom, Stance.pin);
		strength = top.getEffective(Attribute.Ki) * 5;
		pleasure = true;
	}

	@Override
	public String describe() {
		if (top.human()) {
			return "You're sitting on " + bottom.name() + ", pressing your leg into her crotch.";
		}
		else {
			return top.name() + " is pinning you down, grinding her knee into your groin.";
		}
	}

	@Override
	public boolean mobile(Character c) {
		return c == top;
	}

	@Override
	public boolean kiss(Character c) {
		return c == top;
	}

	@Override
	public boolean dom(Character c) {
		return c == top;
	}

	@Override
	public boolean sub(Character c) {
		return c == bottom;
	}

	@Override
	public boolean reachTop(Character c) {
		return c == top;
	}

	@Override
	public boolean reachBottom(Character c) {
		return c == top;
	}

	@Override
	public boolean prone(Character c) {
		return c == bottom;
	}

	@Override
	public boolean feet(Character c) {
		return false;
	}

	@Override
	public boolean oral(Character c) {
		return false;
	}

	@Override
	public boolean behind(Character c) {
		return false;
	}

	@Override
	public boolean penetration(Character c) {
		return false;
	}

	@Override
	public Position insert(Character c) {
		if (top.hasDick() && bottom.hasPussy()) {
			return new Missionary(top, bottom);
		}
		else if (top.hasPussy() && bottom.hasDick()) {
			return new Cowgirl(top, bottom);
		}
		else {
			return this;
		}
	}

	@Override
	public void executeOngoing(Combat c) {
		if (bottom.human()) {
			c.write(top, top.name() + "'s leg continues grinding against your genitals.");
		}
		else if (top.human()) {
			c.write(top, "You can feel " + bottom.name() + "'s arousal on your leg.");
		}
		bottom.pleasure(5, Anatomy.genitals, Result.foreplay, c);
	}
}
