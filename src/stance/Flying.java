package stance;

import characters.Anatomy;
import characters.Character;
import characters.ID;
import characters.Trait;
import combat.Combat;
import global.Flag;
import global.Global;

/**
 * Defines a position where one person is fucking the other while flying through the air
 */
public class Flying extends Position {

	public Flying(Character succ, Character target) {
		super(succ, target, Stance.flying);
		strength = 65;
	}

	@Override
	public String describe() {
		return "You are flying some twenty feet up in the air,"
				+ " joined to your partner by your hips.";
	}

	@Override
	public boolean mobile(Character c) {
		return top.equals(c);
	}

	@Override
	public boolean kiss(Character c) {
		return true;
	}

	@Override
	public boolean dom(Character c) {
		return top.equals(c);
	}

	@Override
	public boolean sub(Character c) {
		return !top.equals(c);
	}

	@Override
	public boolean reachTop(Character c) {
		return true;
	}

	@Override
	public boolean reachBottom(Character c) {
		return top.equals(c);
	}

	@Override
	public boolean prone(Character c) {
		return !top.equals(c);
	}

	@Override
	public boolean feet(Character c) {
		return false;
	}

	@Override
	public boolean oral(Character c) {
		return false;
	}

	@Override
	public boolean behind(Character c) {
		return false;
	}

	@Override
	public boolean penetration(Character c) {
		return true;
	}

	public boolean flying(Character c) {
		return true;
	}

	@Override
	public void executeOngoing(Combat c) {
		if (top.human()) {
			c.write("The strain of flying while carrying another person saps a bit of your stamina.");
		}
		top.weaken(3, c);
		if (top.getStamina().get() < 5) {
			if (top.human()) {
				c.write("You're too tired to stay in the air. You plummet to the ground and " + bottom.name() + " drops on you heavily, knocking the wind out of you.");
			}
			else {
				c.write(top.name() + " falls to the ground and so do you. Fortunately, her body cushions your fall, but you're not sure she appreciates that as much as you do.");
			}
			top.pain(5, Anatomy.chest, c);
			c.stance = new Mount(bottom, top);
		}
		else {
			var m = 6 + (2 * pace);
			var r = Math.max(1, 3 - pace);
			if (top.has(Trait.experienced)) {
				r = r * 2;
			}
			if (pace > 1) {
				if (top.human()) {
					c.write(top, "Your intense fucking continues to drive you both closer to ecstasy.");
				}
				else {
					c.write(top, "Her rapid bouncing on your cock gives you intense pleasure.");
				}
			}
			else if (pace == 1) {
				if (top.human()) {
					c.write(top, "Your steady thrusting pleasures you both.");
				}
				else {
					c.write(top, "Her steady lovemaking continues to erode your resistance.");
				}
			}
			else {
				if (top.human()) {
					c.write(top, "You slowly, but steadily grind against her.");
				}
				else {
					c.write(top, "She continues to stimulate your penis with slow, deliberate movements.");
				}
			}
			bottom.pleasure(top.bonusProficiency(Anatomy.genitals, m), Anatomy.genitals, c);
			top.pleasure(bottom.bonusProficiency(Anatomy.genitals, m / r), Anatomy.genitals, c);
			if (!Global.checkFlag(Flag.exactimages) || top.id() == ID.SELENE) {
				c.offerImage("Selene Blowjob.jpg", "Art by AimlessArt");
			}
		}
	}

	@Override
	public Position insert(Character c) {
		c.weaken(10);
		return new StandingOver(top, bottom);
	}
}
