package stance;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;

/**
 * Defines a position where one person is pinning the other down while putting pressure on their genitals
 */
public class SubmissionPin extends Position {

	public SubmissionPin(Character top, Character bottom) {
		super(top, bottom, Stance.reversepin);
		strength = top.getEffective(Attribute.Ki) * 5;
		submission = true;
	}

	@Override
	public String describe() {
		if (top.human()) {
			if (bottom.hasBalls()) {
				return "You're sitting on " + bottom.name() + "'s upper body, putting pressure on " + bottom.possessive(
						false) + " testicles.";
			}
			else {
				return "You're sitting on " + bottom.name() + "'s upper body, putting pressure on " + bottom.possessive(
						false) + " sensitive genitals.";
			}
		}
		else {
			return top.name() + " is sitting on your chest, holding you in place by your family jewels.";
		}
	}

	@Override
	public boolean mobile(Character c) {
		return c == top;
	}

	@Override
	public boolean kiss(Character c) {
		return false;
	}

	@Override
	public boolean dom(Character c) {
		return c == top;
	}

	@Override
	public boolean sub(Character c) {
		return c == bottom;
	}

	@Override
	public boolean reachTop(Character c) {
		return false;
	}

	@Override
	public boolean reachBottom(Character c) {
		return c == top;
	}

	@Override
	public boolean prone(Character c) {
		return c == bottom;
	}

	@Override
	public boolean feet(Character c) {
		return false;
	}

	@Override
	public boolean oral(Character c) {
		return c == top;
	}

	@Override
	public boolean behind(Character c) {
		return false;
	}

	@Override
	public boolean penetration(Character c) {
		return false;
	}

	@Override
	public Position insert(Character c) {
		if (top.hasDick() && bottom.hasPussy()) {
			return new Missionary(top, bottom);
		}
		else if (top.hasPussy() && bottom.hasDick()) {
			return new ReverseCowgirl(top, bottom);
		}
		else {
			return this;
		}
	}

	@Override
	public void executeOngoing(Combat c) {
		if (bottom.human()) {
			c.write(top, top.name() + " continues to put painful pressure on your genitals.");
		}
		else if (top.human()) {
			c.write(top, bottom.name() + " seems gradually worn down by your submission hold.");
		}
		bottom.pain(5, Anatomy.genitals, c);
	}
}
