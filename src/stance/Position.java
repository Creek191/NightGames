package stance;

import characters.Anatomy;
import characters.Character;
import combat.Combat;

import java.io.Serializable;

/**
 * Base implementation for stances
 */
public abstract class Position implements Serializable, Cloneable {
	public Character top;
	public Character bottom;
	public int time;
	public Stance en;
	protected int pace;
	public int strength;
	protected boolean contact;
	protected boolean submission;
	protected boolean pleasure;

	public Position(Character top, Character bottom, Stance stance) {
		this.top = top;
		this.bottom = bottom;
		this.en = stance;
		time = 0;
		pace = 0;
		strength = 0;
		contact = true;
		submission = false;
		pleasure = false;
	}

	/**
	 * Decays the stance, making it easier to escape with every turn
	 */
	public void decay() {
		time++;
	}

	/**
	 * Executes any ongoing per-turn effects caused by the stance, such as arousal from sex
	 *
	 * @param c A reference to the ongoing combat
	 */
	public void executeOngoing(Combat c) {
	}

	/**
	 * Checks whether the position involves anal sex
	 */
	public boolean anal() {
		return this.en == Stance.anal || this.en == Stance.analm;
	}

	/**
	 * Checks whether the opponents are in contact with each other in this position
	 */
	public boolean inContact() {
		return contact;
	}

	/**
	 * Gets a description of the current stance from the player's perspective
	 */
	public abstract String describe();

	/**
	 * Checks whether the specified character is free to use skills requiring moving around
	 *
	 * @param c The character to check
	 */
	public abstract boolean mobile(Character c);
	/**
	 * Checks whether the specified character can use kissing moves in this position
	 *
	 * @param c The character to check
	 */
	public abstract boolean kiss(Character c);

	/**
	 * Checks whether the specified character is in the dominant position
	 *
	 * @param c The character to check
	 */
	public abstract boolean dom(Character c);

	/**
	 * Checks whether the specified character is in the submissive position
	 *
	 * @param c The character to check
	 */
	public abstract boolean sub(Character c);
	/**
	 * Checks whether the specified character can reach the opponent's top half in this position
	 *
	 * @param c The character to check
	 */
	public abstract boolean reachTop(Character c);

	/**
	 * Checks whether the specified character can reach the opponent's bottom half in this position
	 *
	 * @param c The character to check
	 */
	public abstract boolean reachBottom(Character c);

	/**
	 * Checks whether the specified character is prone in this position
	 *
	 * @param c The character to check
	 */
	public abstract boolean prone(Character c);

	/**
	 * Checks whether the specified character can use their feet freely in this position
	 *
	 * @param c The character to check
	 */
	public abstract boolean feet(Character c);

	/**
	 * Checks whether the specified character can use oral skills in this position
	 *
	 * @param c The character to check
	 */
	public abstract boolean oral(Character c);

	/**
	 * Checks whether the specified character can reach the opponent's ass in this position
	 *
	 * @param c The character to check
	 */
	public abstract boolean behind(Character c);

	/**
	 * Checks whether the specified character is penetrating the opponent in this position
	 *
	 * @param c The character to check
	 */
	public abstract boolean penetration(Character c);

	/**
	 * Switches between being inserted/not inserted, while remaining in a similar position
	 *
	 * @param c The character making the change
	 * @return The new position the characters are in
	 */
	public abstract Position insert(Character c);

	/**
	 * Gets the stance of the position
	 */
	public Stance enumerate() {
		return this.en;
	}

	/**
	 * Sets the pace of the position (e.g. how fast the characters fuck)
	 *
	 * @param speed The pace
	 */
	public void setPace(int speed) {
		this.pace = speed;
	}

	/**
	 * Clones the position object
	 */
	public Position clone() throws CloneNotSupportedException {
		return (Position) super.clone();
	}

	/**
	 * Applies pleasure or pain from struggling to the receiving opponent
	 *
	 * @param c A reference to the ongoing combat
	 */
	public void struggle(Combat c) {
		if (submission) {
			if (bottom.human()) {
				c.write(top, "Your struggles are met with an even tighter grip on your balls.");
			}
			else if (top.human()) {
				c.write(top,
						"You apply more pressure to " + bottom.name() + "'s genitals to stop her from struggling.");
			}
			bottom.pain(5, Anatomy.genitals, c);
		}
		if (pleasure) {
			if (bottom.human()) {
				c.write(top, "The friction from struggling stimulates your penis even more.");
			}
			else if (top.human()) {
				c.write(top, bottom.name() + " grinds against your leg, trying to free herself.");
			}
			bottom.pleasure(5, Anatomy.genitals, c);
		}
	}

	/**
	 * Gets the DC required by the submissive character for escaping from the position
	 *
	 * @param dom The dominant character
	 * @param sub The submissive character
	 */
	public int escapeDC(Character dom, Character sub) {
		return dom.bonusPin(strength) - ((5 * time) + sub.escapeModifier());
	}
}
