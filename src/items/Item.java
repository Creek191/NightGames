package items;

import characters.Character;

import java.util.ArrayList;

/**
 * Defines an item that can be used by a character
 */
public interface Item extends Loot {

	/**
	 * Gets a description of the item
	 */
	String getDesc();

	/**
	 * Gets a full description of the item, including any modifications the owner has made
	 *
	 * @param owner The character owning the item
	 */
	String getFullDesc(Character owner);

	/**
	 * Gets the full name of the item, including any modifications the owner has made
	 *
	 * @param owner The character owning the item
	 */
	String getFullName(Character owner);

	/**
	 * Gets the recipe required to craft the item
	 */
	ArrayList<Item> getRecipe();

	/**
	 * Checks whether the item should be listed in the inventory
	 */
	Boolean listed();
}
