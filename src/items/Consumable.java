package items;

import characters.Character;

import java.util.ArrayList;

/**
 * Defines an item that can be used to produce an effect, either in combat or during a match in general
 */
public enum Consumable implements Item {
	ZipTie("Heavy Zip Tie", 5, "A thick heavy tie suitable for binding someone's hands", "a "),
	Handcuffs("Handcuffs", 200, "Strong steel restraints, hard to escape from", ""),
	Talisman("Dark Talisman", 100, "This unholy trinket can cloud a target's mind", "a "),
	FaeScroll("Summoning Scroll",
			150,
			"A magic scroll imbued with summoning magic. It can briefly support a large number of faeries",
			"a "),
	needle("Drugged Needle", 10, "A small throwing needle coated in a potent aphrodisiac", "a "),
	smoke("Smoke Bomb", 20, "This harmless bomb provides cover for a quick escape", "a "),
	powerband("Power Band",
			250,
			"This headband is a catalyst of great power, temporarily unleashing the wearer's maximum Ki",
			"a "),

	;

	private final String desc;
	private final String name;
	private final String prefix;
	private final int price;

	@Override
	public String getDesc() {
		return desc;
	}

	@Override
	public String getFullDesc(Character owner) {
		return getDesc();
	}

	@Override
	public int getPrice() {
		return price;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getFullName(Character owner) {
		return getName();
	}

	@Override
	public String pre() {
		return prefix;
	}

	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}

	@Override
	public Boolean listed() {
		return true;
	}

	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<>();
	}

	Consumable(String name, int price, String desc, String prefix) {
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
	}
}
