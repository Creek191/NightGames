package items;

import characters.Character;
import status.*;

import java.util.ArrayList;

/**
 * Defines flasks with different contents used to apply a status to an opponent
 */
public enum Flask implements Item {
	Lubricant("Lubricant",
			20,
			"Helps you pleasure your opponent, but makes her hard to hang on to",
			"some ",
			"oily",
			new Oiled(null),
			"The liquid clings to you and makes your whole body slippery.",
			"She's covered in slick lubricant."),
	Aphrodisiac("Aphrodisiac",
			40,
			"Can be thrown like a 'horny bomb'",
			"an ",
			"sweet-smelling",
			new Horny(null, 6, 3),
			"An unnatural warmth spreads through your body and gathers in your dick like a fire.",
			"For a second, she's just surprised, but gradually a growing desire starts to make her weak in the knees"),
	Sedative("Sedative",
			25,
			"Tires out your opponent, but can also make her numb",
			"a ",
			"milky",
			new Drowsy(null, 3),
			"The fog seems to fill your head and your body feels heavy.",
			"She stumbles for a moment, trying to clear the drowsiness from her head."),
	SPotion("Sensitivity Flask",
			25,
			"Who knows what's in this stuff, but it makes any skin it touches tingle",
			"a ",
			"fizzy",
			new Hypersensitive(null),
			"Your skin becomes hot, but goosebumps appear anyway. Even the air touching your skin makes you shiver.",
			"She shivers as it takes hold and heightens her sense of touch."),
	DisSol("Dissolving Solution",
			30,
			"Destroys clothes, but completely non-toxic",
			"a ",
			"clear",
			new Dissolving(null),
			"Your clothes start to disintegrate where ever the liquid lands.",
			"Her clothes start to vanish, exposing more of her soft skin."),
	PAphrodisiac("Potent Aphrodisiac",
			400,
			"More like a 'horny atomic bomb'",
			"a ",
			"intoxicating",
			new Horny(null, 12, 3),
			"You feel momentarily light-headed as all the blood in your body rushes to your penis.",
			"She flushes deeply and instinctively reaches down to touch herself. She manages to restrain herself, but just barely."),

	;

	private final String desc;
	private final String name;
	private final String prefix;
	private final int price;
	private final String color;
	private final Status effect;
	private final String dealText;
	private final String receiveText;

	@Override
	public String getDesc() {
		return desc;
	}

	@Override
	public String getFullDesc(Character owner) {
		return getDesc();
	}

	@Override
	public int getPrice() {
		return price;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getFullName(Character owner) {
		return getName();
	}

	@Override
	public String pre() {
		return prefix;
	}

	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}

	/**
	 * Gets the color of the flasks contents
	 */
	public String getColor() {
		return color;
	}

	/**
	 * Gets the status applied by using the flask
	 */
	public Status effect() {
		return effect;
	}

	/**
	 * Gets a description of the effects the flask had
	 *
	 * @param affected The character affected by the flask
	 */
	public String getText(Character affected) {
		if (affected.human()) {
			return receiveText;
		}
		else {
			return dealText;
		}
	}

	/**
	 * Checks whether the flask can be used on the target
	 *
	 * @param target The character to use the flask on
	 */
	public boolean canUse(Character target) {
		switch (this) {
			case Lubricant:
				return target.nude() && !target.is(Stsflag.oiled);
			case SPotion:
				return target.nude() && !target.is(Stsflag.hypersensitive);
			case DisSol:
				return !target.nude();
			default:
				return true;
		}
	}

	@Override
	public Boolean listed() {
		return true;
	}

	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<>();
	}

	Flask(String name,
			int price,
			String desc,
			String prefix,
			String color,
			Status effect,
			String receiveText,
			String dealText) {
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
		this.color = color;
		this.effect = effect;
		this.dealText = dealText;
		this.receiveText = receiveText;
	}
}
