package items;

import characters.Character;

import java.util.ArrayList;

/**
 * Defines the trophies that can be taken from an opponent after defeating them in combat
 */
public enum Trophy implements Item {
	CassieTrophy("Cassie's Panties", "Cute and simple panties"),
	MaraTrophy("Mara's Underwear", "She wears boys underwear?"),
	AngelTrophy("Angel's Thong", "There's barely anything here"),
	JewelTrophy("Jewel's Panties", "Surprisingly lacy"),
	ReykaTrophy("Reyka's Clit Ring", "What else can you take from someone who goes commando?"),
	PlayerTrophy("Your Boxers", "How did you end up with these?"),
	EveTrophy("Eve's 'Panties'", "Crotchless and of no practical use"),
	KatTrophy("Kat's Panties", "Cute pink panties. Unfortunately there's no cat motif"),
	YuiTrophy("Yui's Panties", "White and innocent, not unlike their owner"),
	MayaTrophy("Maya's Panties", "Black lace. Very sexy"),
	SamanthaTrophy("Samantha's Thong", "A lacy red thong, translucent in all but the most delicate areas."),
	ValerieTrophy("Valerie's Panties", "Silky and high class."),
	SofiaTrophy("Sofia's Panties", "These otherwise plain white panties are unexpectedly narrow wear it matters.");

	private final String desc;
	private final String name;

	@Override
	public String getDesc() {
		return desc;
	}

	@Override
	public String getFullDesc(Character owner) {
		return getDesc();
	}

	@Override
	public int getPrice() {
		return 0;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getFullName(Character owner) {
		return getName();
	}

	public String pre() {
		return "";
	}

	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}

	@Override
	public Boolean listed() {
		return true;
	}

	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<>();
	}

	Trophy(String name, String desc) {
		this.name = name;
		this.desc = desc;
	}
}
