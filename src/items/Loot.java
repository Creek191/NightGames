package items;

import characters.Character;

/**
 * Defines a piece of loot that can be picked up
 */
public interface Loot {
	/**
	 * Gets the loot's name
	 */
	String getName();

	/**
	 * Gets the loot's prefix, for use in a sentence
	 */
	String pre();

	/**
	 * Gets the loot's price
	 */
	int getPrice();

	/**
	 * Picks up the loot, adding it to the specified character
	 *
	 * @param owner The character picking up the loot
	 */
	void pickup(Character owner);
}
