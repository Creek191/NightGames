package items;

/**
 * Defines types of clothing
 */
public enum ClothingType {
	TOPUNDER,
	TOP,
	TOPOUTER,
	UNDERWEAR,
	BOTOUTER,
	SPECIAL
}

