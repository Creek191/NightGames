package items;

import characters.Character;

import java.util.ArrayList;

/**
 * Defines toys that characters can use on their opponent in combat, or during a match
 */
public enum Toy implements Item {
	Dildo("Dildo", 250, "Big rubber cock: not a weapon", "a "),
	Crop("Riding Crop", 200, "Delivers a painful sting to instill discipline", "a "),
	Onahole("Onahole", 300, "An artificial vagina, but you can probably find some real ones pretty easily", "an "),
	Tickler("Tickler", 300, "Tickles and pleasures your opponent's sensitive areas", "a "),
	Strapon("Strap-on Dildo", 600, "Penis envy much?", "a "),
	ShockGlove("Shock Glove", 800, "Delivers a safe, but painful electric shock", "a "),
	Aersolizer("Aerosolizer", 500, "Turns a liquid into an unavoidable cloud of mist", "an "),
	Dildo2("Sonic Dildo", 2000, "Apparently vibrates at the ideal frequency to produce pleasure", "a "),
	Crop2("Hunting Crop", 1500, "Equipped with the fearsome Treasure Hunter attachment", "a "),
	Crop3("Mistress Crop", 10000, "An exquisitely crafted riding crop that looks as expensive as it is painful", "a "),
	Onahole2("Wet Onahole", 3000, "As hot and wet as the real thing", "an "),
	Tickler2("Enhanced Tickler", 3000, "Coated with a substance that can increase sensitivity", "an "),
	Strapon2("Flex-O-Peg", 2500, "A more flexible and versatile strapon with a built in vibrator", "the patented "),
	Excalibur("Sexcalibur", 1000, "A highly customizable toy ", ""),
	Paddle("Paddle", 1000, "", "a "),
	AnalBeads("Anal Beads", 1000, "", "a "),
	nippleclamp("Nipple Clamps", 1000, "", ""),
	bloodhound("Bloodhound", 2000, "An app that gives you your opponents' positions at set intervals", "");

	private final String desc;
	private final String name;
	private final String prefix;
	private final int price;

	@Override
	public String getDesc() {
		return desc;
	}

	@Override
	public String getFullDesc(Character owner) {
		var result = new StringBuilder(getDesc());
		for (var upgrade : getRenames()) {
			if (owner.has(upgrade)) {
				result = new StringBuilder(upgrade.getDesc());
				break;
			}
		}

		for (var addon : getAddons()) {
			if (owner.has(addon) && !addon.getDesc().equals("")) {
				result.append(". ").append(addon.getDesc());
			}
		}
		return result.toString();
	}

	@Override
	public int getPrice() {
		return price;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getFullName(Character owner) {
		for (var upgrade : getRenames()) {
			if (owner.has(upgrade)) {
				return upgrade.getFullName(owner);
			}
		}
		return getName();
	}

	/**
	 * Gets a list of attachments that change the toy's base name
	 */
	private ArrayList<Attachment> getRenames() {
		var renames = new ArrayList<Attachment>();
		switch (this) {
			case Excalibur:
				renames.add(Attachment.Excalibur4);
				renames.add(Attachment.Excalibur3);
				renames.add(Attachment.Excalibur2);
				break;
			case Dildo:
			case Dildo2:
				renames.add(Attachment.DildoSlimy);
				break;
			case Onahole:
			case Onahole2:
				renames.add(Attachment.OnaholeSlimy);
				break;
			case Crop:
			case Crop2:
				renames.add(Attachment.CropShocker);
				break;
			case Tickler:
			case Tickler2:
				renames.add(Attachment.TicklerFluffy);
				break;
		}
		return renames;
	}

	/**
	 * Gets a list of attachments that apply modifiers to the toy's name
	 */
	private ArrayList<Attachment> getAddons() {
		var addons = new ArrayList<Attachment>();
		switch (this) {
			case Excalibur:
				addons.add(Attachment.ExcaliburArcane);
				addons.add(Attachment.ExcaliburScience);
				addons.add(Attachment.ExcaliburKi);
				addons.add(Attachment.ExcaliburDark);
				addons.add(Attachment.ExcaliburFetish);
				addons.add(Attachment.ExcaliburAnimism);
				addons.add(Attachment.ExcaliburNinjutsu);
				break;
			case Dildo:
			case Dildo2:
				addons.add(Attachment.DildoLube);
				break;
			case Onahole:
			case Onahole2:
				addons.add(Attachment.OnaholeVibe);
				break;
			case Crop:
			case Crop2:
				addons.add(Attachment.CropKeen);
				break;
			case Tickler:
			case Tickler2:
				addons.add(Attachment.TicklerPheromones);
				break;
		}
		return addons;
	}

	@Override
	public String pre() {
		return prefix;
	}

	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}

	@Override
	public Boolean listed() {
		return true;
	}

	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<Item>();
	}

	Toy(String name, int price, String desc, String prefix) {
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
	}
}
