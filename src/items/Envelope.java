package items;

import characters.Character;
import global.Challenge;

import java.util.ArrayList;

/**
 * Defines an envelope containing a challenge that was picked up during a match
 */
public class Envelope implements Item {
	private final Challenge goal;

	public Envelope(Challenge goal) {
		this.goal = goal;
	}

	@Override
	public String getDesc() {
		return goal.message();
	}

	@Override
	public String getFullDesc(Character owner) {
		return getDesc();
	}

	@Override
	public int getPrice() {
		return 0;
	}

	@Override
	public String getName() {
		return "Challenge: " + goal.summary();
	}

	@Override
	public String getFullName(Character owner) {
		return getName();
	}

	@Override
	public String pre() {
		return "a ";
	}

	@Override
	public Boolean listed() {
		return true;
	}

	@Override
	public ArrayList<Item> getRecipe() {
		return new ArrayList<>();
	}

	@Override
	public void pickup(Character owner) {
	}
}
