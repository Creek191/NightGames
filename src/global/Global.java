package global;

import actions.*;
import characters.Character;
import characters.*;
import gui.GUI;
import items.*;
import pet.Ptype;
import scenes.Event;
import scenes.SceneFlag;
import skills.*;
import trap.*;

import java.util.*;

/**
 * Defines a container for globally accessible data
 */
public class Global {
	private static Random rng;
	private static GUI gui;
	private static HashSet<Skill> skillPool;
	private static ArrayList<Action> actionPool;
	private static ArrayList<Trap> trapPool;
	private static HashSet<Trait> featPool;
	private static HashSet<Flag> flags;
	private static HashSet<SceneFlag> watched;
	private static HashMap<Flag, Float> counters;
	private static HashMap<Attribute, HashMap<Integer, String>> skillList;
	private static Player human;
	public static Event current;
	public static boolean debug = false;

	public Global(GUI gui) {
		rng = new Random();
		flags = new HashSet<>();
		counters = new HashMap<>();
		watched = new HashSet<>();
		current = null;
		setDefaultOptions();
		Global.gui = gui;
		Global.gui.BuildGUI();
		buildActionPool();
		buildFeatPool();
		buildSkillList();
	}

	/**
	 * Starts a new game
	 *
	 * @param one      A reference to the character controlled by the player
	 * @param tutorial Whether the tutorial should be played
	 */
	public static void newGame(Player one, boolean tutorial) {
		human = one;
		gui.populatePlayer(human);
		buildSkillPool(human);
		Global.gainSkills(human, false);
		Roster.reset(human);
		Scheduler.reset();
		flag(Flag.exactimages);
		if (tutorial) {
			new Tutorial(human).play("");
		}
		else {
			Scheduler.dusk();
		}
	}

	/**
	 * Picks a random number between 0 (inclusive) and d (exclusive)
	 *
	 * @param d The upper range of the numbers returned
	 */
	public static int random(int d) {
		if (d <= 0) {
			return 0;
		}
		return rng.nextInt(d);
	}

	/**
	 * Gets a reference to the GUI
	 */
	public static GUI gui() {
		return gui;
	}

	/**
	 * Gets a reference to the human player
	 */
	public static Player getPlayer() {
		return human;
	}

	/**
	 * Sets the human player reference
	 *
	 * @param player The player
	 */
	public static void setPlayer(Character player) {
		human = (Player) player;
	}

	/**
	 * Builds the pool of all usable skills
	 *
	 * @param p A character reference
	 */
	public static void buildSkillPool(Character p) {
		skillPool = new HashSet<>();

		//Seduction
		skillPool.add(new Kiss(p));
		skillPool.add(new FondleBreasts(p));
		skillPool.add(new Fuck(p));
		skillPool.add(new Tickle(p));
		skillPool.add(new Blowjob(p));
		skillPool.add(new Cunnilingus(p));
		skillPool.add(new LickNipples(p));
		skillPool.add(new Paizuri(p));
		skillPool.add(new SuckNeck(p));
		skillPool.add(new Whisper(p));
		skillPool.add(new Footjob(p));
		skillPool.add(new Handjob(p));
		skillPool.add(new Finger(p));
		skillPool.add(new Piston(p));
		skillPool.add(new Grind(p));
		skillPool.add(new Thrust(p));
		skillPool.add(new Carry(p));
		skillPool.add(new Tighten(p));
		skillPool.add(new AssFuck(p));
		skillPool.add(new Frottage(p));
		skillPool.add(new FingerAss(p));

		//Cunning
		skillPool.add(new StripTop(p));
		skillPool.add(new StripBottom(p));
		skillPool.add(new Escape(p));
		skillPool.add(new Maneuver(p));
		skillPool.add(new Reversal(p));
		skillPool.add(new Taunt(p));
		skillPool.add(new Trip(p));
		skillPool.add(new Turnover(p));

		//Power
		skillPool.add(new Shove(p));
		skillPool.add(new Slap(p));
		skillPool.add(new ArmBar(p));
		skillPool.add(new Flick(p));
		skillPool.add(new Knee(p));
		skillPool.add(new LegLock(p));
		skillPool.add(new Restrain(p));
		skillPool.add(new Spank(p));
		skillPool.add(new Stomp(p));
		skillPool.add(new Tackle(p));
		skillPool.add(new Kick(p));
		skillPool.add(new Squeeze(p));
		skillPool.add(new Nurple(p));
		skillPool.add(new Tear(p));

		//Arcane
		skillPool.add(new MagicMissile(p));
		skillPool.add(new Binding(p));
		skillPool.add(new Illusions(p));
		skillPool.add(new NakedBloom(p));
		skillPool.add(new Barrier(p));
		skillPool.add(new MageArmor(p));
		skillPool.add(new ManaFortification(p));

		//Dark
		skillPool.add(new LustAura(p));
		skillPool.add(new TailPeg(p));
		skillPool.add(new Dominate(p));
		skillPool.add(new DarkTendrils(p));
		skillPool.add(new Sacrifice(p));
		skillPool.add(new Fly(p));
		skillPool.add(new Drain(p));
		skillPool.add(new LustOverflow(p));

		//Ki
		skillPool.add(new WaterForm(p));
		skillPool.add(new FlashStep(p));
		skillPool.add(new FlyCatcher(p));
		skillPool.add(new StoneForm(p));
		skillPool.add(new FireForm(p));
		skillPool.add(new IceForm(p));
		skillPool.add(new Burst(p));
		skillPool.add(new PleasureBomb(p));

		//Science
		skillPool.add(new StunBlast(p));
		skillPool.add(new ShrinkRay(p));
		skillPool.add(new ShortCircuit(p));
		skillPool.add(new Defabricator(p));
		skillPool.add(new Fabricator(p));
		skillPool.add(new MatterConverter(p));

		//Fetish
		skillPool.add(new Masochism(p));
		skillPool.add(new Bondage(p));
		skillPool.add(new TentaclePorn(p));
		skillPool.add(new FaceFuck(p));
		skillPool.add(new BondageStraps(p));
		skillPool.add(new TortoiseWrap(p));
		skillPool.add(new Nymphomania(p));

		//Animism
		skillPool.add(new CatsGrace(p));
		skillPool.add(new TailJob(p));
		skillPool.add(new Purr(p));
		skillPool.add(new PheromoneOverdrive(p));
		skillPool.add(new BeastTF(p));

		//Ninjutsu
		skillPool.add(new StealClothes(p));
		skillPool.add(new BunshinAssault(p));
		skillPool.add(new BunshinService(p));
		skillPool.add(new Substitute(p));
		skillPool.add(new GoodnightKiss(p));
		skillPool.add(new KoushoukaBurst(p));

		//Submissive
		skillPool.add(new Dive(p));
		skillPool.add(new Cowardice(p));
		skillPool.add(new Stumble(p));
		skillPool.add(new Invite(p));
		skillPool.add(new Beg(p));
		skillPool.add(new ShamefulDisplay(p));
		skillPool.add(new Buck(p));
		skillPool.add(new HoneyTrap(p));
		skillPool.add(new PleasureSlave(p));

		//Discipline
		skillPool.add(new WarningCrop(p));
		skillPool.add(new MastersOrder(p));
		skillPool.add(new DominatingGaze(p));
		skillPool.add(new BerserkerBarrage(p));
		skillPool.add(new RegainComposure(p));
		skillPool.add(new Punishment(p));

		//Footballer
		skillPool.add(new ChargeFeint(p));
		skillPool.add(new PenaltyShot(p));
		skillPool.add(new KissBetter(p));
		skillPool.add(new PenaltyKick(p));
		skillPool.add(new SpreadThighs(p));
		skillPool.add(new OpenGoal(p));
		skillPool.add(new HandBall(p));

		//Professional
		skillPool.add(new Blindside(p));

		//Hypnosis
		skillPool.add(new Suggestion(p));
		skillPool.add(new HeightenSenses(p));
		skillPool.add(new Humiliate(p));
		skillPool.add(new EnflameLust(p));
		skillPool.add(new CrushPride(p));

		//Temporal
		skillPool.add(new WindUp(p));
		skillPool.add(new Haste(p));
		skillPool.add(new CheapShot(p));
		skillPool.add(new AttireShift(p));
		skillPool.add(new EmergencyJump(p));
		skillPool.add(new Unstrip(p));
		skillPool.add(new Rewind(p));

		//Contender
		skillPool.add(new DramaticPose(p));
		skillPool.add(new SecondWind(p));
		skillPool.add(new Soulmate(p));

		//Spiritual
		skillPool.add(new Tranquility(p));
		skillPool.add(new CleanseMind(p));
		skillPool.add(new Banish(p));
		skillPool.add(new Purge(p));
		skillPool.add(new InnerPeace(p));

		//Unknowable
		skillPool.add(new DecayClothes(p));
		skillPool.add(new Vertigo(p));
		skillPool.add(new LustRelease(p));
		skillPool.add(new EcstasyWave(p));
		skillPool.add(new Blink(p));

		//Special
		skillPool.add(new PerfectTouch(p));
		skillPool.add(new HipThrow(p));
		skillPool.add(new SpiralThrust(p));
		skillPool.add(new Bravado(p));
		skillPool.add(new Diversion(p));

		//Item
		skillPool.add(new Tie(p));
		skillPool.add(new UseDildo(p));
		skillPool.add(new UseOnahole(p));
		skillPool.add(new UseCrop(p));
		skillPool.add(new Undress(p));
		skillPool.add(new Strapon(p));
		skillPool.add(new VibroTease(p));
		skillPool.add(new Scroll(p));
		skillPool.add(new DarkTalisman(p));
		skillPool.add(new Needle(p));
		skillPool.add(new UsePowerband(p));
		skillPool.add(new UseExcalibur(p));
		skillPool.add(new UseClamps(p));
		skillPool.add(new InsertBead(p));
		skillPool.add(new PullBeads(p));

		//Misc
		skillPool.add(new Release(p));
		skillPool.add(new Masturbate(p));
		skillPool.add(new FaceSit(p));
		skillPool.add(new GoldCock(p));
		skillPool.add(new LowBlow(p));
		skillPool.add(new KittyKick(p));
		skillPool.add(new Rotate(p));

		//Pets
		skillPool.add(new SpawnFaerie(p, Ptype.fairyfem));
		skillPool.add(new SpawnImp(p, Ptype.impfem));
		skillPool.add(new SpawnFaerie(p, Ptype.fairymale));
		skillPool.add(new SpawnImp(p, Ptype.impmale));
		skillPool.add(new SpawnSlime(p));
		skillPool.add(new SpawnFGoblin(p));

		//Commands
		skillPool.add(new Command(p));
		skillPool.add(new Obey(p));
		skillPool.add(new CommandDismiss(p));
		skillPool.add(new CommandDown(p));
		skillPool.add(new CommandGive(p));
		skillPool.add(new CommandHurt(p));
		skillPool.add(new CommandInsult(p));
		skillPool.add(new CommandMasturbate(p));
		skillPool.add(new CommandOral(p));
		skillPool.add(new CommandStrip(p));
		skillPool.add(new CommandStripPlayer(p));
		skillPool.add(new CommandUse(p));

		//EX
		skillPool.add(new KissEX(p));
		skillPool.add(new KneeEX(p));
		skillPool.add(new HandjobEX(p));
		skillPool.add(new StripTopEX(p));
		skillPool.add(new StripBottomEX(p));
		skillPool.add(new FingerEX(p));
		skillPool.add(new ThrustEX(p));
	}

	/**
	 * Builds the pool of all usable actions
	 */
	public static void buildActionPool() {
		actionPool = new ArrayList<>();
		actionPool.add(new Resupply());
		actionPool.add(new Bathe());
		actionPool.add(new Scavenge());
		actionPool.add(new Craft());
		actionPool.add(new Use(Flask.Lubricant));
		actionPool.add(new Recharge());
		actionPool.add(new JerkOff());
		actionPool.add(new Energize());
		actionPool.add(new Hide());
		for (var potion : Potion.values()) {
			actionPool.add(new Drink(potion));
		}
		buildTrapPool();
		for (var trap : trapPool) {
			actionPool.add(new SetTrap(trap));
		}
		actionPool.add(new Wait());
	}

	/**
	 * Add actions that target a specific combatant outside of combat
	 *
	 * @param combatants A collection of combatants
	 */
	public static void populateTargetedActions(Collection<Character> combatants) {
		for (var active : combatants) {
			actionPool.add(new Locate(active));
		}
	}

	/**
	 * Gets all usable actions
	 */
	public static ArrayList<Action> getActions() {
		return actionPool;
	}

	/**
	 * Gets all learnable feats
	 */
	public static HashSet<Trait> getFeats() {
		return featPool;
	}

	/**
	 * Gets a string containing information about a character's upcoming skills (maximum 5)
	 *
	 * @param attribute The attribute to get upcoming skills for
	 * @param level     The character's level in the specified attribute
	 * @return A string containing the character's upcoming skills and their required level,
	 * or an empty string if none were found
	 */
	public static String getUpcomingSkills(Attribute attribute, int level) {
		var list = new StringBuilder(attribute.name() + "(" + level + "): ");
		var found = 0;
		if (skillList.containsKey(attribute)) {
			var skills = skillList.get(attribute);
			var levels = new ArrayList<>(skills.keySet());
			levels.sort(null);
			for (int i : levels) {
				if (level < i) {
					list.append(String.format("%s (%d), ", skills.get(i), i));
					found++;
					if (found >= 5) {
						break;
					}
				}
			}
		}
		if (found > 0) {
			return list.toString();
		}
		else {
			return "";
		}
	}

	/**
	 * Assigns the specified character any skills or traits they are eligible for, based on their stats
	 * <p>
	 * If the character is the player, gained skills are written to the GUI
	 *
	 * @param c The character to gain skills
	 */
	public static void gainSkills(Character c) {
		gainSkills(c, true);
	}

	/**
	 * Assigns the specified character any skills or traits they are eligible for, based on their stats
	 *
	 * @param character The character to gain skills
	 * @param display   Whether the gained skills should be written to the GUI (only if the player is gaining them)
	 */
	public static void gainSkills(Character character, boolean display) {
		for (var skill : skillPool) {
			if (skill.requirements(character) && !character.knows(skill.copy(character))) {
				character.learn(skill.copy(character));
				if (display && character.human()) {
					gui().message("You've learned " + skill + ".");
				}
			}
		}
		if (character.getPure(Attribute.Dark) >= 6 && !character.has(Trait.darkpromises)) {
			character.add(Trait.darkpromises);
		}
		if (character.getPure(Attribute.Animism) >= 2 && !character.has(Trait.pheromones)) {
			if (character.human()) {
				gui().message("You've gained the passive skill Pheromones.");
			}
			character.add(Trait.pheromones);
		}
		if (character.getPure(Attribute.Science) >= 24 && !character.has(Trait.improvedbattery)) {
			if (character.human()) {
				gui().message("You've gained the passive skill Improved Battery.");
			}
			character.getPool(Pool.BATTERY).gainMax(10);
			character.add(Trait.improvedbattery);
		}
		if (character.getPure(Attribute.Fetish) >= 1 && !character.has(Trait.exhibitionist)) {
			if (character.human()) {
				gui().message("You've gained the passive skill Exhibitionist.");
			}
			character.add(Trait.exhibitionist);
		}
		if (character.getPure(Attribute.Contender) >= 5 && !character.has(Trait.determinator)) {
			if (character.human()) {
				gui().message("You've gained the passive skill Determinator.");
			}
			character.add(Trait.determinator);
		}
	}

	/**
	 * Sets the specified flag
	 *
	 * @param f The flag to set
	 */
	public static void flag(Flag f) {
		flags.add(f);
	}

	/**
	 * Clears the specified flag
	 *
	 * @param f The flag to clear
	 */
	public static void unflag(Flag f) {
		flags.remove(f);
	}

	/**
	 * Checks if the specified flag is set
	 *
	 * @param f The flag to check
	 */
	public static boolean checkFlag(Flag f) {
		return flags.contains(f);
	}

	/**
	 * Gets all currently set flags
	 */
	public static HashSet<Flag> getFlags() {
		return flags;
	}

	/**
	 * Gets the value assigned to the specified flag
	 *
	 * @param f The flag to check
	 * @return The value assigned to the flag, or 0 if none was found
	 */
	public static float getValue(Flag f) {
		if (!counters.containsKey(f)) {
			return 0;
		}
		else {
			return counters.get(f);
		}
	}

	/**
	 * Modifies the value assigned to the specified flag
	 *
	 * @param f   The flag to modify
	 * @param inc The amount to change the assigned value by
	 */
	public static void modCounter(Flag f, float inc) {
		if (!counters.containsKey(f)) {
			setCounter(f, inc);
		}
		else {
			counters.put(f, getValue(f) + inc);
		}
	}

	/**
	 * Sets the value assigned to the specified flag
	 *
	 * @param f   The flag to modify
	 * @param val The new value assigned to the flag
	 */
	public static void setCounter(Flag f, float val) {
		counters.put(f, val);
	}

	/**
	 * Gets all flags with their assigned values
	 */
	public static HashMap<Flag, Float> getCounters() {
		return counters;
	}

	/**
	 * Marks the specified scene as watched
	 *
	 * @param f The scene to flag
	 */
	public static void watched(SceneFlag f) {
		watched.add(f);
	}

	/**
	 * Checks whether the specified scene has been marked as watched
	 *
	 * @param f The scene to check
	 */
	public static boolean hasWatched(SceneFlag f) {
		return watched.contains(f);
	}

	/**
	 * Gets a list of all scenes marked as watched
	 */
	public static List<SceneFlag> getAllWatched() {
		return new ArrayList<>(watched);
	}

	/**
	 * Gets a reference to the item with the specified name
	 *
	 * @param name The name to check for
	 * @return A reference to the item, or Null if none was found
	 */
	public static Item getItem(String name) {
		for (var flask : Flask.values()) {
			if (flask.toString().equalsIgnoreCase(name)) {
				return flask;
			}
		}
		for (var potion : Potion.values()) {
			if (potion.toString().equalsIgnoreCase(name)) {
				return potion;
			}
		}
		for (var component : Component.values()) {
			if (component.toString().equalsIgnoreCase(name)) {
				return component;
			}
		}
		for (var consumable : Consumable.values()) {
			if (consumable.toString().equalsIgnoreCase(name)) {
				return consumable;
			}
		}
		for (var trophy : Trophy.values()) {
			if (trophy.toString().equalsIgnoreCase(name)) {
				return trophy;
			}
		}
		for (var toy : Toy.values()) {
			if (toy.toString().equalsIgnoreCase(name)) {
				return toy;
			}
		}
		for (var attachment : Attachment.values()) {
			if (attachment.toString().equalsIgnoreCase(name)) {
				return attachment;
			}
		}
		return null;
	}

	/**
	 * Runs the game
	 *
	 * @param args Arguments passed to the game
	 */
	public static void main(String[] args) {
		new GUI();
	}

	/**
	 * Gets the game intro text
	 */
	public static String getIntro() {
		return "Warning This game contains depictions of explicit sexual content. Do not proceed unless you are at least 18 and are comfortable with such content. "
				+ "All characters depicted are over 18 years of age.\n\n"
				+ "You don't really know why you're going to the Student Union in the middle of the night. You'd have to be insane to accept the invitation you received this afternoon. Seriously, someone " +
				"is offering you money to sexfight a bunch of girls? You're more likely to get mugged (though you're not carrying any money) or murdered if you show up. Best case scenario, it's probably a prank " +
				"for gullible freshmen. You have no good reason to believe the invitation is on the level, but here you are, walking into the empty Student Union.\n\n"
				+ "Not quite empty, it turns out. The same " +
				"woman who approached you this afternoon greets you and brings you to a room near the back of the building. Inside, you're surprised to find three quite attractive girls. After comparing notes, " +
				"you confirm they're all freshmen like you and received the same invitation today. You're surprised, both that these girls would agree to such an invitation, and that you're the only guy here. For " +
				"the first time, you start to believe that this might actually happen.\n\n"
				+ "After a few minutes of awkward small talk (though none of these girls seem self-conscious about being here), the woman walks " +
				"in again leading another girl. Embarrassingly you recognize the girl, named Cassie, who is a classmate of yours, and who you've become friends with over the past couple weeks. She blushes when she " +
				"sees you and the two of you consciously avoid eye contact while the woman explains the rules of the competition.\n\n"
				+ "There are a lot of specific points, but the rules basically boil down to this:\n" +
				"*Competitors move around the empty areas of the campus and engage each other in sexfights.\n"
				+ "*When one competitor orgasms, the other gets a point and can claim their clothes.\n"
				+ "*Additional orgasms between those two players are not worth any points until the loser gets a replacement set of clothes at either the Student Union or the first floor of the dorm building.\n "
				+ "*It seems to be customary, but not required, for the loser to get the winner off after a fight, when it doesn't count.\n"
				+ "*After three hours, the match ends and each player is paid for each opponent they defeat, each set of clothes taken, and a bonus for whoever scores the most points.\n\n"
				+ "After the explanation, she confirms with each participant whether they are still interested in participating.\n\n"
				+ "Everyone agrees. The first match starts at exactly 10:00.";
	}

	/**
	 * Resets the game
	 */
	public static void reset() {
		flags.clear();
		counters.clear();
		human = new Player("Dummy");
		gui.purgePlayer();
		setDefaultOptions();
		Global.gui().clearText();
	}

	/**
	 * Picks a random item from the specified list
	 * Both arrays must have the same length!
	 *
	 * @param array The array to pick an object from
	 * @return An element picked from the list
	 */
	public static String pickRandom(Object[] array) {
		return array[Global.random(array.length)].toString();
	}

	/**
	 * Picks a random item from the specified list, following the specified weights
	 * Both arrays must have the same length!
	 *
	 * @param array   The array to pick an object from
	 * @param weights The array of weights to apply to their respective item
	 * @return An element picked from the list
	 */
	public static String pickRandom(Object[] array, Integer[] weights) {
		if (array.length != weights.length) {
			throw new IllegalArgumentException();
		}

		int choice;
		var totalWeight = 0;
		var count = 0;
		for (var w : weights) {
			totalWeight += w;
		}
		choice = random(totalWeight);
		for (var i = 0; i < array.length; i++) {
			count += weights[i];
			if (choice <= count) {
				return array[i].toString();
			}
		}
		return array[0].toString();
	}

	/**
	 * Builds the pool of learnable feats
	 */
	private static void buildFeatPool() {
		featPool = new HashSet<>();
		featPool.add(Trait.sprinter);
		featPool.add(Trait.QuickRecovery);
		featPool.add(Trait.rapidrecovery);
		featPool.add(Trait.Sneaky);
		featPool.add(Trait.Confident);
		featPool.add(Trait.SexualGroove);
		featPool.add(Trait.BoundlessEnergy);
		featPool.add(Trait.Unflappable);
		featPool.add(Trait.resourceful);
		featPool.add(Trait.treasureSeeker);
		featPool.add(Trait.sympathetic);
		featPool.add(Trait.leadership);
		featPool.add(Trait.tactician);
		featPool.add(Trait.PersonalInertia);
		featPool.add(Trait.fitnessNut);
		featPool.add(Trait.expertGoogler);
		featPool.add(Trait.mojoMaster);
		featPool.add(Trait.fastLearner);
		featPool.add(Trait.veryfastLearner);
		featPool.add(Trait.cautious);
		featPool.add(Trait.responsive);
		featPool.add(Trait.assmaster);
		featPool.add(Trait.Clingy);
		featPool.add(Trait.freeSpirit);
		featPool.add(Trait.houdini);
		featPool.add(Trait.coordinatedStrikes);
		featPool.add(Trait.evasiveManuevers);
		featPool.add(Trait.handProficiency);
		featPool.add(Trait.handExpertise);
		featPool.add(Trait.handMastery);
		featPool.add(Trait.oralProficiency);
		featPool.add(Trait.oralExpertise);
		featPool.add(Trait.oralMastery);
		featPool.add(Trait.intercourseProficiency);
		featPool.add(Trait.intercourseExpertise);
		featPool.add(Trait.intercourseMastery);
		featPool.add(Trait.footloose);
		featPool.add(Trait.amateurMagician);
		featPool.add(Trait.bountyHunter);
		featPool.add(Trait.challengeSeeker);
		featPool.add(Trait.showoff);
		featPool.add(Trait.tieGuy);
		featPool.add(Trait.staydown);
		featPool.add(Trait.opportunist);
		featPool.add(Trait.tracker);
		featPool.add(Trait.advtracker);
		featPool.add(Trait.mastertracker);
	}

	/**
	 * Builds a list of all skills, grouped by their respective attribute and at which level their are learned
	 */
	private static void buildSkillList() {
		skillList = new HashMap<>();
		var powerList = new HashMap<Integer, String>();
		powerList.put(6, "Turnover");
		powerList.put(7, "Slap");
		powerList.put(8, "Pin");
		powerList.put(9, "Squeeze Balls");
		powerList.put(10, "Knee");
		powerList.put(13, "Twist Nipple");
		powerList.put(16, "Stomp");
		powerList.put(17, "Kick");
		powerList.put(20, "Armbar");
		powerList.put(24, "Leg Lock");
		powerList.put(26, "Tackle");
		powerList.put(30, "Carry");
		powerList.put(32, "Tear Clothes");
		skillList.put(Attribute.Power, powerList);

		var seductionList = new HashMap<Integer, String>();
		seductionList.put(8, "Spank");
		seductionList.put(10, "Lick Pussy");
		seductionList.put(12, "Suck Neck");
		seductionList.put(14, "Lick Nipples");
		seductionList.put(17, "Flick");
		seductionList.put(18, "Piston");
		seductionList.put(20, "Ass Fuck");
		seductionList.put(22, "Footjob");
		seductionList.put(24, "Strip Tease");
		seductionList.put(26, "Frottage");
		seductionList.put(28, "Finger Ass");
		seductionList.put(32, "Whisper");
		skillList.put(Attribute.Seduction, seductionList);

		var cunningList = new HashMap<Integer, String>();
		cunningList.put(6, "Alarm");
		cunningList.put(7, "Decoy");
		cunningList.put(8, "Taunt");
		cunningList.put(10, "Spring Trap");
		cunningList.put(11, "Snare");
		cunningList.put(12, "Escape");
		cunningList.put(14, "Aphrodisiac Trap");
		cunningList.put(15, "Focus");
		cunningList.put(16, "Trip");
		cunningList.put(17, "Dissolving Trap");
		cunningList.put(20, "Maneuver");
		cunningList.put(24, "Reversal");
		cunningList.put(28, "Shortcuts");
		cunningList.put(30, "Double Strip");
		skillList.put(Attribute.Cunning, cunningList);

		var arcaneList = new HashMap<Integer, String>();
		arcaneList.put(1, "Magic Missile");
		arcaneList.put(3, "Faerie Familiar");
		arcaneList.put(6, "Illusion Trap");
		arcaneList.put(9, "Binding");
		arcaneList.put(12, "Illusions");
		arcaneList.put(15, "Naked Bloom");
		arcaneList.put(18, "Barrier");
		arcaneList.put(21, "Mage Armor");
		arcaneList.put(24, "Royal Faeries");
		arcaneList.put(30, "Mana Fortification");
		skillList.put(Attribute.Arcane, arcaneList);

		var darkList = new HashMap<Integer, String>();
		darkList.put(1, "Energy Drain");
		darkList.put(3, "Lust Aura");
		darkList.put(6, "Summon Imp");
		darkList.put(9, "Domination");
		darkList.put(12, "Dark Tendrils");
		darkList.put(15, "Sacrifice");
		darkList.put(18, "Fly");
		darkList.put(21, "Drain");
		darkList.put(24, "Shadow Fingers");
		darkList.put(30, "Lust Overflow");
		skillList.put(Attribute.Dark, darkList);

		var kiList = new HashMap<Integer, String>();
		kiList.put(1, "Shredding Palm");
		kiList.put(3, "Water Form");
		kiList.put(6, "Flash Step");
		kiList.put(9, "Fly Catcher");
		kiList.put(12, "Stone Form, Submission Holds");
		kiList.put(15, "Fire Form");
		kiList.put(18, "Ice Form");
		kiList.put(21, "Shatter Kick");
		kiList.put(24, "Burst");
		kiList.put(30, "Pleasure Bomb");
		skillList.put(Attribute.Ki, kiList);

		var scienceList = new HashMap<Integer, String>();
		scienceList.put(1, "Shock Glove");
		scienceList.put(3, "Slime");
		scienceList.put(4, "Strip Mine");
		scienceList.put(6, "Aerosolizer");
		scienceList.put(9, "Stun Blast");
		scienceList.put(12, "Shrink Ray");
		scienceList.put(15, "Short Circuit");
		scienceList.put(18, "Defabricator");
		scienceList.put(21, "Fabricator");
		scienceList.put(24, "Improved Battery");
		scienceList.put(30, "Matter Converter");
		skillList.put(Attribute.Science, scienceList);

		var fetishList = new HashMap<Integer, String>();
		fetishList.put(1, "Exhibitionism");
		fetishList.put(3, "Bondage");
		fetishList.put(6, "Fetish Goblin");
		fetishList.put(9, "Masochism");
		fetishList.put(12, "Tentacle Porn");
		fetishList.put(15, "Face Fuck");
		fetishList.put(18, "Bondage Straps");
		fetishList.put(21, "Tortoise Wrap");
		fetishList.put(30, "Nymphomania");
		skillList.put(Attribute.Fetish, fetishList);

		var animismList = new HashMap<Integer, String>();
		animismList.put(1, "Pounce");
		animismList.put(2, "Pheromones");
		animismList.put(3, "Cat's Grace");
		animismList.put(4, "Feral Power");
		animismList.put(6, "Tail Job");
		animismList.put(9, "Purr");
		animismList.put(12, "Tiger Claw");
		animismList.put(15, "Shred Clothes");
		animismList.put(18, "Beast Transform");
		animismList.put(30, "Pheromone Pink Overdrive");
		skillList.put(Attribute.Animism, animismList);

		var ninjutsuList = new HashMap<Integer, String>();
		ninjutsuList.put(1, "Needle");
		ninjutsuList.put(3, "Smoke Bomb");
		ninjutsuList.put(5, "Enhanced Mobility");
		ninjutsuList.put(6, "Bunshin Assault");
		ninjutsuList.put(9, "Stash");
		ninjutsuList.put(12, "Bunshin Service");
		ninjutsuList.put(15, "Steal Clothes");
		ninjutsuList.put(18, "Goodnight Kiss");
		ninjutsuList.put(21, "Substitution");
		ninjutsuList.put(30, "Fertility Rite");
		skillList.put(Attribute.Ninjutsu, ninjutsuList);

		var disciplineList = new HashMap<Integer, String>();
		disciplineList.put(1, "Composure");
		disciplineList.put(3, "Warning Crop");
		disciplineList.put(6, "Master's Order");
		disciplineList.put(9, "Confident Stance");
		disciplineList.put(12, "Dominating Gaze");
		disciplineList.put(15, "Reward Strike");
		disciplineList.put(18, "Berserker Barrage");
		disciplineList.put(21, "Regain Composure");
		disciplineList.put(30, "Punishment");
		skillList.put(Attribute.Discipline, disciplineList);

		var submissiveList = new HashMap<Integer, String>();
		submissiveList.put(1, "Dive");
		submissiveList.put(3, "Cowardice");
		submissiveList.put(6, "Invite");
		submissiveList.put(9, "Stumble");
		submissiveList.put(12, "Beg");
		submissiveList.put(15, "Shameful Display");
		submissiveList.put(18, "Buck");
		submissiveList.put(21, "Honey Trap");
		submissiveList.put(30, "Pleasure Slave");
		skillList.put(Attribute.Submissive, submissiveList);

		var footballerList = new HashMap<Integer, String>();
		footballerList.put(1, "Charge Feint");
		footballerList.put(3, "Free Kick");
		footballerList.put(6, "Spread Thighs");
		footballerList.put(9, "Hand Ball");
		footballerList.put(12, "Penalty Shot/Kick");
		footballerList.put(15, "Kiss Better");
		footballerList.put(18, "Dribble");
		footballerList.put(21, "Unsporting Celebration");
		footballerList.put(30, "Open Goal");
		skillList.put(Attribute.Footballer, footballerList);

		var eldritchList = new HashMap<Integer, String>();
		eldritchList.put(1, "Tentacle Lash");
		eldritchList.put(3, "Tentacle Caress");
		eldritchList.put(6, "Entanglement");
		eldritchList.put(9, "Entangling Strands");
		eldritchList.put(12, "Slippery Strands");
		eldritchList.put(15, "Leverage");
		eldritchList.put(18, "Intoxicating Strands");
		eldritchList.put(21, "Scintillating Strands");
		eldritchList.put(24, "Tentacle Drain");
		eldritchList.put(30, "Tentacle Ravage");
		skillList.put(Attribute.Eldritch, eldritchList);

		var unknowableList = new HashMap<Integer, String>();
		unknowableList.put(1, "Decay Clothes");
		unknowableList.put(3, "Vertigo");
		unknowableList.put(6, "Delusion");
		unknowableList.put(9, "Ecstasy Wave");
		unknowableList.put(12, "Vitality Drain");
		unknowableList.put(15, "Nullify");
		unknowableList.put(18, "Blink");
		unknowableList.put(21, "Lust Release");
		unknowableList.put(24, "Weak Touch");
		skillList.put(Attribute.Unknowable, unknowableList);

		var professionalList = new HashMap<Integer, String>();
		professionalList.put(1, "Tempting Whisper");
		professionalList.put(3, "Pro Handjob");
		professionalList.put(5, "Pro Oral");
		professionalList.put(7, "Charming Strip Tease");
		professionalList.put(9, "Blindside");
		professionalList.put(11, "Pro Thrust");
		skillList.put(Attribute.Professional, professionalList);

		var temporalList = new HashMap<Integer, String>();
		temporalList.put(1, "Haste");
		temporalList.put(2, "Cheap Shot");
		temporalList.put(4, "Emergency Jump");
		temporalList.put(6, "Attire Shift");
		temporalList.put(8, "Unstrip");
		temporalList.put(10, "Rewind");
		skillList.put(Attribute.Temporal, temporalList);

		var hypnosisList = new HashMap<Integer, String>();
		hypnosisList.put(1, "Suggestion");
		hypnosisList.put(2, "Enflame Lust");
		hypnosisList.put(4, "Heighten Senses");
		hypnosisList.put(6, "Humiliate");
		hypnosisList.put(8, "Crush Pride");
		skillList.put(Attribute.Hypnosis, hypnosisList);

		var contenderList = new HashMap<Integer, String>();
		contenderList.put(1, "Dramatic Pose");
		contenderList.put(2, "2nd Wind");
		contenderList.put(4, "Determinator");
		contenderList.put(6, "Soulmate");
		skillList.put(Attribute.Contender, contenderList);

		var spiritualList = new HashMap<Integer, String>();
		spiritualList.put(1, "Tranquility");
		spiritualList.put(2, "Cleanse Mind");
		spiritualList.put(4, "Banish");
		spiritualList.put(6, "Purge");
		spiritualList.put(8, "Inner Peace");
		skillList.put(Attribute.Spirituality, spiritualList);
	}

	/**
	 * Builds the pool of usable traps
	 */
	private static void buildTrapPool() {
		trapPool = new ArrayList<>();
		trapPool.add(new Alarm());
		trapPool.add(new Tripline());
		trapPool.add(new Snare());
		trapPool.add(new SpringTrap());
		trapPool.add(new AphrodisiacTrap());
		trapPool.add(new DissolvingTrap());
		trapPool.add(new Decoy());
		trapPool.add(new Spiderweb());
		trapPool.add(new EnthrallingTrap());
		trapPool.add(new IllusionTrap());
		trapPool.add(new StripMine());
		trapPool.add(new TentacleTrap());
	}

	private static void setDefaultOptions() {
		setCounter(Flag.NPCBaseStrength, 1.0f);
		setCounter(Flag.NPCScaling, 1.0f);
		setCounter(Flag.PlayerBaseStrength, 1.0f);
		setCounter(Flag.PlayerScaling, 1.0f);
		setCounter(Flag.fontsize, 6);
		setCounter(Flag.PlayerAssLosses, -1.0f); // -1 indicates complete virginity, set to 0 when enemy uses AssFuck
	}
}
