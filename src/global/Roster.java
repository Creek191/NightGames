package global;

import characters.Character;
import characters.*;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashSet;

public class Roster {
	private static Roster instance;
	private final HashSet<Relationship> affection;
	private final HashSet<Relationship> attraction;
	private final ArrayList<Character> players;

	/**
	 * Gets the singleton instance of the roster
	 */
	public static Roster getInstance() {
		if (instance == null) {
			instance = new Roster();
		}
		return instance;
	}

	private Roster() {
		players = new ArrayList<>();
		affection = new HashSet<>();
		attraction = new HashSet<>();
		players.add(Global.getPlayer());
		players.add(new Cassie().getCharacter());
		players.add(new Mara().getCharacter());
		players.add(new Angel().getCharacter());
		players.add(new Jewel().getCharacter());
		players.add(new Yui().getCharacter());
		players.add(new Kat().getCharacter());
		players.add(new Reyka().getCharacter());
		players.add(new Eve().getCharacter());
		players.add(new Samantha().getCharacter());
		players.add(new Valerie().getCharacter());
		players.add(new Sofia().getCharacter());
		players.add(new Maya().getCharacter());
	}

	/**
	 * Resets the roster at the start of a new game
	 *
	 * @param player The created player character
	 */
	public static void reset(Player player) {
		instance = new Roster();
		getInstance().players.remove(get(ID.PLAYER));
		getInstance().players.add(player);
	}

	/**
	 * Gets the character with the specified ID from the roster
	 *
	 * @param identity The ID of the character to return
	 * @return The found character, or Null if none was found
	 */
	public static Character get(ID identity) {
		for (var character : getInstance().players) {
			if (identity == character.id()) {
				return character;
			}
		}
		return null;
	}

	/**
	 * Gets the character with the specified name
	 *
	 * @param name The name of the character to return
	 * @return The found character, or Null if none was found
	 */
	public static Character get(String name) {
		for (var character : getInstance().players) {
			if (character.name().equalsIgnoreCase(name)) {
				return character;
			}
		}
		return null;
	}

	/**
	 * Checks whether the NPC with the specified ID is enabled in the game
	 *
	 * @param npc The ID of the NPC to check for
	 */
	public static boolean exists(ID npc) {
		switch (npc) {
			case EVE:
				return Global.checkFlag(Flag.Eve);
			case KAT:
				return Global.checkFlag(Flag.Kat);
			case REYKA:
				return Global.checkFlag(Flag.Reyka);
			case YUI:
				return Global.checkFlag(Flag.metYui);
			case SAMANTHA:
				return Global.checkFlag(Flag.Samantha);
			case VALERIE:
				return Global.checkFlag(Flag.Valerie);
			case SOFIA:
				return Global.checkFlag(Flag.Sofia);
		}
		return true;
	}

	/**
	 * Checks whether the specified NPC can participate in matches
	 *
	 * @param npc The ID of the NPC to check for
	 */
	public static boolean canParticipate(ID npc) {
		if (!exists(npc)) {
			return false;
		}
		var character = get(npc);
		if (character == null || character.isBenched()) {
			return false;
		}
		switch (npc) {
			case MAYA:
				// Maya can only participate during special events
				return false;
			case YUI:
				// Yui will automatically participate after her first-time event
				return Global.checkFlag(Flag.Yui);
		}
		return true;
	}

	/**
	 * Checks whether the specified characters are intimate with each other
	 *
	 * @param first  The first character
	 * @param second The second character
	 */
	public static boolean areIntimate(ID first, ID second) {
		var pair = new HashSet<ID>();
		pair.add(first);
		pair.add(second);

		// Player must have reached specific events with a character
		if (pair.contains(ID.PLAYER)) {
			if (pair.contains(ID.CASSIE)) {
				return Global.checkFlag(Flag.CassieDate);
			}
			if (pair.contains(ID.MARA)) {
				return Global.checkFlag(Flag.MaraDate);
			}
			if (pair.contains(ID.JEWEL)) {
				return Global.checkFlag(Flag.JewelDate);
			}
			if (pair.contains(ID.ANGEL)) {
				return Global.checkFlag(Flag.AngelDate);
			}
			if (pair.contains(ID.YUI)) {
				return Global.checkFlag(Flag.Yui);
			}
			if (pair.contains(ID.KAT)) {
				return Global.checkFlag(Flag.KatDate);
			}
			if (pair.contains(ID.REYKA)) {
				return Global.checkFlag(Flag.ReykaDate);
			}
			if (pair.contains(ID.VALERIE)) {
				return Global.checkFlag(Flag.Valerie);
			}
			if (pair.contains(ID.SAMANTHA)) {
				return Global.checkFlag(Flag.SamanthaDate);
			}
			if (pair.contains(ID.EVE)) {
				return Global.getValue(Flag.GangRank) >= 1;
			}
			if (pair.contains(ID.SOFIA)) {
				return Global.checkFlag(Flag.SofiaDate);
			}
			return false;
		}

		// NPCs are considered intimate with each other after reaching rank 1
		return exists(first) && get(first).getRank() >= 1
				&& exists(second) && get(second).getRank() >= 1;
	}

	/**
	 * Scales an opponent to the player's level
	 *
	 * @param npc The character to scale
	 */
	public static void scaleOpponent(ID npc) {
		var level = get(ID.PLAYER).getLevel();
		get(npc).scaleLevel(level);
	}

	/**
	 * Gets the attraction between two characters
	 *
	 * @param first  The first character
	 * @param second The second character
	 */
	public static int getAttraction(ID first, ID second) {
		if (!exists(first) || !exists(second)) {
			return 0;
		}

		for (var relationship : getInstance().attraction) {
			if (relationship.is(first, second)) {
				return relationship.get();
			}
		}
		return 0;
	}

	/**
	 * Increases the attraction between two characters
	 *
	 * @param first     The first character
	 * @param second    The second character
	 * @param magnitude The amount of attraction to add
	 */
	public static void gainAttraction(ID first, ID second, int magnitude) {
		Relationship pairing = null;
		for (var relationship : getInstance().attraction) {
			if (relationship.is(first, second)) {
				relationship.add(magnitude);
				pairing = relationship;
				break;
			}
		}

		// Create relationship if it doesn't exist
		if (pairing == null) {
			pairing = new Relationship(first, second, magnitude);
			getInstance().attraction.add(pairing);
		}

		// If the pair's attraction reaches 20, increase their affection
		if (areIntimate(first, second) && pairing.get() >= 20) {
			gainAffection(first, second, 2);
			pairing.add(-20);
		}
	}

	/**
	 * Sets the attraction between two characters
	 *
	 * @param first     The first character
	 * @param second    The second character
	 * @param magnitude The target attraction value
	 */
	public static void setAttraction(ID first, ID second, int magnitude) {
		for (var relationship : getInstance().attraction) {
			if (relationship.is(first, second)) {
				relationship.set(magnitude);
				return;
			}
		}
		var meeting = new Relationship(first, second, magnitude);
		getInstance().attraction.add(meeting);
	}

	/**
	 * Gets the affection between two characters
	 *
	 * @param first  The first character
	 * @param second The second character
	 */
	public static int getAffection(ID first, ID second) {
		if (!exists(first) || !exists(second)) {
			return 0;
		}

		for (var relationship : getInstance().affection) {
			if (relationship.is(first, second)) {
				return relationship.get();
			}
		}
		return 0;
	}

	/**
	 * Increases the affection between two characters
	 *
	 * @param first     The first character
	 * @param second    The second character
	 * @param magnitude The amount of affection to add
	 */
	public static void gainAffection(ID first, ID second, int magnitude) {
		for (var relationship : getInstance().affection) {
			if (relationship.is(first, second)) {
				relationship.add(magnitude);
				return;
			}
		}

		// Create relationship if it doesn't exist
		setAffection(first, second, magnitude);
	}

	/**
	 * Sets the affection between two characters
	 *
	 * @param first     The first character
	 * @param second    The second character
	 * @param magnitude The target affection value
	 */
	public static void setAffection(ID first, ID second, int magnitude) {
		for (var relationship : getInstance().affection) {
			if (relationship.is(first, second)) {
				relationship.set(magnitude);
				return;
			}
		}
		var meeting = new Relationship(first, second, magnitude);
		getInstance().affection.add(meeting);
	}

	/**
	 * Gets a list of existing characters
	 */
	public static ArrayList<Character> getExisting() {
		var group = new ArrayList<Character>();
		getInstance().players.stream()
				.filter(character -> exists(character.id()))
				.forEachOrdered(group::add);
		return group;
	}

	/**
	 * Gets a list of characters that can participate in a match
	 */
	public static ArrayList<Character> getCombatants() {
		var group = new ArrayList<Character>();
		getInstance().players.stream()
				.filter(character -> canParticipate(character.id()))
				.forEachOrdered(group::add);
		return group;
	}

	/**
	 * Gets a list of characters that are currently benched
	 */
	public static ArrayList<Character> getBenched() {
		var group = new ArrayList<Character>();
		getExisting().stream()
				.filter(Character::isBenched)
				.forEachOrdered(group::add);
		return group;
	}

	/**
	 * Saves the roster to a JSON object
	 *
	 * @return The JSON object containing the roster data
	 */
	public static JsonObject Save() {
		var saver = new JsonObject();

		// Characters
		for (var character : Roster.getInstance().players) {
			saver.add(character.id().name(), character.save());
		}

		var saved = new ArrayList<ID>();
		JsonObject current;
		boolean needed;

		// Affections
		var affections = new JsonObject();
		for (var id : ID.values()) {
			needed = false;
			current = new JsonObject();
			for (var relationship : getInstance().affection) {
				if (relationship.contains(id)) {
					if (!saved.contains(relationship.getPartner(id))) {
						needed = true;
						current.addProperty(relationship.getPartner(id).name(), relationship.get());
					}
				}
			}
			if (needed) {
				affections.add(id.name(), current);
			}
			saved.add(id);
		}
		saver.add("Affection", affections);
		saved.clear();

		// Attractions
		var attractions = new JsonObject();
		for (var id : ID.values()) {
			needed = false;
			current = new JsonObject();
			for (var relationship : getInstance().attraction) {
				if (relationship.contains(id)) {
					if (!saved.contains(relationship.getPartner(id))) {
						needed = true;
						current.addProperty(relationship.getPartner(id).name(), relationship.get());
					}
				}
			}
			if (needed) {
				attractions.add(id.name(), current);
			}
			saved.add(id);
		}
		saver.add("Attraction", attractions);
		return saver;
	}

	/**
	 * Loads the roster's data from JSON
	 *
	 * @param loader The loader containing the roster's data
	 */
	public static void Load(JsonObject loader) {
		instance = new Roster();

		// Characters
		for (var character : getInstance().players) {
			if (loader.has(character.id().name())) {
				character.load(loader.get(character.id().name()).getAsJsonObject());
			}
		}
		
		getInstance().attraction.clear();
		getInstance().affection.clear();
		ID p1;
		ID p2;

		// Affections
		var affections = loader.getAsJsonObject("Affection");
		for (var name : affections.keySet()) {
			p1 = ID.valueOf(name);
			var relationship = affections.getAsJsonObject(name);
			for (var name2 : relationship.keySet()) {
				p2 = ID.valueOf(name2);
				setAffection(p1, p2, relationship.get(name2).getAsInt());
			}
		}

		// Attractions
		var attractions = loader.getAsJsonObject("Attraction");
		for (var name : attractions.keySet()) {
			p1 = ID.valueOf(name);
			var relationship = attractions.getAsJsonObject(name);
			for (var name2 : relationship.keySet()) {
				p2 = ID.valueOf(name2);
				setAttraction(p1, p2, relationship.get(name2).getAsInt());
			}
		}
	}
}
