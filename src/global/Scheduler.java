package global;

import characters.Character;
import characters.ID;
import characters.Player;
import characters.Trait;
import daytime.Daytime;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 * Defines the scheduler that keeps track of in-game date and time, matches, and season scoring
 */
public class Scheduler {
	private static Scheduler instance;
	private static boolean paused;
	private final HashMap<ID, Integer> score;
	private int date;
	private int matchNum;
	private LocalTime time;
	private Daytime day;
	private Match match;

	private Scheduler() {
		date = 1;
		time = LocalTime.of(22, 0);
		matchNum = 0;
		score = new HashMap<>();
		paused = false;
	}

	/**
	 * Gets a reference to the scheduler
	 */
	public static Scheduler getInstance() {
		if (instance == null) {
			instance = new Scheduler();
		}
		return instance;
	}

	/**
	 * Completely resets the scheduler
	 */
	public static void reset() {
		instance = new Scheduler();
	}

	/**
	 * Begins a new day and initiates the daytime handling
	 */
	public static void dawn() {
		var now = getInstance();
		now.time = LocalTime.of(9, 0);
		now.match = null;
		if (getMatchNumber() > Constants.SEASONLENGTH && getDate() % 7 == 0) {
			now.matchNum = 0;
			Global.modCounter(Flag.SeasonNumber, 1);
			Global.unflag(Flag.OffSeason);
		}
		for (var character : Roster.getExisting()) {
			character.rest();
		}
		now.date++;
		now.day = new Daytime(Global.getPlayer());
		Global.gui().refresh();
	}

	/**
	 * Begins the end-of-day handling by either starting a match, or initiating the night off event
	 */
	public static void dusk() {
		var now = getInstance();
		now.day = null;
		now.time = LocalTime.of(22, 0);
		var player = (Player) Roster.get(ID.PLAYER);
		if (Scheduler.hasMatch(player)) {
			new Prematch(player);
		}
		else if (isMatchNight()) {
			createMatch(Modifier.quiet);
			new NightOff(player);
			getMatch().automate(LocalTime.of(0, 0));
		}
		else {
			new NightOff(player);
		}
	}

	/**
	 * Creates a match with the specified modifier
	 *
	 * @param matchMod The modifier to apply to the match
	 */
	public static void createMatch(Modifier matchMod) {
		var lineup = new ArrayList<Character>();
		var available = Roster.getCombatants();
		if (matchMod == Modifier.maya) {
			// Add maya to a random lineup of combatants
			lineup.add(Roster.get(ID.PLAYER));
			Collections.shuffle(available);
			for (var character : available) {
				if (!lineup.contains(character) && !character.human() && lineup.size() < 4 && !character.has(Trait.event)) {
					lineup.add(character);
				}
			}
			if (!Global.checkFlag(Flag.Maya)) {
				Global.flag(Flag.Maya);
			}
			// Ensure maya is always a higher level than the player
			var maya = Roster.get(ID.MAYA);
			maya.scaleLevel(Roster.get(ID.PLAYER).getLevel() + 20);
			lineup.add(maya);
			getInstance().match = new Match(lineup, matchMod);
		}
		else if (available.size() > 5) {
			getInstance().match = new Match(calculateLineup(), matchMod);
		}
		else {
			getInstance().match = new Match(available, matchMod);
		}
		Global.buildActionPool();
		Global.populateTargetedActions(lineup);
		if (matchMod != Modifier.quiet) {
			getInstance().match.round();
		}
	}

	/**
	 * Checks whether the specified NPC is available for an event
	 *
	 * @param npc The ID of the NPC to check
	 * @return True if the player is not participating in a match, or the match hasn't started yet, otherwise False
	 */
	public static boolean isAvailable(ID npc) {
		return isAvailable(npc, getTime());
	}

	/**
	 * Checks whether the specified NPC is available for an event at a specific time
	 *
	 * @param npc  The ID of the NPC to check
	 * @param time The time to check the NPC for
	 * @return True if the player is not participating in a match, or the match hasn't started at that time, otherwise False
	 */
	public static boolean isAvailable(ID npc, LocalTime time) {
		var available = Roster.exists(npc);
		if (isMatchNight() && time.getHour() >= 10) {
			if (getMatch().combatants.contains(Roster.get(npc))) {
				available = false;
			}
		}
		return available;
	}

	/**
	 * Gets a list of characters currently available for an event
	 */
	public static ArrayList<Character> getAvailable() {
		return getAvailable(getTime());
	}

	/**
	 * Gets a list of characters available for an event at the specified time
	 *
	 * @param time The time to check for
	 */
	public static ArrayList<Character> getAvailable(LocalTime time) {
		var available = new ArrayList<Character>();
		Roster.getExisting().stream()
				.filter(character -> isAvailable(character.id(), time))
				.forEachOrdered(available::add);
		return available;
	}

	/**
	 * Gets the number of the next match
	 */
	public static int getMatchNumber() {
		return getInstance().matchNum;
	}

	/**
	 * Sets the number of the next match
	 *
	 * @param match The number of the match
	 */
	public static void setMatchNum(int match) {
		getInstance().matchNum = match;
	}

	/**
	 * Increments the match number
	 */
	public static void incMatchNum() {
		getInstance().matchNum++;
	}

	/**
	 * Gets a reference to the current match
	 *
	 * @return The currently ongoing match, or Null if there is none
	 */
	public static Match getMatch() {
		return getInstance().match;
	}

	/**
	 * Gets a reference to the current daytime handler
	 *
	 * @return The daytime handler, or Null if there is none
	 */
	public static Daytime getDay() {
		return getInstance().day;
	}

	/**
	 * Gets the current date
	 */
	public static int getDate() {
		return getInstance().date;
	}

	/**
	 * Checks whether there is a match scheduled for the current day
	 */
	public static boolean isMatchNight() {
		return getDate() % 7 != 0 && getMatchNumber() <= Constants.SEASONLENGTH + 1;
	}

	/**
	 * Sets the date
	 *
	 * @param day The new date
	 */
	public static void setDate(int day) {
		Scheduler.getInstance().date = day;
	}

	/**
	 * Gets the current time
	 */
	public static LocalTime getTime() {
		return getInstance().time;
	}

	/**
	 * Sets the time
	 *
	 * @param time The new time
	 */
	public static void setTime(LocalTime time) {
		getInstance().time = time;
	}

	/**
	 * Advances the time by the specified amount
	 *
	 * @param diff The amount of time that passes
	 */
	public static void advanceTime(LocalTime diff) {
		getInstance().time = getInstance().time.plusHours(diff.getHour());
		getInstance().time = getInstance().time.plusMinutes(diff.getMinute());
	}

	/**
	 * Builds a string containing the current scoring for all combatants
	 */
	public static String displayScores() {
		var display = new StringBuilder();
		var ordered = rankParticipants();
		for (var c : ordered) {
			display.append(c.name())
					.append(": ")
					.append(getScore(c.id()))
					.append(" points.")
					.append("<br>");
		}
		return display.toString();
	}

	/**
	 * Returns a list of all participants ranked by their score in the current season
	 */
	public static ArrayList<Character> rankParticipants() {
		var result = new ArrayList<Character>();
		boolean inserted;
		for (var combatant : Roster.getCombatants()) {
			inserted = false;
			for (var i = 0; i < result.size(); i++) {
				if (getScore(combatant.id()) > getScore(result.get(i).id())) {
					result.add(i, combatant);
					inserted = true;
					break;
				}
			}
			if (!inserted) {
				result.add(combatant);
			}
		}
		return result;
	}

	/**
	 * Checks if the specified character is in the next match.
	 *
	 * @param player The character to check for in the next lineup
	 * @return True if the character participates in the next match, otherwise False
	 */
	public static boolean hasMatch(Character player) {
		if (!isMatchNight()) {
			return false;
		}
		if (getMatchNumber() > Constants.SEASONLENGTH) {
			return player.human();
		}
		else {
			return calculateLineup().contains(player);
		}
	}

	/**
	 * Gets the score of the specified character
	 *
	 * @param player The id of the character to get the score of
	 * @return The specified player's score, or 0 if there isn't one
	 */
	public static int getScore(ID player) {
		return getInstance().score.getOrDefault(player, 0);
	}

	/**
	 * Increases the specified character's season score
	 *
	 * @param player The id of the character to increase the score of
	 * @param points The amount to increase the score by
	 */
	public static void addScore(ID player, int points) {
		setScore(player, getScore(player) + points);
	}

	/**
	 * Sets the specified character's season score
	 *
	 * @param player The id of the character to set the score for
	 * @param points The new score for the player
	 */
	public static void setScore(ID player, int points) {
		getInstance().score.put(player, points);
	}

	/**
	 * Gets a formatted string of the current time of day
	 */
	public static String getTimeString() {
		return LocalTime.parse(getTime().toString(),
				DateTimeFormatter.ofPattern("HH:mm")).format(DateTimeFormatter.ofPattern("hh:mm a"));
	}

	/**
	 * Gets the name of the specified day of the week
	 *
	 * @param day The day to get the name of
	 */
	public static String getDayString(int day) {
		switch (day % 7) {
			case 1:
				return "Monday";
			case 2:
				return "Tuesday";
			case 3:
				return "Wednesday";
			case 4:
				return "Thursday";
			case 5:
				return "Friday";
			case 6:
				return "Saturday";
			default:
				return "Sunday";
		}
	}

	/**
	 * Clears the scoring list of the season
	 */
	public static void clearScores() {
		getInstance().score.clear();
	}

	/**
	 * Pauses the match processing
	 */
	public static void pause() {
		paused = true;
	}

	/**
	 * Unpauses the match processing
	 */
	public static void unpause() {
		paused = false;
		if (getMatch() != null) {
			getMatch().round();
		}
	}

	/**
	 * Checks whether the match processing
	 */
	public static boolean isPaused() {
		return paused;
	}

	/**
	 * Calculate the lineup for the next match
	 */
	private static Collection<Character> calculateLineup() {
		return LineUp.getLineup(Roster.getCombatants(),
				getMatchNumber(),
				Math.round(Global.getValue(Flag.SeasonNumber)));
	}
}
