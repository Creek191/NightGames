package global;

import characters.ID;
import characters.Player;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import scenes.SceneFlag;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.Scanner;

/**
 * Handles saving/loading of the game
 */
public class SaveManager {

	private static String lastDirectory = "";

	/**
	 * Saves the game
	 *
	 * @param auto Whether this is an automatic or manual save
	 */
	public static void save(boolean auto) {
		FileWriter file;
		try {
			if (auto) {
				file = new FileWriter("./auto.sav");
			}
			else {
				// Check for previously used save/load directory
				if (lastDirectory.isBlank() || !Files.isDirectory(Paths.get(lastDirectory))) {
					lastDirectory = "./";
				}

				var dialog = new JFileChooser(lastDirectory);
				dialog.setMultiSelectionEnabled(false);
				dialog.setFileFilter(new FileNameExtensionFilter("Save File (.sav)", "sav"));
				dialog.setSelectedFile(new File("Save.sav"));

				var rv = dialog.showSaveDialog(Global.gui());
				if (rv != JFileChooser.APPROVE_OPTION) {
					return;
				}
				file = new FileWriter(dialog.getSelectedFile());
				lastDirectory = dialog.getSelectedFile().getParent();
			}

			var saver = new JsonObject();
			saver.add("Roster", Roster.Save());

			saver.addProperty("Date", Scheduler.getDate());
			saver.addProperty("Time", Scheduler.getTime().getHour());
			saver.addProperty("Match", Scheduler.getMatchNumber());

			var flags = new JsonArray();
			for (var flag : Global.getFlags()) {
				flags.add(flag.name());
			}
			saver.add("Flags", flags);

			var counters = new JsonObject();
			for (var flag : Global.getCounters().keySet()) {
				counters.addProperty(flag.name(), Global.getValue(flag));
			}
			saver.add("Counters", counters);

			var sceneFlags = new JsonArray();
			for (var flag : Global.getAllWatched()) {
				sceneFlags.add(flag.name());
			}
			saver.add("SceneFlags", sceneFlags);

			file.write(saver.toString());
			file.flush();
			file.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads a previously saved game file
	 */
	public static void load() {
		// Check for previously used save/load directory
		if (lastDirectory.isBlank() || !Files.isDirectory(Paths.get(lastDirectory))) {
			lastDirectory = "./";
		}

		var dialog = new JFileChooser(lastDirectory);
		dialog.setMultiSelectionEnabled(false);
		dialog.setFileFilter(new FileNameExtensionFilter("Save File (.sav)", "sav"));
		var rv = dialog.showOpenDialog(Global.gui());
		if (rv != JFileChooser.APPROVE_OPTION) {
			return;
		}

		Global.reset();

		Global.buildSkillPool(Global.getPlayer());
		FileReader file;
		try {
			file = new FileReader(dialog.getSelectedFile());
			var loader = new JsonParser();
			var data = loader.parse(file).getAsJsonObject();

			Roster.Load(data.getAsJsonObject("Roster"));

			Scheduler.setDate(data.get("Date").getAsInt());
			Scheduler.setTime(LocalTime.of(data.get("Time").getAsInt(), 0));
			Scheduler.setMatchNum(data.get("Match").getAsInt());

			var flags = data.getAsJsonArray("Flags");
			for (var flag : flags) {
				Global.flag(Flag.valueOf(flag.getAsString()));
			}

			var counters = data.getAsJsonObject("Counters");
			for (var key : counters.keySet()) {
				Global.setCounter(Flag.valueOf(key), counters.get(key).getAsFloat());
			}

			if (data.has("SceneFlags")) {
				var sceneFlags = data.getAsJsonArray("SceneFlags");
				for (var flag : sceneFlags) {
					Global.watched(SceneFlag.valueOf(flag.getAsString()));
				}
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (JsonParseException e) {
			try {
				load_legacy(new FileInputStream(dialog.getSelectedFile()));
			}
			catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		lastDirectory = dialog.getSelectedFile().getParent();

		Global.setPlayer(Roster.get(ID.PLAYER));
		Global.gui().populatePlayer((Player) Roster.get(ID.PLAYER));
		handleLegacyScenes();
		if (Scheduler.getTime().getHour() >= 22) {
			Scheduler.dusk();
		}
		else {
			Scheduler.dawn();
		}
	}

	/**
	 * Set scene flags that weren't available in older versions based on existing flags
	 */
	private static void handleLegacyScenes() {
		if (Global.checkFlag(Flag.MaraTemporal)) {
			Global.watched(SceneFlag.MaraSickDayNight);
			Global.watched(SceneFlag.MaraSickDayMorning);
			Global.watched(SceneFlag.MaraSickDayAfterMatch);
			Global.watched(SceneFlag.MaraSickDayInterrogation);
			Global.watched(SceneFlag.MaraSickDayLunch);
			Global.watched(SceneFlag.MaraSickDayShower);
			Global.watched(SceneFlag.MaraSickDayFinale);
		}
		if (Global.checkFlag(Flag.Yui)) {
			Global.watched(SceneFlag.YuiFirstAfterMatch);
			Global.watched(SceneFlag.YuiFirstUndress);
			Global.watched(SceneFlag.YuiFirstLick);
			Global.watched(SceneFlag.YuiFirstService);
			Global.watched(SceneFlag.YuiFirstSex);
		}
		if (Global.getValue(Flag.GangRank) > 0) {
			Global.watched(SceneFlag.EveInititation);
		}
	}

	/**
	 * Use legacy loader to see if the save was created before the JSON switch
	 *
	 * @param file The file to load from
	 */
	private static void load_legacy(FileInputStream file) {
		String header;
		var dawn = false;

		var loader = new Scanner(file);
		while (loader.hasNext()) {
			header = loader.next();
			if (header.equalsIgnoreCase("P")) {
				Global.getPlayer().load(loader);
			}
			else if (header.equalsIgnoreCase("NPC")) {
				var name = loader.next();
				Roster.get(name).load(loader);
			}
			else if (header.equalsIgnoreCase("Flags")) {
				var next = loader.next();
				while (!next.equals("#")) {
					Global.flag(Flag.valueOf(next));
					next = loader.next();
				}
				dawn = loader.next().equalsIgnoreCase("Dawn");
				if (loader.hasNext()) {
					Scheduler.setDate(loader.nextInt());
				}
				else {
					Scheduler.setDate(1);
				}
				while (loader.hasNext()) {
					Global.setCounter(Flag.valueOf(loader.next()), Float.parseFloat(loader.next()));
				}
			}
		}
		loader.close();

		if (dawn) {
			Scheduler.setTime(LocalTime.of(15, 0));
		}
		else {
			Scheduler.setTime(LocalTime.of(22, 0));
		}
	}
}
