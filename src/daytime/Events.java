package daytime;

import characters.Character;
import global.Global;
import scenes.AngelEvent;
import scenes.Event;
import scenes.MaraEvent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Defines a handler for events that should be played during daytime
 */
public class Events {
	private final ArrayList<HashMap<String, Integer>> available;
	private final ArrayList<Event> scenes;

	public Events(Character player, Daytime day) {
		scenes = new ArrayList<>();
		available = new ArrayList<>();
		scenes.add(new Threesomes(player));
		scenes.add(new MaraEvent(player));
		scenes.add(new AngelEvent());
		scenes.add(new MorningEvents(player, day));

		// Get all scenes that are currently available
		for (var i = 0; i < scenes.size(); i++) {
			available.add(new HashMap<>());
			scenes.get(i).addAvailable(available.get(i));
		}
	}

	/**
	 * Checks for mandatory scenes to play at the start of a new day
	 *
	 * @return True if an event requiring user input was started,
	 * preventing the regular daytime planning from continuing
	 */
	public boolean checkMorning() {
		String sceneName;
		for (var event : scenes) {
			sceneName = event.morning();
			if (!sceneName.equals("")) {
				return event.play(sceneName);
			}
		}
		return false;
	}

	/**
	 * Checks for mandatory and optional scenes that can be triggered
	 *
	 * @return True if an event requiring user input was started,
	 * preventing the regular daytime planning from continuing
	 */
	public boolean checkScenes() {
		String sceneName;
		for (var event : scenes) {
			sceneName = event.mandatory();
			if (!sceneName.equals("")) {
				scenes.get(1).play(sceneName);
				return true;
			}
		}

		// Pick an optional scene
		available.clear();
		for (var i = 0; i < scenes.size(); i++) {
			available.add(new HashMap<>());
			scenes.get(i).addAvailable(available.get(i));
		}
		for (var i = 0; i < scenes.size(); i++) {
			for (var scene : available.get(i).keySet()) {
				if (Global.random(100) < available.get(i).get(scene)) {
					return scenes.get(i).play(scene);
				}
			}
		}
		return false;
	}
}
