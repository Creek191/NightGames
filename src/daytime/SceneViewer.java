package daytime;

import characters.Character;
import characters.ID;
import global.Global;
import global.Roster;
import scenes.SceneFlag;
import scenes.SceneManager;
import scenes.SceneType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Defines an activity to rewatch previously seen scenes
 */
public class SceneViewer extends Activity {
	private final Activity parent;
	private final List<SceneFlag> watched;
	private final HashSet<SceneType> categories;
	private final ArrayList<SceneFlag> availableScenes;
	private ID selected;
	private ID target;
	private SceneType category;

	@Override
	public boolean known() {
		return false;
	}

	public SceneViewer(Character player, Activity parent) {
		super("Scene Viewer", player);
		this.parent = parent;
		watched = Global.getAllWatched();
		categories = new HashSet<>();
		availableScenes = new ArrayList<>();
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		if (choice.equalsIgnoreCase("Start")
				|| choice.equalsIgnoreCase("Back")) {
			// Select an NPC
			Global.gui().message(
					"You review the match footage you were sent. You can revist some of the highlights of your sexual escapades here.");
			selected = null;
			category = null;
			target = null;
			availableScenes.clear();
			categories.clear();

			watched.stream().map(SceneFlag::getStar)
					.distinct()
					.forEachOrdered(actor -> Global.gui().choose(this, Roster.get(actor).name()));
			Global.gui().choose(parent, "Back");
		}
		else if (selected == null) {
			// Select scene category
			selected = ID.fromString(choice);
			categories.clear();
			var count = 0;
			for (var flag : watched) {
				if (flag.getStar() == selected) {
					count++;
					categories.add(flag.getType());
				}
			}
			for (var type : categories) {
				Global.gui().choose(this, type.toString());
			}
			Global.gui().choose(this, "Back");
			Global.gui().message("Scenes featuring: " + choice);
			Global.gui().message(count + " total scenes available.");
		}
		else if (category == null) {
			category = SceneType.valueOf(choice);
			availableScenes.clear();
			watched.stream()
					.filter(flag -> flag.getStar() == selected && flag.getType() == category)
					.forEachOrdered(availableScenes::add);
			Global.gui().message(category + " scenes featuring " + Roster.get(selected).name() + ".");
			Global.gui().message(availableScenes.size() + " of " + SceneManager.getTotalCount(selected,
					category) + " seen.");
			if (category == SceneType.INTERVENTION) {
				// Select second character in scene
				Global.gui().message("Choose the other person to appear in the scene.");
				for (var npc : Roster.getCombatants()) {
					if (npc.id() != selected && npc.id() != ID.PLAYER) {
						Global.gui().choose(this, npc.name());
					}
				}
			}
			else {
				// Select actual scene
				for (var flag : availableScenes) {
					Global.gui().choose(this, flag.getLabel());
				}
			}
			Global.gui().choose(this, "Back");
		}
		else if (category == SceneType.INTERVENTION && target == null) {
			// Select actual scene
			target = ID.fromString(choice);
			availableScenes.clear();
			for (var flag : watched) {
				if (flag.getStar() == selected && flag.getType() == category) {
					availableScenes.add(flag);
				}
			}
			Global.gui().message(category.toString() + " scenes featuring " + Roster.get(selected).name() + ".");
			Global.gui().message(availableScenes.size() + " of " + SceneManager.getTotalCount(selected,
					category) + " seen.");
			Global.gui().message("Additional appearance by: " + Roster.get(target).name() + ".");
			for (var flag : availableScenes) {
				Global.gui().choose(this, flag.getLabel());
			}
			Global.gui().choose(this, "Back");
		}
		else {
			// Play selected scene
			availableScenes.stream()
					.filter(flag -> choice.equalsIgnoreCase(flag.getLabel()))
					.forEachOrdered(flag -> SceneManager.play(flag, Roster.get(target)));
			Global.gui().choose(this, "Back");
		}
	}

	@Override
	public void shop(Character npc, int budget) {
	}
}
