package daytime;

import characters.Character;
import global.Global;
import global.Scheduler;

import java.time.LocalTime;
import java.util.HashMap;

/**
 * Defines an activity characters can visit during the daytime before matches
 */
public abstract class Activity {
	protected String name;
	protected LocalTime time;
	protected Character player;
	protected String tooltip;
	private final HashMap<String, Integer> randomScenes;

	public Activity(String name, Character player) {
		this.name = name;
		this.time = LocalTime.of(1, 0);
		this.player = player;
		this.randomScenes = new HashMap<>();
		this.tooltip = "";
	}

	public Activity(String name, Character player, String tooltip) {
		this(name, player);
		this.tooltip = tooltip;
	}

	/**
	 * Checks whether the player knows about the activity
	 */
	public abstract boolean known();

	/**
	 * Visits the activity and begins the scene chosen
	 *
	 * @param choice The name of the chosen scene
	 */
	public abstract void visit(String choice);

	/**
	 * Gets the time the activity takes to complete
	 */
	public LocalTime time() {
		return time;
	}

	/**
	 * Completes the activity
	 *
	 * @param acted Whether the player spent time doing the activity
	 */
	public void done(boolean acted) {
		if (acted) {
			Scheduler.advanceTime(time);
		}
		Scheduler.getDay().plan();
	}

	/**
	 * Gets the name of the activity
	 */
	public String toString() {
		return name;
	}

	/**
	 * Processes the NPC-variant of the activity
	 *
	 * @param npc    The NPC calling the activity
	 * @param budget The budget usable by the character to make purchases
	 */
	public abstract void shop(Character npc, int budget);

	/**
	 * Clears the list of random scenes
	 */
	public void clearRandomScenes() {
		randomScenes.clear();
	}

	/**
	 * Adds a scene to the list
	 *
	 * @param scene  The name of the scene
	 * @param weight The weight used to determine how likely the scene is to be chosen
	 */
	public void addRandomScene(String scene, int weight) {
		randomScenes.put(scene, weight);
	}

	/**
	 * Randomly picks a scene from the list
	 *
	 * @return The name of the picked scene, or an empty string if the list is empty
	 */
	public String getRandomScene() {
		if (randomScenes.isEmpty()) {
			return "";
		}
		else {
			return Global.pickRandom(randomScenes.keySet().toArray(), randomScenes.values().toArray(new Integer[0]));
		}
	}

	public String tooltip() {
		return tooltip;
	}
}
