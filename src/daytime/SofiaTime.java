package daytime;

import characters.Character;
import characters.Dummy;
import characters.Emotion;
import characters.ID;
import combat.Combat;
import global.Flag;
import global.Global;
import global.Modifier;
import global.Roster;

import java.util.List;

/**
 * Defines an activity to spend time with Sofia
 */
public class SofiaTime extends Activity {
	private final Character sofia;
	private final Dummy sprite;

	public SofiaTime(Character player) {
		super("Sofia", player);
		sofia = Roster.get(ID.SOFIA);
		sprite = new Dummy("Sofia", 1, true);
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		sprite.dress();
		sprite.setCostumeLevel(1);
		sprite.setBlush(0);
		sprite.setMood(Emotion.confident);
		if (choice.equalsIgnoreCase("Start")) {
			Global.gui().choose(this, "Leave");
			// TODO: Disabled
			//promptScenes();
		}
		else if (choice.equalsIgnoreCase("Practice Fight")) {
			var fight = new Combat(player, sofia, List.of(Modifier.practice));
			fight.setParent(this);
			fight.go();
		}
		else if (choice.equalsIgnoreCase("PostCombat")) {
			Global.gui().message(
					"With flushed faces and lingering pleasure, you clean up after your practice fight and get dressed.");
			player.rest();
			sofia.rest();
			Global.gui().choose(this, "Leave");
		}
		else if (choice.equalsIgnoreCase("Leave")) {
			Global.gui().showNone();
			Global.setCounter(Flag.SofiaDWV, 0);
			done(true);
		}
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.Sofia);
	}

	private void promptScenes() {
		Global.gui().choose(this, "Practice Fight", "Have a sex-fight off the record.");
	}

	@Override
	public void shop(Character npc, int budget) {
		Roster.gainAffection(npc.id(), ID.SOFIA, 1);
	}
}
