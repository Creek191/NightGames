package daytime;

import characters.Character;
import global.Global;
import items.Clothing;
import items.Item;

import java.util.HashMap;

/**
 * Defines a base activity for stores to support purchasing items
 */
public abstract class Store extends Activity {
	private final HashMap<Item, Integer> itemStock;
	private final HashMap<Clothing, Integer> clothingStock;
	protected boolean acted;

	public Store(String name, Character player) {
		super(name, player);
		itemStock = new HashMap<>();
		clothingStock = new HashMap<>();
		acted = false;
	}

	@Override
	public abstract boolean known();

	@Override
	public abstract void visit(String choice);

	public void add(Item item) {
		itemStock.put(item, item.getPrice());
	}

	public void add(Clothing item) {
		clothingStock.put(item, item.getPrice());
	}

	public HashMap<Item, Integer> items() {
		return itemStock;
	}

	public HashMap<Clothing, Integer> clothing() {
		return clothingStock;
	}

	/**
	 * Prints the buttons for purchasing the stocked items and clothes
	 */
	protected void displayGoods() {
		itemStock.keySet().forEach(item -> Global.gui().sale(this, item));
		clothingStock.keySet().stream()
				.filter(clothing -> !player.has(clothing))
				.forEachOrdered(clothing -> Global.gui().sale(this, clothing));
	}

	/**
	 * Attempts to buy the item or clothing with the specified name
	 *
	 * @param name The name of the item or clothing to buy
	 * @return True if the item was found in the stock, otherwise False
	 */
	protected boolean checkSale(String name) {
		for (var item : itemStock.keySet()) {
			if (name.equals(item.getName())) {
				buy(item);
				return true;
			}
		}
		for (var clothing : clothingStock.keySet()) {
			if (name.equals(clothing.getProperName())) {
				buy(clothing);
				return true;
			}
		}
		return false;
	}

	/**
	 * Buys the specified item
	 *
	 * @param item The item to buy
	 */
	private void buy(Item item) {
		if (player.money >= itemStock.get(item)) {
			player.money -= itemStock.get(item);
			player.gain(item);
			acted = true;
			Global.gui().refreshLite();
		}
		else {
			Global.gui().message("You don't have enough money to purchase that.");
		}
	}

	/**
	 * Buys the specified article of clothing
	 *
	 * @param item The clothing to buy
	 */
	private void buy(Clothing item) {
		if (player.money >= clothingStock.get(item)) {
			player.money -= clothingStock.get(item);
			player.gain(item);
			acted = true;
			Global.gui().refreshLite();
		}
		else {
			Global.gui().message("You don't have enough money to purchase that.");
		}
	}
}
