package daytime;

import characters.Attribute;
import characters.Character;
import characters.NPC;
import characters.Player;
import global.*;

import java.util.ArrayList;

/**
 * Defines the daytime logic handler
 */
public class Daytime {
	private static final int NPCDAYTIME = 8;
	private final Player player;
	private final Events auto;
	private ArrayList<Activity> activities;

	public Daytime(Player player) {
		Global.gui().clearText();
		this.player = player;
		manageFlags();
		buildActivities();
		this.auto = new Events(player, this);
		if (auto.checkMorning()) {
			return;
		}
		plan();
	}

	/**
	 * Trains the specified characters in an attribute
	 * <p>
	 * Training is slightly easier if the other character has a higher attribute
	 *
	 * @param one The first character
	 * @param two The second character
	 * @param att The attribute to train
	 */
	public static void train(Character one, Character two, Attribute att) {
		var chance = 100 - (2 * one.getEffective(Attribute.Perception));
		if (one.getPure(att) < two.getPure(att)) {
			chance = 90 - (2 * one.getEffective(Attribute.Perception));
		}

		if (Global.random(100) >= chance) {
			one.mod(att, 1);
			if (one.human()) {
				Global.gui().message("<b>Your " + att + " has improved.</b>");
			}
		}
	}

	/**
	 * Plans the next daytime activity, or proceeds to dusk
	 */
	public void plan() {
		if (Scheduler.getTime().getHour() < 22) {
			if (Scheduler.getDayString(Scheduler.getDate()).equalsIgnoreCase("Sunday")) {
				Global.gui().message("It is currently " + Scheduler.getTimeString() + ". There is no match tonight.");
			}
			else if (Scheduler.hasMatch(player)) {
				Global.gui().message("It is currently " + Scheduler.getTimeString() + ". Your next match starts at 10:00pm.");
			}
			else {
				Global.gui().message("It is currently " + Scheduler.getTimeString() + ". You have tonight off.");
			}
			Global.gui().refresh();
			Global.gui().clearCommand();
			Global.gui().showNone();

			// Check for pending scenes
			if (auto.checkScenes()) {
				return;
			}

			// Show available activities
			activities.stream()
					.filter(Activity::known)
					.forEachOrdered(activity -> Global.gui().addActivity(activity));
		}
		else {
			// Process the NPCs' daytime activities
			for (var npc : Roster.getExisting()) {
				if (npc.human()) {
					continue;
				}

				if (npc.getLevel() >= 10 * (npc.getRank() + 1)) {
					npc.rankUp();
				}
				((NPC) npc).daytime(NPCDAYTIME, this);
			}
			if (Global.checkFlag(Flag.autosave)) {
				SaveManager.save(true);
			}
			Scheduler.dusk();
		}
	}

	/**
	 * Let's the specified NPC visit an activity to purchase items or improve attributes/relations
	 *
	 * @param name   The name of the activity
	 * @param npc    The NPC that visits the activity
	 * @param budget The budget the NPC has to spend on the activity
	 */
	public void visit(String name, Character npc, int budget) {
		activities.stream()
				.filter(act -> act.toString().equalsIgnoreCase(name))
				.findFirst()
				.ifPresent(activity -> activity.shop(npc, budget));
	}

	private void buildActivities() {
		activities = new ArrayList<>();
		activities.add(new Exercise(player));
		activities.add(new Porn(player));
		activities.add(new VideoGames(player));
		activities.add(new Informant(player));
		activities.add(new BlackMarket(player));
		activities.add(new XxxStore(player));
		activities.add(new HWStore(player));
		activities.add(new Bookstore(player));
		activities.add(new Dojo(player));
		activities.add(new AngelTime(player));
		activities.add(new CassieTime(player));
		activities.add(new JewelTime(player));
		activities.add(new MaraTime(player));
		activities.add(new KatTime(player));
		activities.add(new SamanthaTime(player));
		activities.add(new ReykaTime(player));
		activities.add(new PlayerRoom(player));
		activities.add(new ClothingStore(player));
		activities.add(new MagicTraining(player));
		activities.add(new Workshop(player));
		activities.add(new YuiTime(player));
		activities.add(new GinetteTime(player));
		activities.add(new ValerieTime(player));
		activities.add(new EveTime(player));
		activities.add(new Specialize(player));
	}

	private void manageFlags() {
		if (Global.checkFlag(Flag.metAlice)) {
			if (Global.checkFlag(Flag.victory)) {
				Global.unflag(Flag.AliceAvailable);
			}
			else {
				Global.flag(Flag.AliceAvailable);
			}
		}
		if (Global.checkFlag(Flag.metYui)) {
			Global.flag(Flag.YuiAvailable);
		}
		if (Global.checkFlag(Flag.threesome)) {
			Global.unflag(Flag.threesome);
		}
	}
}
