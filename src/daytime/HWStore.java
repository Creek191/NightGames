package daytime;

import characters.Character;
import global.Flag;
import global.Global;
import items.Component;

/**
 * Defines the Hardware Store activity
 */
public class HWStore extends Store {
	public HWStore(Character player) {
		super("Hardware Store", player);
		add(Component.Tripwire);
		add(Component.Rope);
		add(Component.Spring);
		add(Component.Sprayer);
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.basicStores);
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		if (choice.equalsIgnoreCase("Start")) {
			acted = false;
		}
		else if (choice.equalsIgnoreCase("Leave")) {
			done(acted);
			return;
		}

		checkSale(choice);
		Global.gui().message(
				"Nothing at the hardware store is designed for the sort of activities you have in mind, but there are components you could use to make some " +
						"effective traps.");
		for (var item : items().keySet()) {
			Global.gui().message(item.getName() + ": $" + item.getPrice());
		}
		Global.gui().message("You have: $" + player.money + " available to spend.");
		displayGoods();
		Global.gui().choose(this, "Leave");
	}

	@Override
	public void shop(Character npc, int budget) {
		var remaining = Math.min(budget, 140);
		var bored = 0;

		// Purchase items
		while (remaining > 10 && bored < 10) {
			for (var item : items().keySet()) {
				if (remaining > item.getPrice() && !npc.has(item, 20)) {
					npc.gain(item);
					npc.money -= item.getPrice();
					remaining -= item.getPrice();
				}
				else {
					bored++;
				}
			}
		}
	}
}
