package combat;

import characters.Anatomy;
import skills.Skill;

/**
 * Defines an event that occurred during combat
 */
public class CombatEvent {
	private final Result event;
	private final Skill skill;
	private final Anatomy part;

	public CombatEvent(Result event) {
		this(event, null, null);
	}

	public CombatEvent(Result event, Skill skill) {
		this(event, skill, null);
	}

	public CombatEvent(Result event, Anatomy part) {
		this(event, null, part);
	}

	private CombatEvent(Result event, Skill skill, Anatomy part) {
		this.event = event;
		this.skill = skill;
		this.part = part;
	}

	/**
	 * Gets the event that occurred
	 */
	public Result getEvent() {
		return event;
	}

	/**
	 * Gets the skill that was used
	 *
	 * @return The used skill, or Null if none was set
	 */
	public Skill getSkill() {
		return skill;
	}

	/**
	 * Gets the part that was affected
	 *
	 * @return The affected part, or Null if none was set
	 */
	public Anatomy getPart() {
		return part;
	}
}
