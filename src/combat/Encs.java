package combat;

/**
 * Defines the available choices during an encounter
 */
public enum Encs {
	ambush,
	capitalize,
	showerattack,
	aphrodisiactrick,
	stealclothes,
	fight,
	flee,
	wait,
	smokebomb,
	watch,
}
