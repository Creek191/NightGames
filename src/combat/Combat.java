package combat;

import areas.Area;
import characters.Character;
import characters.*;
import daytime.Activity;
import global.Flag;
import global.Global;
import global.Modifier;
import items.Clothing;
import items.ClothingType;
import items.Item;
import items.Potion;
import pet.Pet;
import skills.Skill;
import skills.SkillTag;
import stance.Neutral;
import stance.Position;
import stance.Stance;
import stance.StandingOver;
import status.Protected;
import status.Status;
import status.Winded;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

public class Combat extends Observable implements Serializable, Cloneable {
	/**
	 *
	 */
	private static final long serialVersionUID = -8279523341570263846L;
	public Character p1;
	public Character p2;
	public int phase;
	private final ArrayList<Character> audience;
	private final List<Modifier> rules;
	private HashMap<Character, Skill> actions;
	private HashMap<Character, List<CombatEvent>> events;

	public Area location;
	private String message;
	public Position stance;
	public ArrayList<Clothing> clothespile;
	public HashMap<Character, ArrayList<Item>> used;
	private int timer;
	public Result state;
	private HashMap<String, String> images;
	private boolean end;
	private Activity parentActivity;

	public Combat(Character p1, Character p2, List<Modifier> rules) {
		this.p1 = p1;
		this.p2 = p2;
		this.rules = rules;
		end = false;
		stance = new Neutral(p1, p2);
		message = "";
		audience = new ArrayList<>();
		clothespile = new ArrayList<>();
		used = new HashMap<>();
		used.put(p1, new ArrayList<>());
		used.put(p2, new ArrayList<>());
		actions = new HashMap<>();
		events = new HashMap<>();
		events.put(p1, new ArrayList<>());
		events.put(p2, new ArrayList<>());
		timer = 0;
		images = new HashMap<>();
		p1.state = State.combat;
		p2.state = State.combat;
		p1.preCombat(p2, this);
		p2.preCombat(p1, this);
	}

	/**
	 * Begin combat
	 */
	public void go() {
		phase = 0;
		if (!(p1.human() || p2.human())) {
			automate();
		}
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * Process a single combat turn
	 */
	public void turn() {
		// Check for draw
		if (p1.getArousal().isFull() && p2.getArousal().isFull()) {
			state = eval(null);
			p1.evalChallenges(this, null);
			p2.evalChallenges(this, null);
			p2.draw(this, state);
			for (var viewer : audience) {
				if (Global.random(2) == 0) {
					viewer.watcher(this, p1, p2);
				}
				else {
					viewer.watcher(this, p2, p1);
				}
			}
			phase = 2;
			this.setChanged();
			this.notifyObservers();
			if (!(p1.human() || p2.human())) {
				end();
			}
			return;
		}

		// Check for P1 loss
		if (p1.getArousal().isFull()) {
			state = eval(p2);
			p1.evalChallenges(this, p2);
			p2.evalChallenges(this, p2);
			p2.victory(this, state);
			for (var viewer : audience) {
				viewer.watcher(this, p2, p1);
			}
			phase = 2;
			this.setChanged();
			this.notifyObservers();
			if (!(p1.human() || p2.human())) {
				end();
			}
			return;
		}

		// Check for P2 loss
		if (p2.getArousal().isFull()) {
			state = eval(p1);
			p1.evalChallenges(this, p1);
			p2.evalChallenges(this, p1);
			p1.victory(this, state);
			for (var viewer : audience) {
				viewer.watcher(this, p1, p2);
			}
			phase = 2;
			this.setChanged();
			this.notifyObservers();
			if (!(p1.human() || p2.human())) {
				end();
			}
			return;
		}

		// Set combatant descriptions
		message = p2.describe(p1.getEffective(Attribute.Perception)) + "<p>"
				+ stance.describe() + "<p>"
				+ p1.describe(p2.getEffective(Attribute.Perception)) + "<p>";

		// Check for NPC comments
		if (p1.human() || p2.human()) {
			var commenter = (NPC) getOther(Global.getPlayer());
			var comment = commenter.getComment(this);
			if (comment != null) {
				write(commenter, comment);
			}
		}

		phase = 1;
		p1.regen(true);
		p2.regen(true);
		actions.put(p1, null);
		actions.put(p2, null);
		events.get(p1).clear();
		events.get(p2).clear();
		p1.act(this);
		p2.act(this);

		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * Get the opponent of the specified character
	 *
	 * @param player The character to get the opponent of
	 */
	public Character getOther(Character player) {
		if (p1 == player) {
			return p2;
		}
		else {
			return p1;
		}
	}

	/**
	 * Get combat result (based on stance at end of combat)
	 *
	 * @param victor The character that won the fight
	 */
	public Result eval(Character victor) {
		if (victor != null) {
			if ((stance.en == Stance.anal || stance.en == Stance.analm)) {
				return Result.anal;
			}
			if ((stance.en == Stance.facesitting)) {
				return Result.facesitting;
			}
		}
		if (stance.penetration(p1) || stance.penetration(p2)) {
			return Result.intercourse;
		}
		return Result.normal;
	}

	/**
	 * Execute a character's action
	 *
	 * @param c      The character executing the action
	 * @param action The action to execute
	 */
	public void act(Character c, Skill action) {
		actions.put(c, action);
		if (actions.get(p1) == null || actions.get(p2) == null) {
			return;
		}

		clear();
		if (Global.debug) {
			System.out.println(p1.name() + " uses " + actions.get(p1).toString());
			System.out.println(p2.name() + " uses " + actions.get(p2).toString());
		}

		// Run pet actions
		if (p1.pet != null && p2.pet != null) {
			petBattle(p1.pet, p2.pet);
		}
		else if (p1.pet != null) {
			p1.pet.act(this, p2);
		}
		else if (p2.pet != null) {
			p2.pet.act(this, p1);
		}

		if (p1.getInitiative(this) + actions.get(p1).speed() >= p2.getInitiative(this) + actions.get(p2).speed()) {
			// P1 goes first
			if (actions.get(p1).usable(this, p2)) {
				actions.get(p1).resolve(this, p2);
				addEvent(p1, new CombatEvent(Result.useskill, actions.get(p1)));
				addEvent(p2, new CombatEvent(Result.receiveskill, actions.get(p1)));
				checkStamina(p2);
			}
			if (actions.get(p2).usable(this, p1)) {
				actions.get(p2).resolve(this, p1);
				addEvent(p2, new CombatEvent(Result.useskill, actions.get(p2)));
				addEvent(p1, new CombatEvent(Result.receiveskill, actions.get(p2)));
				checkStamina(p1);
			}
		}
		else {
			// P2 goes first
			if (actions.get(p2).usable(this, p1)) {
				actions.get(p2).resolve(this, p1);
				addEvent(p2, new CombatEvent(Result.useskill, actions.get(p2)));
				addEvent(p1, new CombatEvent(Result.receiveskill, actions.get(p2)));
				checkStamina(p1);
			}
			if (actions.get(p1).usable(this, p2)) {
				actions.get(p1).resolve(this, p2);
				addEvent(p1, new CombatEvent(Result.useskill, actions.get(p1)));
				addEvent(p2, new CombatEvent(Result.receiveskill, actions.get(p1)));
				checkStamina(p2);
			}
		}

		// End-of-turn cleanup
		p1.endOfTurn(this, p2);
		p2.endOfTurn(this, p1);
		stance.decay();
		stance.executeOngoing(this);

		// Check for NPC comments
		if (p1.human() || p2.human()) {
			var commenter = (NPC) getOther(Global.getPlayer());
			var comment = commenter.getResponse(this);
			if (comment != null) {
				write(commenter, comment);
			}
		}

		this.phase = 0;
		if (!(p1.human() || p2.human())) {
			timer++;
			turn();
		}
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * Automate combat between NPCs
	 */
	public void automate() {
		// Run until either character reaches max arousal
		while (!(p1.getArousal().isFull() || p2.getArousal().isFull())) {
			phase = 1;
			p1.regen(true);
			p2.regen(true);
			events.get(p1).clear();
			events.get(p2).clear();
			actions.put(p1, ((NPC) p1).actFast(this));
			actions.put(p2, ((NPC) p2).actFast(this));
			clear();
			if (Global.debug) {
				System.out.println(p1.name() + " uses " + actions.get(p1).toString());
				System.out.println(p2.name() + " uses " + actions.get(p2).toString());
			}

			// Run pet actions
			if (p1.pet != null && p2.pet != null) {
				petBattle(p1.pet, p2.pet);
			}
			else if (p1.pet != null) {
				p1.pet.act(this, p2);
			}
			else if (p2.pet != null) {
				p2.pet.act(this, p1);
			}

			if (p1.getInitiative(this) + actions.get(p1).speed() >= p2.getInitiative(this) + actions.get(p2).speed()) {
				// P1 goes first
				if (actions.get(p1).usable(this, p2)) {
					actions.get(p1).resolve(this, p2);
					checkStamina(p2);
				}
				if (actions.get(p2).usable(this, p1)) {
					actions.get(p2).resolve(this, p1);
					checkStamina(p1);
				}
			}
			else {
				// P2 goes first
				if (actions.get(p2).usable(this, p1)) {
					actions.get(p2).resolve(this, p1);
					checkStamina(p1);
				}
				if (actions.get(p1).usable(this, p2)) {
					actions.get(p1).resolve(this, p2);
					checkStamina(p2);
				}
			}

			// End-of-turn cleanup
			p1.endOfTurn(this, p2);
			p2.endOfTurn(this, p1);
			stance.decay();
			stance.executeOngoing(this);
			this.phase = 0;
		}

		// Check for draw
		if (p1.willOrgasm(this) && p2.willOrgasm(this)) {
			state = eval(null);
			p1.evalChallenges(this, null);
			p2.evalChallenges(this, null);
			p2.draw(this, state);
			for (var viewer : audience) {
				if (Global.random(2) == 0) {
					viewer.watcher(this, p1, p2);
				}
				else {
					viewer.watcher(this, p2, p1);
				}
			}
		}
		// Check for P1 loss
		else if (p1.willOrgasm(this)) {
			state = eval(p2);
			p1.evalChallenges(this, p2);
			p2.evalChallenges(this, p2);
			p2.victory(this, state);
			for (var viewer : audience) {
				viewer.watcher(this, p2, p1);
			}
		}
		else {
			// P2 has lost
			state = eval(p1);
			p1.evalChallenges(this, p1);
			p2.evalChallenges(this, p1);
			p1.victory(this, state);
			for (var viewer : audience) {
				viewer.watcher(this, p1, p2);
			}
		}

		end();
	}

	/**
	 * Clear text log
	 */
	public void clear() {
		message = "";
	}

	/**
	 * Append line to text log
	 *
	 * @param text Text to append to the log
	 */
	public void write(String text) {
		message += "<br>" + text;
	}

	/**
	 * Append line spoken by the specified character
	 *
	 * @param user Character speaking the line
	 * @param text Text to append to the log
	 */
	public void write(Character user, String text) {
		if (user.human()) {
			message += "<br><font color='rgb(100,100,200)'>" + text + "<font color='black'><br>";
		}
		else {
			message += "<br><font color='rgb(200,100,100)'>" + text + "<font color='black'><br>";
		}
	}

	/**
	 * Writes a status report to the text log.
	 * Will do nothing if reports are disabled
	 *
	 * @param text Text to append to the log
	 */
	public void report(String text) {
		if (!Global.checkFlag(Flag.noReports)) {
			message += "<font color='rgb(100,100,100)'>" + text + "<font color='black'><br>";
		}
	}

	/**
	 * Gets the current text log
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Consumes an item from the specified character's inventory
	 *
	 * @param player The character using the item
	 * @param item   The item that is used
	 */
	public void drop(Character player, Item item) {
		used.get(player).add(item);
		player.consume(item, 1);
	}

	/**
	 * Check the specified characters stamina to apply exhaustion related effects
	 *
	 * @param p The character to check
	 */
	public void checkStamina(Character p) {
		if (!p.getStamina().isEmpty()) {
			return;
		}

		// Automatically use energy drink via PlanB trait
		if (p.has(Trait.planB) && (p.has(Potion.EnergyDrink) || p.has(Potion.SuperEnergyDrink))) {
			if (p.has(Potion.EnergyDrink)) {
				p.consume(Potion.EnergyDrink, 1);
			}
			else {
				p.consume(Potion.SuperEnergyDrink, 1);
			}
			if (p.human()) {
				write("Your legs feel weak, but you pop open an energy drink and down it to recover.");
			}
			else {
				write(p.name() + " pulls a hidden energy drink out of her sleeve and chugs it. "
						+ "It seems to be enough to keep her on her feet.");
			}
			p.heal(p.getStamina().max() / 20, this);
			return;
		}

		// Apply winded and protected status
		var other = getOther(p);
		p.add(new Winded(p, other));
		p.add(new Protected(p));
		if (stance.prone(p)) {
			return;
		}

		// Write appropriate message based on stance and most recent actions
		if ((stance.penetration(p) || stance.penetration(other)) && stance.dom(other)) {
			if (p.human()) {
				write("Your legs give out, but " + other.name() + " holds you up.");
			}
			else {
				write(p.name() + " slumps in your arms, but you support her to keep her from collapsing.");
			}
		}
		else {
			stance = new StandingOver(other, p);
			if (p.human()) {
				var lowblow = false;
				for (var event : events.get(p)) {
					if (event.getEvent() == Result.receivepain
							&& event.getPart() == Anatomy.genitals) {
						lowblow = true;
						break;
					}
				}
				if (lowblow) {
					write("You crumple into the fetal position, cradling your sore testicles.");
				}
				else {
					write("You don't have the strength to stay on your feet. You slump to the floor.");
				}
			}
			else {
				var lowblow = false;
				for (var event : events.get(p)) {
					if (event.getEvent() == Result.receivepain
							&& event.getPart() == Anatomy.genitals) {
						lowblow = true;
						break;
					}
				}
				if (lowblow) {
					if (p.hasBalls()) {
						write(p.name() + " falls to the floor with tears in her eyes, clutching her plums.");
					}
					else {
						write(p.name() + " falls to the floor with tears in her eyes, holding her sensitive girl parts.");
					}
				}
				else {
					write(p.name() + " drops to the floor, exhausted.");
				}
				if (!Global.checkFlag(Flag.exactimages) || p.id() == ID.SOFIA) {
					offerImage("Sofia busted.jpg", "Art by AimlessArt");
				}
			}
		}
	}

	/**
	 * Run next turn or end combat
	 */
	public void next() {
		if (phase == 0) {
			turn();
		}
		else if (phase == 2) {
			end();
		}
	}

	public void intervene(Character intruder, Character assist) {
		Character target;
		if (p1 == assist) {
			target = p2;
		}
		else {
			target = p1;
		}
		p1.undress(this);
		p2.undress(this);
		if (intruder.human()) {
			Global.gui().watchCombat(this);
			Global.gui().loadTwoPortraits(p1, p2);
		}
		else if (p1.human()) {
			Global.gui().loadTwoPortraits(p2, intruder);
		}
		else if (p2.human()) {
			Global.gui().loadTwoPortraits(p1, intruder);
		}
		if (target.resist3p(this, intruder, assist)) {
			target.gainXP(20 + target.lvlBonus(intruder));
			intruder.gainXP(10 + intruder.lvlBonus(target));
			intruder.getArousal().empty();
			if (intruder.has(Trait.insatiable)) {
				intruder.getArousal().restore((int) (intruder.getArousal().max() * .2));
			}
			intruder.undress(this);
			if (clothespile.contains(intruder.outfit[1].firstElement())) {
				target.gain(intruder.getUnderwear());
			}
			intruder.defeated(target);
			intruder.defeated(assist);
		}
		else {
			intruder.intervene3p(this, target, assist);
			assist.victory3p(this, target, intruder);
			phase = 2;
			if (!(p1.human() || p2.human() || intruder.human())) {
				end();
			}
			else if (intruder.human()) {
				Global.gui().watchCombat(this);
			}
		}
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * End combat, return items and give out XP
	 *
	 * @return True if either character has leveled up, otherwise false
	 */
	public boolean end() {
		clear();
		for (var character : used.keySet()) {
			for (var item : used.get(character)) {
				character.gain(item, 1);
			}
		}

		// Clean up temporary status, summons, etc.
		p1.state = State.ready;
		p2.state = State.ready;
		p1.endOfBattle();
		p2.endOfBattle();
		end = true;

		var ding = false;

		// Switch to PostCombat-scene (practice) or give out XP
		if (hasModifier(Modifier.practice)) {
			parentActivity.visit("PostCombat");
		}
		else {
			if (p1.getXP() >= 95 + (5 * p1.getLevel())) {
				p1.ding();
				if (p1.human()) {
					ding = true;
				}
			}
			if (p2.getXP() >= 95 + (5 * p2.getLevel())) {
				p2.ding();
				if (p2.human()) {
					ding = true;
				}
			}
		}
		return ding;
	}

	/**
	 * Perform fight between pets
	 *
	 * @param one First pet
	 * @param two Second pet
	 */
	public void petBattle(Pet one, Pet two) {
		var roll1 = Global.random(20) + one.power();
		var roll2 = Global.random(20) + two.power();

		// Give slight advantage to female pet if fighting male
		if (one.gender() == Trait.female && two.gender() == Trait.male) {
			roll1 += 3;
		}
		else if (one.gender() == Trait.male && two.gender() == Trait.female) {
			roll2 += 3;
		}

		// Determine outcome
		if (roll1 > roll2) {
			one.vanquish(this, two);
		}
		else if (roll2 > roll1) {
			two.vanquish(this, one);
		}
		else {
			write(one.ownerText() + one + " and " + two.ownerText() + two + " engage each other for awhile, but neither can gain the upper hand.");
		}
	}

	/**
	 * Clone combat instance
	 *
	 * @return A clone of the combat instance
	 */
	public Combat clone() throws CloneNotSupportedException {
		var c = (Combat) super.clone();
		c.p1 = p1.clone();
		c.p2 = p2.clone();
		c.clothespile = new ArrayList<>();
		c.stance = stance.clone();
		c.used = new HashMap<>();
		c.used.put(c.p1, new ArrayList<>());
		c.used.put(c.p2, new ArrayList<>());
		c.actions = new HashMap<>();
		c.events = new HashMap<>();
		c.events.put(p1, new ArrayList<>());
		c.events.put(p2, new ArrayList<>());
		c.timer = timer;
		c.images = new HashMap<>();
		if (c.stance.top == p1) {
			c.stance.top = c.p1;
		}
		if (c.stance.top == p2) {
			c.stance.top = c.p2;
		}
		if (c.stance.bottom == p1) {
			c.stance.bottom = c.p1;
		}
		if (c.stance.bottom == p2) {
			c.stance.bottom = c.p2;
		}
		return c;
	}

	/**
	 * Gets the last action of the specified character
	 *
	 * @return The skill last used by the specified character, or null if they don't participate in combat
	 */
	public Skill lastAction(Character user) {
		if (user == p1) {
			return actions.get(p1);
		}
		else if (user == p2) {
			return actions.get(p2);
		}
		else {
			return null;
		}
	}

	/**
	 * Add image to pool of possible images for this turn
	 *
	 * @param path   The path to the image
	 * @param artist The artist who created the image
	 */
	public void offerImage(String path, String artist) {
		images.put(path, artist);
	}

	/**
	 * Shows one of the images currently in the pool.
	 * One character must be human and the feature must be enabled
	 */
	public void showImage() {
		var imagePath = "";
		if ((p1.human() || p2.human()) && !Global.checkFlag(Flag.noimage)) {
			if (!images.isEmpty()) {
				imagePath = images.keySet().toArray(new String[0])[Global.random(images.size())];
			}
			if (!imagePath.equals("")) {
				Global.gui().displayImage(imagePath, images.get(imagePath));
			}
		}
		images.clear();
	}

	/**
	 * Clear image pool
	 */
	public void clearImages() {
		images.clear();
	}

	/**
	 * Immediately end combat
	 *
	 * @param player Not used
	 */
	public void forfeit(Character player) {
		end();
	}

	/**
	 * Report pleasure damage taken by specified character
	 *
	 * @param target    The character that took the pleasure damage
	 * @param magnitude The amount of damage taken
	 * @param mod       The type of pleasure taken
	 */
	public void reportPleasure(Character target, int magnitude, Result mod) {
		var type = "pleasure";
		if (mod == Result.foreplay) {
			type = "foreplay pleasure";
		}
		else if (mod == Result.finisher) {
			type = "finishing pleasure";
		}
		if (target.human()) {
			if (target.canReadAdvanced(target)) {
				report(String.format("You receive %d %s.", magnitude, type));
			}
			else if (target.canReadBasic(target)) {
				report(String.format("You are aroused by %s %s.", target.getBasicArousalDamage(magnitude), type));
			}
		}
		else if (getOther(target).human()) {
			if (getOther(target).canReadAdvanced(target)) {
				report(String.format("%s receives %d %s.", target.name(), magnitude, type));
			}
			else if (getOther(target).canReadBasic(target)) {
				report(String.format("%s receives %s %s.",
						target.name(),
						target.getBasicArousalDamage(magnitude),
						type));
			}
		}
	}

	/**
	 * Report temptation damage taken by specified character
	 *
	 * @param target    The character that took the temptation damage
	 * @param magnitude The amount of damage taken
	 * @param mod       The type of temptation taken
	 */
	public void reportTemptation(Character target, int magnitude, Result mod) {
		var type = "temptation";
		if (mod == Result.foreplay) {
			type = "foreplay temptation";
		}
		else if (mod == Result.finisher) {
			type = "finishing temptation";
		}
		if (target.human()) {
			if (target.canReadAdvanced(target)) {
				report(String.format("You are aroused by %d %s.", magnitude, type));
			}
			else if (target.canReadBasic(target)) {
				report(String.format("You are aroused by %s %s.", target.getBasicArousalDamage(magnitude), type));
			}
		}
		else if (getOther(target).human()) {
			if (getOther(target).canReadAdvanced(target)) {
				report(String.format("%s receives %d %s.", target.name(), magnitude, type));
			}
			else if (getOther(target).canReadBasic(target)) {
				report(String.format("%s receives %s %s.",
						target.name(),
						target.getBasicArousalDamage(magnitude),
						type));
			}
		}
	}

	/**
	 * Report pain damage taken by specified character
	 *
	 * @param target    The character that took the pain damage
	 * @param magnitude The amount of damage taken
	 */
	public void reportPain(Character target, int magnitude) {
		if (target.human()) {
			if (target.canReadAdvanced(target)) {
				report(String.format("You take %d pain damage.", magnitude));
			}
			else if (target.canReadBasic(target)) {
				report(String.format("You take %s pain damage.", target.getBasicPain(magnitude)));
			}
		}
		else if (getOther(target).human()) {
			if (getOther(target).canReadAdvanced(target)) {
				report(String.format("%s takes %d pain damage.", target.name(), magnitude));
			}
			else if (getOther(target).canReadBasic(target)) {
				report(String.format("%s takes %s pain damage.", target.name(), target.getBasicPain(magnitude)));
			}
		}
	}

	/**
	 * Report weakening damage taken by specified character
	 *
	 * @param target    The character that took the weakening damage
	 * @param magnitude The amount of damage taken
	 */
	public void reportWeaken(Character target, int magnitude) {
		if (target.human()) {
			if (target.canReadAdvanced(target)) {
				report(String.format("You are weakened by %d damage.", magnitude));
			}
			else if (target.canReadBasic(target)) {
				report(String.format("You are weakened %s.", target.getBasicWeaken(magnitude)));
			}
		}
		else if (getOther(target).human()) {
			if (getOther(target).canReadAdvanced(target)) {
				report(String.format("%s is weakened by %d damage.", target.name(), magnitude));
			}
			else if (getOther(target).canReadBasic(target)) {
				report(String.format("%s is weakened %s.", target.name(), target.getBasicWeaken(magnitude)));
			}
		}
	}

	/**
	 * Report calming effect taken by specified character
	 *
	 * @param target    The character that was calmed
	 * @param magnitude The amount of calming
	 */
	public void reportCalm(Character target, int magnitude) {
		if (target.human()) {
			if (target.canReadAdvanced(target)) {
				report(String.format("You calm down by %d arousal.", magnitude));
			}
			else if (target.canReadBasic(target)) {
				report(String.format("You calm down %s.", target.getBasicCalm(magnitude)));
			}
		}
		else if (getOther(target).human()) {
			if (getOther(target).canReadAdvanced(target)) {
				report(String.format("%s calms down by %d arousal.", target.name(), magnitude));
			}
			else if (getOther(target).canReadBasic(target)) {
				report(String.format("%s calms down %s.", target.name(), target.getBasicCalm(magnitude)));
			}
		}
	}

	/**
	 * Report healing taken by specified character
	 *
	 * @param target    The character that was healed
	 * @param magnitude The amount of healing
	 */
	public void reportHeal(Character target, int magnitude) {
		if (target.human()) {
			if (target.canReadAdvanced(target)) {
				report(String.format("You heal %d stamina damage.", magnitude));
			}
			else if (target.canReadBasic(target)) {
				report(String.format("You heal %s.", target.getBasicHeal(magnitude)));
			}
		}
		else if (getOther(target).human()) {
			if (getOther(target).canReadAdvanced(target)) {
				report(String.format("%s heals %d stamina damage.", target.name(), magnitude));
			}
			else if (getOther(target).canReadBasic(target)) {
				report(String.format("%s heals %s.", target.name(), target.getBasicHeal(magnitude)));
			}
		}
	}

	/**
	 * Report status applied to specified character
	 *
	 * @param target The character that gained the status
	 * @param sts    The status gained
	 */
	public void reportStatus(Character target, Status sts) {
		if (target.human()) {
			if (target.canReadBasic(target)) {
				report("You gain the status " + sts.toString() + ".");
			}
		}
		else if (getOther(target).human()) {
			if (getOther(target).canReadBasic(target)) {
				report(target.name() + " gains the status " + sts.toString() + ".");
			}
		}
	}

	/**
	 * Report status immunity of specified character
	 *
	 * @param target The character immune to the status
	 * @param sts    The status that failed to apply
	 */
	public void reportStatusFizzle(Character target, Status sts) {
		if (target.human()) {
			if (target.canReadBasic(target)) {
				report("You are immune to " + sts.toString() + ".");
			}
		}
		else if (getOther(target).human()) {
			if (getOther(target).canReadBasic(target)) {
				report(target.name() + " is immune to " + sts.toString() + ".");
			}
		}
	}

	/**
	 * Report status loss of specified character
	 *
	 * @param target The character that lost the status
	 * @param sts    The status that was lost
	 */
	public void reportStatusLoss(Character target, Status sts) {
		if (target.human()) {
			if (target.canReadBasic(target)) {
				report("You are no longer affected by the status " + sts.toString() + ".");
			}
		}
		else if (getOther(target).human()) {
			if (getOther(target).canReadBasic(target)) {
				report(target.name() + " loses the status " + sts.toString() + ".");
			}
		}
	}

	/**
	 * Adds a watcher to the encounter
	 *
	 * @param voyeur The character watching the fight
	 */
	public void addWatcher(Character voyeur) {
		audience.add(voyeur);
	}

	/**
	 * Gets all character watching the fight
	 *
	 * @return A list of characters
	 */
	public List<Character> getAudience() {
		return audience;
	}

	/**
	 * Checks whether the specified character is watching the fight
	 *
	 * @param voyeur The character to check for
	 * @return True if the character is watching, otherwise False
	 */
	public boolean isWatching(ID voyeur) {
		for (var viewer : audience) {
			if (viewer.id() == voyeur) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Adds the specified event to the list
	 *
	 * @param target The character the event applies to
	 * @param event  The event to add
	 */
	public void addEvent(Character target, CombatEvent event) {
		if (events.containsKey(target)) {
			events.get(target).add(event);
		}
	}

	/**
	 * Gets a list of events that happened to the specified character
	 *
	 * @param target The character to get the events for
	 * @return A list of events
	 */
	public List<CombatEvent> getEvents(Character target) {
		return events.get(target);
	}

	/**
	 * Checks whether the specified character's underwear is still intact
	 *
	 * @param target The character to check
	 * @return True if the underwear is still intact, otherwise false
	 */
	public boolean underwearIntact(Character target) {
		var underwear = target.getOutfitItem(ClothingType.UNDERWEAR);
		if (underwear == null) {
			// If character has no underwear in outfit, it is always intact
			return true;
		}
		else {
			return clothespile.contains(underwear);
		}
	}

	/**
	 * Rolls an attack roll using the specified skill
	 * <p>
	 * An attack may fail even if the target cannot move. It can also be countered.
	 *
	 * @param skill    The skill that is used
	 * @param attacker The character using the skill
	 * @param target   The character being targeted
	 * @return True if the attack hits, otherwise False
	 */
	public boolean attackRoll(Skill skill, Character attacker, Character target) {
		var roll = Global.random(20) + 1;
		if (roll == 20) {
			return true;
		}

		// Modify to-hit roll based on dominant/submissive stance
		var toHit = roll + attacker.bonusToHit() + skill.accuracy();
		if (stance.dom(attacker)) {
			toHit *= 1.3f;
		}
		else if (stance.sub(attacker)) {
			toHit *= .8;
		}

		// Compare to-hit roll against target AC
		var ac = target.ac();
		if (toHit != 0 && toHit >= ac) {
			return true;
		}

		// Check if roll was so low the target can counter
		if (toHit + 10 < ac + target.bonusCounter()) {
			target.counterattack(attacker, skill.type(), this);
		}
		return false;
	}

	/**
	 * Rolls an effect roll using the specified skill
	 * <p>
	 * An effect can only fail if it rolls below the target AC. It cannot be countered.
	 *
	 * @param skill    The skill that is used
	 * @param attacker The character using the skill
	 * @param target   The character being targeted
	 * @param power    Unused
	 * @return True if the effect hits, otherwise False
	 */
	public boolean effectRoll(Skill skill, Character attacker, Character target, int power) {
		var roll = Global.random(20) + 1;
		if (roll == 20) {
			return true;
		}

		// Modify to-hit roll based on dominant/submissive stance
		var tohit = roll + attacker.bonusToHit() + skill.accuracy();
		if (stance.dom(attacker)) {
			tohit *= 1.3f;
		}
		else if (stance.sub(attacker)) {
			tohit *= .8;
		}
		// Compare to-hit roll against target AC
		return tohit > target.ac();
	}

	/**
	 * Checks whether the specified skill is permitted by the combat rules.
	 * <p>
	 * Will always allow actions for NPCs
	 *
	 * @param skill The skill to check
	 * @param user  The character performing the action
	 * @return True if the skill is allowed, otherwise False
	 */
	public boolean isAllowed(Skill skill, Character user) {
		if (Global.checkFlag(Flag.nopain) && skill.hasTag(SkillTag.PAINFUL)) {
			return false;
		}

		// Combat rules don't apply to NPCs
		if (!user.human()) {
			return true;
		}

		if (rules.contains(Modifier.pacifist) && skill.hasTag(SkillTag.PAINFUL)) {
			return false;
		}
		if (rules.contains(Modifier.notoys) && skill.hasTag(SkillTag.TOY)) {
			return false;
		}
		if (rules.contains(Modifier.noitems) && skill.hasTag(SkillTag.CONSUMABLE)) {
			return false;
		}
		if (rules.contains(Modifier.norecovery) && skill.hasTag(SkillTag.MASTURBATION)) {
			return false;
		}

		return true;
	}

	/**
	 * Checks whether an action with the specified tag is permitted by the combat rules.
	 * <p>
	 * Will always allow actions for NPCs
	 *
	 * @param tag  The tag to check
	 * @param user The character performing the action
	 * @return True if actions with the tag are allowed, otherwise False
	 */
	public boolean isAllowed(Tag tag, Character user) {
		if (Global.checkFlag(Flag.nopain) && tag == SkillTag.PAINFUL) {
			return false;
		}

		// Combat rules don't apply to NPCs
		if (!user.human()) {
			return true;
		}

		if (rules.contains(Modifier.pacifist) && tag == SkillTag.PAINFUL) {
			return false;
		}
		if (rules.contains(Modifier.notoys) && tag == SkillTag.TOY) {
			return false;
		}
		if (rules.contains(Modifier.noitems) && tag == SkillTag.CONSUMABLE) {
			return false;
		}
		if (rules.contains(Modifier.norecovery) && tag == SkillTag.MASTURBATION) {
			return false;
		}

		return true;
	}

	/**
	 * Checks whether the specified rule modifier is applied
	 *
	 * @param mod The modifier to check for
	 * @return True if the rule modifier is applied, otherwise False
	 */
	public boolean hasModifier(Modifier mod) {
		return rules.contains(mod);
	}

	/**
	 * Checks whether combat is over
	 *
	 * @return True if combat is over, otherwise False
	 */
	public boolean isOver() {
		return end;
	}

	/**
	 * Set parent activity that initiated combat
	 *
	 * @param parent The parent activity
	 */
	public void setParent(Activity parent) {
		this.parentActivity = parent;
		Global.gui().watchCombat(this);
	}
}
