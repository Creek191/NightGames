package areas;

import characters.Character;
import global.Global;
import items.Consumable;
import items.Item;

import java.util.ArrayList;

public class NinjaStash implements Deployable {
	private final ArrayList<Item> contents;
	private final Character owner;

	public NinjaStash(Character owner) {
		this.owner = owner;
		contents = new ArrayList<>();
		for (var i = 0; i < 4; i++) {
			switch (Global.random(3)) {
				case 0:
					contents.add(Consumable.needle);
					contents.add(Consumable.needle);
					break;
				case 1:
					contents.add(Consumable.smoke);
					break;
			}
		}
	}

	@Override
	public void resolve(Character active) {
		if (owner == active && active.human()) {
			Global.gui().message(
					"You have a carefully hidden stash of emergency supplies here. You can replace your clothes and collect the items if you need to.");
		}
	}

	/**
	 * Collects the contents of the stash
	 *
	 * @param active The character activating the stash
	 */
	public void collect(Character active) {
		for (var i : contents) {
			active.gain(i);
		}
		contents.clear();
	}

	public String toString() {
		return "Ninja Stash";
	}

	@Override
	public Character owner() {
		return owner;
	}

	@Override
	public int priority() {
		return 0;
	}
}
