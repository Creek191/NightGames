package areas;

import actions.Movement;
import characters.Character;
import global.Constants;
import global.Global;

public class Trace {
	private final Movement event;
	private final Character person;
	private int time;

	public Trace(Character person, Movement event) {
		this.person = person;
		this.event = event;
		this.time = 0;
	}

	/**
	 * Increases the age of the trace
	 *
	 * @return True if the trace is outdated and can be removed, otherwise False
	 */
	public boolean age() {
		time++;
		return time >= 12;
	}

	public Character getPerson() {
		return person;
	}

	/**
	 * Attempts to read the track to gain information from it
	 *
	 * @param tracking The tracking score of the person reading the track
	 * @return Information about the track and the person who left it, or an empty string
	 */
	public String track(int tracking) {
		var score = tracking - difficulty();
		if (score > 0) {
			return getIdentity(score) + getAct(score) + getTime(score);
		}
		else {
			return "";
		}
	}

	/**
	 * Gets the identity of the person who left the trace
	 *
	 * @param score The tracking score of the person looking at the trace
	 */
	private String getIdentity(int score) {
		if (Global.random(score) >= 5) {
			return person.name();
		}
		else {
			return "Someone";
		}
	}

	/**
	 * Gets the action performed by the person who left the trace
	 *
	 * @param score The tracking score of the person looking at the trace
	 */
	private String getAct(int score) {
		if (Global.random(score) >= 5) {
			return event.traceDesc();
		}
		else {
			return " was here ";
		}
	}

	/**
	 * Gets the time when the trace was left
	 *
	 * @param score The tracking score of the person looking at the trace
	 */
	private String getTime(int score) {
		if (Global.random(score) >= 10) {
			if (time > 6) {
				return "more than 30 minutes ago.";
			}
			else {
				return "within the past " + (time * 5) + " minutes.";
			}
		}
		else if (Global.random(score) >= 5) {
			if (time > 6) {
				return "a while ago.";
			}
			else {
				return "recently.";
			}
		}
		else {
			return "at some point.";
		}
	}

	/**
	 * Gets a value indicating how difficult the trace is to read
	 */
	private int difficulty() {
		return Constants.BASETRACKDIFF + time * 5;
	}
}
