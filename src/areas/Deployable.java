package areas;

import characters.Character;

public interface Deployable {

	/**
	 * Activates the deployable's effects
	 *
	 * @param active The character activating the deployable
	 */
	void resolve(Character active);

	/**
	 * Gets the character who placed the deployable
	 */
	Character owner();

	/**
	 * Gets the priority when determining which deployable to process first
	 * <p>
	 * The deployable with the highest priority is resolved first
	 */
	int priority();
}
