package areas;

import actions.Movement;
import characters.Character;
import combat.Encounter;
import global.Match;
import trap.Trap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Area implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -1372128249588089014L;
	public final String name;
	public final String description;
	public HashSet<Area> adjacent;
	public HashSet<Area> shortcut;
	public HashSet<Area> jump;
	public ArrayList<Character> present;
	public Encounter fight;
	public boolean alarm;
	public ArrayList<Deployable> env;
	public MapDrawHint drawHint;
	private final ArrayList<Trace> traces;
	private final Movement enumerator;
	private final Match match;
	private boolean pinged;

	public Area(String name, String description, Movement enumerator, Match match) {
		this(name, description, enumerator, new MapDrawHint(), match);
	}

	public Area(String name, String description, Movement enumerator, MapDrawHint drawHint, Match match) {
		this.name = name;
		this.description = description;
		this.enumerator = enumerator;
		this.match = match;
		adjacent = new HashSet<>();
		shortcut = new HashSet<>();
		this.traces = new ArrayList<>();
		jump = new HashSet<>();
		present = new ArrayList<>();
		env = new ArrayList<>();
		alarm = false;
		fight = null;
		this.drawHint = drawHint;
	}

	/**
	 * Creates a connection to the specified area
	 * <p>
	 * Linked areas can be reached through regular movement
	 *
	 * @param adj The area to connect
	 */
	public void link(Area adj) {
		adjacent.add(adj);
	}

	/**
	 * Creates a shortcut to the specified area
	 * <p>
	 * A shortcut is only usable by characters with high Cunning
	 *
	 * @param adj The area to connect
	 */
	public void shortcut(Area adj) {
		shortcut.add(adj);
	}

	/**
	 * Creates a jump to the specified area
	 * <p>
	 * A jump is only usable by characters trained in Ninjutsu
	 *
	 * @param adj The area to connect
	 */
	public void jump(Area adj) {
		jump.add(adj);
	}

	/**
	 * Checks whether characters in the area are openly visible
	 * <p>
	 * Prevents things like placing traps
	 */
	public boolean open() {
		return enumerator == Movement.quad;
	}

	/**
	 * Checks whether the area is a narrow link between to adjacent ones
	 * <p>
	 * Prevents spawning caches due to narrow size
	 */
	public boolean corridor() {
		return enumerator == Movement.bridge || enumerator == Movement.tunnel;
	}

	/**
	 * Checks whether the area can be scavenged for items and materials
	 */
	public boolean materials() {
		return enumerator == Movement.workshop || enumerator == Movement.storage;
	}

	/**
	 * Checks whether the area contains equipment to craft flasks and potions
	 */
	public boolean potions() {
		return enumerator == Movement.lab || enumerator == Movement.kitchen;
	}

	/**
	 * Checks whether characters can use the area to bathe/shower
	 */
	public boolean bath() {
		return enumerator == Movement.shower || enumerator == Movement.pool;
	}

	/**
	 * Checks whether characters can get new clothes in the area
	 */
	public boolean resupply() {
		return enumerator == Movement.dorm || enumerator == Movement.union;
	}

	/**
	 * Checks whether the area contains equipment to recharge batteries
	 */
	public boolean recharge() {
		return enumerator == Movement.workshop;
	}

	/**
	 * Checks whether characters can restore their mana in the area
	 */
	public boolean mana() {
		return enumerator == Movement.la;
	}

	/**
	 * Checks whether a character in an adjacent area can notice movement in the area
	 *
	 * @param listener The character to check the area with
	 * @return True if characters or an active alarm were noticed in the area, otherwise False
	 */
	public boolean ping(Character listener) {
		if (fight != null) {
			return true;
		}
		for (var character : present) {
			if (listener.listenCheck(character.sneakValue()) || open()) {
				return true;
			}
		}
		return alarm;
	}

	/**
	 * Checks whether an alarm was triggered in the area
	 */
	public boolean alarmTriggered() {
		return alarm;
	}

	/**
	 * Adds the specified character to those in the area
	 *
	 * @param p The character entering the area
	 */
	public void enter(Character p) {
		present.add(p);
		triggerDeployable(p);
	}

	/**
	 * Checks whether the specified character can trigger an encounter event
	 * <p>
	 * This could either be a new fight, or an intervention in an existing one
	 *
	 * @param p The character to check for
	 */
	public boolean encounter(Character p) {
		if (fight != null && fight.getCharacter(1) != p && fight.getCharacter(2) != p) {
			p.intervene(fight, fight.getCharacter(1), fight.getCharacter(2));
		}
		else if (present.size() > 1) {
			for (var opponent : present) {
				if (opponent != p) {
					fight = new Encounter(p, opponent, this);
					return fight.spotCheck();
				}
			}
		}
		return false;
	}

	/**
	 * Checks whether triggering a trap on the specified character gives another character an opportunity attack
	 *
	 * @param target The character affected by the trap
	 * @param trap   The trap that was triggered
	 * @return True if a fight was triggered, otherwise False
	 */
	public boolean opportunity(Character target, Trap trap) {
		if (present.size() > 1 && fight == null) {
			var opponent = present.stream()
					.filter(character -> character != target && target.eligible(character) && character.eligible(target))
					.findFirst().orElse(null);
			if (opponent != null) {
				fight = new Encounter(opponent, target, this);
				opponent.promptTrap(fight, target, trap);
				return true;
			}
		}
		remove(trap);
		return false;
	}

	/**
	 * Checks whether the player is in this area
	 */
	public boolean humanPresent() {
		return present.stream().anyMatch(Character::human);
	}

	/**
	 * Removes the specified character from the area
	 * <p>
	 * Automatically ends any ongoing fight the character is involved in
	 *
	 * @param p The character to remove
	 */
	public void exit(Character p) {
		present.remove(p);
		if (fight != null && (fight.getCharacter(1) == p || fight.getCharacter(2) == p)) {
			endEncounter();
		}
	}

	/**
	 * Ends the encounter in the area by setting it to null
	 */
	public void endEncounter() {
		fight = null;
	}

	/**
	 * Gets the Movement identifying this area
	 */
	public Movement id() {
		return enumerator;
	}

	/**
	 * Places a deployable in the area
	 *
	 * @param thing The deployable to place
	 */
	public void place(Deployable thing) {
		env.add(thing);
	}

	/**
	 * Removes a deployable from the area
	 *
	 * @param triggered The deployable to remove
	 */
	public void remove(Deployable triggered) {
		env.remove(triggered);
	}

	/**
	 * Gets a deployable placed in the specified area
	 *
	 * @param type The type of deployable to get
	 * @return The deployable that was taken from the area, or Null if none was found
	 */
	public Deployable get(Deployable type) {
		for (var deployable : env) {
			if (deployable.getClass() == type.getClass()) {
				return deployable;
			}
		}
		return null;
	}

	/**
	 * Checks if the area contains a hidden stash for the specified character
	 *
	 * @param owner The character to check stashes for
	 * @return True if the area contains a hidden stash for the character, otherwise False
	 */
	public boolean hasStash(Character owner) {
		return env.stream().anyMatch(deployable -> deployable instanceof NinjaStash && deployable.owner() == owner);
	}

	/**
	 * Checks if the area has been trapped by the specified character
	 *
	 * @param trapper The character to check traps for
	 * @return True if the character has trapped the area, otherwise False
	 */
	public boolean hasTrap(Character trapper) {
		return env.stream().anyMatch(deployable -> deployable instanceof Trap && deployable.owner() == trapper);
	}

	public void setPinged(boolean b) {
		this.pinged = b;
	}

	public boolean isPinged() {
		return pinged;
	}

	public String toString() {
		return name;
	}

	public Match getMatch() {
		return match;
	}

	/**
	 * Leaves a trace of the specified character in the area
	 *
	 * @param person The character to leave a trace for
	 * @param act    The type of trace to leave
	 */
	public void leaveTrace(Character person, Movement act) {
		traces.add(new Trace(person, act));
	}

	public List<Trace> getTraces() {
		return traces;
	}

	/**
	 * Increases the age of existing traces, removing them if they become too old
	 */
	public void age() {
		if (fight != null && fight.isOver()) {
			leaveTrace(fight.getCharacter(1), Movement.fight);
			leaveTrace(fight.getCharacter(2), Movement.fight);
			endEncounter();
		}

		for (var i = traces.size() - 1; i >= 0; i--) {
			if (traces.get(i).age()) {
				traces.remove(i);
			}
		}
	}

	/**
	 * Gets the deployable that will be triggered by entering the area
	 *
	 * @return The deployable with the highest priority, or Null if none was found
	 */
	private Deployable getNextDeployable() {
		if (env.isEmpty()) {
			return null;
		}
		var priority = -1;
		Deployable highest = null;
		for (var deployable : env) {
			if (deployable.priority() > priority) {
				priority = deployable.priority();
				highest = deployable;
			}
		}
		if (highest != null) {
			return highest;
		}
		return env.get(0);
	}

	/**
	 * Triggers a deployable placed the area
	 *
	 * @param p The character triggering the deployable
	 */
	private void triggerDeployable(Character p) {
		var found = getNextDeployable();
		if (found != null) {
			found.resolve(p);
		}
	}
}
