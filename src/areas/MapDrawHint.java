package areas;

import java.awt.*;

public class MapDrawHint {
    public final Rectangle rect;
    public final String label;
    public final boolean vertical;

    public MapDrawHint() {
        this(new Rectangle(0, 0, 0, 0), "", false);
    }

    public MapDrawHint(Rectangle rect, String label, boolean vertical) {
        this.rect = rect;
        this.label = label;
        this.vertical = vertical;
    }
}
