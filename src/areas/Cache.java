package areas;

import characters.Attribute;
import characters.Character;
import characters.State;
import characters.Trait;
import global.Flag;
import global.Global;
import items.*;

import java.util.ArrayList;
import java.util.HashMap;

public class Cache implements Deployable {
	private int dc;
	private final Attribute test;
	private final Attribute secondary;
	private final ArrayList<Loot> reward;
	private final HashMap<Loot, Integer> prizeList;

	/**
	 * Creates a new cache containing random loot, that requires a skill check to open
	 *
	 * @param rank The rank of the player the cache is created for. Influences reward
	 */
	public Cache(int rank) {
		prizeList = new HashMap<>();
		reward = new ArrayList<>();
		dc = 10 * (rank + 1);
		switch (Global.random(4)) {
			case 3:
				test = Attribute.Seduction;
				secondary = Attribute.Dark;
				break;
			case 2:
				test = Attribute.Cunning;
				secondary = Attribute.Science;
				break;
			case 1:
				test = Attribute.Perception;
				secondary = Attribute.Arcane;
				dc = 13 + rank;
				break;
			default:
				test = Attribute.Power;
				secondary = Attribute.Ki;
				break;
		}
	}

	@Override
	public void resolve(Character active) {
		if (active.state != State.ready) {
			return;
		}

		// Calculate reward and DC
		calcReward(active);
		if (active.has(Trait.treasureSeeker)) {
			dc *= 0.75;
		}

		var cacheOpened = false;
		if (active.check(test, dc)) {
			cacheOpened = true;
			if (active.human()) {
				switch (test) {
					case Cunning:
						Global.gui().message(
								"<p>You notice a conspicuous box lying on the floor connected to a small digital touchscreen. The box is sealed tight, but it looks like "
										+ "the touchscreen probably opens it. The screen is covered by a number of abstract symbols with the phrase \"Solve Me\" at the bottom. A puzzle obviously. "
										+ "It would probably be a problem to someone less clever. You quickly solve the puzzle and the box opens.<p>");
						break;
					case Perception:
						Global.gui().message(
								"<p>Something is off in this room, but it's hard to put your finger on it. A trap? No, it's not that. You spot a carefully hidden, but "
										+ "nonetheless out-of-place package. It's not sealed and the contents seem like they could be useful, so you help yourself.<p>");
						break;
					case Power:
						Global.gui().message(
								"<p>You spot a strange box with a heavy steel lid. Fortunately the lid is slightly askew, so you may actually be able to get it open with your bare "
										+ "hands if you're strong enough. With a considerable amount of exertion, you manage to force the lid open. Hopefully the contents are worth it.<p>");
						break;
					case Seduction:
						Global.gui().message(
								"<p>You stumble upon a small, well crafted box. It's obviously out of place here, but there's no obvious way to open it. The only thing on the "
										+ "box is a hole that's too dark to see into and barely big enough to stick a finger into. Fortunately, you're very good with your fingers. With a bit of poking "
										+ "around, you feel some intricate mechanisms and maneuver them into place, allowing you to slide the top of the box off.<p>");
						break;
				}
			}
		}
		else if (active.check(secondary, dc - 5)) {
			cacheOpened = true;
			if (active.human()) {
				switch (test) {
					case Cunning:
						Global.gui().message(
								"<p>You notice a conspicuous box lying on the floor connected to a small digital touchscreen. The box is sealed tight, but it looks like "
										+ "the touchscreen probably opens it. The screen is covered by a number of abstract symbols with the phrase \"Solve Me\" at the bottom. A puzzle obviously. "
										+ "Looks unnecessarily complicated. You pull off the touchscreen instead and short the connectors, causing the box to open so you can collect the contents.<p>");
						break;
					case Perception:
						Global.gui().message(
								"<p>Something is off in this room, but it's hard to put your finger on it. A trap? No, it's not that. You summon a minor spirit to search the "
										+ "area. It's not much good in a fight, but pretty decent at finding hidden objects. It leads you to a small hidden box of goodies.<p>");
						break;
					case Power:
						Global.gui().message(
								"<p>You spot a strange box with a heavy steel lid. Fortunately the lid is slightly askew, so you may actually be able to get it open with your bare "
										+ "hands if you're strong enough. You're about to attempt to lift the cover, but then you notice the box is not quite as sturdy as it initially looked. You focus "
										+ "your ki and strike the weakest point on the crate, which collapses the side. Hopefully no one's going to miss the box. You're more interested in what's inside.<p>");
						break;
					case Seduction:
						Global.gui().message(
								"<p>You stumble upon a small, well crafted box. It's obviously out of place here, but there's no obvious way to open it. The only thing on the "
										+ "box is a hole that's too dark to see into and barely big enough to stick a finger into. However, the dark works to your advantage. You take control of the "
										+ "shadows inside the box, giving them physical form and using them to force the box open. Time to see what's inside.<p>");
						break;
				}
			}
		}
		else if (active.human()) {
			switch (test) {
				case Cunning:
					Global.gui().message(
							"<p>You notice a conspicuous box lying on the floor connected to a small digital touchscreen. The box is sealed tight, but it looks like "
									+ "the touchscreen probably opens it. The screen is covered by a number of abstract symbols with the phrase \"Solve Me\" at the bottom. A puzzle obviously. "
									+ "You do your best to decode it, but after a couple failed attempts, the screen turns off and stops responding.<p>");
					break;
				case Perception:
					Global.gui().message(
							"<p>Something is off in this room, but it's hard to put your finger on it. A trap? No, it's not that. Probably nothing.<p>");
					break;
				case Power:
					Global.gui().message(
							"<p>You spot a strange box with a heavy steel lid. Fortunately the lid is slightly askew, so you may actually be able to get it open with your bare "
									+ "hands if you're strong enough. You try to pry the box open, but it's even heavier than it looks. You lose your grip and almost lose your fingertips as the lid "
									+ "slams firmly into place. No way you're getting it open without a crowbar.<p>");
					break;
				case Seduction:
					Global.gui().message(
							"<p>You stumble upon a small, well crafted box. It's obviously out of place here, but there's no obvious way to open it. The only thing on the "
									+ "box is a hole that's too dark to see into and barely big enough to stick a finger into. You feel around inside, but make no progress in opening it. Maybe "
									+ "you'd have better luck with some precision tools.<p>");
					break;
			}
		}

		if (cacheOpened) {
			if (reward.isEmpty()) {
				active.money += 200 * active.getRank();
			}
			for (var item : reward) {
				item.pickup(active);
			}
		}

		active.location().remove(this);
	}

	/**
	 * Calculate the reward to put into the cache
	 *
	 * @param receiver The character opening the cache
	 */
	private void calcReward(Character receiver) {
		var rank = receiver.getRank();
		var targetValue = (rank + 1) * 10;
		if (rank >= 4) {
			if (Global.checkFlag(Flag.excalibur)) {
				prizeList.put(Component.PBlueprint, 50);
				prizeList.put(Component.PVibrator, 50);
				prizeList.put(Component.PHandle, 50);
			}
		}
		if (rank >= 3) {
			prizeList.put(Component.Capacitor, 45);
			prizeList.put(Component.SlimeCore, 45);
			prizeList.put(Component.Foxtail, 45);
			prizeList.put(Component.DragonBone, 50);
			if (receiver.human()) {
				if (!receiver.has(Clothing.shinobitop)) {
					prizeList.put(Clothing.shinobitop, 25);
				}
				if (!receiver.has(Clothing.ninjapants)) {
					prizeList.put(Clothing.ninjapants, 25);
				}
				if (!receiver.has(Clothing.halfcloak)) {
					prizeList.put(Clothing.halfcloak, 25);
				}
				if (!receiver.has(Clothing.pouchlessbriefs)) {
					prizeList.put(Clothing.pouchlessbriefs, 30);
				}
			}
		}
		if (rank >= 2) {
			prizeList.put(Consumable.Talisman, 30);
			prizeList.put(Consumable.FaeScroll, 30);
			prizeList.put(Consumable.powerband, 30);
			prizeList.put(Component.MSprayer, 35);
			prizeList.put(Component.Totem, 25);
			prizeList.put(Consumable.Handcuffs, 20);
			prizeList.put(Component.Semen, 15);
			prizeList.put(Potion.SuperEnergyDrink, 10);
			prizeList.put(Component.Titanium, 35);
			prizeList.put(Flask.PAphrodisiac, 15);
			prizeList.put(Component.Skin, 35);
			if (receiver.human()) {
				if (!receiver.has(Clothing.warpaint)) {
					prizeList.put(Clothing.warpaint, 35);
				}
				if (!receiver.has(Clothing.gi)) {
					prizeList.put(Clothing.gi, 25);
				}
				if (!receiver.has(Clothing.furcoat)) {
					prizeList.put(Clothing.furcoat, 20);
				}
			}
		}
		if (rank >= 1) {
			if (receiver.human()) {
				if (!receiver.has(Clothing.cup)) {
					prizeList.put(Clothing.cup, 20);
				}
				if (!receiver.has(Clothing.trenchcoat)) {
					prizeList.put(Clothing.trenchcoat, 25);
				}
				if (!receiver.has(Clothing.kilt)) {
					prizeList.put(Clothing.kilt, 25);
				}
			}
			prizeList.put(Component.HGMotor, 20);
			prizeList.put(Potion.Fox, 10);
			prizeList.put(Potion.Bull, 10);
			prizeList.put(Potion.Nymph, 10);
			prizeList.put(Potion.Cat, 10);
			if (Global.checkFlag(Flag.Kat)) {
				prizeList.put(Potion.Furlixir, 15);
			}
		}
		if (receiver.human()) {
			if (!receiver.has(Clothing.speedo)) {
				prizeList.put(Clothing.speedo, 15);
			}
			if (!receiver.has(Clothing.loincloth)) {
				prizeList.put(Clothing.loincloth, 15);
			}
		}
		if (!Global.checkFlag(Flag.CacheNoBasics)) {
			prizeList.put(Component.Sprayer, 7);
			prizeList.put(Flask.Aphrodisiac, 5);
			prizeList.put(Flask.DisSol, 5);
			prizeList.put(Flask.Sedative, 14);
			prizeList.put(Flask.SPotion, 10);
			if (receiver.human()) {
				if (!receiver.has(Clothing.latextop)) {
					prizeList.put(Clothing.latextop, 15);
				}
				if (!receiver.has(Clothing.latexpants)) {
					prizeList.put(Clothing.latexpants, 15);
				}
				if (!receiver.has(Clothing.windbreaker)) {
					prizeList.put(Clothing.windbreaker, 10);
				}
			}
		}

		// Parse prize list and pick items until we reach target value for rewards
		if (!prizeList.isEmpty()) {
			var total = 0;
			var bag = new Loot[prizeList.keySet().size()];
			prizeList.keySet().toArray(bag);
			Loot pick;
			while (total < targetValue && reward.size() < 4) {
				pick = bag[Global.random(bag.length)];
				reward.add(pick);
				total += prizeList.get(pick);
			}
		}
	}

	@Override
	public Character owner() {
		return null;
	}

	@Override
	public int priority() {
		return 1;
	}
}
