package skills;

import characters.Anatomy;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a command where an enthralled character is forced to masturbate
 */
public class CommandMasturbate extends PlayerCommand {
	public CommandMasturbate(Character self) {
		super("Force Masturbation", self);
		addTag(SkillTag.COMMAND);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return super.usable(c, target) && target.pantsless();
	}

	@Override
	public String describe() {
		return "Convince your opponent to pleasure themselves for your viewing pleasure";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var lowStart = target.getArousal().get() < 15;
		target.pleasure(5 + Global.random(10), Anatomy.genitals, c);
		var lowEnd = target.getArousal().get() < 15;
		if (lowStart) {
			if (lowEnd) {
				c.write(self, formattedDeal(0, Result.weak, target));
			}
			else {
				c.write(self, formattedDeal(0, Result.strong, target));
			}
		}
		else {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new CommandMasturbate(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		switch (modifier) {
			case normal:
				if (target.hasDick()) {
					return "%target% seems more than happy to do as you tell %himher%. %HeShe% starts stroking %hisher% cock "
							+ "with abandon.";
				}
				else {
					return "%target% seems more than happy to do as you tell %himher%. %HeShe% starts rubbing %hisher% "
							+ "clit with abandon.";
				}
			case special:
				if (target.hasDick()) {
					return "%target% jerks off %hisher% rigid cock while looking at you lustily.";
				}
				else {
					return "Looking at you lustily, %target% rubs %hisher% clit as %heshe% gets wet.";
				}
			case weak:
				return "%target follows your command to the letter, but it doesn't seem to have that much of an effect "
						+ "on %himher%.";
			default:
				return "<<This should not be displayed, please inform The Silver Bard: CommandMasturbate-deal>>";
		}
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The Silver Bard: CommandMasturbate-receive>>";
	}
}
