package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Stsflag;

/**
 * Defines a skill where a character summons tentacles to lash out at their opponent
 */
public class TentacleLash extends Skill {
	public TentacleLash(Character self) {
		super("Tentacle Lash", self);
		addTag(Attribute.Eldritch);
		addTag(SkillTag.PAINFUL);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Eldritch) >= 1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !self.stunned()
				&& !self.distracted()
				&& !self.is(Stsflag.enthralled);
	}

	@Override
	public String describe() {
		return null;
	}

	@Override
	public void resolve(Combat c, Character target) {
	}

	@Override
	public Skill copy(Character user) {
		return new TentacleLash(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return null;
	}
}
