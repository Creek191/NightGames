package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.ID;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

/**
 * Defines a skill where a character strips their opponent's bottom clothes
 */
public class StripBottom extends Skill {
	public StripBottom(Character self) {
		super("Strip Bottoms", self);
		this.speed = 3;
		addTag(SkillTag.STRIPPING);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !target.pantsless()
				&& c.stance.reachBottom(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var strip = self.getEffective(Attribute.Cunning);
		if (target.stripAttempt(strip, self, c, target.bottom.peek())) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
				if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.YUI) {
					c.offerImage("Strip Bottom Female.jpg", "Art by AimlessArt");
				}
				if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.MARA) {
					c.offerImage("Mara StripBottom.jpg", "Art by AimlessArt");
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.MARA) {
					c.offerImage("Strip Bottom Male.jpg", "Art by AimlessArt");
				}
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.ANGEL) {
					c.offerImage("Angel StripBottom.jpg", "Art by AimlessArt");
				}
			}
			target.strip(1, c);
			if (self.getPure(Attribute.Cunning) >= 30 && !target.pantsless()) {
				if (target.stripAttempt(strip, self, c, target.bottom.peek())) {
					if (self.human()) {
						c.write(self, formattedDeal(0, Result.strong, target));
					}
					else if (target.human()) {
						c.write(self, formattedReceive(0, Result.strong, target));
					}
					target.strip(1, c);
					target.emote(Emotion.nervous, 10);
				}
			}
			if (self.human() && target.nude()) {
				c.write(target, target.nakedLiner());
			}
			if (target.human() && target.pantsless()) {
				if (target.getArousal().get() >= 15) {
					c.write("Your boner springs out, no longer restrained by your pants.");
				}
				else {
					c.write(self.name() + " giggles as your flacid dick is exposed");
				}
			}
			target.emote(Emotion.nervous, 10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new StripBottom(user);
	}

	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			switch (Global.random(2)) {
				case 1:
					return "You try to take off %target%'s " + target.bottom.peek().getName() + ", but %heshe% slaps "
							+ "away your hands in a flurry of movement.";
				default:
					return "You grab %target%'s " + target.bottom.peek().getName() + ", but %heshe% scrambles away before "
							+ "you can strip %himher%.";
			}
		}
		else if (modifier == Result.strong) {
			return "Taking advantage of the situation, you also manage to snag %hisher% " + target.bottom.peek().getName();
		}
		else {
			if (target.canAct()) {
				switch (Global.random(2)) {
					case 1:
						return "%target%'s " + target.bottom.peek().getName() + " comes off in your hands, despite "
								+ "%hisher% efforts to stop you.";
					default:
						return "After a brief struggle, you manage to pull off %target%'s " + target.bottom.peek().getName() + ".";
				}
			}
			else {
				switch (Global.random(2)) {
					case 1:
						return "%target% gives you a mean look as you effortlessly pull off %hisher% " + target.bottom.peek().getName() + ".";
					default:
						return "You strip off %target%'s " + target.bottom.peek().getName() + " without much difficulty.";
				}
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			switch (Global.random(2)) {
				case 1:
					return "%self% grabs your " + target.bottom.peek().getName() + ", but you jump back before %heshe% "
							+ "can pull them off.";
				default:
					return "%self% tries to pull down your " + target.bottom.peek().getName() + ", but you hold them up.";
			}
		}
		else if (modifier == Result.strong) {
			return "Before you can react, %heshe% also strips off your " + target.bottom.peek().getName() + ".";
		}
		else {
			if (target.canAct()) {
				switch (Global.random(2)) {
					case 1:
						return "You try to block %self% from grabbing your " + target.bottom.peek().getName() + ", but "
								+ "%heshe% manages to steal them away from you.";
					default:
						return "%self% grabs the waistband of your " + target.bottom.peek().getName() + " and pulls them down.";
				}
			}
			else {
				return "You have no way to stop %self% as %heshe% yanks off your " + target.bottom.peek().getName() + ".";
			}
		}
	}

	@Override
	public String describe() {
		return "Attempt to remove opponent's pants. More likely to succeed if she's weakened and aroused";
	}
}
