package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import items.Component;
import status.Hypersensitive;
import status.Stsflag;
import status.Tied;

/**
 * Defines a skill where a character ties up their opponent to increase their sensitivity
 */
public class TortoiseWrap extends Skill {
	public TortoiseWrap(Character self) {
		super("Tortoise Wrap", self);
		addTag(Attribute.Fetish);
		addTag(SkillTag.CONSUMABLE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Fetish) >= 21;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.has(Component.Rope)
				&& self.is(Stsflag.bondage)
				&& !target.is(Stsflag.tied)
				&& c.stance.dom(self)
				&& c.stance.reachTop(self)
				&& !c.stance.reachTop(target);
	}

	@Override
	public String describe() {
		return "User your bondage skills to wrap your opponent to increase her sensitivity";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Component.Rope, 1);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.add(new Tied(target), c);
		target.add(new Hypersensitive(target), c);
	}

	@Override
	public Skill copy(Character user) {
		return new TortoiseWrap(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You skillfully tie a rope around %target%'s torso in a traditional bondage wrap. %HeShe% moans softly as "
				+ "the rope digs into %hisher% supple skin.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% ties you up with a complex series of knots. Surprisingly, instead of completely incapacitating "
				+ "you, %heshe% wraps you in a way that only slightly hinders your movement. However the discomfort of "
				+ "the rope wrapping around you seems to make your sense of touch more pronounced.";
	}
}
