package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.ID;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

/**
 * Defines a skill where a character summons shadow clones to pleasure the opponent
 */
public class BunshinService extends Skill {
	public BunshinService(Character self) {
		super("Bunshin Service", self);
		this.speed = 4;
		addTag(Attribute.Ninjutsu);
		addTag(Result.touch);
		addTag(SkillTag.CLONE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu) >= 12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)
				&& self.canSpend(6)
				&& target.nude()
				&& !c.stance.behind(target)
				&& !c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "Pleasure your opponent with shadow clones: 4 mojo per attack (min 2))";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var clones = Math.min(Math.min(self.getMojo().get() / 4, self.getEffective(Attribute.Ninjutsu) / 3), 5);
		self.spendMojo(clones * 4);
		Result r;
		if (self.human()) {
			c.write("You form " + clones + " shadow clones and rush forward.");
		}
		else if (target.human()) {
			c.write(self.name() + " moves in a blur and suddenly you see " + clones + " of " + self.pronounTarget(false)
					+ " approaching you.");
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.YUI) {
				c.offerImage("Yui bunshin service.jpg", "Art by AimlessArt");
			}
		}

		// Run attacks for each clone
		int damage;
		for (var i = 0; i < clones; i++) {
			if (c.attackRoll(this, self, target)) {
				switch (Global.random(4)) {
					case 0:
						r = Result.weak;
						if (self.human()) {
							c.write(self, formattedDeal(0, r, target));
						}
						else if (target.human()) {
							c.write(self, formattedReceive(0, r, target));
						}
						target.tempt(Global.random(3) + self.getEffective(Attribute.Seduction) / 4,
								self.bonusTemptation(),
								Result.foreplay,
								c);
						break;
					case 1:
						r = Result.normal;
						if (self.human()) {
							c.write(self, formattedDeal(0, r, target));
						}
						else if (target.human()) {
							c.write(self, formattedReceive(0, r, target));
						}
						target.pleasure(Global.random(3 + self.getEffective(Attribute.Seduction) / 2) + target.getEffective(
								Attribute.Perception) / 2, Anatomy.chest, c);
						break;
					case 2:
						r = Result.strong;
						if (self.human()) {
							c.write(self, formattedDeal(0, r, target));
						}
						else if (target.human()) {
							c.write(self, formattedReceive(0, r, target));
						}
						damage = Global.random(4 + self.getEffective(Attribute.Seduction)) + target.getEffective(
								Attribute.Perception) / 2;
						damage = self.bonusProficiency(Anatomy.fingers, damage);
						target.pleasure(damage, Anatomy.genitals, c);
						break;
					default:
						r = Result.critical;
						if (self.human()) {
							c.write(self, formattedDeal(0, r, target));
						}
						else if (target.human()) {
							c.write(self, formattedReceive(0, r, target));
						}
						damage = Global.random(6) + self.getEffective(Attribute.Seduction) / 2 + target.getEffective(
								Attribute.Perception);
						damage = self.bonusProficiency(Anatomy.mouth, damage);
						target.pleasure(damage, Anatomy.genitals, c);
						break;
				}
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.miss, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.miss, target));
				}
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new BunshinService(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%target% dodges your clone's groping hands.";
		}
		else if (modifier == Result.weak) {
			return "Your clone darts close to %target% and kisses %himher% on the lips.";
		}
		else if (modifier == Result.strong) {
			if (target.hasDick()) {
				return "Your shadow clone grabs %target%'s dick and strokes it.";
			}
			else {
				return "Your shadow clone fingers and caresses %target%'s pussy lips.";
			}
		}
		else if (modifier == Result.critical) {
			if (target.hasDick()) {
				return "Your clone attacks %target%'s sensitive penis, rubbing and stroking %hisher% glans.";
			}
			else {
				return "Your clone slips between %target%'s legs to lick and suck %hisher% swollen clit.";
			}
		}
		else {
			return "A clone pinches and teases %target%'s nipples.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You manage to avoid one of the shadow clones.";
		}
		else if (modifier == Result.weak) {
			return "One of the %self% clones grabs you and kisses you enthusiastically.";
		}
		else if (modifier == Result.strong) {
			if (target.hasBalls()) {
				return "A clone gently grasps and massages your sensitive balls.";
			}
			else {
				return "A clone teases and tickles your inner thighs and labia.";
			}
		}
		else if (modifier == Result.critical) {
			if (target.hasDick()) {
				return "One of the %self% clones kneels between your legs to lick and suck your cock.";
			}
			else {
				return "One of the %self% clones kneels between your legs to lick your nether lips.";
			}
		}
		else {
			if (self.hasBreasts()) {
				return "A %self% clone presses %hisher% boobs against you and teases your nipples.";
			}
			else {
				return "A %self% clone caresses your chest and teases your nipples.";
			}
		}
	}
}
