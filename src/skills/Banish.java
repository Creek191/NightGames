package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Isolated;

/**
 * Defines a skill that banishes an opponent's pet using focus
 */
public class Banish extends Skill {
	public Banish(Character self) {
		super("Banish", self);
		addTag(Attribute.Spirituality);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Spirituality) >= 3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)
				&& self.canSpend(Pool.FOCUS, 1)
				&& target.pet != null;
	}

	@Override
	public String describe() {
		return "Banish your opponent's pet and prevent them from re-summoning them: 1 Focus";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.FOCUS, 1);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		if (target.pet != null) {
			target.pet.remove();
		}
		target.add(new Isolated(target), c);
	}

	@Override
	public Skill copy(Character user) {
		return new Banish(user);
	}

	@Override
	public Tactics type() {
		return Tactics.summoning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You banish %target%'s pet. The residual spiritual energy should disrupt summoning for a while.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% gestures at your pet, who suddenly vanishes. You feel an uneasy sense of isolation, as though "
				+ "your connection is severed.";
	}
}
