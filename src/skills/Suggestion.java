package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Charmed;
import status.Enthralled;
import status.Stsflag;

/**
 * Defines a skill where a character is hypnotizing their opponent to be unable to defend themself
 */
public class Suggestion extends Skill {
	public Suggestion(Character self) {
		super("Suggestion", self);
		addTag(Attribute.Hypnosis);
		addTag(SkillTag.CHARMING);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Hypnosis) >= 1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(5)
				&& !target.is(Stsflag.charmed)
				&& !target.is(Stsflag.enthralled)
				&& c.stance.mobile(self)
				&& !c.stance.sub(self)
				&& !c.stance.behind(self)
				&& !c.stance.behind(target);
	}

	@Override
	public String describe() {
		return "Hypnotize your opponent so she can't defend herself";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(5);
		if (!target.is(Stsflag.cynical)) {
			if (self.getPure(Attribute.Hypnosis) >= 10) {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.strong, target));
				}
				else {
					c.write(self, formattedReceive(0, Result.strong, target));
				}
				target.add(new Enthralled(target, self), c);
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.normal, target));
				}
				else {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				target.add(new Charmed(target, 2 + self.bonusCharmDuration()), c);
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Suggestion(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You attempt to put %target% under hypnotic suggestion, but %heshe% doesn't appear to be affected.";
		}
		else {
			return "You speak in a calm, rhythmic tone, lulling %target% into a hypnotic trance. %HisHer% eyes seem to "
					+ "glaze over slightly, momentarily slipping under your influence.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% attempts to put you under hypnotic suggestion, but you manage to regain control of your "
					+ "consciousness.";
		}
		else {
			return "%self% speaks in a firm, but relaxing tone, attempting to put you into a trance. Obviously you "
					+ "wouldn't let yourself be hypnotized in the middle of a match, right? ...Right? ...Why were you "
					+ "fighting her again?";
		}
	}
}
