package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import stance.ReverseMount;

/**
 * Defines a skill where a character mounts their opponent while facing away from them
 */
public class ReverseStraddle extends Skill {
	public ReverseStraddle(Character self) {
		super("Mount(Reverse)", self);
		this.speed = 6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.mobile(self)
				&& c.stance.mobile(target)
				&& c.stance.prone(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		c.stance = new ReverseMount(self, target);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new ReverseStraddle(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You straddle %target%, facing %hisher% feet.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% sits on your chest, facing your crotch.";
	}

	@Override
	public String describe() {
		return "Straddle facing groin";
	}
}
