package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

/**
 * Defines a skill where a character flicks their fingers into their opponent's genitals
 */
public class Flick extends Skill {
	public Flick(Character self) {
		super("Flick", self);
		this.speed = 6;
		addTag(SkillTag.MISCHIEF);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.pantsless() && c.stance.reachBottom(self) && self.canAct() && !c.stance.penetration(target) && !self.has(
				Trait.shy);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			var damage = Global.random(6) + 5;
			if (self.human()) {
				c.write(self, formattedDeal(damage, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, Result.normal, target));
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.ANGEL) {
					c.offerImage("Flick.jpg", "Art by AimlessArt");
				}
			}
			if (target.has(Trait.achilles)) {
				damage += 2 + Global.random(target.getEffective(Attribute.Perception) / 2);
			}
			target.pain(damage, Anatomy.genitals, c);
			target.getPool(Pool.MOJO).reduce(15);
			self.buildMojo(15);
			self.emote(Emotion.dominant, 10);
			target.emote(Emotion.angry, 40);
			target.emote(Emotion.nervous, 15);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 17 && !user.has(Trait.softheart);
	}

	@Override
	public Skill copy(Character user) {
		return new Flick(user);
	}

	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You flick your finger between %target%'s legs, but don't hit anything sensitive.";
		}
		else {
			if (target.hasBalls()) {
				return "You use two fingers to simultaneously flick both of %target%'s dangling balls. %HeShe% tries to "
						+ "stifle a yelp and jerks %hisher% hips away reflexively. You feel a twinge of empathy, but "
						+ "%heshe%'s done far worse.";
			}
			else {
				return "You flick your finger sharply across %target%'s sensitive clit, causing %himher% to yelp in "
						+ "surprise and pain. %HeShe% quickly covers %hisher% girl parts and glares at you in indignation.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% flicks at your balls, but hits only air.";
		}

		else {
			switch (Global.random(2)) {
				case 1:
					return "%self%  flicks your balls lightly with one finger, hitting both in a single flick. It doesn't "
							+ "hurt very much, but you still let out a quick and high-pitched yelp of surprise as your "
							+ "body flinches forward.";
				default:
					return "%self% gives you a mischievous grin and flicks each of your balls with %hisher% finger. It "
							+ "startles you more than anything, but it does hurt and %hisher% seemingly carefree abuse "
							+ "of your jewels destroys your confidence.";
			}
		}
	}

	@Override
	public String describe() {
		return "Flick opponent's genitals, which is painful and embarrassing";
	}
}
