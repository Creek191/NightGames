package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import items.*;

import java.util.Arrays;
import java.util.List;

/**
 * Defines a command where an enthralled character is forced to give their master an item
 */
public class CommandGive extends PlayerCommand {
	public static final List<? extends Item> TRANSFERABLES = Arrays.asList(
			Flask.SPotion,
			Flask.Aphrodisiac,
			Flask.Sedative,
			Flask.Lubricant,
			Component.Rope,
			Consumable.ZipTie,
			Component.Tripwire,
			Component.Spring,
			Consumable.smoke,
			Consumable.Handcuffs,
			Consumable.Talisman,
			Consumable.FaeScroll,
			Component.Phone,
			Potion.Beer,
			Potion.EnergyDrink,
			Potion.SuperEnergyDrink,
			Potion.Bull,
			Potion.Cat,
			Potion.Fox,
			Potion.Furlixir,
			Potion.Nymph);
	private Item transfer;

	public CommandGive(Character self) {
		super("Take Item", self);
		transfer = null;
		addTag(SkillTag.COMMAND);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		if (!super.usable(c, target)) {
			return false;
		}
		return TRANSFERABLES.stream().anyMatch(target::has);
	}

	@Override
	public String describe() {
		return "Make your opponent give you an item.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		do {
			transfer = TRANSFERABLES.get(Global.random(TRANSFERABLES.size()));
			if (!target.has(transfer)) {
				transfer = null;
			}
		} while (transfer == null);
		target.consume(transfer, 1);
		self.gain(transfer);
		c.write(self, formattedDeal(0, Result.normal, target));
		transfer = null;
	}

	@Override
	public Skill copy(Character user) {
		return new CommandGive(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		return "%target% takes out " + transfer.pre() + transfer.getName() + " and hands it to you.";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The Silver Bard: CommandGive-receive>>";
	}
}
