package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import items.Toy;
import status.Shamed;

/**
 * Defines a skill where a character forcefully fucks their opponent's face
 */
public class FaceFuck extends Skill {
	public FaceFuck(Character self) {
		super("Face Fuck", self);
		addTag(Attribute.Fetish);
		addTag(SkillTag.DOM);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Fetish) >= 15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.getArousal().get() >= 15
				&& ((self.pantsless() && self.hasDick()) || self.has(Trait.strapped))
				&& c.stance.dom(self)
				&& c.stance.reachTop(self)
				&& !c.stance.penetration(self)
				&& !c.stance.penetration(target)
				&& !c.stance.behind(self)
				&& !c.stance.behind(target);
	}

	@Override
	public String describe() {
		return "Force your opponent to orally pleasure you.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
			target.add(new Shamed(target, self.getEffective(Attribute.Fetish) / 5), c);
			self.pleasure(Global.random(self.getEffective(Attribute.Perception) / 2) + 1, Anatomy.genitals, c);
		}
		else {
			if (self.has(Trait.strapped)) {
				if (target.human()) {
					if (self.has(Toy.Strapon2)) {
						c.write(self, formattedReceive(0, Result.upgrade, target));
						target.tempt(Global.random(3), c);
					}
					else {
						c.write(self, formattedReceive(0, Result.special, target));
					}
				}
			}
			else {
				if (target.human()) {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				self.pleasure(Global.random(self.getEffective(Attribute.Perception) / 2) + 1, Anatomy.genitals, c);
			}
			target.add(new Shamed(target, self.getEffective(Attribute.Fetish) / 5), c);
		}
		self.buildMojo(15);
		target.spendMojo(2 * self.getEffective(Attribute.Seduction));
	}

	@Override
	public Skill copy(Character user) {
		return new FaceFuck(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You grab hold of %target%'s head and push your cock into %hisher% mouth. %HeShe% flushes in shame and "
				+ "anger, but still dutifully services you with %hisher% lips and tongue while you thrust your hips.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "%self% forces %hisher% strap-on cock into your mouth and fucks your face with it. It's only rubber, "
					+ "but your position is still humiliating. You struggle not to gag on the artificial member while "
					+ "%self% revels in %hisher% dominance.";
		}
		else if (modifier == Result.upgrade) {
			return "%self% slightly moves forward on you, pushing %hisher% strap-on against your lips. You try to keep "
					+ "your mouth closed but %self% holds your nose closed, forcing you to eventually part your lips and "
					+ "suck on the rubbery invader. After a few sucks, you manage to push it out, although you're still "
					+ "shivering with a mix of arousal and humiliation.";
		}
		else {
			return "%self% forces your mouth open and shoves %hisher% sizable cock into it. You're momentarily overwhelmed "
					+ "by the strong, musky smell and the taste, but %heshe% quickly starts moving %hisher% hips, fucking "
					+ "your mouth like a pussy. You feel your cheeks redden in shame, but you still do what you can to "
					+ "pleasure her. %HeShe% may be using you like a sex toy, but you're going to try to scrounge whatever "
					+ "advantage you can get.";
		}
	}
}
