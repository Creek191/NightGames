package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import items.Flask;
import items.Toy;

/**
 * Defines a skill where a character uses a flask of some kind on their opponent
 */
public class UseFlask extends Skill {
	private final Flask item;

	public UseFlask(Character self, Flask item) {
		super(item.getName(), self);
		this.item = item;
		addTag(SkillTag.CONSUMABLE);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.has(item)
				&& item.canUse(target)
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return item.getDesc();
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(item, 1);
		if (self.has(Toy.Aersolizer)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.special, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.special, target));
			}
			if (item == Flask.DisSol) {
				if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Mara")) {
					c.offerImage("Dissolving.jpg", "Sky Relyks");
				}
			}
			target.add(item.effect().copy(target), c);
		}
		else if (c.attackRoll(this, self, target)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			if (item == Flask.DisSol) {
				if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Mara")) {
					c.offerImage("Dissolving.jpg", "Sky Relyks");
				}
			}
			target.add(item.effect().copy(target), c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new UseFlask(user, item);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "You pop " + item.pre() + item.getName() + " into your Aerosolizer and spray %target% with a cloud of " + item.getColor() + " mist. " + item.getText(
					target);
		}
		else if (modifier == Result.miss) {
			return "You throw a bottle of " + item.getName() + " at %target%, but %heshe% ducks out of the way and it "
					+ "splashes harmlessly on the ground. What a waste.";
		}
		else {
			return "You thow a flask of " + item.getName() + " at %target%. " + item.getText(target);
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "%self% inserts a bottle into the attachment on %hisher% arm. You're suddenly surrounded by a cloud "
					+ "of " + item.getColor() + " gas. " + item.getText(
					target);
		}
		else if (modifier == Result.miss) {
			return "%self% splashes a bottle of " + item.getColor() + " liquid in your direction, but none of it hits you.";
		}
		else {
			return "%self% throws a bottle of " + item.getColor() + " liquid at you. " + item.getText(target);
		}
	}
}
