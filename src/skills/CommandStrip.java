package skills;

import characters.Character;
import combat.Combat;
import combat.Result;

/**
 * Defines a command where an enthralled character is forced to strip
 */
public class CommandStrip extends PlayerCommand {
	public CommandStrip(Character self) {
		super("Force Strip Self", self);
		addTag(SkillTag.COMMAND);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return super.usable(c, target) && !target.nude();
	}

	@Override
	public String describe() {
		return "Force your opponent to strip naked.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		target.undress(c);
		c.write(self, formattedDeal(0, Result.normal, target));
	}

	@Override
	public Skill copy(Character user) {
		return new CommandStrip(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		return "You look %target% in the eye, sending a psychic command for %himher% to strip. %HeShe% complies without "
				+ "question, standing before you nude only seconds later.";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The Silver Bard: CommandStrip-receive>>";
	}
}
