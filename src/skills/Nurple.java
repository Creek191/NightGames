package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import items.Toy;

/**
 * Defines a skill where a character twists their opponent's nipples
 */
public class Nurple extends Skill {
	public Nurple(Character self) {
		super("Twist Nipples", self);
		this.speed = 7;
		addTag(SkillTag.PAINFUL);
		addTag(SkillTag.MISCHIEF);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power) >= 13 && !user.has(Trait.cursed);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.topless()
				&& c.stance.reachTop(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			if (self.has(Toy.ShockGlove) && self.canSpend(Pool.BATTERY, 2)) {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.special, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.special, target));
				}
				self.spend(Pool.BATTERY, 2);
				target.pain(Global.random(9) + target.getEffective(Attribute.Perception), Anatomy.chest, c);
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.normal, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				target.pain(Global.random(9) + target.getEffective(Attribute.Perception) / 2, Anatomy.chest, c);
			}
			target.calm(Global.random(5), c);
			self.buildMojo(10);
			target.emote(Emotion.angry, 25);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Nurple(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String toString() {
		if (self.has(Toy.ShockGlove)) {
			return "Shock Breasts";
		}
		else {
			return name;
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You grope at %target%'s breasts, but miss.";
		}
		else if (modifier == Result.special) {
			return "You grab %target%'s boob with your shock-gloved hand, painfully shocking %himher%.";
		}
		else {
			switch (Global.random(2)) {
				case 1:
					return "You grab each of %target%'s nipples in your hands and wrench them in opposite direction, "
							+ "eliciting a sharp yelp from %himher%.";
				default:
					return "You pinch and twist %target%'s nipples, causing %himher% to yelp in surprise.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to grab your nipples, but misses.";
		}
		else if (modifier == Result.special) {
			return "%self% touches your nipple with %hisher% glove and a jolt of electricity hits you.";
		}
		else {
			switch (Global.random(2)) {
				case 1:
					return "%self% twists your sensitive nipples, giving you a jolt of pain.";
				default:
					return "%self% takes your nipples in %hisher% fingers and starts twisting, not stopping until %heshe% "
							+ "hears a small cry of pain.";
			}
		}
	}

	@Override
	public String describe() {
		return "Twist opponent's nipples painfully";
	}
}
