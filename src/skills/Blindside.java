package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Mount;

/**
 * Defines a skill used to distract an opponent and knock them down
 */
public class Blindside extends Skill {

	public Blindside(Character self) {
		super("Blindside", self);
		addTag(Attribute.Professional);
		addTag(SkillTag.KNOCKDOWN);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getEffective(Attribute.Professional) >= 9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c) && !c.stance.prone(target);
	}

	@Override
	public String describe() {
		return "Distract your opponent and take them down.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.effectRoll(this, self, target, self.getEffective(Attribute.Professional) - target.knockdownDC())) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			c.stance = new Mount(self, target);
			self.emote(Emotion.confident, 15);
			self.emote(Emotion.dominant, 15);
			target.emote(Emotion.nervous, 10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Blindside(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You try to trip %target%, but %heshe% keeps %hisher% balance.";
		}
		else {
			if (Global.random(2) == 0) {
				return "You run your hands along %target%'s back and whisper something extremely lewd, even for the games, "
						+ "in %hisher% ear. While %heshe% is distracted, you wrap your calf behind %hisher%s and trip "
						+ "%himher% on to the ground, landing on top of %hisher% waist. %HeShe% looks up into your eyes, "
						+ "surprised by what happened.";
			}
			return "You move up to %target% and kiss %himher% strongly. While %heshe% is distracted, you throw %himher% "
					+ "down and plant yourself on top of %himher%.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% hooks your ankle, but you recover without falling.";
		}
		else {
			return "Seductively swaying %hisher% hips, %self% sashays over to you. %HisHer% eyes fix you in place as "
					+ "%heshe% leans in and firmly kisses you, shoving %hisher% tongue down your mouth. You are so absorbed "
					+ "in kissing back, that you only notice %hisher% ulterior motive once %heshe% has already swept your "
					+ "legs out from under you and %heshe% has landed on top of you.";
		}
	}
}
