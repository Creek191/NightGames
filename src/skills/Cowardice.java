package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Behind;
import stance.Stance;

/**
 * Defines a skill where a character tries to run away, but fails
 */
public class Cowardice extends Skill {
	public Cowardice(Character self) {
		super("Cowardice", self);
		addTag(Attribute.Submissive);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive) >= 3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.canAct()
				&& c.stance.en == Stance.neutral;
	}

	@Override
	public String describe() {
		return "Turning your back to an opponent will likely get you attacked from behind.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		c.stance = new Behind(target, self);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Cowardice(user);
	}

	@Override
	public Tactics type() {
		return Tactics.negative;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You try to run away, but %target% catches you and grabs you from behind.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% tries to sprint away, but you quickly grab %himher% from behind before %heshe% can escape.";
	}
}
