package skills;

import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Behind;
import status.Flatfooted;

/**
 * Defines a skill where a character tries to distract their opponent by throwing their clothes at them
 */
public class Diversion extends Skill {
	public Diversion(Character self) {
		super("Diversion", self);
		addTag(Attribute.Cunning);
		addTag(SkillTag.OUTMANEUVER);
	}

	@Override
	public boolean requirements(Character user) {
		return user.has(Trait.misdirection);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.nude()
				&& c.stance.mobile(self)
				&& !c.stance.prone(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.topless()) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.special, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.special, target));
			}
			self.strip(1, c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			self.strip(0, c);
		}
		c.stance = new Behind(self, target);
		target.add(new Flatfooted(target, 1), c);
	}

	@Override
	public Skill copy(Character user) {
		return new Diversion(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		var clothing = modifier == Result.normal
					   ? self.top.peek().getName()
					   : self.bottom.peek().getName();
		switch (Global.random(2)) {
			case 1:
				return "In one swift motion, you pull off your " + clothing + " and toss it in the air, leaving %target% "
						+ "to wonder where you went as it falls into %hisher% hands.";
			default:
				return "You quickly strip off your " + clothing + " and throw it to the right, while you jump to the "
						+ "left. %target% catches your discarded clothing, losing sight of you in the process.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		var clothing = modifier == Result.normal
					   ? self.top.peek().getName()
					   : self.bottom.peek().getName();
		switch (Global.random(2)) {
			case 1:
				return "%self% surprises you by charging straight at you. You put your hand out to stop her from crashing "
						+ "into you and are left holding %hisher% " + clothing + " while %heshe% is nowhere to be found.";
			default:
				return "You lose sight of %self% for just a moment, but then see %himher% moving behind you in your "
						+ "peripheral vision. You quickly spin around and grab %himher%, but you find yourself holding "
						+ "just %hisher% " + clothing + ". Wait... what the fuck?";
		}
	}

	@Override
	public String describe() {
		return "Throws your clothes as a distraction";
	}
}
