package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Behind;

/**
 * Defines a skill where a character stops time to hit where it hurts
 */
public class CheapShot extends Skill {
	public CheapShot(Character self) {
		super("Cheap Shot", self);
		addTag(Attribute.Temporal);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal) >= 2;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)
				&& self.canSpend(Pool.TIME, 3)
				&& !c.stance.prone(target)
				&& !c.stance.behind(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public String describe() {
		return "Stop time long enough to get in an unsportsmanlike attack from behind: 3 charges";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.TIME, 3);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
			if (Global.random(5) >= 3) {
				c.write(self, self.bbLiner());
			}
		}
		c.stance = new Behind(self, target);
		target.pain(8 + Global.random(16) + self.getEffective(Attribute.Power), Anatomy.genitals, c);
		if (self.has(Trait.wrassler)) {
			target.calm(Global.random(6), c);
		}
		else {
			target.calm(Global.random(10), c);
		}
		self.buildMojo(10);

		self.emote(Emotion.confident, 15);
		self.emote(Emotion.dominant, 15);
		target.emote(Emotion.nervous, 10);
		target.emote(Emotion.angry, 40);
	}

	@Override
	public Skill copy(Character user) {
		return new CheapShot(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (target.nude()) {
			if (target.hasBalls()) {
				return "You freeze time briefly, giving you a chance to circle around %target%. When time resumes, %heshe% "
						+ "looks around in confusion, completely unguarded. You capitalize on your advantage by crouching "
						+ "behind %himher% and delivering a decisive uppercut to %hisher% dangling balls.";
			}
			else {
				return "You freeze time briefly, giving you a chance to circle around %target%. When time resumes, %heshe% "
						+ "looks around in confusion, completely unguarded. You capitalize on your advantage by delivering "
						+ "a swift, but painful cunt punt.";
			}
		}
		else {
			return "You freeze time briefly, giving you a chance to circle around %target%. When time resumes, %heshe% "
					+ "looks around in confusion, completely unguarded. You capitalize on your advantage by crouching "
					+ "behind %himher% and delivering a decisive uppercut to the groin.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (target.nude()) {
			if (target.hasBalls()) {
				return "%self% suddenly vanishes right in front of your eyes. That wasn't just fast, %heshe% completely "
						+ "disappeared. Before you can react, you're hit from behind with a devastating punch to your "
						+ "unprotected balls.";
			}
			else {
				return "%self% suddenly vanishes right in front of your eyes. That wasn't just fast, %heshe% completely "
						+ "disappeared. Before you can react, you're hit from behind with a devastating punch to your "
						+ "bare vulva.";
			}
		}
		else {
			return "%self% suddenly vanishes right in front of your eyes. That wasn't just fast, %heshe% completely "
					+ "disappeared. You hear something that sounds like 'Za Warudo' before you suffer a painful groin hit "
					+ "from behind.";
		}
	}
}
