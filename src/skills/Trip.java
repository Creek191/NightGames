package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.ID;
import combat.Combat;
import combat.Result;
import stance.StandingOver;
import status.Braced;
import status.Stsflag;

/**
 * Defines a skill where a character trips up their opponent
 */
public class Trip extends Skill {
	public Trip(Character self) {
		super("Trip", self);
		this.accuracy = 2;
		this.speed = 2;
		addTag(Attribute.Cunning);
		addTag(SkillTag.KNOCKDOWN);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.mobile(self)
				&& !c.stance.behind(self)
				&& !c.stance.prone(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.effectRoll(this, self, target, self.getEffective(Attribute.Cunning) / 4)
				&& self.check(Attribute.Cunning, target.knockdownDC())) {
			if (self.id() == ID.REYKA
					&& self.canSpend(20)
					&& !c.stance.prone(self)) {
				self.spendMojo(20);
				target.pain(self.getLevel() / 2, Anatomy.genitals);
				c.write(self, formattedReceive(0, Result.unique, target));
			}
			else if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			if (c.stance.prone(self) && !self.is(Stsflag.braced)) {
				self.add(new Braced(self));
			}
			c.stance = new StandingOver(self, target);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Cunning) >= 16;
	}

	@Override
	public Skill copy(Character user) {
		return new Trip(user);
	}

	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You try to trip %target%, but %heshe% keeps %hisher% balance.";
		}
		else {
			return "You catch %target% off balance and trip %himher%.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% hooks your ankle, but you recover without falling.";
		}
		else if (modifier == Result.unique) {
			return "Reyka steps in close, hooks your ankle and trips you.  As you fall on your back, she quickly plants "
					+ "her foot on your groin, pinning your balls with the tip of her foot. While it is still quite "
					+ "painful, she seems to be taking great care so as to not press down too hard, and she twists and "
					+ "wiggles her toes to gently grind your nuts. <i>\"You're lucky I need these things...intact...\"</i> "
					+ "she says with a sympathetic smile.";
		}
		else {
			return "%self% takes your feet out from under you and sends you sprawling to the floor.";
		}
	}

	@Override
	public String describe() {
		return "Attempt to trip your opponent";
	}
}
