package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Facesitting;
import status.Enthralled;
import status.Shamed;

/**
 * Defines a skill where a character sits on their opponent's face
 */
public class FaceSit extends Skill {
	public FaceSit(Character self) {
		super("Facesit", self);
		addTag(SkillTag.DOM);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getLevel() >= 15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.pantsless()
				&& !self.has(Trait.shy)
				&& c.stance.dom(self)
				&& c.stance.reachTop(self)
				&& !c.stance.penetration(self)
				&& !c.stance.penetration(target)
				&& c.stance.prone(target);
	}

	@Override
	public String describe() {
		return "Shove your crotch into your opponent's face to demonstrate your superiority";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = self.getMojo().get() / 10;
		var type = Result.normal;
		if (self.has(Trait.succubus) && Global.random(5) == 0) {
			damage += 5;
			if (self.getEffective(Attribute.Dark) >= 6 && Global.random(2) == 0) {
				type = Result.critical;
				target.add(new Enthralled(target, self), c);
			}
			else {
				type = Result.strong;
			}
		}
		if (self.getPure(Attribute.Footballer) >= 21) {
			type = Result.special;
			damage += self.getEffective(Attribute.Footballer) * self.getArousal().percent() / 100;
		}
		if (self.has(Trait.pheromones)) {
			damage *= 2;
		}
		if (self.human()) {
			c.write(self, formattedDeal(0, type, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, type, target));
		}
		target.tempt(damage);
		self.pleasure(Global.random(self.getEffective(Attribute.Perception) / 2) + target.getEffective(Attribute.Seduction) / 3,
				Anatomy.genitals,
				c);
		target.add(new Shamed(target), c);
		self.buildMojo(30);
		c.stance = new Facesitting(self, target);
	}

	@Override
	public Skill copy(Character user) {
		return new FaceSit(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String toString() {
		if (self.getPure(Attribute.Footballer) >= 21) {
			return "Unsporting Celebration";
		}
		if (self.hasBalls()) {
			return "Teabag";
		}
		else {
			return "Facesit";
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (self.hasBalls()) {
			if (modifier == Result.special) {
				return "You crouch over %target%'s face and rub your dick and balls all over %himher%. %HeShe% can do "
						+ "little except lick them submissively, which does feel pretty good. You shake your hips to make "
						+ "sure she's got a face-full of your ball sweat and precum.";
			}
			if (modifier == Result.critical) {
				return "You crouch over %target%'s face and dunk your balls into %hisher% mouth. %HeShe% can do little "
						+ "except lick them submissively, which does feel pretty good. %HeShe%'s so affected by your "
						+ "manliness that %hisher% eyes glaze over and %heshe% falls under your control. Oh yeah. You're "
						+ "awesome.";
			}
			else if (modifier == Result.strong) {
				return "You crouch over %target%'s face and dunk your balls into %hisher% mouth. %HeShe% can do little "
						+ "except lick them submissively, which does feel pretty good. Your powerful musk is clearly "
						+ "starting to turn %himher% on. Oh yeah. You're awesome.";
			}
			else {
				return "You crouch over %target%'s face and dunk your balls into %hisher% mouth. %HeShe% can do little "
						+ "except lick them submissively, which does feel pretty good. Oh yeah. You're awesome.";
			}
		}
		else {
			if (modifier == Result.special) {
				return "You straddle %target%'s face and grind your pussy against %hisher% mouth, forcing %himher% to "
						+ "eat you out. You shake your hips, grinding yourself all over %hisher% face and making sure "
						+ "%heshe% gets a good whiff of your sweaty musk.";
			}
			if (modifier == Result.critical) {
				return "You straddle %target%'s face and grind your pussy against %hisher% mouth, forcing %himher% to "
						+ "eat you out. Your juices take control of %hisher% lust and turn %himher% into a pussy licking "
						+ "slave. Ooh, that feels good. You better be careful not to get carried away with this.";
			}
			else if (modifier == Result.strong) {
				return "You straddle %target%'s face and grind your pussy against %hisher% mouth, forcing %himher% to "
						+ "eat you out. %HeShe% flushes and seeks more of your tainted juices. Ooh, that feels good. You "
						+ "better be careful not to get carried away with this.";
			}
			else {
				return "You straddle %target%'s face and grind your pussy against %hisher% mouth, forcing %himher% to "
						+ "eat you out. Ooh, that feels good. You better be careful not to get carried away with this.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (self.hasBalls()) {
			if (modifier == Result.special) {
				return "%self% straddles your head and confidently puts %hisher% balls in your mouth. %HeShe% gives a "
						+ "superior smile as you obediently suck on %hisher% nuts. %HeShe% gyrates %hisher% hips like "
						+ "%heshe%'s doing a lap dance and rubs %hisher% chest sensually. Your sense of smell and vision "
						+ "are dominated by %hisher% sweaty balls and musk.";
			}
			if (modifier == Result.critical) {
				return "%self% straddles your head and dominates you by putting %hisher% balls in your mouth. For some "
						+ "reason, your mind seems to cloud over and you're desperate to please %himher%. She gives a "
						+ "superior smile as you obediently suck on %hisher% nuts.";
			}
			else if (modifier == Result.strong) {
				return "%self% straddles your head and dominates you by putting %hisher% balls in your mouth. Despite "
						+ "the humiliation, %hisher% scent is turning you on incredibly. %HeShe% gives a superior smile "
						+ "as you obediently suck on %hisher% nuts.";
			}
			else {
				return "%self% straddles your head and dominates you by putting %hisher% balls in your mouth. %HeShe% "
						+ "gives a superior smile as you obediently suck on %hisher% nuts.";
			}
		}
		else {
			if (modifier == Result.special) {
				return "%self% straddles your head and grinds %hisher% pussy on your face. You open your mouth and start "
						+ "to lick %hisher% freely offered muff, but %heshe% keeps moving out of reach. %HeShe% gyrates "
						+ "%hisher% hips like %heshe%'s doing a lap dance and rubs %hisher% chest sensually. Your sense "
						+ "of smell and vision are dominated by %hisher% juicy pussy and sweaty musk.";
			}
			if (modifier == Result.critical) {
				return "%self% straddles your face and presses %hisher% pussy against your mouth. You open your mouth and "
						+ "start to lick %hisher% freely offered muff, but %heshe% just smiles while continuing to queen "
						+ "you. As you swallow %hisher% juices, you feel %hisher% eyes start to bore into your mind. You "
						+ "can't resist %himher%. You don't even want to.";
			}
			else if (modifier == Result.strong) {
				return "%self% straddles your face and presses %hisher% pussy against your mouth. You open your mouth and "
						+ "start to lick %hisher% freely offered muff, but %heshe% just smiles while continuing to queen "
						+ "you. You feel your body start to heat up as %hisher% juices flow into your mouth. %HeShe%'s "
						+ "giving you a mouthful of aphrodisiac straight from the source.";
			}
			else {
				return "%self% straddles your face and presses %hisher% pussy against your mouth. You open your mouth and "
						+ "start to lick %hisher% freely offered muff, but %heshe% just smiles while continuing to queen "
						+ "you. %HeShe% clearly doesn't mind accepting some pleasure to demonstrate %hisher% superiority.";
			}
		}
	}
}
