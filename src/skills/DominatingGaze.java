package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Masochistic;
import status.Stsflag;

/**
 * Defines a skill where a character uses their domination to put their opponent in a masochistic mood
 */
public class DominatingGaze extends Skill {
	public DominatingGaze(Character self) {
		super("Dominating Gaze", self);
		addTag(Attribute.Discipline);
		addTag(SkillTag.DOM);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Discipline) >= 12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.is(Stsflag.composed)
				&& self.canSpend(10)
				&& c.stance.mobile(self)
				&& !target.is(Stsflag.masochism)
				&& (target.getMood() == Emotion.nervous || target.getMood() == Emotion.desperate);
	}

	@Override
	public String describe() {
		return "Use your dominant demeanor to put your opponent in a masochistic mood";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.spendMojo(10);
		target.add(new Masochistic(target), c);
	}

	@Override
	public Skill copy(Character user) {
		return new DominatingGaze(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "As %target% looks at you with a hint of nervousness in %hisher% eyes, you meet %hisher% gaze. You are "
				+ "going to hurt %himher%. You know that. %HeShe% knows that. So wouldn’t it be a lot better if %heshe% "
				+ "simply allowed %himher%self to enjoy it, as you make %himher% hurt in just the right way? %target% "
				+ "shudders slightly, and you press your advantage. You let %himher% know that all %heshe% has to do is "
				+ "let you take control, and just worry about enjoying %himher%self.<p>%target% seems to shrink into "
				+ "%himher%self as your presence overwhelms %himher%. You’ve got %himher%.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "You make the mistake of looking directly at %self%’s face, letting your nervousness show through. %self% "
				+ "isn’t one to let this go, unfortunately. <i>\"You really needn’t be so concerned,\"</i> %heshe% says, "
				+ "though %hisher% smile says otherwise. <i>\"Please don’t get me wrong, I do plan to let you feel the "
				+ "delicious taste of my crop many more times, but would that truly be so bad?\"</i> A chill runs up your "
				+ "spine as %heshe% says this, and it only gets worse as %heshe% leans toward you. <i>\"Why don’t you let "
				+ "me show you just how enjoyable it can be to let your Mistress punish you? Leave everything to me.\"</i>"
				+ "<p>You feel like you’re shrinking into yourself right now, but you can’t help but admit that the thought "
				+ "of letting %self% take control and hurt you as %heshe% wishes sounds rather enticing right now.\n";
	}
}
