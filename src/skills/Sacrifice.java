package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;

/**
 * Defines a skill where a character sacrifices some stamina to reduce their arousal
 */
public class Sacrifice extends Skill {
	public Sacrifice(Character self) {
		super("Sacrifice", self);
		addTag(Attribute.Dark);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark) >= 15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(25)
				&& self.getArousal().percent() >= 70
				&& !c.stance.sub(self);
	}

	@Override
	public String describe() {
		return "Damage yourself to reduce arousal: 25 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(25);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.weaken(15 + self.getEffective(Attribute.Dark), c);
		self.calm(2 * self.getEffective(Attribute.Dark), c);
	}

	@Override
	public Skill copy(Character user) {
		return new Sacrifice(user);
	}

	@Override
	public Tactics type() {
		return Tactics.calming;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You feed your own lifeforce and pleasure to the darkness inside you. Your legs threaten to give out, but "
				+ "you've regained some self control.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% pinches %hisher% nipples hard while screaming in pain. You see %himher% stagger in exhaustion, "
				+ "but she seems much less aroused.";
	}
}
