package skills;

import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import pet.ImpFem;
import pet.ImpMale;
import pet.Ptype;

/**
 * Defines a skill where a character summons an imp
 */
public class SpawnImp extends Skill {
	private final Ptype gender;

	public SpawnImp(Character self, Ptype gender) {
		super("Summon Imp", self);
		this.gender = gender;
		addTag(Attribute.Dark);
		addTag(SkillTag.PET);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark) >= 6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(10)
				&& self.pet == null
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Summon a demonic Imp: 15 arousal";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendArousal(15);
		var type = Result.normal;
		var power = 3 + self.bonusPetPower() + (self.getEffective(Attribute.Dark) / 10);
		var ac = 2 + self.bonusPetEvasion() + (self.getEffective(Attribute.Dark) / 10);
		if (self.has(Trait.royalguard)) {
			power += 6;
			ac += 6;
			type = Result.strong;
		}
		if (self.human()) {
			c.write(self, formattedDeal(0, type, target));
			if (gender == Ptype.impfem) {
				self.pet = new ImpFem(self, power, ac);
				c.offerImage("FemaleImp.png", "Art by AimlessArt");
			}
			else {
				self.pet = new ImpMale(self, power, ac);
			}
		}
		else {
			if (target.human()) {
				c.write(self, formattedReceive(0, type, target));
				c.offerImage("FemaleImp.png", "Art by AimlessArt");
			}
			self.pet = new ImpFem(self, power, ac);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new SpawnImp(user, gender);
	}

	@Override
	public Tactics type() {
		return Tactics.summoning;
	}

	public String toString() {
		if (gender == Ptype.impfem) {
			return "Imp (female)";
		}
		else {
			return "Imp (male)";
		}
	}

	@Override
	public boolean equals(Object other) {
		return this.getClass() == other.getClass() &&
				this.toString().equals(other.toString());
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (gender == Ptype.impfem) {
			return "You focus your dark energy and summon a minion to fight for you. A naked, waist high, female imp "
					+ "steps out of a small burst of flame. She stirs up her honey pot and despite yourself, you're "
					+ "slightly affected by the pheromones she's releasing.";
		}
		else {
			return "You focus your dark energy and summon a minion to fight for you. A brief burst of flame reveals a "
					+ "naked imp. He looks at %target% with hungry eyes and a constant stream of pre-cum leaks from his "
					+ "large, obscene cock.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% spreads out %hisher% dark aura and a demonic imp appears next to %himher% in a burst of flame. "
				+ "The imp stands at about waist height, with bright red hair, silver skin and a long flexible tail. "
				+ "It's naked, clearly female, and surprisingly attractive given its inhuman features.";
	}
}
