package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Charmed;
import status.Stsflag;

/**
 * Defines a skill where the user begs for mercy
 */
public class Beg extends Skill {
	public Beg(Character self) {
		super("Beg", self);
		addTag(Attribute.Submissive);
		addTag(SkillTag.CHARMING);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive) >= 12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct() && !c.stance.dom(self);
	}

	@Override
	public String describe() {
		return "Beg your opponent to go easy on you";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if ((Global.random(30) <= self.getEffective(Attribute.Submissive) - (target.getEffective(Attribute.Cunning) / 2) && !target.is(
				Stsflag.cynical) ||
				target.getMood() == Emotion.dominant) &&
				target.getMood() != Emotion.angry && target.getMood() != Emotion.desperate) {
			target.add(new Charmed(target,
					1 + self.getEffective(Attribute.Submissive) / 10 + self.bonusCharmDuration()), c);
			if (self.human()) {
				c.write(formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(formattedReceive(0, Result.normal, target));
			}
		}
		else {
			if (self.human()) {
				c.write(formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Beg(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "You give in and humble yourself while pleading with %target% to go easier on you. %HeShe% just "
						+ "smiles and shakes %hisher% head and you get the feeling it had the opposite effect.";
			}
			else {
				return "You throw away your pride and ask %target% for mercy. This just seems to encourage %hisher% "
						+ "sadistic side.";
			}
		}
		else {
			if (Global.random(2) == 0) {
				return "You plead with %target% for a brief moment of reprieve. You aren't sure how you manage, but "
						+ "%heshe% agrees and you have a moment to rest.";
			}
			else {
				return "You put yourself completely at %target%'s mercy. %HeShe% takes pity on you and gives you a moment "
						+ "to recover.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "%self% bats %hisher% eyes at you and gives you a desperate plea to ease up. You think it's time "
						+ "you remind %himher% you're here to win, not show mercy.";
			}
			else {
				return "%self% gives you a pleading look and asks you to go light on %himher%. %HeShe%'s cute, but "
						+ "%heshe%'s not getting away that easily.";
			}
		}
		else {
			if (Global.random(2) == 0) {
				return "When %self% looks at you like a kicked puppy and requests you give %himher% a fighting chance, "
						+ "you can't help but give %himher% a second to recover.";
			}
			else {
				return "%self% begs you for mercy, looking ready to cry. Maybe you should give %himher% a break.";
			}
		}
	}
}
