package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character sucks on their opponent's neck
 */
public class SuckNeck extends Skill {
	public SuckNeck(Character self) {
		super("Suck Neck", self);
		this.accuracy = 7;
		this.speed = 5;
		addTag(Attribute.Seduction);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.kiss(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			var damage = 0;
			if (self.getPure(Attribute.Dark) >= 1) {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.special, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.special, target));
				}
				damage = self.getEffective(Attribute.Dark);
				target.weaken(damage, c);
				self.heal(damage, c);
				self.spendArousal(damage);
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.normal, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
			}
			target.pleasure(3 + damage + Global.random(self.getEffective(Attribute.Seduction) / 2), Anatomy.neck, c);
			self.buildMojo(5);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 12;
	}

	@Override
	public Skill copy(Character user) {
		return new SuckNeck(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	public String toString() {
		if (self.getPure(Attribute.Dark) >= 1) {
			return "Energy Drain";
		}
		else {
			return name;
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You lean in to kiss %target%'s neck, but %heshe% slips away.";
		}
		else if (modifier == Result.special) {
			return "You draw close to %target% as %heshe%'s momentarily too captivated to resist. You run your tongue "
					+ "along %hisher% neck and bite gently. %HeShe% shivers and you can feel the energy of %hisher% "
					+ "pleasure flow into you, giving you strength.";
		}
		else {
			switch (Global.random(2)) {
				case 1:
					return "You lick, kiss, suck, and nibble all over %target%'s neck, causing %himher% to moan under "
							+ "your ministrations.";
				default:
					return "You lick and suck %target%'s neck hard enough to leave a hickey.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% goes after your neck, but you push %himher% back.";
		}
		else if (modifier == Result.special) {
			return "%self% presses %hisher% lips against your neck. %HeShe% gives you a hickey and your knees start to "
					+ "go weak. It's like your strength is being sucked out through your skin.";
		}
		else {
			return "%self% licks and sucks your neck, biting lightly when you aren't expecting it.";
		}
	}

	@Override
	public String describe() {
		return "Suck on opponent's neck. Highly variable effectiveness";
	}
}
