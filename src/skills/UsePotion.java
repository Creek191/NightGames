package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import items.Potion;

/**
 * Defines a skill where a character uses a potion on themself
 */
public class UsePotion extends Skill {
	private final Potion item;

	public UsePotion(Character self, Potion item) {
		super(item.getName(), self);
		this.item = item;
		addTag(SkillTag.CONSUMABLE);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.has(item)
				&& c.stance.mobile(self);
	}

	@Override
	public String describe() {
		return item.getDesc();
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(item, 1);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, self));
		}
		self.add(item.getEffect(self), c);
	}

	@Override
	public Skill copy(Character user) {
		return new UsePotion(user, item);
	}

	@Override
	public Tactics type() {
		return item.getTactic();
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return item.getMessage(self);
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name() + item.getMessage(self);
	}
}
