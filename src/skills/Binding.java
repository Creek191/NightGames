package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Bound;

/**
 * Defines a skill that uses magic to bind an opponent's hands
 */
public class Binding extends Skill {
	public Binding(Character self) {
		super("Binding", self);
		addTag(Attribute.Arcane);
		addTag(SkillTag.BIND);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane) >= 9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c) && self.canSpend(20);
	}

	@Override
	public String describe() {
		return "Bind your opponent's hands with a magic seal: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(20);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.add(new Bound(target, 10 + self.getEffective(Attribute.Arcane), "seal"), c);
		target.emote(Emotion.nervous, 5);
		self.emote(Emotion.confident, 20);
		self.emote(Emotion.dominant, 10);
	}

	@Override
	public Skill copy(Character user) {
		return new Binding(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You cast a binding spell on %target%, holding %himher% in place.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% gestures at you and casts a spell. A ribbon of light wraps around your wrists and holds them in place.";
	}
}
