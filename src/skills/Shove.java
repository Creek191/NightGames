package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Mount;
import stance.Neutral;
import stance.ReverseMount;
import stance.StandingOver;
import status.Braced;
import status.Stsflag;

/**
 * Defines a skill where a character shoves their opponent to knock them down
 */
public class Shove extends Skill {
	public Shove(Character self) {
		super("Shove", self);
		this.speed = 7;
		addTag(Attribute.Power);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.reachTop(self)
				&& !c.stance.dom(self)
				&& !c.stance.penetration(self)
				&& !c.stance.prone(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = Global.random(4) + self.getEffective(Attribute.Power) / 3;
		var r = Result.normal;
		if (c.stance.getClass() == Mount.class || c.stance.getClass() == ReverseMount.class) {
			if (self.check(Attribute.Power, target.knockdownDC() + 5)) {
				r = Result.powerful;
				if (!self.is(Stsflag.braced)) {
					self.add(new Braced(self));
				}
				c.stance = new Neutral(self, target);
			}
			else {
				r = Result.weak;
			}
		}
		else {
			if (self.check(Attribute.Power, target.knockdownDC())) {
				r = Result.strong;
				c.stance = new StandingOver(self, target);
			}
		}
		if (self.getPure(Attribute.Ki) >= 1 && !target.topless()) {
			target.shred(Character.OUTFITTOP);
		}
		if (self.human()) {
			c.write(self, formattedDeal(damage, r, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(damage, r, target));
		}
		self.buildMojo(5);
		target.emote(Emotion.angry, 15);
		target.pain(damage, Anatomy.chest, c);
	}

	@Override
	public boolean requirements(Character user) {
		return !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new Shove(user);
	}

	public Tactics type() {
		return Tactics.damage;
	}

	public String toString() {
		if (self.getEffective(Attribute.Ki) >= 1) {
			return "Shredding Palm";
		}
		else {
			return name;
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		switch (modifier) {
			case powerful:
				return "You shove %target% off of you and get to your feet before %heshe% can retaliate.";
			case weak:
				return "You push %target%, but you're unable to dislodge %himher%.";
			case special:
				return "You channel your ki into your hands and strike %target% in the chest, destroying %hisher% "
						+ target.top.peek() + ".";
			case strong:
				return "You shove %target% hard enough to knock %himher% flat on %hisher% back.";
			case miss:

			default:
				return "You shove %target% back a step, but %heshe% keeps %hisher% footing.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		switch (modifier) {
			case powerful:
				return "%self% shoves you hard enough to free %himher%self and jump up.";
			case weak:
				return "%self% shoves you weakly, without success.";
			case special:
				return "%self% strikes you in the chest with %hisher% palm, staggering you a step. Suddenly your "
						+ target.top.peek() + " tears and falls off you in pieces.";
			case strong:
				return "%self% knocks you off balance and you fall at %hisher% feet.";
			case miss:

			default:
				return "%self% pushes you back, but you're able to maintain your balance.";
		}
	}

	@Override
	public String describe() {
		return "Slightly damage opponent and try to knock her down";
	}
}
