package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.ReverseMount;
import stance.SixNine;
import status.Enthralled;
import status.ProfMod;
import status.ProfessionalMomentum;

/**
 * Defines a skill where the user licks the opponent's pussy
 */
public class Cunnilingus extends Skill {
	public Cunnilingus(Character self) {
		super("Lick Pussy", self);
		addTag(Attribute.Seduction);
		addTag(Result.oral);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.pantsless()
				&& target.hasPussy()
				&& c.stance.oral(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			var damage = 0;
			if (self.getPure(Attribute.Professional) >= 5) {
				if (self.has(Trait.silvertongue)) {
					damage = Global.random(8) + self.getEffective(Attribute.Professional) + (2 * self.getEffective(
							Attribute.Seduction) / 3) + target.getEffective(
							Attribute.Perception);
					if (self.human()) {
						c.write(self, formattedDeal(damage, Result.special, target));
					}
				}
				else {
					damage = Global.random(6) + self.getEffective(Attribute.Seduction) / 2 + target.getEffective(
							Attribute.Perception);
					if (self.human()) {
						c.write(self, formattedDeal(damage, Result.normal, target));
					}
				}
				if (self.has(Trait.sexuallyflexible)) {
					self.add(new ProfessionalMomentum(self, self.getEffective(Attribute.Professional) * 5), c);
				}
				else {
					self.add(new ProfMod("Oral Momentum",
							self,
							Anatomy.mouth,
							self.getEffective(Attribute.Professional) * 5), c);
				}
			}
			else if (self.has(Trait.silvertongue)) {
				damage += Global.random(8) + (2 * self.getEffective(Attribute.Seduction) / 3) + target.getEffective(
						Attribute.Perception);
				if (self.human()) {
					if (!target.has(Trait.succubus)) {
						c.write(self, formattedDeal(damage, Result.special, target));
					}
					else {
						if (Global.random(5) == 0) {
							this.self.tempt(5, c);
							if (target.getEffective(Attribute.Dark) >= 6 && Global.random(2) == 0) {
								c.write(self, formattedDeal(-2, Result.special, target));
								this.self.add(new Enthralled(self, target), c);
							}
							else {
								c.write(self, formattedDeal(-1, Result.special, target));
							}
						}
						else {
							c.write(self, formattedDeal(damage, Result.special, target));
						}
					}
				}
			}
			else {
				damage = Global.random(6) + self.getEffective(Attribute.Seduction) / 2 + target.getEffective(Attribute.Perception);
				if (self.human()) {
					if (!target.has(Trait.succubus)) {
						c.write(self, formattedDeal(damage, Result.normal, target));
					}
					else {
						if (Global.random(5) == 0) {
							this.self.tempt(5, c);
							if (target.getEffective(Attribute.Dark) >= 6 && Global.random(2) == 0) {
								c.write(self, formattedDeal(-2, Result.normal, target));
								this.self.add(new Enthralled(self, target), c);
							}
							else {
								c.write(self, formattedDeal(-1, Result.normal, target));
							}
						}
						else {
							c.write(self, formattedDeal(damage, Result.normal, target));
						}
					}
				}
			}
			if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Yui")) {
				c.offerImage("Cunnilingus.jpg", "Art by AimlessArt");
			}
			c.offerImage("Cunnilingus2.jpg", "Art by AimlessArt");
			damage += self.getMojo().get() / 10;
			if (target.has(Trait.lickable)) {
				damage *= 1.5;
			}
			damage = self.bonusProficiency(Anatomy.mouth, damage);
			target.pleasure(damage, Anatomy.genitals, c);
			if (c.stance instanceof ReverseMount) {
				c.stance = new SixNine(self, target);
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 10;
	}

	@Override
	public Skill copy(Character user) {
		return new Cunnilingus(user);
	}

	@Override
	public int speed() {
		return 2;
	}

	@Override
	public int accuracy() {
		return 6;
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "You try to get between %target%'s legs to use your tongue but %heshe% kicks you away.";
			}
			else {
				return "You try to eat out %target%, but %heshe% pushes your head away.";
			}
		}
		if (target.getArousal().get() < 10) {
			if (Global.random(2) == 0) {
				return "You part %target%'s dry lips with your wet tongue and lap between them until %hisher% own "
						+ "lubrication starts to mix with yours.";
			}
			else {
				return "You run your tongue over %target%'s dry vulva, lubricating it with your saliva.";
			}
		}
		if (modifier == Result.special) {
			return "Your skilled tongue explores %target%'s pussy, finding and pleasuring %hisher% more sensitive areas. "
					+ "You frequently tease %hisher% clitoris until %heshe% can't suppress %hisher% pleasured moans."
					+ (damage == -1
					   ? " Under your skilled ministrations, %hisher% juices flow freely, and they have an unmistakable"
							   + " effect on you."
					   : "")
					+ (damage == -2
					   ? " You feel a strange pull on your mind, somehow %heshe% has managed to enthrall you with "
							   + "%hisher% juices."
					   : "");
		}
		if (target.getArousal().percent() > 80) {
			return "You relentlessly lick and suck the lips of %target%'s pussy as %heshe% squirms in pleasure. You let "
					+ "up just for a second before kissing %hisher% swollen clit, eliciting a cute gasp."
					+ (damage == -1
					   ? " The highly aroused succubus' vulva is dripping with %hisher% aphrodisiac juices and you "
							   + "consume generous amounts of them."
					   : "")
					+ (damage == -2
					   ? " You feel a strange pull on your mind, somehow %heshe% has managed to enthrall you with "
							   + "%hisher% juices."
					   : "");
		}

		switch (Global.random(4)) {
			case 0:
				return "You gently lick %target%'s pussy and sensitive clit."
						+ (damage == -1
						   ? " As you drink down %hisher% juices, they seem to flow straight down to your crotch, "
								   + "lighting fires when they arrive."
						   : "")
						+ (damage == -2
						   ? " You feel a strange pull on your mind, somehow %heshe% has managed to enthrall you with "
								   + "%hisher% juices."
						   : "");
			case 1:
				return "You thrust your tongue into %target%'s hot vagina and lick the walls of %hisher% pussy."
						+ (damage == -1
						   ? " Your tongue tingles with %hisher% juices, clouding your mind with lust."
						   : "")
						+ (damage == -2
						   ? " You feel a strange pull on your mind, somehow %heshe% has managed to enthrall you with "
								   + "%hisher% juices."
						   : "");
			case 2:
				return "Your own enjoyment of pleasing your partner feeds in to %target%'s reactions as %heshe% moans "
						+ "with every flick of your tongue."
						+ (damage == -1
						   ? " Your tongue tingles with %hisher% juices, clouding your mind with lust."
						   : "")
						+ (damage == -2
						   ? " You feel a strange pull on your mind, somehow %heshe% has managed to enthrall you with "
								   + "%hisher% juices."
						   : "");
			default:
				return "You locate and capture %target%'s clit between your lips and attack it with your tongue"
						+ (damage == -1
						   ? " %HisHer% juices taste wonderful and you cannot help but desire more."
						   : "")
						+ (damage == -2
						   ? " You feel a strange pull on your mind, somehow %heshe% has managed to enthrall you with "
								   + "%hisher% juices."
						   : "");
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		String special;
		switch (damage) {
			case -1:
				special = " Your aphrodisiac juices manage to arouse %self% as much as %heshe% aroused you.";
				break;
			case -2:
				special = " Your tainted juices quickly reduce %self% into a willing thrall.";
				break;
			default:
				special = "";
		}
		if (modifier == Result.miss) {
			return "%self% tries to tease your cunt with %hisher% mouth, but you push %hisher% face away from you.";
		}
		else if (modifier == Result.special) {
			return "%self%'s skilled tongue explores your pussy, finding and pleasuring your more sensitive areas. %HeShe% "
					+ "repeatedly attacks your clitoris until you can't suppress your pleasured moans." + special;
		}
		return "%self% locates and captures your clit between %hisher% lips and attacks it with %hisher% tongue." + special;
	}

	@Override
	public String describe() {
		return "Perform cunnilingus on opponent, more effective at high Mojo";
	}
}
