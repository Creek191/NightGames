package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import items.Toy;
import status.Shamed;
import status.Stsflag;

/**
 * Defines a skill where a character spanks their opponent
 */
public class Spank extends Skill {
	public Spank(Character self) {
		super("Spank", self);
		this.accuracy = 4;
		this.speed = 8;
		addTag(SkillTag.PAINFUL);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.pantsless() && !c.stance.prone(target) && c.stance.reachBottom(self) && self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			var damage = Global.random(self.getEffective(Attribute.Power) / 4) + 4 + target.getEffective(Attribute.Perception) / 2;
			var item = 0;
			if (self.has(Toy.Paddle)) {
				damage *= 1.5;
				item++;
				self.buildMojo(20);
				self.emote(Emotion.dominant, 30);
			}
			if (self.has(Trait.disciplinarian) && !c.stance.penetration(self)) {
				if (self.human()) {
					c.write(self, formattedDeal(item, Result.special, target));
					c.offerImage("Spank.jpg", "Art by AimlessArt");
				}
				else if (target.human()) {
					c.write(self, formattedReceive(item, Result.special, target));
				}
				damage *= 1.5;
				if (Global.random(10) >= 5 || !target.is(Stsflag.shamed) && self.canSpend(5)) {
					target.add(new Shamed(target), c);
					self.spendMojo(5);
					target.emote(Emotion.angry, 10);
					target.emote(Emotion.nervous, 15);
				}
				target.pain(damage, Anatomy.genitals, c);
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(item, Result.normal, target));
					c.offerImage("Spank.jpg", "Art by AimlessArt");
				}
				else if (target.human()) {
					c.write(self, formattedReceive(item, Result.normal, target));
				}
				target.pain(damage, Anatomy.ass, c);
			}
			target.emote(Emotion.angry, 45);
			target.emote(Emotion.nervous, 15);
			self.buildMojo(10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 8;
	}

	@Override
	public Skill copy(Character user) {
		return new Spank(user);
	}

	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You try to spank %target%, but %heshe% dodges away.";
		}
		if (modifier == Result.special) {
			if (damage == 1) {
				return "You swing your paddle between %target%'s legs, hitting %himher% sensitive genitals with a painful "
						+ "sounding smack.";
			}
			else {
				if (target.has(Trait.imagination) && Global.random(3) == 0) {
					return "You pull %target% over your knees and begin spanking %hisher% ass while whispering in %hisher% "
							+ "ears about just how bad of a "
							+ (target.has(Trait.male)
							   ? "boy"
							   : "girl")
							+ " %heshe%'s been, and what you're going to have to do to %himher% later. Suddenly, you "
							+ "place a hard slap on %hisher% delicate pussy, snapping %hisher% back to reality with a jolt.";
				}
				switch (Global.random(2)) {
					case 0:
						return "You bend %target% over your knee and spank %himher%, alternating between hitting %hisher% "
								+ "soft butt cheek and %hisher% sensitive pussy.";
					default:
						return "Pulling %target% across your knees, you firmly spank %himher% and tell %himher% how bad "
								+ "%heshe%'s been until %hisher% face turns red.";
				}
			}
		}
		else {
			if (damage == 1) {
				return "You paddle %target% sharply on the ass, leaving a bright red mark.";
			}
			else {
				switch (Global.random(3)) {
					case 0:
						return "You spank %target% on the ass, causing a satisfying slapping sound to ring out.";
					case 1:
						return "You slap %target%'s butt, admiring the way it shakes as your hand rebounds off it.";
					default:
						return "You spank %target% on %hisher% naked butt cheek.";
				}
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% aims a slap at your ass, but you dodge it.";
		}
		if (modifier == Result.special) {
			if (damage == 1) {
				return "%self% delivers an eye watering paddle blow to your exposed balls.";
			}
			else {
				if (target.getEffective(Attribute.Submissive) >= 15 && Global.random(3) == 0) {
					return "%self% pulls you across %hisher% knees and rests %hisher% hand on your ass, and you instantly "
							+ "go limp as you realise what %heshe%'s about to do - tensing up will only make it sting "
							+ "more. No slap comes, though, and instead you hear a whisper in your ear: <i>\"Trying to "
							+ "avoid your punishment?  That won't do. Now show me your ass properly like a good boy.\"</i> "
							+ "Without thinking, you tense up again and push out your buttcheeks. %self% gives you a few "
							+ "hard spanks before letting you go, and your cheeks burn with shame when you realise what "
							+ "you just submitted to in the middle of a sex fight.";
				}
				switch (Global.random(2)) {
					case 0:
						return "%self% pulls you across %hisher% knees and starts spanking you, hard. Stinging pain "
								+ "spreads all across your ass, and every blow is accompanied by a matching one to your "
								+ "pride as you are treated like nothing more than a naughty boy. After a firm slap to "
								+ "the testicles, %self% releases you.";
					default:
						return "%self% bends you over like a misbehaving child and spanks your ass twice. The third spank "
								+ "aims lower and connects solidly with your ballsack, injuring your manhood along with "
								+ "your pride.";
				}
			}
		}
		else {
			if (damage == 1) {
				return "%self% hits your ass with a loud, painful smack of %hisher% paddle.";
			}
			else {
				switch (Global.random(2)) {
					case 0:
						return "%self% slaps you on the ass, leaving a red mark and a stinging feeling.";
					default:
						return "%self% lands a stinging slap on your bare ass.";
				}
			}
		}
	}

	@Override
	public String describe() {
		return "Slap opponent on the ass";
	}
}
