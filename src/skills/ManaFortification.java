package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Erebia;
import status.Stsflag;

/**
 * Defines a skill where a character buffs their attributes using magic
 */
public class ManaFortification extends Skill {
	public ManaFortification(Character self) {
		super("Mana Fortification", self);
		addTag(Attribute.Arcane);
		addTag(SkillTag.ULTIMATE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane) >= 30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(80)
				&& !self.is(Stsflag.erebia)
				&& c.stance.mobile(self)
				&& !c.stance.sub(self);
	}

	@Override
	public String describe() {
		return "Dramatically buffs Power, Speed, Seduction. More mojo increases duration: 80 Mojo + 10/turn";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var mojo = self.getMojo().get();
		mojo -= 80;
		mojo = mojo / 10;
		self.spendMojo(80 + 10 * mojo);

		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new Erebia(self, mojo + 2), c);
		self.emote(Emotion.confident, 20);
		self.emote(Emotion.dominant, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new ManaFortification(user);
	}

	@Override
	public Tactics type() {
		return Tactics.preparation;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You gather all your available mana in your hand, but instead of releasing it, you absorb it back into your "
				+ "body. The magical energy rushes through your limbs, boosting your physical abilities.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% gathers up a dangerous quantity of magic in %hisher% hand. You brace yourself for the incoming "
				+ "spell, but instead the mana flows back into %himher%. Causing %himher% to glow with power.";
	}
}
