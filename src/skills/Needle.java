package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Modifier;
import items.Consumable;
import status.Drowsy;
import status.Horny;

/**
 * Defines a skill where a character throws a drugged needle at their opponent
 */
public class Needle extends Skill {
	public Needle(Character self) {
		super("Needle Throw", self);
		this.accuracy = 8;
		this.speed = 9;
		addTag(Attribute.Ninjutsu);
		addTag(SkillTag.CONSUMABLE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu) >= 1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& (!self.human() || !c.hasModifier(Modifier.noitems))
				&& self.has(Consumable.needle)
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Throw a drugged needle at your opponent.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Consumable.needle, 1);
		if (c.attackRoll(this, self, target)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, self));
			}
			target.add(new Horny(target, 3, 4), c);
			target.add(new Drowsy(target, 4), c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, self));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Needle(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You throw a small, drugged needle at %target%, but %heshe% dodges it.";
		}
		else {
			return "You hit %target% with one of your drugged needles. %HeShe% flushes with arousal and appears to have "
					+ "trouble standing.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You spot a glint of metal and dodge out of the way just in time to avoid a small needle. You don't "
					+ "know what %self% coated it with, but it's probably a good thing it missed.";
		}
		else {
			return "You feel a tiny prick on your neck and discover you've been stuck by a small needle. You pull it out "
					+ "quickly, but already feel the drug going to work. Your body feels heavy and dull, except for your "
					+ "cock, which springs to attention.";
		}
	}
}
