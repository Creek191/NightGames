package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Pool;
import combat.Combat;
import combat.Result;
import global.Modifier;

/**
 * Defines a skill where a character rewinds time to the point they're still wearing clothes
 */
public class Unstrip extends Skill {
	public Unstrip(Character self) {
		super("Unstrip", self);
		addTag(Attribute.Temporal);
		addTag(SkillTag.DRESSING);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal) >= 8;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(Pool.TIME, 6)
				&& self.nude()
				&& !self.human()
				&& c.hasModifier(Modifier.nudist)
				&& c.stance.mobile(self)
				&& !c.stance.prone(self)
				&& !c.stance.penetration(self)
				&& !c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "Rewinds your clothing's time to when you were still wearing it: 6 charges";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.change(null);
		self.spend(Pool.TIME, 6);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.emote(Emotion.confident, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new Unstrip(user);
	}

	@Override
	public Tactics type() {
		return Tactics.recovery;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "It's tricky, but with some clever calculations, you restore the state of your outfit. Your outfit from "
				+ "the start of the night reappears on your body.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "You lose sight of %self% for just a moment and almost do a double-take when you see %himher% again, "
				+ "fully dressed. In the second you looked away, how did %heshe% find the time to put %hisher% clothes on?!";
	}
}
