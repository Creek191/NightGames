package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Stsflag;
import status.WaterStance;

/**
 * Defines a skill where a character boosts their evasion and counter chance at the cost of their strength
 */
public class WaterForm extends Skill {
	public WaterForm(Character self) {
		super("Water Form", self);
		addTag(Attribute.Ki);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ki) >= 3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.is(Stsflag.form)
				&& !c.stance.sub(self);
	}

	@Override
	public String describe() {
		return "Improves evasion and counterattack rate at expense of Power";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new WaterStance(self), c);
	}

	@Override
	public Skill copy(Character user) {
		return new WaterForm(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You relax your muscles, prepared to flow with and counter %target%'s attacks.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% takes a deep breath and %hisher% movements become much more fluid.";
	}
}
