package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character fires magical projectiles at their opponent
 */
public class MagicMissile extends Skill {
	public MagicMissile(Character self) {
		super("Magic Missile", self);
		this.accuracy = 7;
		this.speed = 8;
		addTag(Attribute.Arcane);
		addTag(SkillTag.PAINFUL);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane) >= 1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(5)
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Fires a small magic projectile: 5 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(5);
		if (c.effectRoll(this, self, target, self.getEffective(Attribute.Arcane))) {
			if (target.nude() && Global.random(3) == 2) {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.critical, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.critical, target));
				}
				target.pain(9 + Global.random(2 * self.getEffective(Attribute.Arcane) + 1), Anatomy.genitals, c);
				target.emote(Emotion.angry, 10);
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.normal, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				target.pain(6 + Global.random(self.getEffective(Attribute.Arcane) + 2), Anatomy.chest, c);
				target.emote(Emotion.angry, 5);
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new MagicMissile(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You fire a bolt of magical energy, but %target% narrowly dodges out of the way.";
		}
		else if (modifier == Result.critical) {
			if (target.hasBalls()) {
				return "You cast and fire a magic missile at %target%. Just by luck, it hits %himher% directly in the "
						+ "jewels. %HeShe% cringes in pain, cradling %hisher% bruised parts.";
			}
			else {
				return "You cast and fire a magic missile at %target%. By chance, it flies under %hisher% guard and hits "
						+ "%himher% solidly in the pussy. %HeShe% doubles over with a whimper, holding %hisher% bruised "
						+ "parts.";
			}
		}
		else {
			return "You hurl a magic missile at %target%, hitting and staggering %himher% a step.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You see %self% start to cast a spell and you dive to the left, just in time to avoid the missile.";
		}
		else if (modifier == Result.critical) {
			return "%self% casts a quick spell and fires a bolt of magic into your vulnerable groin. You cradle your "
					+ "injured plums as pain saps the strength from your legs.";
		}
		else {
			return "%self%'s hand glows as %heshe% casts a spell. Before you can react, you're struck with an impact like "
					+ "a punch in the gut.";
		}
	}
}
