package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import items.Clothing;

/**
 * Defines a skill where a character 3D prints new clothes
 */
public class Fabricator extends Skill {
	public Fabricator(Character self) {
		super("Fabricator", self);
		addTag(Attribute.Science);
		addTag(SkillTag.DRESSING);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science) >= 21;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& (self.topless() || self.pantsless())
				&& self.canSpend(Pool.BATTERY, 7)
				&& c.stance.mobile(self)
				&& !c.stance.sub(self)
				&& !c.stance.penetration(self)
				&& !c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "3D prints some temporary clothing: 7 Battery";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.BATTERY, 7);
		if (self.pantsless()) {
			self.wear(Clothing.fabloincloth);
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.bottom, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.bottom, target));
			}
		}
		else {
			self.wear(Clothing.fabwrap);
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.top, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.top, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Fabricator(user);
	}

	@Override
	public Tactics type() {
		return Tactics.calming;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.top) {
			return "You activate your Material Fabrication Unit to craft a crude chest wrap.";
		}
		else {
			return "You activate your Material Fabrication Unit to craft a crude loincloth.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.top) {
			return "%self% activates a strange device, which covers %hisher% breasts with a simple fabric wrap.";
		}
		else {
			return "%self% activates a strange device. A primitive cloth loincloth appears around %hisher% waist, barely "
					+ "covering %hisher% privates.";
		}
	}
}
