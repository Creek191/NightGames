package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.Stance;
import stance.Standing;

/**
 * Defines a skill where a is picked up and penetrated while held up
 */
public class Carry extends Skill {
	public Carry(Character self) {
		super("Carry", self);
		this.accuracy = 2;
		addTag(Attribute.Power);
		addTag(Result.intercourse);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power) >= 30 && !user.has(Trait.petite);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)
				&& self.canFuck()
				&& self.isErect()
				&& self.getStamina().get() >= 15
				&& self.canSpend(10)
				&& target.pantsless()
				&& target.isErect()
				&& c.stance.en != Stance.standing
				&& !c.stance.prone(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(10);
		if (c.effectRoll(this, self, target, self.getEffective(Attribute.Power) / 4)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, self));
			}
			var damage = Global.random(5) + target.getEffective(Attribute.Perception);
			if (self.has(Trait.strapped)) {
				damage += +(self.getEffective(Attribute.Science) / 2);
				damage = self.bonusProficiency(Anatomy.toy, damage);
			}
			else {
				damage = self.bonusProficiency(Anatomy.genitals, damage);
			}
			target.pleasure(damage, Anatomy.genitals, c);
			self.pleasure(Global.random(5) + self.getEffective(Attribute.Perception), Anatomy.genitals, c);

			c.stance = new Standing(self, target);
			if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Mara") || self.name().startsWith("Mara")) {
				c.offerImage("Carry.jpg", "AimlessArt");
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Carry(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "As you go to scoop up %target%, %heshe% slides back, right out of your grip.";
			}
			else {
				return "You pick up %target%, but %heshe% scrambles out of your arms.";
			}
		}
		else {
			if (Global.random(2) == 0) {
				return "You swing %target% up into the air and position %himher% so %heshe% comes sinking down onto your "
						+ "cock. You both clench at each other while your cock buries itself deeper inside %hisher% pussy.";
			}
			else {
				return "You scoop up %target%, lifting %himher% into the air and simultaneously thrusting your dick into "
						+ "%hisher% hot depths. %HeShe% lets out a noise that's equal parts surprise and delight as you "
						+ "bounce %himher% on your pole.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "%self% launches %himher%self at you with %hisher% arms and legs spread but you dodge %himher% at "
						+ "the last moment.";
			}
			else {
				return "%self% jumps onto you, but you deposit %himher% back on the floor.";
			}
		}
		else {
			if (Global.random(2) == 0) {
				return "%self% jumps up and latches on to you. You're about to toss %himher% to the ground when %heshe% "
						+ "slides %himher%self onto your twitching cock. %HeShe% wraps %hisher% legs around you, leaving "
						+ "you no choice but to support the both of you, pulling yourself deeper into %hisher% warm depths "
						+ "in the process.";
			}
			else {
				return "%self% leaps into your arms and impales %himher%self on your cock. %HeShe% wraps %hisher% legs "
						+ "around your torso and you quickly support %himher% so %heshe% doesn't fall and injure "
						+ "%himher%self or you.";
			}
		}
	}

	@Override
	public String describe() {
		return "Picks up opponent and penetrates them: Mojo 10.";
	}
}
