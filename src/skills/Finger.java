package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import status.ProfMod;
import status.ProfessionalMomentum;
import status.Stsflag;

/**
 * Defines a skill where a character fingers their opponent's pussy
 */
public class Finger extends Skill {
	public Finger(Character self) {
		super("Finger", self);
		this.accuracy = 7;
		addTag(Result.touch);
		addTag(Attribute.Seduction);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.genitalsAvailable(c)
				&& target.hasPussy();
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = 4 + target.getEffective(Attribute.Perception);
		var type = Result.normal;
		if (c.attackRoll(this, self, target)) {
			if (self.getEffective(Attribute.Seduction) >= 8) {
				damage += Global.random(self.getEffective(Attribute.Seduction) / 2);
				self.buildMojo(10);
			}
			else {
				type = Result.weak;
			}
			if (self.getPure(Attribute.Professional) >= 3) {
				damage += self.getEffective(Attribute.Professional);
				type = Result.special;
				self.buildMojo(15);
				if (self.has(Trait.sexuallyflexible)) {
					self.add(new ProfessionalMomentum(self, self.getEffective(Attribute.Professional) * 5), c);
				}
				else {
					self.add(new ProfMod("Dexterous Momentum",
							self,
							Anatomy.fingers,
							self.getEffective(Attribute.Professional) * 5), c);
				}
			}
			if (self.is(Stsflag.shadowfingers)) {
				type = Result.critical;
			}
			if (self.human()) {
				c.write(self, formattedDeal(damage, type, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, type, target));
			}
			damage = self.bonusProficiency(Anatomy.fingers, damage);
			target.pleasure(damage, Anatomy.genitals, Result.finisher, c);
			c.offerImage("Fingering.jpg", "Art by AimlessArt");
			c.offerImage("Fingering2.jpg", "Art by AimlessArt");
			if (self.has(Trait.roughhandling)) {
				target.weaken(damage / 2, c);
				if (self.human()) {
					c.write(self, formattedDeal(damage, Result.unique, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(damage, Result.unique, target));
				}
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Finger(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "You opportunistically give %hisher% clit a sharp pinch.";
		}
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "You try to slide your hand down to %target%'s pussy, but %heshe% bats your hand away.";
			}
			else {
				return "You grope at %target%'s pussy, but miss.";
			}
		}
		else if (!target.bottom.isEmpty()) {
			if (modifier == Result.weak) {
				return "You fumble as you slip your hand under %target%'s " + target.bottom.peek().getName() + ". You "
						+ "manage to get under it and run your fingers across %hisher% soft flesh, hoping for a positive "
						+ "response. After some poking and prodding, you withdraw your hand.";
			}
			else {
				if (Global.random(2) == 0) {
					return "You slip your hand under %target%'s " + target.bottom.peek().getName() + " and curl a finger "
							+ "inside of %himher%.";
				}
				else {
					return "You slip your hand under %target%'s " + target.bottom.peek().getName() + " and stroke %hisher% "
							+ "sensitive petals.";
				}
			}
		}
		else if (modifier == Result.critical) {
			return "Your shadowy finger-tendrils easily slide between %target%'s nethers and explore every nook and "
					+ "cranny of %hisher% sensitive pussy.";
		}
		else if (modifier == Result.weak) {
			return "You grope between %target%'s legs, not really knowing what you're doing. You don't know where "
					+ "%heshe%'s the most sensitive, so you rub and stroke every bit of moist flesh under your fingers.";
		}
		else {
			if (target.getArousal().get() <= 15) {

				return "You softly rub the petals of %target%'s closed flower.";
			}
			else if (target.getArousal().percent() < 50) {
				if (Global.random(2) == 0) {
					return "You are gently stroking up and down %target%'s sex when you feel your fingertip become wet "
							+ "with %hisher% juices. You speed up your movement, coating %himher% in the supplied lubricant.";
				}
				else {
					return "%target%'s sensitive lower lips start to open up under your skilled touch and you can feel "
							+ "%himher% becoming wet.";
				}
			}
			else if (target.getArousal().percent() < 80) {
				if (Global.random(2) == 0) {
					return "You spread %target%'s lower lips and tease %hisher% wet entrance with another finger.";
				}
				else {
					return "You locate %target%'s clitoris and caress it directly, causing %himher% to tremble from the "
							+ "powerful stimulation.";
				}
			}
			else {
				if (Global.random(2) == 0) {
					return "Two of your fingers gracefully penetrate %target% and press against %hisher% G-spot, making "
							+ "%himher% gasp. %HeShe% moans in chorus as your fingers continue their skilled work.";
				}
				else {
					return "You stir %target%'s increasingly soaked pussy with your fingers and rub %hisher% clit with "
							+ "your thumb.";
				}
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "%self% gives your poor clit a painful pinch.";
		}
		if (modifier == Result.miss) {
			return "%self% gropes at your pussy, but misses the mark.";
		}
		else if (modifier == Result.critical) {
			return "%self% slides %hisher% shadow tentacles between your legs. The tendrils delve into your slick hole, "
					+ "overwhelming you with a strange pleasure";
		}
		else {
			if (target.getArousal().get() <= 15) {
				return "%self% softly rubs your sensitive lower lips. You aren't very aroused yet, but %hisher% gentle "
						+ "touch gives you a ticklish pleasure";
			}
			else if (target.getArousal().percent() < 50) {
				return "%self% skillfully fingers your sensitive pussy. You bite your lip and try to ignore the pleasure, "
						+ "but despite your best efforts, you feel yourself growing wet.";
			}
			else if (target.getArousal().percent() < 80) {
				return "%self% locates your clitoris and caress it directly, causing you to tremble from the powerful "
						+ "stimulation.";
			}
			else {
				return "%self% stirs your increasingly soaked pussy with %hisher% fingers and rubs your clit directly "
						+ "with %hisher% thumb.";
			}
		}
	}

	@Override
	public String toString() {
		if (self.getPure(Attribute.Professional) >= 3) {
			return "Pro Finger";
		}
		else {
			return "Finger";
		}
	}

	@Override
	public String describe() {
		if (self.getPure(Attribute.Professional) >= 3) {
			return "A professional fingering technique that increases effectiveness with repeated use";
		}
		else {
			return "Digitally stimulate opponent's pussy";
		}
	}
}
