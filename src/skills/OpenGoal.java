package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import status.Stsflag;

/**
 * Defines a skill where a character convinces their masochistic opponent to be finished off with a kick to the groin
 */
public class OpenGoal extends Skill {
    public OpenGoal(Character self) {
        super("Open Goal", self);
        this.speed = 2;
        addTag(Attribute.Footballer);
        addTag(SkillTag.PAINFUL);
        addTag(SkillTag.DOM);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer) >= 30;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canAct()
                && !self.has(Trait.sportsmanship)
                && target.is(Stsflag.masochism)
                && target.is(Stsflag.horny)
                && (!c.stance.prone(self) || self.has(Trait.dirtyfighter))
                && c.stance.feet(self)
                && !c.stance.penetration(self);
    }

    @Override
    public String describe() {
        return "Convince a horny, masochistic opponent to let you finish them off with a hard kick to the groin.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        if (self.human()) {
            c.write(self, formattedDeal(0, Result.normal, target));
        }
        else if (target.human()) {
            c.write(self, formattedReceive(0, Result.normal, target));
        }
        target.pain(self.getEffective(Attribute.Footballer) * 10, Anatomy.genitals, c);
    }

    @Override
    public Skill copy(Character user) {
        return new OpenGoal(user);
    }

    @Override
    public Tactics type() {
        return Tactics.damage;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "You lean in to %target%'s ear and suggest that you'll get too turned on to fight back if you can just "
                + "kick %himher% in the groin. In %hisher% lust-addled state, %heshe% can't resist temptation and spreads "
                + "%hisher% legs for you. You take a step back to get the right distance, before giving %himher% your "
                + "best kick to the crotch. %HisHer% eyes roll back in %hisher% head as %heshe% lets out a quiet noise "
                + "between a moan and a whimper.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return "%self% gives you a naughty smile. <i>\"Come on, baby. Let me give your big balls my best kick. "
                + "It'll get me so hot, I'll probably lose on the spot.\"</i> Your overwhelming lust and masochism "
                + "agree with %hisher% suggestion, while the dissenting voice of reason in your head is strangely quiet. "
                + "%self% winds back and gives you a devastating punt in the groin. You howl in agony, doubling over "
                + "and cupping your throbbing testicles.";
    }
}
