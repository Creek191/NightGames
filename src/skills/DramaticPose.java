package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;

/**
 * Defines a skill where a character poses dramatically, gaining mojo
 */
public class DramaticPose extends Skill {
	public DramaticPose(Character self) {
		super("Dramatic Pose", self);
		addTag(Attribute.Contender);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Contender) >= 1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c);
	}

	@Override
	public String describe() {
		return "Gain mojo when close to being stunned or cumming.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var mojo = (100 - self.getStamina().percent()) / 2;
		mojo += self.getArousal().percent() / 2;
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.emote(Emotion.confident, 100);
		self.buildMojo(mojo);
	}

	@Override
	public Skill copy(Character user) {
		return new DramaticPose(user);
	}

	@Override
	public Tactics type() {
		return Tactics.recovery;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "Even though the fight isn't going well, you strike a confident pose. Your counterattack starts now.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "Despite being worn down, %self% poses dramatically.";
	}
}
