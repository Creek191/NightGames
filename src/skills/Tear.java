package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;
import combat.Result;

/**
 * Defines a skill where a character tears their opponent's clothes off
 */
public class Tear extends Skill {
	public Tear(Character self) {
		super("Tear Clothes", self);
		addTag(Attribute.Power);
		addTag(SkillTag.STRIPPING);
	}

	@Override
	public boolean requirements(Character user) {
		return (user.getPure(Attribute.Power) >= 32 || user.getEffective(Attribute.Animism) >= 15)
				&& !user.has(Trait.cursed);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& ((c.stance.reachTop(self) && !target.topless())
				|| ((c.stance.reachBottom(self) && !target.pantsless())));
	}

	@Override
	public String describe() {
		return "Rip off your opponents clothes";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.stance.reachTop(self) && !target.top.empty()) {
			if (target.top.peek().attribute() != Trait.indestructible
					&& self.getPure(Attribute.Animism) >= 15
					&& (target.stripAttempt(self.getEffective(Attribute.Power) + (self.getEffective(Attribute.Animism) * self.getArousal().percent() / 100),
					self,
					c,
					target.top.peek()))) {
				if (self.human()) {
					c.write(self, "You channel your animal spirit and shred " + target.name() + "'s "
							+ target.top.peek().getName() + " with claws you don't actually have.");
				}
				else if (target.human()) {
					c.write(self, self.name() + " lunges toward you and rakes " + self.possessive(false)
							+ " nails across your " + target.top.peek().getName() + ", shredding the garment. That "
							+ "shouldn't be possible. " + self.possessive(true) + " nails are not that sharp, and "
							+ "if they were, you surely wouldn't have gotten away unscathed.");
				}
				target.shred(0);
				if (self.human() && target.nude()) {
					c.write(target.nakedLiner());
				}
			}
			else if (target.top.peek().attribute() != Trait.indestructible
					&& target.stripAttempt(self.getEffective(Attribute.Power), self, c, target.top.peek())) {
				if (self.human()) {
					c.write(self, target.name() + " yelps in surprise as you rip " + target.possessive(false)
							+ " " + target.top.peek().getName() + " apart.");
				}
				else if (target.human()) {
					c.write(self, self.name() + " violently rips your " + target.top.peek().getName() + " off.");
				}
				target.shred(0);
				if (self.human() && target.nude()) {
					c.write(target, target.nakedLiner());
				}
			}
			else {
				if (self.human()) {
					c.write(self, "You try to tear apart " + target.name() + "'s " + target.top.peek().getName()
							+ ", but the material is more durable than you expected.");
				}
				else if (target.human()) {
					c.write(self, self.name() + " yanks on your " + target.top.peek().getName() + ", but fails to "
							+ "remove it.");
				}
			}
		}
		else {
			if (target.bottom.peek().attribute() != Trait.indestructible
					&& self.getPure(Attribute.Animism) >= 12
					&& (target.stripAttempt(self.getEffective(Attribute.Power) + (self.getEffective(Attribute.Animism) * self.getArousal().percent() / 100),
					self,
					c,
					target.bottom.peek()))) {
				if (self.human()) {
					c.write(self, "You channel your animal spirit and shred " + target.name() + "'s "
							+ target.bottom.peek().getName() + " with claws you don't actually have.");
				}
				else if (target.human()) {
					c.write(self, self.name() + " lunges toward you and rakes " + self.possessive(false)
							+ " nails across your " + target.bottom.peek().getName() + ", shredding the garment. That "
							+ "shouldn't be possible. " + self.possessive(true) + " nails are not that sharp, and "
							+ "if they were, you surely wouldn't have gotten away unscathed.");
				}
				target.shred(1);
				if (self.human() && target.nude()) {
					c.write(target, target.nakedLiner());
				}
				if (target.human() && target.pantsless()) {
					if (target.getArousal().get() >= 15) {
						c.write("Your boner springs out, no longer restrained by your pants.");
					}
					else {
						c.write(self.name() + " giggles as your flaccid dick is exposed");
					}
				}
				target.emote(Emotion.nervous, 10);
			}
			else if (target.bottom.peek().attribute() != Trait.indestructible
					&& target.stripAttempt(self.getEffective(Attribute.Power), self, c, target.bottom.peek())) {
				if (self.human()) {
					c.write(self, target.name() + " yelps in surprise as you rip " + target.possessive(false)
							+ " " + target.bottom.peek().getName() + " apart.");
				}
				else if (target.human()) {
					c.write(self, self.name() + " violently rips your " + target.bottom.peek().getName() + " off.");
				}
				target.shred(1);
				if (self.human() && target.nude()) {
					c.write(target, target.nakedLiner());
				}
				if (target.human() && target.pantsless()) {
					if (target.getArousal().get() >= 15) {
						c.write("Your boner springs out, no longer restrained by your pants.");
					}
					else {
						c.write(self.name() + " giggles as your flaccid dick is exposed");
					}
				}
				target.emote(Emotion.nervous, 10);
			}
			else {
				if (self.human()) {
					c.write(self, "You try to tear apart " + target.name() + "'s " + target.bottom.peek().getName()
							+ ", but the material is more durable than you expected.");
				}
				else if (target.human()) {
					c.write(self, self.name() + " yanks on your " + target.bottom.peek().getName() + ", but fails to "
							+ "remove them.");
				}
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Tear(user);
	}

	public String toString() {
		if (self.getEffective(Attribute.Animism) >= 12) {
			return "Shred Clothes";
		}
		else {
			return "Tear Clothes";
		}
	}

	@Override
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return null;
	}
}
