package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import items.Toy;

/**
 * Defines a skill where a character uses their riding crop as a warning to demoralize their opponent
 */
public class WarningCrop extends Skill {
	public WarningCrop(Character self) {
		super("Warning Crop", self);
		addTag(Attribute.Discipline);
		addTag(SkillTag.TOY);
		addTag(SkillTag.CROP);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Discipline) >= 3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& (self.has(Toy.Crop) || self.has(Toy.Crop2) || self.has(Toy.Crop3))
				&& (c.stance.reachTop(self) || c.stance.reachBottom(self))
				&& c.stance.mobile(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = 2 + Global.random(8) + (self.getEffective(Attribute.Discipline) / 3);
		damage = self.bonusProficiency(Anatomy.toy, damage);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.VALERIE) {
				c.offerImage("Valerie Crop.png", "Art by AimlessArt");
			}
		}
		target.pain(damage, Anatomy.fingers, c);
		target.spendMojo(2 * damage);
		target.emote(Emotion.nervous, 70);
	}

	@Override
	public Skill copy(Character user) {
		return new WarningCrop(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "Just as it looks like %target% is thinking of striking you, you lash out with your riding crop. The tip "
				+ "of it strikes across the back of %hisher% hand and %heshe% immediately pulls it back. That should "
				+ "make %himher% think twice about that.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "Just as your mind flashes to the possibility of striking at %self%, %heshe% lashes out with %hisher% "
				+ "riding crop, striking across the back of your hand. You pull your hand back on instinct. %HeShe% must "
				+ "have seen through you and knew what you were thinking. You’ll have to be more careful in the future.";
	}

	@Override
	public String describe() {
		return "Use a light crop strike to demoralize your opponent";
	}
}
