package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Rewired;

/**
 * Defines a skill where a character uses a blast of energy to confuse their opponent's senses
 */
public class ShortCircuit extends Skill {
	public ShortCircuit(Character self) {
		super("Short-Circuit", self);
		addTag(Attribute.Science);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science) >= 15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(Pool.BATTERY, 3)
				&& target.nude()
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "A blast of energy with confused your opponent's nerves so she can't tell pleasure from pain: 3 Batteries.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.BATTERY, 3);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.add(new Rewired(target, 4 + Global.random(3)), c);
	}

	@Override
	public Skill copy(Character user) {
		return new ShortCircuit(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You send a light electrical current through %target%'s body, disrupting %hisher% nerve endings. %HeShe%'ll "
				+ "temporarily feel pleasure as pain and pain as pleasure.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% aims a devices at you and you feel a strange shiver run across your skin. You feel indescribably "
				+ "weird. %HeShe%'s done something to your sense of touch.";
	}
}
