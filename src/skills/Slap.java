package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character slaps their opponent's face
 */
public class Slap extends Skill {
	public Slap(Character self) {
		super("Slap", self);
		this.speed = 8;
		addTag(SkillTag.PAINFUL);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.has(Trait.softheart)
				&& c.stance.reachTop(self)
				&& !c.stance.behind(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			if (self.getPure(Attribute.Animism) >= 12) {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.special, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.special, target));
				}
				if (self.has(Trait.pimphand)) {
					target.pain(Global.random(16 * (self.getArousal().percent() / 100)) + self.getEffective(Attribute.Power) / 2 + target.getEffective(
							Attribute.Perception), Anatomy.head, c);
					target.calm(Global.random(4) + 2, c);
					target.emote(Emotion.nervous, 40);
					target.emote(Emotion.angry, 30);
					self.buildMojo(20);
				}
				else {
					target.pain(Global.random(12 * (self.getArousal().percent() / 100) + 1) + self.getEffective(
							Attribute.Power) / 2, Anatomy.head, c);
					target.calm(Global.random(5) + 4, c);
					target.emote(Emotion.nervous, 25);
					target.emote(Emotion.angry, 30);
					self.buildMojo(10);
				}
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.normal, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				if (self.has(Trait.pimphand)) {
					target.pain(Global.random(8) + 5 + target.getEffective(Attribute.Perception), Anatomy.head, c);
					target.calm(Global.random(4) + 2, c);
					target.emote(Emotion.nervous, 20);
					target.emote(Emotion.angry, 60);
					self.buildMojo(20);
				}
				else {
					target.pain(Global.random(5) + 4, Anatomy.head, c);
					target.calm(Global.random(5) + 4, c);
					target.emote(Emotion.nervous, 10);
					target.emote(Emotion.angry, 60);
					self.buildMojo(10);
				}
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return !user.has(Trait.softheart)
				&& !user.has(Trait.cursed)
				&& (user.getPure(Attribute.Power) >= 7 || self.has(Trait.pimphand));
	}

	@Override
	public Skill copy(Character user) {
		return new Slap(user);
	}

	public Tactics type() {
		return Tactics.damage;
	}

	public String toString() {
		if (self.getPure(Attribute.Animism) >= 12) {
			return "Tiger Claw";
		}
		else {
			return "Slap";
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%target% avoids your slap.";
		}
		else if (modifier == Result.special) {
			return "You channel your bestial power and strike %target% with a solid open hand strike.";
		}
		else {
			return "You slap %target%'s cheek; not hard enough to really hurt %himher%, but enough to break %hisher% "
					+ "concentration.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to slap you but you catch %hisher% wrist.";
		}
		else if (modifier == Result.special) {
			return "%self%'s palm hits you in a savage strike that makes your head ring.";
		}
		else {
			return "%self% slaps you across the face, leaving a stinging heat on your cheek.";
		}
	}

	@Override
	public String describe() {
		return "Slap opponent across the face";
	}
}
