package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Mount;

/**
 * Defines a skill where a character straddles their opponent
 */
public class Straddle extends Skill {
	public Straddle(Character self) {
		super("Mount", self);
		this.speed = 6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.mobile(self)
				&& c.stance.mobile(target)
				&& c.stance.prone(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		c.stance = new Mount(self, target);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Straddle(user);
	}

	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You straddle %target% using your body weight to hold %himher% down.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% plops %himher%self down on top of your stomach.";
	}

	@Override
	public String describe() {
		return "Straddles opponent";
	}
}
