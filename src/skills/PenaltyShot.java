package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Mount;
import stance.Stance;

/**
 * Defines a skill where a character distracts their opponent by kicking them, then performs a knee strike
 */
public class PenaltyShot extends Skill {
    public PenaltyShot(Character self) {
        super("Penalty Shot", self);
        this.accuracy = 7;
        this.speed = 2;
        addTag(Attribute.Footballer);
        addTag(SkillTag.PAINFUL);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer) >= 12;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canAct()
                && self.canSpend(10)
                && target.canAct()
                && (c.stance.en == Stance.mount || c.stance.en == Stance.pin)
                && c.stance.sub(self)
                && !c.stance.penetration(self)
                && !c.stance.penetration(target);
    }

    @Override
    public String describe() {
        return "Kiss your opponent to distract them, then surprise them with a knee strike: 10 Mojo";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spendMojo(10);
        target.tempt(Global.random(4) + self.getEffective(Attribute.Seduction) / 4, c);
        if (c.attackRoll(this, self, target)) {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.normal, target));
            }
            else if (target.human()) {
                c.write(self, formattedReceive(0, Result.normal, target));
            }
            target.pain(4 + Global.random(11) + self.getEffective(Attribute.Footballer), Anatomy.genitals, c);
            if (c.stance.en == Stance.pin) {
                c.stance = new Mount(target, self);
            }
        }
        else {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.miss, target));
            }
            else if (target.human()) {
                c.write(self, formattedReceive(0, Result.miss, target));
            }
        }
        if (c.stance.en == Stance.pin) {
            c.stance = new Mount(target, self);
        }
    }

    @Override
    public Skill copy(Character user) {
        return new PenaltyShot(user);
    }

    @Override
    public Tactics type() {
        return Tactics.damage;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if (modifier == Result.miss) {
            return "You pull %target% down into a passionate kiss. Your tongue tangles with %hishers%, but when you try "
                    + "to move your legs, %heshe%'s not as distracted as you hoped. %HeShe% braces %hisher% body and "
                    + "holds you in place.";
        }
        return "You pull %target% down into a passionate kiss. Your skilled tongue completely captures %hisher% attention, "
                + "and %heshe% seems too distracted to notice that you are slowly parting %hisher% thighs with your knees. "
                + "You catch %himher% by surprise and drive your knee upwards into %hisher% groin.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if (modifier == Result.miss) {
            return "With an alluring twinkle in %hisher% eye, %self% grabs your shoulders and pulls you down into a "
                    + "passionate kiss. Your mind almost goes blank, but you still feel %himher% try to push your legs "
                    + "open. You quickly clamp your thighs together to stop %himher%.";
        }
        return "With an alluring twinkle in %hisher% eye, %self% grabs your shoulders and pulls you down into a passionate "
                + "kiss. Your dick is rock hard and throbbing, and you see this as an excellent opportunity to reposition "
                + "your legs to penetrate %himher%. The moment you open your thighs, however, %heshe% pumps %hisher% knee "
                + "straight upwards between your legs, smashing your balls as well as your erection.";
    }
}
