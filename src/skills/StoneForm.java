package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.StoneStance;
import status.Stsflag;

/**
 * Defines a skill where a character boosts their pain resistance, but reduces their speed
 */
public class StoneForm extends Skill {
	public StoneForm(Character self) {
		super("Stone Form", self);
		addTag(Attribute.Ki);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ki) >= 12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.is(Stsflag.form)
				&& !c.stance.sub(self);
	}

	@Override
	public String describe() {
		return "Improves Pain Resistance rate at expense of Speed";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new StoneStance(self), c);
	}

	@Override
	public Skill copy(Character user) {
		return new StoneForm(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You tense your body to absorb and shrug off attacks.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% braces %himher%self to resist your attacks.";
	}
}
