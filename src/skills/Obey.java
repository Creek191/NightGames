package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import status.Stsflag;

/**
 * Defines a skill where a character awaits their master's next command
 */
public class Obey extends Skill {
	public Obey(Character self) {
		super("Obey", self);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.is(Stsflag.enthralled);
	}

	@Override
	public String describe() {
		return "Obey the succubus' every command";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
	}

	@Override
	public Skill copy(Character paramCharacter) {
		return new Obey(paramCharacter);
	}

	@Override
	public Tactics type() {
		return Tactics.misc;
	}

	@Override
	public String deal(int paramInt, Result paramTag, Character paramCharacter) {
		return "You patiently await your mistress' command";
	}

	@Override
	public String receive(int paramInt, Result paramTag, Character paramCharacter) {
		return "%self% stares ahead blankly, waiting for %hisher% orders.";
	}
}
