package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Masochistic;

/**
 * Defines a skill where a character fondles their opponent's balls, giving masochistic pleasure
 */
public class HandBall extends Skill {
	public HandBall(Character self) {
		super("Hand Ball", self);
		addTag(Attribute.Footballer);
		addTag(SkillTag.DOM);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Footballer) >= 9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.hasBalls()
				&& (target.pantsless() || (self.has(Trait.dexterous) && target.bottom.size() <= 1))
				&& c.stance.reachBottom(self)
				&& c.stance.behind(self);
	}

	@Override
	public String describe() {
		return "Grab your opponent's bits and teach them the pleasures of masochism.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = Global.random(self.getEffective(Attribute.Footballer)) + target.getEffective(Attribute.Perception) - (2 * target.bottom.size());
		if (self.human()) {
			c.write(self, formattedDeal(damage, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(damage, Result.normal, target));
		}
		target.tempt(Global.random(4) + self.getEffective(Attribute.Seduction) / 4, c);
		target.pain(damage, Anatomy.genitals, c);
		if (self.has(Trait.wrassler)) {
			target.calm(damage / 4, c);
		}
		else {
			target.calm(damage / 2, c);
		}
		self.buildMojo(10);
		target.emote(Emotion.angry, 15);
		self.add(new Masochistic(self), c);
		if (c.effectRoll(this, self, target, self.getEffective(Attribute.Footballer))) {
			target.add(new Masochistic(target), c);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new HandBall(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You grab %target%'s balls roughly, taking the fight out of %himher%. While %heshe%'s being cooperative, "
				+ "you pull %himher% into a passionate kiss. After swapping spit for a while, "
				+ (Global.random(2) == 0
				   ? "you give %hisher% testicles a hard squeeze for good measure."
				   : "You give %hisher% sack a couple light slaps.");
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% grabs your ballsack firmly, and you freeze instinctively. <i>\"You know what I could do to these "
				+ "tender things? It would hurt so good!\"</i> Before you can reply, %heshe% kisses you deeply. " +
				(Global.random(2) == 0
				 ? "You groan in pain as %heshe% tightens %hisher% grip on your testicles."
				 : "Without breaking the kiss, %heshe% slaps your tender balls three times.");
	}
}
