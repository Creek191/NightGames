package skills;

import characters.Character;
import combat.Combat;
import status.Enthralled;
import status.Stsflag;

/**
 * Base class for skills where the player gives an enthralled NPC a command
 */
public abstract class PlayerCommand extends Skill {

	public PlayerCommand(String name, Character self) {
		super(name, self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.human();
	}

	@Override
	public Tactics type() {
		return Tactics.demand;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.human()
				&& target.is(Stsflag.enthralled)
				&& ((Enthralled) target.getStatus(Stsflag.enthralled)).master.equals(self)
				&& !c.stance.penetration(self);
	}
}
