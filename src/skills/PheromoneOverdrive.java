package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Stsflag;

/**
 * Defines a skill where a character uses a burst of pheromones to turn on their opponent
 */
public class PheromoneOverdrive extends Skill {
	public PheromoneOverdrive(Character self) {
		super("Pheromone Pink Overdrive", self);
		addTag(Attribute.Animism);
		addTag(SkillTag.ULTIMATE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Animism) >= 30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(30)
				&& self.is(Stsflag.feral)
				&& (self.getArousal().percent() > target.getArousal().percent())
				&& c.stance.dom(self);
	}

	@Override
	public String describe() {
		return "Use an intense burst of pheromones to make your opponent as horny as you are: 30 mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(30);
		target.getArousal().set((self.getArousal().percent() * target.getArousal().max()) / 100);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.emote(Emotion.horny, 30);
	}

	@Override
	public Skill copy(Character user) {
		return new PheromoneOverdrive(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You take a deep breath to focus all your primal pheromones into a thick aura. You grab %target% and give "
				+ "%himher% a passionate kiss while you channel your pheromones into %hisher% body, instantly driving "
				+ "%himher% into heat.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% seems to glow with pink energy. It can't be! Is that a cloud of pheromones so dense it's visible "
				+ "to the naked eye? Before you can escape, %heshe% pulls you into a deep kiss. With your mouth occupied, "
				+ "you're forced to breathe %hisher% heady scent through your nose. The pheromone-filled smell overwhelms "
				+ "you and makes your cock painfully hard.";
	}
}
