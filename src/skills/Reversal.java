package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.*;

/**
 * Defines a skill where a character reverses their position with the opponent to end up on top
 */
public class Reversal extends Skill {
	public Reversal(Character self) {
		super("Reversal", self);
		this.accuracy = 4;
		this.speed = 4;
		addTag(Attribute.Cunning);
		addTag(SkillTag.ESCAPE);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(15)
				&& c.stance.sub(self)
				&& !c.stance.mobile(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(15);
		if (c.effectRoll(this,
				self,
				target,
				self.getEffective(Attribute.Cunning) / 4 - (c.stance.escapeDC(target, self)))) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			if (c.stance.penetration(self)) {
				if (self.hasDick() || self.has(Trait.strapped)) {
					if (c.stance.behind(self)) {
						if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Kat")) {
							c.offerImage("Doggy Style.jpg", "Art by AimlessArt");
						}
						if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Mara")) {
							c.offerImage("Doggy Style Mara.jpg", "Art by AimlessArt");
						}
						c.stance = new Doggy(self, target);
					}
					else if (c.stance.en == Stance.flying) {
						if (self.getPure(Attribute.Dark) >= 18) {
							c.stance = new Flying(self, target);
						}
						else if (self.getPure(Attribute.Ninjutsu) >= 5) {
							c.stance = new Neutral(self, target);
						}
						else {
							c.stance = c.stance.insert(self);
						}
					}
					else {
						if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Angel")) {
							c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri");
							c.offerImage("Angel_Sex.jpg", "Art by AimlessArt");
						}
						c.stance = new Missionary(self, target);
					}
				}
				else {
					if (c.stance.prone(self)) {
						if (!Global.checkFlag(Flag.exactimages) || self.name().startsWith("Cassie")) {
							c.offerImage("Cowgirl.jpg", "Art by AimlessArt");
						}
						if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SAMANTHA) {
							c.offerImage("Samantha Cowgirl.jpg", "Art by AimlessArt");
						}
						c.stance = new Cowgirl(self, target);
					}
					else if (c.stance.en == Stance.flying) {
						if (self.getPure(Attribute.Dark) >= 18) {
							c.stance = new Flying(self, target);
						}
						else if (self.getPure(Attribute.Ninjutsu) >= 5) {
							c.stance = new Neutral(self, target);
						}
						else {
							c.stance = c.stance.insert(self);
						}
					}
					else {
						c.stance = new ReverseCowgirl(self, target);
					}
				}
			}
			else {
				c.stance = new Pin(self, target);
			}
			target.emote(Emotion.nervous, 10);
			self.emote(Emotion.dominant, 10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
			c.stance.struggle(c);
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Cunning) >= 24;
	}

	@Override
	public Skill copy(Character user) {
		return new Reversal(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You try to get on top of %target%, but %heshe%'s apparently more ready for it than you realized.";
		}
		else {
			return "You take advantage of %target%'s distraction and put %himher% in a pin.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to reverse your hold, but you stop %himher%.";
		}
		else {
			return "%self% rolls you over and ends up on top.";
		}
	}

	@Override
	public String describe() {
		return "Take dominant position: 15 Mojo";
	}
}
