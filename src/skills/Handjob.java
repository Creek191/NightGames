package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.Stance;
import status.ProfMod;
import status.ProfessionalMomentum;
import status.Stsflag;

/**
 * Defines a skill where a character gives their opponent a handjob
 */
public class Handjob extends Skill {
	public Handjob(Character self) {
		super("Handjob", self);
		addTag(Result.touch);
		addTag(Attribute.Seduction);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.hasDick()
				&& (target.pantsless() || (self.has(Trait.dexterous) && target.bottom.size() <= 1))
				&& c.stance.reachBottom(self)
				&& (!c.stance.penetration(target) || c.stance.en == Stance.anal);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = 4 + target.getEffective(Attribute.Perception);
		var type = Result.normal;
		if (c.attackRoll(this, self, target)) {
			if (self.getEffective(Attribute.Seduction) >= 8) {
				damage += Global.random(self.getEffective(Attribute.Seduction) / 2);
				self.buildMojo(10);
			}
			else if (target.human()) {
				type = Result.weak;
			}
			if (self.getPure(Attribute.Professional) >= 3) {
				damage += self.getEffective(Attribute.Professional);
				type = Result.special;
				self.buildMojo(15);
				if (self.has(Trait.sexuallyflexible)) {
					self.add(new ProfessionalMomentum(self, self.getEffective(Attribute.Professional) * 5), c);
				}
				else {
					self.add(new ProfMod("Dexterous Momentum",
							self,
							Anatomy.fingers,
							self.getEffective(Attribute.Professional) * 5), c);
				}
			}
			if (self.is(Stsflag.shadowfingers)) {
				type = Result.critical;
			}
			if (self.human()) {
				c.write(self, formattedDeal(damage, type, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, type, target));
				c.offerImage("Handjob.jpg", "Art by AimlessArt");
				c.offerImage("Handjob2.jpg", "Art by AimlessArt");
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.JEWEL) {
					c.offerImage("Jewel Handjob.png", "Art by Fujin Hitokiri");
				}
			}
			damage = self.bonusProficiency(Anatomy.fingers, damage);
			target.pleasure(damage, Anatomy.genitals, Result.finisher, c);
			if (self.has(Trait.roughhandling)) {
				target.weaken(damage / 2, c);
				if (self.human()) {
					c.write(self, formattedDeal(damage, Result.unique, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(damage, Result.unique, target));
				}
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Handjob(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "You opportunistically give %hisher% balls some rough treatment.";
		}
		if (modifier == Result.miss) {
			return "You reach for %target%'s dick but miss.";
		}
		else if (modifier == Result.critical) {
			return "Your dexterous shadow tendrils wrap around %target%'s shaft. As you stroke, you let your shadows go "
					+ "wild, rubbing and massaging %hisher% sensitive member in a way no human appendages could.";
		}
		else if (modifier == Result.special) {
			switch (Global.random(3)) {
				case 0:
					return "You take hold of %target%'s cock and run your fingers over it briskly, hitting all the right "
							+ "spots.";
				case 1:
					return "Your hold on %target%'s dick tightens, and where once there were gentle touches there are now "
							+ "firm jerks.";
				default:
					return "You have latched on to %target%'s cock with both hands now, twisting them in a fierce milking "
							+ "movement and eliciting pleasured groans from %himher%.";
			}
		}
		else {
			return "You grab %target%'s cock and stroke it using the techniques you use when masturbating.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "%HeShe% roughly handles your balls, sapping some of your strength.";
		}
		if (modifier == Result.miss) {
			return "%self% grabs for your dick and misses.";
		}
		else if (modifier == Result.critical) {
			return "%self%'s bizarre shadow fingers coil around your dick and wriggle all over your length. A shiver runs "
					+ "through you at the strange, but extremely pleasurable sensation.";
		}
		else if (modifier == Result.special) {
			switch (Global.random(3)) {
				case 0:
					return "%self% takes hold of your cock and runs %hisher% fingers over it briskly, hitting all the right "
							+ "spots.";
				case 1:
					return "%self%'s hold on your dick tightens, and where once there were gentle touches there are now "
							+ "firm jerks.";
				default:
					return "%self% has latched on to your cock with both hands now, twisting them in a fierce milking "
							+ "movement and eliciting pleasured groans from you.";
			}
		}
		int r;
		if (!target.bottom.isEmpty()) {
			return "%self% slips %hisher% hand into your " + target.bottom.peek().getName() + " and strokes your dick.";
		}
		else if (modifier == Result.weak) {
			return "%self% clumsily fondles your crotch. It's not skillful by any means, but it's also not entirely "
					+ "ineffective.";
		}
		else {
			if (target.getArousal().get() < 15) {
				return "%self% grabs your soft penis and plays with the sensitive organ until it springs into readiness.";
			}

			else if ((r = Global.random(3)) == 0) {
				return "%self% strokes and teases your dick, sending shivers of pleasure up your spine.";
			}
			else if (r == 1) {
				return "%self% rubs the sensitive head of your penis and fondles your balls.";
			}
			else {
				return "%self% jerks you off like %heshe%'s trying to milk every drop of your cum.";
			}
		}
	}

	@Override
	public String toString() {
		if (self.getPure(Attribute.Professional) >= 3) {
			return "Pro Handjob";
		}
		else {
			return "Handjob";
		}
	}

	@Override
	public String describe() {
		if (self.getPure(Attribute.Professional) >= 3) {
			return "A professional handjob that increases effectiveness with repeated use";
		}
		else {
			return "Rub your opponent's dick";
		}
	}
}
