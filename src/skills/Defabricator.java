package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character uses a tool to remove an opponent's clothes
 */
public class Defabricator extends Skill {
	public Defabricator(Character self) {
		super("Defabricator", self);
		addTag(Attribute.Science);
		addTag(SkillTag.STRIPPING);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science) >= 18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(Pool.BATTERY, 8)
				&& c.stance.mobile(self)
				&& !c.stance.prone(self)
				&& !target.nude();
	}

	@Override
	public String describe() {
		return "Does what it says on the tin.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.BATTERY, 8);
		if (!target.check(Attribute.Speed, 5 + self.getEffective(Attribute.Science) / 2)) {
			target.nudify();
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
				c.write(target, target.nakedLiner());
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.weak, target));
				if (target.nude()) {
					c.write(target, target.nakedLiner());
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.weak, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Defabricator(user);
	}

	@Override
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.weak) {
			return "You charge up your Defabricator and point it in %target%'s general direction. %HeShe% jumps away just "
					+ "as it activates and you're only able to get %hisher% " + target.shredRandom().getName() + ".";
		}
		if (Global.random(2) == 0) {
			return "You ready your Defabricator then take aim at %target%, who tries to dash away. You're too fast for "
					+ "%himher% and %heshe% soon finds %himher%self basking in the light, and then completely naked. "
					+ "Isn't technology awesome?";
		}
		else {
			return "You charge up your Defabricator and point it in %target%'s general direction. A bright light engulfs "
					+ "%himher% and %hisher% clothes are disintegrated in moments.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.weak) {
			return "%self% points a device at you and light shines from it like it's a simple flashlight. Instinct causes "
					+ "you to dive out of the way at the last moment. When you land, you realize your "
					+ target.shredRandom().getName() + " is gone.";
		}
		if (Global.random(2) == 0) {
			return "%self% fidgets with a device on %hisher% arm, then extends it towards you. You are immediately "
					+ "engulfed in a bright light that leaves you without any clothing, much to %hisher% pleasure.";
		}
		else {
			return "%self% points a device at you and light shines from it like it's a simple flashlight. The device's "
					+ "function is immediately revealed as your clothes just vanish in the light. You're left naked in "
					+ "seconds.";
		}
	}
}
