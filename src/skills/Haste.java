package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Abuff;

/**
 * Defines a skill where a character buffs their speed
 */
public class Haste extends Skill {
	public Haste(Character self) {
		super("Haste", self);
		addTag(Attribute.Temporal);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal) >= 1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(Pool.TIME, 1)
				&& self.getStatusMagnitude("Speed buff") < 10
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Temporarily buffs your speed: 1 charge";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.TIME, 1);

		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new Abuff(self, Attribute.Speed, 10, 6), c);
	}

	@Override
	public Skill copy(Character user) {
		return new Haste(user);
	}

	@Override
	public Tactics type() {
		return Tactics.preparation;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You spend a stored time charge. The world around you appears to slow down as your personal time accelerates.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% hits a button on %hisher% wristwatch and suddenly speeds up. %HeShe% is moving so fast that %heshe% "
				+ "seems to blur.";
	}
}
