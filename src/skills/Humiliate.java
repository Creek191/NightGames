package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Shamed;
import status.Stsflag;

/**
 * Defines a skill where a character fills their opponent with shame using hypnosis
 */
public class Humiliate extends Skill {
	public Humiliate(Character self) {
		super("Humiliate", self);
		addTag(Attribute.Hypnosis);
		addTag(SkillTag.DOM);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Hypnosis) >= 6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& (target.is(Stsflag.charmed) || target.is(Stsflag.enthralled))
				&& c.stance.mobile(self)
				&& !c.stance.sub(self)
				&& !c.stance.behind(self)
				&& !c.stance.behind(target);
	}

	@Override
	public String describe() {
		return "Use hypnotic suggestion to fill your opponent with shame.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		if (target.is(Stsflag.shamed)) {
			var status = target.getStatus(Stsflag.shamed);
			if (status != null) {
				target.add(new Shamed(target, target.getStatusMagnitude("Shamed")), c);
			}
			else {
				target.add(new Shamed(target), c);
			}
		}
		else {
			target.add(new Shamed(target), c);
		}
		target.emote(Emotion.nervous, 30);
		target.emote(Emotion.desperate, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new Humiliate(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You manipulate all of %target%'s remaining inhibitions, making %himher% believe %heshe% is in the most "
				+ "shameful situation %heshe% can imagine.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% points out that you are exposing quite a lot to the audience. Somehow you failed to notice that "
				+ "you are naked and visibly aroused while the entire student body is watching. The Games are usually a "
				+ "more private matter, you didn't even realize you were the center of so much attention before %heshe% "
				+ "said something.";
	}
}
