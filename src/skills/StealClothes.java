package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;

/**
 * Defines a skill where a character steals their opponent's clothes to wear them
 */
public class StealClothes extends Skill {
	public StealClothes(Character self) {
		super("Steal", self);
		this.speed = 6;
		addTag(Attribute.Ninjutsu);
		addTag(SkillTag.DRESSING);
		addTag(SkillTag.STRIPPING);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu) >= 15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(10)
				&& ((c.stance.reachTop(self) && !target.topless() && self.canWear(target.top.peek()))
				|| (c.stance.reachBottom(self) && !target.pantsless() && self.canWear(target.bottom.peek())));
	}

	@Override
	public String describe() {
		return "Steal and put on an article of clothing: 10 Mojo.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(10);
		if (c.stance.reachTop(self) && !target.topless() && self.canWear(target.top.peek())) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.top, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.top, target));
			}
			self.wear(target.strip(0, c));
		}
		else if (c.stance.reachBottom(self) && !target.pantsless() && self.canWear(target.bottom.peek())) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.bottom, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.bottom, target));
			}
			self.wear(target.strip(1, c));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new StealClothes(user);
	}

	@Override
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.top) {
			return "Using your speed and trickery, you swipe %target%'s " + target.top.peek().getName() + " and put on "
					+ "your ill-gotten new duds.";
		}
		else {
			return "Using your speed and trickery, you swipe %target%'s " + target.bottom.peek().getName() + " and put "
					+ "on your ill-gotten spoils.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.top) {
			return "%self% dodges past you, fast enough that you lose sight of %himher%. You notice the absence of your "
					+ target.top.peek().getName() + " at almost the same time as you spot %himher% wearing it.";
		}
		else {
			return "%self% dodges past you, fast enough that you lose sight of %himher%. You notice the absence of your "
					+ target.bottom.peek().getName() + " at almost the same time as you spot %himher% wearing them.";
		}
	}
}
