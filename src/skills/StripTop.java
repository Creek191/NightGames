package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

/**
 * Defines a skill where a character strips their opponent's top clothes
 */
public class StripTop extends Skill {
	public StripTop(Character self) {
		super("Strip Top", self);
		this.speed = 3;
		addTag(SkillTag.STRIPPING);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !target.topless()
				&& c.stance.reachTop(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var strip = self.getEffective(Attribute.Cunning);
		if (target.stripAttempt(strip, self, c, target.top.peek())) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
				if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Jewel")) {
					c.offerImage("Strip Top Female.jpg", "Art by AimlessArt");
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			target.strip(0, c);
			if (self.getPure(Attribute.Cunning) >= 30 && !target.topless()) {
				if (target.stripAttempt(strip, self, c, target.top.peek())) {
					if (self.human()) {
						c.write(self, formattedDeal(0, Result.strong, target));
					}
					else if (target.human()) {
						c.write(self, formattedReceive(0, Result.strong, target));
					}
					target.strip(0, c);
					target.emote(Emotion.nervous, 10);
				}
			}
			if (self.human() && target.nude()) {
				c.write(target, target.nakedLiner());
			}
			target.emote(Emotion.nervous, 10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new StripTop(user);
	}

	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			switch (Global.random(2)) {
				case 1:
					return "%target% blocks your attempt to remove %hisher% " + target.top.peek().getName() + ".";
				default:
					return "You attempt to strip off %target%'s " + target.top.peek().getName() + ", but %heshe% shoves "
							+ "you away.";
			}
		}
		else if (modifier == Result.strong) {
			return "Taking advantage of the situation, you also manage to snag %hisher% " + target.top.peek().getName();
		}
		else {
			if (target.canAct()) {
				switch (Global.random(4)) {
					case 3:
						return "After a brief struggle, you manage to pull off %target%'s "
								+ target.top.peek().getName() + ".";
					case 2:
						return "With some resistance, you take off %target%'s " + target.top.peek().getName() + ".";
					case 1:
						return "You pull %target%'s " + target.top.peek().getName() + " up and over %hisher% head as "
								+ "%heshe% flails around trying to stop you.";
					default:
						return "%target% can't even put up a fight while you take off %hisher% "
								+ target.top.peek().getName() + ".";
				}
			}
			else {

				return "You remove %target%'s " + target.top.peek().getName() + " without any resistance.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			switch (Global.random(2)) {
				case 1:
					return "%self% pulls on your " + target.top.peek().getName() + ", but you push %himher% off before "
							+ "%heshe% can take it.";
				default:
					return "%self% tries to yank off your " + target.top.peek().getName() + ", but you manage to hang "
							+ "onto it.";
			}
		}
		else if (modifier == Result.strong) {
			return "Before you can react, %heshe% also strips off your " + target.top.peek().getName();
		}
		else {
			if (target.canAct()) {
				switch (Global.random(2)) {
					case 1:
						return "%self% gets a hold of your " + target.top.peek().getName() + " and pulls hard, almost "
								+ "knocking you over as %heshe% takes it off.";
					default:
						return "%self% grabs a hold of your " + target.top.peek().getName() + " and yanks it off before "
								+ "you can stop %himher%.";
				}
			}
			else {
				switch (Global.random(2)) {
					case 1:
						return "%self% casually pulls off your " + target.top.peek().getName() + " while you're "
								+ "incapacitated.";
					default:
						return "%self% easily strips off your " + target.top.peek().getName() + " while you are unable to "
								+ "fight back.";
				}
			}
		}
	}

	@Override
	public String describe() {
		return "Attempt to remove opponent's top. More likely to succeed if she's weakened and aroused";
	}
}
