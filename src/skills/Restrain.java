package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import stance.*;

/**
 * Defines a skill where a character restrains their opponent until they free themselves
 */
public class Restrain extends Skill {
	public Restrain(Character self) {
		super("Pin", self);
		this.accuracy = 4;
		this.speed = 2;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.mobile(self) && c.stance.prone(target) && self.canAct() && !c.stance.penetration(self) && !c.stance.penetration(
				target) &&
				(c.stance.enumerate() == Stance.mount || c.stance.enumerate() == Stance.reversemount);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.effectRoll(this, self, target, self.getEffective(Attribute.Power) / 3)) {
			if (self.getPure(Attribute.Ki) >= 12) {
				if (c.stance.enumerate() == Stance.reversemount) {
					c.stance = new SubmissionPin(self, target);
					if (self.human()) {
						c.write(self, formattedDeal(1, Result.special, target));
					}
					else if (target.human()) {
						c.write(self, formattedReceive(1, Result.special, target));
					}
				}
				else {
					c.stance = new PleasurePin(self, target);
					if (self.human()) {
						c.write(self, formattedDeal(0, Result.special, target));
					}
					else if (target.human()) {
						c.write(self, formattedReceive(0, Result.special, target));
					}
				}
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.normal, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				if (c.stance.enumerate() == Stance.reversemount) {
					c.stance = new ReversePin(self, target);
				}
				else {
					c.stance = new Pin(self, target);
				}
			}
			target.emote(Emotion.nervous, 10);
			target.emote(Emotion.desperate, 10);
			self.emote(Emotion.dominant, 20);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power) >= 8;
	}

	@Override
	public Skill copy(Character user) {
		return new Restrain(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			if (damage == 1) {
				return "You restrain %target%'s arms and put intense pressure on %hisher% groin.";
			}
			else {
				return "You grab %target%'s wrists and press your thigh between %hisher% legs.";
			}
		}
		else if (modifier == Result.miss) {
			return "You try to catch %target%'s hands, but %heshe% squirms too much to keep your grip on %himher%.";
		}
		else {
			return "You manage to restrain %target%, leaving %himher% helpless and vulnerable beneath you.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			if (damage == 1) {
				return "%self% pins your arms with %hisher% thighs and grabs you by the balls.";
			}
			else {
				return "%self% pins your hands and presses %hisher% knee into your groin, leaving you at %hisher% mercy.";
			}
		}
		else if (modifier == Result.miss) {
			return "%self% tries to pin you down, but you keep your arms free.";
		}
		else {
			return "%self% pounces on you and pins your arms in place, leaving you at %hisher% mercy.";
		}
	}

	@Override
	public String describe() {
		return "Restrain opponent until she struggles free";
	}
}
