package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a command where an enthralled character is forced to hurt themselves
 */
public class CommandHurt extends PlayerCommand {
	public CommandHurt(Character self) {
		super("Force Pain", self);
		addTag(SkillTag.COMMAND);
	}

	@Override
	public String describe() {
		return "Convince your thrall running into the nearest wall is a good idea.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (Global.random(2) == 0) {
			target.pain(Global.random(10) + target.getEffective(Attribute.Power) / 2, Anatomy.genitals, c);
			c.write(self, formattedDeal(0, Result.strong, target));
		}
		else {
			target.pain(Global.random(10) + target.getEffective(Attribute.Speed), Anatomy.head, c);
			c.write(self, formattedDeal(0, Result.normal, target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new CommandHurt(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		if (modifier == Result.strong) {
			return " You glare at %target% and point towards your own groin with a nod. %target%'s hand reaches out in "
					+ "front of %himher%, then %heshe% swiftly and firmly slaps %himher%self in the privates.";
		}
		return "Grinning, you point towards the nearest wall. %target% seems confused for a moment, but soon %heshe% "
				+ "understands your meaning and runs headfirst into it.";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The Silver Bard: CommandHurt-receive>>";
	}
}
