package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

/**
 * Defines a skill where a character knees their opponent in the groin
 */
public class Knee extends Skill {
	public Knee(Character self) {
		super("Knee", self);
		this.speed = 4;
		addTag(Attribute.Power);
		addTag(SkillTag.PAINFUL);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.has(Trait.sportsmanship)
				&& c.stance.mobile(self)
				&& !c.stance.prone(self)
				&& !c.stance.behind(target)
				&& !c.stance.penetration(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				if (c.stance.prone(target)) {
					c.write(self, formattedReceive(0, Result.special, target));
				}
				else {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				if (Global.random(5) >= 3) {
					c.write(self, self.bbLiner());
				}
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.CASSIE) {
					c.offerImage("Knee.png", "Art by AimlessArt");
				}
			}
			var damage = 4 + Global.random(11) + self.getEffective(Attribute.Power);
			target.pain(damage, Anatomy.genitals, c);
			if (self.has(Trait.wrassler)) {
				target.calm(damage / 2, c);
			}
			else {
				target.calm(damage, c);
			}
			self.buildMojo(10);
			target.emote(Emotion.angry, 75);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power) >= 10 && !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new Knee(user);
	}

	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%target% blocks your knee strike.";
		}
		if (target.hasBalls()) {
			return "You deliver a powerful knee strike to %target%'s dangling balls. %HeShe% lets out a pained whimper "
					+ "and nurses %hisher% injured parts.";
		}
		else {
			return "You deliver a powerful knee strike to %target%'s delicate lady flower. %HeShe% lets out a pained "
					+ "whimper and nurses %hisher% injured parts.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to knee you in the balls, but you block it.";
		}
		if (modifier == Result.special) {
			return "%self% raises one leg into the air, then brings %hisher% knee down like a hammer onto your balls. "
					+ "You cry out in pain and instinctively try to close your legs, but %heshe% holds them open.";
		}
		else {
			switch (Global.random(2)) {
				case 1:
					return "%self% manages to get one of %hisher% legs in between yours, and jerks %hisher% knee upwards, "
							+ "hitting you squarely in the balls. You go cross-eyed for a moment as the pain works its way "
							+ "up your stomach and into your throat.";
			}
			return "%self% steps in close and brings %hisher% knee up between your legs, crushing your fragile balls. "
					+ "You groan and nearly collapse from the intense pain in your abdomen.";
		}
	}

	@Override
	public String describe() {
		return "Knee opponent in the groin";
	}
}
