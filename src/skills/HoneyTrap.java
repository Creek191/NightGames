package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character performs a sneak attack while feinting at penetration
 */
public class HoneyTrap extends Skill {
	public HoneyTrap(Character self) {
		super("Honey Trap", self);
		addTag(Attribute.Submissive);
		addTag(SkillTag.PAINFUL);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getEffective(Attribute.Submissive) >= 21;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(20)
				&& self.pantsless()
				&& target.canAct()
				&& (target.pantsless() || target.has(Trait.strapped))
				&& ((target.hasDick() || (target.has(Trait.strapped)) && !c.stance.behind(self))
				|| !c.stance.behind(target))
				&& c.stance.mobile(target)
				&& !c.stance.mobile(self)
				&& !c.stance.penetration(self)
				&& !c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "A sneak attack right at the moment of greatest anticipation: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(20);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
			if (Global.random(5) >= 3) {
				c.write(self, self.bbLiner());
			}
		}
		var damage = Math.max(Math.max(self.getEffective(Attribute.Power), self.getEffective(Attribute.Cunning)),
				self.getEffective(Attribute.Seduction));
		var bonus = self.getEffective(Attribute.Submissive);
		if (target.getMood() == Emotion.dominant || target.getMood() == Emotion.horny) {
			bonus *= 2;
		}
		target.pain(damage + bonus, Anatomy.genitals, c);
		if (self.has(Trait.wrassler)) {
			target.calm(damage / 2, c);
		}
		else {
			target.calm(damage, c);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new HoneyTrap(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You slide your eager cock against %target%'s pussy lips, making it clear what you're craving. %HeShe% "
				+ "grins, confident in %hisher% victory, and positions %himher%self over you. You wait patiently until "
				+ "the moment %hisher% guard is completely down. Before %heshe% can fuck you, you deliver a sudden painful "
				+ "slap to %hisher% exposed pussy. For a moment, %hisher% expression is pure shock, then %hisher% eyes "
				+ "water as the pain hits %himher%.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% opens %hisher% lower lips in a lewd invitation and looks at you with needy eyes. There's no way "
				+ "you'd turn down such a tempting offer. You line up your cock with %hisher% dripping entrance and prepare "
				+ "to penetrate %himher%. Suddenly %hisher% knee darts up and impacts your dangling balls. It's so fast "
				+ "that for a moment, you aren't even sure what happened. The pain hits you like a truck, and you're soon "
				+ "unable to do anything but clutch your poor testicles.";
	}
}
