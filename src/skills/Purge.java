package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;

import java.util.ArrayList;

/**
 * Defines a skill where a character purges an opponent from all abnormal status, turning them into pleasure
 */
public class Purge extends Skill {
    public Purge(Character self) {
        super("Purge", self);
        addTag(Attribute.Spirituality);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Spirituality) >= 6;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c)
                && self.canSpend(Pool.FOCUS, 2)
                && !target.getStatus().isEmpty();
    }

    @Override
    public String describe() {
        return "Convert your opponent's abnormal statuses into temptation.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.FOCUS, 2);

        var count = target.getStatus().size();
        target.tempt(20 * count, c);
        if (self.human()) {
            c.write(self, formattedDeal(0, Result.normal, target));
        }
        else if (target.human()) {
            c.write(self, formattedReceive(0, Result.normal, target));
        }
        var copy = new ArrayList<>(target.getStatus());
        for (var status : copy) {
            target.removeStatus(status, c);
        }
    }

    @Override
    public Skill copy(Character user) {
        return new Purge(user);
    }

    @Override
    public Tactics type() {
        return Tactics.pleasure;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "You use a spiritual cleansing ritual to purge all the abnormalities from %target%. As a pleasant "
                + "side-effect, it should be highly arousing.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return "%self% waves %hisher% staff at you. You feel... clean... like everything has been forcefully purged from "
                + "your spirit. For whatever reason, you also feel weirdly turned on.";
    }
}
