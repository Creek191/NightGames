package skills;

import characters.Character;
import combat.Combat;
import items.Item;

/**
 * Base class for skills that use an item during combat
 */
public abstract class UseItem extends Skill {
	private final Item consumable;

	public UseItem(Item item, Character self) {
		super(item.getFullName(self), self);
		consumable = item;
		addTag(SkillTag.CONSUMABLE);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c) && self.has(consumable);
	}

	@Override
	public String describe() {
		return consumable.getFullDesc(self);
	}

	@Override
	public Tactics type() {
		return Tactics.preparation;
	}
}
