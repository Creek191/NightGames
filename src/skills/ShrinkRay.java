package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Shamed;

/**
 * Defines a skill where a character uses a shrink ray on their opponent's assets to shame them
 */
public class ShrinkRay extends Skill {
	public ShrinkRay(Character self) {
		super("Shrink Ray", self);
		addTag(Attribute.Science);
		addTag(SkillTag.MISCHIEF);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science) >= 12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(Pool.BATTERY, 2)
				&& target.nude()
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Briefly shrink your opponent's 'assets' to damage her ego: 2 Batteries";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.BATTERY, 2);
		if (self.human()) {
			if (target.hasDick()) {
				c.write(self, formattedDeal(0, Result.special, target));
			}
			else {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.add(new Shamed(target), c);
		target.spendMojo(15);
		target.emote(Emotion.nervous, 20);
		target.emote(Emotion.desperate, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new ShrinkRay(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "You aim your shrink ray at %target%'s cock, shrinking %hisher% male anatomy. %HeShe% turns red and "
					+ "glares at you in humiliation.";
		}
		else {
			return "You use your shrink ray to turn %target%'s breasts into A cups. %HeShe% whimpers and covers %hisher% "
					+ "chest in shame.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% points a device at your groin and giggles as your genitals shrink. You flush in shame and cover "
				+ "yourself. The effect wears off quickly, but the damage to your dignity lasts much longer.";
	}
}
