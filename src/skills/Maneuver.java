package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import stance.Behind;

/**
 * Defines a skill where a character maneuvers themselves behind their opponent
 */
public class Maneuver extends Skill {
	public Maneuver(Character self) {
		super("Maneuver", self);
		this.accuracy = 6;
		this.speed = 8;
		addTag(Attribute.Cunning);
		addTag(SkillTag.OUTMANEUVER);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(8)
				&& !self.has(Trait.undisciplined)
				&& c.stance.mobile(self)
				&& !c.stance.behind(self)
				&& !c.stance.penetration(self)
				&& !c.stance.prone(self)
				&& !c.stance.prone(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(8);
		if (target.has(Trait.reflexes)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.unique, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.unique, target));
			}
			self.pain(target.getLevel(), Anatomy.genitals);
			self.emote(Emotion.nervous, 10);
			target.emote(Emotion.dominant, 15);
		}
		else if (c.effectRoll(this, self, target, self.getEffective(Attribute.Cunning) / 4)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			c.stance = new Behind(self, target);
			self.emote(Emotion.confident, 15);
			self.emote(Emotion.dominant, 15);
			target.emote(Emotion.nervous, 10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Cunning) >= 20;
	}

	@Override
	public Skill copy(Character user) {
		return new Maneuver(user);
	}

	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "You dodge past %target%, but when you try to grab %himher%, %heshe% dodges forward and counters with "
					+ "a heel kick to the groin.<p><i>\"Way too slow.\"</i> %HeShe% scolds you as you hold your injured plums.";
		}
		if (modifier == Result.miss) {
			return "You try to get behind %target% but are unable to.";
		}
		else {
			return "You dodge past %target%'s guard and grab %himher% from behind.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to slip behind you, but you're able to keep %himher% in sight.";
		}
		else {
			return "%self% lunges at you, but when you try to grab %himher%, %heshe% ducks out of sight. Suddenly %hisher% "
					+ "arms are wrapped around you. How did %heshe% get behind you?";
		}
	}

	@Override
	public String describe() {
		return "Get behind opponent: 8 Mojo";
	}
}
