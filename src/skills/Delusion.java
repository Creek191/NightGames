package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;

public class Delusion extends Skill {
	// TODO: Unused
	public Delusion(Character user) {
		super("Delusion", user);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Unknowable) >= 6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(Pool.ENIGMA, 2)
				&& c.stance.penetration(self);
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.ENIGMA, 2);
		if (target.getEffective(Attribute.Spirituality) >= 2) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
		else if (c.isWatching(ID.ANGEL)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.defended, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.defended, target));
			}
		}
		else {
			var damage = self.getSexPleasure(3,
					Attribute.Seduction) + target.getEffective(Attribute.Perception) + self.getEffective(Attribute.Unknowable);
			damage += self.getMojo().get() / 5;

			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}

			target.pleasure(damage, Anatomy.genitals, Result.finisher, c);
			var recoil = target.getSexPleasure(3, Attribute.Seduction);
			recoil += target.bonusRecoilPleasure(recoil);
			if (self.has(Trait.experienced)) {
				recoil = recoil / 2;
			}
			self.pleasure(recoil, Anatomy.genitals, Result.finisher, c);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Delusion(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "";
		}
		else if (modifier == Result.defended) {
			return "";
		}
		else {
			return "";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "";
		}
		else if (modifier == Result.defended) {
			return "[Placeholder: Angel Assists]";
		}
		else {
			return "";
		}
	}
}
