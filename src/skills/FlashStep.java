package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Behind;

/**
 * Defines a skill where a character moves behind their opponent as quick as lightning
 */
public class FlashStep extends Skill {
	public FlashStep(Character self) {
		super("Flash Step", self);
		this.speed = 99;
		addTag(Attribute.Ki);
		addTag(SkillTag.OUTMANEUVER);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ki) >= 6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.mobile(self)
				&& !c.stance.behind(self)
				&& !c.stance.penetration(self)
				&& !c.stance.prone(self)
				&& !c.stance.prone(target);
	}

	@Override
	public String describe() {
		return "Use lightning speed to get behind your opponent before she can react: 10 stamina";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.weaken(10);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		c.stance = new Behind(self, target);
	}

	@Override
	public Skill copy(Character user) {
		return new FlashStep(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You channel your ki into your feet and dash behind %target% faster than %hisher% eyes can follow.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% starts to move and suddenly vanishes. You hesitate for a second and feel %himher% grab you from "
				+ "behind.";
	}
}
