package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import status.Charmed;

/**
 * Defines a skill where a character kisses their opponent
 */
public class Kiss extends Skill {
	public Kiss(Character self) {
		super("Kiss", self);
		this.speed = 6;
		addTag(Attribute.Seduction);
		addTag(Result.kiss);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct() && c.stance.kiss(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = Global.random(4) + self.getEffective(Attribute.Seduction) / 4;
		if (self.has(Trait.greatkiss) && self.canSpend(10) && Global.random(4) == 3) {
			damage += Global.random(3);
			if (self.human()) {
				c.write(self, formattedDeal(damage, Result.special, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, Result.special, target));
			}

			target.add(new Charmed(target, 2 + self.bonusCharmDuration()), c);
			self.spendMojo(10);
		}
		else if (self.getEffective(Attribute.Seduction) >= 9) {
			if (self.human()) {
				c.write(self, formattedDeal(damage, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, Result.normal, target));
			}

			self.buildMojo(10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(damage, Result.weak, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, Result.weak, target));
			}

			self.buildMojo(5);
		}

		if (self.human()) {
			if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.JEWEL) {
				c.offerImage("Kiss.jpg", "Art by AimlessArt");
			}
			if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.CASSIE) {
				c.offerImage("Cassie Kiss.jpg", "Art by AimlessArt");
			}
			if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.ANGEL) {
				c.offerImage("Angel Kiss.jpg", "Art by AimlessArt");
			}
			if (false && !Global.checkFlag(Flag.exactimages) || target.id() == ID.SELENE) {
				c.offerImage("Selene Kissed.png", "Art by AimlessArt");
			}
		}
		else if (target.human()) {
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.JEWEL) {
				c.offerImage("Kissed.jpg", "Art by AimlessArt");
			}
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.ANGEL) {
				c.offerImage("Angel Kiss.jpg", "Art by AimlessArt");
			}
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.MARA) {
				c.offerImage("Mara kiss.jpg", "Art by AimlessArt");
			}
			if (false && !Global.checkFlag(Flag.exactimages) || self.id() == ID.SELENE) {
				c.offerImage("Selene Kiss.png", "Art by AimlessArt");
			}
		}

		target.tempt(damage, self.bonusTemptation(), Result.foreplay, c);
		if (self.getEffective(Attribute.Seduction) >= 9) {
			target.pleasure(1, Anatomy.mouth, c);
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Kiss(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "You pull %target% to you and kiss %himher% passionately. You run your tongue over %hisher% lips "
					+ "until %heshe% opens them and immediately invade her mouth. You tangle your tongue around %hishers% "
					+ "and probe the sensitive insides of %hisher% mouth. As you finally break the kiss, %heshe% leans "
					+ "against you, looking kiss-drunk and needy.";
		}
		else if (modifier == Result.weak) {
			return "You aggressively kiss %target% on the lips. It catches %himher% off guard for a moment, but %heshe% "
					+ "soon responds approvingly.";
		}
		else {
			switch (Global.random(3)) {
				case 0:
					return "You pull %target% close and capture %hisher% lips. %HeShe% returns the kiss enthusiastically "
							+ "and lets out a soft noise of approval when you push your tongue into %hisher% mouth.";
				case 1:
					return "You press your lips to %target%'s in a romantic kiss. You tease out %hisher% tongue and meet "
							+ "it with your own.";
				default:
					return "You kiss %target% deeply, overwhelming %hisher% senses and swapping quite a bit of saliva.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "%self% seductively pulls you into a deep kiss. At first you try to match %hisher% enthusiastic tongue "
					+ "with your own, but you're quickly overwhelmed. %HeShe% draws your tongue into %hisher% mouth and "
					+ "sucks on it in a way that seems to fill your mind with a pleasant, but intoxicating fog.";
		}
		else if (modifier == Result.weak) {
			return "%self% presses %hisher% lips against yours in a passionate, if not particularly skillful, kiss.";
		}
		else {
			switch (Global.random(3)) {
				case 0:
					return "%self% grabs you and kisses you passionately on the mouth. As you break for air, %heshe% gently nibbles on your bottom lip.";
				case 1:
					return "%self% peppers quick little kisses around your mouth before suddenly taking your lips forcefully and invading your mouth with %hisher% tongue.";
				default:
					return "%self% kisses you softly and romantically, slowly drawing you into %hisher% embrace. As you part, %heshe% teasingly brushes %hisher% lips against yours.";
			}
		}
	}

	@Override
	public String describe() {
		return "Kiss your opponent";
	}
}
