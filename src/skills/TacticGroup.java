package skills;

/**
 * Defines the available tactics groups a skill can belong to
 */
public enum TacticGroup {
	All,
	Pleasure,
	Positioning,
	Hurt,
	Misc,
	Recovery,
	Manipulation,
	Flask,
	Potion,
	Demand,
}
