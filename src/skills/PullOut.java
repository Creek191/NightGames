package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Stance;

/**
 * Defines a skill where a character stops fucking their opponent
 */
public class PullOut extends Skill {
	public PullOut(Character self) {
		super("Pull Out", self);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.penetration(self)
				&& c.stance.dom(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.stance.enumerate() == Stance.anal) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.anal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.anal, target));
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
		}
		c.stance = c.stance.insert(self);
	}

	@Override
	public Skill copy(Character user) {
		return new PullOut(user);
	}

	@Override
	public Tactics type() {
		return Tactics.misc;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.anal) {
			return "You pull your dick completely out of %target%'s ass.";
		}
		return "You pull completely out of %target%'s pussy, causing %himher% to let out a disappointed little whimper.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.anal) {
			return "You feel the pressure in your anus recede as %self% pulls out.";
		}
		else {
			return "%self% lifts %hisher% hips more than normal, letting your dick slip completely out of %himher%.";
		}
	}

	@Override
	public String describe() {
		return "Aborts penetration";
	}
}
