package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;
import items.Attachment;
import items.Toy;
import status.Stsflag;

/**
 * Defines a skill where the user lets loose a barrage of crop strikes
 */
public class BerserkerBarrage extends Skill {

	public BerserkerBarrage(Character self) {
		super("Berserker Barrage", self);
		addTag(Attribute.Discipline);
		addTag(SkillTag.PAINFUL);
		addTag(SkillTag.CROP);
		addTag(SkillTag.TOY);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Discipline) >= 18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !self.human()
				&& self.is(Stsflag.broken)
				&& self.canSpend(20)
				&& self.canAct()
				&& c.stance.mobile(self)
				&& (c.stance.reachTop(self) || c.stance.reachBottom(self))
				&& (self.has(Toy.Crop) || self.has(Toy.Crop2) || self.has(Toy.Crop3));
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(20);

		var damage = 10 + Global.random(14)
				+ target.getEffective(Attribute.Perception)
				+ (self.getEffective(Attribute.Science) / 2)
				+ (self.getEffective(Attribute.Discipline));
		damage = self.bonusProficiency(Anatomy.toy, damage);
		if (self.has(Toy.Crop3)) {
			damage *= 2.5;
		}
		else if (self.has(Attachment.CropShocker)) {
			damage *= 2;
		}

		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.pain(damage, Anatomy.ass, c);
		target.emote(Emotion.nervous, 30);
	}

	@Override
	public Skill copy(Character user) {
		return new BerserkerBarrage(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "Something within you snaps and anger overwhelms you so much you lose all sense of what you're doing for a "
				+ "few moments. By the time you realize what you're doing again, the only evidence to what happened in the "
				+ "lost time is a set of marks on %target%'s skin, evidence to a series of vicious strikes with your "
				+ "riding crop. You almost feel sorry for %himher%. Almost.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "<i>\"Grrrahhhh!\"</i> The war cry barely serves as a warning before %self% lets loose with a flurry of "
				+ "strikes with %hisher% riding crop. You try in vain to shield yourself from the strikes, but there's "
				+ "nothing you can do to keep from being hit again and again by %hisher% crop, each strike inflicting "
				+ "more pain than the last. When it's all over, you catch sight of %self% looking at you in mild confusion, "
				+ "as if %heshe% doesn't even know quite what %heshe%'s just done %himher%self. Not that that'll make you "
				+ "forgive %himher% for this.";
	}

	@Override
	public String describe() {
		return "Wildly strike your opponent with repeated crop strikes";
	}
}
