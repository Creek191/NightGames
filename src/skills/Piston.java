package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.Stance;
import status.ProfMod;
import status.ProfessionalMomentum;

/**
 * Defines a skill where a character thrusts into their opponent without holding back
 */
public class Piston extends Skill {
	public Piston(Character self) {
		super("Piston", self);
		addTag(Attribute.Seduction);
		addTag(Result.intercourse);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.dom(self)
				&& c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = self.getSexPleasure(3, Attribute.Seduction) + target.getEffective(Attribute.Perception);
		Anatomy inside;
		if (c.stance.en == Stance.anal) {
			if (self.human()) {
				if (c.stance.behind(self)) {
					c.write(self, formattedDeal(0, Result.anal, target));
				}
				else {
					c.write(self, formattedDeal(0, Result.upgrade, target));
				}
			}
			else if (target.human()) {
				if (c.stance.behind(self)) {
					c.write(self, formattedReceive(0, Result.anal, target));
				}
				else {
					c.write(self, formattedReceive(0, Result.upgrade, target));
				}
			}
			inside = Anatomy.ass;
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(damage, Result.normal, target));
				if (c.stance.behind(self)) {
					if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.KAT) {
						c.offerImage("Doggy Style.jpg", "Art by AimlessArt");
					}
					if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.MARA) {
						c.offerImage("Doggy Style Mara.jpg", "Art by AimlessArt");
					}
				}
				else {
					if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.ANGEL) {
						c.offerImage("Angel_Sex.jpg", "Art by AimlessArt");
						c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri");
					}
					c.offerImage("Thrust.jpg", "Art by AimlessArt");
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, Result.normal, target));
			}
			inside = Anatomy.genitals;
		}
		damage += self.getMojo().get() / 10;
		if (self.has(Trait.strapped)) {
			damage += (self.getEffective(Attribute.Science) / 2);
			damage = self.bonusProficiency(Anatomy.toy, damage);
		}
		if (self.getPure(Attribute.Professional) >= 11) {
			if (self.has(Trait.sexuallyflexible)) {
				self.add(new ProfessionalMomentum(self, self.getEffective(Attribute.Professional) * 5), c);
			}
			else {
				self.add(new ProfMod("Sexual Momentum",
						self,
						Anatomy.genitals,
						self.getEffective(Attribute.Professional) * 5), c);
			}
		}
		target.pleasure(damage, inside, Result.finisher, c);
		var recoil = target.getSexPleasure(3, Attribute.Seduction);
		recoil += target.bonusRecoilPleasure(recoil);
		if (self.has(Trait.experienced)) {
			recoil = recoil / 2;
		}

		self.pleasure(recoil, Anatomy.genitals, Result.finisher, c);
		self.buildMojo(20);
		c.stance.setPace(2);
	}

	@Override
	public Skill copy(Character user) {
		return new Piston(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.anal || modifier == Result.upgrade) {
			return "You pound %target% in the ass. %HeShe% whimpers in pleasure and can barely summon the strength to "
					+ "hold %himher%self off the floor.";
		}
		else {
			return "You rapidly pound your dick into %target%'s pussy. %HisHer% pleasure-filled cries are proof that "
					+ "you're having an effect, but you're feeling it as much as %heshe% is.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.anal) {
			return "%self% relentlessly pegs you in the ass as you groan and try to endure the sensation.";
		}
		else if (modifier == Result.upgrade) {
			return "%self% pistons into you while pushing your shoulders on the ground. While %hisher% strap-on stimulates "
					+ "your prostate, %self%'s tits are shaking above your head.";
		}
		else {
			return "%self% enthusiastically moves %hisher% hips, bouncing on your cock. %HisHer% intense movements "
					+ "relentlessly drive you both toward orgasm.";
		}
	}

	@Override
	public String describe() {
		return "Fucks opponent without holding back. Very effective, but dangerous";
	}
}
