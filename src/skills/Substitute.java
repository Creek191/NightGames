package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import stance.Behind;
import status.Stsflag;

/**
 * Defines a skill where a character uses a decoy to slip behind their opponent
 */
public class Substitute extends Skill {
	public Substitute(Character self) {
		super("Substitution", self);
		addTag(Attribute.Ninjutsu);
		addTag(SkillTag.ESCAPE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu) >= 21;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canSpend(10)
				&& !self.is(Stsflag.enthralled)
				&& !self.bound()
				&& !self.stunned()
				&& !self.distracted()
				&& !c.stance.mobile(self)
				&& c.stance.sub(self);
	}

	@Override
	public String describe() {
		return "Use a decoy to slip behind your opponent: 10 Mojo.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(10);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}

		c.stance = new Behind(self, target);
		target.emote(Emotion.nervous, 10);
		self.emote(Emotion.dominant, 10);
	}

	@Override
	public Skill copy(Character user) {
		return new Substitute(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "By the time %target% realizes %heshe%'s pinning a dummy, you're already behind %himher%.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "You straddle %self%'s unresisting body. Wait... This is a blow-up doll. The real %self% is standing "
				+ "behind you. When-How- did %heshe% make the switch?";
	}
}
