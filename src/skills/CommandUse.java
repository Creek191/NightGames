package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import items.Flask;
import status.Hypersensitive;
import status.Oiled;
import status.Stsflag;

import java.util.Arrays;
import java.util.List;

/**
 * Defines a command where an enthralled character is forced to use an item on themselves
 */
public class CommandUse extends PlayerCommand {
	public static final List<Flask> CANDIDATES = Arrays.asList(
			Flask.Lubricant,
			Flask.SPotion);
	private Flask used;

	public CommandUse(Character self) {
		super("Force Item Use", self);
		used = null;
		addTag(SkillTag.COMMAND);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		if (!super.usable(c, target) || !target.nude()) {
			return false;
		}
		var usable = false;
		for (var candidate : CANDIDATES)
			if (target.has(candidate)) {
				switch (candidate) {
					case Lubricant:
						usable = !target.is(Stsflag.oiled);
						break;
					case SPotion:
						usable = !target.is(Stsflag.hypersensitive);
						break;
					default:
						break;
				}
			}
		return usable;
	}

	@Override
	public String describe() {
		return "Force your thrall to use a harmful item on themselves";
	}

	@Override
	public void resolve(Combat c, Character target) {
		do {
			used = CANDIDATES.get(Global.random(CANDIDATES.size()));
			var hasStatus = false;
			switch (used) {
				case Lubricant:
					hasStatus = target.is(Stsflag.oiled);
					break;
				case SPotion:
					hasStatus = target.is(Stsflag.hypersensitive);
					break;
				default:
					break;
			}
			if (!target.has(used) || hasStatus) {
				used = null;
			}
		} while (used == null);
		switch (used) {
			case Lubricant:
				target.add(new Oiled(target));
				c.write(self, formattedDeal(0, Result.normal, target));
				break;
			case SPotion:
				target.add(new Hypersensitive(target));
				c.write(self, formattedDeal(0, Result.special, target));
				break;
			default:
				c.write("<<This should not be displayed, please inform The Silver Bard: CommandUse-resolve>>");
				return;
		}
		target.consume(used, 1);
		used = null;
	}

	@Override
	public Skill copy(Character user) {
		return new CommandUse(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		switch (modifier) {
			case normal:
				return "%target% coats %himher%self in a shiny lubricant at your 'request'.";
			case special:
				return "Obediently, %target% smears a sensitivity potion on %himher%self.";
			default:
				return "<<This should not be displayed, please inform The Silver Bard: CommandUse-deal>>";
		}
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The Silver Bard: CommandUse-receive>>";
	}
}
