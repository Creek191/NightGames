package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.ReverseMount;
import stance.SixNine;
import status.ProfMod;
import status.ProfessionalMomentum;

/**
 * Defines a skill where the user gives the opponent a blowjob
 */
public class Blowjob extends Skill {

	public Blowjob(Character self) {
		super("Blow", self);
		addTag(Result.oral);
		addTag(Attribute.Seduction);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.pantsless()
				&& target.hasDick()
				&& c.stance.oral(self)
				&& !c.stance.behind(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			int damage;
			if (self.getPure(Attribute.Professional) >= 5) {
				if (self.has(Trait.silvertongue)) {
					damage = Global.random(8) + self.getEffective(Attribute.Professional) + (2 * self.getEffective(
							Attribute.Seduction) / 3) + target.getEffective(
							Attribute.Perception);
					if (target.human()) {
						if (!Global.checkFlag(Flag.exactimages) || self.name().startsWith("Cassie")) {
							c.offerImage("Blowjob.png", "Art by Phanaxial");
						}
						if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SAMANTHA) {
							c.offerImage("Samanatha Blowjob.jpg", "Art by Phanaxial");
						}
						c.write(self, formattedReceive(damage, Result.special, target));
					}
					else if (self.human()) {
						c.write(self, formattedDeal(damage, Result.special, target));
					}
				}
				else {
					damage = Global.random(6) + self.getEffective(Attribute.Seduction) / 2 + target.getEffective(
							Attribute.Perception);
					if (target.human()) {
						if (!Global.checkFlag(Flag.exactimages) || self.name().startsWith("Cassie")) {
							c.offerImage("Blowjob.png", "Art by Phanaxial");
						}
						if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SAMANTHA) {
							c.offerImage("Samanatha Blowjob.jpg", "Art by AimlessArt");
						}
						if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SELENE) {
							c.offerImage("Selene Blowjob.jpg", "Art by AimlessArt");
						}
						c.offerImage("Blowjob.jpg", "Art by AimlessArt");
						c.write(self, formattedReceive(damage, Result.special, target));
					}
					else if (self.human()) {
						c.write(self, formattedDeal(damage, Result.special, target));
					}
				}
				if (self.has(Trait.sexuallyflexible)) {
					self.add(new ProfessionalMomentum(self, self.getEffective(Attribute.Professional) * 5), c);
				}
				else {
					self.add(new ProfMod("Oral Momentum",
							self,
							Anatomy.mouth,
							self.getEffective(Attribute.Professional) * 5), c);
				}
			}
			else if (self.has(Trait.silvertongue)) {
				damage = Global.random(8) + (2 * self.getEffective(Attribute.Seduction) / 3) + target.getEffective(
						Attribute.Perception);
				if (target.human()) {
					if (!Global.checkFlag(Flag.exactimages) || self.name().startsWith("Cassie")) {
						c.offerImage("Blowjob.png", "Art by Phanaxial");
					}
					if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SAMANTHA) {
						c.offerImage("Samanatha Blowjob.jpg", "Art by Phanaxial");
					}
					if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SELENE) {
						c.offerImage("Selene Blowjob.jpg", "Art by AimlessArt");
					}
					c.offerImage("Blowjob.jpg", "Art by AimlessArt");
					c.write(self, formattedReceive(damage, Result.strong, target));
				}
				else if (self.human()) {
					c.write(self, formattedDeal(damage, Result.strong, target));
				}
			}
			else {
				damage = Global.random(6) + self.getEffective(Attribute.Seduction) / 2 + target.getEffective(Attribute.Perception);
				if (target.human()) {
					if (!Global.checkFlag(Flag.exactimages) || self.name().startsWith("Cassie")) {
						c.offerImage("Blowjob.png", "Art by Phanaxial");
					}
					if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SAMANTHA) {
						c.offerImage("Samanatha Blowjob.jpg", "Art by Phanaxial");
					}
					if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SELENE) {
						c.offerImage("Selene Blowjob.jpg", "Art by AimlessArt");
					}
					c.offerImage("Blowjob.jpg", "Art by AimlessArt");
					c.write(self, formattedReceive(damage, Result.normal, target));
				}
				else if (self.human()) {
					c.write(self, formattedDeal(damage, Result.normal, target));
				}
			}
			damage += self.getMojo().get() / 10;
			if (target.has(Trait.lickable)) {
				damage *= 1.5;
			}
			damage = self.bonusProficiency(Anatomy.mouth, damage);
			target.pleasure(damage, Anatomy.genitals, c);
			if (c.stance instanceof ReverseMount) {
				c.stance = new SixNine(self, target);
			}
		}
		else {
			if (self.human()) {
				c.write(formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 10;
	}

	@Override
	public int accuracy() {
		return 6;
	}

	@Override
	public Skill copy(Character user) {
		return new Blowjob(user);
	}

	@Override
	public int speed() {
		return 2;
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "You thrust your head towards %target%'s penis, but miss and it brushes against your cheek instead.";
			}
			else {
				return "You try to take %target%'s penis into your mouth, but %heshe% manages to pull away.";
			}
		}
		if (target.getArousal().get() < 15) {
			return "You suck on %target%'s flaccid little penis until it grows into an intimidatingly large erection.";
		}
		else if (target.getArousal().percent() >= 90) {
			if (Global.random(2) == 0) {
				return "You suck away on %target%'s cock and you've developed a new appreciation for all the blowjobs you've "
						+ "received in the past. You lick and slurp the underside of the shaft before choking on a mix of your "
						+ "own saliva and %hisher% precum. Thankfully, this display only seems to turn %himher% on more.";
			}
			else {
				return "%target%'s cock seems ready to burst, so you suck on it strongly and attack the glans with your tongue fiercely.";
			}
		}
		else if (modifier == Result.strong) {
			return "You put your skilled tongue to good use tormenting and teasing %hisher% member.";
		}
		else if (modifier == Result.special) {
			switch (Global.random(3)) {
				case 0:
					return "You go to town on %target%'s cock, licking it all over. Long, slow licks along the shaft and "
							+ "small, swift licks around the head cause %himher% to groan in pleasure.";
				case 1:
					return "You lock your lips around the head of %target%'s hard, wet dick and suck on it forcefully "
							+ "while rapidly swirling your tongue around. At the same time, your hands massage and caress "
							+ "every bit of sensitive flesh not covered by your mouth.";
				default:
					return "You are bobbing up and down now, hands still working on any exposed skin while you lick, suck "
							+ "and even nibble all over %target%'s over-stimulated manhood. %HeShe% is not even trying to "
							+ "hide %hisher% enjoyment, and %heshe% grunts loudly every time your teeth graze %hisher% shaft.";
			}
		}
		else {
			if (Global.random(2) == 0) {
				return "You settle into a rhythm and find yourself surprisingly sucked into teasing %target%'s cock with "
						+ "your mouth. Based on %hisher% groans, you are definitely doing something right. As if in "
						+ "confirmation, %heshe% thrusts %hisher% cock against the back of your throat and you both jerk "
						+ "in response.";
			}
			else {
				return "You feel a bit odd, faced with %target%'s rigid cock, but as you lick and suck on it, you "
						+ "discover the taste is quite palatable. Besides, making %target% squirm and moan in pleasure is "
						+ "well worth it.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "%self% clearly intends to try and suck you off, so as %heshe% lowers %hisher% head, you juke to "
						+ "the side instead.";
			}
			else {
				return "%self% tries to suck your cock, but you pull your hips back to avoid %himher%.";
			}
		}
		if (target.getArousal().get() < 15) {
			if (Global.random(2) == 0) {
				return "Your penis reacts almost immediately to the warmth of %self%'s mouth and instantly hardens in "
						+ "%hisher% mouth.";
			}
			else {
				return "%self% takes your soft penis into %hisher% mouth and sucks on it until it hardens";
			}
		}
		else if (target.getArousal().percent() >= 90) {
			return "%self% laps up the precum leaking from your cock and takes the entire length into %hisher% mouth, "
					+ "sucking relentlessly";
		}
		else if (modifier == Result.strong) {
			return "%self%'s soft lips and talented tongue work over your dick, drawing out dangerously irresistible "
					+ "pleasure with each touch.";
		}
		else if (modifier == Result.special) {
			switch (Global.random(3)) {
				case 0:
					return "%self% goes to town on your cock, licking it all over. Long, slow licks along the shaft and "
							+ "small, swift licks around the head cause you to groan in pleasure.";
				case 1:
					return "%self% locks %hisher% lips around the head of your hard and wet dick and sucks on it "
							+ "forcefully while swirling %hisher% tongue rapidly around. At the same time, %hisher% hands "
							+ "are massaging and caressing every bit of sensitive flesh not covered by %hisher% mouth.";
				default:
					return "%self% is bobbing up and down now, hands still working on any exposed skin while %heshe% licks, "
							+ "sucks and even nibbles all over your over-stimulated manhood. You are not even trying to "
							+ "hide your enjoyment, and you grunt loudly every time %hisher% teeth graze your shaft.";
			}
		}
		else {
			switch (Global.random(4)) {
				case 0:
					return "%self% runs %hisher% tongue up the length of your dick, sending a jolt of pleasure up your "
							+ "spine. %HeShe% slowly wraps %hisher% lips around your dick and sucks.";
				case 1:
					return "%self% sucks on the head of your cock while %hisher% hand strokes the shaft.";
				case 2:
					return "%self% licks %hisher% way down to the base of your cock and gently sucks on your balls.";
				default:
					return "%self% runs %hisher% tongue around the glans of your penis and teases your urethra.";
			}
		}
	}

	@Override
	public String describe() {
		return "Lick and suck your opponent's dick, more effective at high Mojo";
	}
}
