package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Charmed;

/**
 * Defines a skill where a character attempts to charm their opponent by purring
 */
public class Purr extends Skill {
	public Purr(Character self) {
		super("Purr", self);
		addTag(Attribute.Animism);
		addTag(SkillTag.CHARMING);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Animism) >= 9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.getArousal().percent() >= 20
				&& c.stance.mobile(self);
	}

	@Override
	public String describe() {
		return "Purr cutely to try to charm your opponent";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (Global.random(target.getLevel()) <= self.getEffective(Attribute.Animism) * self.getArousal().percent() / 100
				&& target.getMood() != Emotion.angry
				&& target.getMood() != Emotion.desperate) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			target.add(new Charmed(target, 2 + self.bonusCharmDuration()), c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Purr(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			switch (Global.random(2)) {
				case 1:
					return "You let out a soft purr and give %target% your best puppy dog eyes. %HeShe% smiles, but then "
							+ "aims a quick punch at your groin, which you barely avoid. Maybe you shouldn't have mixed "
							+ "your animal metaphors.";
				default:
					return "You start moving closer to %target%, giving %himher% your most charming purr. Unfortunately, "
							+ "this only gives %himher% an easier time lining up an attack, which barely misses your face.";
			}
		}
		else {
			switch (Global.random(2)) {
				case 1:
					return "You give %target% an affectionate purr and your most disarming smile. %HisHer% battle aura "
							+ "melts away and %heshe% pats your head, completely taken with your endearing behavior.";
				default:
					return "You get close to %target% and purr gently in %hisher% ear. %HeShe%'s taken in by your charm, "
							+ "so much so that %heshe% forgets %heshe%'s supposed to be fighting you for a moment.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			switch (Global.random(2)) {
				case 1:
					return "%self% slumps submissively and purrs. It's cute, but %heshe%'s not going to get the better of you.";
				default:
					return "%self% gives you an adorable smile and a gentle purr as %heshe% starts inching toward you. "
							+ "It's cute, but not cute enough to stop you from flicking %hisher% forehead to get %himher% "
							+ "to back up.";
			}
		}
		else {
			switch (Global.random(2)) {
				case 1:
					return "%self% purrs cutely, and looks up at you with sad eyes. Oh God, %heshe%'s so adorable! It'd "
							+ "be mean to beat %himher% too quickly. Maybe you should let %himher% get some attacks in "
							+ "while you enjoy watching %hisher% earnest efforts.";
				default:
					return "%self% moves in close to you. As you brace for an attack, %heshe% instead gives you a cute "
							+ "smile and a soft purr, catching you completely off-guard.";
			}
		}
	}
}
