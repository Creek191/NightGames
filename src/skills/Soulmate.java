package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Soulbound;
import status.Stsflag;

/**
 * Defines a skill where a character shares their sense of touch with their opponent
 */
public class Soulmate extends Skill {
    public Soulmate(Character self) {
        super("Soulmate", self);
        addTag(Attribute.Contender);
    }

    @Override
    public boolean requirements(Character user) {
        return self.getPure(Attribute.Contender) >= 7;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canAct()
                && self.canSpend(50)
                && !self.is(Stsflag.soulbound)
                && c.stance.kiss(self);
    }

    @Override
    public String describe() {
        return "Share your sense of touch with your opponent";
    }

    @Override
    public void resolve(Combat c, Character target) {
        if (self.human()) {
            c.write(self, formattedDeal(0, Result.normal, target));
        }
        else if (target.human()) {
            c.write(self, formattedReceive(0, Result.normal, target));
        }
        var duration = self.getPure(Attribute.Contender) / 2;
        self.add(new Soulbound(self, target, duration), c);
    }

    @Override
    public Skill copy(Character user) {
        return new Soulmate(user);
    }

    @Override
    public Tactics type() {
        return Tactics.status;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        return "You pull %target% close and capture %hisher% lips. While %heshe%'s focused on the kiss, you establish a "
                + "single directional mana link. It takes %himher% a moment to notice the new sensation and even longer "
                + "to understand it. Until this wears off, %heshe%'ll feel anything %heshe% does to your body.";
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        return "%self% kisses you softly and romantically, slowly drawing you into %hisher% embrace. As %heshe% pulls "
                + "away, you feel something unnatural, but you can't put your finger on it. %HeShe% grins mischievously "
                + "and softly runs %hisher% hand down %hisher% body. You feel a jolt of pleasure as if %heshe% had touched you.";
    }
}
