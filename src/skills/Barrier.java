package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Shield;

/**
 * Defines a skill that creates a magical barrier protecting the user from damage
 */
public class Barrier extends Skill {
	public Barrier(Character self) {
		super("Barrier", self);
		addTag(Attribute.Arcane);
		addTag(SkillTag.DEFENSIVE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane) >= 18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c) && self.canSpend(3);
	}

	@Override
	public String describe() {
		return "Creates a magical barrier to protect you from physical damage: 3 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(3);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new Shield(self, self.getEffective(Attribute.Arcane) / 2), c);
	}

	@Override
	public Skill copy(Character user) {
		return new Barrier(user);
	}

	@Override
	public Tactics type() {
		return Tactics.recovery;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You conjure a simple magic barrier around yourself, reducing physical damage. Unfortunately, it will do "
				+ "nothing against a gentle caress.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% holds a hand in front of %himher% and you see a magical barrier appear briefly, before it becomes "
				+ "invisible.";
	}
}
