package skills;

import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import combat.Tag;

import java.util.HashSet;

/**
 * Base class for skills used during combat
 */
public abstract class Skill implements Comparable<Skill> {

	protected String name;
	protected Character self;
	protected String image;
	protected String artist;
	protected Integer sortOrder = null;
	protected HashSet<Tag> tags;
	protected int accuracy;
	protected int speed;

	public Skill(String name, Character self) {
		this.name = name;
		this.self = self;
		this.image = null;
		this.artist = null;
		this.speed = 5;
		this.accuracy = 5;
		tags = new HashSet<>();
	}

	/**
	 * Checks whether the specified character  meets the requirements to learn the skill
	 *
	 * @param user The user to check
	 */
	public abstract boolean requirements(Character user);

	/**
	 * Checks whether the skill can be used on the specified character
	 *
	 * @param c      A reference to the ongoing combat
	 * @param target The character to use the skill on
	 */
	public abstract boolean usable(Combat c, Character target);

	/**
	 * Gets a description of the skill from the player's perspective
	 */
	public abstract String describe();

	/**
	 * Resolves the outcome of using the skill on the specified character
	 *
	 * @param c      A reference to the ongoing combat
	 * @param target The character to use the skill on
	 */
	public abstract void resolve(Combat c, Character target);

	/**
	 * Creates a copy of the skill with the specified character as the user
	 *
	 * @param user The character that will use the skill
	 */
	public abstract Skill copy(Character user);

	/**
	 * Gets the tactics type of the skill
	 */
	public abstract Tactics type();

	/**
	 * Gets the message to use when the player is using the skill on a character
	 *
	 * @param damage   The amount of damage dealt by the skill
	 * @param modifier The modifier for the outcome of using the skill
	 * @param target   The character the skill is used on
	 */
	protected abstract String deal(int damage, Result modifier, Character target);

	/**
	 * Gets the message to use when the skill is used on the player
	 *
	 * @param damage   The amount of damage dealt by the skill
	 * @param modifier The modifier for the outcome of using the skill
	 * @param target   The character the skill is used on (should be the player)
	 */
	protected abstract String receive(int damage, Result modifier, Character target);

	/**
	 * Gets the message to use when the player is using the skill on a character
	 *
	 * @param damage   The amount of damage dealt by the skill
	 * @param modifier The modifier for the outcome of using the skill
	 * @param target   The character the skill is used on
	 */
	protected String formattedDeal(int damage, Result modifier, Character target) {
		var message = deal(damage, modifier, target);

		message = message.replaceAll("%hisher%", target.possessive(false))
				.replaceAll("%HisHer%", target.possessive(true))
				.replaceAll("%hishers%", target.possessive(false) + (target.has(Trait.male) ? "" : "s"))
				.replaceAll("%HisHers%", target.possessive(true) + (target.has(Trait.male) ? "" : "s"))
				.replaceAll("%himher%", target.pronounTarget(false))
				.replaceAll("%HimHer%", target.pronounTarget(true))
				.replaceAll("%heshe%", target.pronounSubject(false))
				.replaceAll("%HeShe%", target.pronounSubject(true))
				.replaceAll("%self%", self.name())
				.replaceAll("%target%", target.name());

		return message;
	}

	/**
	 * Gets the message to use when the skill is used on the player
	 *
	 * @param damage   The amount of damage dealt by the skill
	 * @param modifier The modifier for the outcome of using the skill
	 * @param target   The character the skill is used on (should be the player)
	 */
	protected String formattedReceive(int damage, Result modifier, Character target) {
		var message = receive(damage, modifier, target);

		message = message.replaceAll("%hisher%", self.possessive(false))
				.replaceAll("%HisHer%", self.possessive(true))
				.replaceAll("%hishers%", self.possessive(false) + (self.has(Trait.male) ? "" : "s"))
				.replaceAll("%HisHers%", self.possessive(true) + (self.has(Trait.male) ? "" : "s"))
				.replaceAll("%himher%", self.pronounTarget(false))
				.replaceAll("%HimHer%", self.pronounTarget(true))
				.replaceAll("%heshe%", self.pronounSubject(false))
				.replaceAll("%HeShe%", self.pronounSubject(true))
				.replaceAll("%self%", self.name())
				.replaceAll("%target%", target.name());

		return message;
	}

	/**
	 * Gets the skill's base accuracy
	 */
	public int accuracy() {
		return accuracy;
	}

	/**
	 * Gets the skill's base speed
	 */
	public int speed() {
		return speed;
	}

	/**
	 * Gets the skill's name
	 */
	public String toString() {
		return name;
	}

	/**
	 * Gets the character using the skill
	 */
	public Character user() {
		return self;
	}

	/**
	 * Sets the owner of the skill to a different character
	 *
	 * @param self The new character using the skill
	 */
	public void setSelf(Character self) {
		this.self = self;
	}

	/**
	 * Sets the name of the skill
	 *
	 * @param name The new name of the skill
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Adds the specified tag to the skill
	 *
	 * @param tag The tag to add
	 */
	public void addTag(Tag tag) {
		tags.add(tag);
	}

	/**
	 * Checks whether the skill has the specified tag
	 *
	 * @param tag The tag to check
	 */
	public boolean hasTag(Tag tag) {
		return tags.contains(tag);
	}

	/**
	 * Gets the skill's sort order
	 */
	public Integer getSortOrder() {
		if (sortOrder != null) {
			return sortOrder;
		}
		return type().ordinal();
	}

	@Override
	public boolean equals(Object other) {
		return this.getClass() == other.getClass();
	}

	@Override
	public int compareTo(Skill other) {
		var ret = getSortOrder() - other.getSortOrder();
		if (ret != 0) {
			return ret;
		}

		return name.compareTo(other.name);
	}
}
