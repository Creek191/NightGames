package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;

/**
 * Defines a skill where a character thrusts back while being penetrated
 */
public class Buck extends Skill {
	public Buck(Character self) {
		super("Buck", self);
		addTag(Attribute.Submissive);
		addTag(Result.intercourse);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive) >= 18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.penetration(self)
				&& c.stance.sub(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		Anatomy inside;
		if (c.stance.anal()) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.anal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.anal, target));
			}
			inside = Anatomy.ass;
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			inside = Anatomy.genitals;
		}
		var damage = self.getSexPleasure(2, Attribute.Submissive);
		target.pleasure(self.bonusProficiency(inside, damage), inside, Result.finisher, c);

		var recoil = target.getSexPleasure(1, Attribute.Seduction) / 3;
		recoil += target.bonusRecoilPleasure(recoil);
		if (self.has(Trait.experienced)) {
			recoil = recoil / 2;
		}
		self.pleasure(recoil, inside, Result.finisher, c);
		c.stance.setPace(2);
	}

	@Override
	public Skill copy(Character user) {
		return new Buck(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.anal) {
			return "You do your best to thrust back against %target%, as %heshe% pegs you.";
		}
		else {
			return "You buck your hips, thrusting into %target% from below. %HeShe% moans in pleasure as %heshe% tries "
					+ "to retain control.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.anal) {
			return "%self% presses %hisher% ass against your hips, hitting you with unexpected pleasure despite the "
					+ "awkward position.";
		}
		else {
			return "%self% pushes %hisher% hips against you, matching you thrust for thrust. The sudden change of pace "
					+ "catches you off-guard and hits you with a jolt of pleasure.";
		}
	}

	@Override
	public String describe() {
		return "Buck against your opponent from the bottom, giving some pleasure";
	}
}

