package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import status.Charmed;

/**
 * Defines a skill where a character passionately kisses their opponent
 */
public class KissEX extends Skill {
	public KissEX(Character self) {
		super("Passionate Kiss", self);
		this.speed = 6;
		addTag(Attribute.Seduction);
		addTag(Result.kiss);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(15)
				&& c.stance.kiss(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int damage;
		var style = Result.seductive;
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning) && self.getEffective(Attribute.Power) > self.getEffective(
				Attribute.Seduction)) {
			style = Result.powerful;
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Seduction)) {
			style = Result.clever;
		}
		self.spendMojo(15);
		if (style == Result.powerful) {
			damage = Global.random(4) + self.getEffective(Attribute.Power) / 4;
		}
		else if (style == Result.clever) {
			damage = Global.random(4) + self.getEffective(Attribute.Cunning) / 4;
		}
		else {
			damage = Global.random(4) + self.getEffective(Attribute.Seduction) / 4;
			damage *= 1.3f;
		}

		if (self.has(Trait.romantic)) {
			damage *= 1.2f;
		}

		if (self.human()) {
			c.write(self, formattedDeal(damage, style, target));
			if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.JEWEL) {
				c.offerImage("Kiss.jpg", "Art by AimlessArt");
			}
			if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.CASSIE) {
				c.offerImage("Cassie Kiss.jpg", "Art by AimlessArt");
			}
			if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.ANGEL) {
				c.offerImage("Angel Kiss.jpg", "Art by AimlessArt");
			}
			if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.SELENE) {
				c.offerImage("Selene Kissed.png", "Art by AimlessArt");
			}
		}
		else if (target.human()) {
			c.write(self, formattedReceive(damage, style, target));
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.JEWEL) {
				c.offerImage("Kissed.jpg", "Art by AimlessArt");
			}
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.ANGEL) {
				c.offerImage("Angel Kiss.jpg", "Art by AimlessArt");
			}
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SELENE) {
				c.offerImage("Selene Kiss.png", "Art by AimlessArt");
			}
		}

		target.tempt(damage, Result.foreplay, c);
		target.pleasure(1, Anatomy.mouth, c);
		if (self.has(Trait.greatkiss) && self.canSpend(10) && Global.random(2) == 0) {
			target.add(new Charmed(target, 2 + self.bonusCharmDuration()), c);
			self.spendMojo(10);
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getEffective(Attribute.Seduction) >= 9;
	}

	@Override
	public Skill copy(Character user) {
		return new KissEX(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.powerful) {
			return "You aggressively pull %target% into an intense kiss. You push your tongue into %hisher% mouth before "
					+ "%heshe% has a chance to react. By %hisher% response, %heshe% clearly doesn't mind a little "
					+ "assertiveness.";
		}
		else if (modifier == Result.clever) {
			return "You steal a quick kiss from %target%, pulling back before %heshe% can respond. As %heshe% hesitates "
					+ "in confusion, you kiss %himher% twice more, lingering on the last to run your tongue over %hisher% "
					+ "lips.";
		}
		else {
			return "You pull %target% close for a passionate kiss. %HeShe% responds eagerly, parting %hisher% lips and "
					+ "bringing out %hisher% tongue to tangle with yours. Your own tongue proves superior though, and "
					+ "%heshe% soon melts into your arms as you explore the sensitive corners of %hisher% mouth.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.powerful) {
			return "%self% pulls you into an aggressive kiss, forcing %hisher% tongue into your mouth. It catches you off "
					+ "guard, but %hisher% assertiveness is quite arousing.";
		}
		else if (modifier == Result.clever) {
			return "%self% leans in close to capture your lips, a lusty look in %hisher% eyes. You instinctively close "
					+ "your eyes to meet the kiss, but it doesn't arrive. Confused, you open your eyes to see %hisher% "
					+ "mischievous grin only inches away. %HeShe% quickly darts in and steals a kiss, catching you by "
					+ "surprise.";
		}
		else {
			return "%self% licks %hisher% lips seductively, before leaning in to claim yours. Despite the feeling that "
					+ "%heshe% has the advantage here, you can't resist accepting the kiss eagerly. %HisHer% tongue "
					+ "lightly traces your lips, then slips in to explore your mouth. You have a lot of practice kissing, "
					+ "but %heshe%'s so seductive and talented that you can barely respond.";
		}
	}

	@Override
	public String toString() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Power) > self.getEffective(Attribute.Seduction)) {
			return "Forceful Kiss";
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Seduction)) {
			return "Steal Kiss";
		}
		else {
			return name;
		}
	}

	@Override
	public String describe() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Power) > self.getEffective(Attribute.Seduction)) {
			return "Strong kiss using Power: 15 Mojo";
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Seduction)) {
			return "Sneaky kiss using Cunning: 15 Mojo";
		}
		else {
			return "Mojo boosted kiss: 15 Mojo";
		}
	}
}
