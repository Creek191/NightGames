package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Stsflag;
import status.Unwavering;

/**
 * Defines a skill where a character becomes temporarily immune to temptation
 */
public class InnerPeace extends Skill {
	public InnerPeace(Character self) {
		super("Inner Peace", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Spirituality) >= 8;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)
				&& self.canSpend(Pool.FOCUS, 5)
				&& !self.is(Stsflag.unwavering);
	}

	@Override
	public String describe() {
		return "Become temporarily immune to temptation.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.FOCUS, 5);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new Unwavering(self), c);
	}

	@Override
	public Skill copy(Character user) {
		return new InnerPeace(user);
	}

	@Override
	public Tactics type() {
		return Tactics.calming;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You focus on your inner peace, blocking out all temptation.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% takes a deep breath and looks at you with unwavering focus.";
	}
}
