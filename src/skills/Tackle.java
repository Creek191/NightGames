package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Mount;

/**
 * Defines a skill where a character tackles their opponent to the ground
 */
public class Tackle extends Skill {
	private final boolean animalistic;

	public Tackle(Character self) {
		super("Tackle", self);
		animalistic = self.getEffective(Attribute.Animism) >= 1;
		this.accuracy = animalistic ? 3 : 1;
		this.speed = animalistic ? 3 : 1;
		addTag(Attribute.Power);
		addTag(SkillTag.KNOCKDOWN);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.has(Trait.petite)
				&& !c.stance.prone(self)
				&& c.stance.mobile(self)
				&& c.stance.mobile(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)
				&& self.check(Attribute.Power, target.knockdownDC() - self.getEffective(Attribute.Animism))) {
			if (animalistic) {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.special, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.special, target));
				}
				target.pain(4 + Global.random(6), Anatomy.chest, c);
				c.stance = new Mount(self, target);
			}
			else if (target.has(Trait.reflexes)) {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.unique, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.unique, target));
				}
				self.pain(target.getLevel(), Anatomy.chest);
				self.emote(Emotion.nervous, 10);
				target.emote(Emotion.dominant, 15);
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.normal, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				target.pain(3 + Global.random(4), Anatomy.chest, c);
				c.stance = new Mount(self, target);
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return (user.getPure(Attribute.Power) >= 26 || user.getPure(Attribute.Animism) >= 1)
				&& !user.has(Trait.petite);
	}

	@Override
	public Skill copy(Character user) {
		return new Tackle(user);
	}

	public Tactics type() {
		return Tactics.positioning;
	}

	public String toString() {
		if (animalistic) {
			return "Pounce";
		}
		else {
			return name;
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "You lunge at %target%, but %heshe% rolls backwards, using your momentum to throw you behind %himher%. "
					+ "You land hard on the floor, taking the wind out of you.";
		}
		if (modifier == Result.special) {
			return "You let your instincts take over and you pounce on %target% like a predator catching your prey.";
		}
		else if (modifier == Result.normal) {
			return "You tackle %target% to the ground and straddle %himher%.";
		}
		else {
			return "You lunge at %target%, but %heshe% dodges out of the way.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "%self% wiggles %hisher% butt cutely before leaping at you and pinning you to the floor.";
		}
		if (modifier == Result.miss) {
			return "%self% tries to tackle you, but you sidestep out of the way.";
		}
		else {
			return "%self% bowls you over and sits triumphantly on your chest.";
		}
	}

	@Override
	public String describe() {
		return "Knock opponent to ground and get on top of her";
	}
}
