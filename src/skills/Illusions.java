package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Distorted;

/**
 * Defines a skill where a character creates illusions to boost their evasions
 */
public class Illusions extends Skill {
	public Illusions(Character self) {
		super("Illusions", self);
		addTag(Attribute.Arcane);
		addTag(SkillTag.CLONE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane) >= 12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(10)
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Create illusions to act as cover: 10 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(10);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new Distorted(self, self.getEffective(Attribute.Arcane) / 3), c);
	}

	@Override
	public Skill copy(Character user) {
		return new Illusions(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You cast an illusion spell to create several images of yourself.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% casts a brief spell and your vision is filled with naked copies of %himher%. You can still tell "
				+ "which %self% is real, but it's still a distraction.";
	}
}
