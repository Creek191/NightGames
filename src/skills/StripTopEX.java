package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

/**
 * Defines a skill where a character skillfully strips their opponent's top clothes
 */
public class StripTopEX extends Skill {
	public StripTopEX(Character self) {
		super("Tricky Strip Top", self);
		this.speed = 3;
		addTag(SkillTag.STRIPPING);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(15)
				&& !target.topless()
				&& c.stance.reachTop(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int strip;
		var style = Result.clever;
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Power) > self.getEffective(Attribute.Seduction)) {
			style = Result.powerful;
		}
		else if (self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Cunning)) {
			style = Result.seductive;
		}
		self.spendMojo(15);
		if (style == Result.powerful) {
			strip = self.getEffective(Attribute.Power);
		}
		else if (style == Result.clever) {
			strip = self.getEffective(Attribute.Cunning);
			strip *= 3;
		}
		else {
			strip = self.getEffective(Attribute.Seduction);
		}

		if (target.stripAttempt(strip, self, c, target.top.peek())) {
			if (self.human()) {
				c.write(self, formattedDeal(0, style, target));
				if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Jewel")) {
					c.offerImage("Strip Top Female.jpg", "Art by AimlessArt");
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, style, target));
			}
			target.strip(0, c);
			if (self.getPure(Attribute.Cunning) >= 30 && !target.topless()) {
				if (target.stripAttempt(strip, self, c, target.top.peek())) {
					if (self.human()) {
						c.write(self, formattedDeal(0, Result.strong, target));
					}
					else if (target.human()) {
						c.write(self, formattedReceive(0, Result.strong, target));
					}
					target.strip(0, c);
					target.emote(Emotion.nervous, 10);
				}
			}
			if (self.human() && target.nude()) {
				c.write(target, target.nakedLiner());
			}
			target.emote(Emotion.nervous, 10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new StripTopEX(user);
	}

	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You attempt to strip off %target%'s " + target.top.peek().getName() + ", but %heshe% shoves you away.";
		}
		else if (modifier == Result.strong) {
			return "Taking advantage of the situation, you also manage to snag %hisher% " + target.top.peek().getName();
		}
		if (modifier == Result.powerful) {
			return "You grab %target%'s " + target.top.peek().getName() + " and yank it off with enough force that you "
					+ "almost pull %himher% off the ground.";
		}
		else if (modifier == Result.clever) {
			return "You slip your hands under %target%'s " + target.top.peek().getName() + " to caress %himher%. You "
					+ "swipe the garment with a flick of your wrist when %heshe% least expects it.";
		}
		else {
			return "You turn up the charm to maximum, and help %target% out of %hisher% " + target.top.peek().getName()
					+ " before %heshe% can remember to resist.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to yank off your " + target.top.peek().getName() + ", but you manage to hang onto it.";
		}
		else if (modifier == Result.strong) {
			return "Before you can react, %heshe% also strips off your " + target.top.peek().getName();
		}
		if (modifier == Result.powerful) {
			return "%self% forcefully pulls your " + target.top.peek().getName() + " over your head, blinding you. As "
					+ "you struggle to restore your vision, %heshe% easily yanks the garment completely off.";
		}
		else if (modifier == Result.clever) {
			return "You brace yourself for an attack as %self% lunges toward you, only to playfully lick your cheek. "
					+ "You're so shocked by the unexpected action that %heshe% manages to steal your "
					+ target.top.peek().getName() + " in the confusion.";
		}
		else {
			return "%self% coyly saunters up to you and gently removes your " + target.top.peek().getName() + ". Since "
					+ "%heshe% is acting much more intimate than competitive, you end up letting %himher% out of habit.";
		}
	}

	@Override
	public String toString() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Power) > self.getEffective(Attribute.Seduction)) {
			return "Forceful Strip Top";
		}
		else if (self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Cunning)) {
			return "Charming Strip Top";
		}
		return name;
	}

	@Override
	public String describe() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Power) > self.getEffective(Attribute.Seduction)) {
			return "Attempt to remove opponent's top by force: 15 Mojo";
		}
		else if (self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Cunning)) {
			return "Attempt to remove opponent's top by seduction: 15 Mojo";
		}
		return "Attempt to remove opponent's top with extra Mojo: 15 Mojo";
	}
}
