package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;

/**
 * Defines a skill where a character performs special kicks from a disadvantaged position
 */
public class KittyKick extends Skill {
	public KittyKick(Character self) {
		super("Kitty Kick", self);
		addTag(SkillTag.PAINFUL);
	}

	@Override
	public boolean requirements(Character user) {
		return user.id() == ID.KAT && user.getRank() >= 2;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canSpend(30) && c.stance.reachTop(self) && self.canAct() && c.stance.sub(self);
	}

	@Override
	public String describe() {
		return "A specialized low attack from a disadvantageous position: 30 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(30);
		var damage = self.getLevel() + self.getEffective(Attribute.Power);
		if (target.human()) {
			c.write(self, formattedReceive(damage, Result.normal, target));
		}
		target.pain(damage, Anatomy.genitals, c);
		if (self.has(Trait.wrassler)) {
			target.calm(damage / 4, c);
		}
		else {
			target.calm(damage / 2, c);
		}
		target.emote(Emotion.angry, 50);
	}

	@Override
	public Skill copy(Character user) {
		return new KittyKick(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "Kat firmly grabs both of your wrists and then rolls backwards, pulling you over %himher%. %HeShe% begins "
				+ "rapidly bicycle-kicking %hisher% feet out, aiming at your lower stomach and crotch. The kicks don't "
				+ "hurt very much individually, but they are so fast that %heshe% manages to get several quick hits in "
				+ "before you can free your hands and block the attacks.";
	}
}
