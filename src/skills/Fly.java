package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.Flying;
import stance.Stance;

/**
 * Defines a skill where a character picks up their opponent and flies up with them to fuck in the air
 */
public class Fly extends Skill {
	public Fly(Character self) {
		super("Fly", self);
		addTag(Attribute.Dark);
		addTag(Result.intercourse);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark) >= 18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canFuck()
				&& self.getStamina().get() >= 15
				&& self.isErect()
				&& target.pantsless()
				&& target.isErect()
				&& c.stance.en != Stance.flying
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Take off while holding your opponent and have your way with them in the air.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (this.self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, this.self));
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.REYKA) {
				c.offerImage("Reyka Fly.jpg", "Art by AimlessArt");
			}
		}
		var damage = self.getEffective(Attribute.Dark) / 2 + target.getEffective(Attribute.Perception);
		if (self.has(Trait.strapped)) {
			damage += +(self.getEffective(Attribute.Science) / 2);
			damage = self.bonusProficiency(Anatomy.toy, damage);
		}
		else {
			damage = self.bonusProficiency(Anatomy.genitals, damage);
		}
		target.pleasure(damage, Anatomy.genitals, c);
		self.pleasure(self.getEffective(Attribute.Dark) / 2 + self.getEffective(Attribute.Perception),
				Anatomy.genitals,
				c);
		self.emote(Emotion.dominant, 50);
		self.emote(Emotion.horny, 30);
		target.emote(Emotion.desperate, 50);
		target.emote(Emotion.nervous, 75);
		c.stance = new Flying(this.self, target);
	}

	@Override
	public Skill copy(Character target) {
		return new Fly(target);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int amount, Result modifier, Character target) {
		return "Summoning demonic wings, you grab %target% tightly and take off, "
				+ (target.hasDick() && self.hasPussy()
				   ? "inserting %hisher% dick into your hungry pussy."
				   : " holding %hisher% helpless in the air as you thrust deep into %hisher% wet pussy.");
	}

	@Override
	public String receive(int amount, Result modifier, Character target) {
		return "Suddenly, %target% leaps at you, embracing you tightly. %HeShe% then flaps %hisher% powerful wings hard "
				+ "and before you know it you are twenty feet in the sky held up by %hisher% arms and legs. Somehow, "
				+ "your dick ended up inside of %himher% in the process and the rhythmic movements of %hisher% flying "
				+ "arouse you to no end.";
	}
}
