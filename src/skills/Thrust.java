package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import status.ProfMod;
import status.ProfessionalMomentum;

/**
 * Defines a skill where a character slowly thrusts into their opponent
 */
public class Thrust extends Skill {
	public Thrust(Character self) {
		super("Thrust", self);
		addTag(Result.intercourse);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.dom(self)
				&& c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		Anatomy inside;
		if (c.stance.anal()) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.anal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.anal, target));
			}
			inside = Anatomy.ass;
		}
		else {
			var style = Result.normal;
			if (self.getPure(Attribute.Professional) >= 11) {
				style = Result.special;
				if (self.has(Trait.sexuallyflexible)) {
					self.add(new ProfessionalMomentum(self, self.getEffective(Attribute.Professional) * 5), c);
				}
				else {
					self.add(new ProfMod("Sexual Momentum",
							self,
							Anatomy.genitals,
							self.getEffective(Attribute.Professional) * 5), c);
				}
			}

			if (self.human()) {
				c.write(self, formattedDeal(0, style, target));
				if (c.stance.behind(self)) {
					if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.KAT) {
						c.offerImage("Doggy Style.jpg", "Art by AimlessArt");
					}
					if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.MARA) {
						c.offerImage("Doggy Style Mara.jpg", "Art by AimlessArt");
					}
				}
				else {
					if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.ANGEL) {
						c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri");
						c.offerImage("Angel_Sex.jpg", "Art by AimlessArt");
					}
					c.offerImage("Thrust.jpg", "Art by AimlessArt");
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, style, target));
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.CASSIE) {
					c.offerImage("Cowgirl.jpg", "Art by AimlessArt");
				}
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SAMANTHA) {
					c.offerImage("Samantha Cowgirl.jpg", "Art by AimlessArt");
				}
			}
			inside = Anatomy.genitals;
		}

		self.buildMojo(10);
		var damage = self.getSexPleasure(2, Attribute.Seduction) + target.getEffective(Attribute.Perception);
		if (self.has(Trait.strapped)) {
			damage += +(self.getEffective(Attribute.Science) / 2);
			damage = self.bonusProficiency(Anatomy.toy, damage);
		}
		target.pleasure(damage, inside, c);
		var recoil = (target.getSexPleasure(2, Attribute.Seduction) * 2) / 3;
		recoil += target.bonusRecoilPleasure(recoil);
		if (self.has(Trait.experienced)) {
			recoil = recoil / 2;
		}

		self.pleasure(recoil, Anatomy.genitals, c);
		c.stance.setPace(1);
	}

	@Override
	public Skill copy(Character user) {
		return new Thrust(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.anal) {
			return "You thrust steadily into %target%'s ass, eliciting soft groans of pleasure.";
		}
		else {
			return "You thrust into %target% in a slow, steady rhythm. %HeShe% lets out soft breathy moans in time with "
					+ "your lovemaking. You can't deny you're feeling it too, but by controlling the pace, you can "
					+ "hopefully last longer than %heshe% can.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.anal) {
			if (self.has(Trait.strapped)) {
				return "%self% thrusts %hisher% hips, pumping %hisher% artificial cock in and out of your ass and "
						+ "pushing on your prostate.";
			}
			else {
				return "%self%'s cock slowly pumps the inside of your rectum.";
			}
		}
		else {
			if (modifier == Result.special) {
				switch (Global.random(3)) {
					case 0:
						return "%self% is holding just the tip of your dick inside of %hisher% slick pussy, rubbing it "
								+ "against the soft, tight entrance. Slowly moving %hisher% hips, %heshe% is driving you "
								+ "crazy.";
					case 1:
						return "%self% slides down fully onto you, squeezing you tightly with %hisher% vaginal walls. "
								+ "The muscles are wound so tight that it's nearly impossible to move at all, but %heshe% "
								+ "pushes down hard and eventually all of your cock is lodged firmly inside of %himher%.";
					default:
						return "%self% moves up and down your rock-hard cock while the velvet vise of %hisher% pussy is "
								+ "undulating on your shaft, sending ripples along it as if milking it. Overcome with "
								+ "pleasure, your entire body tenses up and you throw your head back, trying hard not to "
								+ "cum instantly.";
				}
			}
			else {
				return "%self% rocks %hisher% hips against you, riding you smoothly and deliberately. Despite the slow "
						+ "pace, the sensation of %hisher% hot, wet pussy surrounding your dick is gradually driving you "
						+ "to your limit.";
			}
		}
	}

	@Override
	public String toString() {
		if (self.getPure(Attribute.Professional) >= 11) {
			return "Pro Thrust";
		}
		else {
			return "Thrust";
		}
	}

	@Override
	public String describe() {
		if (self.getPure(Attribute.Professional) >= 11) {
			return "A professional thrusting technique that gains momentum over time";
		}
		else {
			return "Slow fuck, minimizes own pleasure";
		}
	}
}
