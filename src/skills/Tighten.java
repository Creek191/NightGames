package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Stance;

/**
 * Defines a skill where a character squeezes down on their opponent's dick
 */
public class Tighten extends Skill {
	public Tighten(Character self) {
		super("Tighten", self);
		addTag(Attribute.Seduction);
		addTag(Result.intercourse);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 28 && user.hasPussy();
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.hasPussy()
				&& target.hasDick()
				&& c.stance.enumerate() != Stance.anal
				&& c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		var damage = self.getSexPleasure(2, Attribute.Power);
		target.pleasure(self.bonusProficiency(Anatomy.genitals, damage), Anatomy.genitals, Result.finisher, c);
		c.stance.setPace(0);
	}

	@Override
	public Skill copy(Character user) {
		return new Tighten(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% gives you a seductive wink and suddenly %hisher% pussy squeezes around your dick as though it's "
				+ "trying to milk you.";
	}

	@Override
	public String describe() {
		return "Squeeze opponent's dick, no pleasure to self";
	}
}
