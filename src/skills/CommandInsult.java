package skills;

import characters.Character;
import combat.Combat;
import combat.Result;

/**
 * Defines a command where an enthralled character is fiercely insulted, draining mojo
 */
public class CommandInsult extends PlayerCommand {
	public CommandInsult(Character self) {
		super("Insult", self);
		addTag(SkillTag.COMMAND);
	}

	@Override
	public String describe() {
		return "Temporarily destroy your thrall's self-image, draining their mojo.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (target.canSpend(15)) {
			target.spendMojo(15);
		}
		else {
			target.getMojo().set(0);
		}
		self.buildMojo(10);
		c.write(self, formattedDeal(0, Result.normal, target));
	}

	@Override
	public Skill copy(Character user) {
		return new CommandInsult(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		return "Your words nearly drive %target% to tears with their ferocity and psychic backup. Luckily, %heshe% won't "
				+ "remember any of it later.";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The Silver Bard: CommandInsult-receive>>";
	}
}
