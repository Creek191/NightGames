package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Abuff;

/**
 * Defines a skill that puts a character into a submission hold reducing their power
 */
public class ArmBar extends Skill {
	public ArmBar(Character self) {
		super("Armbar", self);
		this.speed = 2;
		addTag(SkillTag.PAINFUL);
		addTag(SkillTag.CRIPPLING);
		addTag(Attribute.Power);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return c.stance.dom(self)
				&& c.stance.reachTop(self)
				&& self.canAct()
				&& !self.has(Trait.undisciplined)
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			var damage = Global.random(10) + self.getEffective(Attribute.Power) / 2;
			if (self.human()) {
				c.write(self, formattedDeal(damage, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, Result.normal, target));
			}
			target.pain(damage, Anatomy.arm, c);
			target.add(new Abuff(target, Attribute.Power, -4, 5), c);
			target.emote(Emotion.angry, 25);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power) >= 20 && !user.has(Trait.undisciplined);
	}

	@Override
	public Skill copy(Character user) {
		return new ArmBar(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You grab at %target%'s arm, but %heshe% pulls it free.";
		}
		else {
			return "You grab %target%'s arm at the wrist and pull it to your chest in the traditional judo submission technique.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% grabs your wrist, but you pry it out of %hisher% grasp.";
		}
		else {
			return "%self% pulls your arm between %hisher% legs, forcibly overextending your elbow. The pain almost makes "
					+ "you tap out, but you manage to yank your arm out of %hisher% grip.";
		}
	}

	@Override
	public String describe() {
		return "A judo submission hold that hyperextends the arm.";
	}
}
