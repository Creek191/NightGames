package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Masochistic;
import status.Stsflag;

/**
 * Defines a skill where a character makes both themselves and their opponent more aroused by masochism
 */
public class Masochism extends Skill {
	public Masochism(Character self) {
		super("Masochism", self);
		addTag(Attribute.Fetish);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Fetish) >= 9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.getArousal().get() >= 15
				&& !self.is(Stsflag.masochism)
				&& c.stance.mobile(self);
	}

	@Override
	public String describe() {
		return "You and your opponent become aroused by pain: Arousal at least 15";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new Masochistic(self), c);
		target.add(new Masochistic(target), c);
	}

	@Override
	public Skill copy(Character user) {
		return new Masochism(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You fantasize about the pleasure that exquisite pain can bring. You share this pleasure with %target%.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% shivers in arousal. You're suddenly bombarded with thoughts of letting %himher% hurt you in "
				+ "wonderful ways.";
	}
}
