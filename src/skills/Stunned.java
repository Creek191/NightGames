package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character is stunned and unable to move
 */
public class Stunned extends Skill {
	public Stunned(Character self) {
		super("Stunned", self);
		this.speed = 0;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.stunned();
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			if (Global.random(3) >= 2) {
				c.write(self, self.stunLiner());
			}
			else {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return false;
	}

	@Override
	public Skill copy(Character user) {
		return new Stunned(user);
	}

	public Tactics type() {
		return Tactics.misc;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You're unable to move.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% is on the floor, trying to catch %hisher% breath.";
	}

	@Override
	public String describe() {
		return "You're stunned";
	}
}
