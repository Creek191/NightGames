package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character summons shadow clones to attack the opponent
 */
public class BunshinAssault extends Skill {
	public BunshinAssault(Character self) {
		super("Bunshin Assault", self);
		this.speed = 4;
		addTag(Attribute.Ninjutsu);
		addTag(SkillTag.PAINFUL);
		addTag(SkillTag.CLONE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu) >= 6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)
				&& self.canSpend(6)
				&& !c.stance.behind(target)
				&& !c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "Attack your opponent with shadow clones: 3 Mojo per attack (min 2)";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var clones = Math.min(Math.min(self.getMojo().get() / 3, self.getEffective(Attribute.Ninjutsu) / 3), 6);
		Result r;
		self.spendMojo(clones * 3);
		if (self.human()) {
			c.write("You form " + clones + " shadow clones and rush forward.");
		}
		else if (target.human()) {
			c.write(self.name() + " moves in a blur and suddenly you see " + clones + " of " + self.pronounTarget(false)
					+ " approaching you.");
		}

		// Run attacks for each clone
		for (var i = 0; i < clones; i++) {
			if (c.attackRoll(this, self, target)) {
				switch (Global.random(4)) {
					case 0:
						r = Result.weak;
						if (self.human()) {
							c.write(self, formattedDeal(0, r, target));
						}
						else if (target.human()) {
							c.write(self, formattedReceive(0, r, target));
						}
						target.pain(Global.random(4) + 3, Anatomy.ass, c);
						target.calm(3, c);
						break;
					case 1:
						r = Result.normal;
						if (self.human()) {
							c.write(self, formattedDeal(0, r, target));
						}
						else if (target.human()) {
							c.write(self, formattedReceive(0, r, target));
						}
						target.pain(Global.random(4) + self.getEffective(Attribute.Power) / 3, Anatomy.chest, c);
						target.calm(3, c);
						break;
					case 2:
						r = Result.strong;
						if (self.human()) {
							c.write(self, formattedDeal(0, r, target));
						}
						else if (target.human()) {
							c.write(self, formattedReceive(0, r, target));
						}
						target.pain(Global.random(8) + self.getEffective(Attribute.Power) / 2, Anatomy.genitals, c);
						if (!self.has(Trait.wrassler)) {
							target.calm(3, c);
						}
						break;
					default:
						r = Result.critical;
						if (self.human()) {
							c.write(self, formattedDeal(0, r, target));
						}
						else if (target.human()) {
							c.write(self, formattedReceive(0, r, target));
						}
						target.pain(Global.random(12) + self.getEffective(Attribute.Power), Anatomy.genitals, c);
						if (!self.has(Trait.wrassler)) {
							target.calm(3, c);
						}
						break;
				}
				target.emote(Emotion.angry, 30);
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.miss, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.miss, target));
				}
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new BunshinAssault(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%target% dodges one of your shadow clones.";
		}
		else if (modifier == Result.weak) {
			return "Your shadow clone gets behind %target% and slaps %himher% hard on the ass.";
		}
		else if (modifier == Result.strong) {
			if (target.hasBalls()) {
				return "One of your clones grabs and squeezes %target%'s balls.";
			}
			else {
				return "One of your clones hits %target% on %hisher% sensitive tit.";
			}
		}
		else if (modifier == Result.critical) {
			if (target.hasBalls()) {
				return "One lucky clone manages to deliver a clean kick to %target%'s fragile balls.";
			}
			else {
				return "One lucky clone manages to deliver a clean kick to %target%'s sensitive vulva.";
			}
		}
		else {
			return "One of your shadow clones lunges forward and strikes %target% in the stomach.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You quickly dodge a shadow clone's attack.";
		}
		else if (modifier == Result.weak) {
			return "You lose sight of one of the clones until you feel a sharp spank on your ass cheek.";
		}
		else if (modifier == Result.strong) {
			if (target.hasBalls()) {
				return "A %self% clone gets a hold of your balls and squeezes them painfully.";
			}
			else {
				return "A %self% clone unleashes a quick roundhouse kick that hits your sensitive boobs.";
			}
		}
		else if (modifier == Result.critical) {
			if (target.hasBalls()) {
				return "One lucky %self% clone manages to land a snap-kick squarely on your unguarded jewels.";
			}
			else {
				return "One %self% clone hits you between the legs with a fierce cunt-punt.";
			}
		}
		else {
			return "One of %hisher% clones delivers a swift punch to your solar plexus.";
		}
	}
}
