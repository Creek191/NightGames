package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where the character is rubbing themselves against their opponent
 */
public class Frottage extends Skill {
	public Frottage(Character self) {
		super("Frottage", self);
		addTag(Attribute.Seduction);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 26;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& ((self.hasDick() && self.pantsless()) || self.has(Trait.strapped))
				&& target.pantsless()
				&& c.stance.mobile(self)
				&& !c.stance.sub(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public String describe() {
		return "Rub yourself against your opponent";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = Global.random(3 + self.getEffective(Attribute.Seduction) / 2 + target.getEffective(Attribute.Perception));
		if (self.human()) {
			if (target.hasDick()) {
				c.write(self, formattedDeal(damage, Result.special, target));
			}
			else {
				c.write(self, formattedDeal(damage, Result.normal, target));
			}
			damage = self.bonusProficiency(Anatomy.genitals, damage);
			self.pleasure(damage / 2, Anatomy.genitals, c);
		}
		else if (self.has(Trait.strapped)) {
			if (target.human()) {
				c.write(self, formattedReceive(damage, Result.special, target));
			}
			damage = self.bonusProficiency(Anatomy.toy, damage);
			target.buildMojo(-10);
		}
		else {
			if (target.human()) {
				c.write(self, formattedReceive(damage, Result.normal, target));
			}
			damage = self.bonusProficiency(Anatomy.genitals, damage);
			self.pleasure(damage / 2, Anatomy.genitals, c);
		}
		target.pleasure(damage, Anatomy.genitals, c);
		self.buildMojo(20);
		self.emote(Emotion.horny, 15);
		target.emote(Emotion.horny, 15);
	}

	@Override
	public Skill copy(Character user) {
		return new Frottage(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "You tease %target%'s penis with your own, dueling %himher% like a pair of fencers.";
		}
		else {
			return "You press your hips against %target%'s, rubbing %hisher% nether lips and clit with your dick.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "%self% thrusts %hisher% hips to prod your delicate jewels with %hisher% strap-on dildo. As you flinch "
					+ "and pull your hips back, %heshe% presses the toy against your cock, teasing your sensitive parts.";
		}
		else {
			return "%self% pushes %hisher% cock against the sensitive head of your member, dominating your manhood by "
					+ "pleasuring it with %hisher% own.";
		}
	}
}
