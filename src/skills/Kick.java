package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

/**
 * Defines a skill where a character kicks their opponent, either in their groin, or destroying clothing
 */
public class Kick extends Skill {
	public Kick(Character self) {
		super("Kick", self);
		this.speed = 8;
		addTag(Attribute.Power);
		addTag(SkillTag.PAINFUL);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power) >= 17;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.has(Trait.sportsmanship)
				&& c.stance.feet(self)
				&& (!c.stance.prone(self) || self.has(Trait.dirtyfighter))
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			var damage = Global.random(12) + self.getEffective(Attribute.Power);
			var r = Result.normal;
			if (c.stance.prone(self)) {
				r = Result.strong;
				damage *= 1.5;
			}
			if (self.getPure(Attribute.Footballer) >= 15 && target.distracted()) {
				r = Result.critical;
				damage *= 1.5;
			}
			if (!target.bottom.isEmpty() && self.getPure(Attribute.Ki) >= 21) {
				r = Result.special;
			}
			if (self.human()) {
				c.write(self, formattedDeal(damage, r, target));
				if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Yui")) {
					c.offerImage("Yui Kicked.png", "Art by AimlessArt");
				}

				// Delay shredding until message was written
				if (r == Result.special) {
					target.shred(1);
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, r, target));
				if (!Global.checkFlag(Flag.exactimages) || self.name().startsWith("Jewel")) {
					c.offerImage("Kick.jpg", "Art by AimlessArt");
				}
			}
			self.buildMojo(10);
			damage = self.bonusProficiency(Anatomy.feet, damage);
			target.pain(damage, Anatomy.genitals, c);
			if (self.has(Trait.wrassler)) {
				target.calm(damage / 4, c);
			}
			else {
				target.calm(damage / 2, c);
			}
			target.emote(Emotion.angry, 80);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Kick(user);
	}

	public Tactics type() {
		return Tactics.damage;
	}

	public String toString() {
		if (self.getPure(Attribute.Ki) >= 21) {
			return "Shatter Kick";
		}
		else {
			return "Kick";
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "Your kick hits nothing but air.";
		}
		if (modifier == Result.special) {
			return "You focus your ki into a single kick, targeting not %target%'s body, but %hisher% "
					+ target.bottom.peek().name() + ". The garment is completely destroyed, and your foot continues into "
					+ "%hisher% exposed groin.";
		}
		if (modifier == Result.critical) {
			if (target.hasBalls()) {
				return "You capitalize on %target%'s momentary inability to defend %himher%self – you quickly sweep the "
						+ "inside of %hisher% foot with yours, kicking %hisher% legs apart. %HisHer% arms flail out to "
						+ "%hisher% sides as %heshe% struggles to maintain %hisher% footing, and you take a short step "
						+ "back, lining up your attack. You wind up and launch a merciless kick right between %hisher% "
						+ "wide-open legs, your instep colliding solidly with %hisher% tender balls.";
			}
			else {
				return "You capitalize on %target%'s momentary inability to defend %himher%self – you quickly sweep the "
						+ "inside of %hisher% foot with yours, kicking %hisher% legs apart. %HisHer% arms flail out to "
						+ "%hisher% sides as %heshe% struggles to maintain %hisher% footing, and you take a short step "
						+ "back, lining up your attack. You wind up and launch a merciless kick right between %hisher% "
						+ "wide-open legs, your instep colliding solidly with %hisher% tender little pussy.";
			}
		}
		if (modifier == Result.strong) {
			if (target.hasBalls()) {
				return "Lying on the floor, you feign exhaustion, hoping %target% will lower %hisher% guard. As %heshe% "
						+ "approaches unwarily, you suddenly kick up between %hisher% legs, delivering a painful hit to "
						+ "%hisher% family jewels.";
			}
			else {
				return "Lying on the floor, you feign exhaustion, hoping %target% will lower %hisher% guard. As %heshe% "
						+ "approaches unwarily, you suddenly kick up between %hisher% legs, delivering a painful hit to "
						+ "%hisher% sensitive vulva.";
			}
		}
		else {
			if (target.hasBalls()) {
				return "You deliver a swift kick between %target%'s legs, hitting %himher% squarely on the balls.";
			}
			else {
				return "You deliver a swift kick between %target%'s legs, hitting %himher% squarely on the baby maker.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self%'s kick hits nothing but air.";
		}
		if (modifier == Result.special) {
			return "%self% launches a powerful kick straight at your groin, battering your poor testicles. Your "
					+ target.bottom.peek() + " crumble off your body, adding nudity to injury.";
		}
		if (modifier == Result.critical) {
			return "%self% capitalizes on your momentary inability to defend yourself; with a mischievous smirk, %heshe% "
					+ "sweeps the inside of your foot with %hishers%, kicking your legs apart. Your arms flail out to "
					+ "your sides as you struggle to maintain your footing, but in the split-second it takes you to "
					+ "regain your balance, %self% has already taken a short step back, and is lining you up for the "
					+ "coup-de-grace. Smirking slightly, %self% winds up and launches a merciless kick squarely into your "
					+ "dangling and unprotected testicles.";
		}
		if (modifier == Result.strong) {
			return "With %self% flat on %hisher% back, you quickly move in to press your advantage. Faster than you can "
					+ "react, %hisher% foot shoots up between your legs, dealing a critical hit on your unprotected balls.";
		}
		else {
			switch (Global.random(2)) {
				case 1:
					return "%self% delivers a swift and confident kick right into your sensitive nutsack. You can almost "
							+ "taste your balls in the back of your throat.";
				default:
					return "%self%'s foot lashes out into your delicate testicles with devastating force. ";
			}
		}
	}

	@Override
	public String describe() {
		if (self.getPure(Attribute.Ki) >= 21) {
			return "A precise, Ki-powered kick that can destroy clothing";
		}
		else {
			return "Kick your opponent in the groin";
		}
	}
}
