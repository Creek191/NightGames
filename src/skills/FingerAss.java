package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Stsflag;

/**
 * Defines a skill where a character fingers their opponent's ass
 */
public class FingerAss extends Skill {
	public FingerAss(Character self) {
		super("Finger Ass", self);
		this.accuracy = 6;
		addTag(Attribute.Seduction);
		addTag(Result.touch);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.pantsless()
				&& (target.is(Stsflag.oiled) || self.has(Trait.slipperyfingers))
				&& c.stance.reachBottom(self)
				&& !c.stance.prone(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = 4 + target.getEffective(Attribute.Perception);
		var type = Result.normal;
		if (c.attackRoll(this, self, target)) {

			damage += Global.random(self.getEffective(Attribute.Seduction) / 2);
			if (self.has(Trait.slipperyfingers)) {
				type = Result.special;
			}
			if (self.human()) {
				c.write(self, formattedDeal(damage, type, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, type, target));
			}
			damage = self.bonusProficiency(Anatomy.fingers, damage);
			target.pleasure(damage, Anatomy.ass, c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 28;
	}

	@Override
	public Skill copy(Character user) {
		return new FingerAss(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {

		if (modifier == Result.miss) {
			return "You grope at %target%'s ass, but miss.";
		}
		else if (modifier == Result.special) {
			return "You slip your hand into the crack of %target%’s ass. Before %heshe% can react to tense up, you slide "
					+ "your finger expertly into %hisher% anus. By the time %heshe% clenches in to try and keep you out, "
					+ "it’s too late. You wiggle your finger teasingly around within %hisher% ass for a moment before "
					+ "%heshe% manages to pull free.\n";
		}
		else {
			return "With the lubricant coating %target%’s ass, it’s all too easy to slip your finger inside %hisher% anus. "
					+ "You press it as deep as you can and wiggle it around for a bit before %heshe% manages to pull free.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% gropes at your anus, but misses the mark.";
		}
		else if (modifier == Result.special) {
			return "%self% slips %hisher% hand into the crack of your ass, and before you know what’s happening %heshe% "
					+ "plunges %hisher% finger directly into your asshole. You instinctively clench to try to keep %himher% "
					+ "out, but it’s too late. %HeShe% wiggles %hisher% finger around teasingly, the feeling of it inside "
					+ "your ass oddly arousing. You manage to shake off the stun and pull away before %heshe% works "
					+ "too much magic on your asshole.";
		}
		else {

			return "With lubricant coating your ass, %self% has no trouble slipping %hisher% finger inside your anus. "
					+ "%HeShe% presses it in as deep as %heshe% can, the feeling of your ass being penetrated like this "
					+ "oddly arousing. You manage to shake it off and pull away before you let it feel too good.";
		}
	}

	@Override
	public String describe() {
		return "Digitally stimulate opponent's anus";
	}
}
