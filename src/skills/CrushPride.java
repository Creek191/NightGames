package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Stsflag;

/**
 * Defines a skill where a character that is already influenced has their pride crushed, giving masochistic pleasure
 */
public class CrushPride extends Skill {
	public CrushPride(Character self) {
		super("Crush Pride", self);
		addTag(Attribute.Hypnosis);
	}

	@Override
	public boolean requirements(Character user) {
		return self.getPure(Attribute.Hypnosis) >= 8;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)
				&& !c.stance.behind(self)
				&& !c.stance.behind(target)
				&& (target.is(Stsflag.charmed) || target.is(Stsflag.enthralled))
				&& target.is(Stsflag.shamed);
	}

	@Override
	public String describe() {
		return "Exploit opponent's shame to give them extreme masochistic pleasure";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (target.is(Stsflag.shamed)) {
			var status = target.getStatus(Stsflag.shamed);
			if (status != null) {
				var damage = target.getStatusMagnitude("Shamed");
				damage += target.getStatusMagnitude("Horny");
				target.tempt(damage * self.getEffective(Attribute.Hypnosis) * 2, c);
				target.pleasure(1, Anatomy.head, c);
				target.removeStatus(status, c);
			}
		}
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.emote(Emotion.horny, 30);
		target.emote(Emotion.desperate, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new CrushPride(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "%target% is so embarrassed and horny, that it only takes a little push to link the two emotions. You "
				+ "place a simple suggestion in %hisher% head that %heshe% gets off on humiliation. In seconds, %hisher% "
				+ "expression shifts from shame to pure lust.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "As your shame threatens to overwhelm you, %self% laughs mockingly and points out that your cock is "
				+ "responding eagerly. Your humiliation fetish is getting the better of you, and %hisher% scornful look "
				+ "is almost enough to make you cum. Since when have you gotten off on humiliation? Your mind is too "
				+ "foggy right now. You'll think about it later.";
	}
}
