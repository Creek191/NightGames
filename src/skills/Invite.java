package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Anal;
import status.Shamed;

/**
 * Defines a skill where a character invites their opponent to fuck them
 */
public class Invite extends Skill {
	public Invite(Character self) {
		super("Invite", self);
		addTag(Attribute.Submissive);
		addTag(Result.intercourse);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive) >= 6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.pantsless()
				&& target.canAct()
				&& (target.pantsless() || target.has(Trait.strapped))
				&& ((target.hasDick() || (target.has(Trait.strapped)) && !c.stance.behind(self))
				|| !c.stance.behind(target))
				&& !c.stance.mobile(self)
				&& c.stance.mobile(target)
				&& !c.stance.penetration(self)
				&& !c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "Invite your opponent to give you a good fucking";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (!target.isErect()) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
			self.add(new Shamed(self), c);
		}
		else if (anal(c, target)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.anal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.anal, target));
			}
			c.stance = new Anal(target, self);
			self.pleasure(Global.random(5) + self.getEffective(Attribute.Perception), Anatomy.genitals, c);
			target.pleasure(Global.random(5) + self.getEffective(Attribute.Perception), Anatomy.genitals, c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
				c.offerImage("Invite.jpg", "Art by AimlessArt");
			}
			c.stance = c.stance.insert(target);
			self.pleasure(Global.random(5) + self.getEffective(Attribute.Perception), Anatomy.genitals, c);
			target.pleasure(Global.random(5) + self.getEffective(Attribute.Perception), Anatomy.genitals, c);
		}
		if (self.getArousal().get() < 15) {
			self.getArousal().set(15);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Invite(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You offer your manly pole to %target%, but %heshe% just laughs at you. That's a bit of a blow to your "
					+ "pride.";
		}
		else if (modifier == Result.anal) {
			return "You spread your buttcheeks, offering your puckered anus. You try to contain a moan as %target% accepts "
					+ "your invitation and thrusts firmly into you.";
		}
		else {
			return "You slide your eager cock against %target%'s pussy lips, making it clear what you're craving. %HeShe% "
					+ "grins, confident in %hisher% superior position and maneuvers your member into %hisher% tight entrance.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% spreads her lower lips and practically begs you to take %himher%. You hate to disappoint %himher%, "
					+ "but you're not quite hard enough to manage it.";
		}
		else if (modifier == Result.anal) {
			return "%self% spreads %hisher% tight buttcheeks apart, looking away from you in shame. Well, if %heshe%'s "
					+ "that eager, you could always use a change of pace. You slide the tip of your cock into %hisher% "
					+ "tight anus and thrust the rest of the way inside %himher%.";
		}
		else {
			return "%self% opens %hisher% lower lips in a lewd invitation and looks at you with needy eyes. There's no "
					+ "way you'd turn down such a tempting offer. You shove your cock into %himher% as deep as it can go.";
		}
	}

	private boolean anal(Combat c, Character target) {
		return (target.hasDick() || target.has(Trait.strapped)) && !self.hasPussy() && c.stance.behind(target);
	}
}
