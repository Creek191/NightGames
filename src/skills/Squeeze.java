package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import items.Toy;

/**
 * Defines a skill where a character squeezes their opponent's balls
 */
public class Squeeze extends Skill {
	public Squeeze(Character self) {
		super("Squeeze Balls", self);
		addTag(SkillTag.PAINFUL);
		addTag(SkillTag.DOM);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.has(Trait.shy)
				&& target.hasBalls()
				&& c.stance.reachBottom(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			var damage = Global.random(self.getEffective(Attribute.Power)) + target.getEffective(Attribute.Perception) - (2 * target.bottom.size());
			var type = Result.normal;
			if (target.pantsless()) {
				if (self.has(Toy.ShockGlove) && self.canSpend(Pool.BATTERY, 2)) {
					self.spend(Pool.BATTERY, 2);
					type = Result.special;
					damage += self.getEffective(Attribute.Science);
				}
				else if (target.has(Trait.armored)) {
					type = Result.item;
				}
			}
			else if (self.id() == ID.MARA && self.getRank() >= 2) {
				type = Result.unique;
				damage += Global.random(self.getLevel());
			}
			else {
				type = Result.weak;
			}
			if (self.human()) {
				c.write(self, formattedDeal(damage, type, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, type, target));
				if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.MARA) {
					c.offerImage("Squeeze Balls.jpg", "Art by AimlessArt");
				}
				c.offerImage("Balls.jpg", "Art by AimlessArt");
			}
			target.pain(damage, Anatomy.genitals, c);
			if (self.has(Trait.wrassler)) {
				target.calm(damage / 4, c);
			}
			else {
				target.calm(damage / 2, c);
			}
			self.buildMojo(10);
			target.emote(Emotion.angry, 15);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power) >= 9;
	}

	@Override
	public Skill copy(Character user) {
		return new Squeeze(user);
	}

	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You try to grab %target%'s balls, but %heshe% avoids it.";
		}
		else if (modifier == Result.special) {
			return "You use your shock glove to deliver a painful jolt directly into %target%'s testicles.";
		}
		else if (modifier == Result.weak) {
			return "You grab the bulge in %target%'s " + target.bottom.peek().getName() + " and squeeze.";
		}
		else if (modifier == Result.item) {
			return "You grab the bulge in %target%'s " + target.bottom.peek().getName() + ", but find it solidly protected.";
		}
		else {
			return "You manage to grab %target%'s balls and squeeze them hard. You feel a twinge of empathy when %heshe% "
					+ "cries out in pain, but you maintain your grip.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% grabs at your balls, but misses.";
		}
		else if (modifier == Result.unique) {
			return "Mara deftly slips her hand down the front of your pants and tightly wraps her small fingers around "
					+ "your testicles, holding your balls firmly in her gloved hand.  You are thankful that she isn't "
					+ "squeezing them very hard, but you suddenly feel a debilitating shock of electricity being delivered "
					+ "directly to your ballsack.";
		}
		else if (modifier == Result.special) {
			return "%self% grabs your naked balls roughly in %hisher% gloved hand. A painful jolt of electricity shoots "
					+ "through your groin, sapping your will to fight.";
		}
		else if (modifier == Result.weak) {
			return "%self% grabs your balls through your " + target.bottom.peek().getName() + " and squeezes hard.";
		}
		else if (modifier == Result.item) {
			return "%self% grabs your crotch through your " + target.bottom.peek().getName() + ", but you can barely feel it.";
		}
		else {
			switch (Global.random(2)) {
				case 1:
					return "%self% grabs both your balls roughly in %hisher% hand, crushing them in %hisher% grip. "
							+ "You feel your voice momentarily raise an octave as your testicles are squeezed together.";
				default:
					return "%self% reaches between your legs and grabs your exposed balls. You writhe in pain as %heshe% "
							+ "pulls and squeezes them.";
			}
		}
	}

	public String toString() {
		if (self.has(Toy.ShockGlove)) {
			return "Shock Balls";
		}
		else {
			return name;
		}
	}

	@Override
	public String describe() {
		return "Grab opponent's groin, deals more damage if naked";
	}
}
