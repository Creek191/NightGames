package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import stance.Stance;

/**
 * Defines a skill where a character puts all of their mojo into fucking their opponent
 */
public class SpiralThrust extends Skill {
	public SpiralThrust(Character self) {
		super("Spiral Thrust", self);
		addTag(Result.intercourse);
	}

	@Override
	public boolean requirements(Character user) {
		return user.has(Trait.spiral);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(10)
				&& c.stance.dom(self)
				&& c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = self.getMojo().get() / 2;
		damage = Math.min(damage, 20 + self.getLevel());
		damage = self.bonusProficiency(Anatomy.genitals, damage);
		if (c.stance.en == Stance.anal) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.anal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.anal, target));
			}
			target.pleasure(damage, Anatomy.ass, c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			target.pleasure(damage, Anatomy.genitals, c);
		}

		self.getMojo().empty();
		var recoil = target.getSexPleasure(3, Attribute.Seduction);
		recoil += target.bonusRecoilPleasure(recoil);
		if (self.has(Trait.experienced)) {
			recoil = recoil / 2;
		}

		self.pleasure(recoil, Anatomy.genitals, c);
		self.emote(Emotion.horny, 20);
		c.stance.setPace(2);
	}

	@Override
	public Skill copy(Character user) {
		return new SpiralThrust(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.anal) {
			return "You unleash your strongest technique into %target%'s ass, spiraling your hips and stretching %hisher% "
					+ "tight sphincter.";
		}
		else {
			return "As you thrust into %target%'s hot pussy, you feel a power welling up inside you. You put everything "
					+ "you have into moving your hips circularly while you continue to drill into %himher%.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.anal) {
			return "%self% drills into your ass with extraordinary power. Your head seems to go blank and you fall face "
					+ "down to the ground as your arms turn to jelly and give out.";
		}
		else {
			return "%self% begins to move %hisher% hips wildly in circles, rubbing every inch of your cock with %hisher% "
					+ "hot, slippery pussy walls, bringing you more pleasure than you thought possible.";
		}
	}

	@Override
	public String describe() {
		return "Converts your mojo into fucking: All Mojo";
	}
}
