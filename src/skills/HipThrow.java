package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.StandingOver;

/**
 * Defines a skill where a character throws their opponent to the ground
 */
public class HipThrow extends Skill {
	public HipThrow(Character self) {
		super("HipThrow", self);
		addTag(Attribute.Power);
	}

	@Override
	public boolean requirements(Character user) {
		return user.has(Trait.judonovice);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(10)
				&& c.stance.mobile(self)
				&& c.stance.mobile(target)
				&& !c.stance.prone(self)
				&& !c.stance.prone(target)
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(10);
		if (self.check(Attribute.Power, target.knockdownDC())) {
			var damage = Global.random(6) + target.getEffective(Attribute.Power) / 2;
			if (self.human()) {
				c.write(self, formattedDeal(damage, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, Result.normal, target));
			}
			target.pain(damage, Anatomy.chest, c);
			c.stance = new StandingOver(self, target);
			target.emote(Emotion.angry, 5);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new HipThrow(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.normal) {
			return "%target% rushes toward you, but you step in close and pull %himher% towards you, using %hisher% "
					+ "momentum to throw %himher% across your hip and onto the floor.";
		}
		else {
			return "As %target% advances, you pull %himher% towards you and attempt to throw %himher% over your hip, but "
					+ "%heshe% steps away from the throw and manages to keep %hisher% footing.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.normal) {
			return "You see a momentary weakness in %self%'s guard and lunge toward %himher% to take advantage of it. "
					+ "The next thing you know, you're hitting the floor behind %himher%.";
		}
		else {
			return "%self% grabs your arm and pulls you off balance, but you manage to plant your foot behind %hisher% leg "
					+ "sweep. This gives you a more stable stance than %himher% and %heshe% has to break away to stay on "
					+ "her feet.";
		}
	}

	@Override
	public String describe() {
		return "Throw your opponent to the ground, dealing some damage: 10 Mojo";
	}
}
