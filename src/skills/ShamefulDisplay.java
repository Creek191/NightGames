package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Horny;
import status.Shamed;

/**
 * Defines a skill where a character degrades themself to turn on their opponent
 */
public class ShamefulDisplay extends Skill {
	public ShamefulDisplay(Character self) {
		super("Shameful Display", self);
		addTag(Attribute.Submissive);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive) >= 15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.nude()
				&& c.stance.mobile(self);
	}

	@Override
	public String describe() {
		return "Degrade yourself to entice your opponent";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new Shamed(self), c);
		if (target.getMood() == Emotion.dominant) {
			target.add(new Horny(target, self.getEffective(Attribute.Submissive) / 3, 2), c);
		}
		else {
			target.add(new Horny(target, self.getEffective(Attribute.Submissive) / 4, 2), c);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new ShamefulDisplay(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You spread your legs, exposing your naked cock and balls, and thrust your hips out in a show of "
				+ "submission. %target% practically drools at the sight, while you struggle to bear the shame.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% lifts %hisher% hips and spreads %hisher% pussy lips open. %HeShe%'s bright red with shame, but "
				+ "the sight is lewd enough to drive you wild.";
	}
}
