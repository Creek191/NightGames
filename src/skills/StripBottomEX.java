package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.ID;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

/**
 * Defines a skill where a character skillfully strips their opponent's bottom clothes
 */
public class StripBottomEX extends Skill {
	public StripBottomEX(Character self) {
		super("Tricky Strip Bottoms", self);
		this.speed = 3;
		addTag(SkillTag.STRIPPING);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(15)
				&& !target.pantsless()
				&& c.stance.reachBottom(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int strip;
		var style = Result.clever;
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Power) > self.getEffective(Attribute.Seduction)) {
			style = Result.powerful;
		}
		else if (self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Cunning)) {
			style = Result.seductive;
		}
		self.spendMojo(15);
		if (style == Result.powerful) {
			strip = self.getEffective(Attribute.Power);
		}
		else if (style == Result.clever) {
			strip = self.getEffective(Attribute.Cunning);
			strip *= 3;
		}
		else {
			strip = self.getEffective(Attribute.Seduction);
		}
		if (target.stripAttempt(strip, self, c, target.bottom.peek())) {
			if (self.human()) {
				c.write(self, formattedDeal(0, style, target));
				if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Yui")) {
					c.offerImage("Strip Bottom Female.jpg", "Art by AimlessArt");
				}
				if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.MARA) {
					c.offerImage("Mara StripBottom.jpg", "Art by AimlessArt");
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, style, target));
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.MARA) {
					c.offerImage("Strip Bottom Male.jpg", "Art by AimlessArt");
				}
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.ANGEL) {
					c.offerImage("Angel StripBottom.jpg", "Art by AimlessArt");
				}
			}
			target.strip(1, c);
			if (self.getPure(Attribute.Cunning) >= 30 && !target.pantsless()) {
				if (target.stripAttempt(strip, self, c, target.bottom.peek())) {
					if (self.human()) {
						c.write(self, formattedDeal(0, Result.strong, target));
					}
					else if (target.human()) {
						c.write(self, formattedReceive(0, Result.strong, target));
					}
					target.strip(1, c);
					target.emote(Emotion.nervous, 10);
				}
			}
			if (self.human() && target.nude()) {
				c.write(target, target.nakedLiner());
			}
			if (target.human() && target.pantsless()) {
				if (target.getArousal().get() >= 15) {
					c.write("Your boner springs out, no longer restrained by your pants.");
				}
				else {
					c.write(self.name() + " giggles as your flacid dick is exposed");
				}
			}
			target.emote(Emotion.nervous, 10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new StripBottomEX(user);
	}

	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You grab %target%'s " + target.bottom.peek().getName() + ", but %heshe% scrambles away before you "
					+ "can strip %himher%.";
		}
		else if (modifier == Result.strong) {
			return "Taking advantage of the situation, you also manage to snag %hisher% "
					+ target.bottom.peek().getName() + ".";
		}
		if (modifier == Result.powerful) {
			return "You forcefully rip %target%'s " + target.bottom.peek().getName() + " down to the floor before %heshe% "
					+ "can resist.";
		}
		else if (modifier == Result.clever) {
			return "You playfully give %target% a sharp pinch on the butt. When %heshe% yelps and reflexively covers"
					+ " %hisher% bottom, you switch to your real target and pull down %hisher% "
					+ target.bottom.peek().getName() + ".";
		}
		else {
			return "With a smile and a wink, you manage to literally charm the " + target.bottom.peek().getName() + " off of %target%.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to pull down your " + target.bottom.peek().getName() + ", but you hold them up.";
		}
		else if (modifier == Result.strong) {
			return "Before you can react, %heshe% also strips off your " + target.bottom.peek().getName() + ".";
		}
		if (modifier == Result.powerful) {
			return "%self% lunges at you and yanks down your " + target.bottom.peek().getName() + " with unexpected force.";
		}
		else if (modifier == Result.clever) {
			return "%self% slips a hand between your legs to lightly pinch your balls. You reflexively flinch and move to "
					+ "protect your delicate parts, but %heshe% takes advantage of your momentary panic to pull down your "
					+ target.bottom.peek().getName() + ".";
		}
		else {
			return "%self% gives you a dazzling smile and suggestively tugs at the waistband of your "
					+ target.bottom.peek().getName() + ". It would be trivially easy to stop %himher%, but your desire "
					+ "gets the better of you, and you let yourself be stripped.";
		}
	}

	@Override
	public String toString() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Power) > self.getEffective(Attribute.Seduction)) {
			return "Forceful Strip Bottoms";
		}
		else if (self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Cunning)) {
			return "Charming Strip Bottoms";
		}
		return name;
	}

	@Override
	public String describe() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Power) > self.getEffective(Attribute.Seduction)) {
			return "Attempt to remove opponent's pants by force: 15 Mojo";
		}
		else if (self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Cunning)) {
			return "Attempt to remove opponent's pants by seduction: 15 Mojo";
		}
		return "Attempt to remove opponent's pants with extra Mojo: 15 Mojo";
	}
}
