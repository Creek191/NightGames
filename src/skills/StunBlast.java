package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character uses a blast of sound and light to stun their opponent
 */
public class StunBlast extends Skill {
	public StunBlast(Character self) {
		super("Stun Blast", self);
		addTag(Attribute.Science);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science) >= 9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(Pool.BATTERY, 4)
				&& c.stance.mobile(self)
				&& !c.stance.behind(self);
	}

	@Override
	public String describe() {
		return "A blast of light and sound with a chance to stun: 4 Battery";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.BATTERY, 4);
		if (Global.random(10) >= 5) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			target.pain(target.getStamina().get(), Anatomy.head, c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new StunBlast(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You overload the emitter on your arm, but %target% shields %hisher% face to avoid the flash.";
		}
		else {
			return "You overload the emitter on your arm, duplicating the effect of a flashbang. %target% staggers as "
					+ "the blast disorients %himher%.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% covers %hisher% face and points a device in your direction. Sensing danger, you shield your "
					+ "eyes just as the flashbang goes off.";
		}
		else {
			return "%self% points a device in your direction that glows slightly. A sudden flash of light disorients "
					+ "you and your ears ring from the blast.";
		}
	}
}
