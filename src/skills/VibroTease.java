package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import items.Toy;
import stance.Stance;

/**
 * Defines a skill where a character uses their strap-on vibrator to tease their opponent
 */
public class VibroTease extends Skill {
	public VibroTease(Character self) {
		super("Vibro-Tease", self);
		addTag(SkillTag.TOY);
		addTag(Result.anal);
	}

	@Override
	public boolean requirements(Character user) {
		return user.has(Toy.Strapon2);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.has(Toy.Strapon2)
				&& c.stance.en == Stance.anal
				&& c.stance.dom(self);
	}

	@Override
	public String describe() {
		return "Turn up the strap-on vibration";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else {
			if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
		}
		var damage = 3 + self.getEffective(Attribute.Seduction) / 2
				+ target.getEffective(Attribute.Perception)
				+ (self.getEffective(Attribute.Science) / 2);
		damage = self.bonusProficiency(Anatomy.toy, damage);
		target.pleasure(damage, Anatomy.genitals, c);
		self.pleasure(2 + self.getEffective(Attribute.Perception), Anatomy.genitals, c);
		self.buildMojo(20);
		c.stance.setPace(0);
	}

	@Override
	public Skill copy(Character user) {
		return new VibroTease(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% cranks up the vibration of %hisher% strapon to the maximum level, which stirs up your insides. "
				+ "%HeShe% teasingly pokes the tip against your prostate which causes your limbs to get shaky from the "
				+ "pleasure.";
	}
}
