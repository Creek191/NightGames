package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import items.Consumable;
import status.Enthralled;

/**
 * Defines a skill where a character is enthralled using a dark talisman
 */
public class DarkTalisman extends UseItem {
	public DarkTalisman(Character self) {
		super(Consumable.Talisman, self);
		addTag(Attribute.Dark);
		addTag(SkillTag.CONSUMABLE);
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Consumable.Talisman, 1);
		if (self.human()) {
			c.write(formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(formattedReceive(0, Result.normal, target));
		}
		target.add(new Enthralled(target, self), c);
	}

	@Override
	public Skill copy(Character user) {
		return new DarkTalisman(user);
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		switch (Global.random(2)) {
			case 1:
				return "You hold the talisman in front of %target%'s face and watch %hisher% eyes lose focus as %heshe% "
						+ "falls under your control.";
			default:
				return "You brandish the dark talisman, which seems to glow with power. The trinket crumbles to dust, "
						+ "but you see the image remain in the reflection of %target%'s eyes.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		switch (Global.random(3)) {
			case 2:
				return "You find your eyes locked on the talisman in %self%'s hand, unsure of when exactly it got there.";
			case 1:
				return "%self% pulls out an evil looking talisman. Your eyes are drawn to it against your will.";
			default:
				return "%self% holds up a strange talisman. You feel compelled to look at the thing, captivated by its "
						+ "unholy nature.";
		}
	}
}
