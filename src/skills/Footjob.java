package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import status.Stsflag;

/**
 * Defines a skill where a character pleasures their opponent using their feet
 */
public class Footjob extends Skill {
	public Footjob(Character self) {
		super("Footjob", self);
		this.speed = 4;
		addTag(Attribute.Seduction);
		addTag(Result.feet);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 22;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.pantsless()
				&& c.stance.feet(self)
				&& c.stance.prone(self) != c.stance.prone(target)
				&& !c.stance.penetration(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			var damage = Global.random(2) + self.getEffective(Attribute.Seduction) / 2 + target.getEffective(Attribute.Perception) / 2 + 4;
			var r = Result.normal;
			if (self.getPure(Attribute.Footballer) >= 18) {
				damage += self.getEffective(Attribute.Footballer) / 2;
				r = Result.upgrade;
				if (target.is(Stsflag.masochism)) {
					r = Result.special;
					target.pain(self.getEffective(Attribute.Footballer) / 4, Anatomy.genitals, c);
				}
			}
			if (self.human()) {
				c.write(self, formattedDeal(damage, r, target));
			}
			else if (target.human()) {
				if (!Global.checkFlag(Flag.exactimages) || self.name().startsWith("Mara")) {
					c.offerImage("Footjob.jpg", "Art by Sky Relyks");
				}
				c.offerImage("Footjob2.jpg", "Art by AimlessArt");
				c.write(self, formattedReceive(damage, r, target));
			}
			damage += self.getMojo().get() / 10;
			damage = self.bonusProficiency(Anatomy.feet, damage);
			target.pleasure(damage, Anatomy.genitals, c);
			if (c.stance.dom(self)) {
				self.buildMojo(20);
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Footjob(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "You try to slip your foot between %target%'s legs, but %heshe% slips away.";
			}
			else {
				return "You aim your foot between %target%'s legs, but miss.";
			}
		}
		else if (target.hasDick()) {
			if (Global.random(2) == 0) {

				return "You run your toes along the head of %target%'s cock and watch %hisher% twitch. You then press "
						+ "your foot across %hisher% shaft and trap %hisher% cock between your sole and %hisher% stomach.";
			}
			else {
				return "You press your foot against %target%'s cock and stimulate it by grinding it with the sole.";
			}
		}
		else {
			switch (Global.random(3)) {
				case 2:
					return "You slip a toe between %target%'s pussy lips, %hisher% own wetness making it easy, and pump "
							+ "it in and out. Once your foot is sufficiently lubricated, you press it against %hisher% "
							+ "clit and swirl it as your toe probes %himher%.";
				case 1:
					return "You prod at %target%'s clit with your big toe, making %himher% a little wetter before you "
							+ "slip it just inside %hisher% pussy.";
				default:
					return "You rub your foot against %target%'s pussy lips, using %hisher% own wetness as lubricant, "
							+ "and stimulate %hisher% love button with your toe.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "%self% tries to press %hisher% foot against your cock, but you are wise to %hisher% ploy and dodge.";
			}
			else {
				return "%self% swings %hisher% foot at your groin, but misses.";
			}
		}
		else if (modifier == Result.upgrade) {
			return "%self% plants %hisher% foot in your groin, expertly wedging %hisher% toes firmly between your testicles. "
					+ "With a look of both fascination and arousal, %heshe% playfully wriggles %hisher% toes against your "
					+ "defenseless nuts, watching them roll and squirm in your sack. The caress of %hisher% smooth, supple "
					+ "feet against your scrotum feels incredible, and you can’t bring yourself to resist %himher% as "
					+ "%heshe% strokes the sole of %hisher% foot up and down your grateful shaft.";
		}
		else if (modifier == Result.special) {
			return "%self% plants %hisher% foot in your groin, expertly wedging %hisher% toes firmly between your testicles. "
					+ "With a look of both fascination and arousal, %heshe% playfully wriggles %hisher% toes against your "
					+ "defenseless nuts, watching them roll and squirm in your sack. The caress of %hisher% smooth, supple "
					+ "feet against your scrotum feels incredible, and you can’t bring yourself to resist %hisher% as "
					+ "%heshe% strokes the sole of %hisher% foot up and down your grateful shaft, teasingly smacking your "
					+ "balls around with a few playful kicks. %HeShe% traps your nuts against your pubic bone with %hisher% "
					+ "foot and slowly begins twisting, gently grinding your delicates under the balls of %hisher% foot, "
					+ "causing them to ache and swell with desire.";
		}
		else {
			if (Global.random(2) == 0) {
				return "%self% tickles the shaft of your cock with %hisher% toes before rolling it over and rubbing it up "
						+ "and down with %hisher% silky soles. You're surprised at how good %hisher% foot feels and %heshe% "
						+ "seems ecstatic when your cock shows its thanks by leaking precum all over %hisher% tootsies.";
			}
			else {
				return "%self% rubs your dick with the sole of %hisher% soft foot. From time to time, %heshe% teases you "
						+ "by pinching the glans between %hisher% toes and jostling your balls.";
			}
		}
	}

	public String toString() {
		if (self.getPure(Attribute.Footballer) >= 18) {
			return "Dribble";
		}
		return name;
	}

	@Override
	public String describe() {
		return "Pleasure your opponent with your feet, more effective with high Mojo";
	}
}
