package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import pet.Ptype;
import status.BD;
import status.Horny;
import status.Masochistic;
import status.Shamed;

/**
 * Defines a skill where a character uses their fetish goblin's sperm to induce nymphomania in their opponent
 */
public class Nymphomania extends Skill {

	public Nymphomania(Character self) {
		super("Nymphomania", self);
		addTag(Attribute.Fetish);
		addTag(SkillTag.ULTIMATE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Fetish) >= 30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.getArousal().get() >= 100
				&& self.pet != null
				&& self.pet.type() == Ptype.fgoblin
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Finish off your goblin and give your opponent a taste of their sperm to shame and arouse them.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.pet.remove();
		target.add(new Masochistic(target), c);
		target.add(new BD(target), c);
		target.add(new Shamed(target), c);
		target.add(new Horny(target, 5, 5), c);
		target.emote(Emotion.horny, 60);
	}

	@Override
	public Skill copy(Character user) {
		return new Nymphomania(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You pull off your fetish goblin's cock ring and fill her with sexual energy. She orgasms hard, shooting "
				+ "a stream of jizz that hits %target% right in the face. The goblin's potent seed is capable of spreading "
				+ "her perverse and overactive libido. Soon, %target% is bombarded by conflicting desires.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% grabs %hisher% goblin by the dick and quickly strokes her to ejaculation. The load hits you like "
				+ "a squirt gun, and you immediately feel feverish. A dozen new fetishes vie for your attention. Oh God. "
				+ "Everything in the world is so sexual! Does the fetish goblin feel this way all the time? You aren't "
				+ "sure whether to pity her or envy her.";
	}
}
