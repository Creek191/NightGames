package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;

/**
 * Defines a skill where a character uses a spell to turn their opponent's clothes into flowers
 */
public class NakedBloom extends Skill {
	public NakedBloom(Character self) {
		super("Naked Bloom", self);
		addTag(Attribute.Arcane);
		addTag(SkillTag.STRIPPING);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane) >= 15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(20)
				&& !target.nude()
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Cast a spell to transform your opponents clothes into flower petals: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(20);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
			c.write(target, target.nakedLiner());
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.nudify();
		target.emote(Emotion.nervous, 10);
	}

	@Override
	public Skill copy(Character user) {
		return new NakedBloom(user);
	}

	@Override
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You chant a short spell and turn %target%'s clothes into a burst of flowers. The cloud of flower petals "
				+ "flutters to the ground, exposing %hisher% nude body.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% mumbles a spell and you're suddenly surrounded by an eruption of flower petals. As the petals "
				+ "settle, you realize you've been stripped completely naked.";
	}
}
