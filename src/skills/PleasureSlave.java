package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import status.Shamed;
import status.Stsflag;

/**
 * Defines a skill where a character uses their shame to service their master
 */
public class PleasureSlave extends Skill {
	public PleasureSlave(Character self) {
		super("Pleasure Slave", self);
		addTag(Attribute.Submissive);
		addTag(SkillTag.ULTIMATE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getEffective(Attribute.Submissive) >= 30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !self.bound()
				&& !self.distracted()
				&& target.pantsless()
				&& self.is(Stsflag.shamed)
				&& self.getStatus(Stsflag.shamed).mag() >= 5
				&& c.stance.sub(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public String describe() {
		return "Use the power of your shame to service your mistress";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = Math.max(self.getEffective(Attribute.Seduction),
				self.getEffective(Attribute.Submissive)) + target.getEffective(Attribute.Perception);
		damage *= self.getStatusMagnitude("Shamed");
		damage = self.bonusProficiency(Anatomy.mouth, damage);
		if (self.has(Trait.silvertongue)) {
			damage *= 1.2;
		}
		self.add(new Shamed(self, -5));
		if (self.human()) {
			c.write(self, formattedDeal(damage, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(damage, Result.normal, target));
		}
		target.pleasure(damage, Anatomy.genitals, c);
	}

	@Override
	public Skill copy(Character user) {
		return new PleasureSlave(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "Feeling like a worthless slave boy, you devote your entire being to servicing %target%. You lavish %hisher% "
				+ "lovely flower with your tongue, as though %hisher% pleasure is the only thing that can redeem you. "
				+ "Judging by your mistress' moaning and trembling, you are doing an acceptable job.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% holds your penis worshipfully, before taking the entire length in %hisher% mouth. %HeShe% licks "
				+ "and sucks with tremendous enthusiasm, giving %hisher% all to service you. The pleasure is too much to "
				+ "handle, and despite %hisher% attitude, you're completely at %hisher% mercy.";
	}
}
