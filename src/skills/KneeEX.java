package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

/**
 * Defines a skill where a character skillfully knees their opponent in the groin
 */
public class KneeEX extends Skill {
	public KneeEX(Character self) {
		super("Mighty Knee", self);
		this.speed = 4;
		addTag(Attribute.Power);
		addTag(SkillTag.PAINFUL);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(20)
				&& !self.has(Trait.sportsmanship)
				&& c.stance.mobile(self)
				&& !c.stance.prone(self)
				&& !c.stance.behind(target)
				&& !c.stance.penetration(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int damage;
		var style = Result.powerful;
		if (self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Cunning) && self.getEffective(Attribute.Seduction) > self.getEffective(
				Attribute.Power)) {
			style = Result.seductive;
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Power)) {
			style = Result.clever;
		}
		self.spendMojo(20);
		if (c.attackRoll(this, self, target)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, style, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, style, target));
				if (Global.random(5) >= 3) {
					c.write(self, self.bbLiner());
				}
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.CASSIE) {
					c.offerImage("Knee.png", "Art by AimlessArt");
				}
			}
			if (style == Result.powerful) {
				damage = 4 + Global.random(11) + self.getEffective(Attribute.Power);
				damage *= 1.3f;
			}
			else if (style == Result.clever) {
				damage = 4 + Global.random(11) + self.getEffective(Attribute.Cunning);
			}
			else {
				damage = 4 + Global.random(11) + self.getEffective(Attribute.Seduction);
			}

			target.pain(damage, Anatomy.genitals, c);
			if (self.has(Trait.wrassler)) {
				target.calm(damage / 2, c);
			}
			else {
				target.calm(damage, c);
			}
			target.emote(Emotion.angry, 40);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power) >= 10 && !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new KneeEX(user);
	}

	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return target.name() + " blocks your knee strike.";
		}
		if (target.hasBalls()) {
			if (modifier == Result.powerful) {
				return "You brace yourself for a moment, before unleashing your strongest knee strike to %target%'s "
						+ "vulnerable ballsack. %HisHer% eyes water, and %hisher% mouth drops open in silent agony.";
			}
			else if (modifier == Result.clever) {
				return "You distract %target% with a cunning feint, before striking with a knee-shaped sneak attack to "
						+ "%hisher% dangling balls. You don't have a lot of power behind it, but accuracy and surprise do "
						+ "the job just fine.";
			}
			else {
				return "You seductively pull %target% into a heated kiss. You feel %hisher% erection poking you, which "
						+ "helps you locate your real target without looking. You deliver a quick, decisive knee strike "
						+ "to %hisher% defenseless balls. You feel %hisher% completely freeze in shock and pain, but hold "
						+ "the kiss for another couple seconds before releasing %himher%.";
			}
		}
		else {
			if (modifier == Result.powerful) {
				return "You brace yourself for a moment, before unleashing your strongest knee strike to %target%'s "
						+ "unguarded groin. With no testicles to protect, %heshe% probably wasn't expecting a low blow. "
						+ "Judging by %hisher% pained expression, %hisher% pussy is a pretty effective target.";
			}
			else if (modifier == Result.clever) {
				return "You distract %target% with a cunning feint, before delivering a sharp knee strike to %hisher% "
						+ "sensitive vulva. It's a quick attack without much power, but %heshe% was completely unprepared "
						+ "for it. A true critical hit.";
			}
			else {
				return "You tease %target% with multiple quick kisses, until you're sure %heshe%'s completely focused on "
						+ "your lips. While %heshe%'s distracted and defenseless, you knee %himher% firmly in the pussy. "
						+ "%HisHer% eyes go wide in shock for a moment, before they start to water from the pain.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to knee you in the balls, but you block it.";
		}
		if (modifier == Result.powerful) {
			return "%self% grabs you by the shoulders and slams %hisher% knee into your groin with devastating force. "
					+ "The pain is indescribable, and for a second you wonder if your testicles were pushed into your body "
					+ "by the force.";
		}
		else if (modifier == Result.clever) {
			return "%self%'s eyes momentarily dart past you. %HeShe% tries to hide %hisher% reaction, but not quite good "
					+ "enough. You reflexively look behind you, but there's nothing there. Suddenly, a burst of pain from "
					+ "your groin hits you. You left your guard down just long enough for a sneaky knee to the balls.";
		}
		else {
			return "%self% licks %hisher% lips seductively, before leaning in to claim yours. Despite the feeling that "
					+ "%heshe% has the advantage here, you can't resist accepting the kiss eagerly. The moment your lips "
					+ "meet, %heshe% plants %hisher% knee painfully between your legs. The kiss was just a diversion. "
					+ "%HisHer% real target were your delicate balls.";
		}
	}

	@Override
	public String toString() {
		if (self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Power)) {
			return "Kiss and Knee";
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Power)) {
			return "Tricky Knee";
		}
		else {
			return name;
		}
	}

	@Override
	public String describe() {
		if (self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Seduction) > self.getEffective(Attribute.Power)) {
			return "Knee strike after lowering your target's guard with your charms: 15 Mojo";
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Power)) {
			return "Knee strike while misdirecting your target: 15 Mojo";
		}
		else {
			return "Mojo boosted Knee strike: 15 Mojo";
		}
	}
}
