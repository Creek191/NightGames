package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Stance;
import status.Shamed;

/**
 * Defines a skill where a character uses their tail to peg their opponent
 */
public class TailPeg extends Skill {
	public TailPeg(Character self) {
		super("Tail Peg", self);
		addTag(Attribute.Dark);
		addTag(Result.anal);
		addTag(SkillTag.TAIL);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark) > 1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(20)
				&& self.getArousal().get() >= 30
				&& self.has(Trait.tailed)
				&& target.pantsless()
				&& c.stance.en != Stance.standing
				&& c.stance.en != Stance.standingover
				&& !(c.stance.en == Stance.anal && c.stance.dom(self));
	}

	@Override
	public String describe() {
		return "Shove your tail up your opponent's ass.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			self.spendMojo(20);
			var shamed = false;
			if (Global.random(4) == 2) {
				target.add(new Shamed(target), c);
				shamed = true;
			}
			if (target.human()) {
				if (c.stance.penetration(self)) {
					c.write(self, formattedReceive(0, Result.special, target));
				}
				else if (c.stance.dom(target)) {
					c.write(self, formattedReceive(0, Result.critical, target));
				}
				else if (c.stance.behind(self)) {
					c.write(self, formattedReceive(0, Result.strong, target));
				}
				else {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				if (shamed) {
					c.write(self, "The shame of having your ass violated by " + self.name() + " has destroyed your "
							+ "confidence.");
				}
			}
			else if (self.human()) {
				if (c.stance.penetration(self)) {
					c.write(self, formattedDeal(0, Result.special, target));
				}
				else if (c.stance.dom(target)) {
					c.write(self, formattedDeal(0, Result.critical, target));
				}
				else if (c.stance.behind(self)) {
					c.write(self, formattedDeal(0, Result.strong, target));
				}
				else {
					c.write(self, formattedDeal(0, Result.normal, target));
				}
				if (shamed) {
					c.write(self, target.possessive(true) + " face flushes in shame at the foreign object "
							+ "invading " + target.possessive(false) + " ass.");
				}
			}
			target.pleasure(10 + Global.random(10), Anatomy.ass, c);
			target.pain(3 + Global.random(5), Anatomy.ass, c);
			target.emote(Emotion.nervous, 10);
			target.emote(Emotion.desperate, 10);
			self.emote(Emotion.confident, 15);
			self.emote(Emotion.dominant, 25);
		}
		else {
			if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
			else {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new TailPeg(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		switch (modifier) {
			case critical:
				return "With your superior position, %target% is helpless to resist as the tip of your tail teases %himher% "
						+ "anus. You decide to go even further and thrust it several inches into the tight hole.";
			case miss:
				return "You try to sneak your tail into %target%'s back door, but %heshe% catches it and pushes it away.";
			case normal:
				return "You close in on %target%, and your tail curls around outside %hisher% peripheral vision. Before "
						+ "%heshe% realizes what's happening, you thrust it right up %hisher% bum.";
			case special:
				return "As your cock fills %target%'s pussy, your prehensile tail snakes between %hisher% butt cheeks "
						+ "and thrusts into %hisher% ass.";
			case strong:
				return "Holding %target% from behind, you have great access to %hisher% bare ass. You thrust your tail "
						+ "inside, making %himher% let out a startled yelp.";
			default:
				return "<<This should not be displayed, please inform The Silver Bard: TailPeg-deal>>";
		}
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		switch (modifier) {
			case critical:
				return "Smiling down on you, %self% spreads your legs and tickles your butt with %hisher% tail. You "
						+ "notice how the tail itself was slick and wet as it slowly pushes through your anus, spreading "
						+ "your cheeks apart. %self% pumps it in and out a for a few times before taking it out again.";
			case miss:
				return "%self% tries to peg you with %hisher% tail but you manage to push your butt cheeks together in "
						+ "time to keep it out.";
			case normal:
				return "%self% suddenly moves very close to you. You expect an attack from the front and try to move "
						+ "back, but end up shoving %hisher% tail right up your ass.";
			case special:
				return "%self% smirks and wiggles %hisher% tail behind %hisher% back. You briefly look at it and then "
						+ "see the appendage move behind you. You try to keep it out by clenching your butt together, "
						+ "but a squeeze of %self%'s vagina breaks your concentration, so the tail slides up your ass "
						+ "and you almost lose it as your cock and ass are stimulated so thoroughly at the same time.";
			case strong:
				return "%self% hugs you from behind and rubs %hisher% chest against your back. Distracted by that, %heshe% "
						+ "manages to push %hisher% tail between your ass cheeks and starts tickling your prostrate "
						+ "with the tip.";
			default:
				return "<<This should not be displayed, please inform The Silver Bard: TailPeg-receive>>";
		}
	}
}
