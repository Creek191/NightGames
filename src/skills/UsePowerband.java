package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import items.Consumable;
import status.PrismaticStance;

/**
 * Defines a skill where a character uses their powerband to enhance their power
 */
public class UsePowerband extends UseItem {
	public UsePowerband(Character self) {
		super(Consumable.powerband, self);
		addTag(SkillTag.CONSUMABLE);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(formattedReceive(0, Result.normal, target));
		}
		self.consume(Consumable.powerband, 1);
		self.add(new PrismaticStance(self), c);
	}

	@Override
	public Skill copy(Character user) {
		return new UsePowerband(user);
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You put on the Power Band and feel tremendous power flow through you.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% puts on a strange headband. The atmosphere immediately changes. You can practically feel the "
				+ "energy flowing out of %himher%.";
	}
}
