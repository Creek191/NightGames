package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Mount;
import stance.ReverseMount;
import stance.Stance;

/**
 * Defines a skill where a character stumbles, pulling their opponent on top of them
 */
public class Stumble extends Skill {
	public Stumble(Character self) {
		super("Stumble", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Submissive) >= 9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct() && c.stance.en == Stance.neutral;
	}

	@Override
	public String describe() {
		return "An accidental pervert classic";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (Global.random(2) == 0) {
			c.stance = new Mount(target, self);
		}
		else {
			c.stance = new ReverseMount(target, self);
		}
		if (self.human()) {
			c.write(formattedDeal(0, Result.normal, target));
		}
		else {
			c.write(formattedReceive(0, Result.normal, target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Stumble(user);
	}

	@Override
	public Tactics type() {
		return Tactics.negative;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You slip and fall to the ground, pulling %target% awkwardly on top of you.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% stumbles and falls, grabbing you to catch %himher%self. Unfortunately, you can't keep your balance "
				+ "and you fall on top of %himher%. Maybe that's not so unfortunate.";
	}
}
