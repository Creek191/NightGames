package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Horny;
import status.Stsflag;

/**
 * Defines a skill where a character uses hypnosis to fuel their opponent's dirties fantasies
 */
public class EnflameLust extends Skill {
	public EnflameLust(Character self) {
		super("Enflame Lust", self);
		addTag(Attribute.Hypnosis);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Hypnosis) >= 2;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.mobile(self)
				&& !c.stance.behind(self)
				&& !c.stance.sub(self)
				&& !c.stance.behind(target)
				&& (target.is(Stsflag.charmed) || target.is(Stsflag.enthralled));
	}

	@Override
	public String describe() {
		return "Use hypnotic suggestion to fuel your opponent's dirtiest fantasies.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (target.is(Stsflag.horny)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.strong, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.strong, target));
			}
			var status = target.getStatus(Stsflag.horny);
			if (status != null) {
				target.add(new Horny(target, target.getStatusMagnitude("Horny") * 2, 6), c);
			}
			else {
				target.add(new Horny(target, 4, 6), c);
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			target.add(new Horny(target, 4, 6), c);
		}
		target.emote(Emotion.horny, 30);
	}

	@Override
	public Skill copy(Character user) {
		return new EnflameLust(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.strong) {
			return "Your hypnosis builds on %target%'s existing fantasies to shape %hisher% perception of this fight into "
					+ "%hisher% ideal sexual scenario.";
		}
		else {
			return "You use a suggestion to plant a dirty thought in %target%'s head. The vague fantasy will be shaped by "
					+ "%hisher% secret desires and make %himher% hornier than usual.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.strong) {
			return "%self% elaborates in glorious, erotic detail on the fantasy that was running through your head. You "
					+ "have no idea how %heshe% knew what you were thinking about, but %hisher% story is so intensely "
					+ "sexual that you have trouble thinking straight.";
		}
		else {
			return "In a brief lull in the fight, %self% shares the story of %hisher% favorite sexual experience with you. "
					+ "Thinking back, you have trouble remembering all the details, but you do know it was hot enough "
					+ "that you can't help fantasizing about it.";
		}
	}
}
