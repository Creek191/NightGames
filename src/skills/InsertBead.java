package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import items.Toy;
import status.Beaded;

/**
 * Defines a skill where a character inserts an anal bead into their opponent's ass
 */
public class InsertBead extends Skill {
    public InsertBead(Character self) {
        super("Insert Anal Bead", self);
        addTag(SkillTag.TOY);
    }

    @Override
    public boolean requirements(Character user) {
        return true;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return !self.human()
                && self.canAct()
                && self.has(Toy.AnalBeads)
                && target.pantsless()
                && c.stance.reachBottom(self);
    }

    @Override
    public String describe() {
        return "Insert an anal bead into your opponent's ass";
    }

    @Override
    public void resolve(Combat c, Character target) {
        if (c.attackRoll(this, self, target)) {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.normal, target));
            }
            else if (target.human()) {
                c.write(self, formattedReceive(0, Result.normal, target));
            }
            target.add(new Beaded(target, 1), c);
        }
        else {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.miss, target));
            }
            else if (target.human()) {
                c.write(self, formattedReceive(0, Result.miss, target));
            }
        }
    }

    @Override
    public Skill copy(Character user) {
        return new InsertBead(user);
    }

    @Override
    public Tactics type() {
        return Tactics.pleasure;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if (modifier == Result.miss) {
            return "You try to insert an anal bead into %target%'s ass, but %heshe%'s struggling too much.";
        }
        else {
            return "You manage to push an anal bead into %target%'s butt. You could pull it out now, but it would "
                    + "make a bigger impact if you can get some more in there.";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if (modifier == Result.miss) {
            return "%self% tries to stick a bead up your ass, but you manage to get away.";
        }
        else {
            return "%self% shoves an anal bead up your ass before you can resist. As uncomfortable as it is, you're sure "
                    + "getting it pulled out will be worse.";
        }
    }
}
