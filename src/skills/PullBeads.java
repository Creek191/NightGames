package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Stsflag;

/**
 * Defines a skill where a character pulls anal beads out of their opponent's ass
 */
public class PullBeads extends Skill {
    public PullBeads(Character self) {
        super("Pull Out Beads", self);
        addTag(SkillTag.TOY);
    }

    @Override
    public boolean requirements(Character user) {
        return true;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canAct()
                && target.pantsless()
                && target.is(Stsflag.beads)
                && c.stance.reachBottom(self);
    }

    @Override
    public String describe() {
        return "Pull all the anal beads out of your opponent, dealing pleasure based on how many are inside";
    }

    @Override
    public void resolve(Combat c, Character target) {
        var beads = target.getStatus(Stsflag.beads).mag();
        var damage = (10 + (target.getEffective(Attribute.Perception) / 2) + (self.getEffective(Attribute.Science) / 2)) * beads;
        damage = self.bonusProficiency(Anatomy.toy, damage);
        if (self.human()) {
            c.write(self, formattedDeal(beads, Result.normal, target));
        }
        else if (target.human()) {
            c.write(self, formattedReceive(beads, Result.normal, target));
        }
        target.removeStatus(Stsflag.beads, c);
        target.pleasure(damage, Anatomy.ass, c);
    }

    @Override
    public Skill copy(Character user) {
        return new PullBeads(user);
    }

    @Override
    public Tactics type() {
        return Tactics.pleasure;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if (damage > 1) {
            return "You pull the string of " + damage + " beads out of %target%'s anus.";
        }
        else {
            return "You yank the bead out of %target%'s ass with an audible 'pop'.";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if (damage > 1) {
            return "%self% grabs the string of anal beads sticking out of your ass and yanks them all out at once. Your "
                    + "mind goes blank ass you're assaulted by the sensation of your asshole repeatedly getting forced "
                    + "open.";
        }
        else {
            return "%self% pulls the anal bead out of your ass, causing an indescribable sensation.";
        }
    }
}
