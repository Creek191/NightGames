package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.ID;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import items.Attachment;
import items.Flask;
import items.Toy;
import status.Oiled;
import status.Stsflag;

/**
 * Defines a skill where a character uses a dildo on their opponent
 */
public class UseDildo extends Skill {
	public UseDildo(Character self) {
		super(Toy.Dildo.getFullName(self), self);
		addTag(SkillTag.TOY);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& (self.has(Toy.Dildo) || self.has(Toy.Dildo2))
				&& target.hasPussy()
				&& target.pantsless()
				&& c.stance.reachBottom(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var block = 0;
		if (c.lastAction(target) != null
				&& c.lastAction(target).name.equals(Toy.Onahole.getFullName(target))) {
			block = 5;
		}
		if (c.attackRoll(this, self, target) && Global.random(20) >= block) {
			var level = 1;
			var damage = 6 + (target.getEffective(Attribute.Perception) / 2)
					+ (self.getEffective(Attribute.Science) / 2);
			if (self.has(Attachment.DildoSlimy)) {
				damage *= 2;
				level += 1;
			}
			if (self.has(Toy.Dildo2)) {

				damage += 5;
				level += 1;
			}
			if (self.has(Attachment.DildoLube) && self.has(Flask.Lubricant) && !target.is(Stsflag.oiled)) {
				if (self.human()) {
					c.write(self, formattedDeal(1, Result.upgrade, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(1, Result.upgrade, target));
				}
				self.consume(Flask.Lubricant, 1);
				target.add(new Oiled(target));
			}
			if (self.human()) {
				c.write(self, formattedDeal(level, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(level, Result.normal, target));
			}
			if (self.has(Attachment.DildoSlimy)) {
				if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.KAT) {
					c.offerImage("Dildo4.png", "Art by AimlessArt");
				}
			}
			else if (self.has(Attachment.DildoLube)) {
				if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.KAT) {
					c.offerImage("Dildo3.png", "Art by AimlessArt");
				}
			}
			else if (self.has(Toy.Dildo2)) {
				if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.KAT) {
					c.offerImage("Dildo2.png", "Art by AimlessArt");
				}
			}
			else {
				if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.KAT) {
					c.offerImage("Dildo1.png", "Art by AimlessArt");
				}
			}
			damage = self.bonusProficiency(Anatomy.toy, damage);
			if (self.has(Toy.Dildo2)) {
				target.pleasure(damage, Anatomy.genitals, c);
			}
			else {
				target.pleasure(damage, Anatomy.genitals, Result.finisher, c);
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(block, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(block, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new UseDildo(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (damage > 0) {
				return "You try to slip a dildo into %target%, but %heshe% catches it with %hisher% " + Toy.Onahole.getFullName(
						target) + ".";
			}
			else {
				return "You try to slip a dildo into %target%, but %heshe% blocks it.";
			}
		}
		else if (modifier == Result.upgrade) {
			return "As you rub the head of the dildo against %target%'s pussy, it shoots out a generous load of lubricant "
					+ "to coat %hisher% groin.";
		}
		else {
			switch (damage) {
				case 3:
					return "As the slimy dildo touches %target%'s nethers, it starts to react eagerly. It delves into "
							+ "%hisher% vagina and writhes around, exploring %hisher% intimate depths. %HeShe% trembles "
							+ "at the intense sensation and moans loudly.";
				case 2:
					switch (Global.random(2)) {
						case 1:
							return "You push the dildo into %target%. While %heshe%'s reeling, you crank up the sonic "
									+ "vibration, and %heshe% nearly falls to %hisher% knees from the sensation.";
						default:
							return "You touch the imperceptibly vibrating dildo to %target%'s love button and %heshe% "
									+ "jumps as if shocked. Before %heshe% can defend %himher%self, you slip it into "
									+ "%hisher% pussy. %HeShe% starts moaning in pleasure immediately.";
					}

				default:
					switch (Global.random(2)) {
						case 1:
							return "You quickly slip the tip of your dildo into %target%'s pussy. Before %heshe% can "
									+ "react, you push it in further, eliciting a pleasured moan from %hisher% lips.";
						default:
							return "You rub the dildo against %target%'s lower lips to lubricate it before you thrust "
									+ "it inside %himher%. %HeShe% can't help moaning a little as you pump the rubber "
									+ "toy in and out of %hisher% pussy.";
					}
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String describe() {
		return "Pleasure opponent with your dildo";
	}
}
