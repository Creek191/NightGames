package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Neutral;
import stance.Stance;
import status.Stsflag;

/**
 * Defines a skill where a character distracts their opponent by grinding on them, then performs a knee strike
 */
public class PenaltyKick extends Skill {
    public PenaltyKick(Character self) {
        super("Penalty Shot", self);
        this.accuracy = 3;
        this.speed = 2;
        addTag(Attribute.Footballer);
        addTag(SkillTag.PAINFUL);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer) >= 12;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canAct()
                && self.canSpend(10)
                && c.stance.en == Stance.behind
                && c.stance.sub(self);
    }

    @Override
    public String describe() {
        return "Distract your opponent by grinding on them and leave them open to a knee strike. More effective against Horny targets: 10 Mojo";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spendMojo(10);
        target.pleasure(3 + self.getEffective(Attribute.Seduction) / 2 + target.getEffective(Attribute.Perception),
                Anatomy.genitals,
                Result.finisher,
                c);
        var damage = Global.random(6) + self.getEffective(Attribute.Footballer);
        if (target.is(Stsflag.horny)) {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.strong, target));
            }
            else if (target.human()) {
                c.write(self, formattedReceive(0, Result.strong, target));
                if (Global.random(5) >= 3) {
                    c.write(self, self.bbLiner());
                }
            }
            damage *= 1.2;
            target.pain(damage, Anatomy.genitals, c);
            c.stance = new Neutral(self, target);
        }
        else if (c.attackRoll(this, self, target)) {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.normal, target));
            }
            else if (target.human()) {
                c.write(self, formattedReceive(0, Result.normal, target));
                if (Global.random(5) >= 3) {
                    c.write(self, self.bbLiner());
                }
            }
            target.pain(damage, Anatomy.genitals, c);
            c.stance = new Neutral(self, target);
        }
        else {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.miss, target));
            }
            else if (target.human()) {
                c.write(self, formattedReceive(0, Result.miss, target));
            }
        }
    }

    @Override
    public Skill copy(Character user) {
        return new PenaltyKick(user);
    }

    @Override
    public Tactics type() {
        return Tactics.damage;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if (modifier == Result.strong) {
            return "You grind your ass against %target%'s crotch to distract %himher%. %HeShe%'s so horny that %heshe% "
                    + "enthusiastically grinds back. When %heshe%'s distracted you kick backwards, catching %himher% "
                    + "completely off guard. You turn to face %himher% while %heshe%'s busy holding %hisher% groin.";
        }
        else if (modifier == Result.miss) {
            return "You grind your ass against %target%'s crotch to distract %himher%. Unfortunately %heshe% only tightens "
                    + "%hisher% grip on you.";
        }
        else {
            return "You grind your ass against %target%'s crotch to distract %himher%. You feel %hisher% grip weaken as "
                    + "%heshe% loses focus. When %heshe%'s distracted you kick backwards, catching %himher% completely "
                    + "off guard. You turn to face %himher% while %heshe%'s busy holding %hisher% groin.";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if (modifier == Result.strong) {
            return "%self% squirms in your grip, grinding %hisher% ass against your groin. In your lust-fogged state, "
                    + "you can't resist grinding back and rubbing your dick between her firm cheeks. %HeShe% suddenly "
                    + "slams %hisher% heel into your unprotected balls. The pain is devastating, and you release your "
                    + "hold on %himher% to hold your sore bits.";
        }
        else if (modifier == Result.miss) {
            return "%self% squirms in your grip, grinding %hisher% ass against your groin. It feels good, but you suspect "
                    + "this is just a diversion. When %heshe% suddenly tries to kick at you, you're ready to block it with "
                    + "your leg.";
        }
        else {
            return "%self% squirms in your grip, grinding %hisher% ass against your groin. Despite your better judgement, "
                    + "you let yourself indulge in the feeling of %hisher% generous ass rubbing your cock for a long "
                    + "moment. %HeShe% suddenly slams %hisher% heel into your unprotected balls. The pain is devastating, "
                    + "and you release your hold on %himher% to hold your sore bits.";
        }
    }
}
