package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Horny;
import status.Sore;

/**
 * Defines a skill where a character spreads their legs to tempt their opponent
 */
public class SpreadThighs extends Skill {
    public SpreadThighs(Character self) {
        super("Spread Thighs", self);
        this.speed = 9;
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Footballer) >= 6;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c)
                && self.pantsless();
    }

    @Override
    public String describe() {
        return "Spread your legs to tempt your opponent. Can cause Horniness, but makes you more vulnerable to low blows.";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.add(new Sore(self, 2, Anatomy.genitals, 2f));
        target.tempt(self.getEffective(Attribute.Footballer) / 2);
        if (c.effectRoll(this, self, target, self.getEffective(Attribute.Footballer))) {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.normal, target));
            }
            else {
                c.write(self, formattedReceive(0, Result.normal, target));
            }
            var duration = 1;
            if (self.getPure(Attribute.Footballer) >= 12) {
                duration++;
            }
            target.add(new Horny(target, self.getEffective(Attribute.Footballer) / 2, duration), c);
        }
        else {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.miss, target));
            }
            else {
                c.write(self, formattedReceive(0, Result.miss, target));
            }
        }
    }

    @Override
    public Skill copy(Character user) {
        return new SpreadThighs(user);
    }

    @Override
    public Tactics type() {
        return Tactics.status;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if (modifier == Result.miss) {
            return "You spread your legs and flaunt your package with as much flair as you can manage. %target% grins "
                    + "approvingly at the sight, but doesn't seem to be overcome with lust.";
        }
        else {
            return "You spread your legs and flaunt your package with as much flair as you can manage. %target% flushes "
                    + "and %hisher% eyes stay glued to your wagging member.";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if (modifier == Result.miss) {
            return "%self% spreads %hisher% thighs with a seductive smile. %HeShe% runs %hisher% hands down %hisher% "
                    + "belly and gives %hisher% crotch a sensual rub. You do your best to ignore the temptation. If you "
                    + "win, you'll claim that prize soon anyway.";
        }
        else {
            return "%self% spreads %hisher% thighs with a seductive smile. %HeShe% runs %hisher% hands down %hisher% "
                    + "belly and gives %hisher% crotch a sensual rub. You catch yourself drooling at the sight. Your "
                    + "straining cock insists that you fill %himher% as soon as possible.";
        }
    }
}
