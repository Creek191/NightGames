package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import items.Attachment;
import items.Flask;
import items.Toy;
import status.Stsflag;

/**
 * Defines a skill where a character uses their Sexcalibur to pleasure their opponent
 */
public class UseExcalibur extends Skill {
	public UseExcalibur(Character self) {
		super(Toy.Excalibur.getName(), self);
		addTag(SkillTag.TOY);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.has(Toy.Excalibur)
				&& target.hasPussy()
				&& (target.pantsless() || (self.has(Attachment.ExcaliburScience) && self.has(Flask.DisSol)))
				&& c.stance.reachBottom(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target) || self.has(Attachment.ExcaliburNinjutsu)) {
			var damage = 5 + (target.getEffective(Attribute.Perception) / 2);
			var level = 1;
			if (self.has(Attachment.Excalibur2)) {
				level++;
				damage += 1 + Global.random(6);
			}
			if (self.has(Attachment.Excalibur3)) {
				level++;
				damage *= 2;
			}
			if (self.has(Attachment.Excalibur4)) {
				level++;
				damage *= 1.5;
			}
			if (self.has(Attachment.Excalibur5)) {
				level++;
				damage *= 2;
			}
			damage += (self.getEffective(Attribute.Science) / 2);
			if (self.has(Attachment.ExcaliburNinjutsu)) {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.upgrade, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.upgrade, target));
				}
			}
			if (self.has(Attachment.ExcaliburScience) && !target.pantsless() && self.has(Flask.DisSol)) {
				self.consume(Flask.DisSol, 1);

				if (self.human()) {
					c.write(self, formattedDeal(1, Result.upgrade, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(1, Result.upgrade, target));
				}
				target.shred(Character.OUTFITBOTTOM);
			}
			if (self.human()) {
				c.write(self, formattedDeal(level, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(level, Result.normal, target));
			}
			if (self.has(Attachment.ExcaliburFetish)) {
				damage *= 1 + self.getArousal().percent() / 100;
				if (self.human()) {
					c.write(self, formattedDeal(2, Result.upgrade, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(2, Result.upgrade, target));
				}
			}
			if (self.has(Attachment.ExcaliburDark) && target.is(Stsflag.horny)) {
				damage *= 1 + (target.getStatusMagnitude("Horny") / 10);
				if (self.human()) {
					c.write(self, formattedDeal(3, Result.upgrade, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(3, Result.upgrade, target));
				}
			}
			if (self.has(Attachment.ExcaliburAnimism) && self.is(Stsflag.feral)) {
				damage *= 1.5;
				if (self.is(Stsflag.beastform)) {
					damage *= 1.5;
				}
				if (self.human()) {
					c.write(self, formattedDeal(4, Result.upgrade, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(4, Result.upgrade, target));
				}
			}
			if (self.has(Attachment.ExcaliburArcane)) {
				if (self.human()) {
					c.write(self, formattedDeal(5, Result.upgrade, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(5, Result.upgrade, target));
				}
				target.spendMojo(10);
				self.buildMojo(30);
			}
			if (self.has(Attachment.ExcaliburKi)) {
				if (self.human()) {
					c.write(self, formattedDeal(6, Result.upgrade, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(6, Result.upgrade, target));
				}
				self.heal(10, c);
			}
			damage = self.bonusProficiency(Anatomy.toy, damage);
			target.pleasure(damage, Anatomy.genitals, c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new UseExcalibur(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You thrust Sexcalibur at %target%'s groin, but %heshe% blocks it.";
		}
		else if (modifier == Result.upgrade) {
			switch (damage) {
				case 0:
					//Concealed Holster
					return "In one quick motion, you draw Sexcalibur from your concealed holster and attack, catching "
							+ "%target% by surprise";
				case 1:
					//Dissolver
					return "On contact with %hisher% " + target.bottom.peek().getName() + ", the head of your Sexcalibur "
							+ "sprays enough dissolving solution to quickly melt through the garment.";
				case 2:
					//Tentacles
					return "Small tentacles uncoil from the shaft of your Sexcalibur and attack %hisher% sensitive skin.";
				case 3:
					//Lust Curse
					return "You feel the cursed sex toy respond to %hisher% horny thoughts and grow more powerful.";
				case 4:
					//Furry
					return "The soft fur on the vibrating head grows into a bushy tail that caresses %hisher% privates, "
							+ "guided by the animal spirit possessing it.";
				case 5:
					//Enchantment
					return "The enchanted rod glows slightly as it pulls %target%'s mana into you.";
				case 6:
					//Ki
					return "Simply wielding the two improves your Ki flow and restores your stamina.";
				default:
					return "";
			}
		}
		else {
			switch (damage) {
				case 2:
					//Grand
					return "You touch your Grand Sexcalibur to %target%'s bare slit, and %heshe% whimpers with pleasure "
							+ "at the intense vibration.";
				case 3:
					//Masterwork
					return "You attack %target%'s pussy with your Masterwork Sexcalibur, dealing intense pleasure with "
							+ "the finely crafted sex toy. %HeShe% lets out an audible moan at the sensation.";
				case 4:
					//Legendary
					return "You thrust the Legendary Sexcaibur between %target%'s nethers, dealing a critical hit of "
							+ "pleasure to %himher% pussy and clit.";
				case 5:
					//Perfect
					return "You touch the vibrating head of your Perfect Sexcalibur to %target%'s vulva. %HeShe% gasps "
							+ "at the unfathomable pleasure and %hisher% knees buckle.";
				default:
					return "You press Sexcalibur's vibrating head against %target%'s clit, causing %himher% to flinch "
							+ "and let out a yelp of pleasure.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String describe() {
		return String.format("Pleasure opponent with your %s", Toy.Excalibur.getFullName(self));
	}
}
