package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character uses all of their stamina to deliver a powerful pleasure attack
 */
public class PleasureBomb extends Skill {
	public PleasureBomb(Character self) {
		super("Pleasure Bomb", self);
		this.accuracy = 1;
		this.speed = 1;
		addTag(Attribute.Ki);
		addTag(SkillTag.ULTIMATE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ki) >= 30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.pantsless()
				&& c.stance.reachBottom(self)
				&& c.stance.dom(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public String describe() {
		return "Consumes all your stamina to deliver a slow, but powerful pleasure attack. More effective against stunned opponents.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			var damage = 20 + self.getEffective(Attribute.Seduction) + self.getEffective(Attribute.Ki) + Global.random(
					target.getEffective(
							Attribute.Perception));
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			self.getStamina().empty();
			if (!target.canAct()) {
				damage *= 2;
			}
			damage = self.bonusProficiency(Anatomy.fingers, damage);
			target.pleasure(damage, Anatomy.genitals, c);
			target.emote(Emotion.horny, 20);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
			target.emote(Emotion.nervous, 10);
		}
	}

	@Override
	public Skill copy(Character user) {
		return new PleasureBomb(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You charge up your hand with all of your Ki and aim for %target%'s groin. At the last second %heshe% "
					+ "jerks %hisher% hips out of the way and the energy disperses harmlessly, leaving you drained.";
		}
		else if (target.hasDick()) {
			return "You charge up your hand with all of your Ki and aim for %target%'s groin. You grab %hisher% penis "
					+ "and pour your energy into it while stroking rapidly. %HeShe% can't help moaning as you overwhelm "
					+ "%himher% with intense pleasure.";
		}
		else {
			return "You charge up your hand with all of your Ki and aim for %target%'s groin. You focus the massive amount "
					+ "of energy into your fingertips as you rub %hisher% clit directly. %HeShe% can't help moaning as "
					+ "you overwhelm %himher% with intense pleasure.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% raises %hisher% hand and it begins to glow with tremendous power. %HeShe% grabs for your dick, "
					+ "but you manage to dodge out of the way at the last moment.";
		}
		else {
			return "%self% raises %hisher% hand and it begins to glow with tremendous power. %HeShe% grabs your cock, "
					+ "pumping energetically. Your hips buck involuntarily as you're suddenly assaulted with intense "
					+ "pleasure. The sensation is so overwhelming, it's almost painful.";
		}
	}
}
