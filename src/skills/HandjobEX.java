package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.Stance;
import status.ProfMod;
import status.ProfessionalMomentum;

/**
 * Defines a skill where a character gives their opponent a sensual handjob
 */
public class HandjobEX extends Skill {
	public HandjobEX(Character self) {
		super("Sensual Handjob", self);
		addTag(Attribute.Seduction);
		addTag(Result.touch);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(20)
				&& target.hasDick()
				&& (target.pantsless() || (self.has(Trait.dexterous) && target.bottom.size() <= 1))
				&& c.stance.reachBottom(self)
				&& (!c.stance.penetration(target) || c.stance.en == Stance.anal);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = 0;
		var style = Result.seductive;
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning) && self.getEffective(Attribute.Power) > self.getEffective(
				Attribute.Seduction)) {
			style = Result.powerful;
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Seduction)) {
			style = Result.clever;
		}
		self.spendMojo(20);
		if (c.attackRoll(this, self, target)) {
			if (style == Result.powerful) {
				damage = 4 + Global.random(self.getEffective(Attribute.Power) + self.getEffective(Attribute.Professional)) + target.getEffective(
						Attribute.Perception);
			}
			else if (style == Result.clever) {
				damage = 4 + Global.random(self.getEffective(Attribute.Cunning) + self.getEffective(Attribute.Professional)) + target.getEffective(
						Attribute.Perception);
			}
			else {
				damage = 4 + Global.random(self.getEffective(Attribute.Seduction) + self.getEffective(Attribute.Professional)) + target.getEffective(
						Attribute.Perception);
				damage *= 1.3f;
			}

			if (self.getPure(Attribute.Professional) >= 3) {
				if (self.has(Trait.sexuallyflexible)) {
					self.add(new ProfessionalMomentum(self, self.getEffective(Attribute.Professional) * 5), c);
				}
				else {
					self.add(new ProfMod("Dexterous Momentum",
							self,
							Anatomy.fingers,
							self.getEffective(Attribute.Professional) * 5), c);
				}
			}
			if (self.human()) {
				c.write(self, formattedDeal(damage, style, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, style, target));
				c.offerImage("Handjob.jpg", "Art by AimlessArt");
				c.offerImage("Handjob2.jpg", "Art by AimlessArt");
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.JEWEL) {
					c.offerImage("Jewel Handjob.png", "Art by Fujin Hitokiri");
				}
			}
			damage = self.bonusProficiency(Anatomy.fingers, damage);
			target.pleasure(damage, Anatomy.genitals, Result.finisher, c);
			if (self.has(Trait.roughhandling)) {
				target.weaken(damage / 2, c);
				if (self.human()) {
					c.write(self, formattedDeal(damage, Result.unique, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(damage, Result.unique, target));
				}
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getEffective(Attribute.Seduction) >= 8;
	}

	@Override
	public Skill copy(Character user) {
		return new HandjobEX(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "You opportunistically give %hisher% balls some rough treatment.";
		}
		if (modifier == Result.miss) {
			return "You reach for %target%'s dick but miss.";
		}
		if (modifier == Result.powerful) {
			return "You forcefully grab %target%'s cock and jerk it with all your power. %HeShe% moans loudly as the "
					+ "intense sensation hits %himher%";
		}
		else if (modifier == Result.clever) {
			return "You gently tease %target%'s dick with your fingertips. Your featherlight strokes probably don't "
					+ "provide much actual stimulation, but you effectively stoke %hisher% arousal by making %himher% "
					+ "yearn for more.";
		}
		else {
			return "You skillfully service %target%'s penis with slow, tender strokes. This is more complicated than your "
					+ "basic jerk off technique, but you take inspiration from some of the more seductive handjobs you've "
					+ "received.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "%HeShe% roughly handles your balls, sapping some of your strength.";
		}
		if (modifier == Result.miss) {
			return "%self% grabs for your dick and misses.";
		}
		if (modifier == Result.powerful) {
			return "%self% grabs your cock and roughly jerks you off. %HisHer% forceful handjob is almost painful, but "
					+ "it's also surprisingly effective.";
		}
		else if (modifier == Result.clever) {
			return "%self% plays with your dick, using light teasing touches. %HisHer% caress is pretty effective on its "
					+ "own, but the teasing is so frustrating that you involuntary thrust against %hisher% hand, arousing "
					+ "yourself further.";
		}
		else {
			return "%self% takes hold of your manhood with a sensual grip and begins to play you like an instrument. "
					+ "%HisHer% talented fingers seem to know all the most sensitive spots on your penis and balls. It's "
					+ "so good, you almost forget to fight back.";
		}
	}

	@Override
	public String toString() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning) && self.getEffective(Attribute.Power) > self.getEffective(
				Attribute.Seduction)) {
			return "Rough Handjob";
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Seduction)) {
			return "Teasing Handjob";
		}
		return name;
	}

	@Override
	public String describe() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning) && self.getEffective(Attribute.Power) > self.getEffective(
				Attribute.Seduction)) {
			return "Intense handjob using Power: 20 Mojo";
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Seduction)) {
			return "Careful handjob using Cunning: 20 Mojo";
		}
		else {
			return "Mojo boosted erotic handjob: 20 Mojo";
		}
	}
}
