package skills;

import characters.Character;
import combat.Combat;
import combat.Result;

/**
 * Defines a command where an enthralled character is forced to strip their master
 */
public class CommandStripPlayer extends PlayerCommand {
	public CommandStripPlayer(Character self) {
		super("Force Strip Player", self);
		addTag(SkillTag.COMMAND);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return super.usable(c, target) && !self.nude();
	}

	@Override
	public String describe() {
		return "Make your thrall undress you.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.undress(c);
		c.write(self, formattedDeal(0, Result.normal, target));
	}

	@Override
	public Skill copy(Character user) {
		return new CommandStripPlayer(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		return "With an elated gleam in %hisher% eyes, %target% moves %hisher% hands with nigh-inhuman dexterity, "
				+ "stripping all of your clothes in just a second.";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The Silver Bard: CommandStripPlayer-receive>>";
	}
}
