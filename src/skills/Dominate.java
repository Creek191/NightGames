package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.StandingOver;
import status.Stsflag;

/**
 * Defines a skill where a character uses their domination to make their opponent lie down
 */
public class Dominate extends Skill {
	public Dominate(Character self) {
		super("Dominate", self);
		addTag(Attribute.Dark);
		addTag(SkillTag.DOM);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark) >= 9;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !c.stance.sub(self)
				&& !c.stance.prone(self)
				&& !c.stance.prone(target);
	}

	@Override
	public String describe() {
		return "Overwhelm your opponent to force her to lie down: 10 Arousal";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendArousal(10);
		if (self.human()) {
			if (target.is(Stsflag.enthralled)) {
				c.write(self, formattedDeal(0, Result.strong, target));
			}
			else {
				c.write(self, formattedDeal(0, Result.normal, target));
			}

			if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Cassie")) {
				c.offerImage("Dominating.jpg", "Art by AimlessArt");
			}
		}
		else if (target.human()) {
			if (target.is(Stsflag.enthralled)) {
				c.write(self, formattedReceive(0, Result.strong, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			if (!Global.checkFlag(Flag.exactimages) || self.name().startsWith("Angel")) {
				c.offerImage("Dominated.jpg", "Art by AimlessArt");
			}
		}
		c.stance = new StandingOver(self, target);
		self.emote(Emotion.dominant, 20);
		target.emote(Emotion.nervous, 20);
	}

	@Override
	public Skill copy(Character user) {
		return new Dominate(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.strong) {
			return "You take a deep breath, gathering dark energy into your lungs. You expend the power to command "
					+ "%target% to submit. The demonic command renders %himher% unable to resist and %heshe% drops to the "
					+ "floor, spreading %hisher% legs open to you. As you step closer, %heshe% thrusts %hisher% hips off "
					+ "the floor, inviting you to take %himher%.";
		}
		return "You take a deep breath, gathering dark energy into your lungs. You expend the power to command %target% "
				+ "to submit. The demonic command renders %himher% unable to resist and %heshe% drops to the floor, "
				+ "spreading %hisher% legs open to you. As you approach, %heshe% comes to %hisher% senses and quickly "
				+ "closes %hisher% legs. Looks like %hisher% will is still intact.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.strong) {
			return "%self% forcefully orders you to \"Kneel!\" Your body complies without waiting for your brain and you "
					+ "drop to your knees in front of %himher%. %HeShe% smiles and pushes you onto your back. You present "
					+ "yourself as best you can, eager to please your master.";
		}
		return "%self% forcefully orders you to \"Kneel!\" Your body complies without waiting for your brain and you "
				+ "drop to your knees in front of %himher%. %HeShe% smiles and pushes you onto your back. By the time you "
				+ "break free of %hisher% suggestion, you're flat on the floor with %hisher% foot planted on your chest.";
	}
}
