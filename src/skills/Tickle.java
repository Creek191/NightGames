package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import items.Attachment;
import items.Toy;
import status.Horny;
import status.Hypersensitive;

/**
 * Defines a skill where a character tickles their opponent
 */
public class Tickle extends Skill {
	public Tickle(Character self) {
		super("Tickle", self);
		this.speed = 7;
		addTag(SkillTag.TICKLE);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& (c.stance.mobile(self) || c.stance.dom(self))
				&& (c.stance.reachTop(self) || c.stance.reachBottom(self));
	}

	@Override
	public void resolve(Combat c, Character target) {
		var mod = Result.normal;
		if (c.attackRoll(this, self, target)) {
			var level = 0;
			float tempt = Math.max(2
							+ Global.random(3 + target.getEffective(Attribute.Perception))
							- (target.top.size() + target.bottom.size())
					, 1);
			float weaken = Math.max(Global.random(3
							+ target.getEffective(Attribute.Perception))
							- (target.top.size() + target.bottom.size())
					, 1);
			if (target.has(Trait.ticklish)) {
				tempt = Math.round(1.3 * tempt);
				weaken += Math.round(1.3 * tempt);
			}
			if (self.has(Trait.ticklemonster) && target.nude()) {
				mod = Result.special;
				tempt *= 1.5;
				weaken *= 1.5;
			}
			else if (hasTickler() && Global.random(2) == 1 && (!self.human() || c.isAllowed(SkillTag.TOY, self))) {
				level++;
				if (self.has(Attachment.TicklerFluffy)) {
					level++;
					tempt *= 1.5;
					weaken *= 1.5;
				}
				if (target.pantsless() && c.stance.reachBottom(self)) {
					mod = Result.strong;
					tempt *= 1.3;
					weaken *= 1.3;
				}
				else if (target.topless() && c.stance.reachTop(self)) {
					mod = Result.item;
					tempt *= 1.2;
					weaken *= 1.2;
				}
				else {
					mod = Result.weak;
					tempt *= 1.1;
					weaken *= 1.1;
				}
			}

			if (self.human()) {
				c.write(self, formattedDeal(level, mod, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(level, mod, target));
			}
			if (self.has(Toy.Tickler2) && Global.random(2) == 1 && self.canSpend(10) && (!self.human() || c.isAllowed(
					SkillTag.TOY,
					self))) {
				self.spendMojo(10);
				target.add(new Hypersensitive(target), c);
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.critical, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.critical, target));
				}
			}
			if (self.has(Attachment.TicklerPheromones) && Global.random(4) == 1 && (!self.human() || c.isAllowed(
					SkillTag.TOY,
					self))) {
				target.add(new Horny(target, 6, 2), c);
				if (self.human()) {
					c.write(self, formattedDeal(1, Result.critical, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(1, Result.critical, target));
				}
			}
			if (mod == Result.special) {
				target.tempt(Math.round(tempt), self.bonusTemptation(), c);
				target.pleasure(1, Anatomy.genitals, c);
				target.weaken(Math.round(weaken), c);
				self.buildMojo(15);
			}
			else {
				target.tempt(Math.round(tempt), self.bonusTemptation(), Result.foreplay, c);
				target.weaken(Math.round(weaken), c);
				self.buildMojo(10);
			}
			if (self.human()) {
				if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.MARA) {
					c.offerImage("Tickle.jpg", "Art by AimlessArt");
				}
			}
		}
		else {
			mod = Result.miss;
			if (self.human()) {
				c.write(self, formattedDeal(0, mod, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, mod, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new Tickle(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You try to tickle %target%, but %heshe% squirms away.";
		}
		else if (modifier == Result.special) {
			return "You work your fingers across %target%'s most ticklish and most erogenous zones until %heshe%'s "
					+ "writhing in pleasure and can't even form coherent words.";
		}
		else if (modifier == Result.critical) {
			switch (damage) {
				case 1:
					return "%HeShe% looks confused as %hisher% face gradually flushes. The pheromones leaking from your "
							+ "tickler must be affecting %himher%.";
				default:
					return "You brush your tickler over %target%'s body, causing %himher% to shiver and retreat. When "
							+ "you tickle %himher% again, %heshe% yelps and almost falls down. It seems like your special "
							+ "feathers made %himher% more sensitive than usual.";
			}
		}
		else if (modifier == Result.strong) {
			switch (damage) {
				case 2:
					return "You attack %target%'s sensitive vulva with your fluffy tickler. %HeShe% shivers and moans at "
							+ "the supernaturally soft sensation.";
				default:
					return "You run your tickler across %target%'s sensitive thighs and pussy. %HeShe% can't help but "
							+ "let out a quiet whimper of pleasure.";
			}
		}
		else if (modifier == Result.item) {
			switch (damage) {
				case 2:
					return "You tease %target%'s bare breasts with your fluffy tickler, causing %himher% to whimper "
							+ "quietly.";
				default:
					return "You tease %target%'s naked upper body with your feather tickler, paying close attention to "
							+ "%hisher% nipples.";
			}
		}
		else if (modifier == Result.weak) {
			switch (damage) {
				case 2:
					return "You use your tickler to tease %target%'s sensitive ears and neck.";
				default:
					return "You catch %target% off guard by tickling %hisher% neck and ears.";
			}
		}
		else {
			switch (Global.random(2)) {
				case 1:
					return "You give %target% a playful smile before launching an all-out tickle attack.";
				default:
					return "You tickle %target%'s sides as %heshe% giggles and squirms.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to tickle you, but fails to find a sensitive spot.";
		}
		else if (modifier == Result.special) {
			return "%self% tickles your nude body mercilessly, gradually working %hisher% way to your dick and balls. As "
					+ "%hisher% fingers start tormenting your privates, you struggle to clear your head enough to keep "
					+ "from ejaculating immediately.";
		}
		else if (modifier == Result.critical) {
			switch (damage) {
				case 1:
					return "You gradually feel a soft heat spread from where you were tickled. You must be affected by "
							+ "an aphrodisiac.";
				default:
					return "After %heshe% stops, you feel an unnatural sensitivity where %heshe% touched you.";
			}
		}
		else if (modifier == Result.strong) {
			switch (damage) {
				case 2:
					return "%self% rubs %hisher% mystically soft tickler over your sensitive dick and balls.";
				default:
					return "%self% brushes %hisher% tickler over your balls and teases the sensitive head of your penis.";
			}
		}
		else if (modifier == Result.item) {
			switch (damage) {
				case 2:
					return "%self% teases your bare upper body with %hisher% fluffy tickler.";
				default:
					return "%self% runs %hisher% feather tickler across your nipples and abs.";
			}
		}
		else if (modifier == Result.weak) {
			switch (damage) {
				case 2:
					return "%self% attacks your exposed skin with a fluffy tickler.";
				default:
					return "%self% pulls out a feather tickler and teases any exposed skin %heshe% can reach.";
			}
		}
		else {
			return "%self% suddenly springs toward you and tickles you relentlessly until you can barely breathe.";
		}
	}

	@Override
	public String describe() {
		return "Tickles opponent, weakening and arousing her. More effective if she's nude";
	}

	private boolean hasTickler() {
		return self.has(Toy.Tickler) || self.has(Toy.Tickler2);
	}
}
