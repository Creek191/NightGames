package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;

/**
 * Defines a skill where a character unleashes a wave of pleasure
 */
public class EcstasyWave extends Skill {
    public EcstasyWave(Character user) {
        super("Ecstasy Wave", user);
        addTag(Attribute.Unknowable);
    }

    @Override
    public boolean requirements(Character user) {
        return user.getPure(Attribute.Unknowable) >= 9;
    }

    @Override
    public boolean usable(Combat c, Character target) {
        return self.canActNormally(c) && self.canSpend(Pool.ENIGMA, 4);
    }

    @Override
    public String describe() {
        return "Unleash a massive blast of pleasure";
    }

    @Override
    public void resolve(Combat c, Character target) {
        self.spend(Pool.ENIGMA, 4);
        if (target.getEffective(Attribute.Contender) >= 2) {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.miss, target));
            }
            else if (target.human()) {
                c.write(self, formattedReceive(0, Result.miss, target));
            }
        }
        else if (c.isWatching(ID.CASSIE)) {
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.defended, target));
            }
            else if (target.human()) {
                c.write(self, formattedReceive(0, Result.defended, target));
            }
        }
        else {
            var pow = 60 + self.getEffective(Attribute.Unknowable);
            if (self.human()) {
                c.write(self, formattedDeal(0, Result.normal, target));
            }
            else if (target.human()) {
                c.write(self, formattedReceive(0, Result.normal, target));
            }
            target.pleasure(pow, Anatomy.soul, c);
        }
    }

    @Override
    public Skill copy(Character user) {
        return new EcstasyWave(user);
    }

    @Override
    public Tactics type() {
        return Tactics.pleasure;
    }

    @Override
    public String deal(int damage, Result modifier, Character target) {
        if (modifier == Result.miss) {
            return "";
        }
        else if (modifier == Result.defended) {
            return "";
        }
        else {
            return "";
        }
    }

    @Override
    public String receive(int damage, Result modifier, Character target) {
        if (modifier == Result.miss) {
            return "%self% steps back and charges something in %hisher% hands. You catch a glimpse of a dense ball of "
                    + "particularly lascivious looking energy, before it explodes toward you. There is no time to dodge. "
                    + "You brace yourself for the attack by sheer force of will. The energy somehow washes over you, "
                    + "leaving you no worse for wear.";
        }
        else if (modifier == Result.defended) {
            return "";
        }
        else {
            return "%self% steps back and charges something in %hisher% hands. You catch a glimpse of a dense ball of "
                    + "particularly lascivious looking energy, before it explodes toward you. There is no time to dodge. "
                    + "The energy hits you and overloads your nervous system with intense pleasure coming from everywhere "
                    + "and nowhere.";
        }
    }
}
