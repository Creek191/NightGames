package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Stsflag;
import status.Tranquil;

/**
 * Defines a skill where a character uses their spirituality to calm down
 */
public class Tranquility extends Skill {
	public Tranquility(Character self) {
		super("Tranquility", self);
		addTag(Attribute.Spirituality);
		addTag(SkillTag.DEFENSIVE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Spirituality) >= 1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)
				&& self.canSpend(Pool.FOCUS, 3)
				&& !self.is(Stsflag.horny)
				&& !self.is(Stsflag.mindaffecting)
				&& !self.is(Stsflag.tranquil);
	}

	@Override
	public String describe() {
		return "Resist pleasure with spiritual tranquility: 3 Focus";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.FOCUS, 3);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new Tranquil(self), c);
	}

	@Override
	public Skill copy(Character user) {
		return new Tranquility(user);
	}

	@Override
	public Tactics type() {
		return Tactics.calming;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You let a wave of tranquility wash over you, dulling your senses.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% takes a deep breath. You can practically see an oasis of tranquility around %himher%.";
	}
}
