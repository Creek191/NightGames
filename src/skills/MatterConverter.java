package skills;

import characters.Attribute;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Result;
import status.Bound;

/**
 * Defines a skill where a character converts their clothes into a powerful binding for their opponent
 */
public class MatterConverter extends Skill {
	public MatterConverter(Character self) {
		super("Matter Converter", self);
		addTag(Attribute.Science);
		addTag(SkillTag.ULTIMATE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Science) >= 30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.nude()
				&& self.canSpend(Pool.BATTERY, 10)
				&& c.stance.reachTop(self)
				&& !c.stance.reachTop(target)
				&& c.stance.dom(self);
	}

	@Override
	public String describe() {
		return "Converts all your remaining clothing to a powerful binding material: 10 Battery";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.BATTERY, 10);
		var dc = self.top.size() + self.bottom.size();
		self.nudify();

		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.add(new Bound(target, 20 + dc * 10, "Super-web"), c);
	}

	@Override
	public Skill copy(Character user) {
		return new MatterConverter(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You vacuum up your own clothing into your multitool, where it is converted into Super-web material. "
				+ "Grabbing %target%'s wrists, you cover them in the webbing, binding them in place.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self%'s clothes quickly vanish off %hisher% body as they're sucked into %hisher% wrist device. Said device "
				+ "immediately spits a clump of sticky string that binds your wrists tightly in place. You pull against "
				+ "the tangled binding, but you can't get loose.";
	}
}
