package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.ID;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import status.Horny;

/**
 * Defines a skill where a character creates an aura of lust around themselves
 */
public class LustAura extends Skill {
	public LustAura(Character self) {
		super("Lust Aura", self);
		addTag(Attribute.Dark);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Dark) >= 3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(5)
				&& c.stance.mobile(self)
				&& !c.stance.prone(self);
	}

	@Override
	public String describe() {
		return "Inflicts arousal over time: 5 Arousal, 5 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(5);
		self.spendArousal(5);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.REYKA) {
				c.offerImage("Lust Aura.png", "Art by AimlessArt");
			}
		}
		target.add(new Horny(target, 3 + 2 * self.getSkimpiness(), 3 + Global.random(3)), c);
		target.emote(Emotion.horny, 10);
	}

	@Override
	public Skill copy(Character user) {
		return new LustAura(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You allow the corruption in your libido to spread out from your body. %target% flushes with arousal and "
				+ "presses %hisher% thighs together as the aura taints %himher%.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% releases an aura of pure sex. You feel your body becoming hot just being near %himher%.";
	}
}
