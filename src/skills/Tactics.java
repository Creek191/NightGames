package skills;

import java.awt.*;

/**
 * Defines the different tactics a skill can belong to
 */
public enum Tactics {
	damage(TacticGroup.Hurt, new Color(150, 0, 0)),
	pleasure(TacticGroup.Pleasure, Color.PINK),
	fucking(TacticGroup.Pleasure, new Color(255, 100, 200)),
	positioning(TacticGroup.Positioning, new Color(0, 100, 0)),
	stripping(TacticGroup.Positioning, new Color(0, 100, 0)),
	recovery(TacticGroup.Recovery, Color.WHITE),
	calming(TacticGroup.Recovery, Color.WHITE),
	status(TacticGroup.Manipulation, Color.CYAN),
	summoning(TacticGroup.Manipulation, Color.YELLOW),
	misc(TacticGroup.Misc, new Color(200, 200, 200)),
	preparation(TacticGroup.Manipulation, Color.GREEN),
	negative(TacticGroup.Misc, new Color(200, 200, 200)),
	demand(TacticGroup.Demand, Color.RED);

	private final Color color;
	private final TacticGroup group;

	Tactics(TacticGroup group, Color color) {
		this.color = color;
		this.group = group;
	}

	/**
	 * Gets the color assigned to the tactic
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Gets the group the tactic belongs to
	 */
	public TacticGroup getGroup() {
		return group;
	}
}