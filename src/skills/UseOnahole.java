package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import items.Attachment;
import items.Toy;

/**
 * Defines a skill where a character uses an onahole on their opponent
 */
public class UseOnahole extends Skill {
	public UseOnahole(Character self) {
		super(Toy.Onahole.getFullName(self), self);
		addTag(SkillTag.TOY);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& (self.has(Toy.Onahole) || self.has(Toy.Onahole2))
				&& target.hasDick()
				&& target.pantsless()
				&& c.stance.reachBottom(self)
				&& !c.stance.penetration(self)
				;
	}

	@Override
	public void resolve(Combat c, Character target) {
		var block = 0;
		if (c.lastAction(target) != null
				&& c.lastAction(target).name.equals(Toy.Dildo.getFullName(target))) {
			block = 5;
		}
		if (c.attackRoll(this, self, target) && Global.random(20) >= block) {
			var damage = 6 + (target.getEffective(Attribute.Perception) / 2)
					+ (self.getEffective(Attribute.Science) / 2);
			var level = 1;
			if (self.has(Toy.Onahole2)) {
				damage += 5;
				level++;
			}
			if (self.has(Attachment.OnaholeSlimy)) {
				damage *= 2;
				level++;
			}
			if (self.human()) {
				c.write(self, formattedDeal(level, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(level, Result.normal, target));
			}
			if (self.has(Attachment.OnaholeVibe)) {
				if (self.human()) {
					c.write(self, formattedDeal(1, Result.upgrade, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(1, Result.upgrade, target));
				}
			}
			damage = self.bonusProficiency(Anatomy.toy, damage);
			if (self.has(Toy.Onahole2)) {
				target.pleasure(damage, Anatomy.genitals, c);
			}
			else {
				target.pleasure(damage, Anatomy.genitals, Result.finisher, c);
			}
			if (target.human()) {
				if (self.has(Attachment.OnaholeSlimy)) {
					c.offerImage("Onahole4.jpg", "Art by AimlessArt");
				}
				else if (self.has(Attachment.OnaholeVibe)) {
					c.offerImage("Onahole3.jpg", "Art by AimlessArt");
				}
				if (self.has(Toy.Onahole2)) {
					c.offerImage("Onahole2.jpg", "Art by AimlessArt");
				}
				else {
					c.offerImage("Onahole1.jpg", "Art by AimlessArt");
				}
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(block, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(block, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new UseOnahole(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (damage > 0) {
				return "You try to stick your onahole onto %target%'s dick, but %heshe% shoves a dildo into the entrance, "
						+ "blocking it.";
			}
			else {
				return "You try to stick your onahole onto %target%'s dick, but %heshe% manages to avoid it.";
			}
		}
		else if (modifier == Result.upgrade) {
			return "The internal vibrator adds to %hisher% pleasure.";
		}
		else {
			switch (damage) {
				case 3:
					return "You slide your living onahole over %target%'s dick. You can feel the toy reshape itself in "
							+ "your hand to perfectly fit the shape of %hisher% member. You don't even need to move the "
							+ "toy. You just hold the slime as it eagerly milks %target%'s rod.";
				case 2:
					return "You slide your onahole over %target%'s dick. The well-lubricated toy covers %hisher% member "
							+ "with minimal resistance. As you pump it, %heshe% moans in pleasure and %hisher% hips buck "
							+ "involuntarily.";
				default:
					return "You stick your cocksleeve onto %target%'s erection and rapidly pump it. %HeShe% squirms a "
							+ "bit at the sensation and can't quite stifle a moan.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (damage > 0) {
				return "%self% tries to stick a cocksleeve on your dick, but you intercept it with the head of your " + Toy.Dildo.getFullName(
						target) + ".";
			}
			else {
				return "%self% tries to stick a cocksleeve on your dick, but you manage to avoid it.";
			}
		}
		else if (modifier == Result.upgrade) {
			return "The toy vibrates around your penis, giving you additional pleasure";
		}
		else {
			switch (damage) {
				case 3:
					return "%self% slicks %hisher% slimy cocksleeve onto your dick. Before your shaft is even fully "
							+ "inside, the toy expands to engulf your entire cock and balls. You groan with pleasure as "
							+ "it wriggles around and squeezes your genitals as if alive.";
				case 2:
					return "%self% slides %hisher% cocksleeve over your dick and starts pumping it. The sensation is the "
							+ "same as if %heshe% was riding you, but you're the only one who's feeling anything.";
				default:
					return "%self% forces a cocksleeve over your erection and begins to pump it. At first the feeling is "
							+ "strange and a little bit uncomfortable, but the discomfort gradually becomes pleasure.";
			}
		}
	}

	@Override
	public String describe() {
		return "Pleasure opponent with an Onahole";
	}
}
