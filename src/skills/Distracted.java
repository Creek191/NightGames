package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Stsflag;

/**
 * Defines a skill where the user is distracted and fails to act
 */
public class Distracted extends Skill {
	public Distracted(Character self) {
		super("Distracted", self);
	}

	@Override
	public boolean requirements(Character user) {
		return false;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.distracted() && !self.is(Stsflag.enthralled);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Distracted(user);
	}

	@Override
	public Tactics type() {
		return Tactics.misc;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		switch (Global.random(4)) {
			case 0:
				return "You can't seem to keep your focus.";
			case 1:
				return "Your head just isn't in the game right now.";
			case 2:
				return "You feel out of it and fail to do anything meaningful.";
			default:
				return "You miss your opportunity to act.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		switch (Global.random(3)) {
			case 0:
				return "%self% must have %hisher% head in the clouds, but you aren't one to miss an open opportunity...";
			case 1:
				return "%self% doesn't seem like %hisher% mind is keeping up with the rest of %himher% and is full of "
						+ "openings.";
			default:
				return "%self% looks a little unfocused and makes no attempt to defend %himher%self.";
		}
	}

	@Override
	public String describe() {
		return "Caught off guard";
	}
}
