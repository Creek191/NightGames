package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Shamed;

/**
 * Defines a skill where a character taunts their opponent in an attempt to shame them
 */
public class Taunt extends Skill {
	public Taunt(Character self) {
		super("Taunt", self);
		this.speed = 9;
		addTag(SkillTag.MISCHIEF);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(5)
				&& !self.has(Trait.shy)
				&& target.nude()
				&& !c.stance.sub(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(5);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		var damage = 3 + self.getEffective(Attribute.Seduction) / 2;
		if (c.stance.dom(self)) {
			damage *= 1.5;
			target.add(new Shamed(target), c);
		}
		target.tempt(damage, self.bonusTemptation(), c);
		target.emote(Emotion.angry, 30);
		target.emote(Emotion.nervous, 15);
		self.emote(Emotion.dominant, 20);
		target.getPool(Pool.MOJO).reduce(10);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Cunning) >= 8;
	}

	@Override
	public Skill copy(Character user) {
		return new Taunt(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		switch (Global.random(2)) {
			case 1:
				return "You tell %target% that %heshe%'s free to stop fighting back whenever %heshe%'s ready to get "
						+ "fucked.";
			default:
				return "You tell %target% that if %heshe%'s so eager to be fucked senseless, you're available during "
						+ "off hours.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.taunt();
	}

	@Override
	public String describe() {
		return "Embarrass your opponent, may inflict Shamed";
	}
}
