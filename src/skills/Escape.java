package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Neutral;
import status.Braced;
import status.Stsflag;

/**
 * Defines a skill where a character attempts to escape a submissive position
 */
public class Escape extends Skill {
	public Escape(Character self) {
		super("Escape", self);
		this.speed = 1;
		addTag(Attribute.Cunning);
		addTag(SkillTag.ESCAPE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Cunning) >= 12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return ((c.stance.sub(self)
				&& !c.stance.mobile(self)
				&& !c.stance.penetration(self)
				&& !c.stance.penetration(target)) || self.bound())
				&& !self.stunned()
				&& !self.distracted()
				&& !self.is(Stsflag.enthralled);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.bound()) {
			if (self.check(Attribute.Cunning, 10 - self.escapeModifier())) {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.normal, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				self.free();
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.miss, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.miss, target));
				}
			}
		}
		else if (self.check(Attribute.Cunning,
				c.stance.escapeDC(target,
						self) + (target.getStamina().get() / 4 - self.getStamina().get() / 4) + (target.getEffective(
						Attribute.Power) / 2 - Math.max(self.getEffective(Attribute.Power),
						self.getEffective(Attribute.Cunning)) / 2))) {
			if (self.human()) {
				c.write(self, formattedDeal(1, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(1, Result.normal, target));
			}
			if (c.stance.prone(self) && !self.is(Stsflag.braced)) {
				self.add(new Braced(self));
			}
			c.stance = new Neutral(self, target);
		}
		else {
			if (self.human()) {
				if (self.nude()) {
					c.write(self, formattedDeal(1, Result.miss, target));
				}
				else {
					c.write(self, formattedDeal(2, Result.miss, target));
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(1, Result.miss, target));
			}
			c.stance.struggle(c);
			c.stance.decay();
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Escape(user);
	}

	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		switch (modifier) {
			case normal:
				if (damage == 0) {
					return "You slip your hands out of your restraints.";
				}
				else {
					return "Your quick wits find a gap in %target%'s hold and you slip away.";
				}
			case miss:
				if (damage == 0) {
					return "You try to slip your restraints, but can't get free.";
				}
				else if (damage == 1) {
					return "You try to take advantage of an opening in %target%'s stance to slip away, but %heshe% catches "
							+ "you by your protruding penis and reasserts %hisher% position.";
				}
				else {
					return "You think you see an opening in %target%'s stance, but %heshe% corrects it before you can take "
							+ "advantage.";
				}
		}
		// Should never happen
		return "";
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		switch (modifier) {
			case normal:
				if (damage == 0) {
					return "%self% manages to free %himher%self.";
				}
				else {
					return "%self% goes limp and you take the opportunity to adjust your grip on %himher%. As soon as you "
							+ "move, %heshe% bolts out of your weakened hold. It was a trick!";
				}
			case miss:
				if (damage == 0) {
					return "%self% squirms against %hisher% restraints fruitlessly.";
				}
				else {
					return "%self% manages to slip out of your grip for a moment, but you tickle %himher% before %heshe% "
							+ "can get far and regain control.";
				}
		}
		// Should never happen
		return "";
	}

	@Override
	public String describe() {
		return "Uses Cunning to try to escape a submissive position";
	}
}
