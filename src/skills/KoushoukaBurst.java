package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Horny;

/**
 * Defines a skill where a character performs a ritual to increase their opponent's urge for sex
 */
public class KoushoukaBurst extends Skill {
	public KoushoukaBurst(Character self) {
		super("Fertility Rite", self);
		this.speed = 1;
		addTag(Attribute.Ninjutsu);
		addTag(SkillTag.ULTIMATE);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu) >= 30;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(25)
				&& !target.canAct()
				&& target.pantsless()
				&& c.stance.reachBottom(self);
	}

	@Override
	public String describe() {
		return "A lengthy ritual that causes an overwhelming urge to have sex: 25 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(25);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.add(new Horny(target, 20, 10), c);
	}

	@Override
	public Skill copy(Character user) {
		return new KoushoukaBurst(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (target.hasBalls()) {
			return "You carefully grab %target%'s balls between your fingers and quickly complete the gestures Yui showed "
					+ "you. This should briefly boost %hisher% sperm and testosterone production, giving %himher% an "
					+ "uncontrollable need to reproduce.";
		}
		else {
			return "You make a series of gestures on %target%'s bare abdomen, which should, in theory, stimulate the "
					+ "pressure points around %hisher% ovaries and womb. You see %hisher% face quickly flush as %heshe% "
					+ "starts to tremble with an intense need for sex.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% kneels between your legs and carefully holds your bare balls. %HeShe% makes a series of hand "
				+ "movements, while muttering something under %hisher% breath. When %heshe% finishes, a desperate heat "
				+ "starts to grow in your testicles, and you feel overwhelmed by a desire to cum.";
	}
}
