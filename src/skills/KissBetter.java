package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.Horny;
import status.Stsflag;

/**
 * Defines a skill where a character kisses an opponents genitals to make them better
 */
public class KissBetter extends Skill {
	public KissBetter(Character self) {
		super("Kiss Better", self);
		addTag(Attribute.Footballer);
		addTag(Result.oral);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Footballer) >= 15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.is(Stsflag.stunned)
				&& target.pantsless()
				&& c.stance.oral(self)
				&& !c.stance.behind(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public String describe() {
		return "Lick and suck a stunned opponent's genitals to make them feel better.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			if (target.hasBalls()) {
				c.write(self, formattedDeal(0, Result.strong, target));
			}
			else {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.pleasure(self.getEffective(Attribute.Footballer)
						+ (2 * self.getEffective(Attribute.Seduction) / 3)
						+ target.getEffective(Attribute.Perception),
				Anatomy.genitals, c);
		target.heal(target.getStamina().max() / 3);
		target.add(new Horny(target, 2, 8), c);
	}

	@Override
	public Skill copy(Character user) {
		return new KissBetter(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.strong) {
			return "You lean over %target%'s unresisting body and suck on %hisher% tender balls. %HeShe% coos with "
					+ "pleasure as %hisher% pain is replaced with arousal.";
		}
		return "You lean over %target%'s unresisting body and lavish %hisher% exposed pussy with licking and sucking. "
				+ "%HeShe% coos with pleasure as %hisher% pain is replaced with arousal.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% gives your naked balls a sloppy kiss before taking them into %hisher% mouth one at a time and "
				+ "sucking them. You feel better, both in the oral pleasure sense, and the sense of your pain easing.";
	}
}
