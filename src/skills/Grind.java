package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import stance.Stance;
import status.ProfMod;
import status.ProfessionalMomentum;

/**
 * Defines a skill where a character grinds against their opponent during intercourse
 */
public class Grind extends Skill {
	public Grind(Character self) {
		super("Grind", self);
		addTag(Attribute.Seduction);
		addTag(Result.intercourse);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 14;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.en != Stance.anal
				&& c.stance.dom(self)
				&& c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = self.getSexPleasure(1, Attribute.Seduction) + target.getEffective(Attribute.Perception);
		if (self.human()) {
			c.write(self, formattedDeal(damage, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(damage, Result.normal, self));
		}
		if (self.has(Trait.strapped)) {
			damage += +(self.getEffective(Attribute.Science) / 2);
			damage = self.bonusProficiency(Anatomy.toy, damage);
		}
		target.pleasure(damage, Anatomy.genitals, c);
		var recoil = target.getSexPleasure(1, Attribute.Seduction) / 3;
		recoil += target.bonusRecoilPleasure(recoil);
		if (self.has(Trait.experienced)) {
			recoil = recoil / 2;
		}

		self.pleasure(recoil, Anatomy.genitals, c);
		if (self.getPure(Attribute.Professional) >= 11) {
			if (self.has(Trait.sexuallyflexible)) {
				self.add(new ProfessionalMomentum(self, self.getEffective(Attribute.Professional) * 5), c);
			}
			else {
				self.add(new ProfMod("Sexual Momentum",
						self,
						Anatomy.genitals,
						self.getEffective(Attribute.Professional) * 5), c);
			}
		}
		c.stance.setPace(0);
		self.buildMojo(10);
	}

	@Override
	public Skill copy(Character user) {
		return new Grind(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You grind your hips against %target% without thrusting. %HeShe% trembles and gasps as the movement "
				+ "stimulates %hisher% clit and the walls of %hisher% pussy.";
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		return "%self% grinds against you, stimulating your entire manhood and bringing you closer to climax.";
	}

	@Override
	public String describe() {
		return "Grind against your opponent with minimal thrusting. Extremely consistent pleasure";
	}
}
