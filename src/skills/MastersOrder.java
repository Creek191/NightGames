package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.OrderedStrip;
import status.Stsflag;

/**
 * Defines a skill where a character uses their dominance to order their opponent to strip or  be punished
 */
public class MastersOrder extends Skill {
	public MastersOrder(Character self) {
		super("Master's Order", self);
		addTag(Attribute.Discipline);
		addTag(SkillTag.DOM);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Discipline) >= 6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(20)
				&& self.is(Stsflag.composed)
				&& target.canAct()
				&& !target.nude()
				&& !target.is(Stsflag.orderedstrip)
				&& c.stance.mobile(self);
	}

	@Override
	public String describe() {
		return "Order your opponent to undress for you or be punished: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.spendMojo(20);
		target.add(new OrderedStrip(target), c);
	}

	@Override
	public Skill copy(Character user) {
		return new MastersOrder(user);
	}

	@Override
	public Tactics type() {
		return Tactics.stripping;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You catch %self%'s gaze with your own and move toward %himher% menacingly. You briefly glance toward "
				+ "%himher% apparel, then meet %hisher% gaze once more, mentioning that %heshe% seems to be wearing far "
				+ "too much clothing right now. %HeShe%'ll have to deal with it soon, or else face the consequences of "
				+ "%hisher% disobedience.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% suddenly manages to catch your gaze. %HeShe% moves toward you, somehow managing to loom menacingly "
				+ "over you as %heshe% does so. <i>\"Now now,\"</i> %self% says. <i>\"Don't you think you're wearing far "
				+ "too much for an event like this. I recommend you remedy this immediately.\"</i> %HeShe% gives you a "
				+ "grin with just a hint of a threat behind it. You wouldn't want to disobey and make me upset with you, "
				+ "now would you?<p>You get a strong feeling that if you don't strip yourself very soon, you're in deep "
				+ "trouble";
	}
}
