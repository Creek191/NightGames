package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Behind;
import status.Flatfooted;

/**
 * Defines a skill where a character uses a feint to slip behind the opponent
 */
public class ChargeFeint extends Skill {
	public ChargeFeint(Character self) {
		super("Charge Feint", self);
		addTag(Attribute.Footballer);
		addTag(SkillTag.OUTMANEUVER);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Footballer) >= 1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c);
	}

	@Override
	public String describe() {
		return "Fake out your opponent with fancy footwork";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.effectRoll(this, self, target, self.getEffective(Attribute.Footballer))) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			var duration = 1;
			if (self.getPure(Attribute.Footballer) >= 12) {
				duration++;
			}
			target.add(new Flatfooted(target, duration), c);
			c.stance = new Behind(self, target);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new ChargeFeint(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You rush at %target% and attempt to dodge behind %himher%, but %heshe%'s too quick.";
		}
		else {
			return "You rush straight at %target% and feint to the right before dodging left. %HeShe% stumbles and you "
					+ "slip behind %himher%.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% rushes at you with quick footwork, but you stop %himher% from getting behind you.";
		}
		else {
			return "%self% rushes straight at you. At the last moment, %heshe% dodges as expected, but when you try to "
					+ "follow %himher%, %hisher% quick footwork trips you up.";
		}
	}
}
