package skills;

import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import items.Consumable;
import status.Bound;
import status.Stsflag;

/**
 * Defines a skill where a character ties up their opponent's hands
 */
public class Tie extends Skill {
	public Tie(Character self) {
		super("Bind", self);
		this.accuracy = 1;
		this.speed = 2;
		addTag(SkillTag.BIND);
		addTag(SkillTag.CONSUMABLE);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& (self.has(Consumable.ZipTie) || self.has(Consumable.Handcuffs))
				&& !target.is(Stsflag.bound)
				&& c.stance.dom(self)
				&& c.stance.reachTop(self)
				&& !c.stance.mobile(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.has(Consumable.Handcuffs, 1)) {
			self.consume(Consumable.Handcuffs, 1);
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.special, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.special, target));
			}
			target.add(new Bound(target, 40, "handcuffs"), c);
		}
		else {
			self.consume(Consumable.ZipTie, 1);
			if (c.attackRoll(this, self, target)) {
				if (self.has(Trait.tieGuy) && self.has(Consumable.ZipTie)) {
					self.consume(Consumable.ZipTie, 1);
					if (self.human()) {
						c.write(self, formattedDeal(0, Result.strong, target));
					}
					else if (target.human()) {
						c.write(self, formattedReceive(0, Result.strong, target));
						if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Mara")) {
							c.offerImage("Zip Tie.jpg", "Art by AimlessArt");
						}
					}
					target.add(new Bound(target, 30, "zipties"), c);
				}
				else {
					if (self.human()) {
						c.write(self, formattedDeal(0, Result.normal, target));
					}
					else if (target.human()) {
						c.write(self, formattedReceive(0, Result.normal, target));
						if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Mara")) {
							c.offerImage("Zip Tie.jpg", "Art by AimlessArt");
						}
					}
					target.add(new Bound(target, 20, "ziptie"), c);
				}
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.miss, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.miss, target));
				}
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new Tie(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You try to catch %target%'s hands, but %heshe% squirms too much to keep your grip on %himher%.";
		}
		else if (modifier == Result.special) {
			return "You catch %target%'s wrists and slap a pair of cuffs on %himher%.";
		}
		else if (modifier == Result.strong) {
			return "You catch both of %target%'s hands and wrap a zip tie around %hisher% wrists. You get a second tie "
					+ "around %hisher% elbows to make it harder for %himher% to escape.";
		}
		else {
			return "You catch both of %target%'s hands and wrap a zip tie around %hisher% wrists.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to tie you down, but you keep your arms free.";
		}
		else if (modifier == Result.special) {
			return "%self% restrains you with a pair of handcuffs.";
		}
		else if (modifier == Result.strong) {
			return "%self% secures your hands with a zip tie and ties a second one around your elbows.";
		}
		else {
			return "%self% secures your hands with a zip tie.";
		}
	}

	@Override
	public String describe() {
		return "Tie up your opponent's hands with a zip tie";
	}
}
