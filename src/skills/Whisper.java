package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import status.Enthralled;
import status.Sensitive;

/**
 * Defines a skill where a character arouses their opponent by whispering in their ear
 */
public class Whisper extends Skill {
	public Whisper(Character self) {
		super("Whisper", self);
		this.speed = 9;
		addTag(Attribute.Seduction);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.has(Trait.direct)
				&& c.stance.kiss(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.has(Trait.darkpromises) && Global.random(5) == 4 && self.canSpend(15)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.special, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.special, target));
				if (!Global.checkFlag(Flag.exactimages) || self.name().startsWith("Angel")) {
					c.offerImage("Dark Promise.jpg", "Art by AimlessArt");
				}
			}
			self.spendMojo(15);
			target.add(new Enthralled(target, self), c);
		}
		else if (self.getPure(Attribute.Professional) >= 1) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.strong, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.strong, target));
			}
			target.add(new Sensitive(target,
							2,
							Anatomy.genitals,
							1 + self.getEffective(Attribute.Professional) / 10F),
					c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
		}

		target.tempt((self.getEffective(Attribute.Seduction) / 2), Result.foreplay, c);
		if (self.has(Trait.secretkeeper)) {
			target.emote(Emotion.nervous, 50);
		}
		target.emote(Emotion.horny, 30);
		self.buildMojo(10);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 32 && !user.has(Trait.direct);
	}

	@Override
	public Skill copy(Character user) {
		return new Whisper(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "You whisper words of domination in %target%'s ear, filling %himher% with your darkness. The spirit "
					+ "in %hisher% eyes seems to dim as %heshe% submits to your will.";
		}
		if (modifier == Result.strong) {
			return "You whisper extraordinarily suggestive promises into %target%'s ear until %heshe%'s primed to receive "
					+ "your touch.";
		}
		else {
			return "You whisper sweet nothings in %target%'s ear. Judging by %hisher% blush, it was fairly effective.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "%self% whispers in your ear in some eldritch language. %HisHer% words echo through your head and "
					+ "you feel a strong compulsion to do what %heshe% tells you.";
		}
		if (modifier == Result.strong) {
			return "%self% whispers some deliciously seductive suggestions in your ear. %HeShe% paints such an erotic "
					+ "mental picture for you that you find your dick straining with anticipation.";
		}
		else {
			return "%self% whispers some deliciously seductive suggestions in your ear.";
		}
	}

	@Override
	public String describe() {
		return "Arouse opponent by whispering in her ear";
	}
}
