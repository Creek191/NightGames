package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import stance.Mount;
import stance.Stance;

/**
 * Defines a command where an enthralled character is forced to lay on the ground
 */
public class CommandDown extends PlayerCommand {
	public CommandDown(Character self) {
		super("Force Down", self);
		addTag(SkillTag.COMMAND);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return super.usable(c, target) && c.stance.en == Stance.neutral;
	}

	@Override
	public String describe() {
		return "Command your opponent to lay down on the ground.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		c.stance = new Mount(self, target);
		c.write(self, formattedDeal(0, Result.normal, target));
	}

	@Override
	public Skill copy(Character user) {
		return new CommandDown(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		return "Trembling under the weight of your command, %target% lies down. You follow %himher% down and mount "
				+ "%himher%, facing %hisher% head.";
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The Silver Bard: CommandDown-receive>>";
	}
}
