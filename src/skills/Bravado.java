package skills;

import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character spends all their mojo to restore stamina and arousal
 */
public class Bravado extends Skill {
	public Bravado(Character self) {
		super("Determination", self);
	}

	@Override
	public boolean requirements(Character user) {
		return user.has(Trait.fearless);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.mobile(self)
				&& self.canSpend(20);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var mojo = self.getMojo().get();
		if (self.human()) {
			c.write(self, formattedDeal(mojo, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(mojo, Result.normal, target));
		}
		self.spendMojo(mojo);
		self.calm(mojo / 2, c);
		self.heal(mojo, c);
		self.emote(Emotion.confident, 30);
		self.emote(Emotion.dominant, 20);
		self.emote(Emotion.nervous, -20);
		self.emote(Emotion.desperate, -30);
	}

	@Override
	public Skill copy(Character user) {
		return new Bravado(user);
	}

	@Override
	public Tactics type() {
		return Tactics.recovery;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (Global.random(2) == 0) {
			return "You refuse to give in and use all your pent up will to keep fighting on.";
		}
		else {
			return "You grit your teeth and put all your willpower into the fight.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		if (Global.random(2) == 0) {
			return "You can tell by the glint in %self%'s eye that %heshe% has a renewed sense of purpose in this fight.";
		}
		else {
			return "%self% gives you a determined glare as %heshe% seems to gain a second wind.";
		}
	}

	@Override
	public String describe() {
		return "Consume mojo to restore stamina and reduce arousal";
	}
}
