package skills;

import characters.Attribute;
import characters.Character;
import characters.ID;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import status.Stsflag;

/**
 * Defines a skill where a character summons tentacles to drain their opponent
 */
public class TentacleDrain extends Skill {
	public TentacleDrain(Character self) {
		super("Tentacle Drain", self);
		addTag(Attribute.Eldritch);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Eldritch) >= 24;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !self.stunned()
				&& !self.distracted()
				&& !self.is(Stsflag.enthralled)
				&& target.pantsless();
	}

	@Override
	public String describe() {
		return null;
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(1, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(1, Result.normal, target));
			if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SELENE) {
				c.offerImage("Selene Drain.jpg", "Art by AimlessArt");
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new TentacleDrain(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return null;
	}
}
