package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.IceStance;
import status.Stsflag;

/**
 * Defines a skill where a character boosts their pleasure resistance, but gains no more mojo
 */
public class IceForm extends Skill {
	public IceForm(Character self) {
		super("Ice Form", self);
		addTag(Attribute.Ki);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ki) >= 18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.is(Stsflag.form)
				&& !c.stance.sub(self);
	}

	@Override
	public String describe() {
		return "Improves resistance to pleasure, reduces mojo gain to zero.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new IceStance(self), c);
	}

	@Override
	public Skill copy(Character user) {
		return new IceForm(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You visualize yourself at the center of a raging snow storm. You can already feel yourself start to go numb.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% takes a deep breath and %hisher% expression turns so frosty that you're not sure you can ever thaw "
				+ "%himher% out.";
	}
}
