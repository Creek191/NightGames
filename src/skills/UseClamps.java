package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import items.Toy;
import status.Sensitive;
import status.Sore;

/**
 * Defines a skill where a character uses nipple clamps on their opponent
 */
public class UseClamps extends Skill {
	public UseClamps(Character self) {
		super("Nipple Clamps", self);
		addTag(SkillTag.TOY);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.has(Toy.nippleclamp)
				&& target.topless()
				&& c.stance.reachTop(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			var damage = 4 + target.getEffective(Attribute.Perception) + self.getEffective(Attribute.Science) / 2;
			damage = self.bonusProficiency(Anatomy.toy, damage);
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			target.add(new Sore(target, 4, Anatomy.chest, 2f), c);
			target.add(new Sensitive(target, 4, Anatomy.chest, 2f), c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new UseClamps(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You try to attach your nipple clamps to %target% but %heshe% blocks them.";
		}
		else {
			return "Apply your golden clamps to %target%'s nipples, leaving them swollen and extra sensitive.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to put some gold clamps on your nipples, but you push %himher% away.";
		}
		else {
			return "%self% sticks a pair of ornate nipple clamps onto your chest and then yanks them off. You grit your "
					+ "teeth at the sharp pain that leaves your nipples feeling sore and sensitive.";
		}
	}

	@Override
	public String describe() {
		return "Use your clamps to sensitize your opponents' nipples";
	}
}
