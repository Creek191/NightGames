package skills;

import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character focuses their mind to gain mojo
 */
public class Focus extends Skill {

	public Focus(Character self) {
		super("Focus", self);
		this.speed = 0;
		addTag(Attribute.Cunning);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct() && !c.stance.sub(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.calm(Global.random(8));
		self.buildMojo(20);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Cunning) >= 15 && !user.has(Trait.undisciplined);
	}

	@Override
	public Skill copy(Character user) {
		return new Focus(user);
	}

	@Override
	public Tactics type() {
		return Tactics.calming;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You take a moment to clear your thoughts, focusing your mind and calming your body.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% closes %hisher% eyes and takes a deep breath. When %heshe% opens %hisher% eyes, %heshe% seems more composed.";
	}

	@Override
	public String describe() {
		return "Calm yourself and gain some mojo";
	}
}
