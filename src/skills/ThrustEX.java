package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import status.ProfMod;
import status.ProfessionalMomentum;

/**
 * Defines a skill where a character skillfully thrusts into their opponent
 */
public class ThrustEX extends Skill {
	public ThrustEX(Character self) {
		super("Smooth Thrust", self);
		addTag(Result.intercourse);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(15)
				&& c.stance.dom(self)
				&& c.stance.penetration(self);
	}

	@Override
	public String describe() {
		switch (getStyle()) {
			case powerful:
				return "A deep, powerful Thrust: 15 Mojo";
			case clever:
				return "An accurate, cunning Thrust: 15 Mojo";
			default:
				return "A more potent fuck: 15 Mojo";
		}
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(15);
		int damage;
		var style = getStyle();
		if (style == Result.powerful) {
			damage = Global.random(5 + self.getEffective(Attribute.Power)) + target.getEffective(Attribute.Perception);
		}
		else if (style == Result.clever) {
			damage = Global.random(5 + self.getEffective(Attribute.Cunning)) + target.getEffective(Attribute.Perception);
		}
		else {
			damage = Global.random(5 + self.getEffective(Attribute.Seduction)) + target.getEffective(Attribute.Perception);
			damage *= 1.3f;
		}

		Anatomy inside;
		if (c.stance.anal()) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.anal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.anal, target));
			}
			inside = Anatomy.ass;
		}
		else {
			if (self.getPure(Attribute.Professional) >= 11) {
				if (self.has(Trait.sexuallyflexible)) {
					self.add(new ProfessionalMomentum(self, self.getEffective(Attribute.Professional) * 5), c);
				}
				else {
					self.add(new ProfMod("Sexual Momentum",
							self,
							Anatomy.genitals,
							self.getEffective(Attribute.Professional) * 5), c);
				}
			}
			if (self.human()) {
				c.write(self, formattedDeal(0, style, target));
				if (c.stance.behind(self)) {
					if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.KAT) {
						c.offerImage("Doggy Style.jpg", "Art by AimlessArt");
					}
					if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.MARA) {
						c.offerImage("Doggy Style Mara.jpg", "Art by AimlessArt");
					}
				}
				else {
					if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.ANGEL) {
						c.offerImage("Angel_Sex.jpg", "Art by AimlessArt");
						c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri");
					}
					c.offerImage("Thrust.jpg", "Art by AimlessArt");
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, style, target));
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.CASSIE) {
					c.offerImage("Cowgirl.jpg", "Art by AimlessArt");
				}
				if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SAMANTHA) {
					c.offerImage("Samantha Cowgirl.jpg", "Art by AimlessArt");
				}
			}
			inside = Anatomy.genitals;
		}
		if (self.has(Trait.strapped)) {
			damage += +(self.getEffective(Attribute.Science) / 2);
			damage = self.bonusProficiency(Anatomy.toy, damage);
		}
		target.pleasure(damage, inside, c);
		var recoil = (target.getSexPleasure(2, Attribute.Seduction) * 2) / 3;
		recoil += target.bonusRecoilPleasure(recoil);
		if (self.has(Trait.experienced)) {
			recoil = recoil / 2;
		}

		self.pleasure(recoil, Anatomy.genitals, c);
		c.stance.setPace(1);
	}

	@Override
	public Skill copy(Character user) {
		return new ThrustEX(user);
	}

	@Override
	public Tactics type() {
		return Tactics.fucking;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.powerful) {
			return "You thrust deep into %target%'s pussy, in slow, powerful strokes. You push so deep with every thrust "
					+ "that your cock bumps against %hisher% cervix.";
		}
		else if (modifier == Result.clever) {
			return "You angle your hips and thrust into %target%, carefully aiming for %hisher% G-spot. You make sure "
					+ "each stroke brushes your pelvis against %hisher% clit, maximizing %hisher% pleasure.";
		}
		else {
			return "You thrust your hips rhythmically, creating a smooth and constant motion. Your sensual strokes have "
					+ "no clear start or finish, and %target% is quickly pulled into your rhythm as you pleasure %hisher% "
					+ "pussy.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.anal) {
			var using = "cock";
			if (self.has(Trait.strapped)) {
				using = "strapon dildo";
			}
			var style = getStyle();
			if (style == Result.powerful) {
				return "%self% shoves %hisher% " + using + " deep into your ass with such a powerful stroke that you "
						+ "barely stay standing.";
			}
			else if (style == Result.clever) {
				return "%self% skillfully prods and torments your prostate with the head of %hisher% " + using + ".";
			}
			else {
				return "%self% thrusts %hisher% hips, pumping %hisher% " + using + " in and out of your ass like a "
						+ "well-oiled machine. The rhythmic strokes send dizzying jolts of pleasure up your spine.";
			}
		}
		else {
			if (modifier == Result.powerful) {
				return "%self% lifts %himher%self almost off your cock before tightening %hisher% vaginal walls around "
						+ "your glans. %HeShe% drops %hisher% hips, taking your dick completely inside. The tight "
						+ "sensation along your entire length takes your breath away.";
			}
			else if (modifier == Result.clever) {
				return "%self% switches up %hisher% movements, stopping briefly at the top of each thrust to stimulate "
						+ "your sensitive cockhead with %hisher% entrance. The extra attention to the sensitive area "
						+ "arouses you faster than usual.";
			}
			else {
				return "%self% moves %hisher% hips like a skilled dancer, riding you with smooth, continuous motion. "
						+ "The constant stimulation effectively milks your dick, bringing you closer and closer to "
						+ "cumming.";
			}
		}
	}

	@Override
	public String toString() {
		switch (getStyle()) {
			case powerful:
				return "Strong Thrust";
			case clever:
				return "Precise Thrust";
			default:
				return name;
		}
	}

	private Result getStyle() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning)
				&& self.getEffective(Attribute.Power) > self.getEffective(Attribute.Seduction)) {
			return Result.powerful;
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Seduction)) {
			return Result.clever;
		}
		return Result.seductive;
	}
}
