package skills;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a command where an enthralled character is forced to perform oral
 */
public class CommandOral extends PlayerCommand {
	public CommandOral(Character self) {
		super("Force Oral", self);
		addTag(SkillTag.COMMAND);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return super.usable(c, target)
				&& self.pantsless()
				&& c.stance.reachBottom(self);
	}

	@Override
	public String describe() {
		return "Force your opponent to go down on you.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var silvertounge = target.has(Trait.silvertongue);
		var lowStart = self.getArousal().get() < 15;
		self.pleasure((silvertounge ? 8 : 5)
				+ Global.random(10), Anatomy.genitals, c);
		self.buildMojo(30);
		var lowEnd = self.getArousal().get() < 15;

		if (lowStart) {
			if (lowEnd) {
				c.write(self, formattedDeal(0, Result.weak, target));
			}
			else {
				c.write(self, formattedDeal(0, Result.strong, target));
			}
		}
		else {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
	}

	@Override
	public Skill copy(Character user) {
		return new CommandOral(user);
	}

	@Override
	public String deal(int magnitude, Result modifier, Character target) {
		switch (modifier) {
			case normal:
				return "%target% is ecstatic at being given the privilege of pleasuring you and does a fairly good job at "
						+ "it, too. %HeShe% sucks your hard dick powerfully while massaging your balls.";
			case strong:
				return "%target% seems delighted to 'help' you, and makes short work of taking your flaccid length into "
						+ "%hisher% mouth and getting it nice and hard.";
			case weak:
				return "%target% tries %hisher% very best to get you ready by running %hisher% tongue all over your groin, "
						+ "but even %hisher% psychically induced enthusiasm can't get you hard.";
			default:
				return "<<This should not be displayed, please inform The Silver Bard: CommandOral-deal>>";
		}
	}

	@Override
	public String receive(int magnitude, Result modifier, Character target) {
		return "<<This should not be displayed, please inform The Silver Bard: CommandOral-receive>>";
	}
}
