package skills;

import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Cowgirl;
import stance.Mount;
import stance.ReverseMount;
import status.Flatfooted;
import status.Stsflag;

import static combat.Result.*;

/**
 * Defines a skill where a character is ordering an enthralled opponent around
 */
public class Command extends Skill {
	public Command(Character self) {
		super("Command", self);
		addTag(SkillTag.COMMAND);
	}

	@Override
	public boolean requirements(Character user) {
		return !user.human();
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !self.human() && target.is(Stsflag.enthralled);
	}

	@Override
	public String describe() {
		return "Order your thrall around";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (!target.nude()) { // Undress self
			c.write(self, formattedReceive(0, miss, target));
			new Undress(target).resolve(c, self);
		}
		else if (!self.nude()) { // Undress me
			c.write(self, formattedReceive(0, Result.weak, target));
			if (self.topless()) {
				c.write(self, "Like a hungry beast, you rip off " + self.name()
						+ "'s " + self.bottom.pop().getName() + ".");
			}
			else {
				c.write(self, "Like a hungry beast, you rip off " + self.name()
						+ "'s " + self.top.pop().getName() + ".");
			}
		}
		else if (target.getArousal().get() <= 15) { // Masturbate
			c.write(self, formattedReceive(0, normal, target));
			new Masturbate(target).resolve(c, self);
		}
		else if (!c.stance.penetration(self) && c.stance.dom(target)) { // Fuck
			// me
			c.stance = new Mount(target, self);
			c.write(self, formattedReceive(0, special, target));
			new Fuck(target).resolve(c, self);
		}
		else if (c.stance.penetration(self)) { // I drain you
			if (Global.random(5) >= 4) {
				c.write(self, formattedReceive(0, critical, target));
				new Drain(self).resolve(c, target);
			}
			else {
				c.write(self, formattedReceive(0, critical, target));
				new Piston(self).resolve(c, target);
			}
		}
		else if (!c.stance.penetration(self) && self.getArousal().get() <= 15) { // Pleasure me
			c.write(self, formattedReceive(1, critical, target));
			c.stance = new ReverseMount(target, self);
			new Cunnilingus(target).resolve(c, self);
		}
		else if (!c.stance.penetration(self)) { // Lay down
			c.write(self, formattedReceive(2, critical, target));
			c.stance = new Cowgirl(self, target);
			new Thrust(self).resolve(c, target);
		}
		else { // Confused
			c.write(self, formattedReceive(0, null, target));
			target.removeStatus(Stsflag.enthralled, c);
			target.add(new Flatfooted(target, 1));
		}
	}

	@Override
	public Skill copy(Character target) {
		return new Command(target);
	}

	@Override
	public Tactics type() {
		return Tactics.misc;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == null) {
			return "%self%'s order confuses you for a moment, snapping %hisher% control over you.";
		}
		switch (modifier) {
			case critical:
				switch (damage) {
					case 0:
						return "While commanding you to be still, %self% starts bouncing wildly on your dick.";
					case 1:
						return "The scent of %hisher% juices overwhelms you, leaving you wanting nothing more than to "
								+ "taste %himher%.";
					case 2:
						return "You feel an irresistible compulsion to lay down on your back.";
					default:
						break;
				}
			case miss:
				return "You feel an uncontrollable desire to undress yourself.";
			case normal:
				return "%self%'s eyes tell you to pleasure yourself for %hisher% benefit.";
			case special:
				return "%self%'s voice pulls you in and you cannot resist fucking %himher%.";
			case weak:
				return "You are desperate to see more of %self%'s body.";
			default:
				return null;
		}
	}
}
