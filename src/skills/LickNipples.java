package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.Stance;

/**
 * Defines a skill where a character licks their opponent's nipples
 */
public class LickNipples extends Skill {
	public LickNipples(Character self) {
		super("Lick Nipples", self);
		addTag(Attribute.Seduction);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.topless()
				&& c.stance.en != Stance.facesitting
				&& c.stance.reachTop(self)
				&& !c.stance.behind(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
				if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Cassie")) {
					c.offerImage("LickNipples.jpg", "Art by Fujin Hitokiri");
				}
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			var damage = Global.random(4) + self.getEffective(Attribute.Seduction) / 4 + target.getEffective(Attribute.Perception) / 2;
			damage = self.bonusProficiency(Anatomy.mouth, damage);
			target.pleasure(damage, Anatomy.chest, c);
			self.buildMojo(10);
			target.emote(Emotion.horny, 5);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 14 && !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new LickNipples(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You go after %target%'s nipples, but %heshe% pushes you away.";
		}
		else {
			if (target.has(Trait.petite) && Global.random(3) == 0) {
				if (target.getArousal().percent() > 75) {
					return "%target% groans as your lips and tongue aggressively pleasure %hisher% erect nipples. Closing "
							+ "%hisher% eyes for a moment, %heshe% pushes %hisher% small breasts into your face to "
							+ "increase the pressure from your tongue.";
				}
				else {
					return "Pulling %hisher% small frame closer to you, you take one of %target%'s nipples into your mouth "
							+ "and lick all around it, enjoying the perkiness of %hisher% petite breasts.";
				}
			}
			else if (target.getArousal().percent() > 75 && Global.random(2) == 0) {
				return "You run your tongue roughly across %target%'s nipples, although by this point it feels like your "
						+ "input is hardly needed as %hisher% juices are flowing freely and %heshe% seems to be verge of "
						+ "giving up and just fingering %himher%self.";
			}
			else {
				switch (Global.random(4)) {
					case 3:
						return "You pull each of %target%'s nipples a little closer together and furiously run your tongue "
								+ "all over them.";
					case 0:
						return "You gently rub the tip of your tongue up and down each of %target%'s nipples, eliciting a "
								+ "small moan.";
					case 1:
						return "You place your lips around one of %target%'s nipples and attack it with your tongue. "
								+ "%HeShe% shivers in pleasure and you feel %hisher% nipple stiffening in your mouth.";
					default:
						return "You slowly circle your tongue around each of %target%'s nipples, making %himher% moan and "
								+ "squirm in pleasure.";
				}
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to suck on your chest, but you avoid %himher%.";
		}
		else {
			return "%self% licks and sucks your nipples, sending a surge of excitement straight to your groin.";
		}
	}

	@Override
	public String describe() {
		return "Suck your opponent's nipples";
	}
}
