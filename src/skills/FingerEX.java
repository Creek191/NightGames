package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Stance;
import status.ProfMod;
import status.ProfessionalMomentum;

/**
 * Defines a skill where a character expertly fingers their opponent's pussy
 */
public class FingerEX extends Skill {
	public FingerEX(Character self) {
		super("Shining Finger", self);
		this.accuracy = 7;
		addTag(Attribute.Seduction);
		addTag(Result.touch);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(20)
				&& c.stance.reachBottom(self)
				&& target.genitalsAvailable(c)
				&& target.hasPussy()
				&& (!c.stance.penetration(target) || c.stance.en == Stance.anal);
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = 0;
		var style = Result.seductive;
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning) && self.getEffective(Attribute.Power) > self.getEffective(
				Attribute.Seduction)) {
			style = Result.powerful;
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Seduction)) {
			style = Result.clever;
		}
		self.spendMojo(20);
		if (c.attackRoll(this, self, target)) {
			if (style == Result.powerful) {
				damage = 4 + Global.random(self.getEffective(Attribute.Power) + self.getEffective(Attribute.Professional)) + target.getEffective(
						Attribute.Perception);
			}
			else if (style == Result.clever) {
				damage = 4 + Global.random(self.getEffective(Attribute.Cunning) + self.getEffective(Attribute.Professional)) + target.getEffective(
						Attribute.Perception);
			}
			else {
				damage = 4 + Global.random(self.getEffective(Attribute.Seduction) + self.getEffective(Attribute.Professional)) + target.getEffective(
						Attribute.Perception);
				damage *= 1.3f;
			}

			if (self.getPure(Attribute.Professional) >= 3) {
				if (self.has(Trait.sexuallyflexible)) {
					self.add(new ProfessionalMomentum(self, self.getEffective(Attribute.Professional) * 5), c);
				}
				else {
					self.add(new ProfMod("Dexterous Momentum",
							self,
							Anatomy.fingers,
							self.getEffective(Attribute.Professional) * 5), c);
				}
			}
			if (self.human()) {
				c.write(self, formattedDeal(damage, style, target));
				c.offerImage("Fingering.jpg", "Art by AimlessArt");
				c.offerImage("Fingering2.jpg", "Art by AimlessArt");
			}
			else if (target.human()) {
				c.write(self, formattedReceive(damage, style, target));
			}
			damage = self.bonusProficiency(Anatomy.fingers, damage);
			target.pleasure(damage, Anatomy.genitals, Result.finisher, c);
			if (self.has(Trait.roughhandling)) {
				target.weaken(damage / 2, c);
				if (self.human()) {
					c.write(self, formattedDeal(damage, Result.unique, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(damage, Result.unique, target));
				}
			}
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getEffective(Attribute.Seduction) >= 8;
	}

	@Override
	public Skill copy(Character user) {
		return new FingerEX(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "You opportunistically give %hisher% clit a sharp pinch.";
		}
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "You try to slide your hand down to %target%'s pussy, but %heshe% bats your hand away.";
			}
			else {
				return "You grope at %target%'s pussy, but miss.";
			}
		}
		if (modifier == Result.powerful) {
			return "You plunge two fingers deep into %target%'s wet pussy. %HeShe% gasps at the strong penetration, and "
					+ "can barely stay standing as you jackhammer your fingers inside %himher%.";
		}
		else if (modifier == Result.clever) {
			return "You explore %target%'s feminine garden with your finger, watching %hisher% reactions closely. Whenever "
					+ "you find a spot that provokes a stronger reaction, you focus on rubbing and teasing it until "
					+ "%heshe%'s trembling with pleasure.";
		}
		else {
			switch (Global.random(2)) {
				case 1:
					return "Your hand practically glows with an awesome power. You slip two fingers into %target%'s pussy. "
							+ "You pour your love, your anger, even your sorrow into %himher%, leaving %himher% a quivering "
							+ "mess when you're done.";
				default:
					return "You skillfully pleasure %target%'s eager vagina with your best fingering technique. You cycle "
							+ "between tracing %hisher% labia, teasing %hisher% clit, and rubbing %hisher% G-spot. You "
							+ "switch targets frequently enough that %heshe% never gets used to the sensation.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "%self% gives your poor clit a painful pinch.";
		}
		if (modifier == Result.miss) {
			return "%self% gropes at your pussy, but misses the mark.";
		}
		else if (modifier == Result.critical) {
			return "%self% slides %hisher% shadow tentacles between your legs. The tendrils delve into your slick hole, "
					+ "overwhelming you with a strange pleasure";
		}
		else {
			if (target.getArousal().get() <= 15) {
				return "%self% softly rubs your sensitive lower lips. You aren't very aroused yet, but %hisher% gentle "
						+ "touch gives you a ticklish pleasure";
			}
			else if (target.getArousal().percent() < 50) {
				return "%self% skillfully fingers your sensitive pussy. You bite your lip and try to ignore the pleasure, "
						+ "but despite your best efforts, you feel yourself growing wet.";
			}
			else if (target.getArousal().percent() < 80) {
				return "%self% locates your clitoris and caress it directly, causing you to tremble from the powerful "
						+ "stimulation.";
			}
			else {
				return "%self% stirs your increasingly soaked pussy with %hisher% fingers and rubs your clit directly "
						+ "with %hisher% thumb.";
			}
		}
	}

	@Override
	public String toString() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning) && self.getEffective(Attribute.Power) > self.getEffective(
				Attribute.Seduction)) {
			return "Intense Finger";
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Seduction)) {
			return "Probing Finger";
		}
		return name;
	}

	@Override
	public String describe() {
		if (self.getEffective(Attribute.Power) > self.getEffective(Attribute.Cunning) && self.getEffective(Attribute.Power) > self.getEffective(
				Attribute.Seduction)) {
			return "Strong forceful fingering: 20 Mojo";
		}
		else if (self.getEffective(Attribute.Cunning) > self.getEffective(Attribute.Seduction)) {
			return "Exploratory fingering to identify sensitive areas: 20 Mojo";
		}
		else {
			return "Mojo boosted shining finger: 20 Mojo";
		}
	}
}
