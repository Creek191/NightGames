package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Pool;
import combat.Combat;
import combat.Result;

/**
 * Defines a skill where a character rewinds their own time to undo all damage they've taken
 */
public class Rewind extends Skill {
	public Rewind(Character self) {
		super("Rewind", self);
		addTag(Attribute.Temporal);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Temporal) >= 10;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(Pool.TIME, 8)
				&& !c.stance.prone(self)
				&& c.stance.mobile(self);
	}

	@Override
	public String describe() {
		return "Rewind your personal time to undo all damage you've taken: 8 charges";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spend(Pool.TIME, 8);
		self.getArousal().empty();
		self.getStamina().fill();
		self.clearStatus();
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.emote(Emotion.confident, 25);
		self.emote(Emotion.dominant, 20);
		target.emote(Emotion.nervous, 10);
		target.emote(Emotion.desperate, 10);
	}

	@Override
	public Skill copy(Character user) {
		return new Rewind(user);
	}

	@Override
	public Tactics type() {
		return Tactics.recovery;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "It takes a lot of time energy, but you manage to rewind your physical condition back to the very start "
				+ "of the match, removing all damage you've taken.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% hits a button on %hisher% wristband and suddenly seems to completely recover. It's like nothing "
				+ "you've done ever happened.";
	}
}
