package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character uses their tail to tease their opponent
 */
public class TailJob extends Skill {
	public TailJob(Character self) {
		super("Tailjob", self);
		addTag(Attribute.Animism);
		addTag(SkillTag.TAIL);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Animism) >= 6;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.has(Trait.tailed)
				&& target.pantsless()
				&& c.stance.mobile(self)
				&& !c.stance.mobile(target)
				&& !c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "Use your tail to tease your opponent";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.pleasure(((self.getEffective(Attribute.Animism) * self.getArousal().percent()) / 100)
						+ Global.random(target.getEffective(Attribute.Perception)),
				Anatomy.genitals,
				c);
	}

	@Override
	public Skill copy(Character user) {
		return new TailJob(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You skillfully use your flexible tail to stroke and tease %target%'s sensitive parts.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% teases your sensitive dick and balls with %hisher% soft tail. It wraps completely around your "
				+ "shaft and strokes it firmly.";
	}
}
