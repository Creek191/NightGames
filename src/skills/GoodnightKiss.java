package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import global.Global;
import items.Flask;

/**
 * Defines a skill where a character delivers a knockout drug to an opponent by kissing them
 */
public class GoodnightKiss extends Skill {
	public GoodnightKiss(Character self) {
		super("Goodnight Kiss", self);
		this.speed = 7;
		addTag(Attribute.Ninjutsu);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ninjutsu) >= 18;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(20)
				&& self.has(Flask.Sedative)
				&& c.stance.kiss(self);
	}

	@Override
	public String describe() {
		return "Deliver a powerful knockout drug via a kiss: 20 Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(20);
		self.consume(Flask.Sedative, 1);
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		target.tempt(Global.random(4), c);
		target.weaken(target.getStamina().get(), c);
		target.getStamina().empty();
	}

	@Override
	public Skill copy(Character user) {
		return new GoodnightKiss(user);
	}

	@Override
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You surreptitiously coat your lips with a powerful sedative, careful not to accidentally ingest any. As "
				+ "soon as you see an opening, you dart in and kiss %target% softly. Only a small amount of the drug is "
				+ "actually transferred by the kiss, but it's enough. %HeShe% immediately staggers as the strength leaves "
				+ "%hisher% body.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% lunges toward you and kisses you energetically. You respond immediately, but before you can deepen "
				+ "the kiss, %heshe% dodges back with a wink. A second later, you realize something is very wrong. A "
				+ "chill spreads through your body as your strength drains away. %self% quickly wipes the remaining drug "
				+ "from %hisher% lips with a mischievous grin.";
	}
}
