package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;

/**
 * Defines a skill where a character fondles their opponent's breasts
 */
public class FondleBreasts extends Skill {
	public FondleBreasts(Character self) {
		super("Fondle Breasts", self);
		this.speed = 6;
		this.accuracy = 7;
		addTag(Attribute.Seduction);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.hasBreasts()
				&& c.stance.reachTop(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			if (target.topless()) {
				var damage = Global.random(3 + self.getEffective(Attribute.Seduction) / 2) + target.getEffective(
						Attribute.Perception) / 2;
				damage = self.bonusProficiency(Anatomy.fingers, damage);
				if (self.human()) {
					c.write(self, formattedDeal(damage, Result.normal, target));
				}
				target.pleasure(damage, Anatomy.chest, c);
				if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Angel")) {
					c.offerImage("Breast Fondle_nude.jpg", "Art by AimlessArt");
				}
				c.offerImage("Fondle Breasts.jpg", "Art by AimlessArt");
			}
			else {
				var damage = Math.max(Global.random(2 + self.getEffective(Attribute.Seduction) / 2) - target.top.size() + 2,
						1);
				damage = self.bonusProficiency(Anatomy.fingers, damage);
				if (self.human()) {
					c.write(self, formattedDeal(damage, Result.normal, target));
				}
				target.pleasure(damage, Anatomy.chest, Result.foreplay, c);
				if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Angel")) {
					c.offerImage("Breast Fondle_cloth.jpg", "Art by AimlessArt");
				}
			}
			self.buildMojo(10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new FondleBreasts(user);
	}

	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			if (Global.random(2) == 0) {
				return "You reach towards %target%'s chest, but come up short.";
			}
			else {
				return "You grope at %target%'s breasts, but miss.";
			}
		}
		else if (target.top.isEmpty()) {
			switch (Global.random(3)) {
				case 2:
					return "You take your time and gently caress both of %target%'s tender breasts with your fingers. "
							+ "You occasionally brush %hisher% nipple to make %himher% coo.";
				case 1:
					return "You massage %target%'s soft breasts and pinch %hisher% nipples, causing %himher% to moan with desire.";
				default:
					return "You slowly tease %target%'s exposed chest, running your hands all over %hisher% boobs.";
			}
		}
		else {
			switch (Global.random(3)) {
				case 2:
					return "You play with %target%'s breasts, trying to find %hisher% nipples through %hisher% " + target.top.peek().getName() + ".";
				case 1:
					return "You tease %target%'s boobs through %hisher% " + target.top.peek().getName() + ".";
				default:
					return "You massage %target%'s breasts over %hisher% " + target.top.peek().getName() + ".";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character attacker) {
		return null;
	}

	@Override
	public String describe() {
		return "Grope your opponent's breasts. More effective if she's topless";
	}
}
