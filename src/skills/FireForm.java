package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.FireStance;
import status.Stsflag;

/**
 * Defines a skill where a character boosts their mojo at the cost of stamina regeneration
 */
public class FireForm extends Skill {
	public FireForm(Character self) {
		super("Fire Form", self);
		addTag(Attribute.Ki);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Ki) >= 15;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.is(Stsflag.form)
				&& !c.stance.sub(self);
	}

	@Override
	public String describe() {
		return "Boost Mojo gain at the expense of Stamina regeneration.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new FireStance(self), c);
	}

	@Override
	public Skill copy(Character user) {
		return new FireForm(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You let your ki burn, wearing down your body, but enhancing your spirit.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% powers up and you can almost feel the energy radiating from %himher%.";
	}
}
