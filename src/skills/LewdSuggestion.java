package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import status.Horny;
import status.Stsflag;

/**
 * Defines a skill where a character uses hypnosis to plant erotic suggestions in their opponent
 */
public class LewdSuggestion extends Skill {
	public LewdSuggestion(Character self) {
		super("Lewd Suggestion", self);
		addTag(Attribute.Hypnosis);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Hypnosis) >= 3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& target.is(Stsflag.charmed)
				&& c.stance.mobile(self)
				&& !c.stance.sub(self)
				&& !c.stance.behind(self)
				&& !c.stance.behind(target);
	}

	@Override
	public String describe() {
		return "Plant an erotic suggestion in your hypnotized target.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		var damage = 1;
		if (target.is(Stsflag.horny)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.strong, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.strong, target));
			}
			damage += target.getStatus(Stsflag.horny).mag();
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
		}
		target.add(new Horny(target, damage, 4), c);
		target.emote(Emotion.horny, 30);
	}

	@Override
	public Skill copy(Character user) {
		return new LewdSuggestion(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.strong) {
			return "You take advantage of the erotic fantasies already swirling through %target%'s head, whispering ideas "
					+ "that fan the flame of %hisher% lust.";
		}
		else {
			return "You plant an erotic suggestion in %target%'s mind, distracting %himher% with lewd fantasies.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.strong) {
			return "%self% whispers a lewd suggestion to you, intensifying the fantasies you were trying to ignore and "
					+ "enflaming your arousal.";
		}
		else {
			return "%self% gives you a hypnotic suggestion and your head is immediately filled with erotic possibilities.";
		}
	}
}
