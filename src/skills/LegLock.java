package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Abuff;

/**
 * Defines a skill where a character puts their opponent in a leg lock
 */
public class LegLock extends Skill {
	public LegLock(Character self) {
		super("Leg Lock", self);
		this.speed = 2;
		addTag(Attribute.Power);
		addTag(SkillTag.PAINFUL);
		addTag(SkillTag.CRIPPLING);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.dom(self)
				&& c.stance.reachBottom(self)
				&& c.stance.prone(target)
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (c.attackRoll(this, self, target)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			target.add(new Abuff(target, Attribute.Speed, -2, 5), c);
			target.pain(Global.random(10) + 7, Anatomy.leg, c);
			target.emote(Emotion.angry, 25);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power) >= 24 && !user.has(Trait.cursed);
	}

	@Override
	public Skill copy(Character user) {
		return new LegLock(user);
	}

	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You grab %target%'s leg, but %heshe% kicks free.";
		}
		else {
			return "You take hold of %target%'s ankle and force %hisher% leg to extend painfully.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% tries to put you in a leglock, but you slip away.";
		}
		else {
			return "%self% pulls your leg across %himher% in a painful submission hold.";
		}
	}

	@Override
	public String describe() {
		return "A submission hold on your opponent's leg";
	}
}
