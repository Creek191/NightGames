package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character stomps on their opponents groin
 */
public class Stomp extends Skill {
	public Stomp(Character self) {
		super("Stomp", self);
		this.speed = 4;
		addTag(Attribute.Power);
		addTag(SkillTag.PAINFUL);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(20)
				&& !self.has(Trait.softheart)
				&& !self.has(Trait.sportsmanship)
				&& c.stance.feet(self)
				&& !c.stance.penetration(self)
				&& !c.stance.prone(self)
				&& c.stance.prone(target);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.has(Trait.heeldrop) && target.pantsless()) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.special, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.special, target));
				if (Global.random(5) >= 1) {
					c.write(self, self.bbLiner());
				}
			}
			target.pain(30 - (Global.random(2) * target.bottom.size()), Anatomy.genitals, c);
			target.calm(Global.random(30), c);
		}
		else if (target.has(Trait.armored)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.weak, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.weak, target));
			}
			target.pain(20 - (Global.random(3) * target.bottom.size()), Anatomy.genitals, c);
			target.calm(Global.random(10) + 10, c);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
				if (Global.random(5) >= 1) {
					c.write(self, self.bbLiner());
				}
			}
			target.pain(20 - (Global.random(3) * target.bottom.size()), Anatomy.genitals, c);
			target.calm(Global.random(30) + 10, c);
		}
		target.emote(Emotion.angry, 25);
		self.spendMojo(20);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power) >= 16 && !user.has(Trait.softheart);
	}

	@Override
	public Skill copy(Character user) {
		return new Stomp(user);
	}

	public Tactics type() {
		return Tactics.damage;
	}

	public String toString() {
		if (self.has(Trait.heeldrop)) {
			return "Double Legdrop";
		}
		else {
			return name;
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			if (target.hasBalls()) {
				return "You push %target%'s legs apart, exposing %hisher% private parts. %HisHer% cock is hanging over "
						+ "%hisher% testicles, so you prod it with your foot to push it out of the way. %HeShe% becomes "
						+ "slightly aroused at your touch and your attention, not realizing you're planning something "
						+ "extremely painful. You're going to feel a bit bad about this, but probably not nearly as much "
						+ "as %heshe% will. You jump up, lifting both legs and coming down in a double legdrop directly "
						+ "hitting %hisher% unprotected jewels.";
			}
			else {
				return "You push %target%'s legs apart, fully exposing %hisher% womanhood. %HeShe% flushes in shame, "
						+ "apparently misunderstanding you intentions, which compels you to mutter a quick apology before "
						+ "you jump up and slam your heel into %hisher% vulnerable quim.";
			}
		}
		else if (modifier == Result.weak) {
			return "You step between %target%'s legs and stomp down on %hisher% groin. Your foot hits something solid "
					+ "and %heshe% doesn't seem terribly affected.";
		}
		else {
			if (target.hasBalls()) {
				return "You pull %target%'s legs open and stomp on %hisher% vulnerable balls. %HeShe% cries out in pain "
						+ "and curls up in the fetal position.";
			}
			else {
				return "You step between %target%'s legs and stomp down on %hisher% sensitive pussy. %HeShe% cries out "
						+ "in pain and rubs %hisher% injured girl bits.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.special) {
			return "%self% forces your legs open and begins prodding your genitals with %hisher% foot. You're slightly "
					+ "aroused by %hisher% attention, but %heshe%'s not giving you a proper footjob, %heshe%'s mostly "
					+ "just playing with your balls. Too late, you realize that %heshe%'s actually lining up %hisher% "
					+ "targets. Two torrents of pain erupt from your delicates as %hisher% feet crash down on them.";
		}
		else if (modifier == Result.weak) {
			return "%self% grabs your ankles and stomps down on your armored groin, doing little damage.";
		}
		else {
			return "%self% grabs your ankles and stomps down on your unprotected jewels. You curl up in the fetal "
					+ "position, groaning in agony.";
		}
	}

	@Override
	public String describe() {
		return "Stomps on your opponent's groin for extreme damage: 20 mojo";
	}
}
