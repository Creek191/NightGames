package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character pleasures their opponent with their breasts
 */
public class Paizuri extends Skill {

	public Paizuri(Character self) {
		super("Use Breasts", self);
		this.speed = 4;
		addTag(Attribute.Seduction);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.hasBreasts()
				&& self.topless()
				&& target.hasDick()
				&& target.pantsless()
				&& c.stance.reachBottom(self)
				&& !c.stance.behind(self)
				&& !c.stance.behind(target)
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
			c.offerImage("Paizuri.jpg", "Art by AimlessArt");
		}
		if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
			c.offerImage("Paizuri.jpg", "Art by AimlessArt");
		}
		var damage = Global.random(6) + self.getEffective(Attribute.Seduction) / 2 + target.getEffective(Attribute.Perception) / 2;
		damage += self.getMojo().get() / 10;
		damage = self.bonusProficiency(Anatomy.chest, damage);
		target.pleasure(damage, Anatomy.genitals, c);
		self.buildMojo(25);
		target.emote(Emotion.horny, 10);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction) >= 28 && !user.has(Trait.petite) && user.hasBreasts();
	}

	@Override
	public Skill copy(Character user) {
		return new Paizuri(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You rub %target%'s penis between your breasts.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		switch (Global.random(2)) {
			case 1:
				return "%self% gently wraps %hisher% beautiful breasts around your cock and starts stroking, working each "
						+ "up and down individually.";
			default:
				return "%self% squeezes your dick between %hisher% soft breasts. %HeShe% rubs them up and down your shaft "
						+ "and teasingly licks your tip.";
		}
	}

	@Override
	public String describe() {
		return "Rub your opponent's dick between your boobs, more effective at high Mojo";
	}
}
