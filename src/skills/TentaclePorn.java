package skills;

import characters.Anatomy;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;
import combat.Result;
import global.Global;
import status.Bound;
import status.Oiled;
import status.Stsflag;

/**
 * Defines a skill where a character summons tentacles to play with their opponent
 */
public class TentaclePorn extends Skill {
	public TentaclePorn(Character self) {
		super("Tentacle Porn", self);
		addTag(Attribute.Fetish);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Fetish) >= 12;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(10)
				&& self.getArousal().get() >= 20
				&& !c.stance.sub(self)
				&& !c.stance.prone(self)
				&& !c.stance.prone(target);
	}

	@Override
	public String describe() {
		return "Create a bunch of hentai tentacles.";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(10);
		if (c.effectRoll(this, self, target, self.getEffective(Attribute.Fetish))) {
			if (target.nude()) {
				if (target.bound()) {
					if (self.human()) {
						c.write(self, formattedDeal(0, Result.special, target));
					}
					else if (target.human()) {
						c.write(self, formattedReceive(0, Result.special, target));
					}
					target.pleasure(Global.random(self.getEffective(Attribute.Fetish) + target.getEffective(Attribute.Perception)),
							Anatomy.genitals,
							c);
					target.emote(Emotion.horny, 10);
				}
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.normal, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				if (!target.is(Stsflag.oiled)) {
					target.add(new Oiled(target), c);
				}
				target.pleasure(Global.random(self.getEffective(Attribute.Fetish) / 2), Anatomy.genitals, c);
				target.emote(Emotion.horny, 20);
			}
			else {
				if (self.human()) {
					c.write(self, formattedDeal(0, Result.weak, target));
				}
				else if (target.human()) {
					c.write(self, formattedReceive(0, Result.weak, target));
				}
			}
			target.add(new Bound(target, 10 + self.getEffective(Attribute.Fetish) / 3, "tentacles"));
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new TentaclePorn(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "You summon a mass of tentacles that try to snare %target%, but %heshe% nimbly dodges them.";
		}
		else if (modifier == Result.weak) {
			return "You summon a mass of phallic tentacles that wrap around %target%'s arms, holding %himher% in place.";
		}
		else if (modifier == Result.normal) {
			return "You summon a mass of phallic tentacles that wrap around %target%'s naked body. They squirm against "
					+ "%himher% and squirt slimy fluids on %hisher% body.";
		}
		else {
			return "You summon tentacles to toy with %target%'s helpless form. The tentacles toy with %hisher% breasts "
					+ "and penetrate %hisher% pussy and ass.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% stomps on the ground and a bundle of tentacles erupt from the ground. You're barely able to "
					+ "avoid them.";
		}
		else if (modifier == Result.weak) {
			return "%self% stomps on the ground and a bundle of tentacles erupt from the ground around you, entangling "
					+ "your arms and legs.";
		}
		else if (modifier == Result.normal) {
			return "%self% stomps on the ground and a bundle of tentacles erupt from the ground around you, entangling "
					+ "your arms and legs. The slimy appendages wriggle over your body and coat you in the slippery"
					+ " liquid.";
		}
		else {
			return "%self% summons slimy tentacles that cover your helpless body, tease your dick, and probe your ass.";
		}
	}
}
