package skills;

import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Global;
import stance.Neutral;
import status.Braced;
import status.Stsflag;

/**
 * Defines a skill where a character pulls themself together and recovers some energy
 */
public class Recover extends Skill {
	public Recover(Character self) {
		super("Recover", self);
		this.speed = 0;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& c.stance.mobile(self)
				&& c.stance.prone(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		if (c.stance.prone(self) && !self.is(Stsflag.braced)) {
			self.add(new Braced(self));
		}
		c.stance = new Neutral(self, target);
		if (self.has(Trait.determined)) {
			self.calm(self.getArousal().max() / 4, c);
			self.heal(self.getStamina().max() / 4, c);
		}
		else {
			self.heal(Global.random(3), c);
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Recover(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You pull yourself up, taking a deep breath to restore your focus.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% scrambles back to %hisher% feet.";
	}

	@Override
	public String describe() {
		return "Stand up";
	}
}
