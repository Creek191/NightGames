package skills;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import items.Clothing;
import items.Toy;

/**
 * Defines a skill where a character puts on their strap-on
 */
public class Strapon extends Skill {
	public Strapon(Character self) {
		super("Strap On", self);
		addTag(SkillTag.TOY);
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& !self.hasDick()
				&& self.pantsless()
				&& (self.has(Toy.Strapon) || self.has(Toy.Strapon2))
				&& c.stance.mobile(self)
				&& !c.stance.prone(self)
				&& !c.stance.penetration(self)
				&& !c.stance.penetration(target);
	}

	@Override
	public String describe() {
		return "Put on the strap-on dildo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.wear(Clothing.strapon);
		if (self.has(Toy.Strapon2)) {
			c.drop(self, Toy.Strapon2);
		}
		if (self.has(Toy.Strapon)) {
			c.drop(self, Toy.Strapon);
		}
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.buildMojo(10);
		target.buildMojo(-10);
		target.emote(Emotion.nervous, 10);
		self.emote(Emotion.confident, 30);
		self.emote(Emotion.dominant, 40);
	}

	@Override
	public Skill copy(Character user) {
		return new Strapon(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You put on a strap on dildo.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (Global.checkFlag(Flag.PlayerButtslut)) {
			// Anal virginity related
			if (Global.getValue(Flag.PlayerAssLosses) < 0) {
				return "%self% straps on a thick rubber cock and grins, freezing you in your tracks. You can't help but "
						+ "wonder how it would feel inside of you, %hisher% tits against your back as %heshe% robs you of your "
						+ "anal virginity mid-sexfight... You shake your head to regain focus, and notice %self% smirking "
						+ "slightly and giving you an almost thoughtful look.";
			}
			else if (Global.getValue(Flag.PlayerAssLosses) == 0) {
				return "%self% straps on a thick rubber cock and grins, and you feel your own cock twitch and harden in "
						+ "response. You wonder how it would feel if %heshe% bent you over and made you cum shamefully with "
						+ "%hisher% toy inside you... You shake your head to regain focus, and notice %self% giving you a "
						+ "knowing smirk. <i>\"You know, I don't think it totally counts as taking your anal virginity "
						+ "unless you actually finish. Maybe your cum in my hands could be like a sort of proof of ownership.\"</i> "
						+ "%HeShe% licks one finger as if to illustrate %hisher% point, while eyeing your ass appraisingly. "
						+ "<i>\"Sure you're not tempted to just give it up for me?\"</i>";
			}

			// Submissiveness related
			if (target.getEffective(Attribute.Submissive) > 5 && target.getArousal().percent() > 70 && Global.random(3) == 0) {
				return "%self% straps on a thick rubber cock, and it's all you can do to avoid turning around and letting "
						+ "%himher% take you right there and then. %self% smiles innocently, sending your emotions and "
						+ "arousal into overdrive as your brain processes the fact that such as sweet "
						+ (self.has(Trait.male)
						   ? "boy"
						   : "girl")
						+ " is in all likelihood about to mercilessly assfuck you over the brink of a huge orgasm.";
			}
			else if (Global.getValue(Flag.PlayerAssLosses) > 20 && Global.random(10) == 0) {
				return "%self% straps on a thick rubber cock, and memories force themselves into your head of cumming in "
						+ "half a dozen different positions while a strap-on roughly assaults your prostate. Your dick "
						+ "twitches in anticipation.";
			}

			// Generic Buttslut
			switch (Global.random(4)) {
				case 0:
					return "%self% straps on a thick rubber cock and grins at you in a way that makes you feel a bit "
							+ "nervous.";
				case 1:
					return "%self% sexily wiggles %hisher% hips as %heshe% pulls on a strap-on, giving you a sly wink as "
							+ "%heshe% does so. %HeShe% puts a hand on one hip and strikes a pose, smiling confidently at you.";
				case 2:
					return "%self% equips a fairly weighty rubber strap-on. <i>\"Normally I'd be putting this in one of the "
							+ "other girls, but today I think it's your turn to get pegged.\"</i> %HeShe% gives you an almost "
							+ "predatory smile.";
				case 3:
					return "%self% fixes you with an intense stare as %heshe% straps on a thick rubber cock. A hint of a "
							+ "smile plays about the corners of %hisher% mouth as %heshe% notices your breath quickening "
							+ "at the sight, and %heshe% reaches out to give you a quick squeeze on the behind while you're "
							+ "distracted.";
				default:
					return "%self% puts on a strap-on."; // Should never happen
			}
		}

		// Generic
		switch (Global.random(3)) {
			case 0:
				return "%self% straps on a thick rubber cock and grins at you in a way that makes you feel a bit "
						+ "nervous.";
			case 1:
				return "%self% sexily wiggles %hisher% hips as %heshe% pulls on a strap-on, giving you a sly wink as "
						+ "%heshe% does so. %HeShe% puts a hand on one hip and strikes a pose, smiling confidently at you.";
			case 2:
				return "%self% equips a fairly weighty rubber strap-on. <i>\"Normally I'd be putting this in one of the "
						+ "other girls, but today I think it's your turn to get pegged.\"</i> %HeShe% gives you an almost "
						+ "predatory smile.";
			default:
				return "%self% puts on a strap-on."; // Should never happen
		}
	}
}
