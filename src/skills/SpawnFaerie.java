package skills;

import characters.Attribute;
import characters.Character;
import characters.Trait;
import combat.Combat;
import combat.Result;
import pet.*;

/**
 * Defines a skill where a character summons a faerie familiar
 */
public class SpawnFaerie extends Skill {
	private final Ptype gender;

	public SpawnFaerie(Character self, Ptype gender) {
		super("Summon Faerie", self);
		this.gender = gender;
		addTag(Attribute.Arcane);
		addTag(SkillTag.PET);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Arcane) >= 3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)
				&& self.pet == null
				&& self.canSpend(getSummonCost());
	}

	@Override
	public String describe() {
		return "Summon a Faerie familiar to support you: " + getSummonCost() + " Mojo";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(getSummonCost());
		if (self.getPure(Attribute.Arcane) >= 24) {
			var power = 5 + self.bonusPetPower() + (self.getEffective(Attribute.Arcane) / 10);
			var ac = 8 + self.bonusPetEvasion() + (self.getEffective(Attribute.Arcane) / 10);
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.strong, target));
				if (gender == Ptype.fairyfem) {
					self.pet = new FairyFemRoyal(self, power, ac);
					c.offerImage("Fairy_Royal.png", "Art by AimlessArt");
				}
				else {
					self.pet = new FairyMaleRoyal(self, power, ac);
				}
			}
			else {
				if (target.human()) {
					c.write(self, formattedReceive(0, Result.strong, target));
				}
				self.pet = new FairyFemRoyal(self, power, ac);
				c.offerImage("Fairy_Royal.png", "Art by AimlessArt");
			}
		}
		else {
			var power = 2 + self.bonusPetPower() + (self.getEffective(Attribute.Arcane) / 10);
			var ac = 4 + self.bonusPetEvasion() + (self.getEffective(Attribute.Arcane) / 10);
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
				if (gender == Ptype.fairyfem) {
					self.pet = new FairyFem(self, power, ac);
					c.offerImage("Fairy.png", "Art by AimlessArt");
				}
				else {
					self.pet = new FairyMale(self, power, ac);
				}
			}
			else {
				if (target.human()) {
					c.write(self, formattedReceive(0, Result.normal, target));
				}
				self.pet = new FairyFem(self, power, ac);
				c.offerImage("Fairy.png", "Art by AimlessArt");
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new SpawnFaerie(user, gender);
	}

	@Override
	public Tactics type() {
		return Tactics.summoning;
	}

	public String toString() {
		if (self.getPure(Attribute.Arcane) >= 24) {
			if (gender == Ptype.fairyfem) {
				return "Faerie Princess";
			}
			else {
				return "Faerie Prince";
			}
		}
		else {
			if (gender == Ptype.fairyfem) {
				return "Faerie (female)";
			}
			else {
				return "Faerie (male)";
			}
		}
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.strong) {
			if (gender == Ptype.fairyfem) {
				return "You start a summoning chant and in your mind, seek out a familiar. A beautiful faerie girl "
						+ "appears in front of you. Despite her nakedness, she wears exquisitely crafted jewelry and "
						+ "hair decorations. She flies around you gracefully before landing delicately on your shoulder.";
			}
			else {
				return "You start a summoning chant and in your mind, seek out a familiar. A six inch tall faerie prince "
						+ "winks into existence in response to your invitation. His tiny crown glows with powerful magic "
						+ "as he hovers in the air on dragonfly wings.";
			}
		}
		else {
			if (gender == Ptype.fairyfem) {
				return "You start a summoning chant and in your mind, seek out a familiar. A pretty little faerie girl "
						+ "appears in front of you and gives you a friendly wave before landing softly on your shoulder.";
			}
			else {
				return "You start a summoning chant and in your mind, seek out a familiar. A six inch tall faerie boy "
						+ "winks into existence in response to your call. The faerie hovers in the air on dragonfly wings.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% casts a spell as %heshe% extends %hisher% hand. In a flash of magic, a small, naked girl with "
				+ "butterfly wings appears in %hisher% palm.";
	}

	@Override
	public boolean equals(Object other) {
		return this.getClass() == other.getClass() &&
				this.toString().equals(other.toString());
	}

	private int getSummonCost() {
		return self.has(Trait.faefriend) ? 10 : 15;
	}
}
