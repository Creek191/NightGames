package skills;

import characters.Attribute;
import characters.Character;
import combat.Combat;
import combat.Result;
import status.BD;
import status.Stsflag;

/**
 * Defines a skill where both characters are aroused by bondage for a while
 */
public class Bondage extends Skill {

	public Bondage(Character self) {
		super("Bondage", self);
		addTag(Attribute.Fetish);
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Fetish) >= 3;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canActNormally(c)
				&& self.getArousal().get() >= 5
				&& !self.is(Stsflag.bondage);
	}

	@Override
	public String describe() {
		return "You and your opponent become aroused by being tied up for five turns: Arousal at least 5";
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.human()) {
			c.write(self, formattedDeal(0, Result.normal, target));
		}
		else if (target.human()) {
			c.write(self, formattedReceive(0, Result.normal, target));
		}
		self.add(new BD(self), c);
		target.add(new BD(target), c);
	}

	@Override
	public Skill copy(Character user) {
		return new Bondage(user);
	}

	@Override
	public Tactics type() {
		return Tactics.status;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You imagine the exhilarating feeling of ropes digging into your skin and binding you. You push this "
				+ "feeling into %target%'s libido.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return "%self% flushes and wraps %hisher% arms around %himher%self tightly. Suddenly the thought of being tied "
				+ "up and dominated slips into your head.";
	}
}
