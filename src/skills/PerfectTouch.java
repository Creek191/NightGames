package skills;

import characters.Character;
import characters.*;
import combat.Combat;
import combat.Result;
import global.Global;

/**
 * Defines a skill where a character uses their cunning to completely strip their opponent
 */
public class PerfectTouch extends Skill {
	public PerfectTouch(Character self) {
		super("Sleight of Hand", self);
		this.accuracy = 1;
		this.speed = 2;
		addTag(Attribute.Cunning);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()
				&& self.canSpend(40)
				&& !target.nude()
				&& c.stance.mobile(self)
				&& !c.stance.prone(self)
				&& !c.stance.penetration(self);
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(40);
		if (target.has(Trait.reflexes)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.unique, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.unique, target));
			}
			self.pain(5, Anatomy.head);
			self.emote(Emotion.nervous, 10);
			target.emote(Emotion.dominant, 15);
		}
		else if (c.effectRoll(this, self, target, self.getEffective(Attribute.Cunning) / 4)) {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.normal, target));
				c.write(target, target.nakedLiner());
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.normal, target));
			}
			target.undress(c);
			target.emote(Emotion.nervous, 10);
		}
		else {
			if (self.human()) {
				c.write(self, formattedDeal(0, Result.miss, target));
			}
			else if (target.human()) {
				c.write(self, formattedReceive(0, Result.miss, target));
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return user.has(Trait.amateurMagician);
	}

	@Override
	public Skill copy(Character user) {
		return new PerfectTouch(user);
	}

	@Override
	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if (modifier == Result.unique) {
			return "You feint to the left while your right hand grabs %target%'s clothes. Before you can remove anything, "
					+ "%heshe% hits you across the face with a lightning-fast backfist. You stumble back, as your head "
					+ "rings from the impact.";
		}
		if (modifier == Result.miss) {
			return "You try to steal %target%'s clothes off of %himher%, but %heshe% catches you.";
		}
		else {
			switch (Global.random(3)) {
				case 2:
					return "In a flurry of movement, you strip each piece of %target%'s clothing off. By the time %heshe% "
							+ "can block your hands, %heshe%'s already naked.";
				case 1:
					return "You divert %target%'s attention with a small show of footwork. While %heshe%'s distracted, "
							+ "you effortlessly strip each piece of %hisher% clothing off before %heshe% notices what's "
							+ "happening.";
				default:
					return "You feint to the left while your right hand makes quick work of %target%'s clothes. By the "
							+ "time %heshe% realizes what's happening, you've already stripped all %hisher% clothes off.";
			}
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if (modifier == Result.miss) {
			return "%self% lunges toward you, but you catch %hisher% hands before %heshe% can get ahold of your clothes.";
		}
		else {
			return "%self% lunges towards you, but dodges away without hitting you. %HeShe% tosses aside a handful of "
					+ "clothes, at which point you realize you're naked. How the hell did %heshe% manage that?";
		}
	}

	@Override
	public String describe() {
		return "Strips opponent completely: 40 Mojo";
	}
}
