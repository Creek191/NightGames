package skills;

import characters.Attribute;
import characters.Character;
import characters.ID;
import characters.Trait;
import combat.Combat;
import combat.Result;
import global.Flag;
import global.Global;
import stance.*;
import status.Bound;
import status.Stsflag;

/**
 * Defines a skill where a character struggles against their bindings or opponent
 */
public class Struggle extends Skill {

	public Struggle(Character self) {
		super("Struggle", self);
		this.speed = 0;
		addTag(SkillTag.ESCAPE);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return !self.stunned()
				&& !self.distracted()
				&& !self.is(Stsflag.enthralled)
				&& ((!c.stance.mobile(self) && !c.stance.dom(self)) || self.bound());
	}

	@Override
	public void resolve(Combat c, Character target) {
		if (self.bound()) {
			var boundStatus = (Bound) target.getStatus(Stsflag.bound);
			if (self.check(Attribute.Power, 10 - self.escapeModifier())) {
				if (self.human()) {
					if (boundStatus != null) {
						c.write(self, "You manage to break free from the " + boundStatus + ".");
					}
					else {
						c.write(self, "You manage to snap the restraints that are binding your hands.");
					}
				}
				else if (target.human()) {
					if (boundStatus != null) {
						c.write(self, self.name() + " slips free from the " + boundStatus + ".");
					}
					else {
						c.write(self, self.name() + " breaks free.");
					}
				}
				self.free();
			}
			else {
				if (self.human()) {
					if (boundStatus != null) {
						c.write(self, "You struggle against the " + boundStatus + ", but can't get free.");
					}
					else {
						c.write(self, "You struggle against your restraints, but can't get free.");
					}
				}
				else if (target.human()) {
					if (boundStatus != null) {
						c.write(self, self.name() + " struggles against the " + boundStatus + ", but can't free "
								+ self.possessive(false) + " hands.");
					}
					else {
						c.write(self, self.name() + " struggles, but can't free " + self.possessive(false) + " hands.");
					}
				}
			}
		}
		else if (c.stance.penetration(self)) {
			if (c.stance.enumerate() == Stance.anal) {
				var escapeDC = c.stance.escapeDC(target, self)
						+ (target.getStamina().get() / 2 - self.getStamina().get() / 2)
						+ (target.getEffective(Attribute.Power) - self.getEffective(Attribute.Power));
				if (self.check(Attribute.Power, escapeDC)) {
					if (self.human()) {
						c.write(self, "You manage to break away from " + target.name() + ".");
					}
					else if (target.human()) {
						c.write(self, self.name() + " pulls away from you and your dick slides out of "
								+ self.possessive(false) + " butt.");
					}
					c.stance.insert(target);
				}
				else {
					if (self.human()) {
						c.write(self, "You try to pull free, but " + target.name() + " has a good grip on your waist.");
					}
					else if (target.human()) {
						c.write(self, self.name() + " tries to squirm away, but you have better leverage.");
					}
				}
			}
			else {
				var escapeDC = c.stance.escapeDC(target, self)
						+ (target.getStamina().get() / 4 - self.getStamina().get() / 4)
						+ (target.getEffective(Attribute.Power) / 2 - Math.max(self.getEffective(Attribute.Power),
						self.getEffective(Attribute.Cunning)) / 2);
				if (self.check(Attribute.Power, escapeDC)) {
					if (self.human()) {

						if (c.stance.en == Stance.flying) {
							if (self.getPure(Attribute.Dark) >= 18) {
								c.stance = new Flying(self, target);
								c.write(self, "You struggle with " + target.name() + " until "
										+ target.pronounSubject(false) + " is unable to keep the two of you in the "
										+ "air. You start to plummet rapidly to the ground. Fortunately you can fly too. "
										+ "You summon your own wings and snatch " + target.name() + " out of the air, "
										+ "thrusting your cock back into " + target.pronounTarget(false) + ". Now "
										+ "you're in control.");
							}
							else if (self.getPure(Attribute.Ninjutsu) >= 5) {
								c.stance = new Neutral(self, target);
								c.write(self, "You pry yourself free from your captor and drop to the ground. "
										+ "The drop would be a problem for someone without ninja training, but you mange "
										+ "to roll safely and hop back to your feet.");
							}
							else {
								c.write(self, "You manage to shake yourself loose from the demoness.\n"
										+ "Immediatly afterwards you realize letting go of the person holding you a good "
										+ "distance up from the ground may not have been the smartest move you've ever "
										+ "made, as the ground is quickly approaching your face.");
								c.stance = c.stance.insert(self);
							}
						}
						else if (c.stance.en == Stance.standing) {
							c.write(self, "You grab " + target.name() + "'s ass and hold "
									+ target.pronounTarget(false) + " up, taking control of the pace.");
							c.stance = new Standing(self, target);
						}
						else if (c.stance.behind(self)) {
							c.write(self, "You manage to unbalance " + target.name() + " and push "
									+ target.pronounTarget(false) + " forward onto " + target.possessive(false)
									+ " hands and knees. You follow " + target.pronounTarget(false) + ", still "
									+ "inside " + target.possessive(false) + " tight wetness, and continue to fuck "
									+ target.pronounTarget(false) + " from behind.");
							if (!Global.checkFlag(Flag.exactimages) || target.name().startsWith("Kat")) {
								c.offerImage("Doggy Style.jpg", "Art by AimlessArt");
							}
							c.stance = new Doggy(self, target);
						}

						else {
							c.write(self, "You surprise " + target.name() + " by hugging " + target.pronounTarget(false)
									+ " close to your chest, preventing " + target.pronounTarget(false) + " from "
									+ "stabilizing her position with her arms. You roll on top of "
									+ target.pronounTarget(false) + " into traditional missionary position, careful "
									+ "not to let your cock slip out of " + target.pronounTarget(false) + ".");
							if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.ANGEL) {
								c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri");
							}
							c.stance = new Missionary(self, target);
						}
					}
					else if (target.human()) {
						if (c.stance.en == Stance.standing) {
							c.write(self, self.name() + " wraps " + self.possessive(false) + " legs around your "
									+ "waist and manages to take control of your fucking.");
							c.stance = new Standing(self, target);
						}
						else if (c.stance.prone(self)) {
							c.write(self, self.name() + " wraps " + self.possessive(false) + " legs around your "
									+ "waist and suddenly pulls you into a deep kiss. You're so surprised by this sneak "
									+ "attack that you don't even notice " + self.pronounTarget(false) + " roll you "
									+ "onto your back until you feel her weight on your hips. "
									+ self.pronounSubject(true) + " moves " + self.possessive(false) + " hips "
									+ "experimentally, enjoying the control " + self.pronounSubject(false) + " has "
									+ "in cowgirl position.");
							if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.CASSIE) {
								c.offerImage("Cowgirl.jpg", "Art by AimlessArt");
							}
							c.stance = new Cowgirl(self, target);
						}
						else if (c.stance.en == Stance.flying) {
							if (self.getPure(Attribute.Dark) >= 18) {
								c.stance = new Flying(self, target);
								c.write(self, self.name() + " spreads " + self.possessive(false) + " own wings "
										+ "and pulls you into a nose-dive with a strong flap. While you're busy frantically "
										+ "trying to recover, " + self.pronounSubject(false) + " secures her grip on you. "
										+ self.pronounSubject(true) + " catches you before you hit the ground and "
										+ "pulls you back into the air.");
							}
							else if (self.getPure(Attribute.Ninjutsu) >= 5) {
								c.stance = new Neutral(self, target);
								c.write(self, self.name() + " shakes " + self.pronounTarget(false) + "self out "
										+ "of your arms and off your dick. " + self.pronounSubject(true) + " drops "
										+ "to the ground, but lands gracefully, thanks to " + self.possessive(false)
										+ " extensive training.");
							}
							else {
								c.write(self,
										self.name() + " shakes " + self.pronounTarget(false) + "self out of your "
												+ "arms and off your dick. " + self.pronounSubject(true) + " drops "
												+ "to the ground, landing hard. You descend rapidly and confirm "
												+ self.pronounSubject(false) + "'s not seriously hurt. Fortunately "
												+ self.pronounSubject(false) + " seems fine, and also fortunately, "
												+ "you have the drop on " + self.pronounTarget(false) + ".");
								c.stance = c.stance.insert(self);
							}
						}
						else {
							c.write(self, self.name() + " manages to reach between " + self.possessive(false)
									+ " legs and grab hold of your ballsack, stopping you in mid thrust. "
									+ self.pronounSubject(true) + " smirks at you over " + self.possessive(false)
									+ " shoulder and pushes " + self.possessive(false) + " butt against you, using the "
									+ "leverage of your testicles to keep you from backing away to maintain your balance. "
									+ self.pronounSubject(true) + " forces you onto your back, while never breaking "
									+ "your connection. after some complex maneuvering, you end up on the floor while "
									+ self.pronounSubject(false) + " straddles your hips in a reverse cowgirl position.");
							c.stance = new ReverseCowgirl(self, target);
						}
					}
					else {
						if (self.hasDick() || self.has(Trait.strapped)) {
							if (c.stance.behind(self)) {
								c.stance = new Doggy(self, target);
								if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.KAT) {
									c.offerImage("Doggy Style.jpg", "Art by AimlessArt");
								}
								if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.MARA) {
									c.offerImage("Doggy Style Mara.jpg", "Art by AimlessArt");
								}
							}
							else if (c.stance.en == Stance.flying) {
								c.stance = c.stance.insert(self);
							}
							else {
								if (!Global.checkFlag(Flag.exactimages) || target.id() == ID.ANGEL) {
									c.offerImage("Fuck.jpg", "Art by Fujin Hitokiri");
									c.offerImage("Angel_Sex.jpg", "Art by AimlessArt");
								}
								c.stance = new Missionary(self, target);
							}
						}
						else {
							if (c.stance.prone(self)) {
								if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.CASSIE) {
									c.offerImage("Cowgirl.jpg", "Art by AimlessArt");
								}
								if (!Global.checkFlag(Flag.exactimages) || self.id() == ID.SAMANTHA) {
									c.offerImage("Samantha Cowgirl.jpg", "Art by AimlessArt");
								}
								c.stance = new Cowgirl(self, target);
							}
							else {
								c.stance = new ReverseCowgirl(self, target);
							}
						}
					}
				}
				else {
					if (self.human()) {
						c.write(self, "You try to tip " + target.name() + " off balance, but "
								+ target.pronounSubject(false) + " drops " + target.possessive(false) + " hips "
								+ "firmly, pushing your cock deep inside " + target.pronounTarget(false) + " and "
								+ "pinning you to the floor.");
					}
					else if (target.human()) {
						if (c.stance.behind(target)) {
							c.write(self, self.name() + " struggles to gain a more dominant position, but with you "
									+ "behind " + self.pronounTarget(false) + ", holding " + self.possessive(false)
									+ " waist firmly, there is nothing " + self.pronounSubject(false) + " can do.");
						}
						else {
							c.write(self, self.name() + " tries to roll on top of you, but you use you superior "
									+ "upper body strength to maintain your position.");
						}
					}
					c.stance.decay();
				}
			}
		}
		else {
			var escapeDC = c.stance.escapeDC(target, self)
					+ (target.getStamina().get() / 2 - self.getStamina().get() / 2)
					+ (target.getEffective(Attribute.Power) - self.getEffective(Attribute.Power));
			if (self.check(Attribute.Power, escapeDC)) {
				if (self.human()) {
					c.write(self, "You manage to scramble out of " + target.name() + "'s grip.");
				}
				else if (target.human()) {
					c.write(self, self.name() + " squirms out from under you.");
				}
				if (c.stance.prone(self)) {
					c.stance = new StandingOver(target, self);
				}
				c.stance = new Neutral(self, target);
			}
			else {
				if (self.human()) {
					c.write(self, "You try to free yourself from " + target.name() + "'s grasp, but "
							+ self.pronounSubject(false) + " has you pinned too well.");
				}
				else if (target.human()) {
					c.write(self, self.name() + " struggles against you, but you maintain your position.");
				}
				c.stance.struggle(c);
				c.stance.decay();
			}
		}
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public Skill copy(Character user) {
		return new Struggle(user);
	}

	public Tactics type() {
		return Tactics.positioning;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String describe() {
		return "Attempt to escape a submissive position using Power";
	}
}
