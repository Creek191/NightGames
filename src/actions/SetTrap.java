package actions;

import characters.Character;
import global.Global;
import global.Modifier;
import global.Scheduler;
import trap.Trap;

public class SetTrap extends Action {
	private final Trap trap;

	public SetTrap(Trap trap) {
		super("Set(" + trap.toString() + ")");
		this.trap = trap;
	}

	@Override
	public boolean usable(Character user) {
		return trap.recipe(user)
				&& trap.requirements(user)
				&& !user.location().open()
				&& user.location().env.size() < 5
				&& (!user.human() || Scheduler.getMatch().condition != Modifier.noitems);
	}

	@Override
	public Movement execute(Character user) {
		var copy = trap.place(user, user.location());
		var message = copy.setup();
		if (user.human()) {
			Global.gui().message(message);
		}

		return Movement.trap;
	}

	@Override
	public Movement consider() {
		return Movement.trap;
	}
}
