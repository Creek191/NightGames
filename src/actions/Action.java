package actions;

import characters.Character;

import java.io.Serializable;

public abstract class Action implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 4981682001213276175L;
	protected String name;
	protected String tooltip;

	public Action(String name) {
		this(name, "");
	}

	public Action(String name, String tooltip) {
		this.name = name;
		this.tooltip = tooltip;
	}

	/**
	 * Checks whether the specified character can perform this action
	 *
	 * @param user The character to check
	 * @return True if the character can perform this action, otherwise False
	 */
	public abstract boolean usable(Character user);

	/**
	 * Makes the specified character perform this action
	 *
	 * @param user The character performing the action
	 * @return The Movement type of this action
	 */
	public abstract Movement execute(Character user);

	public String toString() {
		return name;
	}

	/**
	 * Gets the type of movement involved in this action
	 */
	public abstract Movement consider();

	/**
	 * Checks whether the action is free (takes no time)
	 *
	 * @return True if the action takes no time, otherwise False
	 */
	public boolean freeAction() {
		return false;
	}

	public String tooltip() {
		return this.tooltip;
	}
}
