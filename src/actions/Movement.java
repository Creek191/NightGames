package actions;

import java.awt.*;

/**
 * Defines all possible Movement types
 */
public enum Movement {
	quad(" head outside, toward the quad.", " moved to the quad ", new Color(250, 250, 250)),
	kitchen(" move into the kitchen.", " moved to the kitchen ", new Color(250, 250, 250)),
	dorm(" move to the first floor of the dorm.", " moved to the dorm ", new Color(250, 250, 250)),
	shower(" run into the showers.", " moved into the showers ", new Color(250, 250, 250)),
	storage(" enter the storage room.", " moved to the storage room ", new Color(250, 250, 250)),
	dining(" head to the dining hall.", " moved to the dining hall ", new Color(250, 250, 250)),
	laundry(" move to the laundry room.", " moved to the laundry room ", new Color(250, 250, 250)),
	tunnel(" move into the tunnel.", " moved to the tunnel ", new Color(250, 250, 250)),
	bridge(" move to the bridge.", " moved to the bridge ", new Color(250, 250, 250)),
	engineering(" head to the first floor of the engineering building.",
			" moved to the engineering building ",
			new Color(250, 250, 250)),
	workshop(" enter a workshop.", " moved to the workshop ", new Color(250, 250, 250)),
	lab(" enter one of the chemistry labs.", " moved to the lab ", new Color(250, 250, 250)),
	la(" move to the liberal arts building.", " moved to the liberal arts building ", new Color(250, 250, 250)),
	library(" enter the library.", " moved to the library ", new Color(250, 250, 250)),
	pool(" move to the indoor pool.", " went into the pool ", new Color(250, 250, 250)),
	union(" head toward the student union.", " moved to the student union ", new Color(250, 250, 250)),
	hide(" disappear into a hiding place.", " spent time hiding ", new Color(200, 200, 200)),
	trap(" start rigging up something weird, probably a trap.", " rigged a trap here ", new Color(120, 200, 120)),
	bathe(" start bathing in the nude, not bothered by your presence.", " got cleaned up ", new Color(170, 200, 250)),
	scavenge(" begin scrounging through some boxes in the corner.", " rummaged around here ", new Color(220, 220, 150)),
	craft(" start mixing various liquids. Whatever it is doesn't look healthy.",
			" brewed something ",
			new Color(220, 220, 150)),
	wait(" loitering nearby", " loitered here ", new Color(200, 200, 200)),
	resupply(" heads for one of the safe rooms, probably to get a change of clothes.",
			" got dressed ",
			new Color(170, 200, 250)),
	oil(" rubbing body oil on her every inch of her skin. Wow, you wouldn't mind watching that again.",
			" spilled some body oil ",
			new Color(100, 250, 250)),
	energydrink(" opening an energy drink and downing the whole thing.",
			" left an energy drink can here ",
			new Color(100, 250, 250)),
	beer(" opening a beer and downing the whole thing.", " dropped a beer can here ", new Color(100, 250, 250)),
	recharge(" plugging a battery pack into a nearby charging station.",
			" used the power supply ",
			new Color(170, 200, 250)),
	locating(" is holding someone's underwear in her hands and breathing deeply. Strange.",
			" burned something ",
			new Color(170, 200, 250)),
	masturbate(" starts to pleasure herself, while trying not to make much noise. It's quite a show.",
			" touched herself here, leaving some drops of love juice ",
			new Color(250, 170, 170)),
	mana(" doing something with a large book. When she's finished, you can see a sort of aura coming from her.",
			" looked through these thick texts ",
			new Color(170, 200, 250)),
	retire(" has left the match.", " exited the match area ", new Color(250, 100, 100)),
	potion(" drink an unidentifiable potion.", " dropped an empty plastic container here ", new Color(100, 250, 250)),
	fight(" has a fight.", " fought here ", Color.WHITE);

	private final String desc;
	private final String track;
	private final Color color;

	/**
	 * Gets the description of the Movement
	 */
	public String describe() {
		return desc;
	}

	/**
	 * Gets the color used in the message log
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Gets the description used when tracking another character
	 */
	public String traceDesc() {
		return track;
	}

	private Movement(String desc, String track, Color buttoncolor) {
		this.desc = desc;
		this.track = track;
		this.color = buttoncolor;
	}
}
