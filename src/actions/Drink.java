package actions;

import characters.Character;
import global.Global;
import global.Modifier;
import global.Scheduler;
import items.Potion;

public class Drink extends Action {
	private final Potion item;

	public Drink(Potion item) {
		super(item.getName(), item.getDesc());
		this.item = item;
	}

	@Override
	public boolean usable(Character user) {
		return user.has(item) && (!user.human() || Scheduler.getMatch().condition != Modifier.noitems);
	}

	@Override
	public Movement execute(Character user) {
		if (user.human()) {
			Global.gui().message(item.getMessage(user));
		}
		user.add(item.getEffect(user));
		user.consume(item, 1);
		return item.getAction();
	}

	@Override
	public Movement consider() {
		return item.getAction();
	}
}
