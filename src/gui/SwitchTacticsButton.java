package gui;

import global.Global;
import skills.TacticGroup;
import skills.Tactics;

import javax.swing.border.LineBorder;
import java.awt.*;

/**
 * Defines a button used to switch to a different tactics group
 */
public class SwitchTacticsButton extends KeyableButton {
    public SwitchTacticsButton(TacticGroup group) {
        super(group.name());
        setBorderPainted(false);
        setOpaque(true);
        var bgColor = new Color(80, 220, 120);
        for (var tactic : Tactics.values()) {
            if (tactic.getGroup() == group) {
                bgColor = tactic.getColor();
                break;
            }
        }

        setBackground(bgColor);
        setMinimumSize(new Dimension(0, 20));
        setForeground(foregroundColor(bgColor));
        setFont(new Font("Georgia", 0, 16));
        setBorder(new LineBorder(getBackground(), 3));

        var nSkills = Global.gui().nSkillsForGroup(group);
        setText(group.name() + " [" + nSkills + "]");
        if (nSkills == 0 && group != TacticGroup.All) {
            setEnabled(false);
            setForeground(Color.WHITE);
            setBackground(getBackground().darker());
        }

        addActionListener(arg0 -> Global.gui().switchTactics(group));
    }

    private static Color foregroundColor(Color bgColor) {
        var hsb = new float[3];
        Color.RGBtoHSB(bgColor.getRed(), bgColor.getGreen(), bgColor.getRed(), hsb);
        if (hsb[2] < .6) {
            return Color.WHITE;
        }
        else {
            return Color.BLACK;
        }
    }
}