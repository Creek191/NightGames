package gui;

import javax.swing.*;

/**
 * Defines a button that shows its assigned hotkey
 */
public abstract class KeyableButton extends JButton {
	private final String text;

	public KeyableButton(String text) {
		super(text);
		this.text = text;
	}

	/**
	 * Triggers the button click
	 */
	public void call() {
		doClick();
	}

	/**
	 * Sets the displayed hotkey to the specified text
	 *
	 * @param string The hotkey assigned to the key
	 */
	public void setHotkeyText(String string) {
		setText(String.format("%s [%s]", text, string));
	}
}