package gui;

import characters.Character;
import combat.Encounter;
import global.Global;

import java.awt.*;

/**
 * Defines a button used to start a scene where the player intervenes in a fight
 */
public class InterveneButton extends KeyableButton {
	public InterveneButton(Encounter enc, Character assist) {
		super("Help " + assist.name());
		setFont(new Font("Georgia", Font.PLAIN, 18));
		addActionListener(arg0 -> enc.intrude(Global.getPlayer(), assist));
	}
}
