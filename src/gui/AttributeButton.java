package gui;

import characters.Attribute;
import global.Global;

import java.awt.*;

/**
 * Defines a button that increases the player's attribute
 */
public class AttributeButton extends KeyableButton {
	public AttributeButton(Attribute att) {
		super(att.toString());
		setFont(new Font("Georgia", Font.PLAIN, 18));
		setToolTipText(att.getDescription());
		addActionListener(arg0 -> {
			Global.getPlayer().mod(att, 1);
			Global.getPlayer().attpoints -= 1;
			Global.gui().ding();
		});
	}
}
