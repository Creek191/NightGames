package gui;

import characters.Attribute;
import characters.Player;
import characters.Trait;
import global.Constants;
import global.Flag;
import global.Global;

import javax.swing.*;
import java.awt.*;
import java.util.Hashtable;

/**
 * Defines the character creation GUI
 */
public class CreationGUI extends JPanel {
	private final JTextField powerfield;
	private final JTextField seductionfield;
	private final JTextField cunningfield;
	private final JTextField attPoints;
	private final JTextField namefield;
	private final int perception;
	private int stamina;
	private int arousal;
	private int mojo;
	private int speed;
	private int powerpoints;
	private int seductionpoints;
	private int cunningpoints;
	private int remaining;
	private int money;
	private float startScale;
	private float xpScale;
	private float npcStartScale;
	private float npcXpScale;
	private final JButton btnPowMin;
	private final JButton btnPowPlus;
	private final JButton btnSedMin;
	private final JButton btnSedPlus;
	private final JButton btnCunMin;
	private final JButton btnCunPlus;
	private final JTextPane textPane;
	private final JScrollPane scrollPane;
	private final JSeparator separator_1;
	private final Box verticalBox;
	private final JLabel lblStrength;
	private final JComboBox<Trait> StrengthBox;
	private final JSeparator separator_2;
	private final JLabel lblWeakness;
	private final JComboBox<Trait> WeaknessBox;
	private final Color textColor;
	private final Color frameColor;
	private final Color backgroundColor;
	private final JPanel charPanel;
	private final JPanel bioPanel;
	private final JLabel lblSpeed;
	private final JTextField speedField;
	private final JLabel lblPerception;
	private final JTextField perceptionField;
	private final JButton btnReset;
	private final JLabel lblMale;
	private final JLabel lblStaminaMax;
	private final JTextField lblStaminaValue;
	private final JLabel lblArousalMax;
	private final JTextField lblArousalValue;
	private final JLabel lblMojoMax;
	private final JTextField lblMojoValue;
	private final JCheckBox chckbxSkipTutorial;
	private final JLabel lblModifiers;
	private final JCheckBox chckbxChallengeMode;
	private final JCheckBox chckbxShortMatches;
	private final JCheckBox chckbxGentle;
	private final JSlider playerStart;
	private final JSlider playerScale;
	private final JSlider npcStart;
	private final JSlider npcScale;
	private final JLabel dispplayerstart;
	private final JLabel dispplayerscale;
	private final JLabel dispnpcstart;
	private final JLabel dispnpcscale;
	private final JPanel panel_1;
	private final JPanel miscPanel;
	private final JLabel lblCreation;
	private JTextPane StrengthDescription;
	private JTextPane WeaknessDescription;

	public CreationGUI(gui.GUI window) {
		textColor = Constants.PRIMARYTEXTCOLOR;
		frameColor = Constants.PRIMARYFRAMECOLOR;
		backgroundColor = Constants.PRIMARYBGCOLOR;
		setBackground(frameColor);
		setForeground(textColor);
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		// Set starting values
		startScale = 1f;
		xpScale = 1f;
		npcStartScale = 1f;
		npcXpScale = 1f;
		stamina = Math.round(Constants.STARTINGSTAMINA * startScale);
		arousal = Math.round(Constants.STARTINGAROUSAL * startScale);
		mojo = Math.round(Constants.STARTINGMOJO * startScale);
		speed = Math.round(Constants.STARTINGSPEED * startScale);
		perception = Constants.STARTINGPERCEPTION;
		money = Math.round(Constants.STARTINGCASH * startScale);
		powerpoints = 0;
		seductionpoints = 0;
		cunningpoints = 0;
		remaining = Constants.STARTINGSTATPOINTS;

		scrollPane = new JScrollPane();
		add(scrollPane);
		scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		textPane.setForeground(SystemColor.textText);
		textPane.setBackground(SystemColor.inactiveCaptionBorder);
		textPane.setFont(new Font("Georgia", Font.PLAIN, 18));
		textPane.setEditable(false);
		textPane.setText(Global.getIntro());

		var creationPanel = new JPanel();
		creationPanel.setBackground(frameColor);
		creationPanel.setForeground(textColor);

		add(creationPanel);
		creationPanel.setLayout(new BorderLayout(0, 0));

		charPanel = new JPanel();
		creationPanel.add(charPanel, BorderLayout.CENTER);
		charPanel.setLayout(new BoxLayout(charPanel, BoxLayout.X_AXIS));

		bioPanel = new JPanel();
		charPanel.add(bioPanel);
		bioPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		bioPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		bioPanel.setBackground(backgroundColor);
		var gbl_bioPanel = new GridBagLayout();
		gbl_bioPanel.columnWidths = new int[]{84, 50, 60, 42, 0};
		gbl_bioPanel.rowHeights = new int[]{32, 32, 32, 32, 32, 32, 32, 32, 0, 0, 0};
		gbl_bioPanel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		gbl_bioPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		bioPanel.setLayout(gbl_bioPanel);

		var lblName = new JLabel("Name");
		var gbc_lblName = new GridBagConstraints();
		gbc_lblName.fill = GridBagConstraints.BOTH;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		bioPanel.add(lblName, gbc_lblName);
		lblName.setForeground(textColor);
		lblName.setFont(new Font("Georgia", Font.PLAIN, 18));

		namefield = new JTextField();
		var gbc_namefield = new GridBagConstraints();
		gbc_namefield.gridwidth = 3;
		gbc_namefield.fill = GridBagConstraints.BOTH;
		gbc_namefield.insets = new Insets(0, 0, 5, 5);
		gbc_namefield.gridx = 1;
		gbc_namefield.gridy = 0;
		bioPanel.add(namefield, gbc_namefield);
		namefield.setFont(new Font("Georgia", Font.PLAIN, 18));
		namefield.setColumns(10);
		namefield.setForeground(textColor);

		lblMale = new JLabel("Male");
		var gbc_lblMale = new GridBagConstraints();
		gbc_lblMale.fill = GridBagConstraints.BOTH;
		gbc_lblMale.insets = new Insets(0, 0, 5, 5);
		gbc_lblMale.gridx = 0;
		gbc_lblMale.gridy = 1;
		lblMale.setFont(new Font("Georgia", Font.PLAIN, 18));
		lblMale.setForeground(textColor);
		bioPanel.add(lblMale, gbc_lblMale);

		lblStaminaMax = new JLabel("Stamina Max");
		var gbc_lblStaminaMax = new GridBagConstraints();
		gbc_lblStaminaMax.fill = GridBagConstraints.BOTH;
		gbc_lblStaminaMax.insets = new Insets(0, 0, 5, 0);
		gbc_lblStaminaMax.gridx = 0;
		gbc_lblStaminaMax.gridy = 2;
		lblStaminaMax.setFont(new Font("Georgia", Font.PLAIN, 18));
		lblStaminaMax.setForeground(textColor);
		bioPanel.add(lblStaminaMax, gbc_lblStaminaMax);

		lblStaminaValue = new JTextField("35");
		lblStaminaValue.setFont(new Font("Georgia", Font.PLAIN, 18));
		lblStaminaValue.setEditable(false);
		lblStaminaValue.setBackground(backgroundColor);
		lblStaminaValue.setForeground(textColor);
		var gbc_lblStaminaValue = new GridBagConstraints();
		gbc_lblStaminaValue.fill = GridBagConstraints.BOTH;
		gbc_lblStaminaValue.insets = new Insets(0, 0, 5, 5);
		gbc_lblStaminaValue.gridx = 1;
		gbc_lblStaminaValue.gridy = 2;
		bioPanel.add(lblStaminaValue, gbc_lblStaminaValue);

		lblArousalMax = new JLabel("Arousal Max");
		var gbc_lblArousalMax = new GridBagConstraints();
		gbc_lblArousalMax.fill = GridBagConstraints.BOTH;
		gbc_lblArousalMax.insets = new Insets(0, 0, 5, 5);
		gbc_lblArousalMax.gridx = 0;
		gbc_lblArousalMax.gridy = 3;
		lblArousalMax.setFont(new Font("Georgia", Font.PLAIN, 18));
		lblArousalMax.setForeground(textColor);
		bioPanel.add(lblArousalMax, gbc_lblArousalMax);

		lblArousalValue = new JTextField("50");
		lblArousalValue.setFont(new Font("Georgia", Font.PLAIN, 18));
		lblArousalValue.setEditable(false);
		lblArousalValue.setBackground(backgroundColor);
		lblArousalValue.setForeground(textColor);
		var gbc_lblArousalValue = new GridBagConstraints();
		gbc_lblArousalValue.fill = GridBagConstraints.BOTH;
		gbc_lblArousalValue.insets = new Insets(0, 0, 5, 5);
		gbc_lblArousalValue.gridx = 1;
		gbc_lblArousalValue.gridy = 3;
		bioPanel.add(lblArousalValue, gbc_lblArousalValue);

		lblMojoMax = new JLabel("Mojo Max");
		lblMojoMax.setForeground(textColor);
		var gbc_lblMojoMax = new GridBagConstraints();
		gbc_lblMojoMax.fill = GridBagConstraints.BOTH;
		gbc_lblMojoMax.insets = new Insets(0, 0, 5, 0);
		gbc_lblMojoMax.gridx = 0;
		gbc_lblMojoMax.gridy = 4;
		lblMojoMax.setFont(new Font("Georgia", Font.PLAIN, 18));
		bioPanel.add(lblMojoMax, gbc_lblMojoMax);

		lblMojoValue = new JTextField("30");
		lblMojoValue.setFont(new Font("Georgia", Font.PLAIN, 18));
		lblMojoValue.setEditable(false);
		lblMojoValue.setForeground(textColor);
		lblMojoValue.setBackground(backgroundColor);
		var gbc_lblMojoValue = new GridBagConstraints();
		gbc_lblMojoValue.fill = GridBagConstraints.BOTH;
		gbc_lblMojoValue.insets = new Insets(0, 0, 5, 5);
		gbc_lblMojoValue.gridx = 1;
		gbc_lblMojoValue.gridy = 4;
		bioPanel.add(lblMojoValue, gbc_lblMojoValue);

		var lblPower = new JLabel("Power");
		var gbc_lblPower = new GridBagConstraints();
		gbc_lblPower.fill = GridBagConstraints.BOTH;
		gbc_lblPower.insets = new Insets(0, 0, 5, 5);
		gbc_lblPower.gridx = 0;
		gbc_lblPower.gridy = 5;
		bioPanel.add(lblPower, gbc_lblPower);
		lblPower.setForeground(textColor);
		lblPower.setFont(new Font("Georgia", Font.PLAIN, 18));

		powerfield = new JTextField();
		powerfield.setEditable(false);
		var gbc_powerfield = new GridBagConstraints();
		gbc_powerfield.fill = GridBagConstraints.BOTH;
		gbc_powerfield.insets = new Insets(0, 0, 5, 5);
		gbc_powerfield.gridx = 1;
		gbc_powerfield.gridy = 5;
		bioPanel.add(powerfield, gbc_powerfield);
		powerfield.setBackground(backgroundColor);
		powerfield.setForeground(textColor);
		powerfield.setFont(new Font("Georgia", Font.BOLD, 18));

		btnPowPlus = new JButton("+");
		var gbc_btnPowPlus = new GridBagConstraints();
		gbc_btnPowPlus.fill = GridBagConstraints.BOTH;
		gbc_btnPowPlus.insets = new Insets(0, 0, 5, 0);
		gbc_btnPowPlus.gridx = 2;
		gbc_btnPowPlus.gridy = 5;
		bioPanel.add(btnPowPlus, gbc_btnPowPlus);
		btnPowPlus.setFont(new Font("Georgia", Font.PLAIN, 18));
		btnPowPlus.addActionListener(arg0 -> {
			powerpoints++;
			remaining--;
			refresh();
		});

		btnPowMin = new JButton("-");
		var gbc_btnPowMin = new GridBagConstraints();
		gbc_btnPowMin.fill = GridBagConstraints.BOTH;
		gbc_btnPowMin.insets = new Insets(0, 0, 5, 5);
		gbc_btnPowMin.gridx = 3;
		gbc_btnPowMin.gridy = 5;
		bioPanel.add(btnPowMin, gbc_btnPowMin);
		btnPowMin.setFont(new Font("Georgia", Font.PLAIN, 18));
		btnPowMin.addActionListener(arg0 -> {
			powerpoints--;
			remaining++;
			refresh();
		});

		var lblSeduction = new JLabel("Seduction");
		var gbc_lblSeduction = new GridBagConstraints();
		gbc_lblSeduction.fill = GridBagConstraints.BOTH;
		gbc_lblSeduction.insets = new Insets(0, 0, 5, 5);
		gbc_lblSeduction.gridx = 0;
		gbc_lblSeduction.gridy = 6;
		bioPanel.add(lblSeduction, gbc_lblSeduction);
		lblSeduction.setForeground(textColor);
		lblSeduction.setFont(new Font("Georgia", Font.PLAIN, 18));

		seductionfield = new JTextField();
		var gbc_seductionfield = new GridBagConstraints();
		gbc_seductionfield.fill = GridBagConstraints.BOTH;
		gbc_seductionfield.insets = new Insets(0, 0, 5, 5);
		gbc_seductionfield.gridx = 1;
		gbc_seductionfield.gridy = 6;
		bioPanel.add(seductionfield, gbc_seductionfield);
		seductionfield.setBackground(backgroundColor);
		seductionfield.setForeground(textColor);
		seductionfield.setFont(new Font("Georgia", Font.BOLD, 18));
		seductionfield.setEditable(false);
		seductionfield.setColumns(2);

		btnSedPlus = new JButton("+");
		var gbc_btnSedPlus = new GridBagConstraints();
		gbc_btnSedPlus.fill = GridBagConstraints.BOTH;
		gbc_btnSedPlus.insets = new Insets(0, 0, 5, 0);
		gbc_btnSedPlus.gridx = 2;
		gbc_btnSedPlus.gridy = 6;
		bioPanel.add(btnSedPlus, gbc_btnSedPlus);
		btnSedPlus.setFont(new Font("Georgia", Font.PLAIN, 18));
		btnSedPlus.addActionListener(arg0 -> {
			seductionpoints++;
			remaining--;
			refresh();
		});

		btnSedMin = new JButton("-");
		var gbc_btnSedMin = new GridBagConstraints();
		gbc_btnSedMin.fill = GridBagConstraints.BOTH;
		gbc_btnSedMin.insets = new Insets(0, 0, 5, 5);
		gbc_btnSedMin.gridx = 3;
		gbc_btnSedMin.gridy = 6;
		bioPanel.add(btnSedMin, gbc_btnSedMin);
		btnSedMin.setFont(new Font("Georgia", Font.PLAIN, 18));
		btnSedMin.addActionListener(arg0 -> {
			seductionpoints--;
			remaining++;
			refresh();
		});

		var lblCunning = new JLabel("Cunning");
		var gbc_lblCunning = new GridBagConstraints();
		gbc_lblCunning.fill = GridBagConstraints.BOTH;
		gbc_lblCunning.insets = new Insets(0, 0, 5, 5);
		gbc_lblCunning.gridx = 0;
		gbc_lblCunning.gridy = 7;
		bioPanel.add(lblCunning, gbc_lblCunning);
		lblCunning.setForeground(textColor);
		lblCunning.setFont(new Font("Georgia", Font.PLAIN, 18));

		cunningfield = new JTextField();
		var gbc_cunningfield = new GridBagConstraints();
		gbc_cunningfield.fill = GridBagConstraints.BOTH;
		gbc_cunningfield.insets = new Insets(0, 0, 5, 5);
		gbc_cunningfield.gridx = 1;
		gbc_cunningfield.gridy = 7;
		bioPanel.add(cunningfield, gbc_cunningfield);
		cunningfield.setBackground(backgroundColor);
		cunningfield.setForeground(textColor);
		cunningfield.setFont(new Font("Georgia", Font.BOLD, 18));
		cunningfield.setEditable(false);
		cunningfield.setColumns(2);

		btnCunPlus = new JButton("+");
		var gbc_btnCunPlus = new GridBagConstraints();
		gbc_btnCunPlus.fill = GridBagConstraints.BOTH;
		gbc_btnCunPlus.insets = new Insets(0, 0, 5, 0);
		gbc_btnCunPlus.gridx = 2;
		gbc_btnCunPlus.gridy = 7;
		bioPanel.add(btnCunPlus, gbc_btnCunPlus);
		btnCunPlus.setFont(new Font("Georgia", Font.PLAIN, 18));
		btnCunPlus.addActionListener(arg0 -> {
			cunningpoints++;
			remaining--;
			refresh();
		});

		btnCunMin = new JButton("-");
		var gbc_btnCunMin = new GridBagConstraints();
		gbc_btnCunMin.fill = GridBagConstraints.BOTH;
		gbc_btnCunMin.insets = new Insets(0, 0, 5, 5);
		gbc_btnCunMin.gridx = 3;
		gbc_btnCunMin.gridy = 7;
		bioPanel.add(btnCunMin, gbc_btnCunMin);
		btnCunMin.setFont(new Font("Georgia", Font.PLAIN, 18));
		btnCunMin.addActionListener(arg0 -> {
			cunningpoints--;
			remaining++;
			refresh();
		});

		lblSpeed = new JLabel("Speed");
		var gbc_lblSpeed = new GridBagConstraints();
		gbc_lblSpeed.fill = GridBagConstraints.BOTH;
		gbc_lblSpeed.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpeed.gridx = 0;
		gbc_lblSpeed.gridy = 8;
		lblSpeed.setFont(new Font("Georgia", Font.PLAIN, 18));
		lblSpeed.setForeground(textColor);
		bioPanel.add(lblSpeed, gbc_lblSpeed);

		speedField = new JTextField();
		speedField.setEditable(false);
		var gbc_speedField = new GridBagConstraints();
		gbc_speedField.fill = GridBagConstraints.BOTH;
		gbc_speedField.insets = new Insets(0, 0, 5, 5);
		gbc_speedField.gridx = 1;
		gbc_speedField.gridy = 8;
		speedField.setFont(new Font("Georgia", Font.PLAIN, 18));
		speedField.setBackground(backgroundColor);
		speedField.setForeground(textColor);
		bioPanel.add(speedField, gbc_speedField);
		speedField.setColumns(2);

		lblPerception = new JLabel("Perception");
		var gbc_lblPerception = new GridBagConstraints();
		gbc_lblPerception.fill = GridBagConstraints.BOTH;
		gbc_lblPerception.insets = new Insets(0, 0, 5, 0);
		gbc_lblPerception.gridx = 0;
		gbc_lblPerception.gridy = 9;
		lblPerception.setFont(new Font("Georgia", Font.PLAIN, 18));
		lblPerception.setForeground(textColor);
		bioPanel.add(lblPerception, gbc_lblPerception);

		perceptionField = new JTextField();
		perceptionField.setEditable(false);
		var gbc_perceptionField = new GridBagConstraints();
		gbc_perceptionField.fill = GridBagConstraints.BOTH;
		gbc_perceptionField.insets = new Insets(0, 0, 0, 5);
		gbc_perceptionField.gridx = 1;
		gbc_perceptionField.gridy = 9;
		perceptionField.setFont(new Font("Georgia", Font.PLAIN, 18));
		perceptionField.setBackground(backgroundColor);
		perceptionField.setForeground(textColor);
		bioPanel.add(perceptionField, gbc_perceptionField);
		perceptionField.setColumns(2);

		var lblAttributePoints = new JLabel("Remaining");
		var gbc_lblAttributePoints = new GridBagConstraints();
		gbc_lblAttributePoints.fill = GridBagConstraints.BOTH;
		gbc_lblAttributePoints.insets = new Insets(0, 0, 0, 5);
		gbc_lblAttributePoints.gridx = 0;
		gbc_lblAttributePoints.gridy = 10;
		bioPanel.add(lblAttributePoints, gbc_lblAttributePoints);
		lblAttributePoints.setForeground(textColor);
		lblAttributePoints.setFont(new Font("Georgia", Font.BOLD, 18));

		attPoints = new JTextField();
		var gbc_attPoints = new GridBagConstraints();
		gbc_attPoints.fill = GridBagConstraints.BOTH;
		gbc_attPoints.insets = new Insets(0, 0, 0, 5);
		gbc_attPoints.gridx = 1;
		gbc_attPoints.gridy = 10;
		bioPanel.add(attPoints, gbc_attPoints);
		attPoints.setForeground(textColor);
		attPoints.setBackground(backgroundColor);
		attPoints.setFont(new Font("Georgia", Font.BOLD, 18));
		attPoints.setEditable(false);
		attPoints.setColumns(2);

		btnReset = new JButton("Reset");
		var gbc_btnReset = new GridBagConstraints();
		gbc_btnReset.gridwidth = 2;
		gbc_btnReset.fill = GridBagConstraints.BOTH;
		gbc_btnReset.gridx = 2;
		gbc_btnReset.gridy = 10;
		bioPanel.add(btnReset, gbc_btnReset);
		btnReset.addActionListener(arg0 -> reset());

		verticalBox = Box.createVerticalBox();
		verticalBox.setAlignmentY(Component.TOP_ALIGNMENT);
		verticalBox.setBackground(frameColor);
		charPanel.add(verticalBox);

		lblStrength = new JLabel("Strength");
		lblStrength.setHorizontalAlignment(SwingConstants.CENTER);
		lblStrength.setFont(new Font("Georgia", Font.BOLD, 18));
		lblStrength.setForeground(textColor);
		lblStrength.setBackground(backgroundColor);
		verticalBox.add(lblStrength);

		StrengthBox = new JComboBox<>();
		StrengthBox.setBackground(frameColor);
		StrengthBox.setForeground(textColor);
		StrengthBox.addItem(Trait.romantic);
		StrengthBox.addItem(Trait.dexterous);
		StrengthBox.addItem(Trait.experienced);
		StrengthBox.addItem(Trait.wrassler);
		StrengthBox.addItem(Trait.streaker);
		StrengthBox.addItem(Trait.pimphand);
		StrengthBox.addItem(Trait.brassballs);
		StrengthBox.addItem(Trait.bramaster);
		StrengthBox.addItem(Trait.pantymaster);
		StrengthBox.addItem(Trait.toymaster);
		StrengthBox.addActionListener(arg0 -> StrengthDescription.setText(((Trait) StrengthBox.getSelectedItem()).getDesc()));
		verticalBox.add(StrengthBox);

		StrengthDescription = new JTextPane();
		StrengthDescription.setBackground(backgroundColor);
		StrengthDescription.setPreferredSize(new Dimension(100, 100));
		StrengthDescription.setEditable(false);
		StrengthDescription.setFont(new Font("Georgia", Font.PLAIN, 18));
		StrengthDescription.setText(((Trait) StrengthBox.getSelectedItem()).getDesc());
		verticalBox.add(StrengthDescription);

		separator_2 = new JSeparator();
		verticalBox.add(separator_2);

		lblWeakness = new JLabel("Weakness");
		lblWeakness.setHorizontalAlignment(SwingConstants.CENTER);
		lblWeakness.setFont(new Font("Georgia", Font.BOLD, 18));
		lblWeakness.setForeground(textColor);
		lblWeakness.setBackground(backgroundColor);
		verticalBox.add(lblWeakness);

		WeaknessBox = new JComboBox<>();
		WeaknessBox.setBackground(frameColor);
		WeaknessBox.setForeground(textColor);
		WeaknessBox.addItem(Trait.insatiable);
		WeaknessBox.addItem(Trait.imagination);
		WeaknessBox.addItem(Trait.achilles);
		WeaknessBox.addItem(Trait.ticklish);
		WeaknessBox.addItem(Trait.lickable);
		WeaknessBox.addItem(Trait.hairtrigger);
		WeaknessBox.addItem(Trait.buttslut);
		WeaknessBox.addActionListener(arg0 -> WeaknessDescription.setText(((Trait) WeaknessBox.getSelectedItem()).getDesc()));
		verticalBox.add(WeaknessBox);
		WeaknessDescription = new JTextPane();
		WeaknessDescription.setBackground(backgroundColor);
		WeaknessDescription.setPreferredSize(new Dimension(100, 100));
		WeaknessDescription.setEditable(false);
		WeaknessDescription.setFont(new Font("Georgia", Font.PLAIN, 18));
		WeaknessDescription.setText(((Trait) WeaknessBox.getSelectedItem()).getDesc());
		verticalBox.add(WeaknessDescription);

		separator_1 = new JSeparator();
		verticalBox.add(separator_1);

		miscPanel = new JPanel();
		creationPanel.add(miscPanel, BorderLayout.SOUTH);

		var labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put(1, new JLabel("0.25"));
		labelTable.put(4, new JLabel("1"));
		labelTable.put(8, new JLabel("2"));
		labelTable.put(12, new JLabel("3"));

		var lblplayerstart = new JLabel("Player Starting Stats");
		playerStart = new JSlider(JSlider.HORIZONTAL, 1, 12, 1);
		playerStart.setValue(4);
		playerStart.setMinorTickSpacing(1);
		playerStart.setLabelTable(labelTable);
		playerStart.setPaintTicks(true);
		playerStart.setPaintLabels(true);
		playerStart.addChangeListener(e -> {
			var source = (JSlider) e.getSource();
			if (!source.getValueIsAdjusting()) {
				startScale = source.getValue() * .25f;
				refresh();
			}
		});
		dispplayerstart = new JLabel("x" + startScale);

		var lblplayerscale = new JLabel("Player Progression Speed");
		playerScale = new JSlider(JSlider.HORIZONTAL, 1, 12, 1);
		playerScale.setValue(4);
		playerScale.setMinorTickSpacing(1);
		playerScale.setPaintTicks(true);
		playerScale.setLabelTable(labelTable);
		playerScale.setPaintLabels(true);
		playerScale.addChangeListener(e -> {
			var source = (JSlider) e.getSource();
			if (!source.getValueIsAdjusting()) {
				xpScale = source.getValue() * .25f;
				refresh();
			}
		});
		dispplayerscale = new JLabel("x" + xpScale);

		var lblnpcstart = new JLabel("NPC Starting Stats");
		npcStart = new JSlider(JSlider.HORIZONTAL, 1, 12, 1);
		npcStart.setValue(4);
		npcStart.setMinorTickSpacing(1);
		npcStart.setPaintTicks(true);
		npcStart.setLabelTable(labelTable);
		npcStart.setPaintLabels(true);
		npcStart.addChangeListener(e -> {
			var source = (JSlider) e.getSource();
			if (!source.getValueIsAdjusting()) {
				npcStartScale = source.getValue() * .25f;
				refresh();
			}
		});
		dispnpcstart = new JLabel("x" + npcStartScale);

		var lblnpcscale = new JLabel("NPC Progression Speed");
		npcScale = new JSlider(JSlider.HORIZONTAL, 1, 12, 1);
		npcScale.setValue(4);
		npcScale.setMinorTickSpacing(1);
		npcScale.setPaintTicks(true);
		npcScale.setLabelTable(labelTable);
		npcScale.setPaintLabels(true);
		npcScale.addChangeListener(e -> {
			var source = (JSlider) e.getSource();
			if (!source.getValueIsAdjusting()) {
				npcXpScale = source.getValue() * .25f;
				refresh();
			}
		});
		dispnpcscale = new JLabel("x" + npcXpScale);

		var scalePanel = Box.createVerticalBox();
		verticalBox.add(scalePanel);

		scalePanel.add(lblplayerstart);
		var pstartbox = Box.createHorizontalBox();
		pstartbox.add(playerStart);
		pstartbox.add(dispplayerstart);
		scalePanel.add(pstartbox);

		scalePanel.add(lblplayerscale);
		var pscalebox = Box.createHorizontalBox();
		pscalebox.add(playerScale);
		pscalebox.add(dispplayerscale);
		scalePanel.add(pscalebox);

		scalePanel.add(lblnpcstart);
		var npcstartbox = Box.createHorizontalBox();
		npcstartbox.add(npcStart);
		npcstartbox.add(dispnpcstart);
		scalePanel.add(npcstartbox);

		scalePanel.add(lblnpcscale);
		var npcscalebox = Box.createHorizontalBox();
		npcscalebox.add(npcScale);
		npcscalebox.add(dispnpcscale);
		scalePanel.add(npcscalebox);

		var optionsPanel = new JPanel();
		miscPanel.add(optionsPanel);
		optionsPanel.setForeground(textColor);
		optionsPanel.setLayout(new GridLayout(0, 2, 0, 0));

		lblModifiers = new JLabel("Game Modifiers");
		lblModifiers.setFont(new Font("Georgia", Font.BOLD, 18));
		optionsPanel.add(lblModifiers);

		panel_1 = new JPanel();
		optionsPanel.add(panel_1);

		chckbxChallengeMode = new JCheckBox("Challenge Mode");
		chckbxChallengeMode.setToolTipText("Player will be assigned a handicap each match, but only get normal pay");
		chckbxChallengeMode.setFont(new Font("Georgia", Font.PLAIN, 18));
		optionsPanel.add(chckbxChallengeMode);

		chckbxShortMatches = new JCheckBox("Short Matches");
		chckbxShortMatches.setToolTipText(
				"Matches will last 2 hours instead of 3. Good for players who want to play in short session");
		chckbxShortMatches.setFont(new Font("Georgia", Font.PLAIN, 18));
		optionsPanel.add(chckbxShortMatches);

		chckbxSkipTutorial = new JCheckBox("Skip Tutorial");
		chckbxSkipTutorial.setToolTipText("Go directly to first match after character creation");
		chckbxSkipTutorial.setFont(new Font("Georgia", Font.PLAIN, 18));
		optionsPanel.add(chckbxSkipTutorial);

		chckbxGentle = new JCheckBox("Gentle Fights");
		chckbxGentle.setToolTipText("Painful skills are disabled (not balanced)");
		chckbxGentle.setFont(new Font("Georgia", Font.PLAIN, 18));
		optionsPanel.add(chckbxGentle);

		var btnStart = new JButton("Start");
		miscPanel.add(btnStart);
		btnStart.setFont(new Font("Georgia", Font.PLAIN, 27));

		lblCreation = new JLabel("Character Creation");
		lblCreation.setFont(new Font("Georgia", Font.PLAIN, 26));
		creationPanel.add(lblCreation, BorderLayout.NORTH);
		btnStart.addActionListener(arg0 -> {
			if (!this.namefield.getText().isEmpty()) {
				var player = new Player(this.namefield.getText().replaceAll("\\s", ""));
				player.set(Attribute.Power, Math.round(Constants.STARTINGPOWER * startScale) + powerpoints);
				player.set(Attribute.Seduction,
						Math.round(Constants.STARTINGSEDUCTION * startScale) + seductionpoints);
				player.set(Attribute.Cunning, Math.round(Constants.STARTINGCUNNING * startScale) + cunningpoints);
				player.set(Attribute.Speed, speed);
				player.getStamina().setMax(stamina);
				player.getArousal().setMax(arousal);
				player.getMojo().setMax(mojo);
				player.add((Trait) StrengthBox.getSelectedItem());
				player.add((Trait) WeaknessBox.getSelectedItem());
				player.money = money;
				player.rest();
				if (chckbxChallengeMode.isSelected()) {
					Global.flag(Flag.challengemode);
				}
				if (chckbxShortMatches.isSelected()) {
					Global.flag(Flag.shortmatches);
				}
				if (chckbxGentle.isSelected()) {
					Global.flag(Flag.nopain);
				}
				if (WeaknessBox.getSelectedItem() == Trait.buttslut) {
					Global.flag(Flag.PlayerButtslut);
				}
				Global.setCounter(Flag.PlayerBaseStrength, startScale);
				Global.setCounter(Flag.PlayerScaling, xpScale);
				Global.setCounter(Flag.NPCBaseStrength, npcStartScale);
				Global.setCounter(Flag.NPCScaling, npcXpScale);

				Global.newGame(player, !chckbxSkipTutorial.isSelected());
			}
		});

		window.disableOptions();
		window.pack();
		refresh();
	}

	private void refresh() {
		dispplayerstart.setText("x" + startScale);
		dispplayerscale.setText("x" + xpScale);
		dispnpcstart.setText("x" + npcStartScale);
		dispnpcscale.setText("x" + npcXpScale);
		powerfield.setText("" + (Math.round(Constants.STARTINGPOWER * startScale) + powerpoints));
		seductionfield.setText("" + (Math.round(Constants.STARTINGSEDUCTION * startScale) + seductionpoints));
		cunningfield.setText("" + (Math.round(Constants.STARTINGCUNNING * startScale) + cunningpoints));
		stamina = Math.round(Constants.STARTINGSTAMINA * startScale);
		arousal = Math.round(Constants.STARTINGAROUSAL * startScale);
		mojo = Math.round(Constants.STARTINGMOJO * startScale);
		speed = Math.round(Constants.STARTINGSPEED * startScale);
		money = Math.round(Constants.STARTINGCASH * startScale);
		lblStaminaValue.setText("" + stamina);
		lblArousalValue.setText("" + arousal);
		lblMojoValue.setText("" + mojo);
		speedField.setText("" + speed);
		perceptionField.setText("" + perception);

		attPoints.setText("" + remaining);
		if (remaining <= 0) {
			btnPowPlus.setEnabled(false);
			btnSedPlus.setEnabled(false);
			btnCunPlus.setEnabled(false);
		}
		else {
			btnPowPlus.setEnabled(true);
			btnSedPlus.setEnabled(true);
			btnCunPlus.setEnabled(true);
		}
		btnPowMin.setEnabled(powerpoints > 0);
		btnSedMin.setEnabled(seductionpoints > 0);
		btnCunMin.setEnabled(cunningpoints > 0);
	}

	/**
	 * Reset the created character to the default values
	 */
	private void reset() {
		powerpoints = 0;
		seductionpoints = 0;
		cunningpoints = 0;
		remaining = Constants.STARTINGSTATPOINTS;
		refresh();
	}
}
