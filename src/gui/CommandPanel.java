package gui;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import java.awt.*;
import java.util.List;
import java.util.*;

/**
 * Defines a panel that renders buttons and their associated hotkeys
 */
public class CommandPanel extends JPanel {
	private static final List<Character> POSSIBLE_HOTKEYS = Arrays.asList(
			'q', 'w', 'e', 'r', 't', 'y',
			'a', 's', 'd', 'f', 'g', 'h',
			'z', 'x', 'c', 'v', 'b', 'n');
	private static final List<Character> CRAMPED_HOTKEYS = Arrays.asList(
			'q', 'w', 'e', 'r',
			'a', 's', 'd', 'f',
			'z', 'x', 'c', 'v');
	private static final Set<String> DEFAULT_CHOICES = new HashSet<>(Arrays.asList("Wait",
			"Nothing",
			"Next",
			"Leave",
			"Back"));

	private final int perPage;
	private final Map<Character, KeyableButton> hotkeyMapping;
	private final List<KeyableButton> buttons;
	private final JPanel[] rows;
	private int index;
	private int page;

	private int rowLimit = 6;

	public CommandPanel(int width, int height) {
		super();
		this.setBackground(new Color(245, 245, 245));
		if (height > 768) {
			this.setPreferredSize(new Dimension(width, 150));
			this.setMinimumSize(new Dimension(width, 150));
		}
		else {
			this.setPreferredSize(new Dimension(width, 100));
			this.setMinimumSize(new Dimension(width, 100));
		}
		this.setBorder(new CompoundBorder());
		hotkeyMapping = new HashMap<>();
		if (width <= 1024) {
			rowLimit = 4;
		}
		if (height > 768) {
			perPage = 3 * rowLimit;
		}
		else {
			perPage = 2 * rowLimit;
		}
		rows = new JPanel[3];
		rows[0] = new JPanel();
		rows[1] = new JPanel();
		rows[2] = new JPanel();
		for (var row : rows) {
			var layout = new BoxLayout(row, BoxLayout.LINE_AXIS);
			row.setLayout(layout);
			row.setOpaque(false);
			row.setBorder(BorderFactory.createEmptyBorder());
			this.add(row);
		}
		var layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		this.setLayout(layout);
		this.setAlignmentY(Component.TOP_ALIGNMENT);
		this.add(Box.createVerticalGlue());
		this.add(Box.createVerticalStrut(15));
		buttons = new ArrayList<>();
		index = 0;
	}

	/**
	 * Resets the panel
	 */
	public void reset() {
		buttons.clear();
		hotkeyMapping.clear();
		clear();
		refresh();
	}

	/**
	 * Refreshes the panel
	 */
	public void refresh() {
		repaint();
		revalidate();
	}

	/**
	 * Adds the specified button to the panel
	 *
	 * @param button The button to add
	 */
	public void add(KeyableButton button) {
		page = 0;
		buttons.add(button);
		placeOnPanel(button);
	}

	/**
	 * Gets the button registered for the specified hotkey
	 *
	 * @param keyChar The character to get the hotkey for
	 * @return An optional containing the button that was registered to the hotkey, or Null if there is none
	 */
	public Optional<KeyableButton> getButtonForHotkey(char keyChar) {
		return Optional.ofNullable(hotkeyMapping.get(keyChar));
	}

	/**
	 * Registers the specified hotkey/button combination
	 *
	 * @param hotkey The hotkey to register
	 * @param button The button that will be triggered
	 */
	public void register(Character hotkey, KeyableButton button) {
		button.setHotkeyText(hotkey.toString().toUpperCase());
		hotkeyMapping.put(hotkey, button);
	}

	/**
	 * Clears all buttons
	 */
	private void clear() {
		for (var row : rows) {
			row.removeAll();
		}
		if (rowLimit == 4) {
			for (var mapping : CRAMPED_HOTKEYS) {
				hotkeyMapping.remove(mapping);
			}
		}
		else {
			for (var mapping : POSSIBLE_HOTKEYS) {
				hotkeyMapping.remove(mapping);
			}
		}
		index = 0;
	}

	private void placeOnPanel(KeyableButton button) {
		var effectiveIndex = index - page * perPage;
		final var currentPage = page;
		// Button is on current panel page
		if (effectiveIndex >= 0 && effectiveIndex < perPage) {
			var rowIndex = Math.min(rows.length - 1, effectiveIndex / rowLimit);
			var row = rows[rowIndex];
			row.add(button);
			row.add(Box.createHorizontalStrut(8));
			Character hotkey;
			if (rowLimit == 4) {
				hotkey = CRAMPED_HOTKEYS.get(effectiveIndex);
			}
			else {
				hotkey = POSSIBLE_HOTKEYS.get(effectiveIndex);
			}
			register(hotkey, button);
			if (DEFAULT_CHOICES.contains(button.getText()) && !hotkeyMapping.containsKey(' ')) {
				hotkeyMapping.put(' ', button);
			}
		}
		// Button is on previous page
		else if (effectiveIndex == -1) {
			KeyableButton leftPage = new RunnableButton("<<<", () -> setPage(currentPage - 1));
			rows[0].add(leftPage, 0);
			register('~', leftPage);
		}
		// Button is on next page
		else if (effectiveIndex == perPage) {
			KeyableButton rightPage = new RunnableButton(">>>", () -> setPage(currentPage + 1));
			rows[0].add(rightPage);
			register('`', rightPage);
		}
		index += 1;
	}

	private void setPage(int page) {
		this.page = page;
		clear();
		for (var button : buttons) {
			placeOnPanel(button);
		}
		refresh();
	}
}