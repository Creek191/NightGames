package gui;

import global.Scheduler;

import java.awt.*;

/**
 * Defines a button used to go to bed
 */
public class SleepButton extends KeyableButton {
	public SleepButton() {
		super("Go to sleep");
		setFont(new Font("Georgia", Font.PLAIN, 18));
		addActionListener(arg0 -> Scheduler.dawn());
	}
}
