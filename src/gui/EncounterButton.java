package gui;

import characters.Character;
import combat.Encounter;
import combat.Encs;
import global.Scheduler;
import trap.Trap;

import java.awt.*;

/**
 * Defines a button that controls how to react to an encounter
 */
public class EncounterButton extends KeyableButton {
	public EncounterButton(String label, Encounter enc, Character target, Encs choice) {
		super(label);
		setFont(new Font("Baskerville Old Face", 0, 18));
		addActionListener(arg0 -> {
			enc.parse(choice, target);
			Scheduler.unpause();
		});
	}

	public EncounterButton(String label, Encounter enc, Character target, Encs choice, Trap trap) {
		super(label);
		setFont(new Font("Baskerville Old Face", 0, 18));
		addActionListener(arg0 -> {
			enc.parse(choice, target, trap);
			Scheduler.unpause();
		});
	}
}
