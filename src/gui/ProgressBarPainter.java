package gui;

import javax.swing.*;
import java.awt.*;

/**
 * Defines a custom progress bar painter
 */
public class ProgressBarPainter implements Painter<JProgressBar> {
	private final Color color;
	
	public ProgressBarPainter(Color c1) {
		this.color = c1;
	}

	@Override
	public void paint(Graphics2D gd, JProgressBar t, int width, int height) {
		gd.setColor(color);
		gd.fillRect(0, 0, width, height);
	}
}
