package gui;

import characters.Trait;
import global.Global;

import java.awt.*;

/**
 * Defines a button that assigns a feat to the player
 */
public class FeatButton extends KeyableButton {
	public FeatButton(Trait feat) {
		super(feat.toString());
		setFont(new Font("Georgia", Font.PLAIN, 18));
		setToolTipText(feat.getDesc());
		addActionListener(arg0 -> {
			Global.getPlayer().add(feat);
			Global.gainSkills(Global.getPlayer());
			Global.gui().endCombat();
		});
	}
}
