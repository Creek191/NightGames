package gui;

import combat.Combat;
import global.Global;

import java.awt.*;

/**
 * Defines a button that advances to the next combat turn
 */
public class NextTurnButton extends KeyableButton {
	public NextTurnButton(Combat combat) {
		super("Next");
		setFont(new Font("Georgia", Font.PLAIN, 18));
		addActionListener(arg0 -> {
			Global.gui().clearCommand();
			if (combat.phase == 0) {
				combat.clear();
				Global.gui().clearText();
				combat.turn();
			}
			else if (combat.phase == 2) {
				if (!combat.end()) {
					Global.gui().endCombat();
				}
			}
		});
	}
}
