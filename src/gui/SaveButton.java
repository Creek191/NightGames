package gui;

import global.SaveManager;

import java.awt.*;

/**
 * Defines a button used to save the game
 */
public class SaveButton extends KeyableButton {
	public SaveButton() {
		super("Save");
		setFont(new Font("Georgia", 0, 18));
		addActionListener(arg0 -> SaveManager.save(false));
	}
}