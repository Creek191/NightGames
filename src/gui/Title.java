package gui;

import global.Global;
import global.SaveManager;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * Defines the title panel of the game
 */
public class Title extends JPanel {
	private final int width;
	private final int height;
	private final GUI gui;
	private final Image titleImage;

	public Title(GUI gui, int width, int height) {
		this.gui = gui;
		this.width = width;
		this.height = height;
		setBackground(new Color(0, 6, 18));

		titleImage = getTitleImage();

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		var verticalStrut = Box.createVerticalStrut(400);
		add(verticalStrut);

		var horizontalBox = Box.createHorizontalBox();
		add(horizontalBox);

		var btnNewGame = new JButton("New Game");
		btnNewGame.setFont(new Font("Georgia", Font.BOLD, 18));
		horizontalBox.add(btnNewGame);
		btnNewGame.setAlignmentX(Component.CENTER_ALIGNMENT);

		var horizontalStrut = Box.createHorizontalStrut(60);
		horizontalBox.add(horizontalStrut);

		var btnLoad = new JButton("Load");
		btnLoad.setFont(new Font("Georgia", Font.BOLD, 18));
		horizontalBox.add(btnLoad);
		btnLoad.addActionListener(arg0 -> {
			new Global(Title.this.gui);
			SaveManager.load();
			Title.this.setVisible(false);
		});
		btnLoad.setAlignmentX(Component.CENTER_ALIGNMENT);

		var horizontalStrut_1 = Box.createHorizontalStrut(60);
		horizontalBox.add(horizontalStrut_1);

		var btnExit = new JButton("Exit");
		btnExit.setFont(new Font("Georgia", Font.BOLD, 18));
		horizontalBox.add(btnExit);
		btnExit.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnExit.addActionListener(arg0 -> System.exit(0));
		btnNewGame.addActionListener(arg0 -> {
			new Global(Title.this.gui);
			Title.this.setVisible(false);
		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(titleImage, 0, 0, null);
	}

	private Image getTitleImage() {
		Image image = null;

		try {
			var titleurl = getClass().getResource("assets/Title.png");
			if (titleurl != null) {
				image = ImageIO.read(titleurl);
			}
			if (image != null) {
				image = image.getScaledInstance(width, height - 30, Image.SCALE_FAST);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		return image;
	}
}
