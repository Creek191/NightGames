package gui;

import actions.Action;
import characters.Character;
import global.Global;
import global.Scheduler;

import java.awt.*;

/**
 * Defines a button used to perform an action
 */
public class ActionButton extends KeyableButton {
	public ActionButton(Action action, Character user) {
		super(action.toString());
		setFont(new Font("Georgia", Font.PLAIN, 18));
		if (!action.tooltip().isEmpty()) {
			this.setToolTipText(action.tooltip());
		}
		setBackground(action.consider().getColor());
		addActionListener(arg0 -> {
			Global.gui().clearText();
			action.execute(user);
			if (!action.freeAction()) {
				Scheduler.unpause();
			}
		});
	}
}
