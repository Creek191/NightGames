package gui;

import java.awt.*;

/**
 * Defines a button that can run an action in a separate thread
 */
public class RunnableButton extends KeyableButton {
	private final String text;

	public RunnableButton(String text, Runnable runnable) {
		super(formatHTMLMultiline(text, ""));
		this.text = text;
		resetFontSize();

		addActionListener(evt -> runnable.run());
	}

	@Override
	public String getText() {
		return text;
	}

	/**
	 * Sets the displayed hotkey to the specified text
	 *
	 * @param string The hotkey assigned to the key
	 */
	public void setHotkeyText(String string) {
		setText(formatHTMLMultiline(text, String.format(" [%s]", string)));
		resetFontSize();
	}

	private void resetFontSize() {
		if (getText().contains("<br/>")) {
			setFont(new Font("Georgia", 0, 14));
		}
		else {
			setFont(new Font("Georgia", 0, 18));
		}
	}

	private static String formatHTMLMultiline(String original, String hotkeyExtra) {
		// do not word wrap the hotkey extras, since it looks pretty bad.
		return String.format("<html><center>%s%s</center></html>", original, hotkeyExtra);
	}
}