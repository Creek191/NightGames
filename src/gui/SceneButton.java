package gui;

import global.Global;

import java.awt.*;

/**
 * Defines a button used to play a scene
 */
public class SceneButton extends KeyableButton {
	public SceneButton(String label) {
		super(label);
		setFont(new Font("Georgia", 0, 18));
		addActionListener(arg0 -> Global.current.respond(label));
	}

	public SceneButton(String label, String description) {
		this(label);
		this.setToolTipText(description);
	}
}
