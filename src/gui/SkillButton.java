package gui;

import combat.Combat;
import skills.Skill;
import skills.Tactics;

import java.awt.*;

/**
 * Defines a button used to perform a skill during combat
 */
public class SkillButton extends KeyableButton {
	public SkillButton(Skill action, Combat combat) {
		super(action.toString());
		setFont(new Font("Georgia", Font.PLAIN, 18));
		setToolTipText(action.describe());
		addActionListener(arg0 -> combat.act(action.user(), action));

		setButtonStyle(action);
	}

	private void setButtonStyle(Skill action) {
		if (action.type() == Tactics.damage) {
			setBackground(new Color(150, 0, 0));
			setForeground(Color.WHITE);
		}
		else if (action.type() == Tactics.pleasure || action.type() == Tactics.fucking) {
			setBackground(Color.PINK);
		}
		else if (action.type() == Tactics.positioning) {
			setBackground(new Color(0, 100, 0));
			setForeground(Color.WHITE);
		}
		else if (action.type() == Tactics.stripping) {
			setBackground(new Color(0, 100, 0));
			setForeground(Color.WHITE);
		}
		else if (action.type() == Tactics.status) {
			setBackground(Color.CYAN);
		}
		else if (action.type() == Tactics.recovery || action.type() == Tactics.calming) {
			setBackground(Color.WHITE);
		}
		else if (action.type() == Tactics.summoning) {
			setBackground(Color.YELLOW);
		}
		else {
			setBackground(new Color(200, 200, 200));
		}
	}
}
