package gui;

import daytime.Activity;
import items.Clothing;
import items.Item;

import java.awt.*;

/**
 * Defines a button to purchase an item from a shop
 */
public class ItemButton extends EventButton {
	public ItemButton(Activity event, Item i) {
		super(event, i.getName());
		setFont(new Font("Georgia", Font.PLAIN, 18));
		setToolTipText(i.getDesc());
	}

	public ItemButton(Activity event, Clothing i) {
		super(event, i.getProperName());
		setFont(new Font("Georgia", Font.PLAIN, 18));
		setToolTipText(i.getSpecialDescription());
	}
}
