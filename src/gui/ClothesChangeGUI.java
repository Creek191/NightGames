package gui;

import characters.Character;
import daytime.Activity;
import global.Global;
import global.Modifier;
import items.Clothing;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ClothesChangeGUI extends JPanel {
	private final Character player;
	private final JLabel img;
	private final ArrayList<Clothing> TopOut;
	private final ArrayList<Clothing> TopMid;
	private final ArrayList<Clothing> TopIn;
	private final ArrayList<Clothing> BotOut;
	private final ArrayList<Clothing> BotIn;
	private final JComboBox<String> TOBox;
	private final JComboBox<String> TMBox;
	private final JComboBox<String> TIBox;
	private final JComboBox<String> BOBox;
	private final JComboBox<String> BIBox;
	private final HashMap<String, Clothing> translator;
	private Activity resume;
	private boolean freeze;

	public ClothesChangeGUI(Character player) {
		freeze = true;
		this.player = player;
		translator = new HashMap<>();
		for (var article : Clothing.values()) {
			translator.put(article.getFullDesc(), article);
		}
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

		var menuPane = new JPanel();
		var imgPane = new JPanel();
		img = new JLabel();
		imgPane.add(img);
		add(imgPane);
		add(menuPane);

		menuPane.setLayout(new GridLayout(0, 1, 0, 0));

		TopOut = new ArrayList<>();
		TopMid = new ArrayList<>();
		TopIn = new ArrayList<>();
		BotOut = new ArrayList<>();
		BotIn = new ArrayList<>();

		var loadImageAction = (ActionListener) (arg0 -> {
			if (!this.freeze) {
				renderCloset();
			}
		});

		var lblTopOuter = new JLabel("Outerwear");
		lblTopOuter.setHorizontalAlignment(SwingConstants.CENTER);
		lblTopOuter.setFont(new Font("Georgia", Font.PLAIN, 20));
		menuPane.add(lblTopOuter);

		TOBox = new JComboBox<>();
		TOBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		menuPane.add(TOBox);
		TOBox.addItem(Clothing.none.getFullDesc());
		TOBox.setSelectedItem(Clothing.none.getFullDesc());
		TOBox.addActionListener(loadImageAction);

		var lblTop = new JLabel("Tops");
		lblTop.setHorizontalAlignment(SwingConstants.CENTER);
		lblTop.setFont(new Font("Georgia", Font.PLAIN, 20));
		menuPane.add(lblTop);

		TMBox = new JComboBox<>();
		TMBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		menuPane.add(TMBox);
		TMBox.addItem(Clothing.none.getFullDesc());
		TMBox.setSelectedItem(Clothing.none.getFullDesc());
		TMBox.addActionListener(loadImageAction);

		var lblTopInner = new JLabel("Undershirts");
		lblTopInner.setHorizontalAlignment(SwingConstants.CENTER);
		lblTopInner.setFont(new Font("Georgia", Font.PLAIN, 20));
		menuPane.add(lblTopInner);

		TIBox = new JComboBox<>();
		TIBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		menuPane.add(TIBox);
		TIBox.addItem(Clothing.none.getFullDesc());
		TIBox.setSelectedItem(Clothing.none.getFullDesc());
		TIBox.addActionListener(loadImageAction);

		var separator = new JSeparator();
		menuPane.add(separator);

		var lblBottom = new JLabel("Pants");
		lblBottom.setHorizontalAlignment(SwingConstants.CENTER);
		lblBottom.setFont(new Font("Georgia", Font.PLAIN, 20));
		menuPane.add(lblBottom);

		BOBox = new JComboBox<>();
		BOBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		menuPane.add(BOBox);
		BOBox.addItem(Clothing.none.getFullDesc());
		BOBox.setSelectedItem(Clothing.none.getFullDesc());
		BOBox.addActionListener(loadImageAction);

		var lblBottomInner = new JLabel("Underwear");
		lblBottomInner.setHorizontalAlignment(SwingConstants.CENTER);
		lblBottomInner.setFont(new Font("Georgia", Font.PLAIN, 20));
		menuPane.add(lblBottomInner);

		BIBox = new JComboBox<>();
		BIBox.setFont(new Font("Georgia", Font.PLAIN, 22));
		menuPane.add(BIBox);
		BIBox.addActionListener(loadImageAction);

		var horizontalBox_2 = Box.createHorizontalBox();
		menuPane.add(horizontalBox_2);

		var horizontalStrut = Box.createHorizontalStrut(200);
		horizontalBox_2.add(horizontalStrut);

		var btnOk = new JButton("OK");
		btnOk.addActionListener(arg0 -> {
			this.player.outfit[0].clear();
			this.player.outfit[1].clear();
			if (this.TIBox.getSelectedItem() != Clothing.none.getFullDesc()) {
				this.player.outfit[0].push(translator.get((String) this.TIBox.getSelectedItem()));
			}
			if (this.TMBox.getSelectedItem() != Clothing.none.getFullDesc()) {
				this.player.outfit[0].push(translator.get((String) this.TMBox.getSelectedItem()));
			}
			if (this.TOBox.getSelectedItem() != Clothing.none.getFullDesc()) {
				this.player.outfit[0].push(translator.get((String) this.TOBox.getSelectedItem()));
			}
			if (this.BIBox.getSelectedItem() != Clothing.none.getFullDesc()) {
				this.player.outfit[1].push(translator.get((String) this.BIBox.getSelectedItem()));
			}
			if (this.BOBox.getSelectedItem() != Clothing.none.getFullDesc()) {
				this.player.outfit[1].push(translator.get((String) this.BOBox.getSelectedItem()));
			}
			this.player.change(Modifier.normal);
			Global.gui().removeClosetGUI();
			this.resume.visit("Start");
		});
		btnOk.setFont(new Font("Georgia", Font.PLAIN, 24));
		horizontalBox_2.add(btnOk);
		freeze = false;
	}

	/**
	 * Update the clothes GUI with the current state of the player's equipped clothing and closet
	 *
	 * @param event The event to return to after changing clohtes
	 */
	public void update(Activity event) {
		freeze = true;
		this.resume = event;
		TopOut.clear();
		TopMid.clear();
		TopIn.clear();
		BotOut.clear();
		BotIn.clear();
		TOBox.removeAllItems();
		TOBox.addItem(Clothing.none.getFullDesc());
		TOBox.setSelectedItem(Clothing.none.getFullDesc());
		TMBox.removeAllItems();
		TMBox.addItem(Clothing.none.getFullDesc());
		TMBox.setSelectedItem(Clothing.none.getFullDesc());
		TIBox.removeAllItems();
		TIBox.addItem(Clothing.none.getFullDesc());
		TIBox.setSelectedItem(Clothing.none.getFullDesc());
		BOBox.removeAllItems();
		BOBox.addItem(Clothing.none.getFullDesc());
		BOBox.setSelectedItem(Clothing.none.getFullDesc());
		BIBox.removeAllItems();

		for (var article : player.closet) {
			translator.put(article.getFullDesc(), article);
			switch (article.getType()) {
				case TOPOUTER:
					TopOut.add(article);
					TOBox.addItem(article.getFullDesc());
					break;
				case TOP:
					TopMid.add(article);
					TMBox.addItem(article.getFullDesc());
					break;
				case TOPUNDER:
					TopIn.add(article);
					TIBox.addItem(article.getFullDesc());
					break;
				case BOTOUTER:
					BotOut.add(article);
					BOBox.addItem(article.getFullDesc());
					break;
				case UNDERWEAR:
					BotIn.add(article);
					BIBox.addItem(article.getFullDesc());
					break;
			}
		}

		for (var article : player.outfit[0]) {
			if (TopOut.contains(article)) {
				TOBox.setSelectedItem(article.getFullDesc());
			}
		}
		for (var article : player.outfit[0]) {
			if (TopMid.contains(article)) {
				TMBox.setSelectedItem(article.getFullDesc());
			}
		}
		for (var article : player.outfit[0]) {
			if (TopIn.contains(article)) {
				TIBox.setSelectedItem(article.getFullDesc());
			}
		}
		for (var article : player.outfit[1]) {
			if (BotOut.contains(article)) {
				BOBox.setSelectedItem(article.getFullDesc());
			}
		}
		for (var article : player.outfit[1]) {
			if (BotIn.contains(article)) {
				BIBox.setSelectedItem(article.getFullDesc());
			}
		}
		renderCloset();
		freeze = false;
	}

	/**
	 * Loads an image from the specified resource path
	 *
	 * @param path The path to the image resource
	 * @return The loaded image, or Null if none was found, or the image failed to load
	 */
	private BufferedImage loadImage(String path) {
		var url = getClass().getResource(path);
		if (url == null) {
			return null;
		}

		try {
			return ImageIO.read(url);
		}
		catch (IOException ignored) {
			return null;
		}
	}

	private void renderCloset() {
		img.setIcon(null);
		var imagepath = "/gui/assets/room/Room.jpg";
		BufferedImage room = null;

		try {
			var imgurl = getClass().getResource(imagepath);
			if (imgurl != null) {
				room = ImageIO.read(imgurl);
			}
		}
		catch (IOException ignored) {
		}

		if (room == null) {
			return;
		}
		var g = room.createGraphics();

		var TO = translator.get(TOBox.getSelectedItem());
		var TM = translator.get(TMBox.getSelectedItem());
		var TI = translator.get(TIBox.getSelectedItem());
		var BO = translator.get(BOBox.getSelectedItem());
		var BI = translator.get(BIBox.getSelectedItem());

		BufferedImage TOIMG;
		BufferedImage TMIMG;
		BufferedImage TIIMG;
		BufferedImage BIIMG;
		BufferedImage BOIMG;

		if (TO != Clothing.none) {
			switch (TO) {
				case cloak:
				case halfcloak:
					imagepath = "/gui/assets/room/TO_Cloak.png";
					break;
				case windbreaker:
					imagepath = "/gui/assets/room/TO_Windbreaker.png";
					break;
				case blazer:
					imagepath = "/gui/assets/room/TO_Blazer.png";
					break;
				case trenchcoat:

					imagepath = "/gui/assets/room/TO_Coat.png";
					break;
				case labcoat:
					imagepath = "/gui/assets/room/TO_LabCoat.png";
					break;
				case furcoat:
					imagepath = "/gui/assets/room/TO_FurCoat.png";
					break;
				default:
					imagepath = "/gui/assets/room/TO_Jacket.png";
			}

			TOIMG = loadImage(imagepath);
			if (TOIMG != null) {
				g.drawImage(TOIMG, 0, 0, null);
			}
		}
		if (TI != Clothing.none) {
			switch (TI) {
				case lacebra:
				case bra:
					imagepath = "/gui/assets/room/TI_Bra.png";
					break;
				case undershirt:
					imagepath = "/gui/assets/room/TI_Undershirt.png";
			}

			TIIMG = loadImage(imagepath);
			if (TIIMG != null) {
				g.drawImage(TIIMG, 0, 0, null);
			}
		}
		if (TM != Clothing.none) {
			switch (TM) {
				case silkShirt:
					imagepath = "/gui/assets/room/TM_SilkShirt.png";
					break;
				case sweater:
					imagepath = "/gui/assets/room/TM_Sweater.png";
					break;
				case shirt:
					imagepath = "/gui/assets/room/TM_Shirt.png";
					break;
				case sweatshirt:
					imagepath = "/gui/assets/room/TM_SweatShirt.png";
					break;
				case gi:
					imagepath = "/gui/assets/room/TM_KungfuShirt.png";
					break;
				case legendshirt:
					imagepath = "/gui/assets/room/TM_LegendTShirt.png";
					break;
				case shinobitop:
					imagepath = "/gui/assets/room/TM_Ninja.png";
					break;
				case blouse:
					imagepath = "/gui/assets/room/TM_Blouse.png";
					break;
				default:
					imagepath = "/gui/assets/room/TM_TShirt.png";
			}

			TMIMG = loadImage(imagepath);
			if (TMIMG != null) {
				g.drawImage(TMIMG, 0, 0, null);
			}
		}
		if (BI != Clothing.none) {
			switch (BI) {
				case boxers:
					imagepath = "/gui/assets/room/BI_Boxers.png";
					break;
				case cup:
					imagepath = "/gui/assets/room/BI_Cup.png";
					break;
				case loincloth:
					imagepath = "/gui/assets/room/BI_Loincloth.png";
					break;
				case pouchlessbriefs:
					imagepath = "/gui/assets/room/BI_PouchlessBriefs.png";
					break;
				case speedo:
					imagepath = "/gui/assets/room/BI_Speedo.png";
					break;
				case lacepanties:
				case panties:
					imagepath = "/gui/assets/room/BI_Panties.png";
					break;
				default:
					imagepath = "/gui/assets/room/BI_Briefs.png";
			}

			BIIMG = loadImage(imagepath);
			if (BIIMG != null) {
				g.drawImage(BIIMG, 0, 0, null);
			}
		}
		if (BO != Clothing.none) {
			switch (BO) {
				case shorts:
					imagepath = "/gui/assets/room/BO_Shorts.png";
					break;
				case cutoffs:
					imagepath = "/gui/assets/room/BO_JeanShorts.png";
					break;
				case jeans:
					imagepath = "/gui/assets/room/BO_Jeans.png";
					break;
				case gothpants:
					imagepath = "/gui/assets/room/BO_Goth.png";
					break;
				case kungfupants:
					imagepath = "/gui/assets/room/BO_KungfuPants.png";
					break;
				case kilt:
					imagepath = "/gui/assets/room/BO_Kilt.png";
					break;
				case skirt:
					imagepath = "/gui/assets/room/BO_Skirt.png";
					break;
				default:
					imagepath = "/gui/assets/room/BO_Pants.png";
			}

			BOIMG = loadImage(imagepath);
			if (BOIMG != null) {
				g.drawImage(BOIMG, 0, 0, null);
			}
		}
		g.dispose();
		img.setIcon(new ImageIcon(room));
	}
}
