package gui;

import areas.MapDrawHint;
import global.Constants;
import global.Global;
import global.Scheduler;

import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

/**
 * Defines a component for rendering the minimap for a match
 */
public class MapComponent extends JComponent {
	private static final Font font = new Font("Courier", Font.BOLD, 11);

	public MapComponent() {
	}

	public void paint(Graphics g) {
		if (Scheduler.getMatch() == null) {
			return;
		}
		if (g instanceof Graphics2D) {
			var graphics2D = (Graphics2D) g;
			graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		}

		var multiplier = 10;
		var width = Math.max(getWidth(), 25 * multiplier);
		var height = Math.max(getHeight(), 19 * multiplier);
		var mapBorder = 12;
		var yOffset = 0;
		g.setColor(Constants.ALTFRAMECOLOR);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(Color.WHITE);
		var borderWidth = 3;
		g.drawRect(borderWidth, borderWidth, width - borderWidth * 2, height - borderWidth * 2);
		for (var area : Scheduler.getMatch().getAreas()) {
			if (area.drawHint.rect.width == 0 || area.drawHint.rect.height == 0) {
				return;
			}
			// Draw room box with hint to present opponents
			var rect = new Rectangle(area.drawHint.rect.x * multiplier + mapBorder,
					area.drawHint.rect.y * multiplier + yOffset + mapBorder,
					area.drawHint.rect.width * multiplier,
					area.drawHint.rect.height * multiplier);
			if (area.humanPresent()) {
				g.setColor(new Color(25, 74, 120));
			}
			else if (Global.getPlayer().opponentDetected(area)) {
				g.setColor(new Color(150, 45, 60));
			}
			else {
				g.setColor(new Color(0, 34, 100));
			}
			g.fillRect(rect.x, rect.y, rect.width, rect.height);

			// Draw room border
			var rectRoom = new Rectangle(area.drawHint.rect.x * multiplier + mapBorder,
					area.drawHint.rect.y * multiplier + yOffset + mapBorder,
					area.drawHint.rect.width * multiplier, area.drawHint.rect.height * multiplier);
			g.setColor(new Color(50, 100, 200));
			g.drawRect(rectRoom.x, rectRoom.y, rectRoom.width, rectRoom.height);

			// Draw room name
			var rectName = new Rectangle(area.drawHint.rect.x * multiplier + mapBorder,
					area.drawHint.rect.y * multiplier + yOffset + mapBorder,
					area.drawHint.rect.width * multiplier, area.drawHint.rect.height * multiplier);
			g.setColor(Color.WHITE);
			centerString(g, rectName, area.drawHint);
		}
	}

	private void centerString(Graphics g, Rectangle r, MapDrawHint drawHint) {
		var frc = new FontRenderContext(null, true, true);
		var r2D = MapComponent.font.getStringBounds(drawHint.label, frc);

		var rWidth = (int) Math.round(r2D.getWidth());
		var rHeight = (int) Math.round(r2D.getHeight());
		var rX = (int) Math.round(r2D.getX());
		var rY = (int) Math.round(r2D.getY());

		var a = (r.width / 2) - (rWidth / 2) - rX;
		var b = (r.height / 2) - (rHeight / 2) - rY;
		AffineTransform orig = null;
		if (drawHint.vertical && g instanceof Graphics2D) {
			var g2d = (Graphics2D) g;
			orig = g2d.getTransform();
			g2d.rotate(Math.PI / 2, (r.x + a) + rWidth * .5, r.y + b - rHeight / 4F);
		}
		g.setFont(MapComponent.font);
		g.drawString(drawHint.label, r.x + a, r.y + b);

		if (orig != null) {
			var g2d = (Graphics2D) g;
			g2d.setTransform(orig);
		}
	}
}
