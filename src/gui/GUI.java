package gui;

import actions.Action;
import characters.Attribute;
import characters.Character;
import characters.Meter;
import characters.Player;
import combat.Combat;
import combat.Encounter;
import combat.Encs;
import daytime.Activity;
import daytime.Store;
import global.*;
import items.*;
import skills.Skill;
import skills.TacticGroup;
import trap.Trap;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;

/**
 * Defines the application GUI
 */
public class GUI extends JFrame implements Observer {

	protected Combat combat;
	private Player player;
	private HashMap<TacticGroup, ArrayList<SkillButton>> skills;
	private ArrayList<SkillButton> flasks;
	private ArrayList<SkillButton> potions;
	private ArrayList<SkillButton> demands;
	private String bodytext;
	private CommandPanel commandPanel;
	private Box groupBox;
	private JTextPane textPane;
	private Title titlePanel;
	private JLabel stamina;
	private JLabel arousal;
	private JLabel mojo;
	private JProgressBar staminaBar;
	private JProgressBar arousalBar;
	private JProgressBar mojoBar;
	private JPanel topPanel;
	private JLabel loclbl;
	private JLabel timelbl;
	private JLabel cashlbl;
	private JLabel daylbl;
	private Panel panel0;
	private CreationGUI creation;
	private JScrollPane textScroll;
	private JPanel mainpanel;
	private JToggleButton invbtn;
	private JToggleButton stsbtn;
	private JPanel inventoryPanel;
	private JPanel statusPanel;
	private JPanel centerPanel;
	private JLabel clothesdisplay;
	private JPanel optionspanel;
	private JPanel portraitPanel;
	private JLabel portrait;
	private JLabel sprite;
	private JComponent map;
	private JLabel imgPanel;
	//Options
	private JRadioButton rdnormal;
	private JRadioButton rdhard;
	private JRadioButton rdautosaveon;
	private JRadioButton rdautosaveoff;
	private JRadioButton rdpor1;
	private JRadioButton rdpor2;
	private JRadioButton rdpor3;
	private JRadioButton rdimgon;
	private JRadioButton rdimgoff;
	private JRadioButton rdimgexact;
	private JRadioButton rdstsimgon;
	private JRadioButton rdstsimgoff;
	private JRadioButton rdgentleon;
	private JRadioButton rdgentleoff;
	private JSpinner fontspinner;
	private JRadioButton rdrpt1;
	private JRadioButton rdrpt2;
	private JRadioButton rdcol1;
	private JRadioButton rdcol2;
	private JSlider playerScale;
	private JSlider npcScale;
	//ColorScheme
	private Color textColor;
	private Color frameColor;
	private Color backgroundColor;
	private int width;
	private int height;
	public int fontsize;
	private JMenuItem mntmQuitMatch;
	private JMenuItem mntmOptions;
	private JPanel midPanel;
	private ClothesChangeGUI closet;
	private TacticGroup currentTactics;
	private boolean lowres;

	private final static String USE_PORTRAIT = "PORTRAIT";
	private final static String USE_MAP = "MAP";
	private final static String USE_NONE = "NONE";
	private final static String USE_SPRITE = "SPRITE";
	private static final String USE_MAIN_TEXT_UI = "MAIN_TEXT";
	private static final String USE_CLOSET_UI = "CLOSET";
	private JPanel groupPanel;

	private HashMap<String, BufferedImage> statusFilterImages;

	public GUI() {
		try {
			for (var info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		}
		catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look and feel.
		}

		setBackground(Color.GRAY);

		// frame title
		setTitle("Night Games");

		// closing operation
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		// resolution resolver
		if (Toolkit.getDefaultToolkit().getScreenSize().getHeight() >= 900) {
			height = 900;
		}
		else {
			height = 720;
			lowres = true;
		}
		if (Toolkit.getDefaultToolkit().getScreenSize().getWidth() >= 1600) {
			width = 1600;
		}
		else {
			width = 1024;
		}
		setPreferredSize(new Dimension(width, height));
		// center the window on the monitor

		var y = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		var x = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();

		var x1 = x / 2 - width / 2;
		var y1 = y / 2 - height / 2;

		this.setLocation(x1, y1);

		titlePanel = new Title(this, width, height);
		getContentPane().add(titlePanel);
		pack();
		setVisible(true);
	}

	/**
	 * Builds the main GUI
	 */
	public void BuildGUI() {
		getContentPane().remove(titlePanel);
		// Colors
		textColor = Constants.PRIMARYTEXTCOLOR;
		frameColor = Constants.PRIMARYFRAMECOLOR;
		backgroundColor = Constants.PRIMARYBGCOLOR;

		buildMenuBar();

		// panel layouts

		// mainpanel - everything is contained within it

		this.mainpanel = new JPanel();
		this.mainpanel.setBackground(backgroundColor);
		getContentPane().add(this.mainpanel);
		this.mainpanel.setLayout(new BoxLayout(this.mainpanel, BoxLayout.Y_AXIS));

		// panel0 - invisible, only handles topPanel
		this.panel0 = new Panel();
		this.mainpanel.add(this.panel0);
		this.panel0.setLayout(new BoxLayout(this.panel0, BoxLayout.X_AXIS));

		// topPanel - invisible, menus
		this.topPanel = new JPanel();
		this.panel0.add(this.topPanel);
		this.topPanel.setLayout(new BoxLayout(this.topPanel, BoxLayout.X_AXIS));

		// centerPanel - invisible, body of GUI

		this.centerPanel = new JPanel();
		this.mainpanel.add(this.centerPanel);
		this.centerPanel.setLayout(new BorderLayout(0, 0));

		// inventoryPanel - visible, items and clothing
		this.inventoryPanel = new JPanel();
		this.inventoryPanel.setLayout(new BorderLayout(0, 0));
		this.inventoryPanel.setBackground(frameColor);
		this.inventoryPanel.setMaximumSize(new Dimension(100, height));

		// statusPanel - visible, character status
		this.statusPanel = new JPanel();
		this.statusPanel.setLayout(new BoxLayout(this.statusPanel, BoxLayout.X_AXIS));
		this.statusPanel.setBackground(frameColor);

		// portraitPanel
		portraitPanel = new JPanel();
		centerPanel.add(portraitPanel, BorderLayout.WEST);
		portraitPanel.setLayout(new CardLayout());
		portraitPanel.setBackground(backgroundColor);
		portrait = new JLabel("");
		portrait.setVerticalAlignment(SwingConstants.TOP);
		sprite = new JLabel("");
		sprite.setVerticalAlignment(SwingConstants.BOTTOM);
		portraitPanel.add(portrait, USE_PORTRAIT);
		portraitPanel.add(sprite, USE_SPRITE);

		map = new MapComponent();
		portraitPanel.add(map, USE_MAP);
		var logo = new JLabel("");
		portraitPanel.add(logo, USE_NONE);
		var logoImg = loadImage("assets/logo.png");
		if (logoImg != null) {
			logo.setIcon(new ImageIcon(logoImg));
		}
		map.setPreferredSize(new Dimension(300, 385));

		midPanel = new JPanel();
		midPanel.setLayout(new CardLayout());
		centerPanel.add(midPanel, BorderLayout.CENTER);

		// imgPanel - visible, contains imgLabel
		imgPanel = new JLabel();
		imgPanel.setLayout(new BorderLayout(2, 2));
		imgPanel.setBackground(backgroundColor);

		// textScroll
		this.textScroll = new JScrollPane();
		textScroll.setOpaque(false);
		midPanel.add(textScroll, USE_MAIN_TEXT_UI);

		// textPane
		this.textPane = new JTextPane();
		var caret = (DefaultCaret) textPane.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		this.textPane.setForeground(textColor);
		this.textPane.setBackground(backgroundColor);
		this.textPane.setPreferredSize(new Dimension(width, 400));
		this.textPane.setEditable(false);
		this.textPane.setContentType("text/html");
		bodytext = "";
		this.textScroll.setViewportView(this.textPane);

		fontsize = Math.round(Global.getValue(Flag.fontsize));

		var debug = new JButton("Debug");
		debug.addActionListener(arg0 -> Scheduler.unpause());

		// commandPanel - visible, contains the player's command buttons
		groupBox = Box.createHorizontalBox();
		groupBox.setBackground(frameColor);
		groupBox.setBorder(new CompoundBorder());
		groupPanel = new JPanel();
		mainpanel.add(groupPanel);

		commandPanel = new CommandPanel(width, height);
		groupPanel.add(groupBox);
		groupPanel.add(commandPanel);
		commandPanel.setBackground(frameColor);

		groupPanel.setBackground(frameColor);
		groupPanel.setBorder(new CompoundBorder());
		skills = new HashMap<>();
		currentTactics = TacticGroup.All;
		flasks = new ArrayList<>();
		potions = new ArrayList<>();
		demands = new ArrayList<>();
		clearCommand();
		createCharacter();
		setVisible(true);
		pack();

		statusFilterImages = new HashMap<>();

		var panel = (JPanel) getContentPane();
		panel.setFocusable(true);
		panel.addKeyListener(new KeyListener() {
			/**
			 * Space bar will select the first option, unless they are in the default actions list.
			 */
			@Override
			public void keyReleased(KeyEvent e) {
				var buttonOptional = commandPanel.getButtonForHotkey(e.getKeyChar());
				buttonOptional.ifPresent(KeyableButton::call);
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
	}

	/**
	 * Switches to the area map panel
	 */
	public void showMap() {
		map.setPreferredSize(new Dimension(300, 385));
		var portraitLayout = (CardLayout) (portraitPanel.getLayout());
		portraitLayout.show(portraitPanel, USE_MAP);
		portraitPanel.repaint();
		portraitPanel.revalidate();
	}

	/**
	 * Switches to the portrait panel
	 */
	public void showPortrait() {
		var portraitLayout = (CardLayout) (portraitPanel.getLayout());
		if (Global.checkFlag(Flag.portraits2)) {
			portrait.setPreferredSize(new Dimension(300, 385));
			portraitLayout.show(portraitPanel, USE_PORTRAIT);
		}
		else {
			sprite.setPreferredSize(new Dimension(300, 385));
			portraitLayout.show(portraitPanel, USE_SPRITE);
		}
		portraitPanel.repaint();
		portraitPanel.revalidate();
	}

	/**
	 * Clears the portrait panel
	 */
	public void showNone() {
		var portraitLayout = (CardLayout) (portraitPanel.getLayout());
		portraitLayout.show(portraitPanel, USE_NONE);
		midPanel.repaint();
		midPanel.revalidate();
	}

	/**
	 * Displays the specified image in the message panel
	 *
	 * @param path   The path to the image inside the assets folder
	 * @param artist The name of the artist, used as the image title
	 */
	public void displayImage(String path, String artist) {
		if (Global.checkFlag(Flag.noimage)) {
			return;
		}
		var imgSrc = getClass().getResource("assets/" + path);
		if (imgSrc != null) {
			message(String.format("<img src=\"%s\" title=\"%s\"/>", imgSrc, artist));
		}
	}

	/**
	 * Clears the displayed portrait
	 */
	private void resetPortrait() {
		portrait.setIcon(null);
	}

	/**
	 * Loads a portrait of one of the specified characters
	 * <p>
	 * The portrait shown will be of the first non-human character
	 *
	 * @param first  The first character
	 * @param second The second character
	 */
	public void loadPortrait(Character first, Character second) {
		if (Global.checkFlag(Flag.noportraits)) {
			return;
		}

		String imagePath = null;
		BufferedImage spriteImg = null;

		// Find first character that isn't the human player (no portrait)
		if (!first.human()) {
			imagePath = first.getPortrait();
			spriteImg = first.getSpriteImage();
		}
		else if (!second.human()) {
			imagePath = second.getPortrait();
			spriteImg = second.getSpriteImage();
		}

		// Try to display sprite first if preferences are set that way
		sprite.setIcon(null);
		if (spriteImg != null && !Global.checkFlag(Flag.portraits2)) {
			sprite.setIcon(new ImageIcon(spriteImg));
			var portraitLayout = (CardLayout) (portraitPanel.getLayout());
			portraitLayout.show(portraitPanel, USE_SPRITE);
		}
		else if (imagePath != null) {
			var face = loadImage(imagePath);
			if (face != null) {
				portrait.setIcon(null);
				portrait.setIcon(new ImageIcon(face));
				var portraitLayout = (CardLayout) (portraitPanel.getLayout());
				portraitLayout.show(portraitPanel, USE_PORTRAIT);
			}
		}

		this.portraitPanel.repaint();
	}

	/**
	 * Loads the portraits of the specified characters, as long as they aren't human
	 *
	 * @param first  The first character
	 * @param second The second character
	 */
	public void loadTwoPortraits(Character first, Character second) {
		if (Global.checkFlag(Flag.noportraits)) {
			return;
		}

		String imagePath = null;
		BufferedImage spriteImg = null;
		String imagePath2 = null;
		BufferedImage spriteImg2 = null;
		BufferedImage sprites;
		if (!first.human()) {
			imagePath = first.getPortrait();
			spriteImg = first.getSpriteImage();
		}
		if (!second.human()) {
			imagePath2 = second.getPortrait();
			spriteImg2 = second.getSpriteImage();
		}

		// Try to display sprites first if preferences are set that way
		sprite.setIcon(null);
		if ((spriteImg != null || spriteImg2 != null) && !Global.checkFlag(Flag.portraits2)) {
			if (spriteImg != null && spriteImg2 != null) {
				sprites = new BufferedImage(spriteImg.getWidth(), spriteImg.getHeight(), spriteImg.getType());
				var g = sprites.createGraphics();
				g.drawImage(spriteImg, -60, 0, null);
				g.drawImage(spriteImg2, 60, 0, null);
			}
			else if (spriteImg != null) {
				sprites = spriteImg;
			}
			else {
				sprites = spriteImg2;
			}
			sprite.setIcon(new ImageIcon(sprites));
			var portraitLayout = (CardLayout) (portraitPanel.getLayout());
			portraitLayout.show(portraitPanel, USE_SPRITE);
		}
		else if (imagePath != null || imagePath2 != null) {
			BufferedImage face = null;
			BufferedImage face2 = null;
			BufferedImage faces = null;
			var height = 0;
			var width = 0;
			if (imagePath != null) {
				face = loadImage(imagePath);
				if (face != null) {
					height += face.getHeight();
					width = face.getWidth();
				}
			}
			if (imagePath2 != null) {
				face2 = loadImage(imagePath2);
				if (face2 != null) {
					height += face2.getHeight();
					width = Math.max(face2.getWidth(), width);
				}
			}

			if (face != null && face2 != null) {
				faces = new BufferedImage(width, height, face.getType());
				var g = faces.createGraphics();
				g.drawImage(face, 0, 0, null);
				g.drawImage(face2, 0, face.getHeight(), null);
			}
			else if (face != null) {
				faces = face;
			}
			else if (face2 != null) {
				faces = face2;
			}

			if (faces != null) {
				portrait.setIcon(null);
				portrait.setIcon(new ImageIcon(faces));
				var portraitLayout = (CardLayout) (portraitPanel.getLayout());
				portraitLayout.show(portraitPanel, USE_PORTRAIT);
			}
		}

		this.portraitPanel.repaint();
	}

	/**
	 * Loads an image of the specified character's status filters, for use as an overlay
	 *
	 * @param NPC The character to create the image for
	 * @return The loaded image, containing a combination of all status filters
	 */
	public BufferedImage loadStatusFilters(Character NPC) {
		var filter = new BufferedImage(350, 540, BufferedImage.TYPE_INT_ARGB);
		var g = filter.createGraphics();

		for (var status : NPC.listStatus()) {
			// Convert specific status where filter image has different name
			if (status.equalsIgnoreCase("Shield")) {
				status = "Barrier";
			}
			else if (status.equalsIgnoreCase("Tied Up")) {
				status = "Bound";
			} else if (status.equalsIgnoreCase("Flat-Footed")) {
				status = "Distracted";
			} else if (status.endsWith(" Form")) {
				status = status.replace(" ", "");
			}

			// Check for already cached image
			var image = statusFilterImages.get(status);
			if (image == null) {
				image = loadImage("assets/" + status + "_Status.png");
				statusFilterImages.put(status, image);
			}

			if (image != null) {
				g.drawImage(image, 0, 0, null);
			}
		}

		return filter;
	}

	/**
	 * Begins combat between the specified players
	 *
	 * @param player The player character
	 * @param enemy  The opponent
	 * @return The created combat object
	 */
	public Combat beginCombat(Character player, Character enemy) {
		this.combat = Scheduler.getMatch().buildCombat(player, enemy);
		this.combat.addObserver(this);
		resetPortrait();
		if (!Global.checkFlag(Flag.noportraits)) {
			loadPortrait(player, enemy);
			showPortrait();
		}
		return this.combat;
	}

	/**
	 * Begins watching the specified combat
	 *
	 * @param c A reference to the ongoing combat
	 */
	public void watchCombat(Combat c) {
		this.combat = c;
		this.combat.addObserver(this);
	}

	/**
	 * Gets a string displaying the specified meter's current/maximum value
	 *
	 * @param meter The meter to parse
	 */
	public String getLabelString(Meter meter) {
		return meter.get() + "/" + meter.max();
	}

	/**
	 * Populates the UI with the player's information
	 *
	 * @param player The player character
	 */
	public void populatePlayer(Player player) {
		if (Global.checkFlag(Flag.altcolors)) {
			textColor = Constants.ALTTEXTCOLOR;
			frameColor = Constants.ALTFRAMECOLOR;
			backgroundColor = Constants.ALTBGCOLOR;
		}

		// Switch to the main panel
		getContentPane().remove(this.creation);
		getContentPane().add(this.mainpanel);
		getContentPane().validate();
		this.player = player;
		player.gui = this;
		player.addObserver(this);
		var meter = new JPanel();
		meter.setBackground(frameColor);
		this.topPanel.add(meter);
		meter.setLayout(new GridLayout(0, 3, 0, 0));
		fontsize = Math.round(Global.getValue(Flag.fontsize));

		// Set player stat texts
		this.stamina = new JLabel("Stamina: " + getLabelString(player.getStamina()));
		this.stamina.setFont(new Font("Georgia", Font.BOLD, 15));
		this.stamina.setHorizontalAlignment(0);
		this.stamina.setForeground(new Color(200, 14, 12));
		this.stamina.setToolTipText(
				"Stamina represents your endurance and ability to keep fighting. If it drops to zero, you'll be temporarily stunned.");
		meter.add(this.stamina);

		this.arousal = new JLabel("Arousal: " + getLabelString(player.getArousal()));
		this.arousal.setFont(new Font("Georgia", Font.BOLD, 15));
		this.arousal.setHorizontalAlignment(0);
		this.arousal.setForeground(new Color(254, 1, 107));
		this.arousal.setToolTipText(
				"Arousal is raised when your opponent pleasures or seduces you. If it hits your max, you'll orgasm and lose the fight.");
		meter.add(this.arousal);

		this.mojo = new JLabel("Mojo: " + getLabelString(player.getMojo()));
		this.mojo.setFont(new Font("Georgia", Font.BOLD, 15));
		this.mojo.setHorizontalAlignment(0);
		this.mojo.setForeground(new Color(51, 153, 255));
		this.mojo.setToolTipText(
				"Mojo is the abstract representation of your momentum and style. It increases with normal techniques and is used to power special moves");
		meter.add(this.mojo);

		// Set player stat bars
		this.staminaBar = new JProgressBar();
		var staminaOver = new UIDefaults();
		staminaOver.putAll(UIManager.getLookAndFeelDefaults());
		staminaOver.put("ProgressBar[Enabled+Finished].foregroundPainter",
				new ProgressBarPainter(new Color(200, 14, 12, 180)));
		staminaOver.put("ProgressBar[Enabled].foregroundPainter", new ProgressBarPainter(new Color(200, 14, 12, 180)));
		staminaOver.put("ProgressBar[Enabled+Indeterminate].foregroundPainter",
				new ProgressBarPainter(new Color(200, 14, 12, 180)));
		this.staminaBar.putClientProperty("Nimbus.Overrides", staminaOver);
		this.staminaBar
				.setBorder(new SoftBevelBorder(1, null, null, null, null));
		meter.add(this.staminaBar);
		this.staminaBar.setMaximum(player.getStamina().max());
		this.staminaBar.setValue(player.getStamina().get());

		this.arousalBar = new JProgressBar();
		this.arousalBar
				.setBorder(new SoftBevelBorder(1, null, null, null, null));
		this.arousalBar.setForeground(new Color(254, 1, 107));
		var arousalOver = new UIDefaults();
		arousalOver.putAll(UIManager.getLookAndFeelDefaults());
		arousalOver.put("ProgressBar[Enabled+Finished].foregroundPainter",
				new ProgressBarPainter(new Color(254, 1, 107, 180)));
		arousalOver.put("ProgressBar[Enabled].foregroundPainter", new ProgressBarPainter(new Color(254, 1, 107, 180)));
		arousalOver.put("ProgressBar[Enabled+Indeterminate].foregroundPainter",
				new ProgressBarPainter(new Color(254, 1, 107, 180)));
		this.arousalBar.putClientProperty("Nimbus.Overrides", arousalOver);
		meter.add(this.arousalBar);
		this.arousalBar.setMaximum(player.getArousal().max());
		this.arousalBar.setValue(player.getArousal().get());

		this.mojoBar = new JProgressBar();
		this.mojoBar.setBorder(new SoftBevelBorder(1, null, null, null, null));
		this.mojoBar.setForeground(new Color(51, 153, 255));
		this.mojoBar.setBackground(frameColor);
		var mojoOver = new UIDefaults();
		mojoOver.putAll(UIManager.getLookAndFeelDefaults());
		mojoOver.put("ProgressBar[Enabled+Finished].foregroundPainter",
				new ProgressBarPainter(new Color(51, 153, 255, 180)));
		mojoOver.put("ProgressBar[Enabled].foregroundPainter", new ProgressBarPainter(new Color(51, 153, 255, 180)));
		mojoOver.put("ProgressBar[Enabled+Indeterminate].foregroundPainter",
				new ProgressBarPainter(new Color(51, 153, 255, 180)));
		this.mojoBar.putClientProperty("Nimbus.Overrides", mojoOver);
		meter.add(this.mojoBar);
		this.mojoBar.setMaximum(player.getMojo().max());
		this.mojoBar.setValue(player.getMojo().get());

		var bio = new JPanel();
		this.topPanel.add(bio);
		bio.setLayout(new GridLayout(2, 0, 0, 0));
		bio.setBackground(frameColor);

		this.timelbl = new JLabel();
		this.timelbl.setFont(new Font("Georgia", Font.BOLD, 16));
		this.timelbl.setForeground(textColor);
		bio.add(this.timelbl);

		this.daylbl = new JLabel();
		this.daylbl.setFont(new Font("Georgia", Font.BOLD, 16));
		this.daylbl.setForeground(textColor);
		bio.add(this.daylbl);

		UIManager.put("ToggleButton.select", new Color(75, 88, 102));
		this.stsbtn = new JToggleButton("Status");
		stsbtn.setBackground(frameColor);
		stsbtn.setForeground(textColor);

		this.stsbtn.addActionListener(arg0 -> {
			if (GUI.this.stsbtn.isSelected()) {
				GUI.this.centerPanel.remove(GUI.this.portraitPanel);
				GUI.this.centerPanel.add(GUI.this.statusPanel, BorderLayout.WEST);
			}
			else {
				GUI.this.centerPanel.remove(GUI.this.statusPanel);
				GUI.this.centerPanel.add(GUI.this.portraitPanel, BorderLayout.WEST);
			}
			GUI.this.refresh();
			GUI.this.centerPanel.repaint();
		});
		bio.add(this.stsbtn);

		this.cashlbl = new JLabel();
		this.cashlbl.setFont(new Font("Georgia", Font.BOLD, 16));
		this.cashlbl.setForeground(textColor);
		bio.add(this.cashlbl);

		this.loclbl = new JLabel();
		this.loclbl.setFont(new Font("Georgia", Font.BOLD, 16));
		this.loclbl.setForeground(textColor);
		bio.add(this.loclbl);

		this.invbtn = new JToggleButton("Inventory");
		invbtn.setBackground(frameColor);
		invbtn.setForeground(textColor);
		this.invbtn.addActionListener(arg0 -> {
			if (GUI.this.invbtn.isSelected()) {
				GUI.this.centerPanel.add(GUI.this.inventoryPanel, "East");
			}
			else {
				GUI.this.centerPanel.remove(GUI.this.inventoryPanel);
			}
			GUI.this.refresh();
		});
		bio.add(this.invbtn);
		closet = new ClothesChangeGUI(player);
		midPanel.add(closet, USE_CLOSET_UI);
		showNone();
		enableOptions();
		this.commandPanel.setBackground(frameColor);
		this.groupPanel.setBackground(frameColor);
		portraitPanel.setBackground(backgroundColor);
		imgPanel.setBackground(backgroundColor);
		this.mainpanel.setBackground(backgroundColor);
		this.topPanel.validate();
	}

	/**
	 * Removes all player information from the GUI
	 */
	public void purgePlayer() {
		getContentPane().remove(this.mainpanel);
		clearText();
		clearCommand();
		showNone();
		if (closet != null) {
			midPanel.remove(closet);
		}
		this.mntmQuitMatch.setEnabled(false);
		this.combat = null;
		this.topPanel.removeAll();
	}

	/**
	 * Clears all text from the main body
	 */
	public void clearText() {
		bodytext = "";
		this.textPane.setText("");
	}

	/**
	 * Writes the specified message to the main body
	 *
	 * @param text The text to add
	 */
	public void message(String text) {
		if (text == null || text.equals("")) {
			return;
		}
		if (text.trim().length() == 0) {
			return;
		}

		var doc = (HTMLDocument) textPane.getDocument();
		var editorKit = (HTMLEditorKit) textPane.getEditorKit();
		try {
			bodytext = bodytext + text + "<br>";
			editorKit.insertHTML(doc,
					doc.getLength(),
					"<font face='Georgia'><font color='black'><font size='" + fontsize + "'>" + text + "<br>",
					0,
					0,
					null);
		}
		catch (BadLocationException | IOException e) {
			e.printStackTrace();
		}

		// Scroll to bottom
		javax.swing.SwingUtilities.invokeLater(() -> textScroll.getVerticalScrollBar().setValue(0));
	}

	/**
	 * Writes the specified text at the top of the main body
	 *
	 * @param text The text to write
	 */
	public void messageHead(String text) {
		if (text == null || text.equals("")) {
			return;
		}
		if (text.trim().length() == 0) {
			return;
		}
		var newText = text + bodytext;
		clearText();
		message(newText);
	}

	/**
	 * Writes the latest combat message to the main body
	 *
	 * @param text The text to write
	 */
	public void combatMessage(String text) {
		var doc = (HTMLDocument) textPane.getDocument();
		var editorKit = (HTMLEditorKit) textPane.getEditorKit();
		try {
			bodytext = bodytext + text + "<br>";
			editorKit.insertHTML(doc,
					doc.getLength(),
					"<font face='Georgia'><font color='black'><font size='" + fontsize + "'>" + text + "<br>",
					0,
					0,
					null);
		}
		catch (BadLocationException | IOException e) {
			e.printStackTrace();
		}

		// Scroll to bottom
		javax.swing.SwingUtilities.invokeLater(() -> textScroll.getVerticalScrollBar().setValue(1));
	}

	/**
	 * Clears the command panel
	 */
	public void clearCommand() {
		commandPanel.reset();
		skills.clear();
		flasks.clear();
		potions.clear();
		demands.clear();
		for (var tactic : TacticGroup.values()) {
			skills.put(tactic, new ArrayList<>());
		}
		groupBox.removeAll();
		this.commandPanel.refresh();
	}

	/**
	 * Adds the specified action as a skill button to the Flask tactic group
	 *
	 * @param action The action to add
	 * @param com    A reference to the ongoing combat
	 */
	public void addFlasks(Skill action, Combat com) {
		skills.get(TacticGroup.Flask).add(new SkillButton(action, com));
	}

	/**
	 * Adds the specified action as a skill button to the Potion tactic group
	 *
	 * @param action The action to add
	 * @param com    A reference to the ongoing combat
	 */
	public void addPotions(Skill action, Combat com) {
		skills.get(TacticGroup.Potion).add(new SkillButton(action, com));
	}

	/**
	 * Adds the specified action as a skill button to the Demand tactic group
	 *
	 * @param action The action to add
	 * @param com    A reference to the ongoing combat
	 */
	public void addDemands(Skill action, Combat com) {
		skills.get(TacticGroup.Demand).add(new SkillButton(action, com));
	}

	/**
	 * Adds the specified action as a skill button to its tactic group
	 *
	 * @param action The action to add
	 * @param com    A reference to the ongoing combat
	 */
	public void addSkill(Combat com, Skill action) {
		skills.get(action.type().getGroup()).add(new SkillButton(action, com));
	}

	/**
	 * Shows buttons grouped by their tactic group in the command panel
	 */
	public void showSkills() {
		commandPanel.reset();
		var i = 1;

		// Add buttons to switch to other tactic groups
		for (var group : TacticGroup.values()) {
			var tacticsButton = new SwitchTacticsButton(group);
			commandPanel.register(java.lang.Character.forDigit(i % 10, 10), tacticsButton);
			groupBox.add(tacticsButton);
			groupBox.add(Box.createHorizontalStrut(4));
			i += 1;
		}

		// Build flat list of all commands
		var flatList = new ArrayList<SkillButton>();
		for (var group : TacticGroup.values()) {
			skills.get(group).sort(Comparator.comparing(AbstractButton::getText));
			flatList.addAll(skills.get(group));
		}

		// Check whether to display all command, or only the active tactic group
		if (currentTactics == TacticGroup.All || flatList.size() <= 6) {
			for (var b : flatList) {
				addToCommandPanel(b);
			}
		}
		else {
			for (var button : skills.get(currentTactics)) {
				addToCommandPanel(button);
			}
		}
		Scheduler.pause();
		commandPanel.refresh();
	}

	/**
	 * Adds the specified action to the command panel
	 *
	 * @param action The action to add
	 * @param user   The character using the action
	 */
	public void addAction(Action action, Character user) {
		this.commandPanel.add(new ActionButton(action, user));
		Scheduler.pause();
		this.commandPanel.refresh();
	}

	/**
	 * Adds the specified activity to the command panel
	 *
	 * @param act The activity to add
	 */
	public void addActivity(Activity act) {
		if (act.tooltip().isEmpty()) {
			this.commandPanel.add(new ActivityButton(act));
		}
		else {
			this.commandPanel.add(new ActivityButton(act, act.tooltip()));
		}
		this.commandPanel.refresh();
	}

	/**
	 * Adds a button to make a choice in the active scene
	 *
	 * @param choice The choice
	 */
	public void choose(String choice) {
		this.commandPanel.add(new SceneButton(choice));
		this.commandPanel.refresh();
	}

	/**
	 * Adds a button to make a choice in the active scene
	 *
	 * @param choice      The choice
	 * @param description The description tooltip
	 */
	public void choose(String choice, String description) {
		this.commandPanel.add(new SceneButton(choice, description));
		this.commandPanel.refresh();
	}

	/**
	 * Adds a disabled scene choice button
	 *
	 * @param choice      The choice
	 * @param description The description tooltip
	 */
	public void unchoose(String choice, String description) {
		var button = new SceneButton(choice, description);
		button.setEnabled(false);
		this.commandPanel.add(button);
		this.commandPanel.refresh();
	}

	/**
	 * Adds a button to make a choice in the specified event
	 *
	 * @param event  The event
	 * @param choice The choice
	 */
	public void choose(Activity event, String choice) {
		this.commandPanel.add(new EventButton(event, choice));
		this.commandPanel.refresh();
	}

	/**
	 * Adds a button to make a choice in the specified event
	 *
	 * @param event       The event
	 * @param choice      The choice
	 * @param description The description tooltip
	 */
	public void choose(Activity event, String choice, String description) {
		this.commandPanel.add(new EventButton(event, choice, description));
		this.commandPanel.refresh();
	}

	/**
	 * Adds a disabled event choice button
	 *
	 * @param event       The event
	 * @param choice      The choice
	 * @param description The description tooltip
	 */
	public void unchoose(Activity event, String choice, String description) {
		var button = new EventButton(event, choice, description);
		button.setEnabled(false);
		this.commandPanel.add(button);
		this.commandPanel.refresh();
	}

	/**
	 * Adds a button to purchase an item
	 *
	 * @param shop The store to buy the item at
	 * @param item The item
	 */
	public void sale(Store shop, Item item) {
		this.commandPanel.add(new ItemButton(shop, item));
		this.commandPanel.refresh();
	}

	/**
	 * Adds a button to purchase an article of clothing
	 *
	 * @param shop     The store to buy the clothing at
	 * @param clothing The clothing
	 */
	public void sale(Store shop, Clothing clothing) {
		this.commandPanel.add(new ItemButton(shop, clothing));
		this.commandPanel.refresh();
	}

	/**
	 * Adds the buttons for fight/flight in an encounter
	 *
	 * @param enc    A reference to the encounter
	 * @param target The character to fight or flee from
	 */
	public void promptFF(Encounter enc, Character target) {
		clearCommand();
		this.commandPanel.add(new EncounterButton("Fight", enc, target, Encs.fight));
		this.commandPanel.add(new EncounterButton("Flee", enc, target, Encs.flee));
		if (player.has(Consumable.smoke)) {
			this.commandPanel.add(new EncounterButton("Smoke Bomb", enc, target, Encs.smokebomb));
		}
		Scheduler.pause();
		this.commandPanel.refresh();
	}

	/**
	 * Adds the button to ambush a character in an encounter
	 *
	 * @param enc    A reference to the encounter
	 * @param target The character to ambush
	 */
	public void promptAmbush(Encounter enc, Character target) {
		clearCommand();
		this.commandPanel.add(new EncounterButton("Attack " + target.name(), enc, target, Encs.ambush));
		Scheduler.pause();
		this.commandPanel.refresh();
	}

	/**
	 * Adds the button for an opportunity attack in an encounter
	 *
	 * @param enc    A reference to the encounter
	 * @param target The character to attack
	 * @param trap   The trap to use in the attack
	 */
	public void promptOpportunity(Encounter enc, Character target, Trap trap) {
		clearCommand();
		this.commandPanel.add(new EncounterButton("Attack " + target.name(), enc, target, Encs.capitalize, trap));
		Scheduler.pause();
		this.commandPanel.refresh();
	}

	/**
	 * Adds the buttons for a shower ambush in an encounter
	 *
	 * @param encounter A reference to the encounter
	 * @param target    The character to ambush
	 */
	public void promptShower(Encounter encounter, Character target) {
		clearCommand();
		this.commandPanel.add(new EncounterButton("Surprise " + target.pronounTarget(true),
				encounter,
				target,
				Encs.showerattack));
		if (!target.nude()) {
			this.commandPanel.add(new EncounterButton("Steal Clothes", encounter, target, Encs.stealclothes));
		}
		if (this.player.has(Flask.Aphrodisiac)) {
			this.commandPanel.add(new EncounterButton("Use Aphrodisiac", encounter, target, Encs.aphrodisiactrick));
		}
		this.commandPanel.add(new EncounterButton("Do Nothing", encounter, target, Encs.wait));
		Scheduler.pause();
		this.commandPanel.refresh();
	}

	public void promptIntervene(Encounter enc, Character p1, Character p2) {
		clearCommand();
		this.commandPanel.add(new InterveneButton(enc, p1));
		this.commandPanel.add(new InterveneButton(enc, p2));
		this.commandPanel.add(new EncounterButton("Watch", enc, p1, Encs.watch));
		Scheduler.pause();
		this.commandPanel.refresh();
	}

	/**
	 * Shows the specified prompt in the main body
	 *
	 * @param message The prompt message
	 * @param choices The choices available to the user
	 */
	public void prompt(String message, ArrayList<KeyableButton> choices) {
		if (!message.equals("")) {
			clearText();
			message(message);
		}

		clearCommand();
		for (var button : choices) {
			this.commandPanel.add(button);
		}
		this.commandPanel.refresh();
	}

	/**
	 * Displays the level-up screen to the user
	 */
	public void ding() {
		// Increase attributes
		if (this.player.attpoints > 0) {
			message(this.player.attpoints + " Attribute Points remain.\n");
			clearCommand();
			for (Attribute att : this.player.att.keySet()) {
				if (att.isTrainable()) {
					this.commandPanel.add(new AttributeButton(att));
				}
			}
			Scheduler.pause();
			this.commandPanel.refresh();
		}
		// Gain feats
		else if (player.countFeats() < player.getLevel() / 4) {
			message("You've earned a new perk. Select one below.");
			clearCommand();
			var available = false;
			for (var feat : Global.getFeats()) {
				if (!player.has(feat) && feat.meetsRequirement(player)) {
					this.commandPanel.add(new FeatButton(feat));
					available = true;
				}
				this.commandPanel.refresh();
			}
			// No feats available, done
			if (!available) {
				clearCommand();
				Global.gainSkills(this.player);
				endCombat();
			}
		}
		// Done, gain skills and end combat
		else {
			clearCommand();
			Global.gainSkills(this.player);
			endCombat();
		}
	}

	/**
	 * Clears combat related panels and switches back to the match map
	 */
	public void endCombat() {
		this.combat = null;
		clearText();
		resetPortrait();
		showMap();
		this.centerPanel.revalidate();
		this.portraitPanel.revalidate();
		this.portraitPanel.repaint();
		Scheduler.unpause();
	}

	/**
	 * Marks the match as started by making the area map visible
	 */
	public void startMatch() {
		this.mntmQuitMatch.setEnabled(true);
		showMap();
	}

	/**
	 * Ends the match and provides the player the choice to save or sleep
	 */
	public void endMatch() {
		clearCommand();
		Global.flag(Flag.night);
		this.mntmQuitMatch.setEnabled(false);
		this.commandPanel.add(new SleepButton());
		this.commandPanel.add(new SaveButton());
		this.commandPanel.refresh();
	}

	/**
	 * Refreshes the GUI to represent any changed information about the player
	 */
	public void refresh() {
		this.stamina.setText("Stamina: " + getLabelString(player.getStamina()));
		this.arousal.setText("Arousal: " + getLabelString(player.getArousal()));
		this.mojo.setText("Mojo: " + getLabelString(player.getMojo()));
		this.staminaBar.setMaximum(this.player.getStamina().max());
		this.staminaBar.setValue(this.player.getStamina().get());
		this.arousalBar.setMaximum(this.player.getArousal().max());
		this.arousalBar.setValue(this.player.getArousal().get());
		this.mojoBar.setMaximum(this.player.getMojo().max());
		this.mojoBar.setValue(this.player.getMojo().get());
		this.loclbl.setText(this.player.location().name);
		displayInventory();
		displayStatus();
		if (map != null) {
			map.repaint();
		}
		refreshLite();
	}

	/**
	 * Partially refreshes the GUI
	 * <p>
	 * Updates only the day/time/money labels
	 */
	public void refreshLite() {
		var day = Scheduler.getDayString(Scheduler.getDate());
		this.daylbl.setText(day);
		this.timelbl.setText(Scheduler.getTimeString());
		this.cashlbl.setText("$" + this.player.money);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		refresh();
		if (this.combat != null) {
			if (this.combat.phase != 2) {
				resetPortrait();
				loadPortrait(this.combat.p1, this.combat.p2);
			}
			combatMessage(this.combat.getMessage());
			this.combat.showImage();
			if ((this.combat.phase == 0) || (this.combat.phase == 2)) {
				next(this.combat);
			}
		}
	}

	/**
	 * Gets the number of skills that belong to the specified group
	 *
	 * @param group The tactics group to get the number of skills for
	 */
	public int nSkillsForGroup(TacticGroup group) {
		return skills.get(group).size();
	}

	/**
	 * Switches to the specified tactics group, displaying only skills belonging to it
	 *
	 * @param group The tactics group to switch to
	 */
	public void switchTactics(TacticGroup group) {
		groupBox.removeAll();
		currentTactics = group;
		Global.gui().showSkills();
	}

	/**
	 * Disables the options menu item
	 */
	public void disableOptions() {
		this.mntmOptions.setEnabled(false);
	}

	/**
	 * Enables the options menu item
	 */
	public void enableOptions() {
		this.mntmOptions.setEnabled(true);
	}

	/**
	 * Checks whether the low-res image mode is enabled
	 */
	public boolean lowRes() {
		return lowres;
	}

	/**
	 * Opens the closet GUI
	 *
	 * @param event The event to return to after changing clothes
	 */
	public void changeClothes(Activity event) {
		closet.update(event);
		var midLayout = (CardLayout) (midPanel.getLayout());
		midLayout.show(midPanel, USE_CLOSET_UI);
		midPanel.repaint();
	}

	/**
	 * Closes the closet GUI and switches to the main text
	 */
	public void removeClosetGUI() {
		var midLayout = (CardLayout) (midPanel.getLayout());
		midLayout.show(midPanel, USE_MAIN_TEXT_UI);
		midPanel.repaint();
		displayStatus();
	}

	/**
	 * Adds the specified button to the command panel
	 *
	 * @param button The button to add
	 */
	private void addToCommandPanel(KeyableButton button) {
		commandPanel.add(button);
	}

	private void buildMenuBar() {
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		var menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		var mntmNewgame = new JMenuItem("New Game");
		mntmNewgame.setForeground(textColor);
		mntmNewgame.setBackground(frameColor);
		mntmNewgame.setHorizontalAlignment(SwingConstants.CENTER);
		mntmNewgame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int result = JOptionPane.showConfirmDialog(GUI.this,
						"Do you want to restart the game? You'll lose any unsaved progress.", "Start new game?",
						JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
				if (result == JOptionPane.OK_OPTION) {
					Global.reset();
				}
			}
		});
		menuBar.add(mntmNewgame);
		JMenuItem mntmLoad = new JMenuItem("Load");
		mntmLoad.setForeground(textColor);
		mntmLoad.setBackground(frameColor);
		mntmLoad.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(mntmLoad);
		mntmLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SaveManager.load();
			}
		});
		// menu bar - options
		mntmOptions = new JMenuItem("Options");
		mntmOptions.setForeground(textColor);
		mntmOptions.setBackground(frameColor);
		menuBar.add(mntmOptions);
		// options submenu creator
		optionspanel = new JPanel();
		optionspanel.setLayout(new GridLayout(0, 3, 0, 0));
		JLabel lbldif = new JLabel("AI Difficulty");
		ButtonGroup dif = new ButtonGroup();
		rdnormal = new JRadioButton("Normal");
		rdhard = new JRadioButton("Hard");
		dif.add(rdnormal);
		dif.add(rdhard);
		optionspanel.add(lbldif);
		optionspanel.add(rdnormal);
		optionspanel.add(rdhard);

		JLabel lblgen = new JLabel("Combat Restrictions");
		ButtonGroup gentle = new ButtonGroup();
		rdgentleoff = new JRadioButton("Normal");
		rdgentleon = new JRadioButton("Gentle(Not Balanced)");
		optionspanel.add(lblgen);
		optionspanel.add(rdgentleoff);
		optionspanel.add(rdgentleon);

		JLabel lblauto = new JLabel("Autosave (saves to auto.sav)");
		ButtonGroup auto = new ButtonGroup();
		rdautosaveon = new JRadioButton("On");
		rdautosaveoff = new JRadioButton("Off");
		auto.add(rdautosaveon);
		auto.add(rdautosaveoff);
		optionspanel.add(lblauto);
		optionspanel.add(rdautosaveon);
		optionspanel.add(rdautosaveoff);

		ButtonGroup portype = new ButtonGroup();
		rdpor1 = new JRadioButton("Sprite");
		rdpor2 = new JRadioButton("Portrait");
		rdpor3 = new JRadioButton("None");
		portype.add(rdpor1);
		portype.add(rdpor2);
		portype.add(rdpor3);
		optionspanel.add(rdpor1);
		optionspanel.add(rdpor2);
		optionspanel.add(rdpor3);

		ButtonGroup image = new ButtonGroup();
		rdimgon = new JRadioButton("All Skill Images");
		rdimgoff = new JRadioButton("No Skill Images ");
		rdimgexact = new JRadioButton("Match Character");
		image.add(rdimgon);
		image.add(rdimgoff);
		image.add(rdimgexact);
		optionspanel.add(rdimgon);
		optionspanel.add(rdimgoff);
		optionspanel.add(rdimgexact);
		JLabel lblstsimg = new JLabel("Status Effects on Sprites");
		ButtonGroup stsimg = new ButtonGroup();
		rdstsimgon = new JRadioButton("On");
		rdstsimgoff = new JRadioButton("Off");
		stsimg.add(rdstsimgon);
		stsimg.add(rdstsimgoff);
		optionspanel.add(lblstsimg);
		optionspanel.add(rdstsimgon);
		optionspanel.add(rdstsimgoff);
		JLabel lblfnt = new JLabel("Font Size");
		fontspinner = new JSpinner(new SpinnerNumberModel(6, 5, 16, 1));

		optionspanel.add(lblfnt);
		optionspanel.add(fontspinner);
		optionspanel.add(new JPanel());
		JLabel lblrpt = new JLabel("Combat Reports");
		ButtonGroup reports = new ButtonGroup();
		rdrpt1 = new JRadioButton("On");
		rdrpt2 = new JRadioButton("Off");
		reports.add(rdrpt1);
		reports.add(rdrpt2);
		optionspanel.add(lblrpt);
		optionspanel.add(rdrpt1);
		optionspanel.add(rdrpt2);

		JLabel lblcol = new JLabel("Color Set");
		ButtonGroup colors = new ButtonGroup();
		rdcol1 = new JRadioButton("Default");
		rdcol2 = new JRadioButton("Dark");
		colors.add(rdcol1);
		colors.add(rdcol2);
		optionspanel.add(lblcol);
		optionspanel.add(rdcol1);
		optionspanel.add(rdcol2);

		Hashtable labelTable = new Hashtable();
		labelTable.put(1, new JLabel("0.25"));
		labelTable.put(4, new JLabel("1"));
		labelTable.put(8, new JLabel("2"));
		labelTable.put(12, new JLabel("3"));

		JLabel lblplayerscale = new JLabel("Player Progression Speed");
		playerScale = new JSlider(JSlider.HORIZONTAL, 1, 12, 1);
		playerScale.setValue(4);
		playerScale.setMinorTickSpacing(1);
		playerScale.setPaintTicks(true);
		playerScale.setLabelTable(labelTable);
		playerScale.setPaintLabels(true);

		JLabel lblnpcscale = new JLabel("NPC Progression Speed");
		npcScale = new JSlider(JSlider.HORIZONTAL, 1, 12, 1);
		npcScale.setValue(4);
		npcScale.setMinorTickSpacing(1);
		npcScale.setPaintTicks(true);
		npcScale.setLabelTable(labelTable);
		npcScale.setPaintLabels(true);

		optionspanel.add(lblplayerscale);
		optionspanel.add(playerScale);
		optionspanel.add(new JPanel());
		optionspanel.add(lblnpcscale);
		optionspanel.add(npcScale);
		optionspanel.add(new JPanel());

		mntmOptions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Global.checkFlag(Flag.hardmode)) {
					rdhard.setSelected(true);
				}
				else {
					rdnormal.setSelected(true);
				}
				if (Global.checkFlag(Flag.nopain)) {
					rdgentleon.setSelected(true);
				}
				else {
					rdgentleoff.setSelected(true);
				}
				if (Global.checkFlag(Flag.autosave)) {
					rdautosaveon.setSelected(true);
				}
				else {
					rdautosaveoff.setSelected(true);
				}
				if (Global.checkFlag(Flag.noportraits)) {
					rdpor3.setSelected(true);
				}
				else if (Global.checkFlag(Flag.portraits2)) {
					rdpor2.setSelected(true);
				}
				else {
					rdpor1.setSelected(true);
				}
				if (Global.checkFlag(Flag.noimage)) {
					rdimgoff.setSelected(true);
				}
				else if (Global.checkFlag(Flag.exactimages)) {
					rdimgexact.setSelected(true);
				}
				else {
					rdimgon.setSelected(true);
				}
				if (Global.checkFlag(Flag.statussprites)) {
					rdstsimgon.setSelected(true);
				}
				else {
					rdstsimgoff.setSelected(true);
				}
				fontspinner.getModel().setValue(Math.round(Global.getValue(Flag.fontsize)));
				if (Global.checkFlag(Flag.noReports)) {
					rdrpt2.setSelected(true);
				}
				else {
					rdrpt1.setSelected(true);
				}
				if (Global.checkFlag(Flag.altcolors)) {
					rdcol2.setSelected(true);
				}
				else {
					rdcol1.setSelected(true);
				}
				playerScale.setValue(Math.round(Global.getValue(Flag.PlayerScaling) / 0.25f));
				npcScale.setValue(Math.round(Global.getValue(Flag.NPCScaling) / 0.25f));
				int result = JOptionPane.showConfirmDialog(GUI.this,
						optionspanel,
						"Options",
						JOptionPane.OK_CANCEL_OPTION,
						JOptionPane.INFORMATION_MESSAGE);
				if (result == JOptionPane.OK_OPTION) {
					if (rdnormal.isSelected()) {
						Global.unflag(Flag.hardmode);
					}
					else {
						Global.flag(Flag.hardmode);
					}
					if (rdgentleon.isSelected()) {
						Global.flag(Flag.nopain);
					}
					else {
						Global.unflag(Flag.nopain);
					}
					if (rdautosaveoff.isSelected()) {
						Global.unflag(Flag.autosave);
					}
					else {
						Global.flag(Flag.autosave);
					}
					if (rdpor2.isSelected()) {
						Global.flag(Flag.portraits2);
						Global.unflag(Flag.noportraits);
					}
					else if (rdpor3.isSelected()) {
						Global.flag(Flag.noportraits);
						Global.unflag(Flag.portraits2);
						showNone();
					}
					else {
						Global.unflag(Flag.portraits2);
						Global.unflag(Flag.noportraits);
					}
					if (rdimgon.isSelected()) {
						Global.unflag(Flag.noimage);
						Global.unflag(Flag.exactimages);
					}
					else if (rdimgexact.isSelected()) {
						Global.unflag(Flag.noimage);
						Global.flag(Flag.exactimages);
					}
					else {
						Global.flag(Flag.noimage);
						Global.unflag(Flag.exactimages);
					}
					if (rdstsimgon.isSelected()) {
						Global.flag(Flag.statussprites);
					}
					else {
						Global.unflag(Flag.statussprites);
					}
					Global.setCounter(Flag.fontsize, (Integer) fontspinner.getModel().getValue());
					fontsize = (Integer) fontspinner.getModel().getValue();
					if (rdrpt2.isSelected()) {
						Global.flag(Flag.noReports);
					}
					else {
						Global.unflag(Flag.noReports);
					}
					Global.setCounter(Flag.PlayerScaling, playerScale.getValue() * 0.25f);
					Global.setCounter(Flag.NPCScaling, npcScale.getValue() * 0.25f);
					if (rdcol2.isSelected()) {
						Global.flag(Flag.altcolors);
						textColor = Constants.ALTTEXTCOLOR;
						frameColor = Constants.ALTFRAMECOLOR;
						backgroundColor = Constants.ALTBGCOLOR;
						refreshColors();
					}
					else {
						Global.unflag(Flag.altcolors);
						textColor = Constants.PRIMARYTEXTCOLOR;
						frameColor = Constants.PRIMARYFRAMECOLOR;
						backgroundColor = Constants.PRIMARYBGCOLOR;
						refreshColors();
					}
				}
			}
		});

		// credits
		var mntmCredits = new JMenuItem("Credits");
		mntmCredits.setForeground(textColor);
		mntmCredits.setBackground(backgroundColor);
		menuBar.add(mntmCredits);

		// supporters
		var mntmPatrons = new JMenuItem("Supporters");
		mntmPatrons.setForeground(textColor);
		mntmPatrons.setBackground(backgroundColor);
		menuBar.add(mntmPatrons);

		// quit match
		mntmQuitMatch = new JMenuItem("Quit Match");
		mntmQuitMatch.setEnabled(false);
		mntmQuitMatch.setForeground(textColor);
		mntmQuitMatch.setBackground(backgroundColor);
		mntmQuitMatch.addActionListener(arg0 -> {
			var result = JOptionPane.showConfirmDialog(GUI.this,
					"Do you want to quit for the night? Your opponents will continue to fight and gain exp.",
					"Retire early?",
					JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.INFORMATION_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				Scheduler.getMatch().quit();
			}
		});
		menuBar.add(mntmQuitMatch);
		mntmCredits.addActionListener(arg0 -> JOptionPane.showMessageDialog(GUI.this,
				"Design:\n"
						+ "    The Silver Bard\n\n"
						+ "Art:\n"
						+ "    AimlessArt,\n"
						+ "    Fujin Hitokiri,\n"
						+ "    Sky Relyks,\n"
						+ "    Phanaxial\n\n"
						+ "Development:\n"
						+ "    The Silver Bard,\n"
						+ "    Nergantre,\n"
						+ "    dndw,\n"
						+ "    Jos,\n"
						+ "    InvalidCharacter,\n"
						+ "    GruntledDev\n\n" +
						"Additional Characters by:\n" +
						"    dndw,\n" +
						"    Napebaf,\n" +
						"    J\n\n"
						+ "Writing:\n"
						+ "    The Silver Bard,\n"
						+ "    dndw,\n"
						+ "    Onyxdime,\n"
						+ "    Legion,\n"
						+ "    GruntedDev,\n"
						+ "    Jotor,\n"
						+ "    Vica,\n"
						+ "    Sakruff,\n"
						+ "    KajunKrust,\n"
						+ "    Marcus Koen,\n"
						+ "    CK,\n"
						+ "    Rex,\n"
						+ "    DJ Writer,\n"
						+ "    WT Cash,\n"
						+ "    KVernik,\n"
						+ "    banana,\n"
						+ "    Anon64,\n"
						+ "    Napebaf,\n"
						+ "    Whitman,\n"
						+ "    Game Salamander,\n"
						+ "    Guardian Soul, \n" +
						"    Scrublord,\n" +
						"    Dosrev,\n" +
						"    Fapworthy Diction,\n" +
						"    GM Kcalb,\n" +
						"    J"));
		mntmPatrons.addActionListener(arg0 -> JOptionPane.showMessageDialog(GUI.this,
				"The Darklord, Jay S, Richard Penner, bob bobberson, John Aspuria, Brownell Combs, Unsung, Jaryd, Logan Aragon, Evil_kiwi,\n " +
						"Game Salamander, CJ, Maisson Phelps, Tyler, Somedude, Kasev, Quintin Blake, Mark Owen, Banana, Ramon Diaz, S Fly,\n " +
						"Akira Reinheart, Josh, CorrupterAxel, Rob Brance, Jasmine, Kou Mao, BabylonKing, SillyGordo, Bob Jones, PureKaos,\n " +
						"fake name, Drew Smith, derek floyd, John Doe, Shames90210, Jacob Ware, KitsunesGame, Mistor Games, Maurice Spicer,\n " +
						"Hahn Ackles, Rolfo, onenine, Glenn McGurrin, pebismani, Piotr Januszewski, Portent, Marcus Doyle, Somira, dawewq,\n " +
						"Sylvester Marciniak, TheMuseBattle, Phil Bill, Trisx, Mark Griffin, John Bridges, XanderPre Development, Brian Hobbs,\n " +
						"Aaron, Aghjil, Heliosbull, Grnd91, Bryan Gillis, Jason Joers, Jeff Hungerford, Matthew McConnell, Gul4sch, turkeymann,\n " +
						"Kaerea Shine, Chad Rast, MB, Musasy, Dan, M, JC, Rune Johansen, MacBuchi, AimlessArt, Tenten1010, Apoc326, Justin,\n " +
						"Quite Shallow, D, Bavari, TheApplebane, Drew, Jahkari Robinson, finn, Reginald C Dawson, Shettern, Derp Derpersson,\n " +
						"Horizon, Rasmus Victor Johnsson, Zivich, Dustin Parrish, Kevin, Brian Sheppard, Chit, Gia Trang, Anomander Rake,\n " +
						"Nicholas Diamante, BearPerson, Some Guy, Azione, Cameron, Dave McCl, griege, DragonPiggy, Bob Fruman, The HiGHDOLM@STER,\n " +
						"Jacob Grindstaff, Bannhammer, Pezantri, Ilsig, Foridin, karumi kaizen, james ramage, Ryan Krause, Zeke, Arcadion,\n " +
						"Derek Cheong, Dylan Devenny, John Echols, Hippoblue64, Ken Arthur, Clap Hands, Tai Parry, Christopher Nicdao, Rick Fox,\n " +
						"Evan Haukenfrers, Aureate_Folly, AeronGreva, cs, Aaron Hand, Jeremy Drakeford, A D, JaB, kaze31, Deveroux02, Flintfire,\n " +
						"kemious, Ker, RHR, SirSquidjr, 64bitrobot, K, not a name, Makido205, Brian Timothy, Invert_87, ph, Big Daddy Cool, SocomSeal,\n " +
						"Alex Lopez, Joshua Smith, Kirby, dommsdominickchung, Loser4Ever, Kagith, Michael Pechawer, William Reynolds, Thomas Gray,\n " +
						"brian, Justin Smith, Apestativ, enemygunman, Cheap Shot, Aederrex, Jamie, Toast Baron, tjd, TMoD36, James (Burnthemage),\n " +
						"Cameron Kulper, Jake Stirkul, dumbname, Richard Arsonault, Laughterglow, Bartholemew Case, Dvspuss, Kaine, Qianwei,\n " +
						"Andrew Findlay, Zach, AzureShade, Yuan, nobody, MePe, Necro Vee, Znewbie R, Cloudmoon, Alan, Job, nic611, Nathan Lindy,\n " +
						"Nforte, narori sayo, katro, SD, Jim Stauble, Nick Pazer, Scott Throgmorton, Apprentice Tinkerer, LordJerle, 0, Jayson tremblay,\n " +
						"EzarTek, Roger Faux, Taco Joe, Smith, Johan Hedlund, Justin Boley, "));
	}

	/**
	 * Switches to the character creation GUI
	 */
	private void createCharacter() {
		getContentPane().remove(this.mainpanel);
		this.creation = new CreationGUI(this);
		getContentPane().add(this.creation);
	}

	/**
	 * Displays the player's inventory, consisting of their items and an image of the clothes currently worn
	 */
	private void displayInventory() {
		this.inventoryPanel.removeAll();
		this.inventoryPanel.repaint();
		var itemPane = new JPanel();
		var itemScroll = new JScrollPane();
		itemPane.setBackground(backgroundColor);
		itemPane.setLayout(new BoxLayout(itemPane, BoxLayout.Y_AXIS));
		itemScroll.setViewportView(itemPane);

		var clothingPane = new JPanel();
		clothingPane.setBackground(frameColor);
		inventoryPanel.add(itemScroll);
		inventoryPanel.add(clothingPane, BorderLayout.SOUTH);

		var items = this.player.listInventory();
		var count = 0;
		var itemLabels = new ArrayList<JLabel>();
		for (var item : items.keySet()) {
			if (item.listed()) {
				itemLabels.add(count, new JLabel(item.getName() + ": " + items.get(item)));
				itemLabels.get(count).setForeground(textColor);
				itemLabels.get(count).setFont(new Font("Georgia", Font.BOLD, 16));
				itemLabels.get(count).setToolTipText(item.getFullDesc(this.player));
				itemPane.add(itemLabels.get(count));
				count++;
			}
		}
		BufferedImage clothesIcon;
		if (this.player.top.isEmpty()) {
			if (this.player.bottom.isEmpty()) {
				clothesIcon = loadImage("clothes0.png");
			}
			else if (this.player.bottom.peek().getType() == ClothingType.BOTOUTER) {
				clothesIcon = loadImage("clothes4.png");
			}
			else {
				clothesIcon = loadImage("clothes2.png");
			}
		}
		else if (this.player.top.peek().getType() == ClothingType.TOPOUTER) {
			if (this.player.bottom.isEmpty()) {
				clothesIcon = loadImage("clothes6.png");
			}
			else if (this.player.bottom.peek().getType() == ClothingType.BOTOUTER) {
				clothesIcon = loadImage("clothes8.png");
			}
			else {
				clothesIcon = loadImage("clothes7.png");
			}
		}
		else {
			if (this.player.bottom.isEmpty()) {
				clothesIcon = loadImage("clothes1.png");
			}
			else if (this.player.bottom.peek().getType() == ClothingType.BOTOUTER) {
				clothesIcon = loadImage("clothes5.png");
			}
			else {
				clothesIcon = loadImage("clothes3.png");
			}
		}

		if (clothesIcon != null) {
			if (Global.checkFlag(Flag.statussprites)) {
				var sts = loadStatusFilters(this.player);
				if (sts != null) {
					var scaled = sts.getScaledInstance(clothesIcon.getWidth(),
							clothesIcon.getHeight(),
							BufferedImage.SCALE_FAST);
					clothesIcon.createGraphics().drawImage(scaled, 0, 0, null);
				}
			}
			this.clothesdisplay = new JLabel(new ImageIcon(clothesIcon));
			clothingPane.add(this.clothesdisplay);
		}
		this.centerPanel.revalidate();
		this.inventoryPanel.revalidate();
		this.inventoryPanel.repaint();
	}

	/**
	 * Displays the status panels providing information about the player
	 */
	private void displayStatus() {
		this.statusPanel.removeAll();
		statusPanel.setMinimumSize(new Dimension(500, centerPanel.getHeight()));
		statusPanel.setPreferredSize(new Dimension(500, centerPanel.getHeight()));

		if (width < 720) {
			statusPanel.setMaximumSize(new Dimension(width / 3, height));
			System.out.println("STATUS PANEL");
		}

		var statsLeft = new JPanel();
		var statsRight = new JPanel();
		statusPanel.add(statsLeft);
		statusPanel.add(statsRight);
		statsLeft.setMinimumSize(new Dimension(250, centerPanel.getHeight()));
		statsRight.setMinimumSize(new Dimension(250, centerPanel.getHeight()));

		statsLeft.setLayout(new BoxLayout(statsLeft, BoxLayout.Y_AXIS));
		statsLeft.setAlignmentY(TOP_ALIGNMENT);
		statsRight.setLayout(new BoxLayout(statsRight, BoxLayout.Y_AXIS));
		statsRight.setAlignmentY(TOP_ALIGNMENT);

		var statsPanel = new JPanel();
		statsPanel.setLayout(new BoxLayout(statsPanel, BoxLayout.Y_AXIS));
		statsPanel.setMinimumSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight() / 3));

		var currentStatusPanel = new JPanel();
		currentStatusPanel.setLayout(new BoxLayout(currentStatusPanel, BoxLayout.Y_AXIS));
		currentStatusPanel.setMinimumSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight() / 3));

		var traitPanel = new JPanel();
		traitPanel.setLayout(new BoxLayout(traitPanel, BoxLayout.Y_AXIS));
		traitPanel.setMinimumSize(new Dimension(statsLeft.getWidth(), centerPanel.getHeight() / 2));

		var bioPanel = new JPanel();
		bioPanel.setLayout(new BoxLayout(bioPanel, BoxLayout.Y_AXIS));
		bioPanel.setMinimumSize(new Dimension(statsLeft.getWidth(), centerPanel.getHeight() / 3));

		var statsScroll = new JScrollPane();
		statsScroll.setPreferredSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight() / 2));
		var traitScroll = new JScrollPane();
		traitScroll.setPreferredSize(new Dimension(statsRight.getWidth(), 3 * centerPanel.getHeight() / 2));
		var statusScroll = new JScrollPane();
		statusScroll.setPreferredSize(new Dimension(statsRight.getWidth(), centerPanel.getHeight() / 2));

		var sep = new JSeparator();
		sep.setPreferredSize(new Dimension(statsLeft.getWidth(), 2));
		statsLeft.add(sep);
		var bioLbl = new JLabel("Player Info");
		bioLbl.setForeground(textColor);
		bioLbl.setFont(new Font("Georgia", Font.BOLD, 24));
		statsLeft.add(bioLbl);
		sep = new JSeparator();
		sep.setPreferredSize(new Dimension(statsLeft.getWidth(), 2));
		statsLeft.add(sep);

		var name = new JLabel(player.name());
		name.setForeground(textColor);
		name.setFont(new Font("Georgia", Font.BOLD, 20));
		var level = new JLabel("Level: " + player.getLevel());
		level.setForeground(textColor);
		level.setFont(new Font("Georgia", Font.BOLD, 20));
		var exp = new JLabel("XP: " + player.getXP());
		exp.setForeground(textColor);
		exp.setFont(new Font("Georgia", Font.BOLD, 20));
		var rank = new JLabel("Rank: " + player.getRank());
		rank.setForeground(textColor);
		rank.setFont(new Font("Georgia", Font.BOLD, 20));
		bioPanel.add(name);
		bioPanel.add(level);
		bioPanel.add(exp);
		bioPanel.add(rank);
		statsLeft.add(bioPanel);

		sep = new JSeparator();
		sep.setPreferredSize(new Dimension(statsRight.getWidth(), 2));
		statsRight.add(sep);
		var attributesHeaderLabel = new JLabel("Attributes");
		attributesHeaderLabel.setForeground(textColor);
		attributesHeaderLabel.setFont(new Font("Georgia", Font.BOLD, 24));
		statsRight.add(attributesHeaderLabel);
		statsRight.add(statsScroll);
		statsScroll.setViewportView(statsPanel);

		sep = new JSeparator();
		sep.setPreferredSize(new Dimension(statsLeft.getWidth(), 2));
		statsLeft.add(sep);
		var traitsHeaderLabel = new JLabel("Traits");
		traitsHeaderLabel.setForeground(textColor);
		traitsHeaderLabel.setFont(new Font("Georgia", Font.BOLD, 24));
		statsLeft.add(traitsHeaderLabel);
		statsLeft.add(traitScroll);
		traitScroll.setViewportView(traitPanel);

		sep = new JSeparator();
		sep.setPreferredSize(new Dimension(statsRight.getWidth(), 2));
		statsRight.add(sep);
		var statusHeaderLabel = new JLabel("Current Status");
		statusHeaderLabel.setForeground(textColor);
		statusHeaderLabel.setFont(new Font("Georgia", Font.BOLD, 24));
		statsRight.add(statusHeaderLabel);
		statsRight.add(statusScroll);
		statusScroll.setViewportView(currentStatusPanel);

		statsLeft.setBackground(backgroundColor);
		statsRight.setBackground(backgroundColor);
		bioPanel.setBackground(backgroundColor);
		traitPanel.setBackground(backgroundColor);
		currentStatusPanel.setBackground(backgroundColor);
		statsPanel.setBackground(backgroundColor);

		var count = 0;
		var attributeLabels = new ArrayList<JLabel>();
		for (var attribute : player.att.keySet()) {
			var amt = player.getEffective(attribute);
			if (amt > 0) {
				var label = new JLabel(attribute.toString() + ": " + amt);
				label.setForeground(textColor);
				label.setFont(new Font("Georgia", Font.BOLD, 16));
				label.setHorizontalAlignment(SwingConstants.CENTER);
				label.setToolTipText(attribute.getDescription());
				attributeLabels.add(count, label);

				statsPanel.add(attributeLabels.get(count));
				count++;
			}
		}
		count = 0;
		var traitLabels = new ArrayList<JLabel>();
		for (var trait : player.traits) {
			var label = new JLabel(trait.toString());
			label.setForeground(textColor);
			label.setFont(new Font("Georgia", Font.BOLD, 16));
			label.setHorizontalAlignment(SwingConstants.CENTER);
			label.setToolTipText(trait.getDesc());
			traitLabels.add(count, label);

			traitPanel.add(traitLabels.get(count));
			count++;
		}
		count = 0;
		var statusLabels = new ArrayList<JLabel>();
		for (var status : player.getStatus()) {
			var label = new JLabel(status.toString());
			label.setForeground(textColor);
			label.setFont(new Font("Georgia", Font.BOLD, 16));
			label.setHorizontalAlignment(SwingConstants.CENTER);
			label.setToolTipText(status.getTooltip());
			statusLabels.add(count, label);

			currentStatusPanel.add(statusLabels.get(count));
			count++;
		}
		if (player.listStatus().isEmpty()) {
			var noStatus = new JLabel("No status effects");
			noStatus.setForeground(textColor);
			noStatus.setFont(new Font("Georgia", Font.BOLD, 18));
			currentStatusPanel.add(noStatus);
		}
		this.statusPanel.repaint();
		this.statusPanel.revalidate();
		this.centerPanel.revalidate();
	}

	/**
	 * Loads an image from the specified resource path
	 *
	 * @param path The path to the image resource
	 * @return The loaded image, or Null if none was found, or the image failed to load
	 */
	private BufferedImage loadImage(String path) {
		var url = getClass().getResource(path);
		if (url == null) {
			return null;
		}

		try {
			return ImageIO.read(url);
		}
		catch (IOException ignored) {
			return null;
		}
	}

	/**
	 * Adds a button to advance to the next combat turn
	 *
	 * @param combat A reference to the ongoing combat
	 */
	private void next(Combat combat) {
		refresh();
		clearCommand();
		this.commandPanel.add(new NextTurnButton(combat));
		Scheduler.pause();
		this.commandPanel.refresh();
	}

	private void refreshColors() {
		getContentPane().remove(this.mainpanel);
		this.topPanel.removeAll();
		populatePlayer(Global.getPlayer());
		displayInventory();
		displayStatus();
		this.inventoryPanel.setBackground(frameColor);
		this.statusPanel.setBackground(frameColor);
		this.commandPanel.setBackground(frameColor);
		this.textPane.setForeground(textColor);
		this.textPane.setBackground(backgroundColor);
	}
}