package gui;

import daytime.Activity;

import java.awt.*;

/**
 * Defines a button used to perform an activity
 */
public class ActivityButton extends KeyableButton {
	public ActivityButton(Activity act) {
		super(act.toString());
		setFont(new Font("Georgia", Font.PLAIN, 18));
		addActionListener(arg0 -> act.visit("Start"));
	}

	public ActivityButton(Activity act, String description) {
		this(act);
		this.setToolTipText(description);
	}
}