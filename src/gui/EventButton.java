package gui;

import daytime.Activity;
import global.Global;

import java.awt.*;

/**
 * Defines a button that runs an event
 */
public class EventButton extends KeyableButton {
	public EventButton(Activity event, String choice) {
		super(choice);
		setFont(new Font("Georgia", Font.PLAIN, 18));
		addActionListener(arg0 -> {
			event.visit(choice);
			Global.gui().refreshLite();
		});
	}

	public EventButton(Activity event, String choice, String description) {
		this(event, choice);
		this.setToolTipText(description);
	}
}
