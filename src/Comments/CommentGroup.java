package Comments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class CommentGroup {
	private final HashMap<CommentTrigger, ArrayList<String>> comments;

	public CommentGroup() {
		comments = new HashMap<>();
	}

	/**
	 * Adds the specified comment to the group
	 *
	 * @param situation The condition causing the comment to trigger
	 * @param comment   The comment
	 */
	public void put(CommentTrigger situation, String comment) {
		if (!comments.containsKey(situation)) {
			comments.put(situation, new ArrayList<>());
		}
		comments.get(situation).add(comment);
	}

	/**
	 * Gets all conditions defined in the group
	 */
	public Set<CommentTrigger> getTriggers() {
		return comments.keySet();
	}

	/**
	 * Gets all comments applicable to the specified trigger
	 *
	 * @param situation The trigger to check for
	 */
	public ArrayList<String> getComments(CommentTrigger situation) {
		return comments.get(situation);
	}
}
