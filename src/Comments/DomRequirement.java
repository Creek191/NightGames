package Comments;

import characters.Character;
import combat.Combat;

/**
 * Defines a requirement related to a dominant stance
 */
public class DomRequirement implements CustomRequirement {
	@Override
	public boolean meets(Combat c, Character self, Character other) {
		if (c == null) {
			return false;
		}
		return c.stance.dom(self);
	}
}