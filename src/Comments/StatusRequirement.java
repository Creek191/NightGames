package Comments;

import characters.Character;
import combat.Combat;
import status.Stsflag;

/**
 * Defines a requirement triggered by a specific status flag
 */
public class StatusRequirement implements CustomRequirement {

	private final Stsflag flag;

	public StatusRequirement(String flag) {
		this(Stsflag.valueOf(flag));
	}

	public StatusRequirement(Stsflag flag) {
		this.flag = flag;
	}

	@Override
	public boolean meets(Combat c, Character self, Character other) {
		if (c == null || flag == null)
			return false;

		return self.getStatus(flag) != null;
	}
}