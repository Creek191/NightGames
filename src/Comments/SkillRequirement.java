package Comments;

import characters.Character;
import combat.Combat;
import skills.Skill;

public class SkillRequirement implements CustomRequirement {
	private Skill skill;
	
	public SkillRequirement(Skill skill){
		this.skill = skill;
	}
	
	@Override
	public boolean meets(Combat c, Character self, Character other) {
		
		return skill.getClass().isInstance(c.lastAction(self));
	}

}
