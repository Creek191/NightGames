package Comments;

import characters.Character;
import combat.Combat;

/**
 * Defines a condition that triggers a comment during combat
 */
public interface CommentTrigger {

	/**
	 * Checks whether the trigger is applicable to the current state of combat
	 *
	 * @param c     A reference to the ongoing combat
	 * @param self  The character checking the requirement
	 * @param other The other character that is part of the fight
	 */
	boolean isApplicable(Combat c, Character self, Character other);

	/**
	 * Gets the priority of the trigger
	 * <p>
	 * If multiple triggers are applicable, the one with the highest priority will happen
	 */
	int getPriority();

	/**
	 * Gets the probability of an applicable comment actually triggering
	 * <p>
	 * Ensures that a comment won't keep repeating every turn
	 *
	 * @return An integer between 0 (never) and 100 (always)
	 */
	int getProbability();

	/**
	 * Checks whether the specified object equals this one
	 *
	 * @param o The object to check
	 */
	boolean equals(Object o);
}
