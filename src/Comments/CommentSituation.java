package Comments;

import characters.Character;
import combat.Combat;
import global.Global;
import stance.Stance;
import status.Stsflag;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Defines a list of pre-defined comment triggers
 */
public enum CommentSituation implements CommentTrigger {
	// Fucking
	VAG_DOM_PITCH_WIN(2, 40, new InsertedRequirement(true),
			rev(new AnalRequirement(false)), new DomRequirement(),
			new WinningRequirement()),
	VAG_DOM_PITCH_LOSE(2, 40, new InsertedRequirement(true),
			rev(new AnalRequirement(false)), new DomRequirement(),
			rev(new WinningRequirement())),
	VAG_DOM_CATCH_WIN(2, 40, rev(new InsertedRequirement(true)),
			new AnalRequirement(false), new DomRequirement(),
			new WinningRequirement()),
	VAG_DOM_CATCH_LOSE(2, 40, rev(new InsertedRequirement(true)),
			new AnalRequirement(false), new DomRequirement(),
			rev(new WinningRequirement())),
	VAG_SUB_PITCH_WIN(2, 40, new InsertedRequirement(true),
			rev(new AnalRequirement(false)), new SubRequirement(),
			new WinningRequirement()),
	VAG_SUB_PITCH_LOSE(2, 40, new InsertedRequirement(true),
			rev(new AnalRequirement(false)), new SubRequirement(),
			rev(new WinningRequirement())),
	VAG_SUB_CATCH_WIN(2, 40, rev(new InsertedRequirement(true)),
			new AnalRequirement(false), new SubRequirement(),
			new WinningRequirement()),
	VAG_SUB_CATCH_LOSE(2, 40, rev(new InsertedRequirement(true)),
			new AnalRequirement(false), new SubRequirement(),
			rev(new WinningRequirement())),
	ANAL_PITCH_WIN(2, 40, new InsertedRequirement(true),
			rev(new AnalRequirement(true)),
			new WinningRequirement()),
	ANAL_PITCH_LOSE(2, 40, new InsertedRequirement(true),
			rev(new AnalRequirement(true)),
			rev(new WinningRequirement())),
	ANAL_CATCH_WIN(2, 40, rev(new InsertedRequirement(true)),
			new AnalRequirement(true),
			new WinningRequirement()),
	ANAL_CATCH_LOSE(2, 40, rev(new InsertedRequirement(true)),
			new AnalRequirement(true),
			rev(new WinningRequirement())),

	// Stances
	BEHIND_DOM_WIN(1, 30, new StanceRequirement(Stance.behind), new DomRequirement(),
			new WinningRequirement()),
	BEHIND_DOM_LOSE(1, 30, new StanceRequirement(Stance.behind), new DomRequirement(),
			rev(new WinningRequirement())),
	BEHIND_SUB_WIN(1, 30, new StanceRequirement(Stance.behind), new SubRequirement(),
			new WinningRequirement()),
	BEHIND_SUB_LOSE(1, 30, new StanceRequirement(Stance.behind), new SubRequirement(),
			rev(new WinningRequirement())),
	SIXTYNINE_WIN(1, 30, new StanceRequirement(Stance.sixnine), new WinningRequirement()),
	SIXTYNINE_LOSE(1, 30, new StanceRequirement(Stance.sixnine), rev(new WinningRequirement())),
	MOUNT_DOM_WIN(1, 30, new StanceRequirement(Stance.mount), new DomRequirement(),
			new WinningRequirement()),
	MOUNT_DOM_LOSE(1, 30, new StanceRequirement(Stance.mount), new DomRequirement(),
			rev(new WinningRequirement())),
	MOUNT_SUB_WIN(1, 30, new StanceRequirement(Stance.mount), new SubRequirement(),
			new WinningRequirement()),
	MOUNT_SUB_LOSE(1, 30, new StanceRequirement(Stance.mount), new SubRequirement(),
			rev(new WinningRequirement())),
	PIN_DOM_WIN(1, 30, new StanceRequirement(Stance.pin), new DomRequirement(),
			new WinningRequirement()),
	PIN_DOM_LOSE(1, 30, new StanceRequirement(Stance.pin), new DomRequirement(),
			rev(new WinningRequirement())),
	PIN_SUB_WIN(1, 30, new StanceRequirement(Stance.pin), new SubRequirement(),
			new WinningRequirement()),
	PIN_SUB_LOSE(1, 30, new StanceRequirement(Stance.pin), new SubRequirement(),
			rev(new WinningRequirement())),
	FACESIT_DOM_WIN(1, 30, new StanceRequirement(Stance.facesitting), new DomRequirement(),
			new WinningRequirement()),
	FACESIT_DOM_LOSE(1, 30, new StanceRequirement(Stance.facesitting), new DomRequirement(),
			rev(new WinningRequirement())),
	FACESIT_SUB_WIN(1, 30, new StanceRequirement(Stance.facesitting), new SubRequirement(),
			new WinningRequirement()),
	FACESIT_SUB_LOSE(1, 30, new StanceRequirement(Stance.facesitting), new SubRequirement(),
			rev(new WinningRequirement())),

	// Statuses
	SELF_BOUND(0, 30, new StatusRequirement(Stsflag.bound)),
	OTHER_BOUND(0, 30, rev(new StatusRequirement(Stsflag.bound))),
	OTHER_STUNNED(0, 30, rev(new StatusRequirement(Stsflag.stunned))),
	SELF_CHARMED(0, 30, new StatusRequirement(Stsflag.charmed)),
	OTHER_CHARMED(0, 30, rev(new StatusRequirement(Stsflag.charmed))),
	OTHER_ENTHRALLED(0, 30, rev(new StatusRequirement(Stsflag.enthralled))),
	SELF_HORNY(0, 30, new StatusRequirement(Stsflag.horny)),
	OTHER_HORNY(0, 30, rev(new StatusRequirement(Stsflag.horny))),
	SELF_OILED(0, 30, new StatusRequirement(Stsflag.oiled)),
	OTHER_OILED(0, 30, rev(new StatusRequirement(Stsflag.oiled))),
	SELF_SHAMED(0, 30, new StatusRequirement(Stsflag.shamed)),
	OTHER_SHAMED(0, 30, rev(new StatusRequirement(Stsflag.shamed))),
	NO_COMMENT(-1, 0),

	//Event
	SELF_BUSTED(5, 100, new LowBlowRequirement()),
	OTHER_BUSTER(5, 50, rev(new LowBlowRequirement())),
	SELF_PET(1, 30, new PetRequirement()),
	OTHER_PET(1, 30, rev(new PetRequirement()));

	private final int priority;
	private final int probability;
	private final Set<CustomRequirement> reqs;

	CommentSituation(int priority, int probability, CustomRequirement... reqs) {
		this.priority = priority;
		this.probability = probability;
		this.reqs = Collections
				.unmodifiableSet(new HashSet<>((Arrays.asList(reqs))));
	}

	@Override
	public boolean isApplicable(Combat c, Character self, Character other) {
		for (var r : reqs) {
			if (!r.meets(c, self, other)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int getPriority() {
		return priority;
	}

	@Override
	public int getProbability() {
		return probability;
	}

	/**
	 * Gets all comments applicable to the current state of combat
	 *
	 * @param c     A reference to the ongoing combat
	 * @param self  The character checking the requirement
	 * @param other The other character that is part of the fight
	 * @return A set of comments
	 */
	public static Set<CommentSituation> getApplicableComments(Combat c, Character self, Character other) {
		Set<CommentSituation> comments = new HashSet<>();
		for (var comment : CommentSituation.values()) {
			if (comment.isApplicable(c, self, other)) {
				comments.add(comment);
			}
		}
		if (comments.isEmpty()) {
			return Collections.singleton(NO_COMMENT);
		}
		return comments;
	}

	/**
	 * Gets the most applicable comment for the current state of combat
	 *
	 * @param offered The comment group to check
	 * @param c       A reference to the ongoing combat
	 * @param self    The character checking the requirement
	 * @param other   The other character that is part of the fight
	 * @return A string containing the comment with the highest priority, or Null if none was found
	 */
	public static String getBestComment(CommentGroup offered, Combat c, Character self, Character other) {
		CommentTrigger best = null;
		for (var comment : offered.getTriggers()) {
			if (comment.isApplicable(c, self, other) && !offered.getComments(comment).isEmpty()) {
				if (best == null || comment.getPriority() > best.getPriority()) {
					best = comment;
				}
			}
		}
		if (best == null || Global.random(100) > best.getProbability()) {
			return null;
		}
		return Global.pickRandom(offered.getComments(best).toArray());
	}

	/**
	 * Creates a variant of the specified requirement that inverts the characters checked
	 *
	 * @param req The requirement to invert
	 */
	private static CustomRequirement rev(CustomRequirement req) {
		return new ReverseRequirement(Arrays.asList(req));
	}
}