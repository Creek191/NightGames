package Comments;

import characters.Character;
import combat.Combat;

/**
 * Defines a requirement related to the presence of a pet
 */
public class PetRequirement implements CustomRequirement {

	@Override
	public boolean meets(Combat c, Character self, Character other) {
		return self.pet != null;
	}
}
