package Comments;

import characters.Character;
import combat.Combat;

/**
 * Defines a requirement related to penetration
 */
public class InsertedRequirement implements CustomRequirement {
	boolean inserted;

	public InsertedRequirement(boolean inserted) {
		this.inserted = inserted;
	}

	@Override
	public boolean meets(Combat c, Character self, Character other) {
		if (c == null) {
			return false;
		}
		return c.stance.penetration(self) == inserted;
	}
}
