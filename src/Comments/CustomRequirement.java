package Comments;

import characters.Character;
import combat.Combat;

public interface CustomRequirement {
	/**
	 * Checks whether an ongoing fight meets a requirement
	 *
	 * @param c     A reference to the ongoing fight
	 * @param self  The character checking the requirement
	 * @param other The other character that is part of the fight
	 */
	boolean meets(Combat c, Character self, Character other);
}
