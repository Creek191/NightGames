package Comments;

import characters.Anatomy;
import characters.Character;
import combat.Combat;
import combat.Result;

/**
 * Defines a requirement related to a painful hit to the genitals
 */
public class LowBlowRequirement implements CustomRequirement {

	@Override
	public boolean meets(Combat c, Character self, Character other) {
		for (var event : c.getEvents(self)) {
			if (event.getEvent() == Result.receivepain && event.getPart() == Anatomy.genitals) {
				return true;
			}
		}
		return false;
	}
}
