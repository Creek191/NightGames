package Comments;

import characters.Character;
import combat.Combat;
import combat.Result;

/**
 * Defines a comment based on a combat event
 */
public class EventComment implements CommentTrigger {
    private static final int DEFAULTPRIORITY = 3;
    private static final int DEFAULTPROBABILITY = 100;

    private final int priority;
    private final int probability;
    private final Result event;

    public EventComment(Result event) {
        this(DEFAULTPRIORITY, DEFAULTPROBABILITY, event);
    }

    public EventComment(int priority, int probability, Result event) {
        this.priority = priority;
        this.probability = probability;
        this.event = event;
    }

    @Override
    public boolean isApplicable(Combat c, Character self, Character other) {
        for (var event : c.getEvents(self)) {
            if (event.getEvent() == this.event) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public int getProbability() {
        return probability;
    }

    @Override
    public boolean equals(Object o) {

        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of EventComment or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof EventComment)) {
            return false;
        }

        // typecast o to EventComment so that we can compare data members
        var c = (EventComment) o;

        // Compare the data members and return accordingly
        return c.event == event;
    }
}
