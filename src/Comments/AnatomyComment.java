package Comments;

import characters.Anatomy;
import characters.Character;
import combat.Combat;
import combat.Result;

/**
 * Defines a comment based on a character's anatomy
 */
public class AnatomyComment implements CommentTrigger {
    private static final int DEFAULTPRIORITY = 2;
    private static final int DEFAULTPROBABILITY = 50;
    
    private final int priority;
    private final int probability;
    private final Result event;
    private final Anatomy part;

    public AnatomyComment(Result event, Anatomy part) {
        this(DEFAULTPRIORITY, DEFAULTPROBABILITY, event, part);
    }

    public AnatomyComment(int priority, int probability, Result event, Anatomy part) {
        this.priority = priority;
        this.probability = probability;
        this.event = event;
        this.part = part;
    }

    @Override
    public boolean isApplicable(Combat c, Character self, Character other) {
        for (var event : c.getEvents(self)) {
            if (event.getEvent() == this.event && event.getPart() == part) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public int getProbability() {
        return probability;
    }

    @Override
    public boolean equals(Object o) {

        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        // Check if o is an instance of AnatomyComment or not
        if (!(o instanceof AnatomyComment)) {
            return false;
        }

        // typecast o to AnatomyComment so that we can compare data members
        var c = (AnatomyComment) o;

        // Compare the data members and return accordingly
        return c.event == event && c.part == part;
    }
}
