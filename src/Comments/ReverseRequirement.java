package Comments;

import characters.Character;
import combat.Combat;

import java.util.List;

/**
 * Defines a requirement that reverses the characters checked
 */
public class ReverseRequirement implements CustomRequirement {
	private List<CustomRequirement> reqs;

	public ReverseRequirement(List<CustomRequirement> reqs) {
		this.reqs = reqs;
	}

	@Override
	public boolean meets(Combat c, Character self, Character other) {
		for (var r : reqs) {
			if (!r.meets(c, other, self)) {
				return false;
			}
		}
		return true;
	}
}
