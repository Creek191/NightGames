package trap;

import characters.Character;
import combat.Encounter;

/**
 * Defines a trap that removes clothes when triggered
 */
public class ClothesSnareTrap extends Trap {
    // TODO: Not implemented

    public ClothesSnareTrap() {
        super("Clothes Snare", "", "", 3);
    }

    @Override
    public void trigger(Character target) {
    }

    @Override
    public boolean requirements(Character owner) {
        return false;
    }

    @Override
    public Trap copy() {
        return new ClothesSnareTrap();
    }

    @Override
    public void capitalize(Character attacker, Character victim, Encounter enc) {
    }
}
