package trap;

import characters.Attribute;
import characters.Character;
import global.Global;
import items.Component;

/**
 * Defines an alarm that alerts nearby characters when triggered
 */
public class Alarm extends Trap {
	public Alarm() {
		super("Alarm",
				"Will alert all participants in nearby areas when someone enters the room, even if they are being stealthy",
				"You rig up a disposable phone to a tripwire. When someone trips the wire, it should set of the phone's alarm.",
				2);
		recipe.put(Component.Tripwire, 1);
		recipe.put(Component.Phone, 1);
	}

	@Override
	public void trigger(Character target) {
		if (target.human()) {
			Global.gui().message(
					"You're walking through the eerily quiet campus, when a loud beeping almost makes you jump out of your skin. You realize the beeping is " +
							"coming from a cell phone on the floor. You shut it off as quickly as you can, but it's likely everyone nearby heard it already.");
		}
		else if (target.location().humanPresent()) {
			Global.gui().message(target.name() + " sets off your alarm, giving away " + target.possessive(false) + " presence.");
		}
		target.location().alarm = true;
		target.location().remove(this);
	}

	@Override
	public boolean requirements(Character owner) {
		return owner.getPure(Attribute.Cunning) >= 6;
	}

	@Override
	public Trap copy() {
		return new Alarm();
	}
}
