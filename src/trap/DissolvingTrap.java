package trap;

import characters.Attribute;
import characters.Character;
import characters.Trait;
import global.Global;
import items.Component;
import items.Flask;

/**
 * Defines a trap that dissolves any clothes worn by the character triggering it
 */
public class DissolvingTrap extends Trap {
	public DissolvingTrap() {
		super("Dissolving Trap",
				"Will completely strip the victim",
				"You rig up a trap to dissolve the clothes of whoever triggers it.",
				3);
		recipe.put(Component.Tripwire, 1);
		recipe.put(Flask.DisSol, 1);
		recipe.put(Component.Sprayer, 1);
	}

	@Override
	public void trigger(Character target) {
		if (!target.check(Attribute.Perception,
				20 - (target.getEffective(Attribute.Perception) + target.bonusDisarm()))) {
			if (target.human()) {
				Global.gui().message(
						"You spot a liquid spray trap in time to avoid setting it off. You carefully manage to disarm the trap and pocket the potion.");
				target.gain(Flask.DisSol);
				target.location().remove(this);
			}
		}
		else {
			if (target.human()) {
				if (target.nude()) {
					Global.gui().message(
							"Your bare foot hits a tripwire and you brace yourself as liquid rains down on you. You hastily do your best to brush the liquid off, " +
									"but after about a minute you realize nothing has happened. Maybe the trap was a dud.");
				}
				else {
					Global.gui().message(
							"You are sprayed with a clear liquid. Everywhere it lands on clothing, it immediately dissolves it, but it does nothing to your skin. " +
									"You try valiantly to save enough clothes to preserve your modesty, but you quickly end up naked.");
				}
			}
			else if (target.location().humanPresent()) {
				if (target.nude()) {
					Global.gui().message(target.name() + " is caught in your clothes dissolving trap, but "
							+ target.pronounSubject(false) + " was already naked. Oh well.");
				}
				else {
					Global.gui().message(target.name() + " is caught in your trap and is showered in dissolving solution. In seconds, "
							+ target.possessive(false) + " clothes vanish off her body, leaving her " +
							"completely nude.");
				}
			}
			target.nudify();
			target.location().opportunity(target, this);
		}
	}

	@Override
	public boolean requirements(Character owner) {
		return owner.getPure(Attribute.Cunning) >= 17 && !owner.has(Trait.direct);
	}

	@Override
	public Trap copy() {
		return new DissolvingTrap();
	}
}
