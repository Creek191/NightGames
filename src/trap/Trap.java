package trap;

import areas.Area;
import areas.Deployable;
import characters.Character;
import characters.Pool;
import combat.Combat;
import combat.Encounter;
import global.Global;
import global.Scheduler;
import items.Item;
import status.Flatfooted;

import java.util.HashMap;

/**
 * Defines a trap that can be deployed during a match
 */
public abstract class Trap implements Deployable {
	protected Character owner;
	protected Area location;
	protected boolean decoy;
	protected HashMap<Item, Integer> recipe;
	protected HashMap<Pool, Integer> cost;
	private final String label;
	private final String description;
	private final String setup;
	private final int priority;

	public Trap(String label, String description, String setup, int priority) {
		this.label = label;
		this.description = description;
		this.setup = setup;
		this.decoy = false;
		this.priority = priority;
		recipe = new HashMap<>();
		cost = new HashMap<>();
	}

	/**
	 * Checks whether the trap is a decoy
	 */
	public boolean decoy() {
		return decoy;
	}

	/**
	 * Checks whether the specified character has the items required to create the trap
	 *
	 * @param owner The character to check
	 */
	public boolean recipe(Character owner) {
		var affordable = true;
		for (var item : recipe.keySet()) {
			if (!owner.has(item, recipe.get(item))) {
				affordable = false;
			}
		}
		for (var pool : cost.keySet()) {
			if (!owner.canSpend(pool, cost.get(pool))) {
				affordable = false;
			}
		}
		return affordable;
	}

	/**
	 * Places the trap in an area during the match
	 *
	 * @param owner The character placing the trap
	 * @param area  The area where the trap is placed
	 * @return The placed trap object
	 */
	public Trap place(Character owner, Area area) {
		var copy = copy();
		copy.owner = owner;
		copy.location = area;
		for (var i : recipe.keySet()) {
			owner.consume(i, recipe.get(i));
		}
		for (var p : cost.keySet()) {
			owner.spend(p, cost.get(p));
		}
		area.place(copy);
		return copy;
	}

	/**
	 * Triggers the trap
	 *
	 * @param target The character who triggered the trip
	 */
	public abstract void trigger(Character target);

	/**
	 * Checks whether the specified character meets the attribute requirements to use the trap
	 *
	 * @param owner The character to check
	 */
	public abstract boolean requirements(Character owner);

	/**
	 * Creates a copy of the trap object
	 */
	protected abstract Trap copy();

	/**
	 * Gets the message used when setting the trap
	 */
	public String setup() {
		return setup;
	}

	/**
	 * Gets a description of the trap
	 */
	public String description() {
		return description;
	}

	/**
	 * Gets the owner of the trap
	 */
	public Character owner() {
		return owner;
	}

	@Override
	public void resolve(Character active) {
		if (active != owner) {
			trigger(active);
		}
	}

	@Override
	public int priority() {
		return priority;
	}

	/**
	 * Gets the name of the trap
	 */
	public String toString() {
		return label;
	}

	/**
	 * Plays a scene where the attacker capitalizes on the victim being caught in a trap
	 *
	 * @param attacker The character attacking the victim
	 * @param victim   The character caught in the trap
	 * @param enc      A reference to the encounter
	 */
	public void capitalize(Character attacker, Character victim, Encounter enc) {
		Combat c;
		if (attacker.human() || victim.human()) {
			c = Global.gui().beginCombat(attacker, victim);
		}
		else {
			c = Scheduler.getMatch().buildCombat(attacker, victim);
		}
		victim.add(new Flatfooted(victim, 1));
		enc.engage(c);
		attacker.location().remove(this);
	}
}
