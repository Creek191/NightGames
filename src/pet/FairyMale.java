package pet;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;
import global.Global;
import status.Flatfooted;
import status.Shield;

/**
 * Defines the male fairy pet
 */
public class FairyMale extends Pet {

	public FairyMale(Character owner, int power, int ac) {
		super("faerie", owner, Ptype.fairymale, power, ac);
	}

	@Override
	public void act(Combat c, Character target) {
		// Not (yet?) implemented
		if (target.human()) {
			return;
		}

		switch (Global.random(4)) {
			case 3:
				pleasure(c, target);
				break;
			case 2:
				shield(c);
				break;
			case 1:
				distract(c, target);
				break;
			default:
				heal(c);
		}
	}

	@Override
	public void vanquish(Combat c, Pet opponent) {
		switch (opponent.type()) {
			case fairyfem:
				c.write(owner(), "Your faerie boy chases " + opponent.ownerText() + "faerie and catches her from "
						+ "behind. He plays with the faerie girl's pussy and nipples while she's unable to retaliate. As "
						+ "she orgasms, she vanishes with a sparkle.");
				break;
			case fairymale:
				c.write("");
				break;
			case impfem:
				c.write(owner(), "Your faerie gets under " + opponent.ownerText() + "imp's guard and punches her "
						+ "squarely in her comparatively large clitoris. The imp shrieks in pain and collapses before "
						+ "vanishing.");
				break;
			case impmale:
				c.write("");
				break;
			case slime:
				c.write(owner(), "Your fae glows as he surrounds himself with magic before charging at "
						+ opponent.ownerText() + "slime like a tiny missile. The slime splashes more than it explodes, "
						+ "it's pieces only shudder once before going still.");
				break;
			case fgoblin:
				c.write(owner(), "Your faerie flies down between " + opponent.ownerText() + " fetish goblin's legs, "
						+ "targeting the vibrator sticking out of her wet pussy. He gives it a shot of magic, which causes "
						+ "it to go into overdrive. The goblin's knees tremble as she's overwhelmed by the intense "
						+ "sensations from the enchanted toy. A stifled whimper escapes her as she orgasms and vanishes.");
				break;
		}
		opponent.remove();
	}

	@Override
	public void caught(Combat c, Character captor) {
		if (!captor.human()) {
			c.write(captor, captor.name() + " snatches your faerie out of the air and flicks his little testicles with"
					+ " her finger. You wince in sympathy as the tiny male curls up in the fetal position and vanishes.");
		}
		remove();
	}

	@Override
	public Trait gender() {
		return Trait.male;
	}

	private void distract(Combat c, Character target) {
		if (Global.random(3) == 0) {
			c.write(owner(), "Your faerie flies behind " + target.name() + " and creates a small flash of "
					+ "light, distracting " + target.pronounTarget(false) + " .");
			target.add(new Flatfooted(target, 1), c);
		}
		else {
			c.write(owner(), "Your faerie flies at " + target.name() + ", but " + target.pronounSubject(false)
					+ " swats him away.");
		}
	}

	private void heal(Combat c) {
		c.write(owner(), "Your faerie lands on your shoulder and casts a spell, restoring your vitality.");
		owner().heal(power + Global.random(10), c);
	}

	private void pleasure(Combat c, Character target) {
		if (target.pantsless()) {
			c.write(owner(), "Your faerie flies between " + target.name() + "'s legs and rubs "
					+ target.possessive(false) + " sensitive " + (target.hasPussy() ? "clit" : "glans")
					+ " with both his tiny hands.");
		}
		else {
			c.write(owner(), "Your faerie crawls into " + target.name() + "'s "
					+ target.bottom.peek().getName() + " and fondles " + target.pronounTarget(false)
					+ " until " + target.pronounSubject(false) + " fishes him out.");
		}
		target.pleasure(2 + 3 * Global.random(power), Anatomy.genitals, c);
	}

	private void shield(Combat c) {
		c.write(owner(), "Your faerie flies in front of you and creates a magic barrier, reducing the "
				+ "physical damage you take.");
		owner().add(new Shield(owner(), 2 * power), c);
	}
}
