package pet;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;
import global.Global;
import status.Horny;

/**
 * Defines the female imp pet
 */
public class ImpFem extends Pet {

	public ImpFem(Character owner, int power, int ac) {
		super("Imp", owner, Ptype.impfem, power, ac);
	}

	@Override
	public void act(Combat c, Character target) {
		switch (Global.random(4)) {
			case 3:
				oral(target, c);
				break;
			case 2:
				punch(target, c);
				break;
			case 1:
				pheromones(target, c);
				break;
			default:
				strip(target, c);
				break;
		}
	}

	@Override
	public void vanquish(Combat c, Pet opponent) {
		switch (opponent.type()) {
			case fairyfem:
				c.write(owner(), ownerText() + "imp grabs " + opponent.ownerText() + "faerie and inserts the tiny girl "
						+ "into her soaking cunt. She pulls the faerie out after only a few seconds, but the sprite is "
						+ "completely covered with the imp's aphrodisiac wetness. The demon simply watches as the horny fae "
						+ "girl frantically masturbates in a sex drunk daze. It doesn't take long until the faerie "
						+ "disappears with an orgasmic moan.");
				break;
			case fairymale:
				c.write(owner(), ownerText() + "imp catches " + opponent.ownerText() + "faerie boy and holds him in "
						+ "her palm. The imp uses one finger to toy with the fae's tiny penis and the little male squirms "
						+ "helplessly. She dexterously rubs the faerie until it cums on her finger and vanishes with a flash.");
				break;
			case impfem:
				c.write(owner(), "The two female imps grapple with each other and " + opponent.ownerText() + "imp "
						+ "throws " + ownerText() + "imp to the floor. " + opponent.ownerText() + "imp approaches to press " +
						"her advantage, but " + ownerText() + "imp's tail suddenly thrusts into her pussy. As the tails "
						+ "fucks her, " + ownerText() + "imp collects some of her own wetness and forces the aphrodisiac "
						+ "filled fluid into the other female's mouth. " + opponent.ownerText() + "imp's orgasmic moan is "
						+ "stifled and she vanishes in a puff of brimstone.");
				break;
			case impmale:
				c.write(owner(), ownerText() + "imp grapples with " + opponent.ownerText() + "imp and gets a hold of "
						+ "his erection. She uses her leverage to parade the male around the edge of the battle. She "
						+ "strokes the demonic dick to the edge of orgasm and then mercilessly slams her knee into his "
						+ "balls. The male howls in pain and disappears.");
				break;
			case slime:
				c.write(owner(), ownerText() + "imp shoves both her hands into " + opponent.ownerText() + "slime. "
						+ "The slime trembles at her touch, encouraging her to wiggle her fingers more inside its semi-solid "
						+ "body. The slime writhes more and more before it suddenly shudders, then slowly melts into a puddle.");
				break;
			case fgoblin:
				c.write(owner(), ownerText() + "imp wrestles " + opponent.ownerText() + " fetish goblin to the ground "
						+ "and sits on her face. The imp girl rubs her dripping slit against the goblin's mouth, giving her "
						+ "a direct dose of laced juice. She targets the goblin's exposed cock and balls, jerking the former "
						+ "and slapping the latter. The goblin soon shudders and vanishes, but you have no idea whether it "
						+ "was due to pleasure or pain.");
				break;
		}
		opponent.remove();
	}

	@Override
	public void caught(Combat c, Character captor) {
		if (owner().human()) {
			c.write(captor, captor.name() + " grabs your imp and forces her to bend over. "
					+ captor.pronounSubject(true) + " thrusts two fingers into the little demon's pussy and pumps "
					+ "until she's overflowing with wetness. " + captor.pronounSubject(true) + " removes "
					+ captor.possessive(false) + " fingers from the imp's lower lips and forces them into the "
					+ "creature's mouth. Your demon, affected by the aphrodisiacs in her own juices, spreads her legs to "
					+ captor.name() + " and makes a pleading sound. " + captor.name() + " rubs and pinches the imp's clit "
					+ "until she spasms and disappears.");
		}
		else if (captor.human()) {
			c.write(captor, "You manage to catch " + ownerText() + "imp by her tail and pull her off balance. The "
					+ "imp falls to the floor and you plant your foot on her wet box before she can recover. You rub her "
					+ "slick folds with the sole of your foot and the demon writhes in pleasure, letting out incoherent "
					+ "whimpers. You locate her engorged clit with your toes and rub it quickly to finish her off. The "
					+ "imp climaxes and vanishes, leaving no trace except the wetness on your foot.");
		}
		remove();
	}

	@Override
	public Trait gender() {
		return Trait.female;
	}

	private void oral(Character target, Combat c) {
		String message;
		if (target.pantsless() && !c.stance.penetration(target)) {
			if (target.hasDick()) {
				message = ownerText() + "imp grabs " + (target.human() ? "your" : target.possessive(false))
						+ " dick and begins sucking it hungrily until "
						+ (target.human() ? "you push " : (target.pronounSubject(false) + " pushes "))
						+ "her away.";
			}
			else {
				message = ownerText() + "imp dives between "
						+ (target.human() ? "your" : (target.name() + "'s"))
						+ " legs and begins hungrily licking "
						+ (target.human() ? "your" : target.possessive(false))
						+ " pussy until "
						+ (target.human() ? "you push " : (target.pronounSubject(false) + " pushes "))
						+ "her away.";
			}
			c.write(owner(), message);
			target.pleasure(3 + 2 * Global.random(power), Anatomy.genitals, c);
		}
		else {
			c.write(owner(), ownerText() + "imps stands at the periphery of the fight, touching herself idly.");
		}
	}

	private void pheromones(Character target, Combat c) {
		String message;
		if (target.human()) {
			if (c.stance.prone(target)) {
				message = ownerText() + "imp straddles your face, forcing her wet pussy onto your nose and mouth. "
						+ "Her scent is unnaturally intoxicating and fires up your libido.";
			}
			else {
				message = ownerText() + "imp gets a running start and jumps higher than you thought possible, "
						+ "wrapping her legs around your head and pushing her soaked cunt into your face. Her "
						+ "musky scent affects you more than it should, there must be a high concentration of "
						+ "pheromones in her juices.";
			}
			c.write(owner(), message);
			target.add(new Horny(target, 3, 3), c);
		}
		else {
			c.write(owner(), "Your imp masturbates until her hand is coated in her own love juices, then "
					+ "flicks the fluid at " + target.name() + "'s face. You see her shiver slightly as the "
					+ "pheromone-filled juices take effect.");
			target.tempt(2 + 2 * Global.random(power), c);
		}
	}

	private void punch(Character target, Combat c) {
		if (target.hasBalls()) {
			c.write(owner(), "While " + (target.human() ? "your" : (target.name() + "'s")) + " attention is "
					+ "focused on " + (owner().human() ? "you" : owner().name()) + ", the imp creeps close to "
					+ (target.human() ? "you" : target.pronounTarget(false)) + " and uppercuts "
					+ (target.human() ? "you" : target.pronounTarget(false)) + " in the balls.");
			target.pain(4 + 3 * Global.random(power), Anatomy.genitals, c);
		}
		else {
			c.write(owner(), ownerText() + "imp runs up to " + (target.human() ? "you" : target.name())
					+ " and punches " + (target.human() ? "you" : target.pronounTarget(false) + " in the gut"));
			target.pain(2 + Global.random(power), Anatomy.genitals, c);
		}
	}

	private void strip(Character target, Combat c) {
		if (!target.topless() || !target.pantsless()) {
			var clothing = target.topless() ? target.bottom.peek() : target.top.peek();
			var stripDC = clothing.dc() + (target.getStamina().percent() - target.getArousal().percent()) / 4;
			if (Global.random(25) > stripDC || !target.canAct()) {
				c.write(owner(), ownerText() + "imp grabs "
						+ (target.human() ? "your" : target.possessive(false)) + " "
						+ clothing.getName() + " and yanks it off.");
				target.strip(target.topless() ? Character.OUTFITBOTTOM : Character.OUTFITTOP, c);
			}
			else {
				c.write(owner(), ownerText() + "imp pulls on "
						+ (target.human() ? "your" : target.possessive(false)) + " "
						+ clothing.getName() + ", accomplishing nothing except being slightly annoying.");
			}
		}
		else {
			c.write(owner(), ownerText() + "imps stands at the periphery of the fight, touching herself idly.");
		}
	}
}
