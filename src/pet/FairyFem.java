package pet;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;
import global.Global;

/**
 * Defines the female fairy pet
 */
public class FairyFem extends Pet {

	public FairyFem(Character owner, int power, int ac) {
		super("faerie", owner, Ptype.fairyfem, power, ac);
	}

	@Override
	public void act(Combat c, Character target) {
		switch (Global.random(4)) {
			case 3:
				attackPain(target, c);
			case 2:
				attackPleasure(target, c);
				break;
			case 1:
				buffMojo(c);
				break;
			default:
				healOwner(c);
				break;
		}
	}

	@Override
	public void vanquish(Combat c, Pet opponent) {
		switch (opponent.type()) {
			case fairyfem:
				c.write(owner(), "The two faeries circle around each other vying for the upper hand. " + ownerText()
						+ "faerie catches " + opponent.ownerText() + "faerie by the hips and starts to eat her out. "
						+ opponent.ownerText() + "fae struggles to break free, but can barely keep flying as she rapidly "
						+ "reaches orgasm and vanishes.");
				break;
			case fairymale:
				c.write(owner(), "The faeries zip through the air like a couple dogfighting planes. "
						+ opponent.ownerText() + "male manages to catch the female's hands, but you see her foot shoot up "
						+ "decisively between his legs. The stricken male tumbles lazily toward the floor and vanishes in "
						+ "midair.");
				break;
			case impfem:
				c.write(owner(), ownerText() + "faerie flies between the legs of " + opponent.ownerText() + "imp, "
						+ "slipping both arms into the larger pussy. The imp trembles and falls to the floor as the faerie "
						+ "puts her entire upper body into pleasuring her. " + ownerText() + "faerie is forcefully expelled "
						+ "by the imp's orgasm just before the imp vanishes.");
				break;
			case impmale:
				c.write(owner(), opponent.ownerText() + "imp grabs at " + ownerText() + "faerie, but the nimble "
						+ "sprite changes direction in midair and darts between the imp's legs. You can't see exactly "
						+ "what happens next, but the imp clutches his groin in pain and disappears.");
				break;
			case slime:
				c.write(owner(), ownerText() + "fae glows with magic as it circles " + opponent.ownerText() + "slime "
						+ "rapidly. The slime begins to tremble and slowly elongates into the shape of a crude phallus. "
						+ "It shudders violently and sprays liquid from the tip until the entire creature is a puddle on "
						+ "the floor.");
				break;
			case fgoblin:
				c.write(owner(), ownerText() + "faerie flies down to the base of " + opponent.ownerText()
						+ "futanari goblin's cock. There's a brief magic glow as she breaks the band constricting the "
						+ "goblin's member. With the obstacle removed, it only takes a single playful stroke to set off "
						+ "the pent-up herm.");
				break;
		}
		opponent.remove();
	}

	@Override
	public void caught(Combat c, Character captor) {
		if (captor.human()) {
			c.write(captor, "You snag " + ownerText() + "faerie out of the air. She squirms in your hand, but has no "
					+ "chance of breaking free. You lick the fae from pussy to breasts and the little thing squeals in "
					+ "pleasure. The taste is surprisingly sweet and makes your tongue tingle. You continue lapping up the "
					+ "flavor until she climaxes and disappears.");
		}
		else {
			c.write(captor, captor.name() + " manages to catch your faerie and starts pleasuring her with the tip of "
					+ "her finger. The sensitive fae clings to the probing finger desperately as she thrashes in ecstasy. "
					+ "Before you can do anything to help, your faerie vanishes in a burst of orgasmic magic.");
		}
		remove();
	}

	@Override
	public Trait gender() {
		return Trait.female;
	}

	private void attackPain(Character target, Combat c) {
		if (target.hasBreasts() && target.topless()) {
			c.write(owner(), ownerText() + "faerie dashes in from above and latches onto "
					+ (target.human() ? "your" : target.name()) + " left breast, giving "
					+ (target.human() ? "your" : target.possessive(false)) + " nipple a hearty bite.");
			target.pain(2 + Global.random(power), Anatomy.chest, c);
		}
		else if (target.hasBalls() && target.pantsless()) {
			c.write(owner(), ownerText() + "faerie flies in from below and kicks "
					+ (target.human() ? "you" : target.name()) + " in the balls.");
			target.pain(3 + 2 * Global.random(power), Anatomy.genitals, c);
		}
		else {
			c.write(owner(), ownerText() + "faerie flies around the edge of the fight looking for an opening.");
		}
	}

	private void attackPleasure(Character target, Combat c) {
		if (target.topless() && target.hasBreasts()) {
			c.write(owner(), ownerText() + "faerie lands on " + (target.human() ? "your" : (target.name() + "'s"))
					+ " tits and plays with " + (target.human() ? "your" : target.possessive(false))
					+ " sensitive nipples.");
			target.pleasure(3 + 2 * Global.random(power), Anatomy.chest, c);
		}
		else if (!c.stance.penetration(target)) {
			String message;
			if (target.pantsless()) {
				message = ownerText() + "faerie jumps on " + (target.human() ? "your" : (target.name() + "'s"))
						+ "exposed " + (target.hasDick() ? "dick" : "pussy") + " and rubs her tiny body against it.";
			}
			else {
				message = ownerText() + "faerie slips into " + (target.human() ? "your" : (target.name() + "'s"))
						+ target.bottom.peek().getName() + " and teases "
						+ (target.human() ? "your" : target.possessive(false)) + " genitals until she is " +
						"forcibly removed.";
			}
			c.write(owner(), message);
			target.pleasure(2 + 3 * Global.random(power), Anatomy.genitals, c);
		}
		else {
			c.write(owner(), ownerText() + "faerie flies around the edge of the fight looking for an opening");
		}

		if (c.stance.penetration(target)) {
			c.write(owner(),
					ownerText() + "faerie flies around the edge of the fight looking for an opening");
		}
		else if (target.pantsless()) {
			c.write(owner(),
					ownerText() + "faerie hugs your dick and rubs it with her entire body until you pull her off");
			target.pleasure(2 + 3 * Global.random(power), Anatomy.genitals, c);
		}
		else {
			c.write(owner(),
					ownerText() + "faerie slips into your " + target.bottom.peek().getName() + " and plays with your penis until you manage to remove her.");
			target.pleasure(2 + 3 * Global.random(power), Anatomy.genitals, c);
		}

		if (target.topless()) {
			c.write(owner(),
					"Your faerie lands on " + target.name() + "'s tit and plays with her sensitive nipple");
			target.pleasure(3 + 2 * Global.random(power), Anatomy.chest, c);
		}
		else if (target.hasDick() && !c.stance.penetration(target)) {
			if (target.pantsless()) {
				c.write(owner(),
						ownerText() + "faerie jumps on " + target.name() + "'s exposed dick and rubs her tiny body against it.");
			}
			else {
				c.write(owner(),
						ownerText() + "faerie darts into " + target.name() + "'s " + target.bottom.peek().getName() + " and teases her genitals until she is forcibly removed.");
			}
			target.pleasure(2 + 3 * Global.random(power), Anatomy.genitals, c);
		}
		else {
			c.write(owner(),
					ownerText() + "faerie flies around the edge of the fight looking for an opening");
		}
	}

	private void buffMojo(Combat c) {
		String message;
		if (owner().human()) {
			message = "Your faerie circles around your with a faint glow and kisses you on the cheek. You feel energy "
					+ "building inside you.";
		}
		else {
			message = ownerText() + "faerie flies around " + owner().pronounTarget(false) + ", channelling energy "
					+ "into " + owner().pronounTarget(false) + ".";
		}
		c.write(owner(), message);
		owner().buildMojo(20);
	}

	private void healOwner(Combat c) {
		String message;
		if (owner().human()) {
			message = "Your faerie flies next to your ear in speaks to you. The words aren't in English and you have no "
					+ "idea what she said, but somehow you feel your fatigue drain away.";
		}
		else {
			message = ownerText() + "faerie rains magic energy on " + owner().pronounTarget(false) + ", restoring "
					+ owner().pronounTarget(false) + " strength.";
		}
		c.write(owner(), message);
		owner().heal(power + Global.random(10), c);
	}
}
