package pet;

/**
 * Defines the types of pets
 */
public enum Ptype {
	fairyfem,
	fairymale,
	impfem,
	impmale,
	slime,
	fgoblin,
}
