package pet;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;
import global.Global;
import status.BD;
import status.Masochistic;
import status.Shamed;

import java.util.ArrayList;

/**
 * Defines the fetish goblin pet
 */
public class FGoblin extends Pet {

	public FGoblin(Character owner, int pow, int ac) {
		super("Fetish Goblin", owner, Ptype.fgoblin, pow, ac);
	}

	@Override
	public void act(Combat c, Character target) {
		String message;
		switch (pickSkill(c, target)) {
			case VIBRATOR:
				c.write(owner(), ownerText() + "fetish goblin removes the humming vibrator from her own wet pussy "
						+ "and shoves it into " + (target.human() ? "yours" : target.possessive(false)) + ".");
				c.write(owner(), ownerText() + "fetish goblin pulls the vibrator out of her wet hole and thrusts it "
						+ "between " + owner().possessive(false) + "legs.");
				target.pleasure(2 + 3 * Global.random(power), Anatomy.genitals, c);
				break;
			case MASOCHISM:
				c.write(owner(), ownerText() + "fetish goblin draws a riding crop and hits her own balls with it. "
						+ "She shivers with delight at the pain and you can feel an aura of masochism radiate off her.");
				owner().add(new Masochistic(owner()), c);
				target.add(new Masochistic(target), c);
				break;
			case BONDAGE:
				c.write(owner(), ownerText() + "fetish goblin pulls the bondage straps tighter around herself. "
						+ "You can see the leather and latex digging into her skin as her bondage fascination begins to "
						+ "affect both you and %s.");
				owner().add(new BD(owner()), c);
				target.add(new BD(target), c);
				break;
			case DENIAL:
				if (owner().human()) {
					message = ownerText() + "fetish goblin suddenly turns and slaps you sharply on the balls. You wince "
							+ "in pain and let out a yelp of protest. You can't see the goblin's expression through her "
							+ "mask, but her eyes seem to be scolding you for your lack of self control.";
				}
				else {
					message = ownerText() + "fetish goblin suddenly appears to turn against "
							+ owner().pronounTarget(false) + " and slaps " + owner().possessive(false) + " sensitive"
							+ "testicles. You're momentarily confused, buy you realize the shock probably undid your "
							+ "efforts to make " + owner().pronounTarget(false) + " cum.";
				}
				c.write(owner(), message);
				owner().pain(3 * Global.random(power), Anatomy.genitals, c);
				owner().calm((3 + Global.random(5)) * power, c);
				break;
			case FACEFUCK:
				if (target.human()) {
					message = ownerText() + "fetish goblin straddles your head, giving you an eyeful of her assorted genitals. "
							+ "She pulls the vibrator out of her pussy, causing a rain of love juice to splash your face. "
							+ "She then wipes her leaking cock on your forehead, smearing you with precum. "
							+ "You feel your face flush with shame as she marks you with her fluids.";
				}
				else {
					message = ownerText() + "fetish goblin leaps onto " + target.name() + "'s face as "
							+ target.pronounSubject(false) + " is lying on the floor. The goblin rups her cock "
							+ "and balls on " + target.possessive(false) + " face, humiliating "
							+ target.pronounTarget(false);
				}
				c.write(owner(), message);
				target.add(new Shamed(target), c);
				break;
			case ANALDILDO:
				if (target.human()) {
					message = "You jump in surprise as you suddenly feel something solid penetrating your asshole. "
							+ ownerText() + "fetish goblin got behind you during the fight and delivered a sneak attack "
							+ "with an anal dildo. Before you can retaliate she withdraws the toy and retreats to safety.";
				}
				else {
					message = "Your fetish goblin manages to sneak up on " + target.name() + " and stick a dildo "
							+ "in " + target.possessive(false) + " ass. " + target.pronounSubject(true)
							+ " lets out a shriek of surprise at the sudden sensation and your goblin gets away before "
							+ target.name() + " can catch her.";
				}
				c.write(owner(), message);
				target.pleasure(2 + 3 * Global.random(power), Anatomy.ass, c);
				break;
			case ANALBEADS:
				if (target.human()) {
					message = ownerText() + "fetish goblin takes advantage of your helplessness and positions herself behind "
							+ "you. She produces a string of anal beads and proceeds to insert them one bead at a time into "
							+ "your anus. She manages to get five beads in while you're unable to defend yourself. When she "
							+ "pulls them out, it feels like they're turning you inside out.";
				}
				else {
					message = "Your fetish goblin takes advantage of " + target.name() + "'s defenselessness to push a string "
							+ "of anal beads into " + target.possessive(false) + " butt. "
							+ target.pronounSubject(true) + " lets out a whimper of protest as each bead goes in and a "
							+ "moan of pleasure as they're all pulled out.";
				}
				c.write(owner(), message);
				target.pleasure(5 * Global.random(power), Anatomy.ass, c);
				break;
			default:
				c.write(owner(),
						ownerText() + "fetish goblin stays at the edge of battle and touches herself absentmindedly.");
				break;
		}
	}

	@Override
	public void vanquish(Combat c, Pet opponent) {
		switch (opponent.type()) {
			case fairyfem:
				c.write(owner(), opponent.ownerText() + " faerie girl flies low, aiming for " + ownerText() + " fetish "
						+ "goblin's weak spot, but the goblin knocks the little fae out of the air with a swing "
						+ "of her hefty girl-cock. The fetish goblin grabs the dazed faerie and finishes her off by pressing "
						+ "a vibrator against her tiny slit.");
				break;
			case fairymale:
				c.write(owner(), ownerText() + " fetish goblin manages to catch " + opponent.ownerText() + " faerie "
						+ "as he carelessly flies too close. She shoves the tiny male between her heavy boobs, completely "
						+ "engulfing him within her cleavage. You don't know if or when he desummoned, but he's clearly "
						+ "not coming out of there.");
				break;
			case impfem:
				c.write(owner(), ownerText() + " fetish goblin overpowers " + opponent.ownerText() + " imp girl and "
						+ "quickly binds her wrists. The goblin bends the helpless imp over her knee and slides a "
						+ "suspiciously wet vibrator into her exposed pussy. The imp moans and squirms, but is unable to "
						+ "get away. The goblin punishes the escape attempt with ten hard slaps on the imp's upturned ass. "
						+ "By the final slap, the imp is practically screaming in orgasm and quickly vanishes in a puff of "
						+ "brimstone.");
				break;
			case impmale:
				c.write(owner(), ownerText() + " fetish goblin and " + opponent.ownerText() + " imp grapple with each "
						+ "other, vying for dominance. The larger goblin gains the upper hand and staggers the imp with a "
						+ "quick knee to the groin. The imp doubles over in pain, giving the hermaphroditic goblin time to "
						+ "get behind him. She lines up her cock with the imp's unprotected ass and penetrates him with a "
						+ "firm thrust. " + opponent.ownerText() + " imp squeals in alarm at being suddenly fucked from "
						+ "behind. The goblin pegs the imp steadily and reaches around to stroke his cock. Soon, the imp "
						+ "sprays cum into the air and disappears.");
				break;
			case slime:
				c.write(owner(), ownerText() + " fetish goblin removes her boots and steps barefoot into "
						+ opponent.ownerText() + " slime. The amorphous creature squirms happily around the goblin's feet, "
						+ "but makes no attempt to attack. It seems to be completely fascinated with her feet. The goblin "
						+ "wiggles her toes and the slime trembles with delight, before melting into a content puddle.");
				break;
			case fgoblin:
				c.write(owner(), ownerText() + " fetish goblin tackles " + opponent.ownerText() + " goblin, pinning "
						+ "her arms behind her back. " + ownerText() + " goblin ties up the other, using some spare ropes "
						+ "and bandage straps. The poor, bound herm is left completely immobile and vulnerable, but seems "
						+ "to be getting very aroused by her situation. The dominant fetish goblin takes her time getting "
						+ "her helpless opponent off, but eventually " + opponent.ownerText() + " goblin is whimpering and "
						+ "twitching in orgasms.");
				break;
		}
		opponent.remove();
	}

	@Override
	public void caught(Combat c, Character captor) {
		if (owner().human()) {
			c.write(captor, captor.name() + " gets a short running start and delivers a powerful punt to your fetish "
					+ "goblin's dangling balls. The impact lifts the short hermaphrodite completely off the floor as you "
					+ "cringe in sympathetic pain. Apparently it's too much even for the masochistic creature, because she "
					+ "collapses and disappears.");
		}
		else {
			c.write(captor, "You manage to catch " + owner().name() + "'s fetish goblin by her bondage gear, keeping "
					+ "her from escaping. It's not immediately clear how you can finish off the overstimulated goblin. "
					+ "There's not much you can do to her genitals beyond what she's already doing with her 'accessories.' "
					+ "You need a strong enough stimulus to push her over the threshold. You grab the end of the anal beads "
					+ "sticking out of her ass and yank them out all at once. The goblin shudders and the flow of liquid "
					+ "leaking out of her holes signals her orgasm before she vanishes.");
		}
		remove();
	}

	@Override
	public Trait gender() {
		return Trait.herm;
	}

	/**
	 * Picks a skill to use on the specified target
	 *
	 * @param c      A reference to the ongoing combat
	 * @param target The target character
	 * @return The skill that was picked
	 */
	public PetSkill pickSkill(Combat c, Character target) {
		var available = new ArrayList<PetSkill>();
		available.add(PetSkill.IDLE);
		available.add(PetSkill.MASOCHISM);
		available.add(PetSkill.BONDAGE);
		if (owner().hasBalls() && owner().getArousal().percent() >= 80) {
			available.add(PetSkill.DENIAL);
		}
		if (target.hasPussy() && target.pantsless()) {
			available.add(PetSkill.VIBRATOR);
		}
		if (target.pantsless() && target.canAct()) {
			available.add(PetSkill.ANALDILDO);
		}
		if (target.pantsless() && !target.canAct()) {
			available.add(PetSkill.ANALBEADS);
		}
		if (c.stance.prone(target)) {
			available.add(PetSkill.FACEFUCK);
		}
		return available.get(Global.random(available.size()));
	}

	private enum PetSkill {
		VIBRATOR,
		MASOCHISM,
		BONDAGE,
		DENIAL,
		FACEFUCK,
		ANALDILDO,
		ANALBEADS,
		IDLE,
	}
}
