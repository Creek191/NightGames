package pet;

import characters.Character;
import characters.Trait;
import combat.Combat;

/**
 * Base implementation for pets tha can be summoned during combat
 */
public abstract class Pet {
	private final String name;
	private final Character owner;
	private final Ptype type;
	protected int power;
	protected int ac;

	public Pet(String name, Character owner, Ptype type, int power, int ac) {
		this.owner = owner;
		this.name = name;
		this.type = type;
		this.power = power;
		this.ac = ac;
	}

	/**
	 * Gets the name of the pet
	 */
	public String toString() {
		return name;
	}

	/**
	 * Gets the character that summoned the pet
	 */
	public Character owner() {
		return owner;
	}

	/**
	 * Gets a string with the owner's name for use in messages
	 */
	public String ownerText() {
		if (owner.human()) {
			return "Your ";
		}
		else {
			return owner.name() + "'s ";
		}
	}

	/**
	 * Plays an action where the pet performs an action in combat
	 *
	 * @param c      A reference to the ongoing combat
	 * @param target The character targeted by the pet
	 */
	public abstract void act(Combat c, Character target);

	/**
	 * Plays an action where the pet vanquishes an opponent's pet
	 *
	 * @param c        A reference to the ongoing combat
	 * @param opponent The pet that was defeated
	 */
	public abstract void vanquish(Combat c, Pet opponent);

	/**
	 * Plays an action where the pet is caught by the specified character, defeating it
	 *
	 * @param c      A reference to the ongoing combat
	 * @param captor The character that caught the pet
	 */
	public abstract void caught(Combat c, Character captor);

	/**
	 * Gets the pet's gender trait
	 */
	public abstract Trait gender();

	/**
	 * Removes the pet
	 */
	public void remove() {
		owner.pet = null;
	}

	/**
	 * Gets the pet's type
	 */
	public Ptype type() {
		return type;
	}

	/**
	 * Gets the pet's combat power
	 */
	public int power() {
		return power;
	}

	/**
	 * Gets the pet's AC
	 */
	public int ac() {
		return ac;
	}
}
