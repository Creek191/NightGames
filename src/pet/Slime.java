package pet;

import characters.Anatomy;
import characters.Character;
import characters.Trait;
import combat.Combat;
import global.Global;
import stance.StandingOver;
import status.Oiled;

/**
 * Defines the slime pet
 */
public class Slime extends Pet {

	public Slime(Character owner, int power, int ac) {
		super("Slime", owner, Ptype.slime, power, ac);
	}

	@Override
	public void act(Combat c, Character target) {
		switch (Global.random(4)) {
			case 3:
				penetrate(c, target);
				break;
			case 2:
				hug(c, target);
				break;
			case 1:
				stagger(c, target);
				break;
			default:
				c.write(owner(), ownerText() + "slime takes on a humanoid shape and watches you like a curious child.");
		}
	}

	@Override
	public void vanquish(Combat c, Pet opponent) {
		switch (opponent.type()) {
			case fairyfem:
				c.write(owner(), opponent.ownerText() + "faerie flies over " + ownerText() + "slime and begins casting "
						+ "a spell. Without warning, several appendages shoot out from the blob and snag the faerie girl's "
						+ "limbs before she can escape. More appendages attach to her breasts and groin as the slime starts "
						+ "to vibrate. The faerie lets out a high pitched moan and squirms against her bonds until she "
						+ "shudders in orgasm and vanishes.");
				break;
			case fairymale:
				c.write(owner(), opponent.ownerText() + "faerie flies too close to " + ownerText() + "slime and is "
						+ "suddenly engulfed up to his waist before he can react. He tries to free himself, but groans as "
						+ "it starts to suck and massage his penis. He tries to push the slime off his groin, but it just "
						+ "sucks in his hands, leaving him completely helpless until he ejaculates.");
				break;
			case impfem:
				c.write(owner(), ownerText() + "slime gathers around " + opponent.ownerText() + "imp's ankles. With "
						+ "unexpected speed, it surges up her legs and simultaneously penetrates her pussy and ass. She "
						+ "screams in pleasure and falls to her knees as the amorphous blob fucks both her holes. By the "
						+ "time she climaxes and disappears, she's completely fucked senseless.");
				break;
			case impmale:
				c.write(owner(), opponent.ownerText() + "imp grabs for " + ownerText() + "slime, but it leaps past "
						+ "his guard and covers his cock. The slime forms perfectly to the imp's dick and balls, milking "
						+ "as much pre-cum as it can get. The imp tries to pull off the slime, but it acts as lubricant "
						+ "and the imp's attempts to remove it devolve into masturbation. The imp demon ejaculates into "
						+ "the slime and disappears.");
				break;
			case slime:
				c.write(owner(), "The two slimes circle around each other, while gradually taking on human shape. "
						+ "One of the oozes looks vaguely like " + ownerText() + "small slimy twin, while the other takes "
						+ opponent.ownerText() + "form. The two grapple and melt into each other so it's impossible to "
						+ "tell where one ends and the other begins. You can make out vaguely sexual shapes being formed "
						+ "in the mix. Somehow you can tell that they're each trying to pleasure the other. Eventually the "
						+ "battle ends and a single humanoid shape forms from the amorphous mass, revealing that "
						+ ownerText() + "slime was victorious.");
				break;
			case fgoblin:
				c.write(owner(), ownerText() + " slime quickly pounces on " + opponent.ownerText() + " goblin, coating "
						+ "the outside of her latex outfit. The wriggling slime melts through the material, stripping the "
						+ "sexy goblin girl. The slime wraps around her dick and probes her pussy and ass, but it's barely "
						+ "necessary. The sensation of being released from her tight, restricting bondage gear is enough "
						+ "to make her cum. The defeated goblin writhes helplessly in pleasure, while the slime absorbs as "
						+ "much of her fluids as it can before she disappears.");
				break;
		}
		opponent.remove();
	}

	@Override
	public void caught(Combat c, Character captor) {
		if (owner().human()) {
			c.write(captor, captor.name() + " seizes your slime and holds it near her groin. The ooze reacts to the "
					+ "closeness of her vagina and immediately forms a phallic appendage. She grabs the slimy cock before "
					+ "it can penetrate her and strokes it quickly. With each stroke, the shape becomes more defined, until "
					+ "the slime has a perfectly human penis and a set of testicles. " + captor.name() + " speeds up her "
					+ "strokes and grabs the artificial balls with her free hand. The slime ejaculates its own fluid and "
					+ "melts into a puddle.");
		}
		else {
			c.write(captor, "You manage to catch " + ownerText() + "slime, but you're not sure what to do with it. "
					+ "It occurs to you that this thing is actively seeking sexual pleasure, so you push two fingers into "
					+ "the mass and pump them back and forth. Soon a convincing replica of a vagina forms around your "
					+ "fingers and the entire slime gradually takes the shape of a woman. As soon at the clit forms you "
					+ "focus attention on it. The slime climaxes just as its girlish shape finishes forming. Its realistic "
					+ "face shapes in a silent moan and looks very content until it melts into a puddle of goo.");
		}
		remove();
	}

	@Override
	public Trait gender() {
		return Trait.herm;
	}

	private void hug(Combat c, Character target) {
		String message;
		if (!target.topless() || !target.pantsless()) {
			String clothingName;
			if (!target.top.isEmpty()) {
				clothingName = target.top.peek().getName();
				target.shred(Character.OUTFITTOP);
			}
			else {
				clothingName = target.bottom.peek().getName();
				target.shred(Character.OUTFITBOTTOM);
			}
			if (target.human()) {
				message = ownerText() + "slime forms into a shape that's vaguely human and clearly female. Somehow "
						+ "it manages to look cute and innocent while still being an animated blob of slime. "
						+ "While you're processing this, the slime jumps on you and your " + clothingName
						+ " dissolves under its touch.";
			}
			else {
				message = "Your slime playfully pounces on " + target.name() + " and it's corrosive body melts "
						+ target.possessive(false) + " " + clothingName + " in a fortunate accident.";
			}
			c.write(owner(), message);
		}
		else {
			if (target.human()) {
				message = ownerText() + "slime forms into a shape that's vaguely human and clearly female. Somehow "
						+ "it manages to look cute and innocent while still being an animated blob of slime. "
						+ "The slime suddenly pounces on you and wraps itself around you. It doesn't seem to be "
						+ "attacking you as much as giving you a hug, but it leaves you covered in slimy residue.";
			}
			else {
				message = "Your slime hugs " + target.name() + " affectionately, covering "
						+ target.pronounTarget(false) + " in slimy liquid.";
			}
			c.write(owner(), message);
			target.add(new Oiled(target), c);
		}
	}

	private void penetrate(Combat c, Character target) {
		String message;
		if (target.pantsless() && !c.stance.penetration(target)) {
			if (target.hasDick()) {
				message = ownerText() + "slime forms into a humanoid shape and grabs "
						+ (target.human() ? "your" : (target.name() + "'s")) + " dick. The slime hand "
						+ "molds to " + (target.human() ? "your" : target.possessive(false)) + " penis and "
						+ "rubs " + (target.human() ? "you" : target.pronounTarget(false)) + " with a "
						+ "slippery pleasure.";
			}
			else {
				message = "Two long appendages extend from " + ownerText() + " slime and wrap around "
						+ (target.human() ? "your" : (target.name() + "'s")) + " legs. "
						+ "A third, phallic shaped appendage forms and penetrates "
						+ (target.human() ? "your" : target.pronounSubject(false)) + " "
						+ (target.hasPussy() ? "pussy" : "ass") + ". "
						+ (target.human() ? "You stifle" : (target.pronounSubject(true) + " stifles"))
						+ " a moan as the slimy tentacles thrust in and out of "
						+ (target.human() ? "you" : target.pronounTarget(false)) + ".";
			}
			c.write(owner(), message);
			target.pleasure(3 + 2 * (Global.random(power)), Anatomy.genitals, c);
		}
		else {
			c.write(owner(),
					"You see eyes form in " + ownerText() + "slime as it watches the fight curiously.");
		}
	}

	private void stagger(Combat c, Character target) {
		String message;
		if (!c.stance.prone(target)) {
			if (power * Global.random(20) >= target.knockdownDC()) {
				if (target.human()) {
					message = ownerText() + "slime wraps around your ankles and you are unable to keep your footing.";
				}
				else {
					message = target.name() + " slips on your slime as it clings to " + target.possessive(false)
							+ " feet. " + target.pronounSubject(true) + " falls on "
							+ target.possessive(false) + " butt "
							+ "and extracts " + target.possessive(false) + " feet form the ooze.";
				}
				c.write(owner(), message);
				c.stance = new StandingOver(owner(), target);
			}
			else {
				if (target.human()) {
					message = ownerText() + "slime glomps onto your ankles. You almost lose your balance, but "
							+ "manage to recover.";
				}
				else {
					message = target.name() + " stumbles as your slime clings to " + target.pronounTarget(false)
							+ "leg. " + target.pronounSubject(true) + " manages to catch "
							+ target.pronounTarget(false) + "self and scrapes off the clingy blob.";
				}
				c.write(owner(), message);
			}
		}
		else {
			c.write(owner(),
					"You see eyes form in " + ownerText() + "slime as it watches the fight curiously.");
		}
	}
}
