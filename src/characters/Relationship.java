package characters;

/**
 * Defines a relationship between two characters
 */
public class Relationship {
	private final ID person1;
	private final ID person2;
	private int value;

	public Relationship(ID p1, ID p2, int value) {
		person1 = p1;
		person2 = p2;
		set(value);
	}

	/**
	 * Gets the relationship value
	 */
	public int get() {
		return value;
	}

	/**
	 * Sets the relationship value
	 *
	 * @param value The new value of the relationship
	 */
	public void set(int value) {
		this.value = value;
	}

	/**
	 * Increases the relationship value
	 *
	 * @param value The value to add
	 */
	public void add(int value) {
		this.value += value;
	}

	/**
	 * Checks whether the specified character is part of the relationship
	 *
	 * @param person The ID of the character to check for
	 */
	public boolean contains(ID person) {
		return person1 == person || person2 == person;
	}

	/**
	 * Checks whether the relationship is between the specified characters
	 *
	 * @param person1 The ID of the first character to check for
	 * @param person2 The ID of the second character to check for
	 */
	public boolean is(ID person1, ID person2) {
		return contains(person1) && contains(person2);
	}

	/**
	 * Gets the partner of the specified character
	 *
	 * @param person The ID of the character to get the partner for
	 * @return The ID of the character's partner, or Null if the specified character is not part of the relationship
	 */
	public ID getPartner(ID person) {
		if (person1 == person) {
			return person2;
		}
		if (person2 == person) {
			return person1;
		}
		return null;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() == Relationship.class) {
			var other = (Relationship) obj;
			return other.contains(person1) && other.contains(person2);
		}
		return false;
	}
}
