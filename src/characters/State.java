package characters;

/**
 * Defines the states a character can be in
 */
public enum State {
	ready,
	shower,
	combat,
	searching,
	crafting,
	hidden,
	resupplying,
	lostclothes,
	webbed,
	masturbating,
	quit,
}
