package characters;

import Comments.CommentGroup;
import Comments.CommentSituation;
import Comments.SkillComment;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Result;
import combat.Tag;
import daytime.Daytime;
import global.Flag;
import global.Global;
import global.Match;
import global.Modifier;
import items.Clothing;
import items.Toy;
import items.Trophy;
import scenes.SceneFlag;
import scenes.SceneManager;
import skills.*;
import status.Drowsy;
import status.Energized;
import status.Stsflag;

import java.util.ArrayList;
import java.util.HashSet;

public class Maya implements Personality {
	private final NPC character;

	public Maya() {
		character = new NPC("Maya", ID.MAYA, 50, this);

		// Set clothing
		resetOutfit();
		character.closet.add(Clothing.blouse);
		character.closet.add(Clothing.skirt);
		character.closet.add(Clothing.camisole);
		character.closet.add(Clothing.lacepanties);
		character.change(Modifier.normal);
		character.setUnderwear(Trophy.MayaTrophy);

		// Set base stats
		character.set(Attribute.Unknowable, 40);
		character.set(Attribute.Seduction, 66);
		character.set(Attribute.Cunning, 39);
		character.set(Attribute.Speed, 17);
		character.set(Attribute.Power, 36);
		character.set(Attribute.Hypnosis, 8);
		character.getStamina().gainMax(160);
		character.getArousal().gainMax(300);
		character.getMojo().gainMax(300);

		// Set starting traits
		character.add(Trait.female);
		character.add(Trait.darkpromises);
		character.add(Trait.greatkiss);
		character.add(Trait.Confident);
		character.add(Trait.event);
		character.add(Trait.cursed);
		character.gain(Toy.Onahole2);
		character.gain(Toy.Dildo2);
		character.gain(Toy.Tickler2);
		character.gain(Toy.Crop2);

		// Set AI logic
		character.plan = Emotion.hunting;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.hunting, 4);
		character.strategy.put(Emotion.sneaking, 2);
		character.preferredSkills.add(Suggestion.class);
		character.preferredSkills.add(Footjob.class);
		character.preferredSkills.add(Whisper.class);
		character.preferredSkills.add(Stomp.class);
		character.preferredSkills.add(Spank.class);
		if (Global.checkFlag(Flag.PlayerButtslut)) {
			character.preferredSkills.add(AssFuck.class);
			character.preferredSkills.add(Strapon.class);
			character.preferredSkills.add(FingerAss.class);

			character.gain(Toy.Strapon2);
		}

		Global.gainSkills(this.character);
	}

	@Override
	public Skill act(HashSet<Skill> available, Combat c) {
		var mandatory = new HashSet<Skill>();
		for (var skill : available) {
			if (skill.toString().equalsIgnoreCase("Command")
					|| skill.toString().equalsIgnoreCase("Ass Fuck")) {
				mandatory.add(skill);
			}
			if (character.is(Stsflag.orderedstrip)
					&& (skill.toString().equalsIgnoreCase("Undress")
					|| skill.toString().equalsIgnoreCase("Strip Tease"))) {
				mandatory.add(skill);
			}
			if (Global.checkFlag(Flag.PlayerButtslut) && character.has(Trait.strapped)) {
				if ((skill.toString().equalsIgnoreCase("Mount") && character.has(Toy.Strapon2))
						|| skill.toString().equalsIgnoreCase("Turn Over")) {
					mandatory.add(skill);
				}
			}
		}

		if (!mandatory.isEmpty()) {
			var actions = mandatory.toArray(new Skill[0]);
			return actions[Global.random(actions.length)];
		}

		Skill chosen;
		var priority = character.parseSkills(available, c);
		if (Global.checkFlag(Flag.hardmode) && !c.hasModifier(Modifier.quiet)) {
			chosen = character.prioritizeSimulated(priority, c);
		}
		else {
			chosen = character.prioritizeRandom(priority);
		}

		if (chosen != null) {
			return chosen;
		}

		var actions = available.toArray(new Skill[0]);
		return actions[Global.random(actions.length)];
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		return character.parseMoves(available, radar, match);
	}

	@Override
	public NPC getCharacter() {
		return this.character;
	}

	@Override
	public void rest(int time, Daytime day) {
		var available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		available.add("Play Video Games");
		for (var i = 0; i < time - 3; i++) {
			var location = available.get(Global.random(available.size()));
			day.visit(location, character, Global.random(character.money));
		}
		character.visit(3);
	}

	@Override
	public String bbLiner() {
		return "Maya looks at you sympathetically. <i>\"Was that painful? Don't worry, you aren't seriously injured. Our Benefactor protects us.\"</i>";
	}

	@Override
	public String nakedLiner() {
		return "Maya smiles, unashamed of her nudity. <i>\"Well done. Not many participants are able to get my clothes off anymore. You'll at least be able to look at a "
				+ "naked woman while you orgasm.\"</i>";
	}

	@Override
	public String stunLiner() {
		return "You think you see something dangerous flicker in Maya's eyes. <i>\"Well done. I may need to get a little serious.\"</i>";
	}

	@Override
	public String taunt() {
		return "Maya gives you a look of gentle disapproval. <i>\"You aren't putting up much of a fight, are you? Aren't you a little overeager to cum?\"</i>";
	}

	@Override
	public void victory(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		opponent.add(new Drowsy(opponent, 5));
		character.getArousal().empty();
		character.add(new Energized(character, 10));
		if (!opponent.human()) {
			return;
		}

		if (flag == Result.anal && c.stance.penetration(character)) {
			if (Global.getValue(Flag.PlayerAssLosses) <= 0) {
				Global.setCounter(Flag.PlayerAssLosses, 1);
				SceneManager.play(SceneFlag.MayaPeggingVirginity);
			}
			else {
				Global.modCounter(Flag.PlayerAssLosses, 1);
				SceneManager.play(SceneFlag.MayaPeggingVictory);
			}
		}
		else {
			SceneManager.play(SceneFlag.MayaVictory);
		}
	}

	@Override
	public void defeat(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		declareGrudge(character, c);
		if (!opponent.human()) {
			return;
		}

		Global.flag(Flag.Mayabeaten);
		if (flag == Result.anal && c.stance.penetration(opponent)) {
			SceneManager.play(SceneFlag.MayaAnalDefeat);
		}
		else {
			SceneManager.play(SceneFlag.MayaDefeat);
		}
	}

	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		if (target.human()) {
			return "<i>\"Oh my. I don't think the boy had much of a chance to start with, but now the other girls are picking on him too.\"</i> She looks over your "
					+ "helpless body and sides of her mouth curl slightly with amusement. She delicately extends her stocking-covered foot and teases the tip of your penis.<br>"
					+ "<i>\"I think you'll enjoy this bullying, at least.\"</i> She rubs and squeezes your sensitive glans with her toes. Your hips buck involuntarily as she "
					+ "keeps moving her foot to stimulate different spots on your shaft. How can she give you this kind of pleasure with just one foot? You knew she was experienced, "
					+ "but this is insane! She steps down on your dick, grinding it with the sole of her foot. You can't hold on any more. Your cock throbs and you shoot your seed "
					+ "onto her foot.";
		}
		else {
			if (assist.eligible(this.character)) {
				assist.defeated(this.character);
			}
			return "Maya looks a bit surprised as you restrain her opponent. <i>\"You're going to side with me? I appreciate the thought.\"</i> She kneels down in front of " + target.name()
					+ " and kisses her softly while staring deeply into her eyes. She turns her attention toward you and leans close to whisper in your ear. <i>\"But I don't need any help\"</i> "
					+ "You don't notice her hand snake between your legs until she suddenly gives your balls a painful flick. The shock and surprise makes you lose your grip on " + target.name()
					+ ", who twists out of your arms and pins you to the floor. Suddenly you're the one being double-teamed. Her eyes have a certain glazed over appearance that tells you "
					+ "she's under Maya's influence.<p>"
					+ "Maya casually moves between your legs, where she can easily reach your dick and " + target.name() + "'s slit. You groan despite yourself as she starts to stroke your shaft "
					+ "with both hands. Her mouth goes to work on the other girl's pussy. The two of you writhe in pleasure, completely at the mercy of the veteran sexfighter. " + target.name()
					+ " kisses you passionately as you both cum simultaneously.";
		}
	}

	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if (target.human()) {
			return "In the middle of your fight with " + assist.name() + ", you feel an overwhelming presence approach from behind. She apparently senses it too, because you both stop and turn "
					+ "to see Maya confidently walking towards you. She smiles and looks at the two of you. <i>\"Eenie... Meenie... Minie....\"</i> Her finger flicks in your direction. <i>\"Mo.\"</i><br>"
					+ "You take a step back and prepare to defend yourself, but she's too fast. She reaches you in an instant and sweeps your feet out from under you, depositing you on the floor. She "
					+ "pins your arms helplessly under her legs and sits casually on your chest. She's lighter than she looks, but you're still completely unable to move her. <i>\"Sorry, it's just your "
					+ "bad luck. I could have helped you instead, but I don't do this very often anymore and I wanted to watch a cute boy orgasm up close. You should try to enjoy your defeat, it's just another "
					+ "part of the Game.\"</i><br>";
		}
		else {
			return "In the middle of your fight with " + target.name() + ", you feel an overwhelming presence approach from behind. She apparently senses it too, because you both stop and turn "
					+ "to see Maya confidently walking towards you. She smiles and looks at the two of you. <i>\"Eenie... Meenie... Minie....\"</i> She points toward " + target.name() + ". <i>\"Mo.\"</i><br>"
					+ "Before the girl can react, Maya is on her and kisses her passionately. " + target.name() + " struggles for just a moment before going limp. Maya moves to licking and sucking "
					+ "her neck, while she eases the helpless girl to the floor. She glances back at you and you feel a chill run down your spine as her eyes seem to bore into you. <i>\"Are you "
					+ "going to make me do all the work? There's a lovely, naked girl in front of you who needs a good licking. Don't keep her waiting.</i><br>";
		}
	}

	@Override
	public void watched(Combat c, Character target, Character viewer) {
		SceneManager.play(SceneFlag.MayaWatch);
	}

	@Override
	public String describe() {
		return "Maya is a beautiful young woman in her early twenties, though she carries herself with the grace of a more mature lady. She has soft, shapely breasts, larger than her slim frame would "
				+ "imply. Her waist length, raven black hair is tied in a single braid down her back. She wears elbow-length silk gloves, giving the appearance of sensual elegance. Her eyes are a beautiful dark "
				+ "blue, behind her red-framed glasses, but every so often the light catches them in a way that makes you think there might be something dangerous inside.";
	}

	@Override
	public void draw(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		if (!opponent.human()) {
			return;
		}

		Global.flag(Flag.Clue1);
		SceneManager.play(SceneFlag.MayaDraw);
	}

	@Override
	public boolean fightFlight(Character opponent) {
		return fit();
	}

	@Override
	public boolean attack(Character opponent) {
		return true;
	}

	@Override
	public void ding() {
		var numIncreases = (Global.random(3) / 2) + 2;

		for (var i = 0; i < numIncreases; i++) {
			switch (Global.random(5)) {
				case 0:
					character.mod(Attribute.Power, 1);
					break;
				case 1:
					character.mod(Attribute.Seduction, 1);
					break;
				case 2:
					character.mod(Attribute.Cunning, 1);
					break;
				case 3:
					character.mod(Attribute.Unknowable, 1);
					break;
				case 4:
					character.mod(Attribute.Hypnosis, 1);
					break;
			}
		}

		character.getStamina().gainMax(6);
		character.getArousal().gainMax(7);
		character.getMojo().gainMax(4);
	}

	@Override
	public String startBattle(Character opponent) {
		return "Maya smiles softly as she confidently steps toward you. <i>\"Are you simply unfortunate or were you actually hoping to challenge me? What a brave boy. I'll try not to "
				+ "disappoint you.\"</i>";
	}

	@Override
	public boolean fit() {
		return !character.nude();
	}

	@Override
	public boolean night() {
		return false;
	}

	@Override
	public void advance(int rank) {
	}

	@Override
	public boolean checkMood(Emotion mood, int value) {
		switch (mood) {
			case dominant:
				return value >= 50;
			case nervous:
			case desperate:
				return value >= 200;
			default:
				return value >= 100;
		}
	}

	@Override
	public float moodWeight(Emotion mood) {
		switch (mood) {
			case dominant:
				return 1.4f;
			case nervous:
			case desperate:
				return .5f;
			default:
				return 1f;
		}
	}

	@Override
	public String image() {
		return null;
	}

	@Override
	public void pickFeat() {
		var available = new ArrayList<Trait>();
		for (var feat : Global.getFeats()) {
			if (!character.has(feat) && feat.meetsRequirement(character)) {
				available.add(feat);
			}
		}

		if (!available.isEmpty()) {
			character.add((Trait) available.toArray()[Global.random(available.size())]);
		}
	}

	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		if (assist.human()) {
			return "As you attempt to hold off Maya, you see " + target.name() + " stealthily approaching behind her. Looks like you're about to get some help. Maya suddenly "
					+ "winks at you and her foot darts up almost playfully between your legs. She barely seemed to use any force, but the impact is still staggering. You "
					+ "crumple to the floor, holding your balls in debilitating pain.<p>"
					+ target.name() + " hesitates, realizing she doesn't have the advantage of superior numbers. Maya is already moving, though, and pounces on the other girl "
					+ "before she can escape. From your position on the floor, you can't see exactly what is happening, but it's clear Maya is overwhelming her. As soon as the "
					+ "pain subsides, you force yourself back to your feet. You'd hoped to team up with " + target.name() + ", but she is already trembling in orgasm under Maya's "
					+ "fingers. The older girl returns her attention to you and smiles. <i>\"Sorry about the interruption. Where were we?\"</i>";
		}
		else {
			return assist.name() + " isn't likely to be able to hold off Maya on her own. Your best bet is to work together to take her down. You creep up behind her, with Maya "
					+ "showing no sign of noticing you. When you've gotten close enough, you lunge toward her, hoping to catch her from behind. To your surprise, she vanishes the "
					+ "moment you touch her and you stumble forward into " + assist.name() + ". You turn around and see Maya standing a couple feet away. You've lost the element of "
					+ "surprise (you probably never had it), but it's still 2 on 1.<br>"
					+ "You suddenly feel " + assist.name() + " grab you from behind. You turn your head and notice her eyes are dull and unfocused. Maya must have hypnotized her to "
					+ "help trap you. Maya speaks up in a melodic voice. <i>\"How rude of you to interrupt a perfectly enjoyable fight. Naughty boys should be punished.\"</i> She strips off your clothes and "
					+ "runs her fingers over your exposed dick. You immediately grow hard under her touch. She's too skilled with her hands for you to hold back and you're completely "
					+ "unable to defend yourself. She makes you cum embarrassingly quickly and both girls discard you unceremoniously to the floor. Maya snaps her fingers in front of "
					+ assist.name() + "'s face to bring her out of the trance and the girl looks down at your defeated form in confusion. <i>\"Thank you for your cooperation. Now we can "
					+ "continue our fight without interruption.\"</i>";
		}
	}

	@Override
	public CommentGroup getComments() {
		var comments = new CommentGroup();
		comments.put(CommentSituation.SELF_BUSTED,
				"<i>\"Good, this is so much more satisfying when you put up a struggle.\"</i> Maya says, trying her best to keep her composure, though you see her knees get a little weak and her voice sounds slightly choked-up.");
		comments.put(new SkillComment(SkillTag.PET, true),
				"<i>\"Your little pet isn't going to pose much of a challenge.\"</i>");
		comments.put(new SkillComment(Attribute.Arcane, true),
				"<i>\"Nice magic, but it's clear you're still learning.\"</i>");
		comments.put(new SkillComment(Attribute.Fetish, true),
				"<i>\"Fetish magic, huh? It's pretty specialized to this type of competition, but I can't say I'm a fan.\"</i>");
		comments.put(new SkillComment(Attribute.Science, true),
				"<i>\"Did Jett take you how to make that? It's a cute toy.\"</i>");
		comments.put(new SkillComment(Attribute.Animism, true),
				"<i>\"Ooh, good boy. If you roll over and behave, I don't mind petting you a bit.\"</i>");

		return comments;
	}

	@Override
	public CommentGroup getResponses() {
		return new CommentGroup();
	}

	@Override
	public int getCostumeSet() {
		return 1;
	}

	@Override
	public void declareGrudge(Character opponent, Combat c) {
	}

	@Override
	public void resetOutfit() {
		character.outfit[Character.OUTFITTOP].clear();
		character.outfit[Character.OUTFITBOTTOM].clear();

		character.outfit[Character.OUTFITTOP].add(Clothing.camisole);
		character.outfit[Character.OUTFITTOP].add(Clothing.blouse);
		character.outfit[Character.OUTFITBOTTOM].add(Clothing.lacepanties);
		character.outfit[Character.OUTFITBOTTOM].add(Clothing.skirt);
	}
}
