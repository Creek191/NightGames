package characters;

import com.google.gson.JsonObject;
import combat.Combat;
import combat.Encounter;
import combat.Tag;
import global.Global;
import global.Match;
import skills.Tactics;
import trap.Trap;

import java.awt.image.BufferedImage;
import java.util.Scanner;

/**
 * Dummy character implementation used for displaying sprites of arbitrary characters
 */
public class Dummy extends Character {
	int costume;
	boolean topouter;
	boolean top;
	boolean topinner;
	boolean botouter;
	boolean botinner;
	int blush;
	boolean strapped;

	public Dummy(String name) {
		super(name, 1);
		costume = 1;
		topouter = false;
		top = false;
		topinner = false;
		botouter = false;
		botinner = false;
		blush = 0;
		strapped = false;
		mood = Emotion.confident;
	}

	public Dummy(String name, int costume, boolean dressed) {
		this(name);
		this.costume = costume;
		if (dressed) {
			dress();
		}
	}

	@Override
	public void ding() {
	}

	@Override
	public void detect() {
	}

	@Override
	public void faceOff(Character opponent, Encounter enc) {
	}

	@Override
	public void spy(Character opponent, Encounter enc) {
	}

	@Override
	public String describe(int perception) {
		return null;
	}

	@Override
	public void victory(Combat c, Tag flag) {
	}

	@Override
	public void defeat(Combat c, Tag flag) {
	}

	@Override
	public void intervene3p(Combat c, Character target, Character assist) {
	}

	@Override
	public void victory3p(Combat c, Character target, Character assist) {
	}

	@Override
	public void act(Combat c) {
	}

	@Override
	public void move(Match match) {
	}

	@Override
	public void draw(Combat c, Tag flag) {
	}

	@Override
	public boolean human() {
		return false;
	}

	@Override
	public String bbLiner() {
		return null;
	}

	@Override
	public String nakedLiner() {
		return null;
	}

	@Override
	public String stunLiner() {
		return null;
	}

	@Override
	public String taunt() {
		return null;
	}

	@Override
	public void intervene(Encounter enc, Character p1, Character p2) {
	}

	@Override
	public boolean resist3p(Combat combat, Character intruder, Character assist) {
		return false;
	}

	@Override
	public void showerScene(Character target, Encounter encounter) {
	}

	@Override
	public JsonObject save() {
		return null;
	}

	@Override
	public void load(Scanner loader) {
	}

	@Override
	public void afterParty() {
	}

	@Override
	public void endOfTurn(Combat c, Character opponent) {
	}

	@Override
	public Emotion moodSwing() {
		return null;
	}

	@Override
	public void resetOutfit() {
	}

	@Override
	public String challenge(Character opponent) {
		return null;
	}

	@Override
	public void promptTrap(Encounter enc, Character target, Trap trap) {
	}

	@Override
	public void counterattack(Character target, Tactics type, Combat c) {
	}

	/**
	 * Sets the dummy's costume level
	 *
	 * @param level The new costume level
	 */
	public void setCostumeLevel(int level) {
		costume = level;
		clearSpriteImages();
	}

	/**
	 * Sets the dummy's blush level
	 *
	 * @param level The new blush level
	 */
	public void setBlush(int level) {
		blush = level;
	}

	/**
	 * Completely dresses the dummy
	 */
	public void dress() {
		topouter = true;
		top = true;
		topinner = true;
		botouter = true;
		botinner = true;
	}

	/**
	 * Undresses the dummy
	 */
	public void undress() {
		topouter = false;
		top = false;
		topinner = false;
		botouter = false;
		botinner = false;
	}

	/**
	 * Sets whether the dummy is wearing top outerwear
	 */
	public void setTopOuter(boolean wearing) {
		topouter = wearing;
	}

	/**
	 * Sets whether the dummy is wearing a top
	 */
	public void setTop(boolean wearing) {
		top = wearing;
	}

	/**
	 * Sets whether the dummy is wearing top innerwear
	 */
	public void setTopInner(boolean wearing) {
		topinner = wearing;
	}

	/**
	 * Sets whether the dummy is wearing bottom outerwear
	 */
	public void setBotOuter(boolean wearing) {
		botouter = wearing;
	}

	/**
	 * Sets whether the dummy is bottom innerwear
	 */
	public void setBotInner(boolean wearing) {
		botinner = wearing;
	}

	/**
	 * Sets whether the dummy is wearing a strapon
	 */
	public void setStrapped(boolean wearing) {
		strapped = wearing;
	}

	/**
	 * Set the dummy's primary mood
	 *
	 * @param mood The emotion to set
	 */
	public void setMood(Emotion mood) {
		this.mood = mood;
	}

	@Override
	public String getPortrait() {
		return "assets/" + name + "_" + mood.name() + ".jpg";
	}

	@Override
	public BufferedImage getSpriteImage() {
		var res = "";
		if (Global.gui().lowRes()) {
			res = "_low";
		}

		if (spriteBody == null) {
			spriteBody = loadImage("assets/" + name + "/" + name + costume + "_Sprite" + res + ".png");
			if (spriteBody == null) {
				spriteBody = loadImage("assets/" + name + "/" + name + "_Sprite" + res + ".png");
			}
		}
		if (spriteBody == null) {
			return null;
		}

		var sprite = new BufferedImage(spriteBody.getWidth(), spriteBody.getHeight(), spriteBody.getType());
		var graphics = sprite.createGraphics();

		// Clothes drawn behind the body sprite
		if (topouter) {
			if (spriteCoattail == null) {
				spriteCoattail = loadImage("assets/" + name + "/" + name + costume + "_outerback" + res + ".png");
			}
			graphics.drawImage(spriteCoattail, 0, 0, null);
		}
		if (top) {
			if (spriteShirttail == null) {
				spriteShirttail = loadImage("assets/" + name + "/" + name + costume + "_topback" + res + ".png");
			}
			graphics.drawImage(spriteShirttail, 0, 0, null);
		}

		graphics.drawImage(spriteBody, 0, 0, null);
		graphics.drawImage(loadImage("assets/" + name + "/" + name + "_" + mood.name() + res + ".png"), 0, 0, null);

		if (blush >= 3) {
			if (blushHigh == null) {
				blushHigh = loadImage("assets/" + name + "/" + name + "_blush3" + res + ".png");
			}
			graphics.drawImage(blushHigh, 0, 0, null);
		}
		else if (blush == 2) {
			if (blushMed == null) {
				blushMed = loadImage("assets/" + name + "/" + name + "_blush2" + res + ".png");
			}
			graphics.drawImage(blushMed, 0, 0, null);
		}
		else if (blush == 1) {
			if (blushLow == null) {
				blushLow = loadImage("assets/" + name + "/" + name + "_blush1" + res + ".png");
			}
			graphics.drawImage(blushLow, 0, 0, null);
		}

		if (spriteAccessory == null) {
			spriteAccessory = loadImage("assets/" + name + "/" + name + costume + "_accessory" + res + ".png");
		}
		graphics.drawImage(spriteAccessory, 0, 0, null);

		if (botinner) {
			if (strapped) {
				if (spriteStrapon == null) {
					spriteStrapon = loadImage("assets/" + name + "/" + name + "_strapon" + res + ".png");
				}
				graphics.drawImage(spriteStrapon, 0, 0, null);
			}
			else {
				if (spriteUnderwear == null) {
					spriteUnderwear = loadImage("assets/" + name + "/" + name + costume + "_underwear" + res + ".png");
				}
				graphics.drawImage(spriteUnderwear, 0, 0, null);
			}
		}

		if (topinner) {
			if (spriteBra == null) {
				spriteBra = loadImage("assets/" + name + "/" + name + costume + "_bra" + res + ".png");
			}
			graphics.drawImage(spriteBra, 0, 0, null);
		}

		if (botouter) {
			if (spriteBottom == null) {
				spriteBottom = loadImage("assets/" + name + "/" + name + costume + "_bottom" + res + ".png");
			}
			graphics.drawImage(spriteBottom, 0, 0, null);
		}

		if (top) {
			if (spriteTop == null) {
				spriteTop = loadImage("assets/" + name + "/" + name + costume + "_top" + res + ".png");
			}
			graphics.drawImage(spriteTop, 0, 0, null);
		}

		if (topouter) {
			if (spriteOuter == null) {
				spriteOuter = loadImage("assets/" + name + "/" + name + costume + "_outer" + res + ".png");
			}
			graphics.drawImage(spriteOuter, 0, 0, null);
		}

		graphics.dispose();
		return sprite;
	}

	@Override
	public void watcher(Combat c, Character victory, Character defeated) {
	}

	@Override
	public void watched(Combat c, Character voyeur, Character defeated) {
	}
}
