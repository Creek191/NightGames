package characters;

import Comments.CommentGroup;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Tag;
import daytime.Daytime;
import global.Match;
import skills.Skill;

import java.io.Serializable;
import java.util.HashSet;

public interface Personality extends Serializable {
	/**
	 * Determine the next combat action used by the NPC
	 *
	 * @param available A list of available actions
	 * @param c         A reference to the ongoing combat
	 * @return The skill chosen by the character
	 */
	Skill act(HashSet<Skill> available, Combat c);

	/**
	 * Determine the next movement action used by the NPC
	 *
	 * @param available A list of available actions
	 * @param radar     A list of known enemy character locations
	 * @param match     A reference to the ongoing match
	 * @return The movement chosen by the NPC
	 */
	Action move(HashSet<Action> available, HashSet<Movement> radar, Match match);

	/**
	 * Gets a reference to the NPC object
	 */
	NPC getCharacter();

	/**
	 * Performs rest (daytime) actions like training on purchasing items
	 *
	 * @param time The amount of time the NPC can spend
	 * @param day  A reference to the daytime object
	 */
	void rest(int time, Daytime day);

	/**
	 * Get a response line when the NPC has performed a ball-busting move
	 *
	 * @return A string containing the NPC's response
	 */
	String bbLiner();

	/**
	 * Get a response line when encountering the NPC while they are naked
	 *
	 * @return A string containing the NPC's response
	 */
	String nakedLiner();

	/**
	 * Get a response line when the NPC has been stunned
	 *
	 * @return A string containing the NPC's response
	 */
	String stunLiner();

	/**
	 * Get a response line when the NPC is using a taunt
	 *
	 * @return A string containing the NPC's response
	 */
	String taunt();

	/**
	 * Processes the result of a combat where the NPC has won
	 *
	 * @param c    A reference to the ongoing combat
	 * @param flag A flag defining how the combat was won (e.g. intercourse/anal)
	 */
	void victory(Combat c, Tag flag);

	/**
	 * Processes the result of a combat where the NPC has lost
	 *
	 * @param c    A reference to the ongoing combat
	 * @param flag A flag defining how the combat was lost (e.g. intercourse/anal)
	 */
	void defeat(Combat c, Tag flag);

	/**
	 * Gets a scene where the NPC wins in combat through assistance from another character.
	 * Either target or assist is the player
	 *
	 * @param c      A reference to the ongoing combat
	 * @param target The character the NPC is fighting against
	 * @param assist The character assisting the NPC
	 * @return A string containing the scene text
	 */
	String victory3p(Combat c, Character target, Character assist);

	/**
	 * Gets a scene where the NPC intervenes in combat between two characters.
	 * Either target or assist is the player
	 *
	 * @param c      A reference to the ongoing combat
	 * @param target The character to attack
	 * @param assist The character to assist
	 * @return A string containing the scene text
	 */
	String intervene3p(Combat c, Character target, Character assist);

	/**
	 * Unused
	 */
	String resist3p(Combat c, Character target, Character assist);

	/**
	 * Runs the scene where a third character watches a combat encounter.
	 * Usually only plays a scene when the viewer is the player
	 *
	 * @param c      A reference to the ongoing combat
	 * @param target The character actively participating in the combat
	 * @param viewer The character watching the combat encounter
	 */
	void watched(Combat c, Character target, Character viewer);

	/**
	 * Returns a description of the character
	 */
	String describe();

	/**
	 * Processes the result of a combat resulting in a draw
	 *
	 * @param c    A reference to the ongoing combat
	 * @param flag A flag defining how the combat has ended (e.g. intercourse/anal)
	 */
	void draw(Combat c, Tag flag);

	/**
	 * Determines whether the NPC will fight or flee from the character initiating combat
	 *
	 * @param opponent The character initiating combat
	 * @return True if the NPC chooses to fight, otherwise False
	 */
	boolean fightFlight(Character opponent);

	/**
	 * Determines whether the NPC is willing to attack an opponent
	 *
	 * @param opponent The opponent to determine response for
	 * @return True if the NPC wants to attack, otherwise False
	 */
	boolean attack(Character opponent);

	/**
	 * Increases the NPC's skills and stats after a level-up
	 */
	void ding();

	/**
	 * Get a response line when the NPC is entering combat
	 *
	 * @param opponent The opponent for the initiated combat
	 * @return A string containing the NPC's response
	 */
	String startBattle(Character opponent);

	/**
	 * Determines whether the NPC considers themselves fit for combat
	 */
	boolean fit();

	/**
	 * Runs the scene where the player spends the night (post-match) with the NPC
	 */
	boolean night();

	/**
	 * Advances the NPC to the specified rank
	 *
	 * @param rank The new rank of the NPC
	 */
	void advance(int rank);

	/**
	 * Checks whether the NPC is currently in the specified mood
	 *
	 * @param mood  The mood to check for
	 * @param value The current value of the specified mood
	 * @return True if the NPC is in the specified mood, otherwise False
	 */
	boolean checkMood(Emotion mood, int value);

	/**
	 * Gets the NPC's weight modifier for the specified mood
	 *
	 * @param mood The mood to get the modifier for
	 * @return A float specifying the mood modifier (base = 1.0F)
	 */
	float moodWeight(Emotion mood);

	/**
	 * Gets the path to the NPC's current base image
	 */
	String image();

	/**
	 * Picks a random feat the NPC meets the requirements for and assigns it to them
	 */
	void pickFeat();

	/**
	 * Gets a collection of comments by the NPC applicable to different situations
	 */
	CommentGroup getComments();

	/**
	 * Unused
	 */
	CommentGroup getResponses();

	/**
	 * Gets the id of the NPC's currently used costume set
	 */
	int getCostumeSet();

	/**
	 * Declares a grudge against the specified opponent after being defeated,
	 * based on the NPC's personality and the combat outcome
	 *
	 * @param opponent The opponent to declare a grudge against
	 * @param c        A reference to the ongoing combat
	 */
	void declareGrudge(Character opponent, Combat c);

	/**
	 * Resets the NPC's outfit to its default state
	 */
	void resetOutfit();
}
