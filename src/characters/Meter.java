package characters;

import java.io.Serializable;

public class Meter implements Serializable, Cloneable {
	/**
	 *
	 */
	private static final long serialVersionUID = 2L;
	private int current;
	private int max;

	public Meter(int max) {
		this.max = max;
		this.current = 0;
	}

	/**
	 * Gets the current value of the meter
	 */
	public int get() {
		return current;
	}

	/**
	 * Gets the maximum value of the meter
	 */
	public int max() {
		return max;
	}

	/**
	 * Reduces the meter by the specified value
	 *
	 * @param amount The amount to reduce the meter by
	 */
	public void reduce(int amount) {
		if (amount > 0) {
			current -= amount;
		}
		if (current < 0) {
			current = 0;
		}
	}

	/**
	 * Reduces the meter by the specified value without setting it to 0
	 *
	 * @param amount The maximum amount to reduce the meter by
	 * @return The actual amount the meter was reduced by
	 */
	public int minimize(int amount) {
		var mag = amount;
		if (mag > 0) {
			if (mag >= current) {
				mag = current - 1;
			}
			current -= mag;
		}
		return mag;
	}

	/**
	 * Restores the meter by the specified amount
	 *
	 * @param amount The amount to restore the meter by
	 */
	public void restore(int amount) {
		current += amount;
		if (current > max()) {
			current = max();
		}
	}

	/**
	 * Restores the meter by the specified amount without maxing it out
	 *
	 * @param amount The amount to restore the meter by
	 * @return The actual amount the meter was restored by
	 */
	public int edge(int amount) {
		var mag = amount;
		if (current + mag >= max) {
			mag = max - (current + 1);
		}
		current += mag;
		return mag;
	}

	/**
	 * Adds the specified percentage to the meter
	 *
	 * @param percentage The percentage to increase the meter by
	 * @return The actual amount the meter was increased by
	 */
	public int addPercent(int percentage) {
		var amount = (percentage * max()) / 100;
		restore(amount);
		return amount;
	}

	/**
	 * Checks whether the meter is empty
	 */
	public boolean isEmpty() {
		return current <= 0;
	}

	/**
	 * Checks whether the meter is full
	 */
	public boolean isFull() {
		return current == max();
	}

	/**
	 * Completely empties the meter
	 */
	public void empty() {
		current = 0;
	}

	/**
	 * Completely fills the meter
	 */
	public void fill() {
		current = max();
	}

	/**
	 * Sets the meter to the specified value
	 *
	 * @param value The value to set the meter to
	 */
	public void set(int value) {
		current = value;
		if (current > max()) {
			current = max();
		}
	}

	/**
	 * Increases the maximum of the meter
	 *
	 * @param amount The amount to increase the maximum by
	 */
	public void gainMax(int amount) {
		max += amount;
		if (current > max()) {
			current = max();
		}
	}

	/**
	 * Sets the maximum of the meter
	 *
	 * @param value The value to set the maximum to
	 */
	public void setMax(int value) {
		max = value;
		current = max;
	}

	/**
	 * Gets the percentage of the meter that is filled
	 *
	 * @return An integer between 0 and 100
	 */
	public int percent() {
		return 100 * current / max();
	}

	/**
	 * Creates a clone of the meter
	 *
	 * @return The cloned Meter
	 */
	public Meter clone() throws CloneNotSupportedException {
		return (Meter) super.clone();
	}
}
