package characters;

import Comments.CommentGroup;
import Comments.CommentSituation;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Result;
import combat.Tag;
import daytime.Daytime;
import global.*;
import items.Clothing;
import items.Toy;
import items.Trophy;
import scenes.SceneFlag;
import scenes.SceneManager;
import skills.*;
import stance.Stance;
import status.Stsflag;

import java.util.ArrayList;
import java.util.HashSet;

public class Valerie implements Personality {
	public NPC character;

	public Valerie() {
		character = new NPC("Valerie", ID.VALERIE, 10, this);

		// Set clothing
		resetOutfit();
		character.closet.add(Clothing.bra);
		character.closet.add(Clothing.noblevest);
		character.closet.add(Clothing.noblecloak);
		character.closet.add(Clothing.panties);
		character.closet.add(Clothing.tdresspants);
		character.change(Modifier.normal);
		character.setUnderwear(Trophy.ValerieTrophy);

		// Set base stats
		character.mod(Attribute.Power, 5);
		character.mod(Attribute.Discipline, 6);
		character.mod(Attribute.Seduction, 5);
		character.mod(Attribute.Seduction, 4);
		character.getStamina().gainMax(55);
		character.getArousal().gainMax(75);
		character.getMojo().gainMax(15);
		character.money += 2000;
		character.gain(Toy.Crop3);
		character.gain(Toy.Dildo);
		character.gain(Toy.Onahole);

		// Set starting traits
		character.add(Trait.female);
		character.add(Trait.composure);
		character.add(Trait.cropexpert);
		character.add(Trait.buttslut);

		// Set AI logic
		character.plan = Emotion.hunting;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.sneaking, 1);
		character.preferredSkills.add(UseCrop.class);
		character.preferredSkills.add(DominatingGaze.class);
		character.preferredSkills.add(Punishment.class);
		character.preferredSkills.add(Footjob.class);
		character.preferredSkills.add(MastersOrder.class);
		character.preferredSkills.add(Tie.class);
		if (Global.checkFlag(Flag.PlayerButtslut)) {
			character.preferredSkills.add(AssFuck.class);
			character.preferredSkills.add(Strapon.class);
			character.preferredSkills.add(FingerAss.class);
			character.gain(Toy.Strapon);
		}

		Global.gainSkills(character);
	}

	@Override
	public Skill act(HashSet<Skill> available, Combat c) {
		var mandatory = new HashSet<Skill>();
		for (var skill : available) {
			if (skill.toString().equalsIgnoreCase("Command")
					|| skill.toString().equalsIgnoreCase("Ass Fuck")) {
				mandatory.add(skill);
			}
			if (character.is(Stsflag.orderedstrip)
					&& (skill.toString().equalsIgnoreCase("Undress")
					|| skill.toString().equalsIgnoreCase("Strip Tease"))) {
				mandatory.add(skill);
			}
			if (Global.checkFlag(Flag.PlayerButtslut) && character.has(Trait.strapped)) {
				if (skill.toString().equalsIgnoreCase("Mount") && character.has(Toy.Strapon2)) {
					if (Global.random(3) == 0) {
						mandatory.add(skill);
					}
				}
				if (skill.toString().equalsIgnoreCase("Turn Over")) {
					mandatory.add(skill);
				}
			}
		}

		if (!mandatory.isEmpty()) {
			var actions = mandatory.toArray(new Skill[0]);
			return actions[Global.random(actions.length)];
		}

		Skill chosen;
		var priority = character.parseSkills(available, c);
		if (Global.checkFlag(Flag.hardmode) && !c.hasModifier(Modifier.quiet)) {
			chosen = character.prioritizeSimulated(priority, c);
		}
		else {
			chosen = character.prioritizeRandom(priority);
		}

		if (chosen != null) {
			return chosen;
		}

		var actions = available.toArray(new Skill[0]);
		return actions[Global.random(actions.length)];
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		return character.parseMoves(available, radar, match);
	}

	@Override
	public NPC getCharacter() {
		return character;
	}

	@Override
	public void rest(int time, Daytime day) {
		var available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		if (character.rank > 0) {
			available.add("Workshop");
		}
		available.add("Play Video Games");
		if (!(character.has(Toy.Strapon) || character.has(Toy.Strapon2))
				&& character.money >= 600
				&& character.getPure(Attribute.Seduction) >= 20) {
			character.gain(Toy.Strapon);
			character.money -= 600;
		}
		if (!(character.has(Toy.Tickler) || character.has(Toy.Tickler2))
				&& character.money >= 300) {
			character.gain(Toy.Tickler);
			character.money -= 300;
		}
		for (var i = 0; i < time - 4; i++) {
			var location = available.get(Global.random(available.size()));
			day.visit(location, character, Global.random(character.money));
			if (!location.equalsIgnoreCase("Exercise")
					&& !location.equalsIgnoreCase("Browse Porn Sites")) {
				available.remove(location);
			}
		}
		if (character.getAffection(Global.getPlayer()) > 0) {
			Global.modCounter(Flag.JewelDWV, 1);
		}
		character.visit(4);
	}

	/**
	 * Checks whether the character is composed
	 */
	private boolean composed() {
		return character.is(Stsflag.composed);
	}

	@Override
	public String bbLiner() {
		if (composed()) {
			return "";
		}
		else {
			return "";
		}
	}

	@Override
	public String nakedLiner() {
		if (composed()) {
			return "Valerie's expression shows just a hint of frustration with herself. <i>\"I suppose I need to work harder on shoring up my defenses, if you could strip me this easily.\"</i>";
		}
		else {
			return " Valerie's eyes widen as she tries momentarily to cover herself. <i>\"Damn it, you're going to pay for this.\"</i>";
		}
	}

	@Override
	public String stunLiner() {
		if (composed()) {
			return "Valerie's eyes fall closed for a moment. <i>\"Touché...\"</i> she breathes out.";
		}
		else {
			return "Valerie grimaces from pain, but she's too out of it to fight back. <i>\"Urgh... that was a bullshit tactic and you know it...\"</i>";
		}
	}

	@Override
	public String taunt() {
		if (composed()) {
			return "Valerie's eyes narrow, almost imperceptibly. <i>\"I know you can do better than this.\"</i>";
		}
		else {
			return "Valerie grins widely, and you can see a touch of madness in her eyes for just a moment. <i>\"Aha! Now it's your turn, you bastard!\"</i>";
		}
	}

	@Override
	public void victory(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		character.clearGrudge(opponent);
		if (!opponent.human()) {
			return;
		}

		if (composed()) {
			if (flag == Result.anal && c.stance.penetration(character)) {
				Global.modCounter(Flag.PlayerAssLosses, 1);
				SceneManager.play(SceneFlag.ValeriePeggingVictory);
			}
			else if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.ValerieComposedSexVictory);
			}
			else if (Global.random(2) == 0) {
				SceneManager.play(SceneFlag.ValerieComposedForeplayVictoryAlt);
			}
			else {
				SceneManager.play(SceneFlag.ValerieComposedForeplayVictory);
			}
		}
		else {
			if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.ValerieBrokenSexVictory);
			}
			else {
				SceneManager.play(SceneFlag.ValerieBrokenForeplayVictory);
			}
		}
	}

	@Override
	public void defeat(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		declareGrudge(opponent, c);
		if (!opponent.human()) {
			return;
		}

		if (composed()) {
			if (flag == Result.anal && c.stance.penetration(opponent)) {
				SceneManager.play(SceneFlag.ValerieComposedAnalDefeat);
			}
			else if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.ValerieComposedSexDefeat);
			}
			else if (Global.random(2) == 0) {
				SceneManager.play(SceneFlag.ValerieComposedForeplayDefeatAlt);
			}
			else {
				SceneManager.play(SceneFlag.ValerieComposedForeplayDefeat);
			}
		}
		else {
			if (flag == Result.anal && c.stance.penetration(opponent)) {
				SceneManager.play(SceneFlag.ValerieBrokenAnalDefeat);
			}
			else if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.ValerieBrokenSexDefeat);
			}
			else {
				SceneManager.play(SceneFlag.ValerieBrokenForeplayDefeat);
			}
		}
	}

	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		if (target.human()) {
			return "<i>\"Well,\"</i> Valerie says, taking a moment to brush a strand of hair out of her face as she takes a moment to regain her composure. <i>\"This wasn't exactly how I'd planned to end this match, but I do hope you won't be mad if I take advantage of it.\"</i> She steps toward you, leaning in to plant a quick kiss on your cheek before she gets down to business. <i>\"Don't worry,\"</i> she whispers as her hands slowly slide down your body, toward your cock. <i>\"You're going to love this.\"</i>";
		}
		else {
			return "Valerie's eyes widen with glee as she sees " + target.name() + " displayed before her, ready for her to finish off. An uncharacteristic madness is in her eyes, and you almost feel sorry for " + target.name() + ". Valerie can get quite wild when she's been pushed too far, and it seems that " + target.name() + " has made the mistake of doing just that.<p>"
					+ "<i>\"What a delightful turn of events,\"</i> Valerie says, stepping slowly toward " + target.name() + " and you. <i>\"How shall we do this?\"</i> She places a finger beneath " + target.name() + "'s chin and lifts her face up so she can make eye contact. <i>\"So many ways to get you off, but that would hardly be a suitable punishment. Maybe we should just tie you up and then "
					+ Roster.get(ID.PLAYER).name() + " and I can have some fun our own while you can do nothing but watch?\"</i><p>"
					+ "While that idea is certainly tempting, it would rather go against the spirit of the Games.<p>"
					+ "<i>\"The Games?\"</i> Valerie says, blinking in confusion for a moment. <i>\"Oh, right.\"</i> She looks back at " + target.name() + " and purses her lips. <i>\"Alright, I've decided. Hard and fast, no mercy, and if you don't scream loudly enough, we're doing it again and again until you do.\"</i><p>"
					+ "Okay, you definitely feel sorry for " + target.name() + ". But not nearly enough to switch teams and help her out. She knew what she was getting in for, and now she has to pay the price<p>";
		}
	}

	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		if (target.human()) {
			return "Your fight against " + assist.name() + " has barely gotten started when you're distracted by a tap on the shoulder. You tense up, knowing this can't be good news, and you take a step back to see who's arrived, without letting " + assist.name() + " out of your sight. Valerie appears in your field of vision, and she gives you an apologetic smile. <i>\"I hope you don't take offense to this, "
					+ Roster.get(ID.PLAYER).name() + ",\"</i> she says, <i>\"but I'll be helping " + assist.name() + " out with this one. Don't worry, I'll make sure not to let her go too hard on you.\"</i><p>"
					+ "You glance back and forth between " + assist.name() + " and Valerie, gauging your chances at taking them both on, or perhaps even convincing " + assist.name() + " to ally with you against Valerie, but neither option has good odds. You make a last-ditch attempt to lunge at " + assist.name() + ", in the hope that you might somehow be able to take her out quickly, but Valerie manages to restrain you before you can land a single attack.<p>"
					+ "<i>\"I can't blame you for not giving up, but I'm afraid this ends now,\"</i> Valerie says as she pulls your arms back and restrains them behind your back. <i>\"Just relax and let yourself enjoy it, okay?\"</i>";
		}
		else {
			return "Your fight against " + target.name() + " has barely gotten started when you're distracted by a tap on the shoulder. You tense up, knowing this can't be good news, and you take a step back to see who's arrived, without letting " + target.name() + " out of your sight. Valerie appears in your field of vision, and after making eye contact with you for a moment, her eyes widen and she gives you an apologetic smile. <i>\"Oh!\"</i> she says. <i>\"Did I give you the impression I was here to help out " + target.name() + " against you? Sorry, you can relax; it's the other way around.\"</i><p>"
					+ "You glance back at " + target.name() + " as you hear this, just in time to see her expression change from glee to dread. She makes the mistake of spending too long trying to figure out what to do next, and Valerie uses this time to dash in capture " + target.name() + " in a hold before she can flee.<p>"
					+ "<i>\"You can finish her off,\"</i> Valerie says, turning herself and " + target.name() + " to face you. <i>\"Just don't be too mean. We did have the advantage after all, so it wouldn't be fair to punish her too much.\"</i><p>";
		}
	}

	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		return null;
	}

	@Override
	public void watched(Combat c, Character target, Character viewer) {
		if (viewer.human()) {
			SceneManager.play(SceneFlag.ValerieWatch);
		}
	}

	@Override
	public String describe() {
		if (character.is(Stsflag.composed)) {
			return "Valerie appears calm and collected, not letting a single emotion slip through her facade. Even in the middle of the Games, she's somehow managed to ensure that not a single one of her hairs is out of place. Her eyes examine you closely as she looks for an opening, and her muscles are tensed and ready to strike at any sign of weakness you show.";
		}
		else {
			return "Madness has taken over Valerie's eyes in a way that shouldn't be humanly possible. With the stress of combat keeping her from keeping her composure, she's given up on holding herself back. She's more vulnerable now, but also more dangerous.";
		}
	}

	@Override
	public void draw(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		character.clearGrudge(opponent);
		if (!opponent.human()) {
			return;
		}

		if (composed()) {
			if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.ValerieComposedSexDraw);
			}
			else {
				SceneManager.play(SceneFlag.ValerieComposedForeplayDraw);
			}
		}
		else {
			if (flag == Result.intercourse) {
				SceneManager.play(SceneFlag.ValerieBrokenSexDraw);
			}
			else {
				SceneManager.play(SceneFlag.ValerieBrokenForeplayDraw);
			}
		}
	}

	@Override
	public boolean fightFlight(Character opponent) {
		return fit();
	}

	@Override
	public boolean attack(Character opponent) {
		return true;
	}

	@Override
	public void ding() {
		var numIncreases = (Global.random(3) / 2) + 1;

		character.mod(Attribute.Discipline, 1);

		for (var i = 0; i < numIncreases; i++) {
			switch (Global.random(3)) {
				case 0:
					character.mod(Attribute.Power, 1);
					break;
				case 1:
					character.mod(Attribute.Seduction, 1);
					break;
				case 2:
					character.mod(Attribute.Cunning, 1);
					break;
			}
		}

		character.getStamina().gainMax(3);
		character.getArousal().gainMax(6);
		character.getMojo().gainMax(2);
	}

	@Override
	public String startBattle(Character opponent) {
		if (character.getGrudge() != null) {
			switch (character.getGrudge()) {
				case icequeen:
					return "<i>\"Are you ready to try this again?\"</i> Valerie asks, giving you a slight smile. <i>\"You did quite a good job satisfying me last match, but you made the mistake of leaving me just a bit too sated. Now that I've had my fill, it won't be so easy this time.\"</i>";
				case highcomposure:
					return "The only emotion you can make out in Valerie's face is frustration, but it almost seems like she's making a choice to let you see that. Is she mad about her previous loss to you?<p>"
							+ "<i>\"No, I'm not mad at you for winning,\"</i> Valerie says, shaking her head. <i>\"I'm disappointed in myself for losing control like that. Be warned, I don't plan to let it happen again.\"</i><p>";
				case potentcomposure:
					return "Valerie's face is an utter blank as she faces off against you, without even her normal facade of politeness. All of her efforts seem to be focused on holding herself together right now, and she doesn't even spare the energy to greet you before the start of combat.";
				case enraged:
					return "<i>\"You!\"</i> Valerie snaps as she sees you, a fury burning in her eyes. <i>\"It's time for me to get my revenge for earlier. Don't expect me to hold back this time.\"</i>";
				default:
					break;
			}
		}
		if (character.nude()) {
			return "From the confident expression on Valerie's face, you'd never know that she was naked right now. There might be just a bit of nervousness in her eyes though, from starting off combat with this sort of disadvantage, but she's doing a good job not to let it show. Just as you begin to let your gaze drop to look over her body, you spot her mouth widening into a grin. <i>\"I hope you enjoy the view, but don't let it distract you from what we came here to do. That is, unless you plan to give me an easy win.\"</i>";
		}

		return "Valerie holds her riding crop in her right hand, twirling it around casually for just a moment before she grips it firmly and shifts her stance to focus on the battle. She points the riding crop toward you like a sword and gives you a small grin.  <i>\"En garde.\"</i>";
	}

	@Override
	public boolean fit() {
		return !character.nude() && character.getStamina().percent() >= 50;
	}

	@Override
	public boolean night() {
		if (Global.checkFlag(Flag.ValerieSub)) {
			SceneManager.play(SceneFlag.ValerieAfterMatchSub);
		}
		else {
			SceneManager.play(SceneFlag.ValerieAfterMatch);
		}
		return true;
	}

	@Override
	public void advance(int Rank) {

	}

	@Override
	public boolean checkMood(Emotion mood, int value) {
		if (composed()) {
			switch (mood) {
				case dominant:
				case confident:
					return value >= 50;
				case angry:
				case desperate:
					return value >= 200;
				default:
					return value >= 100;
			}
		}
		else {
			switch (mood) {
				case angry:
				case desperate:
					return value >= 30;
				case nervous:
				case horny:
					return value >= 50;
				case confident:
					return value >= 250;
				default:
					return value >= 100;
			}
		}
	}

	@Override
	public float moodWeight(Emotion mood) {
		if (composed()) {
			switch (mood) {
				case confident:
				case dominant:
					return 1.2f;
				case desperate:
				case angry:
					return .7f;
				default:
					return 1f;
			}
		}
		else {
			switch (mood) {
				case angry:
				case desperate:
					return 1.2f;
				case confident:
				case dominant:
					return .5f;
				default:
					return 1f;
			}
		}
	}

	@Override
	public String image() {
		return null;
	}

	@Override
	public void pickFeat() {
		var available = new ArrayList<Trait>();
		for (var feat : Global.getFeats()) {
			if (!character.has(feat) && feat.meetsRequirement(character)) {
				available.add(feat);
			}
		}
		if (!available.isEmpty()) {
			character.add((Trait) available.toArray()[Global.random(available.size())]);
		}
	}

	@Override
	public CommentGroup getComments() {
		var comments = new CommentGroup();
		if (composed()) {
			comments.put(CommentSituation.VAG_DOM_CATCH_WIN,
					"<i>\"Alright, just let me do all the work now, and let yourself enjoy it.\"</i>");
			comments.put(CommentSituation.VAG_DOM_CATCH_LOSE,
					"<i>\"Don't you want to cum inside me? Please, just let me help you do so.\"</i>");
			comments.put(CommentSituation.VAG_SUB_CATCH_WIN,
					"<i>\"That's it. Do me as hard as you'd like, and fill me up with your seed.\"</i>");
			comments.put(CommentSituation.VAG_SUB_CATCH_LOSE,
					"<i>\"Ah! I mean... slow down, please. You wouldn't want to make me wait for you after I climax, right?\"</i>");
			comments.put(CommentSituation.ANAL_PITCH_WIN,
					"<i>\"Don't worry, you'll forget about all the pain soon enough.\"</i>");
			comments.put(CommentSituation.ANAL_PITCH_LOSE,
					"<i>\"Please forgive me for this, but you left me with few options if I wish to win this match.\"</i>");
			comments.put(CommentSituation.ANAL_CATCH_WIN,
					"<i>\"Haa... you don't need to do this. It's too late anyway. Just... mm... just let yourself go and then leave my rear alone, please?\"</i>");
			comments.put(CommentSituation.ANAL_CATCH_LOSE,
					"<i>\"Please stop... If you keep doing this, I... I'll... Please...\"</i>");
			comments.put(CommentSituation.MOUNT_DOM_WIN,
					"<i>\"Mm, good. There's no point in fighting back at this point. Just try to enjoy yourself.\"</i>");
			comments.put(CommentSituation.MOUNT_DOM_LOSE,
					"<i>\"You haven't beaten me yet. Now let me show you just how good it can feel to be on the receiving end.\"</i>");
			comments.put(CommentSituation.MOUNT_SUB_WIN,
					"<i>\"I know you want to have sex with me. Go ahead, you have my permission. You can even come inside me if you want.\"</i>");
			comments.put(CommentSituation.MOUNT_SUB_LOSE,
					"<i>\"So this is how you want to finish the match, is it? I can't say it's a bad way to end it, but don't think I'm giving up just yet.\"</i>");
			comments.put(CommentSituation.SIXTYNINE_WIN,
					"<i>\"I really do want to taste your seed. You wouldn't mind obliging me, would you?\"</i>");
			comments.put(CommentSituation.SIXTYNINE_LOSE,
					"<i>\"Oh wow... how did you ever get so good with your tongue?\"</i>");
			comments.put(CommentSituation.BEHIND_DOM_WIN,
					"<i>\"Just relax now, and let me take care of the rest.\"</i>");
			comments.put(CommentSituation.BEHIND_DOM_LOSE,
					"<i>\"Alright, you've had your fun. Now it's my turn, if you don't mind.\"</i>");
			comments.put(CommentSituation.BEHIND_SUB_WIN,
					"<i>\"I hope you don't have anything nefarious in mind. Be warned that after I win, I might just have to punish you for anything you do now.\"</i>");
			comments.put(CommentSituation.BEHIND_SUB_LOSE, "<i>\"Wait, what are you planning to do?\"</i>");
			comments.put(CommentSituation.PIN_DOM_WIN, "<i>\"You're all mine, now.\"</i>");
			comments.put(CommentSituation.PIN_DOM_LOSE,
					"<i>\"You didn't think it would be that easy, did you? Let's see if we can make things a little harder for you... pun intended.\"</i>");
			comments.put(CommentSituation.PIN_SUB_WIN,
					"<i>\"I knew you still had some fight left in you. If you think you can still win, I'm curious to see what you can do.\"</i>");
			comments.put(CommentSituation.PIN_SUB_LOSE, "<i>\"Don't expect me to give up, even now.\"</i>");
			comments.put(CommentSituation.SELF_CHARMED,
					"<i>\"Did anyone ever tell you how lovely your eyes are? I just can't stop gazing at them...\"</i>");
			comments.put(CommentSituation.SELF_BOUND, "<i>\"Hmm. This is hardly a sporting tactic...\"</i>");
			comments.put(CommentSituation.OTHER_BOUND,
					"<i>\"My apologies for this, but I'm going to need to keep you from resisting for a bit.\"</i>");
			comments.put(CommentSituation.OTHER_STUNNED,
					"<i>\"You can take a moment to recover. I'll just occupy myself with your body while you do so.\"</i>");
			comments.put(CommentSituation.SELF_HORNY,
					"<i>\"If you don't mind, could we perhaps hurry this match along? I find myself rather eager to get to the fun part.\"</i>");
			comments.put(CommentSituation.OTHER_HORNY,
					"<i>\"I see you're having trouble resisting me. If you want to stop resisting, I have absolutely no complaints about that.\"</i>");
			comments.put(CommentSituation.SELF_OILED,
					"<i>\"Why did you do this...? Please don't tell me you're planning to... to...\"</i>");
			comments.put(CommentSituation.OTHER_OILED,
					"<i>\"Mm, there we go. And I must say, you look quite lovely with your skin glistening like this.\"</i>");
			comments.put(CommentSituation.SELF_SHAMED, "<i>\"Ah... no... no... please...\"</i>");
		}
		else {
			comments.put(CommentSituation.VAG_DOM_CATCH_WIN,
					"<i>\"Now, let me show you what this pussy of mine can do.\"</i>");
			comments.put(CommentSituation.VAG_DOM_CATCH_LOSE,
					"<i>\"Damn it... this just feels too good to stop...\"</i>");
			comments.put(CommentSituation.VAG_SUB_CATCH_WIN,
					"<i>\"You like this, don't you? You like feeling your cock deep in my pussy, pounding me mercilessly...\"</i>");
			comments.put(CommentSituation.VAG_SUB_CATCH_LOSE,
					"<i>\"Ahhh... no... stop! Stop! You're making me cum, fuck, stop!\"</i>");
			comments.put(CommentSituation.ANAL_PITCH_WIN,
					"<i>\"Your turn, asshole! Or I should say, my turn in your asshole! Okay, I know it's a horrible pun, but that shouldn't be what you're complaining about right now!\"</i>");
			comments.put(CommentSituation.ANAL_PITCH_LOSE,
					"<i>\"Fuck it, I don't care if this makes me cum! I'm going to fuck your ass until I can't stand up any longer.\"</i>");
			comments.put(CommentSituation.ANAL_CATCH_WIN,
					"<i>\"Yes! That's it! Fuck my ass just like that! I want to feel your seed fill me up, so cum in me!\"</i>");
			comments.put(CommentSituation.ANAL_CATCH_LOSE,
					"<i>\"No no no... fuck... my ass... no... fuck... fuck my ass... yes... I mean... fuck... fuck... just like that... make me cum from my ass... I'm almost there...\"</i>");
			comments.put(CommentSituation.MOUNT_DOM_WIN,
					"<i>\"Alright, asshole. Do you think you've earned the privilege to cum in my cunt, or should I just make you cum on the ground?\"</i>");
			comments.put(CommentSituation.MOUNT_DOM_LOSE,
					"<i>\"Okay, we can do this the hard way, or the hard way. I mean... just be hard for me, okay?\"</i>");
			comments.put(CommentSituation.MOUNT_SUB_WIN,
					"<i>\"Come on, fuck my pussy already. I know you want to.\"</i>");
			comments.put(CommentSituation.MOUNT_SUB_LOSE, "<i>\"Just fucking put it in me already!\"</i>");
			comments.put(CommentSituation.SIXTYNINE_WIN,
					"<i>\"I can't believe how fucking good your cock tastes... now let me taste your sperm too.\"</i>");
			comments.put(CommentSituation.SIXTYNINE_LOSE,
					" <i>\"Fuck your fucking tongue... that thing should be illegal...\"</i>");
			comments.put(CommentSituation.BEHIND_DOM_WIN,
					" <i>\"Alright. No more mister nice girl. ...I mean missus nice guy. I mean... fuck!\"</i>");
			comments.put(CommentSituation.BEHIND_DOM_LOSE,
					"<i>\"Alright, you've had your fun. Now it's my turn, if you don't mind.\"</i>");
			comments.put(CommentSituation.BEHIND_SUB_WIN,
					" <i>\"Pfft! Like you can really turn things around at this point, even if you were to... to... The point is you've lost!\"</i>");
			comments.put(CommentSituation.BEHIND_SUB_LOSE,
					" <i>\"Ahh... Are you really going to take me from this position?\"</i>");
			comments.put(CommentSituation.PIN_DOM_WIN,
					"<i>\"Oh, you have no idea how much fun I'm going to have now that I have you at my mercy like this.\"</i>");
			comments.put(CommentSituation.PIN_DOM_LOSE, "<i>\"Fucking finally... now it's my turn.\"</i>");
			comments.put(CommentSituation.PIN_SUB_WIN,
					" <i>\"Don't fucking think you can still manage to win this! Your ass is mine and we both know it!\"</i>");
			comments.put(CommentSituation.PIN_SUB_LOSE,
					"<i>\"...damn it, why am I so fucking turned on by this...?\"</i>");
			comments.put(CommentSituation.SELF_CHARMED, "<i>\"How the hell are you so damn sexy?\"</i>");
			comments.put(CommentSituation.SELF_BOUND, " <i>\"Oh come on! This is a dirty trick and you know it!\"</i>");
			comments.put(CommentSituation.OTHER_BOUND,
					"<i>\"There we go. Now it's time to have some fun with you.\"</i>");
			comments.put(CommentSituation.OTHER_STUNNED,
					"<i>\"You didn't think I'd let you go unpunished, did you?\"</i>");
			comments.put(CommentSituation.SELF_HORNY,
					"<i>\"What the fuck are you waiting for? Fuck me already before I fuck you!\"</i>");
			comments.put(CommentSituation.OTHER_HORNY,
					" <i>\"Don't give me that face. I was already planning to make you cum.\"</i>");
			comments.put(CommentSituation.SELF_OILED,
					"<i>\"Wait... what are you planning? You'd better fucking leave my ass alone!\"</i>");
			comments.put(CommentSituation.OTHER_OILED, "<i>\"Oh damn, you're one hot boy like this...\"</i>");
			comments.put(CommentSituation.SELF_SHAMED,
					"<i>\"Fine, I'm a fucking slut who wants you to fuck her brains out, are you happy?\"</i>");
		}
		return comments;
	}

	@Override
	public CommentGroup getResponses() {
		return new CommentGroup();
	}

	@Override
	public int getCostumeSet() {
		return 1;
	}

	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if (character.getGrudge() == Trait.icequeen
				|| character.getGrudge() == Trait.highcomposure
				|| character.getGrudge() == Trait.potentcomposure) {
			character.addGrudge(opponent, Trait.enraged);
		}
		else {
			switch (Global.random(3)) {
				case 0:
					character.addGrudge(opponent, Trait.icequeen);
					break;
				case 1:
					character.addGrudge(opponent, Trait.highcomposure);
					break;
				default:
					character.addGrudge(opponent, Trait.potentcomposure);
					break;
			}
		}
	}

	@Override
	public void resetOutfit() {
		character.outfit[Character.OUTFITTOP].clear();
		character.outfit[Character.OUTFITBOTTOM].clear();

		character.outfit[Character.OUTFITTOP].add(Clothing.bra);
		character.outfit[Character.OUTFITTOP].add(Clothing.noblevest);
		character.outfit[Character.OUTFITTOP].add(Clothing.noblecloak);
		character.outfit[Character.OUTFITBOTTOM].add(Clothing.panties);
		character.outfit[Character.OUTFITBOTTOM].add(Clothing.tdresspants);
	}
}
