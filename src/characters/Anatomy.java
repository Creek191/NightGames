package characters;

import combat.Tag;

/**
 * Defines different parts of anatomy that can be targeted or used
 */
public enum Anatomy implements Tag {
	mouth,
	fingers,
	chest,
	genitals,
	ass,
	feet,
	arm,
	leg,
	head,
	neck,
	toy,
	soul,
}
