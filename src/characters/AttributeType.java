package characters;

/**
 * Defines the types of attributes
 */
public enum AttributeType {
	Basic,
	Passive,
	Advanced,
	Specialization
}
