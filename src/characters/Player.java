package characters;

import actions.Leap;
import actions.Move;
import actions.Shortcut;
import areas.Area;
import combat.Combat;
import combat.Encounter;
import combat.Tag;
import global.*;
import gui.GUI;
import items.*;
import skills.Skill;
import skills.Tactics;
import stance.Behind;
import status.Enthralled;
import status.Feral;
import status.Horny;
import status.Stsflag;
import trap.Trap;

import java.util.*;

public class Player extends Character {
	/**
	 *
	 */
	private static final long serialVersionUID = -7547460660118646782L;
	public GUI gui;
	public int attpoints;
	public HashSet<Area> pings;

	public Player(String name) {
		super(name, 1);
		identity = ID.PLAYER;

		// Set clothing
		resetOutfit();
		closet.add(Clothing.Tshirt);
		closet.add(Clothing.boxers);
		closet.add(Clothing.jeans);
		change(Modifier.normal);
		setUnderwear(Trophy.PlayerTrophy);

		// Set base stats
		att.put(Attribute.Power, Math.round(Constants.STARTINGPOWER * Global.getValue(Flag.PlayerBaseStrength)));
		att.put(Attribute.Cunning, Math.round(Constants.STARTINGCUNNING * Global.getValue(Flag.PlayerBaseStrength)));
		att.put(Attribute.Seduction,
				Math.round(Constants.STARTINGSEDUCTION * Global.getValue(Flag.PlayerBaseStrength)));
		att.put(Attribute.Perception, Constants.STARTINGPERCEPTION);
		att.put(Attribute.Speed, Math.round(Constants.STARTINGSPEED * Global.getValue(Flag.PlayerBaseStrength)));
		this.money = 0;
		getStamina().setMax(Math.round(Constants.STARTINGSTAMINA * Global.getValue(Flag.PlayerBaseStrength)));
		getStamina().fill();
		getArousal().setMax(Math.round(Constants.STARTINGAROUSAL * Global.getValue(Flag.PlayerBaseStrength)));
		getMojo().setMax(Math.round(Constants.STARTINGMOJO * Global.getValue(Flag.PlayerBaseStrength)));

		// Set starting traits
		add(Trait.male);

		attpoints = 0;
		pings = new HashSet<>();
	}

	@Override
	public String describe(int perception) {
		var description = new StringBuilder();
		description.append("<i>");
		for (var s : status) {
			if (s.describe() != null && !s.describe().equals("")) {
				description.append(s.describe())
						.append("<br>");
			}
		}
		description.append("</i>");

		if (top.empty() && bottom.empty()) {
			description.append("You are completely naked.");
		}
		else {
			if (top.empty()) {
				description.append("You are shirtless and wearing ");
			}
			else {
				description.append("You are wearing a ")
						.append(top.peek().getName())
						.append(" and ");
			}
			if (bottom.empty()) {
				description.append("are naked from the waist down.");
			}
			else {
				description.append(bottom.peek().getName())
						.append(".");
			}
		}

		for (var p : pools.keySet()) {
			if (p != Pool.STAMINA && p != Pool.AROUSAL && p != Pool.MOJO) {
				if (pools.get(p).get() > 0) {
					description.append("<br>")
							.append(p.getLabel())
							.append(": ")
							.append(pools.get(p).get());
				}
			}
		}
		return description.toString();
	}

	@Override
	public void victory(Combat c, Tag flag) {
		var target = c.getOther(this);
		target.defeat(c, flag);
		this.gainXP(15 + lvlBonus(target));
		target.gainXP(15 + target.lvlBonus(this));
		if (c.stance.penetration(this)) {
			getMojo().gainMax(has(Trait.mojoMaster) ? 4 : 2);
			Global.modCounter(Flag.IntercourseWins, 1);
		}

		if (!has(Trait.nosatisfaction)) {
			getArousal().empty();
		}

		if (!c.hasModifier(Modifier.practice)) {
			undress(c);
			target.undress(c);
			if (c.underwearIntact(target)) {
				this.gain(target.getUnderwear());
			}
			dress(c);
			target.defeated(this);
			Roster.gainAttraction(this.id(), target.id(), target.has(Trait.gracefulloser) ? 4 : 2);

			if (has(Trait.insatiable)) {
				getArousal().set(Math.round(getArousal().max() * .5f));
			}
		}
	}

	@Override
	public void defeat(Combat c, Tag flag) {
		c.write("Bad thing");
	}

	@Override
	public void act(Combat c) {
		gui.clearCommand();
		Character target;
		if (c.p1 == this) {
			target = c.p2;
		}
		else {
			target = c.p1;
		}

		var skillArray = new ArrayList<Skill>();
		var demandsArray = new ArrayList<Skill>();
		var flaskArray = new ArrayList<Skill>();
		var potionArray = new ArrayList<Skill>();

		for (var a : skills) {
			if (a.usable(c, target) && c.isAllowed(a, this)) {
				if (a.type() == Tactics.demand) {
					demandsArray.add(a);
				}
				else {
					skillArray.add(a);
				}
			}
		}

		for (var s : flaskskills) {
			if (s.usable(c, target) && c.isAllowed(s, this)) {
				flaskArray.add(s);
			}
		}
		for (var s : potionskills) {
			if (s.usable(c, target) && c.isAllowed(s, this)) {
				potionArray.add(s);
			}
		}

		// sort them.
		Collections.sort(skillArray);
		Collections.sort(demandsArray);
		Collections.sort(flaskArray);
		Collections.sort(potionArray);

		// add them to the gui.
		for (var s : skillArray) {
			gui.addSkill(c, s);
		}
		for (var s : demandsArray) {
			gui.addDemands(s, c);
		}
		for (var s : flaskArray) {
			gui.addFlasks(s, c);
		}
		for (var s : potionArray) {
			gui.addPotions(s, c);
		}

		gui.showSkills();
	}

	@Override
	public boolean human() {
		return true;
	}

	@Override
	public void draw(Combat c, Tag flag) {
		getArousal().empty();
		if (c.p1.human()) {
			c.p2.draw(c, flag);
		}
		else {
			c.p1.draw(c, flag);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void change(Modifier rule) {
		if (rule == Modifier.nudist) {
			top.clear();
			bottom.clear();
		}
		else if (rule == Modifier.pantsman) {
			top.clear();
			bottom.clear();
			for (var article : outfit[Player.OUTFITBOTTOM]) {
				if (article.getType() == ClothingType.UNDERWEAR) {
					wear(article);
				}
			}
			for (var article : outfit[Player.OUTFITTOP]) {
				if (article.getType() == ClothingType.TOPUNDER) {
					wear(article);
				}
			}
		}
		else {
			top = (Stack<Clothing>) outfit[Player.OUTFITTOP].clone();
			bottom = (Stack<Clothing>) outfit[Player.OUTFITBOTTOM].clone();
		}
	}

	@Override
	public String bbLiner() {
		return null;
	}

	@Override
	public String nakedLiner() {
		return null;
	}

	@Override
	public String stunLiner() {
		return null;
	}

	@Override
	public String taunt() {
		return null;
	}

	@Override
	public void detect() {
		pings.clear();
		for (var distant : Scheduler.getMatch().getAreas()) {
			if (distant != location
					&& !location.adjacent.contains(distant)
					&& distant.alarmTriggered()) {
				Global.gui().message("You can faintly hear a far-away noise from the <b>" + distant.name + "</b>.");
				pings.add(distant);
			}
		}
		for (var adjacent : location.adjacent) {
			if (adjacent.ping(this)) {
				Global.gui().message("You hear something in the <b>" + adjacent.name + "</b>.");
				pings.add(adjacent);
			}
		}
	}

	/**
	 * Attempt to read traces in the current location
	 */
	public void track() {
		for (var trace : location.getTraces()) {
			if (trace.getPerson() != this) {
				trace.track(getTrackingScore());
			}
		}
	}

	/**
	 * Checks whether an opponent was detected in the specified location
	 *
	 * @param location The area to check
	 */
	public boolean opponentDetected(Area location) {
		return pings.contains(location);
	}

	@Override
	public void faceOff(Character opponent, Encounter enc) {
		gui.message("You run into <b>" + opponent.name + "</b> and you both hesitate for a moment, deciding whether to attack or retreat.");
		assessOpponent(opponent);
		for (var nearby : location.present) {
			if (nearby != this && nearby != opponent) {
				gui.message("<b>" + nearby.name + "</b> is watching nearby and may get involved.");
			}
		}
		gui.promptFF(enc, opponent);
	}

	@Override
	public void spy(Character opponent, Encounter enc) {
		gui.message("You spot <b>" + opponent.name + "</b> but she hasn't seen you yet. You could probably catch her off guard, or you could slip away before she notices you.");
		assessOpponent(opponent);
		for (var nearby : location.present) {
			if (nearby != this && nearby != opponent) {
				gui.message("<b>" + nearby.name + "</b> is watching nearby and may get involved.");
			}
		}
		gui.promptAmbush(enc, opponent);
		for (var path : location.adjacent) {
			gui.addAction(new Move(path), this);
		}
		if (getPure(Attribute.Cunning) >= 28) {
			for (var path : location.shortcut) {
				gui.addAction(new Shortcut(path), this);
			}
		}
		if (getPure(Attribute.Ninjutsu) >= 5) {
			for (var path : location.jump) {
				gui.addAction(new Leap(path), this);
			}
		}
	}

	@Override
	public void move(Match match) {
		gui.clearCommand();
		if (state == State.combat) {
			if (!location.fight.battle()) {
				Scheduler.unpause();
			}
		}
		else if (busy > 0) {
			busy--;
		}
		else if (this.is(Stsflag.enthralled)) {
			var master = ((Enthralled) getStatus(Stsflag.enthralled)).master;
			if (master != null) {
				var compelled = findPath(master.location());
				if (compelled != null) {
					gui.message("You feel an irresistible compulsion to head to the <b>" + master.location().name + "</b>");
					this.gui.addAction(compelled, this);
				}
			}
		}
		else if (state == State.shower || state == State.lostclothes) {
			bathe();
		}
		else if (state == State.crafting) {
			craft();
		}
		else if (state == State.searching) {
			search();
		}
		else if (state == State.resupplying) {
			resupply();
		}
		else if (state == State.webbed) {
			gui.message(
					"You eventually manage to get an arm free, which you then use to extract yourself from the trap.");
			state = State.ready;
		}
		else if (state == State.masturbating) {
			masturbate();
		}
		else {
			// Allow player to perform an action
			gui.showMap();
			gui.messageHead(location.description + "<p>");
			for (var trap : location.env) {
				if (trap.owner() == this) {
					gui.message("You've set a " + trap + " here.");
				}
			}
			detect();
			if (has(Toy.bloodhound)) {
				var targets = new ArrayList<Character>(Scheduler.getMatch().combatants);
				targets.remove(this);
				var target = targets.get(Global.random(targets.size()));
				gui.message("You Bloodhound app tells you that " + target.name()
						+ " is currently in " + target.location().name);
			}
			track();
			if (location.encounter(this)) {
				return;
			}

			// Show possible player actions
			for (var path : location.adjacent) {
				gui.addAction(new Move(path), this);
			}
			if (getPure(Attribute.Cunning) >= 28) {
				for (var path : location.shortcut) {
					gui.addAction(new Shortcut(path), this);
				}
			}
			if (getPure(Attribute.Ninjutsu) >= 5) {
				for (var path : location.jump) {
					gui.addAction(new Leap(path), this);
				}
			}
			for (var act : Global.getActions()) {
				if (act.usable(this)) {
					gui.addAction(act, this);
				}
			}
		}
	}

	@Override
	public void ding() {
		xp -= 95 + (level * 5);
		level++;
		attpoints += 2;
		gui.clearText();
		gui.message("You've gained a Level!");
		for (var att : att.keySet()) {
			if (att.isSpecialization()) {
				mod(att, 1);
				gui.message("You've gained a point in " + att);
			}
		}
		gui.message("Select which attributes to increase.");
		gui.message("Upcoming Skills:");
		for (var att : this.att.keySet()) {
			if (att != Attribute.Perception
					&& att != Attribute.Speed
					&& !att.isSpecialization()) {
				gui.message(Global.getUpcomingSkills(att, getPure(att)));
			}
		}
		gui.ding();
	}

	@Override
	public int income(int amount) {
		var total = Math.round(amount * Global.getValue(Flag.PlayerScaling));
		return super.income(total);
	}

	@Override
	public void flee() {
		var adjacent = location.adjacent.toArray(new Area[0]);
		var destination = adjacent[Global.random(adjacent.length)];
		gui.message("You dash away and escape into the <b>" + destination.name + "</b>");
		travel(destination);
		location.endEncounter();
	}

	@Override
	public void bathe() {
		var copy = new ArrayList<>(status);
		for (var s : copy) {
			if (s.removable()) {
				status.remove(s);
			}
		}
		
		if (Scheduler.getMatch() != null) {
			applyMatchConditionStatus(Scheduler.getMatch());
		}

		getStamina().fill();
		if (location.name.equalsIgnoreCase("Showers")) {
			gui.message("You let the hot water wash away your exhaustion and soon you're back to peak condition");
		}
		if (location.name.equalsIgnoreCase("Pool")) {
			gui.message(
					"The hot water soothes and relaxes your muscles. You feel a bit exposed, skinny-dipping in such an open area. You decide it's time to get moving.");
		}
		if (state == State.lostclothes) {
			gui.message("Your clothes aren't where you left them. Someone must have come by and taken them.");
		}
		state = State.ready;
	}

	@Override
	public void masturbate() {
		if (getPure(Attribute.Dark) >= 9) {
			gui.message(
					"You hurriedly stroke yourself off, eager to finish before someone catches you. Ever pragmatic, you carefully aim your ejaculation into a small container, "
							+ "which you store in your pocket. You feel much more refreshed and now you have some prime fuel for your dark powers.");
			gain(Component.Semen);
		}
		else {
			gui.message(
					"You hurriedly stroke yourself off, eager to finish before someone catches you. After what seems like an eternity, you ejaculate into a tissue and "
							+ "throw it in the trash. Looks like you got away with it.");
		}

		getArousal().empty();
		state = State.ready;
	}

	@Override
	public void showerScene(Character target, Encounter encounter) {
		if (target.location().name.equalsIgnoreCase("Showers")) {
			gui.message(
					"You hear running water coming from the first floor showers. There shouldn't be any residents on this floor right now, so it's likely one "
							+ "of your opponents. You peek inside and sure enough, <b>" + target.name() + "</b> is taking a shower and looking quite vulnerable. Do you take advantage "
							+ "of her carelessness?");
		}
		else if (target.location().name.equalsIgnoreCase("Pool")) {
			gui.message("You stumble upon <b>" + target.name + "</b> skinny dipping in the pool. She hasn't noticed you yet. It would be pretty easy to catch her off-guard.");
		}
		assessOpponent(target);
		gui.promptShower(encounter, target);
	}

	@Override
	public void intervene(Encounter enc, Character p1, Character p2) {
		gui.message("You find <b>" + p1.name() + "</b> and <b>" + p2.name()
				+ "</b> fighting too intensely to notice your arrival. If you intervene now, it'll essentially decide the winner.");
		gui.promptIntervene(enc, p1, p2);
	}

	@Override
	public boolean resist3p(Combat combat, Character intruder, Character assist) {
		return has(Trait.cursed);
	}

	@Override
	public void intervene3p(Combat c, Character target, Character assist) {
		this.gainXP(20 + lvlBonus(target));
		target.defeated(this);
		assist.gainAttraction(this, 1);
		c.write("You take your time, approaching " + target.name() + " and " + assist.name()
				+ " stealthily. " + assist.name() + " notices you first and before her reaction "
				+ "gives you away, you quickly lunge and grab " + target.name()
				+ " from behind. She freezes in surprise for just a second, but that's all you need to "
				+ "restrain her arms and leave her completely helpless. Both your hands are occupied holding her, so you focus on kissing and licking the "
				+ "sensitive nape of her neck.<p>");
	}

	@Override
	public void victory3p(Combat c, Character target, Character assist) {
		this.gainXP(20 + lvlBonus(target));
		target.gainXP(10 + target.lvlBonus(this) + target.lvlBonus(assist));
		target.getArousal().empty();
		if (target.has(Trait.insatiable)) {
			target.getArousal().restore((int) (getArousal().max() * .2));
		}
		dress(c);
		target.undress(c);
		if (c.underwearIntact(target)) {
			this.gain(target.getUnderwear());
		}
		target.defeated(this);
		if (target.hasDick()) {
			c.write("You position yourself between " + target.name() + "'s legs, gently forcing them open with your knees. " + target.possessive(
					true) + " dick stands erect, fully exposed and "
					+ "ready for attention. You grip the needy member and start jerking it with a practiced hand. " + target.name() + " moans softly, but seems to be able to handle "
					+ "this level of stimulation. You need to turn up the heat some more. Well, if you weren't prepared to suck a cock or two, you may have joined "
					+ "the wrong competition. You take just the glans into your mouth, attacking the most sensitive area with your tongue. "
					+ target.pronounSubject(true) + " lets out a gasp and "
					+ "shudders. That's a more promising reaction.<p>"
					+ "You continue your oral assault until you hear a breathy moan, <i>\"I'm gonna cum!\"</i> You hastily remove "
					+ target.possessive(false) + " dick out of your mouth and "
					+ "pump it rapidly. " + target.name() + " shoots " + target.possessive(false) + " load into the air, barely missing you.");
		}
		else {
			c.write(target.name() + "'s arms are firmly pinned, so she tries to kick you ineffectually. You catch her ankles and slowly begin kissing and licking your way "
					+ "up her legs while gently, but firmly, forcing them apart. By the time you reach her inner thighs, she's given up trying to resist. Since you no "
					+ "longer need to hold her legs, you can focus on her flooded pussy. You pump two fingers in and out of her while licking and sucking her clit. In no "
					+ "time at all, she's trembling and moaning in orgasm.");
		}
		gainAttraction(target, 1);
		target.gainAttraction(this, 1);
	}

	@Override
	public void watcher(Combat c, Character victor, Character defeated) {
		if (has(Trait.voyeurism)) {
			buildMojo(50);
		}
		Global.gui().clearText();
		victor.watched(c, this, defeated);
	}

	@Override
	public void watched(Combat c, Character voyeur, Character defeated) {
	}

	@Override
	public void gain(Item item) {
		gui.message("<b>You've gained " + item.pre() + item.getName() + "</b>");
		super.gain(item);
	}

	@Override
	public void gain(Clothing article) {
		gui.message("<b>You've gained " + article.pre() + article.getName() + "</b>");
		super.gain(article);
	}

	@Override
	public void load(Scanner loader) {
		name = loader.next();
		level = Integer.parseInt(loader.next());
		setRank(Integer.parseInt(loader.next()));
		xp = Integer.parseInt(loader.next());
		money = Integer.parseInt(loader.next());
		var e = loader.next();

		// Attributes
		while (!e.equals("@")) {
			set(Attribute.valueOf(e), Integer.parseInt(loader.next()));
			e = loader.next();
		}
		getStamina().setMax(Integer.parseInt(loader.next()));
		getArousal().setMax(Integer.parseInt(loader.next()));
		getMojo().setMax(Integer.parseInt(loader.next()));

		// Affections
		e = loader.next();
		while (!e.equals("*")) {
			gainAffection(Roster.get(e), Integer.parseInt(loader.next()));
			e = loader.next();
		}

		// Attractions
		e = loader.next();
		while (!e.equals("*")) {
			gainAttraction(Roster.get(e), Integer.parseInt(loader.next()));
			e = loader.next();
		}

		// Clothes (TOP)
		e = loader.next();
		outfit[Player.OUTFITTOP].clear();
		while (!e.equals("!")) {
			outfit[Player.OUTFITTOP].add(Clothing.valueOf(e));
			e = loader.next();
		}

		// Clothes (BOTTOM)
		e = loader.next();
		outfit[Player.OUTFITBOTTOM].clear();
		while (!e.equals("!") && !e.equals("!!")) {
			outfit[Player.OUTFITBOTTOM].add(Clothing.valueOf(e));
			e = loader.next();
		}

		// Clothes (CLOSET)
		if (e.equals("!!")) {
			e = loader.next();
			closet.clear();
			while (!e.equals("!!!")) {
				closet.add(Clothing.valueOf(e));
				e = loader.next();
			}
		}

		// Traits
		e = loader.next();
		while (!e.equals("@")) {
			traits.add(Trait.valueOf(e));
			e = loader.next();
		}

		// Inventory
		e = loader.next();
		while (!e.equals("$")) {
			super.gain(Global.getItem(e));
			e = loader.next();
		}
		change(Modifier.normal);
		resetSkills();
		Global.gainSkills(this);
		getStamina().fill();
		getArousal().empty();
		getMojo().empty();
	}

	@Override
	public String challenge(Character opponent) {
		return null;
	}

	@Override
	public void promptTrap(Encounter enc, Character target, Trap trap) {
		Global.gui().message("Do you want to take the opportunity to ambush <b>" + target.name() + "</b>?");
		assessOpponent(target);
		gui.promptOpportunity(enc, target, trap);
	}

	/**
	 * Gets a list of all items in the inventory and their quantities
	 */
	public HashMap<Item, Integer> listInventory() {
		return new HashMap<>(getInventory());
	}

	@Override
	public void afterParty() {
	}

	@Override
	public void counterattack(Character target, Tactics type, Combat c) {
		switch (type) {
			case damage:
				c.write("You dodge " + target.name() + "'s slow attack and hit her sensitive tit to stagger her.");
				target.pain(4 + Global.random(getEffective(Attribute.Cunning)), Anatomy.chest, c);
				break;
			case pleasure:
				if (!target.nude()) {
					c.write("You pull " + target.name() + " off balance and lick her sensitive ear. She trembles as you nibble on her earlobe.");
				}
				else {
					c.write("You pull " + target.name() + " to you and rub your thigh against her girl parts.");
				}
				target.pleasure(4 + Global.random(getEffective(Attribute.Cunning)), Anatomy.genitals, c);
				break;
			case positioning:
				c.write(target.name() + " loses her balance while grappling with you. Before she can fall to the floor, you catch her from behind and hold her up.");
				c.stance = new Behind(this, target);
				break;
			default:
		}
	}

	@Override
	public void endOfTurn(Combat c, Character opponent) {
		for (var s : getStatus()) {
			s.turn(c);
		}

		// Attempt to capture opponent's pet
		if (opponent.pet != null
				&& canAct()
				&& c.stance.mobile(this)
				&& !c.stance.prone(this)) {
			if (getEffective(Attribute.Speed) > opponent.pet.ac() * Global.random(20)) {
				opponent.pet.caught(c, this);
			}
		}

		// Check if opponent's pheromones turn us on
		if (opponent.has(Trait.pheromones)
				&& opponent.getArousal().percent() >= 50
				&& !is(Stsflag.horny)
				&& Global.random(5) == 0) {
			c.write("Whenever you're near " + opponent.name() + ", you feel your body heat up. Something in her scent is making you extremely horny.");
			add(new Horny(this, 2 + 2 * opponent.getSkimpiness(), 3), c);
		}

		// Check if opponent's tail can pleasure us
		if (opponent.has(Trait.tailmastery)
				&& opponent.has(Trait.tailed)
				&& !opponent.distracted()
				&& !opponent.stunned()
				&& pantsless()) {
			c.write(opponent.name() + " opportunistically teases you with her soft tail.");
			pleasure(Global.random(5), Anatomy.genitals, c);
		}

		if (has(Trait.RawSexuality)) {
			tempt(1);
			opponent.tempt(1);
		}

		// Get feral status if feral or aroused
		if (has(Trait.feral)
				|| (getPure(Attribute.Animism) >= 4 && getArousal().percent() >= 50)
				|| (has(Trait.furaffinity) && getArousal().percent() >= 25)) {
			if (!is(Stsflag.feral)) {
				add(new Feral(this));
			}
		}
	}

	@Override
	public String getPortrait() {
		return null;
	}

	@Override
	public Emotion moodSwing() {
		var current = mood;
		int max = emotes.get(current);
		for (var e : emotes.keySet()) {
			if (emotes.get(e) > max) {
				mood = e;
				max = emotes.get(e);
			}
		}
		return mood;
	}

	@Override
	public void resetOutfit() {
		outfit[Player.OUTFITTOP].clear();
		outfit[Player.OUTFITBOTTOM].clear();

		outfit[Player.OUTFITTOP].add(Clothing.Tshirt);
		outfit[Player.OUTFITBOTTOM].add(Clothing.boxers);
		outfit[Player.OUTFITBOTTOM].add(Clothing.jeans);
	}

	/**
	 * Assesses the specified opponent
	 *
	 * @param opponent The character to assess
	 */
	private void assessOpponent(Character opponent) {
		String arousal;
		String stamina;
		if (opponent.state == State.webbed) {
			gui.message("She is naked and helpless<br>");
			return;
		}
		if (getEffective(Attribute.Perception) >= 6) {
			gui.message("She is level " + opponent.getLevel());
		}
		if (getEffective(Attribute.Perception) >= 8) {
			gui.message("Her Power is " + opponent.getEffective(Attribute.Power)
					+ ", her Cunning is " + opponent.getEffective(Attribute.Cunning)
					+ ", and her Seduction is " + opponent.getEffective(Attribute.Seduction));
		}
		if (opponent.nude() || opponent.state == State.shower) {
			gui.message("She is completely naked.");
		}
		else {
			gui.message("She is dressed and ready to fight.");
		}
		if (getEffective(Attribute.Perception) >= 4) {
			if (opponent.getArousal().percent() > 70) {
				arousal = "horny";
			}
			else if (opponent.getArousal().percent() > 30) {
				arousal = "slightly aroused";
			}
			else {
				arousal = "composed";
			}
			if (opponent.getStamina().percent() < 50) {
				stamina = "tired";
			}
			else {
				stamina = "eager";
			}
			gui.message("She looks " + stamina + " and " + arousal + ".");
		}
	}
}
