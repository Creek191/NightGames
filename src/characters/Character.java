package characters;

import actions.Move;
import actions.Movement;
import areas.Area;
import areas.NinjaStash;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import combat.*;
import global.*;
import items.*;
import pet.Pet;
import skills.*;
import stance.Position;
import status.*;
import trap.Trap;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;

public abstract class Character extends Observable implements Cloneable {
	/**
	 *
	 */
	protected String name;
	protected ID identity;
	protected int level;
	protected int xp;
	protected int rank;
	public int money;
	public HashMap<Attribute, Integer> att;
	protected Meter stamina;
	protected Meter arousal;
	protected Meter mojo;
	protected HashMap<Pool, Meter> pools;
	public Stack<Clothing> top;
	public Stack<Clothing> bottom;
	protected Area location;
	public Stack<Clothing>[] outfit;
	protected HashSet<Skill> skills;
	protected HashSet<Skill> flaskskills;
	protected HashSet<Skill> potionskills;
	protected HashSet<Status> status;
	public HashSet<Trait> traits;
	private HashSet<Character> mercy;
	protected HashMap<Item, Integer> inventory;
	protected Item underwear;
	public State state;
	protected int busy;
	public HashMap<Emotion, Integer> emotes;
	public Emotion mood;
	public HashSet<Clothing> closet;
	public Pet pet;
	public ArrayList<Challenge> challenges;
	protected HashMap<Character, Trait> grudges;
	protected Trait activeGrudge;
	protected Trait matchmod;
	protected Boolean benched;

	BufferedImage spriteBody = null;
	BufferedImage spriteAccessory = null;
	BufferedImage spriteAccessory2 = null;
	BufferedImage spriteUnderwear = null;
	BufferedImage spriteStrapon = null;
	BufferedImage spriteBra = null;
	BufferedImage spriteBottom = null;
	BufferedImage spriteTop = null;
	BufferedImage spriteOuter = null;
	BufferedImage spriteCoattail = null;
	BufferedImage spriteShirttail = null;
	BufferedImage spritePenisSoft = null;
	BufferedImage spritePenisHard = null;
	BufferedImage spriteKemono = null;
	BufferedImage blushLow = null;
	BufferedImage blushMed = null;
	BufferedImage blushHigh = null;

	public static final int OUTFITTOP = 0;
	public static final int OUTFITBOTTOM = 1;

	public Character(String name, int level) {
		this.name = name;
		this.level = level;
		att = new HashMap<>();
		pools = new HashMap<>();
		top = new Stack<>();
		bottom = new Stack<>();
		outfit = new Stack[2];
		outfit[OUTFITTOP] = new Stack<>();
		outfit[OUTFITBOTTOM] = new Stack<>();
		closet = new HashSet<>();
		skills = new HashSet<>();
		flaskskills = new HashSet<>();
		potionskills = new HashSet<>();
		status = new HashSet<>();
		traits = new HashSet<>();
		mercy = new HashSet<>();
		inventory = new HashMap<>();
		challenges = new ArrayList<>();
		grudges = new HashMap<>();
		location = new Area("", "", null, null);
		state = State.ready;
		busy = 0;
		setRank(0);
		this.pet = null;
		this.activeGrudge = null;
		this.benched = false;
		emotes = new HashMap<>();
		mood = Emotion.confident;
		for (var emotion : Emotion.values()) {
			emotes.put(emotion, 0);
		}
		skills.add(new Struggle(this));
		skills.add(new Nothing(this));
		skills.add(new Recover(this));
		skills.add(new Straddle(this));
		skills.add(new ReverseStraddle(this));
		skills.add(new Stunned(this));
		skills.add(new Distracted(this));
		skills.add(new PullOut(this));
		for (var flask : Flask.values()) {
			flaskskills.add(new UseFlask(this, flask));
		}
		for (var potion : Potion.values()) {
			potionskills.add(new UsePotion(this, potion));
		}
	}

	/**
	 * Clone the character, creating a new instance with the same data
	 *
	 * @return The cloned character
	 */
	@SuppressWarnings("unchecked")
	public Character clone() throws CloneNotSupportedException {
		var character = (Character) super.clone();
		character.att = (HashMap<Attribute, Integer>) att.clone();
		character.top = (Stack<Clothing>) top.clone();
		character.bottom = (Stack<Clothing>) bottom.clone();
		character.outfit = outfit.clone();
		character.skills = (HashSet<Skill>) skills.clone();
		character.status = new HashSet<>();

		// Deep-clone pools
		var map = new HashMap<Pool, Meter>();
		for (var pool : this.pools.entrySet()) {
			if (map.put(pool.getKey(), pool.getValue().clone()) != null) {
				throw new IllegalStateException("Duplicate key");
			}
		}

		character.pools = map;

		for (var s : status) {
			character.status.add(s.copy(character));
		}
		character.traits = (HashSet<Trait>) traits.clone();
		character.mercy = (HashSet<Character>) mercy.clone();
		character.inventory = (HashMap<Item, Integer>) inventory.clone();
		character.emotes = (HashMap<Emotion, Integer>) emotes.clone();
		character.activeGrudge = activeGrudge;
		character.mood = mood;
		return character;
	}

	public String name() {
		return name;
	}

	public ID id() {
		return identity;
	}

	/**
	 * Gets the effective value of the specified attribute
	 * <p>
	 * Considers status effects and traits
	 *
	 * @param attribute The attribute to get the value for
	 */
	public int getEffective(Attribute attribute) {
		var total = 0;
		if (att.containsKey(attribute)) {
			total = att.get(attribute);
		}
		for (var s : status) {
			total += s.mod(attribute);
		}

		// Apply trait bonuses
		switch (attribute) {
			case Power:
				if (has(Trait.powerup)) {
					total += 10;
				}
				if (has(Trait.mighty)) {
					total += 2;
				}
				if (has(Trait.revvedup)) {
					if (getArousal().percent() > 25) {
						total += 5;
					}
					if (getArousal().percent() > 50) {
						total += 5;
					}
					if (getArousal().percent() > 75) {
						total += 5;
					}
				}
				break;
			case Cunning:
				if (has(Trait.inspired)) {
					total += 10;
				}
				break;
			case Seduction:
				if (has(Trait.confidentdom)
						&& (getMood() == Emotion.dominant
						|| getMood() == Emotion.confident)) {
					total *= 2;
				}
				if (has(Trait.seductress)) {
					total += 10;
				}
				if (has(Trait.revvedup)) {
					if (getArousal().percent() > 25) {
						total += 5;
					}
					if (getArousal().percent() > 50) {
						total += 5;
					}
					if (getArousal().percent() > 75) {
						total += 5;
					}
				}
				break;
			case Arcane:
				if (has(Trait.mystic)) {
					total += 2;
				}
				break;
			case Dark:
				if (has(Trait.broody)) {
					total += 2;
				}
				if (has(Trait.darkness)) {
					total += 10;
				}
				break;
			case Ki:
				if (has(Trait.martial)) {
					total += 2;
				}
				break;
			case Fetish:
				if (has(Trait.kinky)) {
					total += 2;
				}
				if (has(Trait.revvedup)) {
					if (getArousal().percent() > 25) {
						total += 5;
					}
					if (getArousal().percent() > 50) {
						total += 5;
					}
					if (getArousal().percent() > 75) {
						total += 5;
					}
				}
				break;
			case Science:
				if (has(Trait.geeky)) {
					total += 2;
				}
				break;
			case Ninjutsu:
				if (has(Trait.stealthy)) {
					total += 2;
				}
				break;
			case Animism:
				if (has(Trait.furry)) {
					total += 2;
				}
				break;
			case Speed:
				for (var article : top) {
					if (article.attribute() == Trait.bulky) {
						total--;
					}
				}
				for (var article : bottom) {
					if (article.attribute() == Trait.bulky) {
						total--;
					}
				}
				if (has(Trait.streaker) && nude()) {
					total += 4;
				}
				if (has(Trait.flash)) {
					total += 10;
				}
				break;
		}
		return Math.max(total, 0);
	}

	/**
	 * Gets the base value of the specified attribute
	 *
	 * @param attribute The attribute to get the value for
	 */
	public int getPure(Attribute attribute) {
		var total = 0;
		if (att.containsKey(attribute)) {
			total = att.get(attribute);
		}
		return total;
	}

	/**
	 * Gets the cost for the next training of an advanced attribute
	 */
	public int getAdvancedTrainingCost() {
		return 400 + (Constants.TRAININGSCALING * getAdvanced());
	}

	/**
	 * Checks whether the character has any specialization
	 */
	public boolean hasSpecialization() {
		return att.keySet().stream()
				.anyMatch(Attribute::isSpecialization);
	}

	/**
	 * Modifies the specified attribute by the given value
	 *
	 * @param attribute The attribute to modify
	 * @param value     The value to add
	 */
	public void mod(Attribute attribute, int value) {
		if (att.containsKey(attribute)) {
			att.put(attribute, att.get(attribute) + value);
		}
		else {
			set(attribute, value);
		}
	}

	/**
	 * Sets the specified attribute to the given value
	 *
	 * @param attribute The attribute to set
	 * @param value     The new value
	 */
	public void set(Attribute attribute, int value) {
		att.put(attribute, value);
	}

	/**
	 * Check if the characters attribute value beats the given DC
	 *
	 * @param attribute The attribute to use
	 * @param dc        The DC to beat
	 */
	public boolean check(Attribute attribute, int dc) {
		var value = getEffective(attribute);
		if (value == 0) {
			return false;
		}
		else {
			return value + Global.random(20) >= dc;
		}
	}

	public int getLevel() {
		return level;
	}

	/**
	 * Increases the character's XP
	 *
	 * @param value The XP to add
	 */
	public void gainXP(int value) {
		var scale = 1f;
		if (human()) {
			scale *= Global.getValue(Flag.PlayerScaling);
		}
		else {
			scale *= Global.getValue(Flag.NPCScaling);
		}
		if (has(Trait.fastLearner)) {
			scale += 0.1f;
		}
		if (has(Trait.veryfastLearner)) {
			scale += 0.1f;
		}
		if (Global.checkFlag(Flag.doublexp)) {
			scale += 1f;
		}
		if (has(Trait.hiddenpotential)) {
			scale += 0.3f;
		}
		if (has(Trait.limitedpotential)) {
			scale -= 0.2f;
		}
		xp += Math.round(value * scale);
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	/**
	 * Increases the character's rank
	 */
	public void rankUp() {
		this.rank++;
	}

	/**
	 * Increases the character's skills and stats after a level-up
	 */
	public abstract void ding();

	public int getXP() {
		return xp;
	}

	/**
	 * Scales the character to the specified level
	 *
	 * @param targetLevel The level to scale the character to
	 */
	public void scaleLevel(int targetLevel) {
		while (level < targetLevel) {
			ding();
		}
		xp = 0;
	}

	/**
	 * Deals pain damage to the character
	 *
	 * @param value The amount of damage
	 * @param area  The area being targeted
	 * @param c     A reference to the ongoing combat
	 */
	public int pain(int value, Anatomy area, Combat c) {
		var pain = pain(value, area);
		c.addEvent(this, new CombatEvent(Result.receivepain, area));
		c.addEvent(c.getOther(this), new CombatEvent(Result.dealpain, area));
		c.reportPain(this, pain);
		return pain;
	}

	/**
	 * Deals pain damage to the character
	 *
	 * @param value The amount of damage
	 * @param area  The area being targeted
	 */
	public int pain(int value, Anatomy area) {
		if (has(Trait.painimmune)) {
			return 0;
		}

		var pain = Math.max(1, value);

		// When targeting soul, take unmodified damage
		if (area != Anatomy.soul) {
			for (var s : status) {
				pain += s.damage(value, area);
				pain *= s.sore(area);
			}

			if (area == Anatomy.genitals) {
				if (has(Trait.achilles)) {
					pain *= 1.5;
				}
				if (has(Trait.brassballs)) {
					pain *= 0.8;
				}
				if (has(Trait.armored)) {
					pain *= 0.3;
				}
			}
		}

		emote(Emotion.angry, pain);
		getPool(Pool.STAMINA).reduce(pain);
		return pain;
	}

	/**
	 * Deals weaken damage to the character
	 *
	 * @param value The amount of damage
	 * @param c     A reference to the ongoing combat
	 */
	public int weaken(int value, Combat c) {
		var weak = weaken(value);
		c.reportWeaken(this, weak);
		return weak;
	}

	/**
	 * Deals weaken damage to the character
	 *
	 * @param value The amount of damage
	 */
	public int weaken(int value) {
		if (has(Trait.weakimmune)) {
			return 0;
		}

		var weak = Math.max(1, value);
		for (var s : status) {
			weak += s.weakened(value);
		}

		emote(Emotion.nervous, weak);
		getPool(Pool.STAMINA).minimize(weak);
		return weak;
	}

	/**
	 * Heals the character
	 *
	 * @param value The amount of healing
	 * @param c     A reference to the ongoing combat
	 */
	public int heal(int value, Combat c) {
		var healing = heal(value);
		c.reportHeal(this, healing);
		emote(Emotion.confident, healing);
		return healing;
	}

	/**
	 * Heals the character
	 *
	 * @param value The amount of healing
	 */
	public int heal(int value) {
		getPool(Pool.STAMINA).restore(value);
		return value;
	}

	/**
	 * Deals pleasure damage to the character
	 *
	 * @param value The amount of damage
	 * @param area  The area being targeted
	 * @param c     A reference to the ongoing combat
	 */
	public int pleasure(int value, Anatomy area, Combat c) {
		return pleasure(value, area, Result.normal, c);
	}

	/**
	 * Deals pleasure damage to the character
	 *
	 * @param value The amount of damage
	 * @param area  The area being targeted
	 * @param type  The type of pleasure damage taken
	 * @param c     A reference to the ongoing combat
	 */
	public int pleasure(int value, Anatomy area, Result type, Combat c) {
		var total = Math.max(1, value);
		float damage = Math.max(1, value);
		if (type == Result.foreplay) {
			total = Math.round((damage * .2f) + (damage * .8f * (Math.min(1,
					1.25f - (getArousal().get() / (.8f * getArousal().max()))))));
		}
		else if (type == Result.finisher) {
			total = Math.round((damage * .2f) + (damage * .8f * (Math.min(1,
					getArousal().get() / (.8f * getArousal().max())))));
		}
		total = pleasure(total, area);
		c.addEvent(this, new CombatEvent(Result.receivepleasure, area));
		c.addEvent(c.getOther(this), new CombatEvent(Result.dealpleasure, area));
		c.reportPleasure(this, total, type);
		return total;
	}

	/**
	 * Deals pleasure damage to the character
	 *
	 * @param value The amount of damage
	 * @param area  The area being targeted
	 */
	public int pleasure(int value, Anatomy area) {
		var pleasure = Math.max(1, value);
		if (area != Anatomy.soul) {
			for (var s : status) {
				pleasure += s.pleasure(value, area);
				pleasure *= s.sensitive(area);
			}
		}
		if (has(Trait.pleasureimmune)) {
			return 0;
		}
		if (has(Trait.desensitized)) {
			pleasure--;
		}

		if (area == Anatomy.genitals) {
			if (has(Trait.hairtrigger)) {
				pleasure *= 2;
			}
			else if (has(Trait.veteranprostitute)) {
				pleasure *= 0.75;
			}
			else if (has(Trait.silvercock)) {
				pleasure *= 0.9;
			}
			else if (has(Trait.hardon)) {
				pleasure *= 1.3;
			}
		}
		else if (area == Anatomy.ass) {
			if (has(Trait.buttslut)) {
				pleasure *= 1.3;
			}
		}
		else if (area == Anatomy.chest) {
			if (hasBreasts()) {
				pleasure *= 1.2;
			}
		}

		pleasure = Math.max(1, pleasure);
		getPool(Pool.AROUSAL).restore(pleasure);
		return pleasure;
	}

	/**
	 * Deals temptation damage to the character
	 *
	 * @param value                 The amount of damage
	 * @param proficiencyMultiplier A multiplier based on the attacker's proficiencies
	 * @param c                     A reference to the ongoing combat
	 */
	public int tempt(int value, float proficiencyMultiplier, Combat c) {
		return tempt(value, proficiencyMultiplier, Result.normal, c);
	}

	/**
	 * Deals temptation damage to the character
	 *
	 * @param value The amount of damage
	 * @param type  The type of temptation damage taken
	 * @param c     A reference to the ongoing combat
	 */
	public int tempt(int value, Tag type, Combat c) {
		return tempt(value, 1f, type, c);
	}

	/**
	 * Deals temptation damage to the character
	 *
	 * @param value The amount of damage
	 * @param c     A reference to the ongoing combat
	 */
	public int tempt(int value, Combat c) {
		return tempt(value, 1f, Result.normal, c);
	}

	/**
	 * Deals temptation damage to the character
	 *
	 * @param value                 The amount of damage
	 * @param proficiencyMultiplier A multiplier based on the attacker's proficiencies
	 * @param type                  The type of temptation damage taken
	 * @param c                     A reference to the ongoing combat
	 */
	public int tempt(int value, float proficiencyMultiplier, Tag type, Combat c) {
		var temptation = value;
		temptation = tempt(temptation, proficiencyMultiplier, type);
		c.addEvent(this, new CombatEvent(Result.receivetemptation));
		c.addEvent(c.getOther(this), new CombatEvent(Result.dealtemptation));
		c.reportTemptation(this, temptation, Result.normal);
		return temptation;
	}

	/**
	 * Deals temptation damage to the character
	 *
	 * @param value                 The amount of damage
	 * @param proficiencyMultiplier A multiplier based on the attacker's proficiencies
	 * @param type                  The type of temptation damage taken
	 */
	public int tempt(int value, float proficiencyMultiplier, Tag type) {
		float magnitude = Math.max(1, value);
		if (type == Result.foreplay) {
			magnitude = (magnitude * .2f) + (magnitude * .8f * (Math.min(1,
					1.25f - (getArousal().get() / (.8f * getArousal().max())))));
		}
		if (type == Result.finisher) {
			magnitude = (magnitude * .2f) + (magnitude * .8f * (Math.min(1,
					getArousal().get() / (.8f * getArousal().max()))));
		}
		magnitude *= proficiencyMultiplier;
		return tempt(Math.round(magnitude));
	}

	/**
	 * Deals temptation damage to the character
	 *
	 * @param value The amount of damage
	 */
	public int tempt(int value) {
		if (getArousal().isFull() || has(Trait.temptimmune)) {
			return 0;
		}

		var temptation = Math.max(1, value);
		for (var s : status) {
			temptation += s.tempted(value);
		}
		if (has(Trait.imagination)) {
			temptation *= 1.5f;
		}
		if (has(Trait.icequeen)) {
			temptation *= 0.5f;
		}
		getPool(Pool.AROUSAL).edge(temptation);
		emote(Emotion.horny, Math.round(temptation));
		return temptation;
	}

	/**
	 * Calms the character's arousal
	 *
	 * @param value The amount of calming
	 * @param c     A reference to the ongoing combat
	 */
	public int calm(int value, Combat c) {
		var mag = calm(value);
		c.reportCalm(this, mag);
		return mag;
	}

	/**
	 * Calms the character's arousal
	 *
	 * @param value The amount of calming
	 */
	public int calm(int value) {
		getPool(Pool.AROUSAL).reduce(value);
		return value;
	}

	/**
	 * Gets the Meter for the specified resource pool
	 *
	 * @param res The resource pool
	 */
	public Meter getPool(Pool res) {
		if (!pools.containsKey(res)) {
			pools.put(res, new Meter(res.getBaseCap()));
		}
		return pools.get(res);
	}

	public Meter getStamina() {
		return getPool(Pool.STAMINA);
	}

	public Meter getArousal() {
		return getPool(Pool.AROUSAL);
	}

	public Meter getMojo() {
		return getPool(Pool.MOJO);
	}

	/**
	 * Increases mojo
	 *
	 * @param percent The percentage to increase mojo by
	 */
	public void buildMojo(int percent) {
		var x = percent;
		for (var s : status) {
			x += s.gain(Pool.MOJO, x);
		}
		if (has(Trait.lameglasses) || has(Trait.legend)) {
			x = 0;
		}
		getMojo().addPercent(x);
	}

	/**
	 * Spends mojo
	 *
	 * @param value The amount of mojo to use
	 */
	public void spendMojo(int value) {
		var cost = value;
		for (var s : status) {
			cost += s.spend(Pool.MOJO, value);
		}
		if (has(Trait.overflowingmana)) {
			cost /= 20;
		}
		getMojo().reduce(cost);
	}

	/**
	 * Spends arousal (increases it!)
	 *
	 * @param value The amount of arousal to gain
	 */
	public void spendArousal(int value) {
		var mag = value;
		for (var s : status) {
			mag += s.spend(Pool.AROUSAL, value);
		}
		if (has(Trait.lustconduit)) {
			mag = Math.round(mag / 2F);
		}
		if (has(Trait.infernalexertion)) {
			weaken(mag);
		}
		else {
			tempt(mag);
		}
	}

	/**
	 * Spends stamina
	 *
	 * @param value The amount of stamina to use
	 */
	public void spendStamina(int value) {
		weaken(value);
	}

	/**
	 * Spends the specified resource
	 *
	 * @param resource The resource to spend
	 * @param value    The amount to spend
	 */
	public void spend(Pool resource, int value) {
		switch (resource) {
			case MOJO:
				spendMojo(value);
				break;
			case AROUSAL:
				spendArousal(value);
				break;
			case STAMINA:
				spendStamina(value);
				break;
			default:
				var cost = value;
				for (var s : status) {
					cost += s.spend(resource, value);
				}
				getPool(resource).reduce(cost);
		}
	}

	public Area location() {
		return location;
	}

	/**
	 * Checks whether the character is busy performing an action
	 */
	public boolean isBusy() {
		return state != State.ready && state != State.quit;
	}

	/**
	 * Gets the characters skill at reading tracks
	 */
	public int getTrackingScore() {
		var score = getEffective(Attribute.Perception) * 3;
		score += Global.random(getLevel());
		if (has(Trait.tracker)) {
			score += 10;
		}
		if (has(Trait.advtracker)) {
			score += 15;
		}
		if (has(Trait.mastertracker)) {
			score += 20;
		}
		return score;
	}

	/**
	 * Gets the character's combat initiative
	 *
	 * @param c A reference to the ongoing combat
	 */
	public int getInitiative(Combat c) {
		var s = getEffective(Attribute.Speed) + Global.random(10);
		if (c.stance.sub(this)) {
			s -= 2;
		}
		if (c.stance.prone(this)) {
			s -= 2;
		}
		return s;
	}

	/**
	 * Checks whether the character is nude
	 */
	public boolean nude() {
		return topless() && pantsless();
	}

	/**
	 * Checks whether the character wears a top that is effective
	 */
	public boolean topless() {
		return top.stream()
				.allMatch(article -> article.attribute() == Trait.ineffective);
	}

	/**
	 * Checks whether the character wears a bottom that is effective
	 */
	public boolean pantsless() {
		return bottom.stream()
				.allMatch(article -> article.attribute() == Trait.ineffective);
	}

	/**
	 * Checks whether the character can fuck wearing the current clothing
	 */
	public boolean canFuck() {
		return bottom.stream()
				.allMatch(article -> article.attribute() == Trait.ineffective
						|| article.attribute() == Trait.accessible);
	}

	/**
	 * Checks whether the character is currently erect, or aroused enough for penetration
	 */
	public boolean isErect() {
		return getArousal().get() >= 10
				|| has(Trait.bronzecock)
				|| has(Trait.strapped);
	}

	/**
	 * Dresses the character in items they stripped during combat
	 *
	 * @param c A reference to the ongoing combat
	 */
	public void dress(Combat c) {
		for (var article : outfit[OUTFITTOP]) {
			if (c.clothespile.contains(article) && !top.contains(article)) {
				top.push(article);
				c.clothespile.remove(article);
			}
		}
		for (var article : outfit[OUTFITBOTTOM]) {
			if (c.clothespile.contains(article) && !bottom.contains(article)) {
				bottom.push(article);
				c.clothespile.remove(article);
			}
		}
	}

	/**
	 * Changes the character's clothing set to the default
	 */
	public void change() {
		change(Modifier.normal);
	}

	/**
	 * Changes the characters clothing set to match the specified rule
	 *
	 * @param rule The rule that worn clothes must satisfy
	 */
	public void change(Modifier rule) {
		top = (Stack<Clothing>) outfit[OUTFITTOP].clone();
		bottom = (Stack<Clothing>) outfit[OUTFITBOTTOM].clone();
	}

	/**
	 * Removes all worn clothes and adds them to the combat clothes pile
	 *
	 * @param c A reference to the ongoing combat
	 */
	public void undress(Combat c) {
		c.clothespile.addAll(top);
		c.clothespile.addAll(bottom);
		var clist = (ArrayList<Clothing>) c.clothespile.clone();
		for (var item : clist) {
			if (item.isTemp()) {
				c.clothespile.remove(item);
			}
		}
		top.clear();
		bottom.clear();
	}

	/**
	 * Destroys all destructible clothing the character is wearing
	 *
	 * @return True if the character is now nude, otherwise False
	 */
	public boolean nudify() {
		var temp = new ArrayList<>(top);
		for (var article : temp) {
			if (article.attribute() != Trait.indestructible) {
				top.remove(article);
			}
		}
		temp.clear();
		temp.addAll(bottom);
		for (var article : temp) {
			if (article.attribute() != Trait.indestructible) {
				bottom.remove(article);
			}
		}
		return nude();
	}

	/**
	 * Resolves whether an attempt to strip the character is successful
	 *
	 * @param primaryAttribute The value of the primary attribute used to strip the character
	 * @param attacker         The character performing the stripping
	 * @param c                A reference to the ongoing combat
	 * @param target           The clothing item to be stripped
	 * @return True if the character will be stripped of the item successfully, otherwise False
	 */
	public boolean stripAttempt(int primaryAttribute, Character attacker, Combat c, Clothing target) {
		if (!canAct()) {
			return true;
		}

		// Calculate strip DC
		var dc = target.dc() * (getLevel() * getStamina().percent() / 100F);
		if (has(Trait.modestlydressed)) {
			dc += 10;
		}
		dc *= 1 - (getArousal().percent() / 200F);

		// Calculate effectiveness
		var magnitude = primaryAttribute * attacker.getStamina().percent() / 100F;
		for (var s : status) {
			magnitude += s.stripAttempt(target);
		}

		magnitude *= target.stripOffenseBonus(attacker);
		if (c.stance.dom(attacker)) {
			magnitude *= 1.2;
		}
		else if (c.stance.sub(attacker)) {
			magnitude *= .8;
		}
		magnitude += Global.random(20);

		// Mark clothing as disheveled to make future attempts easier
		if (magnitude <= dc && magnitude + 10 > dc) {
			add(new Disheveled(this, target), c);
		}
		return magnitude > dc;
	}

	/**
	 * Strips an item of clothing
	 * <p>
	 * Any non-temporary item is added to the combat's clothes pile
	 *
	 * @param half 0 - top clothing, 1 - bottom clothing
	 * @param c    A reference to the ongoing combat
	 * @return The clothing item that was stripped, or Null if none was removed
	 */
	public Clothing strip(int half, Combat c) {
		if (half == 0) {
			if (topless()) {
				return null;
			}
			else {
				if (!top.peek().isTemp()) {
					c.clothespile.add(top.peek());
				}
				return top.pop();
			}
		}
		else {
			if (pantsless()) {
				return null;
			}
			else {
				if (!bottom.peek().isTemp()) {
					c.clothespile.add(bottom.peek());
				}
				return bottom.pop();
			}
		}
	}

	/**
	 * Strips a random item of clothing
	 * <p>
	 * Any non-temporary item is added to the combat's clothes pile
	 *
	 * @param c A reference to the ongoing combat
	 * @return The clothing item that was stripped, or Null if none was removed
	 */
	public Clothing stripRandom(Combat c) {
		var target = Global.random(2);
		if (top.isEmpty()) {
			target = OUTFITBOTTOM;
		}
		else if (bottom.isEmpty()) {
			target = OUTFITTOP;
		}

		return strip(target, c);
	}

	/**
	 * Shreds an item of clothing if it is destructible
	 * <p>
	 * Shredded items cannot be recovered until resupplying after combat
	 *
	 * @param half 0 - top clothing, 1 - bottom clothing
	 * @return The clothing item that was shredded, or Null if none was removed
	 */
	public Clothing shred(int half) {
		if (half == 0) {
			if (!topless() && top.peek().attribute() != Trait.indestructible) {
				return top.pop();
			}
			else {
				return null;
			}
		}
		else {
			if (!pantsless() && bottom.peek().attribute() != Trait.indestructible) {
				return bottom.pop();
			}
			else {
				return null;
			}
		}
	}

	/**
	 * Shreds a random item of clothing if it is destructible
	 * <p>
	 * Shredded items cannot be recovered until resupplying after combat
	 *
	 * @return The clothing item that was shredded, or Null if none was removed
	 */
	public Clothing shredRandom() {
		var target = Global.random(2);
		if (top.isEmpty()) {
			target = OUTFITBOTTOM;
		}
		else if (bottom.isEmpty()) {
			target = OUTFITTOP;
		}

		return shred(target);
	}

	/**
	 * Checks whether the character can wear the specified item
	 *
	 * @param article The clothing item to check
	 */
	public boolean canWear(Clothing article) {
		return !isWearing(article.getType());
	}

	/**
	 * Checks whether the character is wearing an item of the specified type
	 *
	 * @param type The clothing type to check
	 */
	public boolean isWearing(ClothingType type) {
		for (var worn : top) {
			if (worn.getType() == type) {
				return true;
			}
		}
		for (var worn : bottom) {
			if (worn.getType() == type) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Removes worn clothing matching the specified type
	 *
	 * @param type The clothing type to remove
	 */
	public void removeLayer(ClothingType type) {
		top.removeIf(worn -> worn.getType() == type);
		bottom.removeIf(worn -> worn.getType() == type);
	}

	/**
	 * Puts on the specified clothing item
	 *
	 * @param article The clothing item to wear
	 */
	public void wear(Clothing article) {
		switch (article.getType()) {
			case TOPUNDER:
				top.insertElementAt(article, 0);
				break;
			case UNDERWEAR:
				bottom.insertElementAt(article, 0);
				break;
			case TOP:
				if (isWearing(ClothingType.TOPUNDER)) {
					top.insertElementAt(article, 1);
				}
				else {
					top.insertElementAt(article, 0);
				}
				break;
			case TOPOUTER:
				top.push(article);
				break;
			default:
				bottom.push(article);
		}
	}

	/**
	 * Replaces worn clothing with the specified item
	 *
	 * @param article The clothing item to wear
	 */
	public void replaceArticle(Clothing article) {
		if (canWear(article)) {
			wear(article);
		}
		else {
			removeLayer(article.getType());
			wear(article);
		}
	}

	/**
	 * Gets the character's skimpiness value
	 */
	public int getSkimpiness() {
		var result = 2;
		for (var article : top) {
			if (article.attribute() != Trait.skimpy && article.attribute() != Trait.ineffective) {
				result--;
				break;
			}
		}
		for (var article : bottom) {
			if (article.attribute() != Trait.skimpy && article.attribute() != Trait.ineffective) {
				result--;
				break;
			}
		}
		if (has(Trait.scandalous)) {
			result += 1;
		}
		return result;
	}

	/**
	 * Gets a worn clothing item matching the specified type
	 *
	 * @param slot The clothing type to get
	 * @return The clothing item the character is wearing, or Null if none was found
	 */
	public Clothing getOutfitItem(ClothingType slot) {
		for (var c : outfit[OUTFITTOP]) {
			if (c.getType() == slot) {
				return c;
			}
		}
		for (var c : outfit[OUTFITBOTTOM]) {
			if (c.getType() == slot) {
				return c;
			}
		}
		return null;
	}

	/**
	 * Gets a description of the currently worn top clothing
	 */
	public String getTop() {
		return topless()
			   ? "shirt"
			   : top.peek().getName();
	}

	/**
	 * Gets a description of the currently worn bottom clothing
	 */
	public String getBottom() {
		return (pantsless() || (bottom.peek().getType() != ClothingType.BOTOUTER))
			   ? "pants"
			   : bottom.peek().getName();
	}

	/**
	 * Adds the specified trait
	 *
	 * @param trait The trait to add
	 */
	public void add(Trait trait) {
		traits.add(trait);
	}

	/**
	 * Removes the specified trait
	 *
	 * @param trait The trait to remove
	 */
	public void remove(Trait trait) {
		traits.remove(trait);
	}

	/**
	 * Checks if the specified trait currently applies to the character
	 *
	 * @param trait The trait to check
	 */
	public boolean has(Trait trait) {
		for (var shirt : top) {
			if (shirt.adds(trait)) {
				return true;
			}
		}
		for (var pants : bottom) {
			if (pants.adds(trait)) {
				return true;
			}
		}
		if (activeGrudge == trait) {
			return true;
		}
		if (matchmod == trait) {
			return true;
		}
		for (var s : status) {
			if (s.tempTraits(trait)) {
				return true;
			}
		}
		return traits.contains(trait);
	}

	/**
	 * Sets the match modifier applied to the character
	 *
	 * @param modifier The modifier to apply
	 */
	public void setModifier(Trait modifier) {
		matchmod = modifier;
	}

	/**
	 * Checks whether the character has a dick
	 */
	public boolean hasDick() {
		return traits.contains(Trait.male) || traits.contains(Trait.herm);
	}

	/**
	 * Checks whether the character has balls
	 */
	public boolean hasBalls() {
		return traits.contains(Trait.male) || traits.contains(Trait.herm);
	}

	/**
	 * Checks whether the character has a pussy
	 */
	public boolean hasPussy() {
		return traits.contains(Trait.female) || traits.contains(Trait.herm);
	}

	/**
	 * Checks whether the character has breasts
	 */
	public boolean hasBreasts() {
		return traits.contains(Trait.female) || traits.contains(Trait.herm);
	}

	/**
	 * Checks whether the character is benched from matches
	 */
	public boolean isBenched() {
		return benched;
	}

	/**
	 * Sets whether the character is benched from matches
	 */
	public void setBenched(boolean benched) {
		this.benched = benched;
	}

	/**
	 * Gets the number of feats the character has
	 */
	public int countFeats() {
		return (int) (traits.stream()
				.filter(Trait::isFeat)
				.count());
	}

	/**
	 * Regenerates the character's stats
	 *
	 * @param combat Whether the character is in combat
	 */
	public void regen(boolean combat) {
		// Stamina
		var regen = Math.max(1, getStamina().max() / 50);
		for (var s : status) {
			regen += s.regen();
		}
		if (has(Trait.BoundlessEnergy)) {
			regen += Math.max(1, getStamina().max() / 20);
		}
		if (has(Trait.streaker) && nude()) {
			regen += Math.max(1, getStamina().max() / 50);
		}
		if (has(Trait.healing)) {
			regen += Math.max(1, getStamina().max() / 10);
		}
		heal(regen);

		// Arousal
		var calming = 0;
		if (has(Trait.confidentdom)
				&& (getMood() == Emotion.dominant
				|| getMood() == Emotion.confident)) {
			calming += getArousal().max() / 10;
		}
		if (has(Trait.tantra)) {
			calming += getArousal().max() / 20;
		}
		if (has(Trait.legend)) {
			calming += getArousal().max() / 10;
		}
		if (has(Trait.freeenergy)) {
			getPool(Pool.BATTERY).restore(1);
		}
		calm(calming);
		if (!combat) {
			return;
		}

		// Mojo
		if (has(Trait.exhibitionist) && nude()) {
			buildMojo(5);
		}
		if (has(Trait.streaker) && nude()) {
			buildMojo(10);
		}
		if (has(Trait.stylish)) {
			buildMojo(3);
		}
		if (has(Trait.SexualGroove)) {
			buildMojo(2);
		}
		if (has(Trait.lame)) {
			buildMojo(-3);
		}
		if (has(Trait.spirited)) {
			buildMojo(50);
		}

		// Focus
		if (getPure(Attribute.Spirituality) > 0
				&& !is(Stsflag.mindaffecting)
				&& !is(Stsflag.cynical)
				&& !is(Stsflag.stunned)) {
			getPool(Pool.FOCUS).restore(1);
		}

		// Enigma
		if (getPure(Attribute.Unknowable) > 0) {
			getPool(Pool.ENIGMA).restore(1);
		}
	}

	/**
	 * Adds the specified status if the character is not immune
	 *
	 * @param status The status to add
	 */
	public void add(Status status) {
		add(status, null);
	}

	/**
	 * Adds the specified status if the character is not immune
	 *
	 * @param status The status to add
	 * @param c      A reference to the ongoing combat
	 */
	public void add(Status status, Combat c) {
		if ((has(Trait.shameless) && status.flags().contains(Stsflag.shamed))
				|| (has(Trait.antihorny) && status.flags().contains(Stsflag.horny))) {
			if (c != null) {
				c.reportStatusFizzle(this, status);
			}
			return;
		}

		var cynical = false;
		for (var s : this.status) {
			if (s.flags().contains(Stsflag.cynical)) {
				cynical = true;
			}
			if (s.toString().equals(status.toString())) {
				if (s.stacking()) {
					s.stack(status);
				}
				return;
			}
		}
		if (cynical && status.flags().contains(Stsflag.mindaffecting)) {
			if (c != null) {
				c.reportStatusFizzle(this, status);
			}
			return;
		}

		if (c != null) {
			c.reportStatus(this, status);
		}
		this.status.add(status);
	}

	/**
	 * Removes the specified status
	 *
	 * @param status The status to remove
	 */
	public void removeStatus(Status status) {
		if (status.removable()) {
			this.status.remove(status);
		}
	}

	/**
	 * Removes the specified status
	 *
	 * @param status The status to remove
	 * @param c      A reference to the ongoing combat
	 */
	public void removeStatus(Status status, Combat c) {
		if (!status.removable()) {
			return;
		}

		removeStatus(status);
		c.reportStatusLoss(this, status);
	}

	/**
	 * Removes any status with the specified flag
	 *
	 * @param flag The flag to remove
	 */
	public void removeStatus(Stsflag flag) {
		removeStatus(flag, null);
	}

	/**
	 * Removes any status with the specified flag
	 *
	 * @param flag The flag to remove
	 * @param c    A reference to the ongoing combat
	 */
	public void removeStatus(Stsflag flag, Combat c) {
		var copy = new ArrayList<>(status);
		for (var s : copy) {
			if (s.flags().contains(flag) && s.removable()) {
				status.remove(s);
				if (c != null) {
					c.reportStatusLoss(this, s);
				}
			}
		}
	}

	/**
	 * Checks whether the specified status flag is applied to the character
	 *
	 * @param flag The flag to check
	 */
	public boolean is(Stsflag flag) {
		return status.stream()
				.anyMatch(s -> s.flags().contains(flag));
	}

	/**
	 * Checks whether the character is stunned
	 */
	public boolean stunned() {
		return is(Stsflag.stunned);
	}

	/**
	 * Checks whether the character is distracted
	 */
	public boolean distracted() {
		return is(Stsflag.distracted) || is(Stsflag.charmed);
	}

	/**
	 * Checks whether the character is bound
	 */
	public boolean bound() {
		return is(Stsflag.bound);
	}

	/**
	 * Frees the character from any binding status
	 */
	public void free() {
		removeStatus(Stsflag.bound);
	}

	/**
	 * Gets the DC modifier for escaping from a binding or pin
	 *
	 * @return The escape DC modifier (positive = easier, negative = harder)
	 */
	public int escapeModifier() {
		var total = 0;
		var maxbind = 0;

		// Calculate status effects
		for (var s : status) {
			if (s.flags().contains(Stsflag.bound)) {
				maxbind = Math.min(maxbind, s.escape());
			}
			else {
				total += s.escape();
			}
		}
		total += maxbind;

		// Calculate trait effects
		if (has(Trait.houdini)) {
			total += getEffective(Attribute.Cunning) / 5;
		}
		if (has(Trait.freeSpirit)) {
			total += 2;
		}
		return total;
	}

	/**
	 * Checks whether the character can act at all
	 */
	public boolean canAct() {
		return !(stunned() || distracted() || bound() || is(Stsflag.enthralled));
	}

	/**
	 * Checks whether the character can act normally
	 *
	 * @param c A reference to the ongoing combat
	 */
	public boolean canActNormally(Combat c) {
		return canAct() && c.stance.mobile(this) && !c.stance.prone(this) && !c.stance.sub(this);
	}

	/**
	 * Checks whether the character's genitals are accessible to the opponent
	 *
	 * @param c A reference to the ongoing combat
	 */
	public boolean genitalsAvailable(Combat c) {
		var available = pantsless() || (c.getOther(this).has(Trait.dexterous) && bottom.size() <= 1);
		if (!c.stance.reachBottom(c.getOther(this))) {
			return false;
		}
		if (c.stance.penetration(this) && !(hasDick() && hasPussy())) {
			return false;
		}
		return available;
	}

	/**
	 * Checks whether the character will orgasm or can resist
	 *
	 * @param c A reference to the ongoing combat
	 */
	public boolean willOrgasm(Combat c) {
		if (!getArousal().isFull()) {
			return false;
		}
		if (getPure(Attribute.Contender) < 5) {
			return true;
		}

		var avoidChance = has(Trait.protagonist)
						  ? Math.min(60, getPure(Attribute.Contender) * 5)
						  : Math.min(40, getPure(Attribute.Contender) * 3);
		if (Global.random(100) < avoidChance) {
			if (human()) {
				c.write(this,
						"You feel yourself about to ejaculate, but you muster all of your grit and determination and push the urge down.");
			}
			else {
				c.write(this,
						"You are sure " + name() + " is on the verge of cumming, but she grits her teeth and keeps fighting. Judging by the fire in her eyes, she's far from giving up.");
			}
			calm(Math.round(getArousal().max() * .3f));
			return false;
		}
		return true;
	}

	/**
	 * Checks the surrounding areas to detect enemies
	 */
	public abstract void detect();
	/**
	 * Initiates a face-off with the specified character to determine whether to start a fight
	 *
	 * @param opponent The character to face-off against
	 * @param enc      A reference to the encounter event
	 */
	public abstract void faceOff(Character opponent, Encounter enc);
	/**
	 * Initiates a spying event when the opponent has not yet seen this character to determine whether to start a fight
	 *
	 * @param opponent The character to spy on
	 * @param enc      A reference to the encounter event
	 */
	public abstract void spy(Character opponent, Encounter enc);
	/**
	 * Gets a description of the character, based on the specified perception
	 *
	 * @param perception The perception value of the character getting the description
	 */
	public abstract String describe(int perception);
	/**
	 * Processes the result of a combat where the character has won
	 *
	 * @param c    A reference to the ongoing combat
	 * @param flag A flag defining how the combat was won (e.g. intercourse/anal)
	 */
	public abstract void victory(Combat c, Tag flag);
	/**
	 * Processes the result of a combat where the character has lost
	 *
	 * @param c    A reference to the ongoing combat
	 * @param flag A flag defining how the combat was lost (e.g. intercourse/anal)
	 */
	public abstract void defeat(Combat c, Tag flag);
	/**
	 * Processes the result of a combat where the character intervenes in an ongoing fight
	 *
	 * @param c      A reference to the ongoing combat
	 * @param target The character that is being defeated
	 * @param assist The character that is being assisted
	 */
	public abstract void intervene3p(Combat c, Character target, Character assist);
	/**
	 * Processes the result of a combat where the character wins through assistance of another character
	 *
	 * @param c      A reference to the ongoing combat
	 * @param target The character that is being defeated
	 * @param assist The character that assisted
	 */
	public abstract void victory3p(Combat c, Character target, Character assist);
	/**
	 * Processes the result of a combat where the character watched
	 *
	 * @param c        A reference to the ongoing combat
	 * @param victory  The character that won the fight
	 * @param defeated The character that was defeated
	 */
	public abstract void watcher(Combat c, Character victory, Character defeated);
	/**
	 * Processes the result of a combat where a character was watching
	 *
	 * @param c        A reference to the ongoing combat
	 * @param voyeur   The character that watched the fight
	 * @param defeated The character that was defeated
	 */
	public abstract void watched(Combat c, Character voyeur, Character defeated);
	/**
	 * Determine the next combat action used by the character
	 *
	 * @param c A reference to the ongoing combat
	 */
	public abstract void act(Combat c);
	/**
	 * Determine the next movement action used by the character
	 *
	 * @param match A reference to the ongoing match
	 */
	public abstract void move(Match match);
	/**
	 * Processes the result of a combat resulting in a draw
	 *
	 * @param c    A reference to the ongoing combat
	 * @param flag A flag defining how the combat has ended (e.g. intercourse/anal)
	 */
	public abstract void draw(Combat c, Tag flag);
	/**
	 * Checks whether the character is controlled by the player
	 */
	public abstract boolean human();
	/**
	 * Get a response line when the character is entering combat
	 *
	 * @param opponent The opponent for the initiated combat
	 * @return A string containing the character's response
	 */
	public abstract String challenge(Character opponent);

	/**
	 * Get a response line when the character has performed a ball-busting move
	 *
	 * @return A string containing the character's response
	 */
	public abstract String bbLiner();

	/**
	 * Get a response line when encountering the character while they are naked
	 *
	 * @return A string containing the character's response
	 */
	public abstract String nakedLiner();

	/**
	 * Get a response line when the character has been stunned
	 *
	 * @return A string containing the character's response
	 */
	public abstract String stunLiner();

	/**
	 * Get a response line when the character is using a taunt
	 *
	 * @return A string containing the character's response
	 */
	public abstract String taunt();

	/**
	 * Determine the character's response when encountering an ongoing fight
	 *
	 * @param enc A reference to the ongoing encounter
	 * @param p1  The first character
	 * @param p2  The second character
	 */
	public abstract void intervene(Encounter enc, Character p1, Character p2);

	/**
	 * Determines whether the character can resist the intervention in an ongoing fight
	 *
	 * @param combat   A reference to the ongoing combat
	 * @param intruder The character intervening in the fight
	 * @param assist   The character being assisted
	 */
	public abstract boolean resist3p(Combat combat, Character intruder, Character assist);

	/**
	 * Initiates a scene where the target is encountered while showering
	 *
	 * @param target    The character taking a shower
	 * @param encounter A reference to the ongoing encounter
	 */
	public abstract void showerScene(Character target, Encounter encounter);

	/**
	 * Loads the character's data from the specified loader instance
	 * <p>
	 * !Legacy function for old saves!
	 *
	 * @param loader The loader containing the character data
	 */
	public abstract void load(Scanner loader);

	/**
	 * Runs the character's post-match party logic
	 */
	public abstract void afterParty();

	/**
	 * Perform end-of-turn logic
	 *
	 * @param c        A reference to the ongoing combat
	 * @param opponent A reference to the opponent character
	 */
	public abstract void endOfTurn(Combat c, Character opponent);

	/**
	 * Processes the character's moods to determine their primary emotion
	 *
	 * @return The new primary emotion of the character
	 */
	public abstract Emotion moodSwing();

	/**
	 * Resets the character's outfit to its default
	 */
	public abstract void resetOutfit();

	/**
	 * Saves the character's data
	 *
	 * @return The JSON object containing the saved data
	 */
	public JsonObject save() {
		var saver = new JsonObject();
		saver.addProperty("Name", name);
		saver.addProperty("Level", level);
		saver.addProperty("Rank", getRank());
		saver.addProperty("XP", xp);
		saver.addProperty("Money", money);
		saver.addProperty("Score", Scheduler.getScore(id()));
		saver.addProperty("Benched", benched);

		var attributes = new JsonObject();
		for (var attribute : att.keySet()) {
			attributes.addProperty(attribute.toString(), att.get(attribute));
		}
		saver.add("Attributes", attributes);

		var pool = new JsonObject();
		for (var resource : pools.keySet()) {
			pool.addProperty(resource.toString(), pools.get(resource).max());
		}
		saver.add("Pools", pool);

		var traits = new JsonArray();
		for (var trait : this.traits) {
			traits.add(trait.name());
		}
		saver.add("Traits", traits);

		var upper = new JsonArray();
		for (var clothing : outfit[Character.OUTFITTOP]) {
			upper.add(clothing.name());
		}
		saver.add("Upper Clothing", upper);

		var lower = new JsonArray();
		for (var clothing : outfit[Character.OUTFITBOTTOM]) {
			lower.add(clothing.name());
		}
		saver.add("Lower Clothing", lower);

		var allClothes = new JsonArray();
		for (var clothing : closet) {
			allClothes.add(clothing.name());
		}
		saver.add("Owned Clothes", allClothes);

		var items = new JsonObject();
		for (var item : inventory.entrySet()) {
			items.addProperty(item.getKey().toString(), item.getValue());
		}
		saver.add("Inventory", items);
		return saver;
	}

	/**
	 * Loads the character's data from JSON
	 *
	 * @param loader The loader containing the character's data
	 */
	public void load(JsonObject loader) {
		this.name = loader.get("Name").getAsString();
		this.level = loader.get("Level").getAsInt();
		this.rank = loader.get("Rank").getAsInt();
		this.xp = loader.get("XP").getAsInt();
		this.money = loader.get("Money").getAsInt();
		if (loader.has("Benched")) {
			benched = loader.get("Benched").getAsBoolean();
		}

		grudges.clear();
		mercy.clear();
		matchmod = null;

		// Attributes
		att.clear();
		var attributes = loader.getAsJsonObject("Attributes");
		for (var attribute : Attribute.values()) {
			if (attributes.has(attribute.name())) {
				att.put(attribute, attributes.get(attribute.name()).getAsInt());
			}
		}

		// Stats
		if (loader.has("Pools")) {
			var pool = loader.getAsJsonObject("Pools");
			pools.clear();
			for (var resource : Pool.values()) {
				if (pool.has(resource.name())) {
					pools.put(resource, new Meter(pool.get(resource.name()).getAsInt()));
				}
			}
		}
		if (loader.has("Stamina")) {
			pools.get(Pool.STAMINA).setMax(loader.get("Stamina").getAsInt());
		}
		if (loader.has("Arousal")) {
			pools.get(Pool.AROUSAL).setMax(loader.get("Arousal").getAsInt());
		}
		if (loader.has("Mojo")) {
			pools.get(Pool.MOJO).setMax(loader.get("Mojo").getAsInt());
		}

		// Traits
		traits.clear();
		for (var trait : loader.getAsJsonArray("Traits")) {
			traits.add(Trait.valueOf(trait.getAsString()));
		}

		// Clothing
		if (human()) {
			outfit[Character.OUTFITTOP].clear();
			for (var clothing : loader.getAsJsonArray("Upper Clothing")) {
				outfit[Character.OUTFITTOP].push(Clothing.valueOf(clothing.getAsString()));
			}

			outfit[Character.OUTFITBOTTOM].clear();
			for (var clothing : loader.getAsJsonArray("Lower Clothing")) {
				outfit[Character.OUTFITBOTTOM].push(Clothing.valueOf(clothing.getAsString()));
			}
		}
		else {
			resetOutfit();
		}

		closet.clear();
		for (var clothing : loader.getAsJsonArray("Owned Clothes")) {
			closet.add(Clothing.valueOf(clothing.getAsString()));
		}

		// Items
		inventory.clear();

		var inventoryElement = loader.get("Inventory");
		if (inventoryElement.isJsonArray()) {
			// Old inventory
			for (var itemName : inventoryElement.getAsJsonArray()) {
				var item = Global.getItem(itemName.getAsString());
				inventory.put(item, inventory.getOrDefault(item, 0) + 1);
			}
		}
		else if (inventoryElement.isJsonObject()) {
			// New inventory
			var inventoryMap = inventoryElement.getAsJsonObject();
			if (inventoryMap != null) {
				for (var itemEntry : inventoryMap.entrySet()) {
					var item = Global.getItem(itemEntry.getKey());
					inventory.put(item, itemEntry.getValue().getAsInt());
				}
			}
		}

		Scheduler.setScore(id(), loader.get("Score").getAsInt());
		resetSkills();
		Global.gainSkills(this, false);
		getStamina().fill();
		getArousal().empty();
		getMojo().empty();
	}

	/**
	 * Reset the character's skills to the defaults available to everyone
	 */
	public void resetSkills() {
		skills.clear();
		skills.add(new Struggle(this));
		skills.add(new Nothing(this));
		skills.add(new Recover(this));
		skills.add(new Straddle(this));
		skills.add(new ReverseStraddle(this));
		skills.add(new Stunned(this));
		skills.add(new Distracted(this));
		skills.add(new PullOut(this));
		flaskskills.clear();
		for (var flask : Flask.values()) {
			flaskskills.add(new UseFlask(this, flask));
		}
		potionskills.clear();
		for (var potion : Potion.values()) {
			potionskills.add(new UsePotion(this, potion));
		}
	}

	/**
	 * Learns the specified skill
	 *
	 * @param skill The skill to learn
	 */
	public void learn(Skill skill) {
		skills.add(skill);
	}

	/**
	 * Gets the character's proficiency at sneaking
	 */
	public int sneakValue() {
		var sneak = 5;
		if (state == State.ready) {
			sneak = Math.toIntExact(Math.round(Math.pow(getEffective(Attribute.Cunning), 1.1) * 2));
		}
		if (has(Trait.Sneaky)) {
			sneak += 10;
		}
		if (has(Trait.assassin) && Global.random(2) == 0) {
			sneak += 50;
		}
		return sneak;
	}

	/**
	 * Checks whether the character can beat the specified sneak challenge for hearing an opponent
	 *
	 * @param sneakDC The DC of the opponent's sneak skill
	 */
	public boolean listenCheck(int sneakDC) {
		return Global.random(getEffective(Attribute.Perception) * (20 + getLevel())) > sneakDC * 2;
	}

	/**
	 * Checks whether the character can beat the specified sneak challenge for seeing an opponent
	 *
	 * @param sneakDC The DC of the opponent's sneak skill
	 */
	public boolean spotCheck(int sneakDC) {
		return Global.random(getEffective(Attribute.Perception) * (20 + getLevel())) > sneakDC;
	}

	/**
	 * Gets the character's bonus to disarming or avoiding traps
	 */
	public int bonusDisarm() {
		return has(Trait.cautious) ? 5 : 0;
	}

	/**
	 * Calculate pleasure from performing sexual action
	 *
	 * @param pace A numerical indicator of the action's pace (higher = faster)
	 * @param used The attribute used to determine how skilled the character is at performing the action
	 * @return The sexual pleasure the action would inflict
	 */
	public int getSexPleasure(int pace, Attribute used) {
		var total = Math.round((3 * pace + getEffective(used) / 2F) * (.8f + (.1f * Global.random(4))));
		if (has(Trait.strapped)) {
			total += (getEffective(Attribute.Science) / 2);
			total = bonusProficiency(Anatomy.toy, total);
		}
		else {
			total = bonusProficiency(Anatomy.genitals, total);
		}
		return total;
	}

	/**
	 * Calculate recoil pleasure from intercourse with character
	 *
	 * @param mag The magnitude of regular pleasure damage
	 */
	public int bonusRecoilPleasure(int mag) {
		var cap = .1f;
		if (is(Stsflag.shamed)) {
			cap += getStatusMagnitude("Shamed") * .05f;
		}

		var modifier = Math.min(getEffective(Attribute.Seduction) * .01f, cap);
		if (has(Trait.responsive)) {
			modifier += .2f;
		}
		if (has(Trait.ishida3rdart)) {
			modifier += getEffective(Attribute.Ninjutsu) / 20F;
		}
		return Math.round(mag * modifier);
	}

	/**
	 * Calculate bonus to pet power
	 */
	public int bonusPetPower() {
		var total = 0;
		if (has(Trait.leadership)) {
			total += getEffective(Attribute.Perception) / 2;
		}
		if (has(Trait.coordinatedStrikes)) {
			total += getEffective(Attribute.Cunning) / 4;
		}
		return total;
	}

	/**
	 * Calculate bonus to pet evasion
	 */
	public int bonusPetEvasion() {
		var total = 0;
		if (has(Trait.tactician)) {
			total += getEffective(Attribute.Perception) / 2;
		}
		if (has(Trait.evasiveManuevers)) {
			total += getEffective(Attribute.Cunning) / 4;
		}
		return total;
	}

	/**
	 * Calculate bonus to enthrall duration
	 */
	public int bonusEnthrallDuration() {
		var total = getEffective(Attribute.Dark) / 10;
		total += getEffective(Attribute.Hypnosis) / 3;
		return total;
	}

	/**
	 * Calculate bonus to charm duration
	 */
	public int bonusCharmDuration() {
		var total = getEffective(Attribute.Hypnosis) / 3;
		if (getSkimpiness() >= 2) {
			total++;
		}
		return total;
	}

	/**
	 * Apply proficiency bonus to action damage
	 *
	 * @param using      The Anatomy used in the action
	 * @param baseDamage The base damage of the action
	 * @return The total damage inflicted by the action
	 */
	public int bonusProficiency(Anatomy using, int baseDamage) {
		var proficiencyModifier = 1.0f;
		for (var s : status) {
			proficiencyModifier *= s.proficiency(using);
		}
		if (has(Trait.legend)) {
			proficiencyModifier++;
		}

		switch (using) {
			case fingers:
				if (has(Trait.handProficiency)) {
					proficiencyModifier += .1f;
				}
				if (has(Trait.handExpertise)) {
					proficiencyModifier += .1f;
				}
				if (has(Trait.handMastery)) {
					proficiencyModifier += .1f;
				}
				if (has(Trait.mittens)) {
					proficiencyModifier -= .75f;
				}
				if (has(Trait.smallhands)) {
					proficiencyModifier += .3f;
				}
				break;
			case mouth:
				if (has(Trait.oralProficiency)) {
					proficiencyModifier += .1f;
				}
				if (has(Trait.oralExpertise)) {
					proficiencyModifier += .1f;
				}
				if (has(Trait.oralMastery)) {
					proficiencyModifier += .1f;
				}
				break;
			case genitals:
				if (has(Trait.intercourseProficiency)) {
					proficiencyModifier += .1f;
				}
				if (has(Trait.intercourseExpertise)) {
					proficiencyModifier += .1f;
				}
				if (has(Trait.intercourseMastery)) {
					proficiencyModifier += .1f;
				}
				if (has(Trait.succubusvagina)) {
					proficiencyModifier += 1.5f;
				}
				if (has(Trait.hardon)) {
					proficiencyModifier += 1f;
				}
				break;
			case feet:
				if (has(Trait.footloose)) {
					proficiencyModifier += .5f;
				}
				if (has(Trait.striker)) {
					proficiencyModifier += .3f;
				}
				break;
			case toy:
				if (has(Trait.toymaster)) {
					proficiencyModifier += .2f;
				}
				if (has(Trait.experimentalweaponry)) {
					proficiencyModifier += 1f;
				}
				break;
		}
		return Math.round(proficiencyModifier * baseDamage);
	}

	/**
	 * Calculate temptation proficiency modifier
	 */
	public float bonusTemptation() {
		var prof = 1f;
		if (has(Trait.romantic)) {
			prof += .2f;
		}
		if (has(Trait.careerseductress)) {
			prof += .5f;
		}
		prof += getSkimpiness() * .5f;
		return prof;
	}

	/**
	 * Calculate total DC for pins
	 *
	 * @param baseDC Base DC of the pin
	 */
	public int bonusPin(int baseDC) {
		var scale = 1f;
		if (has(Trait.Clingy)) {
			scale += .1f;
		}
		if (has(Trait.predatorinstincts)) {
			scale += .5f;
		}
		return Math.round(baseDC * scale);
	}

	/**
	 * Travels to the specified area
	 *
	 * @param destination The area to travel to
	 */
	public void travel(Area destination) {
		state = State.ready;
		location.exit(this);
		location = destination;
		destination.enter(this);
	}

	/**
	 * Flees from the current location
	 */
	public void flee() {
		var adjacent = location.adjacent.toArray(new Area[0]);
		travel(adjacent[Global.random(adjacent.length)]);
		location.endEncounter();
	}

	/**
	 * Run character upkeep during a match
	 */
	public void upkeep() {
		regen(false);
		if (has(Trait.Confident)) {
			getMojo().reduce(5);
		}
		else {
			getMojo().reduce(10);
		}

		if (bound()) {
			free();
		}

		if (has(Trait.QuickRecovery)) {
			heal(4);
		}
		if (has(Trait.exhibitionist) && nude()) {
			buildMojo(3);
		}
		for (var s : new HashSet<>(status)) {
			s.decay();
		}
		decayMood();
		moodSwing();
		setChanged();
		notifyObservers();
	}

	/**
	 * Adds money to the character's bank
	 *
	 * @param amount The amount of money to add
	 * @return The amount of money added
	 */
	public int income(int amount) {
		money += amount;
		return amount;
	}

	/**
	 * Adds the specified item to the inventory
	 *
	 * @param item The item to add
	 */
	public void gain(Item item) {
		gain(item, 1);
	}

	/**
	 * Adds the specified item to the inventory
	 *
	 * @param item     The item to add
	 * @param quantity The quantity to add
	 */
	public void gain(Item item, int quantity) {
		if (item == null) {
			return;
		}

		var newQuantity = Math.max(count(item) + quantity, 0);
		updateInventory(item, newQuantity);
	}

	/**
	 * Adds the specified clothing article to the closet
	 *
	 * @param article The article of clothing
	 */
	public void gain(Clothing article) {
		closet.add(article);
	}

	/**
	 * Checks if the inventory contains the specified item
	 *
	 * @param item The item to check for
	 */
	public boolean has(Item item) {
		return has(item, 1);
	}

	/**
	 * Checks if the inventory contains the specified item
	 *
	 * @param item     The item to check for
	 * @param quantity The number of items to check for
	 */
	public boolean has(Item item, int quantity) {
		return count(item) >= quantity;
	}

	/**
	 * Checks whether the character has all items required for the specified recipe
	 *
	 * @param recipe The recipe to check
	 */
	public boolean hasAll(ArrayList<Item> recipe) {
		var counts = new HashMap<Item, Integer>();
		for (var item : recipe) {
			if (counts.containsKey(item)) {
				counts.put(item, counts.get(item) + 1);
			}
			else {
				counts.put(item, 1);
			}
		}

		return counts.keySet().stream()
				.allMatch(item -> has(item, counts.get(item)));
	}

	/**
	 * Checks if the closet contains the specified clothing article
	 *
	 * @param article The article of clothing
	 */
	public boolean has(Clothing article) {
		return closet.contains(article);
	}

	/**
	 * Consume the specified quantity of an item
	 *
	 * @param item     The item to consume
	 * @param quantity The amount to consume
	 */
	public void consume(Item item, int quantity) {
		if (has(Trait.resourceful) && Global.random(5) == 0) {
			quantity--;
		}

		if (quantity == 0) {
			return;
		}

		var newQuantity = Math.max(count(item) - quantity, 0);
		updateInventory(item, newQuantity);
	}

	/**
	 * Consume all items from the list
	 *
	 * @param items The items to consume
	 */
	public void consumeAll(ArrayList<Item> items) {
		for (var item : items) {
			consume(item, 1);
		}
	}

	/**
	 * Removes the specified quantity of an item
	 *
	 * @param item     The item to remove
	 * @param quantity The quantity to remove
	 */
	public void remove(Item item, int quantity) {
		var newQuantity = Math.max(count(item) - quantity, 0);
		updateInventory(item, newQuantity);
	}

	/**
	 * Gets the quantity of the specified item in the inventory
	 *
	 * @param item The item to check for
	 */
	public int count(Item item) {
		return inventory.getOrDefault(item, 0);
	}

	/**
	 * Updates the inventory for the specified item
	 * <p>
	 * If the new quantity is 0, the item will be removed from the inventory.
	 *
	 * @param item     The item to update
	 * @param quantity The new quantity of the item
	 */
	private void updateInventory(Item item, int quantity) {
		if (quantity > 0) {
			inventory.put(item, quantity);
		}
		else {
			inventory.remove(item);
		}
	}

	//Special Currency

	/**
	 * Completely refills the battery pool
	 */
	public void chargeBattery() {
		pools.get(Pool.BATTERY).fill();
	}

	/**
	 * Gets the grudge for the current fight
	 */
	public Trait getGrudge() {
		return activeGrudge;
	}

	/**
	 * Adds a grudge against the specified character
	 *
	 * @param target The character the grudge is against
	 * @param grudge The type of grudge
	 */
	public void addGrudge(Character target, Trait grudge) {
		grudges.put(target, grudge);
	}

	/**
	 * Clears the grudge against the specified character
	 *
	 * @param target The character to remove the grudge against
	 */
	public void clearGrudge(Character target) {
		grudges.remove(target);
	}

	/**
	 * Adds the specified character to the mercy list, preventing further attacks until this character has resupplied
	 *
	 * @param victor The character who won the fight
	 */
	public void defeated(Character victor) {
		mercy.add(victor);
	}

	/**
	 * Run resupply logic
	 */
	public void resupply() {
		// Award scores to characters who defeated this one
		for (var victor : mercy) {
			victor.bounty(has(Trait.event) ? 5 : 1);
		}

		// Clear mercy list and get fresh clothes
		mercy.clear();
		change(Scheduler.getMatch().condition);
		state = State.ready;
		if (location().present.size() <= 1) {
			return;
		}

		// Attempt to move to safe location
		if (location().id() == Movement.dorm) {
			if (Scheduler.getMatch().gps(Movement.quad).present.isEmpty()) {
				if (human()) {
					Global.gui().message(
							"You hear your opponents searching around the dorm, so once you finish changing, you hop out the window and head to the quad.");
				}
				travel(Scheduler.getMatch().gps(Movement.quad));
			}
			else {
				if (human()) {
					Global.gui().message(
							"You hear your opponents searching around the dorm, so once you finish changing, you quietly move downstairs to the laundry room.");
				}
				travel(Scheduler.getMatch().gps(Movement.laundry));
			}
		}
		else if (location().id() == Movement.union) {
			if (Scheduler.getMatch().gps(Movement.union).present.isEmpty()) {
				if (human()) {
					Global.gui().message(
							"You don't want to be ambushed leaving the student union, so once you finish changing, you hop out the window and head to the quad.");
				}
				travel(Scheduler.getMatch().gps(Movement.quad));
			}
			else {
				if (human()) {
					Global.gui().message(
							"You don't want to be ambushed leaving the student union, so once you finish changing, you sneak out the back door and head to the pool.");
				}
				travel(Scheduler.getMatch().gps(Movement.pool));
			}
		}
	}

	/**
	 * Run match cleanup logic
	 */
	public void finishMatch() {
		// Award scores to characters who defeated this one
		for (var victor : mercy) {
			victor.bounty(has(Trait.event) ? 5 : 1);
		}

		// Remove match-only items and clear data tracking the match
		inventory.entrySet().removeIf(item -> item.getKey().getClass() == Envelope.class);
		grudges.clear();
		mercy.clear();
		matchmod = null;
		rest();
	}

	/**
	 * Places the character at the specified location
	 *
	 * @param loc The area to place the character at
	 */
	public void place(Area loc) throws IllegalStateException {
		location = loc;
		loc.present.add(this);
	}

	/**
	 * Rests the character, resetting their stats
	 */
	public void rest() {
		change(Modifier.normal);
		clearStatus();
		for (var resource : pools.values()) {
			resource.empty();
		}
		getStamina().fill();
	}

	/**
	 * Awards a match bounty to the character
	 *
	 * @param points The number of points to award
	 */
	public void bounty(int points) {
		Scheduler.getMatch().score(this, points);
	}

	/**
	 * Checks whether the specified character is an eligible opponent
	 *
	 * @param opponent The character to check
	 */
	public boolean eligible(Character opponent) {
		return (!mercy.contains(opponent)) && state != State.resupplying;
	}

	/**
	 * Sets the character's underwear to the specified item
	 *
	 * @param panties The item
	 */
	public void setUnderwear(Item panties) {
		underwear = panties;
	}

	/**
	 * Gets the character's assigned underwear
	 */
	public Item getUnderwear() {
		return underwear;
	}

	/**
	 * Refills stamina and clears status
	 */
	public void bathe() {
		clearStatus();
		getStamina().fill();
		state = State.ready;
	}

	/**
	 * Empties arousal
	 */
	public void masturbate() {
		getArousal().empty();
		state = State.ready;
	}

	/**
	 * Attempts to craft items from resources lying around
	 */
	public void craft() {
		var roll = Global.random(10);
		if (check(Attribute.Cunning, 40)) {
			if (Global.checkFlag(Flag.Kat) && roll >= 7) {
				gain(Potion.Furlixir);
			}
			else {
				gain(Flask.PAphrodisiac);
			}
		}

		roll = Global.random(10);
		if (check(Attribute.Ninjutsu, 5)) {
			if (roll == 9) {
				gain(Consumable.needle);
				gain(Consumable.smoke);
				gain(Flask.Sedative);
			}
			else if (roll >= 7) {
				gain(Flask.Sedative);
			}
			else if (roll >= 5) {
				gain(Consumable.smoke);
			}
			else if (roll >= 3) {
				gain(Consumable.needle);
			}
		}

		roll = Global.random(10);
		if (check(Attribute.Science, 20)) {
			if (roll == 9) {
				gain(Potion.Fox);
				gain(Potion.Nymph);
				gain(Potion.Bull);
			}
			else if (roll >= 7) {
				gain(Potion.Cat);
			}
			else if (roll >= 5) {
				gain(Potion.Fox);
			}
			else if (roll >= 3) {
				gain(Potion.Nymph);
			}
			else if (roll >= 1) {
				gain(Potion.Bull);
			}
			else {
				gain(Potion.SuperEnergyDrink);
			}
		}
		else if (check(Attribute.Science, 5)) {
			if (roll == 9) {
				gain(Flask.Aphrodisiac, 2);
				gain(Flask.DisSol);
				gain(Potion.SuperEnergyDrink);
			}
			else if (roll >= 7) {
				gain(Flask.Aphrodisiac);
				gain(Flask.DisSol);
				gain(Potion.SuperEnergyDrink);
			}
			else if (roll >= 5) {
				gain(Flask.Lubricant, 3);
				gain(Potion.SuperEnergyDrink);
			}
			else if (roll >= 3) {
				gain(Flask.Lubricant);
				gain(Flask.Sedative);
				gain(Potion.SuperEnergyDrink);
			}
			else {
				gain(Potion.SuperEnergyDrink);
			}
		}
		else if (check(Attribute.Cunning, 25)) {
			if (roll == 9) {
				gain(Flask.Aphrodisiac);
				gain(Flask.DisSol);
			}
			else if (roll >= 5) {
				gain(Flask.Aphrodisiac);
			}
			else {
				gain(Flask.Lubricant);
				gain(Flask.Sedative);
			}
		}
		else if (check(Attribute.Cunning, 20)) {
			if (roll == 9) {
				gain(Flask.Aphrodisiac);
			}
			else if (roll >= 7) {
				gain(Flask.DisSol);
			}
			else if (roll >= 5) {
				gain(Flask.Lubricant);
			}
			else if (roll >= 3) {
				gain(Flask.Sedative);
			}
			else {
				gain(Potion.EnergyDrink);
			}
		}
		else if (check(Attribute.Cunning, 15)) {
			if (roll == 9) {
				gain(Flask.Aphrodisiac);
			}
			else if (roll >= 8) {
				gain(Flask.DisSol);
			}
			else if (roll == 7) {
				gain(Flask.Lubricant);
			}
			else if (roll == 6) {
				gain(Potion.EnergyDrink);
			}
			else if (human()) {
				Global.gui().message(
						"Your concoction turns a sickly color and releases a foul smelling smoke. You trash it before you do any more damage.");
			}
		}
		else {
			if (roll >= 7) {
				gain(Flask.Lubricant);
			}
			else if (roll >= 5) {
				gain(Flask.Sedative);
			}
			else if (human()) {
				Global.gui().message(
						"Your concoction turns a sickly color and releases a foul smelling smoke. You trash it before you do any more damage.");
			}
		}
		state = State.ready;
	}

	/**
	 * Searches the area for useful items
	 */
	public void search() {
		switch (Global.random(10)) {
			case 9:
				gain(Component.Tripwire);
				gain(Component.Tripwire);
				break;
			case 8:
				gain(Consumable.ZipTie);
				gain(Consumable.ZipTie);
				gain(Consumable.ZipTie);
				break;
			case 7:
				gain(Component.Phone);
				break;
			case 6:
				gain(Component.Rope);
				break;
			case 5:
				gain(Component.Spring);
				break;
			default:
				if (human()) {
					Global.gui().message("You don't find anything useful");
				}
		}
		state = State.ready;
	}

	/**
	 * Delays the character for the specified number of turns
	 *
	 * @param turns The number of turns the character is occupied for
	 */
	public void delay(int turns) {
		busy += turns;
	}

	/**
	 * Allows the character to perform an opportunity attack after an opponent gets caught in a trap
	 *
	 * @param enc    A reference to the encounter
	 * @param target The character caught in the trap
	 * @param trap   The trap that caught the character
	 */
	public abstract void promptTrap(Encounter enc, Character target, Trap trap);

	/**
	 * Gets the XP bonus based on the opponent's level
	 *
	 * @param opponent The opponent to get the bonus for
	 */
	public int lvlBonus(Character opponent) {
		if (opponent.getLevel() <= this.getLevel()) {
			return 0;
		}
		if (opponent.has(Trait.event)) {
			return 50;
		}
		return 5 * (opponent.getLevel() - this.getLevel());
	}

	/**
	 * Gets the attraction with the specified character
	 *
	 * @param other The character to get the attraction with
	 */
	public int getAttraction(Character other) {
		return Roster.getAttraction(id(), other.id());
	}

	/**
	 * Increases attraction with the specified character
	 *
	 * @param other     The character to gain attraction with
	 * @param magnitude The amount of attraction to add
	 */
	public void gainAttraction(Character other, int magnitude) {
		Roster.gainAttraction(id(), other.id(), magnitude);
	}

	/**
	 * Gets the affection with the specified character
	 *
	 * @param other The character to get the affection with
	 */
	public int getAffection(Character other) {
		return Roster.getAffection(id(), other.id());
	}

	/**
	 * Increases affection with the specified character
	 *
	 * @param other     The character to gain affection with
	 * @param magnitude The amount of affection to add
	 */
	public void gainAffection(Character other, int magnitude) {
		Roster.gainAffection(id(), other.id(), magnitude);
	}

	/**
	 * Gets a random character with a positive attraction
	 *
	 * @return A character with a positive attraction, or Null if none was found
	 */
	public Character getRandomFriend() {
		var friends = new ArrayList<Character>();
		Roster.getExisting().stream()
				.filter(person -> !person.human() && Roster.getAttraction(id(), person.id()) > 0)
				.forEachOrdered(friends::add);

		return friends.isEmpty()
			   ? null
			   : friends.get(Global.random(friends.size()));
	}

	/**
	 * Gets the character's AC
	 */
	public int ac() {
		if (!canAct()) {
			return -999;
		}

		var ac = (getEffective(Attribute.Cunning) / 4) + 15;
		for (var s : status) {
			ac += s.evade();
		}
		if (has(Trait.clairvoyance)) {
			ac += 1;
		}
		if (has(Trait.untouchable)) {
			ac += 5;
		}
		if (has(Trait.speeddemon)) {
			ac += 5;
		}
		return ac;
	}

	/**
	 * Calculates the character's bonus to countering an attack that failed to hit
	 */
	public int bonusCounter() {
		var counter = 0;
		for (var s : status) {
			counter += s.counter();
		}
		if (has(Trait.clairvoyance)) {
			counter += 3;
		}
		if (has(Trait.aikidoNovice)) {
			counter += 3;
		}
		return counter;
	}

	/**
	 * Calculates to-hit bonus based on attributes
	 */
	public int bonusToHit() {
		return (getEffective(Attribute.Speed) / 2) + getEffective(Attribute.Perception);
	}

	/**
	 * Calculates DC required to knock the character down
	 */
	public int knockdownDC() {
		var dc = getStamina().get() / 4;
		if (is(Stsflag.braced)) {
			dc += getStatus(Stsflag.braced).value();
		}
		if (getPure(Attribute.Discipline) >= 9 && is(Stsflag.composed)) {
			dc += getStatus(Stsflag.composed).mag();
		}
		if (has(Trait.landsonfeet)) {
			dc += 50;
		}
		return dc;
	}

	/**
	 * Performs a counterattack against the specified character
	 *
	 * @param target The target of the counter
	 * @param type   The type of attack that is being countered
	 * @param c      A reference to the ongoing combat
	 */
	public abstract void counterattack(Character target, Tactics type, Combat c);

	/**
	 * Clears all active status
	 */
	public void clearStatus() {
		status.clear();
	}

	/**
	 * Gets a status with the specified flag
	 *
	 * @param flag The status flag to search for
	 * @return A status with the specified flag, or Null if none was found
	 */
	public Status getStatus(Stsflag flag) {
		for (var s : status) {
			if (s.flags().contains(flag)) {
				return s;
			}
		}
		return null;
	}

	/**
	 * Gets the magnitude of the specified status
	 *
	 * @param name The name of the status
	 * @return The magnitude of the status, or 0 if none was found
	 */
	public int getStatusMagnitude(String name) {
		for (var s : status) {
			if (s.toString().equalsIgnoreCase(name)) {
				return s.mag();
			}
		}
		return 0;
	}

	/**
	 * Checks whether the character can read basic information about the specified character
	 *
	 * @param target The character to check against
	 */
	public boolean canReadBasic(Character target) {
		if (getEffective(Attribute.Perception) <= 3) {
			return false;
		}

		return target == this
				|| !target.is(Stsflag.unreadable)
				|| getEffective(Attribute.Perception) > 6;
	}

	/**
	 * Gets a basic description of the inflicted arousal damage
	 *
	 * @param magnitude The amount of damage
	 */
	public String getBasicArousalDamage(int magnitude) {
		var percentage = magnitude * 1.0 / getArousal().max();
		if (percentage > .25) {
			return "extreme";
		}
		else if (percentage > .15) {
			return "intense";
		}
		else if (percentage > .1) {
			return "strong";
		}
		else if (percentage > .05) {
			return "moderate";
		}
		else if (percentage > .02) {
			return "a little";
		}
		else {
			return "minimal";
		}
	}

	/**
	 * Gets a basic description of the inflicted pain damage
	 *
	 * @param magnitude The amount of damage
	 */
	public String getBasicPain(int magnitude) {
		var percentage = magnitude * 1.0 / getStamina().max();
		if (percentage > .25) {
			return "severe";
		}
		else if (percentage > .15) {
			return "intense";
		}
		else if (percentage > .1) {
			return "major";
		}
		else if (percentage > .05) {
			return "manageable";
		}
		else if (percentage > .02) {
			return "small";
		}
		else {
			return "negligible";
		}
	}

	/**
	 * Gets a basic description of the inflicted weaken damage
	 *
	 * @param magnitude The amount of damage
	 */
	public String getBasicWeaken(int magnitude) {
		var percentage = magnitude * 1.0 / getStamina().max();
		if (percentage > .25) {
			return "severely";
		}
		else if (percentage > .15) {
			return "heavily";
		}
		else if (percentage > .1) {
			return "significantly";
		}
		else if (percentage > .05) {
			return "moderately";
		}
		else if (percentage > .02) {
			return "slightly";
		}
		else {
			return "minimally";
		}
	}

	/**
	 * Gets a basic description of the inflicted calming
	 *
	 * @param magnitude The amount of damage
	 */
	public String getBasicCalm(int magnitude) {
		var percentage = magnitude * 1.0 / getArousal().max();
		if (percentage > .7) {
			return "completely";
		}
		else if (percentage > .4) {
			return "a lot";
		}
		else if (percentage > .2) {
			return "significantly";
		}
		else if (percentage > .1) {
			return "moderately";
		}
		else if (percentage > .03) {
			return "slightly";
		}
		else {
			return "minimally";
		}
	}

	/**
	 * Gets a basic description of the inflicted healing
	 *
	 * @param magnitude The amount of damage
	 */
	public String getBasicHeal(int magnitude) {
		var percentage = magnitude * 1.0 / getStamina().max();
		if (percentage > .7) {
			return "completely";
		}
		else if (percentage > .4) {
			return "a lot";
		}
		else if (percentage > .2) {
			return "significantly";
		}
		else if (percentage > .1) {
			return "moderately";
		}
		else if (percentage > .03) {
			return "slightly";
		}
		else {
			return "minimally";
		}
	}

	/**
	 * Checks whether the character can read advanced information about the specified character
	 *
	 * @param target The character to check against
	 */
	public boolean canReadAdvanced(Character target) {
		return (target == this && getEffective(Attribute.Perception) > 3)
				|| (!target.is(Stsflag.unreadable) && getEffective(Attribute.Perception) > 6);
	}

	/**
	 * Calculates the prize money for beating the character
	 */
	public int prize() {
		return 50 + (100 * ((int) Math.pow(getRank(), 2)));
	}

	/**
	 * Updates the specified emotion
	 *
	 * @param emotion The emotion to update
	 * @param amount  The amount to increase the emotion by
	 */
	public void emote(Emotion emotion, int amount) {
		// Increase emotion
		emotes.put(emotion, Math.max(Math.min(emotes.get(emotion) + amount, 100), 0));

		// Decrease inverse emotion
		var inverse = emotion.inverse();
		emotes.put(inverse, Math.min(Math.max(emotes.get(inverse) - amount / 2, 0), 100));
	}

	/**
	 * Gets the primary mood of the character
	 */
	public Emotion getMood() {
		return mood;
	}

	/**
	 * Decays emotions that no longer apply
	 */
	public void decayMood() {
		for (var emotion : emotes.keySet()) {
			if (emotion == Emotion.nervous && nude() && !has(Trait.exhibitionist) && !has(Trait.shameless)) {
				emotes.put(emotion, emotes.get(emotion) - ((emotes.get(emotion) - 50) / 2));
			}
			else if (emotion == Emotion.horny && nude() && has(Trait.exhibitionist)) {
				emotes.put(emotion, emotes.get(emotion) - ((emotes.get(emotion) - 50) / 2));
			}
			else if (emotion == Emotion.confident && !nude()) {
				emotes.put(emotion, emotes.get(emotion) - ((emotes.get(emotion) - 50) / 2));
			}
			else if (emotes.get(emotion) >= 5) {
				emotes.put(emotion, emotes.get(emotion) - (emotes.get(emotion) / 2));
			}
		}
	}

	/**
	 * Attempts to find a path to the target area
	 *
	 * @param target The area to move towards
	 * @return A Move action that will lead towards the area, or Null if none was found
	 */
	public Move findPath(Area target) {
		if (this.location.name.equals(target.name)) {
			return null;
		}
		var queue = new ArrayDeque<Area>();
		var vector = new Vector<Area>();
		var parents = new HashMap<Area, Area>();
		queue.push(this.location);
		vector.add(this.location);
		Area last = null;
		while (!queue.isEmpty()) {
			var possibleArea = queue.pop();
			parents.put(possibleArea, last);
			if (possibleArea.name.equals(target.name)) {
				while (!this.location.adjacent.contains(possibleArea)) {
					possibleArea = parents.get(possibleArea);
				}
				return new Move(possibleArea);
			}
			for (var area : possibleArea.adjacent) {
				if (!vector.contains(area)) {
					vector.add(area);
					queue.push(area);
				}
			}
			last = possibleArea;
		}
		return null;
	}

	/**
	 * Checks whether the character knows the specified skill
	 *
	 * @param skill The skill to check
	 */
	public boolean knows(Skill skill) {
		return skills.stream()
				.anyMatch(s -> s.equals(skill));
	}

	/**
	 * Perform pre-combat preparation
	 *
	 * @param opponent The character to fight against
	 * @param c        A reference to the ongoing combat
	 */
	public void preCombat(Character opponent, Combat c) {
		if (has(Trait.perfectplan)) {
			opponent.nudify();
		}
		if (has(Trait.ninjapreparation)) {
			opponent.add(new Bound(opponent, 30, "net"));
			opponent.add(new Horny(opponent, 3, 4), c);
			opponent.add(new Drowsy(opponent, 4), c);
		}
		if (has(Trait.sadisticmood)) {
			opponent.add(new Masochistic(opponent));
		}
		if (has(Trait.enthralling)) {
			opponent.add(new Enthralled(opponent, this));
		}
		if (has(Trait.sparehandcuffs)) {
			gain(Consumable.Handcuffs);
			gain(Consumable.Handcuffs);
		}
		if (has(Trait.defensivemeasures)) {
			replaceArticle(Clothing.cup);
		}
		if (has(Trait.confidentdom)) {
			emote(Emotion.dominant, 50);
		}
		if (getPure(Attribute.Discipline) >= 1) {
			add(new Composed(this, 5 + (getEffective(Attribute.Discipline) / 2), getEffective(Attribute.Discipline)));
		}
		if (has(Trait.cheapshot)) {
			opponent.weaken(opponent.getStamina().get());
		}
	}

	/**
	 * Perform end-of-battle cleanup
	 */
	public void endOfBattle() {
		var copy = new ArrayList<>(status);
		for (var s : copy) {
			if (!s.lingering()) {
				removeStatus(s);
			}
		}

		if (pet != null) {
			pet.remove();
		}
		activeGrudge = null;
	}

	/**
	 * Checks whether the character can spend the specified amount of mojo
	 *
	 * @param baseCost The base mojo cost of the action
	 */
	public boolean canSpend(int baseCost) {
		return canSpend(Pool.MOJO, baseCost);
	}

	/**
	 * Checks whether the character can spend the specified amount of a resource
	 *
	 * @param res      The resource pool that would be used
	 * @param baseCost The base cost of the action
	 */
	public boolean canSpend(Pool res, int baseCost) {
		var cost = baseCost;

		for (var s : status) {
			cost += s.spend(res, baseCost);
		}
		if (res == Pool.MOJO && has(Trait.overflowingmana)) {
			cost = cost / 20;
		}
		return getMojo().get() >= cost;
	}

	/**
	 * Gets the character's inventory
	 */
	public HashMap<Item, Integer> getInventory() {
		return inventory;
	}

	/**
	 * Gets the status affecting the character
	 *
	 * @return A non-modifiable list of status affecting the character
	 */
	public ArrayList<Status> getStatus() {
		return new ArrayList<>(status);
	}

	/**
	 * Gets a list of all status affecting the character
	 */
	public ArrayList<String> listStatus() {
		var result = new ArrayList<String>();
		for (var s : status) {
			result.add(s.toString());
		}
		return result;
	}

	/**
	 * Gets a message string containing the character's attributes and stats
	 */
	public String dumpStats() {
		var statMessage = new StringBuilder(name() + ": Level " + getLevel() + "; ");
		for (var attribute : att.keySet()) {
			statMessage.append(attribute.name())
					.append(" ")
					.append(att.get(attribute))
					.append(", ");
		}
		statMessage.append("<br>Max Stamina ")
				.append(getStamina().max())
				.append(", Max Arousal ")
				.append(getArousal().max())
				.append(", Max Mojo ")
				.append(getMojo().max())
				.append(", Affection ")
				.append(getAffection(Global.getPlayer()))
				.append(".<p>");
		return statMessage.toString();
	}

	/**
	 * Accepts the specified challenge
	 *
	 * @param challenge The accepted challenge
	 */
	public void accept(Challenge challenge) {
		challenges.add(challenge);
	}

	/**
	 * Evaluates whether the character has successfully completed any accepted challenges
	 *
	 * @param c      A reference to the ongoing combat
	 * @param victor The character that won the fight
	 */
	public void evalChallenges(Combat c, Character victor) {
		for (var challenge : challenges) {
			challenge.check(c, victor);
		}
	}

	/**
	 * Calculate how urgently the character wants the fight to end
	 */
	public float getUrgency() {
		// How urgent do we want the fight to end? We are
		// essentially trying to approximate how many
		// moves we will still have until the end of the
		// fight.

		// Once arousal is too high, the fight ends.
		float remArousal = getArousal().max() - getArousal().get();

		// Arousal is worse the more clothes we lose, because
		// we will probably lose it quicker.
		float arousalPerRound = 5;
		for (var i = 0; i < top.size(); i++) {
			arousalPerRound /= 1.2;
		}
		for (var i = 0; i < bottom.size(); i++) {
			arousalPerRound /= 1.2;
		}

		// Depending on our stamina, we should count on
		// spending a few of these rounds knocked down,
		// so effectively we have even less rounds to
		// work with - let's make them count!
		var fitness = Math.max(0.5f, Math.min(1.0f, getStamina().get() / 40F));

		return remArousal / arousalPerRound * fitness;
	}

	/**
	 * Calculate fitness to determine how confident the NPC is in winning
	 *
	 * @param urgency  How urgent the character wants the fight to end
	 * @param position The current position of the character
	 */
	public float getFitness(float urgency, Position position) {
		float fit = 0;
		// Urgency marks
		var ushort = rateUrgency(urgency, 5);
		var umid = rateUrgency(urgency, 15);
		var ulong = rateUrgency(urgency, 25);
		var usum = ushort + umid + ulong;
		// Always important: Position
		if (position.dom(this)) {
			fit += 2;
		}
		if (position.sub(this)) {
			fit -= 2;
		}
		if (position.reachTop(this)) {
			fit += 1;
		}
		if (position.reachBottom(this)) {
			fit += 1;
		}
		if (position.mobile(this)) {
			fit += 2;
		}
		if (position.penetration(this) && position.dom(this)) {
			fit += 5;
		}
		if (position.behind(this)) {
			fit += 1;
		}
		// Also important: Clothing.
		// Especially when aroused (protects vs KO)
		fit += ushort * 3 * (top.size() + bottom.size());
		// Also somewhat of a factor: Inventory (so we don't
		// just use it without thinking)
		for (var item : inventory.keySet()) {
			fit += (float) item.getPrice() / 10;
		}
		// Extreme situations
		if (getArousal().isFull()) {
			fit -= 99;
		}
		if (getStamina().isEmpty()) {
			fit -= umid * 3;
		}
		// Short-term: Arousal
		fit += ushort / usum * (getArousal().max() - getArousal().get());
		// Mid-term: Stamina
		fit += umid / usum * getStamina().get();
		// Long term: Mojo
		fit += ulong / usum * getMojo().get();
		// TODO: Pets, status effects...
		return fit;
	}

	/**
	 * Gets the path to the character's portrait image
	 *
	 * @return A string containing the path to an image, or Null if none was found
	 */
	public abstract String getPortrait();

	/**
	 * Gets the subject pronoun for the character's gender
	 *
	 * @param capital Whether the first letter should be capitalized
	 */
	public String pronounSubject(boolean capital) {
		if (capital) {
			return has(Trait.male) ? "He" : "She";
		}
		return has(Trait.male) ? "he" : "she";
	}

	/**
	 * Gets the target pronoun for the character's gender
	 *
	 * @param capital Whether the first letter should be capitalized
	 */
	public String pronounTarget(boolean capital) {
		if (capital) {
			return has(Trait.male) ? "Him" : "Her";
		}
		return has(Trait.male) ? "him" : "her";
	}

	/**
	 * Gets the possessive for the character's gender
	 *
	 * @param capital Whether the first letter should be capitalized
	 */
	public String possessive(boolean capital) {
		if (capital) {
			return has(Trait.male) ? "His" : "Her";
		}
		return has(Trait.male) ? "his" : "her";
	}

	/**
	 * Prepares for the upcoming match
	 *
	 * @param match A reference to the ongoing match
	 */
	public void matchPrep(Match match) {
		// Refresh stats and clothing
		rest();
		change(match.condition);
		state = State.ready;
		Global.gainSkills(this);

		// Process attributes and traits
		if (getPure(Attribute.Ninjutsu) >= 9) {
			placeStash(match);
		}
		if (has(Trait.challengeSeeker)) {
			new Challenge(match).resolve(this);
		}
		if (getPure(Attribute.Science) >= 1) {
			getPool(Pool.BATTERY).fill();
		}

		// Process match conditions
		applyMatchConditionStatus(match);
	}

	/**
	 * Applies status related to the ongoing match to the player
	 *
	 * @param match A reference to the ongoing match
	 */
	protected void applyMatchConditionStatus(Match match) {
		if (match.condition == Modifier.slippery && human() && !is(Stsflag.oiled)) {
			Status s = new Oiled(this);
			s.fix();
			add(s);
		}
		if (match.condition == Modifier.furry && !human() && !is(Stsflag.beastform)) {
			Status s = new Beastform(this);
			s.fix();
			add(s);
		}
	}

	/**
	 * Places a stash for the character
	 *
	 * @param match A reference to the ongoing match
	 */
	private void placeStash(Match match) {
		Movement location;
		switch (Global.random(6)) {
			case 0:
				location = Movement.library;
				break;
			case 1:
				location = Movement.dining;
				break;
			case 2:
				location = Movement.lab;
				break;
			case 3:
				location = Movement.workshop;
				break;
			case 4:
				location = Movement.storage;
				break;
			default:
				location = Movement.la;
				break;
		}
		var loc = match.gps(location);
		loc.place(new NinjaStash(this));
		if (human()) {
			Global.gui().message("<b>You've arranged for a hidden stash to be placed in the " + loc + ".</b>");
		}
	}

	/**
	 * Gets an identifier of the character's costume set
	 */
	protected int getCostumeSet() {
		return 1;
	}

	/**
	 * Gets a buffered image of the full character sprite
	 *
	 * @return The buffered image, or Null if no body sprite was found
	 */
	public BufferedImage getSpriteImage() {
		var costume = getCostumeSet();
		var res = "";
		if (Global.gui().lowRes()) {
			res = "_low";
		}

		if (spriteBody == null) {
			spriteBody = loadImage("assets/" + name + "/" + name + getCostumeSet() + "_Sprite" + res + ".png");
			if (spriteBody == null) {
				spriteBody = loadImage("assets/" + name + "/" + name + "_Sprite" + res + ".png");
			}
		}
		if (spriteBody == null) {
			return null;
		}

		var sprite = new BufferedImage(spriteBody.getWidth(), spriteBody.getHeight(), spriteBody.getType());
		var graphics = sprite.createGraphics();

		// Clothes drawn behind the body sprite
		if (isWearing(ClothingType.TOPOUTER)) {
			if (spriteCoattail == null) {
				spriteCoattail = loadImage("assets/" + name + "/" + name + costume + "_outerback" + res + ".png");
			}
			graphics.drawImage(spriteCoattail, 0, 0, null);
		}
		if (isWearing(ClothingType.TOP)) {
			if (spriteShirttail == null) {
				spriteShirttail = loadImage("assets/" + name + "/" + name + costume + "_topback" + res + ".png");
			}
			graphics.drawImage(spriteShirttail, 0, 0, null);
		}

		graphics.drawImage(spriteBody, 0, 0, null);
		graphics.drawImage(loadImage("assets/" + name + "/" + name + "_" + mood.name() + res + ".png"), 0, 0, null);

		if (getArousal().percent() >= 75) {
			if (blushHigh == null) {
				blushHigh = loadImage("assets/" + name + "/" + name + "_blush3" + res + ".png");
			}
			graphics.drawImage(blushHigh, 0, 0, null);
		}
		else if (getArousal().percent() >= 50) {
			if (blushMed == null) {
				blushMed = loadImage("assets/" + name + "/" + name + "_blush2" + res + ".png");
			}
			graphics.drawImage(blushMed, 0, 0, null);
		}
		else if (getArousal().percent() >= 25) {
			if (blushLow == null) {
				blushLow = loadImage("assets/" + name + "/" + name + "_blush1" + res + ".png");
			}
			graphics.drawImage(blushLow, 0, 0, null);
		}

		if (spriteAccessory == null) {
			spriteAccessory = loadImage("assets/" + name + "/" + name + costume + "_accessory" + res + ".png");
		}
		graphics.drawImage(spriteAccessory, 0, 0, null);

		if (is(Stsflag.beastform)) {
			if (spriteKemono == null) {
				spriteKemono = loadImage("assets/" + name + "/" + name + getCostumeSet() + "_kemono" + res + ".png");
			}

			if (spriteKemono == null) {
				spriteKemono = loadImage("assets/" + name + "/" + name + "_kemono" + res + ".png");
			}
			graphics.drawImage(spriteKemono, 0, 0, null);
		}
		else {
			if (spriteAccessory2 == null) {
				spriteAccessory2 = loadImage("assets/" + name + "/" + name + costume + "_accessory2" + res + ".png");
			}
			graphics.drawImage(spriteAccessory2, 0, 0, null);
		}

		if (isWearing(ClothingType.UNDERWEAR)) {
			if (has(Trait.strapped)) {
				if (spriteStrapon == null) {
					spriteStrapon = loadImage("assets/" + name + "/" + name + costume + "_strapon" + res + ".png");
					if (spriteStrapon == null) {
						spriteStrapon = loadImage("assets/" + name + "/" + name + "_strapon" + res + ".png");
					}
				}
				graphics.drawImage(spriteStrapon, 0, 0, null);
			}
			else {
				if (spriteUnderwear == null) {
					spriteUnderwear = loadImage("assets/" + name + "/" + name + costume + "_underwear" + res + ".png");
				}
				graphics.drawImage(spriteUnderwear, 0, 0, null);
			}
		}

		if (isWearing(ClothingType.TOPUNDER)) {
			if (spriteBra == null) {
				spriteBra = loadImage("assets/" + name + "/" + name + costume + "_bra" + res + ".png");
			}
			graphics.drawImage(spriteBra, 0, 0, null);
		}

		if (isWearing(ClothingType.BOTOUTER)) {
			if (spriteBottom == null) {
				spriteBottom = loadImage("assets/" + name + "/" + name + costume + "_bottom" + res + ".png");
			}
			graphics.drawImage(spriteBottom, 0, 0, null);
		}

		if (isWearing(ClothingType.TOP)) {
			if (spriteTop == null) {
				spriteTop = loadImage("assets/" + name + "/" + name + costume + "_top" + res + ".png");
			}
			graphics.drawImage(spriteTop, 0, 0, null);
		}

		if (isWearing(ClothingType.TOPOUTER)) {
			if (spriteOuter == null) {
				spriteOuter = loadImage("assets/" + name + "/" + name + costume + "_outer" + res + ".png");
			}
			graphics.drawImage(spriteOuter, 0, 0, null);
		}

		if (hasDick() && !isWearing(ClothingType.BOTOUTER)) {
			if (isErect()) {
				if (spritePenisHard == null) {
					spritePenisHard = loadImage("assets/" + name + "/" + name + "_penis_hard" + res + ".png");
				}
				graphics.drawImage(spritePenisHard, 0, 0, null);
			}
			else {
				if (spritePenisSoft == null) {
					spritePenisSoft = loadImage("assets/" + name + "/" + name + "_penis_soft" + res + ".png");
				}
				graphics.drawImage(spritePenisSoft, 0, 0, null);
			}
		}
		if (Global.checkFlag(Flag.statussprites)) {
			var sts = Global.gui().loadStatusFilters(this);
			if (sts != null) {
				graphics.drawImage(sts, 0, 0, null);
			}
		}

		graphics.dispose();
		return sprite;
	}

	/**
	 * Clears cached sprites for the character
	 */
	public void clearSpriteImages() {
		spriteBody = null;
		spriteAccessory = null;
		spriteUnderwear = null;
		spriteStrapon = null;
		spriteBra = null;
		spriteBottom = null;
		spriteTop = null;
		spriteOuter = null;
		spriteCoattail = null;
		spritePenisHard = null;
		spritePenisSoft = null;
		blushLow = null;
		blushMed = null;
		blushHigh = null;
	}

	/**
	 * Loads an image from the specified resource path
	 *
	 * @param path The path to the image resource
	 * @return The loaded image, or Null if none was found, or the image failed to load
	 */
	protected BufferedImage loadImage(String path) {
		var url = Global.gui().getClass().getResource(path);
		if (url == null) {
			return null;
		}

		try {
			return ImageIO.read(url);
		}
		catch (IOException ignored) {
			return null;
		}
	}

	/**
	 * Gets the total number of points spent on advanced attributes
	 */
	private int getAdvanced() {
		var total = 0;
		for (var attribute : att.keySet()) {
			if (attribute.getType() == AttributeType.Advanced) {
				total += att.get(attribute);
			}
		}
		return total;
	}

	private float rateUrgency(float urgency, float urgency2) {
		// How important is it to reach a goal with given
		// urgency, given that we are gunning roughly for
		// the second urgency? We use a normal distribution
		// (hah, fancy!) that gives roughly 50% weight at
		// a difference of 15.
		var x = (urgency - urgency2) / 15;
		return (float) Math.exp(-x * x);
	}
}
