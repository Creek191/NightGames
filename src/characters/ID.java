package characters;

/**
 * Defines the IDs of all implemented characters
 */
public enum ID {
	// Match participants
	PLAYER,
	CASSIE,
	MARA,
	ANGEL,
	JEWEL,
	YUI,
	KAT,
	REYKA,
	EVE,
	SAMANTHA,
	MAYA,
	VALERIE,
	SOFIA,
	SELENE,
	// Other characters
	JUSTINE,
	AESOP,
	LILLY,
	AISHA,
	SUZUME,
	JETT,
	RIN,
	ALICE,
	GINETTE,
	CAROLINE,
	SARAH,
	MEI,
	CUSTOM1,
	CUSTOM2,
	CUSTOM3,
	CUSTOM4,
	CUSTOM5;

	/**
	 * Gets the id of the character with the specified name
	 * <p>
	 * Ignores characters that can't take part in a match
	 *
	 * @param name The name to check
	 * @return The ID of the matching character, or the ID of the player if none was found
	 */
	public static ID fromString(String name) {
		if (name.equalsIgnoreCase("Cassie")) {
			return CASSIE;
		}
		else if (name.equalsIgnoreCase("Mara")) {
			return MARA;
		}
		else if (name.equalsIgnoreCase("Angel")) {
			return ANGEL;
		}
		else if (name.equalsIgnoreCase("Jewel")) {
			return JEWEL;
		}
		else if (name.equalsIgnoreCase("Yui")) {
			return YUI;
		}
		else if (name.equalsIgnoreCase("Kat")) {
			return KAT;
		}
		else if (name.equalsIgnoreCase("Reyka")) {
			return REYKA;
		}
		else if (name.equalsIgnoreCase("Eve")) {
			return EVE;
		}
		else if (name.equalsIgnoreCase("Samantha")) {
			return SAMANTHA;
		}
		else if (name.equalsIgnoreCase("Maya")) {
			return MAYA;
		}
		else if (name.equalsIgnoreCase("Valerie")) {
			return VALERIE;
		}
		else if (name.equalsIgnoreCase("Sofia")) {
			return SOFIA;
		}
		else if (name.equalsIgnoreCase("Selene")) {
			return SELENE;
		}
		else {
			return PLAYER;
		}
	}
}
