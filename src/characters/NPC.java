package characters;

import Comments.CommentSituation;
import actions.*;
import areas.Area;
import combat.Combat;
import combat.Encounter;
import combat.Tag;
import daytime.Daytime;
import global.*;
import items.Clothing;
import items.Consumable;
import items.Flask;
import skills.Skill;
import skills.Tactics;
import stance.Behind;
import status.Enthralled;
import status.Feral;
import status.Horny;
import status.Stsflag;
import trap.Trap;

import java.time.LocalTime;
import java.util.*;

public class NPC extends Character {
	private final Personality ai;
	public Emotion plan;
	public Area goal;
	public HashMap<Emotion, Integer> strategy;
	public ArrayList<Class> preferredSkills;

	public NPC(String name, ID identity, int level, Personality ai) {
		super(name, level);
		this.identity = identity;
		this.ai = ai;
		att.put(Attribute.Power, Math.round(Constants.STARTINGPOWER * Global.getValue(Flag.NPCBaseStrength)));
		att.put(Attribute.Cunning, Math.round(Constants.STARTINGCUNNING * Global.getValue(Flag.NPCBaseStrength)));
		att.put(Attribute.Seduction, Math.round(Constants.STARTINGSEDUCTION * Global.getValue(Flag.NPCBaseStrength)));
		att.put(Attribute.Perception, Constants.STARTINGPERCEPTION);
		att.put(Attribute.Speed, Math.round(Constants.STARTINGSPEED * Global.getValue(Flag.NPCBaseStrength)));
		this.money = 0;
		getStamina().setMax(Math.round(Constants.STARTINGSTAMINA * Global.getValue(Flag.NPCBaseStrength)));
		getStamina().fill();
		getArousal().setMax(Math.round(Constants.STARTINGAROUSAL * Global.getValue(Flag.NPCBaseStrength)));
		getMojo().setMax(Math.round(Constants.STARTINGMOJO * Global.getValue(Flag.NPCBaseStrength)));
		strategy = new HashMap<>();
		strategy.put(Emotion.hunting, 3);
		strategy.put(Emotion.bored, 3);
		strategy.put(Emotion.sneaking, 3);
		preferredSkills = new ArrayList<>();
	}

	@Override
	public String describe(int perception) {
		var description = new StringBuilder(ai.describe());
		for (var s : status) {
			if (s.describe() != null && !s.describe().equals("")) {
				description.append("<br>").append(s.describe());
			}
		}

		description.append("<p>");
		if (top.empty() && bottom.empty()) {
			description.append("She is completely naked.<br>");
		}
		else {
			if (top.empty()) {
				description.append("She is topless and wearing ");
			}
			else {
				description.append("She is wearing ")
						.append(top.peek().pre())
						.append(top.peek().getName())
						.append(" and ");
			}
			if (bottom.empty()) {
				description.append("is naked from the waist down.<br>");
			}
			else {
				description.append(bottom.peek().pre())
						.append(bottom.peek().getName())
						.append(".<br>");
			}
		}
		description.append(observe(perception));
		return description.toString();
	}

	@Override
	public void victory(Combat c, Tag flag) {
		ai.victory(c, flag);
		var target = c.getOther(this);
		this.gainXP(15 + lvlBonus(target));
		target.gainXP(15 + target.lvlBonus(this));
		if (c.stance.penetration(this)) {
			getMojo().gainMax(has(Trait.mojoMaster) ? 4 : 2);
		}

		target.getArousal().empty();
		if (c.hasModifier(Modifier.practice)) {
			return;
		}

		undress(c);
		target.undress(c);
		if (c.underwearIntact(target)) {
			this.gain(target.getUnderwear());
		}
		dress(c);
		target.defeated(this);
		Roster.gainAttraction(this.id(), target.id(), target.has(Trait.gracefulloser) ? 4 : 2);
		plan = rethink();
		if (has(Trait.insatiable)) {
			getArousal().addPercent(20);
		}
	}

	@Override
	public void defeat(Combat c, Tag flag) {
		ai.defeat(c, flag);
		getArousal().empty();
		if (!c.hasModifier(Modifier.practice)) {
			plan = rethink();
		}
	}

	@Override
	public boolean resist3p(Combat combat, Character intruder, Character assist) {
		if (has(Trait.cursed)) {
			if (intruder.human() || assist.human()) {
				Global.gui().message(ai.resist3p(combat, intruder, assist));
			}
			return true;
		}
		return false;
	}

	@Override
	public void intervene3p(Combat c, Character target, Character assist) {
		this.gainXP(10 + lvlBonus(target));
		target.defeated(this);
		c.write(ai.intervene3p(c, target, assist));
		assist.gainAttraction(this, 1);
	}

	@Override
	public void victory3p(Combat c, Character target, Character assist) {
		this.gainXP(15 + lvlBonus(target));
		target.gainXP(15 + target.lvlBonus(this) + target.lvlBonus(assist));
		target.getArousal().empty();
		dress(c);
		target.undress(c);

		if (c.underwearIntact(target)) {
			this.gain(target.getUnderwear());
		}
		target.defeated(this);
		c.write(ai.victory3p(c, target, assist));
		gainAttraction(target, 1);
	}

	@Override
	public void watcher(Combat c, Character victor, Character defeated) {
		if (has(Trait.voyeurism)) {
			buildMojo(50);
		}
		gainAttraction(victor, 3);
	}

	@Override
	public void watched(Combat c, Character voyeur, Character defeated) {
		ai.watched(c, defeated, voyeur);
	}

	@Override
	public void act(Combat c) {
		var available = new HashSet<Skill>();
		Character target;
		if (c.p1 == this) {
			target = c.p2;
		}
		else {
			target = c.p1;
		}
		for (var act : skills) {
			if (act.usable(c, target) && c.isAllowed(act, this)) {
				available.add(act);
			}
		}
		for (var act : flaskskills) {
			if (act.usable(c, target) && c.isAllowed(act, this)) {
				available.add(act);
			}
		}
		c.act(this, ai.act(available, c));
	}

	/**
	 * Get the next action performed by the NPC during simulated combat
	 *
	 * @param c A reference to the ongoing combat
	 * @return The skill chosen by the NPC
	 */
	public Skill actFast(Combat c) {
		var available = new HashSet<Skill>();
		Character target;
		if (c.p1 == this) {
			target = c.p2;
		}
		else {
			target = c.p1;
		}
		for (var act : skills) {
			if (act.usable(c, target) && c.isAllowed(act, this)) {
				available.add(act);
			}
		}
		return ai.act(available, c);
	}

	@Override
	public boolean human() {
		return false;
	}

	@Override
	public void draw(Combat c, Tag flag) {
		Character target;
		if (c.p1 == this) {
			target = c.p2;
		}
		else {
			target = c.p1;
		}

		this.gainXP(15 + lvlBonus(target));
		target.gainXP(15 + target.lvlBonus(this));
		this.getArousal().empty();
		target.getArousal().empty();
		if (this.has(Trait.insatiable)) {
			this.getArousal().restore((int) (getArousal().max() * .2));
		}
		if (target.has(Trait.insatiable)) {
			target.getArousal().restore((int) (getArousal().max() * .2));
		}

		target.undress(c);
		this.undress(c);
		if (c.underwearIntact(this)) {
			target.gain(this.getUnderwear());
		}
		if (c.underwearIntact(target)) {
			this.gain(target.getUnderwear());
		}

		target.defeated(this);
		this.defeated(target);
		ai.draw(c, flag);
		Roster.gainAttraction(this.id(), target.id(), 4);
		if (getAffection(target) > 0) {
			var affectionate = this.has(Trait.affectionate) || target.has(Trait.affectionate);
			Roster.gainAffection(this.id(), target.id(), affectionate ? 2 : 1);
		}
		plan = rethink();
	}

	@Override
	public String bbLiner() {
		return ai.bbLiner();
	}

	@Override
	public String nakedLiner() {
		return ai.nakedLiner();
	}

	@Override
	public String stunLiner() {
		return ai.stunLiner();
	}

	@Override
	public String taunt() {
		return ai.taunt();
	}

	@Override
	public void detect() {
	}

	@Override
	public void move(Match match) {
		if (state == State.combat) {
			if (location.fight == null) {
				state = State.ready;
				move(match);
			}
			else {
				location.fight.battle();
			}
		}
		else if (busy > 0) {
			busy--;
		}
		else if (this.is(Stsflag.enthralled)) {
			var master = ((Enthralled) getStatus(Stsflag.enthralled)).master;
			var compelledMove = findPath(master.location);
			if (compelledMove != null) {
				location.leaveTrace(this, compelledMove.execute(this));
			}
		}
		else if (state == State.shower || state == State.lostclothes) {
			bathe();
		}
		else if (state == State.crafting) {
			craft();
		}
		else if (state == State.searching) {
			search();
		}
		else if (state == State.resupplying) {
			resupply();
		}
		else if (state == State.webbed) {
			state = State.ready;
		}
		else if (state == State.masturbating) {
			masturbate();
		}
		else {
			if (location.encounter(this)) {
				return;
			}

			var available = new HashSet<Action>();
			var radar = new HashSet<Movement>();
			for (var path : location.adjacent) {
				available.add(new Move(path));
				if (path.ping(this)) {
					radar.add(path.id());
				}
			}
			if (getPure(Attribute.Cunning) >= 28) {
				for (var path : location.shortcut) {
					available.add(new Shortcut(path));
				}
			}
			if (getPure(Attribute.Ninjutsu) >= 5) {
				for (var path : location.jump) {
					available.add(new Leap(path));
				}
			}

			for (var act : Global.getActions()) {
				if (act.usable(this)) {
					available.add(act);
				}
			}

			if (location.humanPresent()) {
				Global.gui().message(
						"You notice " + name() + parseMoves(available, radar, match).execute(this).describe());
			}
			else {
				var act = parseMoves(available, radar, match);
				location.leaveTrace(this, act.execute(this));
			}
		}
	}

	@Override
	public void faceOff(Character opponent, Encounter enc) {
		if (has(Consumable.smoke) && !ai.fightFlight(opponent)) {
			consume(Consumable.smoke, 1);
			enc.fightOrFlight(this, ai.fightFlight(opponent), true);
		}
		else {
			enc.fightOrFlight(this, ai.fightFlight(opponent), false);
		}
	}

	@Override
	public void spy(Character opponent, Encounter enc) {
		if (ai.attack(opponent)) {
			enc.ambush(this, opponent);
		}
		else {
			location.endEncounter();
		}
	}

	@Override
	public void ding() {
		xp -= 95 + (level * 5);
		if (xp < 0) {
			xp = 0;
		}
		level++;
		ai.ding();

		if (countFeats() < level / 4) {
			ai.pickFeat();
		}
		if (has(Trait.expertGoogler)) {
			getArousal().gainMax(1);
		}
		if (has(Trait.fitnessNut)) {
			getStamina().gainMax(1);
		}

		Global.gainSkills(this);
	}

	@Override
	public int income(int amount) {
		var total = Math.round(amount * Global.getValue(Flag.NPCScaling));
		return super.income(total);
	}

	@Override
	public void rankUp() {
		super.rankUp();
		ai.advance(rank);
	}

	@Override
	public void showerScene(Character target, Encounter encounter) {
		if (this.has(Flask.Aphrodisiac)) {
			encounter.aphrodisiacTrick(this, target);
		}
		else if (!target.nude() && Global.random(3) >= 2) {
			encounter.steal(this, target);
		}
		else {
			encounter.showerAmbush(this, target);
		}
	}

	@Override
	public void intervene(Encounter enc, Character p1, Character p2) {
		if (Global.random(5) == 0) {
			enc.watch(this);
			return;
		}

		// Determine who to assist
		var p1Sympathy = Global.random(20)
				+ getAffection(p1)
				+ (p1.has(Trait.sympathetic) ? 10 : 0)
				+ (p1.has(Trait.mostlyharmless) ? 10 : 0);
		var p2Sympathy = Global.random(20)
				+ getAffection(p2)
				+ (p2.has(Trait.sympathetic) ? 10 : 0)
				+ (p2.has(Trait.mostlyharmless) ? 10 : 0);
		if (p1Sympathy >= p2Sympathy) {
			enc.intrude(this, p1);
		}
		else {
			enc.intrude(this, p2);
		}
	}

	@Override
	public void load(Scanner loader) {
		level = Integer.parseInt(loader.next());
		setRank(Integer.parseInt(loader.next()));
		xp = Integer.parseInt(loader.next());
		money = Integer.parseInt(loader.next());

		// Attributes
		var e = loader.next();
		while (!e.equals("@")) {
			set(Attribute.valueOf(e), Integer.parseInt(loader.next()));
			e = loader.next();
		}
		getStamina().setMax(Integer.parseInt(loader.next()));
		getArousal().setMax(Integer.parseInt(loader.next()));
		getMojo().setMax(Integer.parseInt(loader.next()));

		// Affections
		e = loader.next();
		while (!e.equals("*")) {
			Roster.gainAffection(id(), ID.fromString(e), Integer.parseInt(loader.next()));
			e = loader.next();
		}

		// Attractions
		e = loader.next();
		while (!e.equals("*")) {
			Roster.gainAttraction(id(), ID.fromString(e), Integer.parseInt(loader.next()));
			e = loader.next();
		}

		// Clothes (TOP)
		e = loader.next();
		outfit[Character.OUTFITTOP].clear();
		while (!e.equals("!")) {
			outfit[Character.OUTFITTOP].add(Clothing.valueOf(e));
			e = loader.next();
		}

		// Clothes (BOTTOM)
		e = loader.next();
		outfit[Character.OUTFITBOTTOM].clear();
		while (!e.equals("!") && !e.equals("!!")) {
			outfit[Character.OUTFITBOTTOM].add(Clothing.valueOf(e));
			e = loader.next();
		}

		// Clothes (CLOSET)
		if (e.equals("!!")) {
			e = loader.next();
			closet.clear();
			while (!e.equals("!!!")) {
				closet.add(Clothing.valueOf(e));
				e = loader.next();
			}
		}

		// Traits
		e = loader.next();
		while (!e.equals("@")) {
			traits.add(Trait.valueOf(e));
			e = loader.next();
		}

		// Inventory
		e = loader.next();
		while (!e.equals("$")) {
			super.gain(Global.getItem(e));
			e = loader.next();
		}

		change(Modifier.normal);
		resetSkills();
		Global.gainSkills(this);
	}

	@Override
	public String challenge(Character opponent) {
		if (grudges.containsKey(opponent)) {
			activeGrudge = grudges.get(opponent);
		}
		return ai.startBattle(opponent);
	}

	@Override
	public void promptTrap(Encounter enc, Character target, Trap trap) {
		if (ai.attack(target)) {
			enc.trap(this, target, trap);
		}
		else {
			location.endEncounter();
		}
	}

	@Override
	public void afterParty() {
		ai.night();
	}

	/**
	 * Performs the NPC's daytime actions
	 *
	 * @param time The amount of time the NPC can spend
	 * @param day  A reference to the daytime object
	 */
	public void daytime(int time, Daytime day) {
		ai.rest(time, day);
	}

	@Override
	public void counterattack(Character target, Tactics type, Combat c) {
		switch (type) {
			case damage:
				c.write(name() + " avoids your clumsy attack and swings her fist into your nuts.");
				target.pain(4 + Global.random(getEffective(Attribute.Cunning)), Anatomy.genitals, c);
				break;
			case pleasure:
				if (target.pantsless()) {
					c.write(name() + " catches you by the penis and rubs your sensitive glans.");
				}
				else {
					c.write(name() + " catches you as you approach and grinds her knee into the tent in your " + target.bottom.peek());
				}
				target.pleasure(4 + Global.random(getEffective(Attribute.Cunning)), Anatomy.genitals, c);
				break;
			case positioning:
				c.write(name() + " outmaneuvers you and catches you from behind when you stumble.");
				c.stance = new Behind(this, target);
				break;
			default:
		}
	}

	/**
	 * Picks a random skill from the provided list for the NPC to use
	 *
	 * @param possibleSkills A list of available skills
	 * @return A skill picked from the list, or Null if none was found
	 */
	public Skill prioritizeRandom(ArrayList<HashSet<Skill>> possibleSkills) {
		if (possibleSkills.isEmpty()) {
			return null;
		}
		var wlist = new ArrayList<HashSet<Skill>>();
		for (var i = 0; i < possibleSkills.size(); i++) {
			if (!possibleSkills.get(i).isEmpty()) {
				for (var j = 0; j < possibleSkills.size() - i; j++) {
					wlist.add(possibleSkills.get(i));
				}
			}
		}
		if (wlist.isEmpty()) {
			return null;
		}
		var cat = wlist.get(Global.random(wlist.size()));
		if (cat.isEmpty()) {
			return null;
		}
		return cat.toArray(new Skill[0])[Global.random(cat.size())];
	}

	/**
	 * Prioritize possible actions by simulating their outcome and selecting one
	 *
	 * @param possibleSkills A list of available skills
	 * @param c              A reference to the ongoing combat
	 * @return The skill chosen as the next action, may be null
	 */
	public Skill prioritizeSimulated(ArrayList<HashSet<Skill>> possibleSkills, Combat c) {
		if (possibleSkills.isEmpty()) {
			return null;
		}

		// The higher, the better the AI will plan for "rare" events better
		final var RUN_COUNT = 5;
		// Decrease to get an "easier" AI. Make negative to get a suicidal AI.
		final var RATING_FACTOR = 0.1f;
		// Increase to take input weights more into consideration
		final var INPUT_WEIGHT = 1.0f;

		// Starting fitness
		var urgency0 = Math.min(c.p1.getUrgency(), c.p2.getUrgency());
		var fit1 = c.p1.getFitness(urgency0, c.stance);
		var fit2 = c.p2.getFitness(urgency0, c.stance);

		// Now simulate the result of all actions
		var ratingMap = new TreeMap<Float, Skill>();
		float sum = 0;
		float weight = 0;
		for (var skills : possibleSkills) {
			for (var skill : skills) {
				// Run it a couple of times
				float rating = 0;
				for (var i = 0; i < RUN_COUNT; i++) {
					rating += rateMove(skill, c, fit1, fit2);
				}

				// Sum up rating, add to map
				rating = (float) Math.exp(RATING_FACTOR * rating - weight);
				sum += rating;
				ratingMap.put(sum, skill);
			}

			if (!skills.isEmpty()) {
				weight += INPUT_WEIGHT;
			}
		}
		c.clearImages();
		if (sum == 0) {
			return null;
		}

		// Debug
		if (Global.debug) {
			var debugMessage = new StringBuilder("AI choices: ");
			float last = 0;
			for (var entry : ratingMap.entrySet()) {
				var d = entry.getKey() - last;
				last = entry.getKey();
				debugMessage.append(entry.getValue().toString())
						.append("(")
						.append(String.format("%.1f", d / sum * 100))
						.append("%) ");
			}
			System.out.println(debugMessage);
		}

		// Select
		var s = ((float) Global.random((int) (sum * 1024))) / 1024;
		return ratingMap.ceilingEntry(s).getValue();
	}

	/**
	 * Parse available skills and determine what to prioritise by their type and the character's mood
	 *
	 * @param available A list of available skills
	 * @param c         A reference to the ongoing combat
	 * @return A list of skill groups, containing the skills the character would consider using
	 */
	public ArrayList<HashSet<Skill>> parseSkills(HashSet<Skill> available, Combat c) {
		var target = c.getOther(this);
		var damage = new HashSet<Skill>();
		var pleasure = new HashSet<Skill>();
		var fucking = new HashSet<Skill>();
		var position = new HashSet<Skill>();
		var debuff = new HashSet<Skill>();
		var recovery = new HashSet<Skill>();
		var calming = new HashSet<Skill>();
		var summoning = new HashSet<Skill>();
		var stripping = new HashSet<Skill>();
		var preferred = new HashSet<Skill>();
		var priority = new ArrayList<HashSet<Skill>>();

		// Sort skills into categories
		for (var a : available) {
			if (preferredSkills.contains(a.getClass())) {
				preferred.add(a);
			}
			if (a.type() == Tactics.damage) {
				damage.add(a);
			}
			else if (a.type() == Tactics.pleasure) {
				pleasure.add(a);
			}
			else if (a.type() == Tactics.fucking) {
				fucking.add(a);
			}
			else if (a.type() == Tactics.positioning) {
				position.add(a);
			}
			else if (a.type() == Tactics.status) {
				debuff.add(a);
			}
			else if (a.type() == Tactics.recovery) {
				recovery.add(a);
			}
			else if (a.type() == Tactics.calming) {
				calming.add(a);
			}
			else if (a.type() == Tactics.summoning
					|| a.type() == Tactics.preparation) {
				summoning.add(a);
			}
			else if (a.type() == Tactics.stripping) {
				stripping.add(a);
			}
		}

		// Determine priorities based on mood
		switch (this.mood) {
			case confident:
				priority.add(pleasure);
				priority.add(fucking);
				priority.add(preferred);
				priority.add(position);
				priority.add(stripping);
				priority.add(summoning);
				if (!target.stunned()) {
					priority.add(damage);
				}
				priority.add(debuff);
				break;
			case angry:
				if (!target.is(Stsflag.protection)) {
					priority.add(damage);
				}
				priority.add(preferred);
				priority.add(debuff);
				priority.add(position);
				priority.add(stripping);
				priority.add(fucking);
				break;
			case nervous:
				priority.add(position);
				priority.add(calming);
				priority.add(summoning);
				priority.add(recovery);
				priority.add(pleasure);
				if (!target.is(Stsflag.protection)) {
					priority.add(damage);
				}
				break;
			case desperate:
				priority.add(calming);
				priority.add(recovery);
				priority.add(position);
				if (!target.is(Stsflag.protection)) {
					priority.add(damage);
				}
				priority.add(pleasure);
				break;
			case horny:
				priority.add(fucking);
				priority.add(stripping);
				priority.add(pleasure);
				priority.add(preferred);
				priority.add(position);
				break;
			case dominant:
				priority.add(fucking);
				priority.add(stripping);
				priority.add(pleasure);
				priority.add(preferred);
				priority.add(debuff);
				priority.add(summoning);
				priority.add(position);
				if (!target.is(Stsflag.protection)) {
					priority.add(damage);
				}
				break;
		}
		return priority;
	}

	/**
	 * Parses the available moves and selects the one to perform
	 *
	 * @param available All currently available actions
	 * @param radar     A collection of locations where enemies are known to be
	 * @param match     A reference to the ongoing match
	 * @return The action chosen by the NPC
	 */
	public Action parseMoves(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		var enemy = new HashSet<Action>();
		var safe = new HashSet<Action>();
		var highPriority = new HashSet<Action>();
		var lowPriority = new HashSet<Action>();
		if (location == goal) {
			plan = rethink();
		}

		// Try to move to dorm/union to get new clothes
		if (nude()) {
			for (var act : available) {
				if (act.consider() == Movement.resupply) {
					return act;
				}
				if (goal == null && act.getClass() == Move.class) {
					var movement = (Move) act;
					if ((movement.consider() == Movement.union && !radar.contains(Movement.union))
							|| (movement.consider() == Movement.dorm && !radar.contains(Movement.dorm))) {
						goal = movement.getDestination();
					}
				}
			}

			if (goal == null) {
				if (Global.random(2) == 1) {
					goal = match.gps(Movement.dorm);
				}
				else {
					goal = match.gps(Movement.union);
				}
			}
		}

		// Reduce arousal by masturbating if no one is around
		if (getArousal().percent() >= 40
				&& !location().humanPresent()
				&& radar.isEmpty()) {
			for (var act : available) {
				if (act.consider() == Movement.masturbate) {
					return act;
				}
			}
		}

		// Refresh by showering/bathing
		if (getStamina().percent() <= 60 || getArousal().percent() >= 30) {
			for (var act : available) {
				if (act.consider() == Movement.bathe) {
					return act;
				}
				if (goal == null && act.getClass() == Move.class) {
					Move movement = (Move) act;
					if (movement.consider() == Movement.pool
							|| movement.consider() == Movement.shower) {
						goal = movement.getDestination();
					}
				}
			}
			if (goal == null) {
				if (Global.random(2) == 1) {
					goal = match.gps(Movement.shower);
				}
				else {
					goal = match.gps(Movement.pool);
				}
			}
		}

		// Get more batteries
		if (getEffective(Attribute.Science) >= 1 && getPool(Pool.BATTERY).get() < 10) {
			for (var act : available) {
				if (act.consider() == Movement.recharge) {
					return act;
				}
			}
			if (goal == null) {
				goal = match.gps(Movement.workshop);
			}
		}

		// No specific goal, move according to plan
		if (goal == null) {
			// Move around to hunt other players
			if (plan == Emotion.hunting) {
				switch (Global.random(5)) {
					case 4:
						goal = match.gps(Movement.shower);
						break;
					case 3:
						goal = match.gps(Movement.union);
						break;
					case 2:
						goal = match.gps(Movement.la);
						break;
					case 1:
						goal = match.gps(Movement.dining);
						break;
					default:
						goal = match.gps(Movement.workshop);
						break;
				}
			}
			// Sneak around
			else if (plan == Emotion.sneaking) {
				switch (Global.random(5)) {
					case 4:
						goal = match.gps(Movement.storage);
						break;
					case 3:
						goal = match.gps(Movement.bridge);
						break;
					case 2:
						goal = match.gps(Movement.tunnel);
						break;
					case 1:
						goal = match.gps(Movement.workshop);
						break;
					default:
						goal = match.gps(Movement.lab);
						break;
				}
			}
			// Bored, wander around
			else if (plan == Emotion.bored) {
				switch (Global.random(5)) {
					case 4:
						goal = match.gps(Movement.storage);
						break;
					case 3:
						goal = match.gps(Movement.kitchen);
						break;
					case 2:
						goal = match.gps(Movement.shower);
						break;
					case 1:
						goal = match.gps(Movement.workshop);
						break;
					default:
						goal = match.gps(Movement.lab);
						break;
				}
			}
			// Head back to resupply areas
			else if (plan == Emotion.retreating) {
				if (Global.random(2) == 1) {
					goal = match.gps(Movement.dorm);
				}
				else {
					goal = match.gps(Movement.union);
				}
			}
		}

		assert goal != null;
		highPriority.add(findPath(goal));
		for (var act : available) {
			if (radar.contains(act.consider())) {
				// Track locations with known enemies
				enemy.add(act);
			}
			else if (act.consider() == Movement.quad
					|| act.consider() == Movement.kitchen
					|| act.consider() == Movement.dorm
					|| act.consider() == Movement.shower
					|| act.consider() == Movement.storage
					|| act.consider() == Movement.dining
					|| act.consider() == Movement.laundry
					|| act.consider() == Movement.tunnel
					|| act.consider() == Movement.bridge
					|| act.consider() == Movement.engineering
					|| act.consider() == Movement.workshop
					|| act.consider() == Movement.lab
					|| act.consider() == Movement.la
					|| act.consider() == Movement.library
					|| act.consider() == Movement.pool
					|| act.consider() == Movement.union) {
				// Track safe movement opportunities
				safe.add(act);
			}
			else if (plan == Emotion.hunting) {
				if (act.consider() == Movement.mana
						|| act.consider() == Movement.recharge
						|| act.consider() == Movement.locating
						|| act.consider() == Movement.potion
						|| act.consider() == Movement.trap
						|| act.consider() == Movement.energydrink) {
					lowPriority.add(act);
				}
			}
			else if (plan == Emotion.sneaking) {
				// If we trapped the area, make hiding/waiting the highest priority
				if (location.hasTrap(this)) {
					highPriority.clear();
					if (act.consider() == Movement.hide
							|| act.consider() == Movement.wait) {
						highPriority.add(act);
					}
				}
				// Stay in the current area to hide/scavenge/etc
				else {
					if (act.consider() == Movement.hide
							|| act.consider() == Movement.scavenge
							|| act.consider() == Movement.trap
							|| act.consider() == Movement.recharge) {
						highPriority.add(act);
					}
					else if (act.consider() == Movement.mana
							|| act.consider() == Movement.potion
							|| act.consider() == Movement.bathe
							|| act.consider() == Movement.energydrink) {
						lowPriority.add(act);
					}
				}
			}
			else if (plan == Emotion.bored) {
				// While bored, prefer crafting/scavenging and refreshing
				if (act.consider() == Movement.mana
						|| act.consider() == Movement.recharge
						|| act.consider() == Movement.scavenge
						|| act.consider() == Movement.craft) {
					highPriority.add(act);
				}
				else if (act.consider() == Movement.trap
						|| act.consider() == Movement.potion
						|| act.consider() == Movement.bathe
						|| act.consider() == Movement.energydrink
						|| act.consider() == Movement.beer
						|| act.consider() == Movement.oil) {
					lowPriority.add(act);
				}
			}
			else if (plan == Emotion.retreating) {
				if (act.consider() == Movement.energydrink) {
					highPriority.add(act);
				}
			}
		}
		if ((plan == Emotion.hunting || plan == Emotion.bored) && !enemy.isEmpty()) {
			highPriority.addAll(enemy);
		}
		else if (plan == Emotion.retreating) {
			// While retreating, don't consider enemy engagements and prefer save movements
			highPriority.removeAll(enemy);
			lowPriority.addAll(safe);
		}
		else if (plan == Emotion.sneaking && !enemy.isEmpty()) {
			lowPriority.addAll(enemy);
		}

		// 25% chance to pick from all high- AND low-priority actions
		if (highPriority.isEmpty() || Global.random(4) == 0) {
			highPriority.addAll(lowPriority);
		}

		// If no prioritized actions available, choose from all possible
		if (highPriority.isEmpty()) {
			highPriority.addAll(available);
		}

		var actions = highPriority.toArray(new Action[0]);
		var chosen = actions[Global.random(actions.length)];

		// If no other action is possible, wait
		if (chosen == null) {
			chosen = new Wait();
		}
		return chosen;
	}

	@Override
	public Emotion moodSwing() {
		var current = mood;
		float max = emotes.get(current);
		for (var e : emotes.keySet()) {
			if (emotes.get(e) * ai.moodWeight(e) > max) {
				mood = e;
				max = emotes.get(e);
			}
		}
		return mood;
	}

	@Override
	public void resetOutfit() {
		ai.resetOutfit();
	}

	/**
	 * Rethink the NPCs current plan (retreating, hunting, etc.)
	 *
	 * @return The emotion that describes the NPCs current preferred plan
	 */
	public Emotion rethink() {
		goal = null;
		var current = plan;
		if (!ai.fit()) {
			return Emotion.retreating;
		}
		var total = 5;
		total += strategy.get(Emotion.hunting);
		total += strategy.get(Emotion.sneaking);
		total += strategy.get(Emotion.bored);
		var result = Global.random(total);
		if (result >= 5 + strategy.get(Emotion.hunting) + strategy.get(Emotion.bored)) {
			current = Emotion.bored;
		}
		else if (result >= 5 + strategy.get(Emotion.hunting)) {
			current = Emotion.sneaking;
		}
		else if (result >= 5) {
			current = Emotion.hunting;
		}
		return current;
	}

	@Override
	public void endOfTurn(Combat c, Character opponent) {
		// Execute status effects
		for (var s : getStatus()) {
			s.turn(c);
		}

		// Attempt to capture opponent's pet
		if (opponent.pet != null
				&& canAct()
				&& c.stance.mobile(this)
				&& !c.stance.prone(this)) {
			if (getEffective(Attribute.Speed) > opponent.pet.ac() * Global.random(20)) {
				opponent.pet.caught(c, this);
			}
		}

		// Check if opponent's pheromones turn us on
		if (opponent.has(Trait.pheromones)
				&& opponent.getArousal().percent() >= 50
				&& !is(Stsflag.horny)
				&& Global.random(5) == 0) {
			c.write("You see " + name() + " swoon slightly as she gets close to you. Seems like she's starting to feel the effects of your musk.");
			add(new Horny(this, 2 + 2 * opponent.getSkimpiness(), 3), c);
		}

		// Check if opponent's tail can pleasure us
		if (opponent.has(Trait.tailmastery)
				&& opponent.has(Trait.tailed)
				&& !opponent.distracted()
				&& !opponent.stunned()
				&& pantsless()) {
			c.write("You tease " + name() + "'s sensitive ares with your fluffy tail.");
			pleasure(Global.random(5), Anatomy.genitals, c);
		}

		if (has(Trait.RawSexuality)) {
			tempt(1);
			opponent.tempt(1);
		}

		// Update emotions on dom/sub stance
		if (c.stance.dom(this)) {
			emote(Emotion.dominant, 30);
			emote(Emotion.confident, 10);
		}
		else if (c.stance.sub(this)) {
			emote(Emotion.nervous, 30);
			emote(Emotion.desperate, 20);
		}

		if (c.stance.penetration(this) || c.stance.penetration(opponent)) {
			emote(Emotion.horny, 20);
		}

		// Get turned on by nude opponent
		if (opponent.nude()) {
			emote(Emotion.horny, 5);
			emote(Emotion.confident, 10);
			emote(Emotion.dominant, 10);
		}

		// Get nervous (and possibly horny) from being nude
		if (nude()) {
			emote(Emotion.nervous, 20);
			emote(Emotion.desperate, 10);
			if (has(Trait.exhibitionist)) {
				emote(Emotion.horny, 20);
			}
		}

		// Become confident if opponent is very aroused
		if (opponent.getArousal().percent() >= 75) {
			emote(Emotion.dominant, 15);
			emote(Emotion.confident, 5);
		}

		// Become desperate if very aroused
		if (getArousal().percent() >= 75) {
			emote(Emotion.desperate, 15);
			emote(Emotion.nervous, 10);
		}

		// Become more desperate when extremely aroused
		if (getArousal().percent() >= 90) {
			emote(Emotion.desperate, 25);
		}

		// Become desperate if unable to act
		if (!canAct()) {
			emote(Emotion.desperate, 20);
		}

		// Become more dominant if opponent can't act
		if (!opponent.canAct()) {
			emote(Emotion.dominant, 30);
		}

		// Get feral status if feral or aroused
		if (has(Trait.feral)
				|| (getPure(Attribute.Animism) >= 4 && getArousal().percent() >= 50)
				|| (has(Trait.furaffinity) && getArousal().percent() >= 25)) {
			if (!is(Stsflag.feral)) {
				add(new Feral(this));
			}
		}
		moodSwing();
	}

	/**
	 * Attempt to visit friends to increase affection and train together
	 *
	 * @param time The maximum number of visits to perform
	 */
	public void visit(int time) {
		var max = 0;
		var visitsRemaining = time;

		// Find best friend (the highest affection to another NPC)
		Character bff = null;
		for (var friend : Scheduler.getAvailable(LocalTime.of(3, 0))) {
			if (getAttraction(friend) > max && !friend.human()) {
				max = getAttraction(friend);
				bff = friend;
			}
		}
		if (bff != null) {
			visitsRemaining -= 1;
			bff.gainAffection(this, 1);
			switch (Global.random(3)) {
				case 0:
					Daytime.train(this, bff, Attribute.Power);
				case 1:
					Daytime.train(this, bff, Attribute.Cunning);
				default:
					Daytime.train(this, bff, Attribute.Seduction);
			}
		}

		// For remaining visits, get a random NPC friend
		while (visitsRemaining > 0) {
			visitsRemaining -= 1;
			bff = getRandomFriend();
			if (bff != null) {
				bff.gainAffection(this, 1);
				switch (Global.random(3)) {
					case 0:
						Daytime.train(this, bff, Attribute.Power);
					case 1:
						Daytime.train(this, bff, Attribute.Cunning);
					default:
						Daytime.train(this, bff, Attribute.Seduction);
				}
			}
		}
	}

	@Override
	public NPC clone() throws CloneNotSupportedException {
		return (NPC) super.clone();
	}

	@Override
	public String getPortrait() {
		return ai.image();
	}

	/**
	 * Gets a comment matching the current combat state
	 *
	 * @param c A reference to the ongoing combat
	 */
	public String getComment(Combat c) {
		return CommentSituation.getBestComment(ai.getComments(), c, this, c.getOther(this));
	}

	/**
	 * Gets a response matching the current combat state
	 *
	 * @param c A reference to the ongoing combat
	 */
	public String getResponse(Combat c) {
		return CommentSituation.getBestComment(ai.getResponses(), c, this, c.getOther(this));
	}

	@Override
	protected int getCostumeSet() {
		return ai.getCostumeSet();
	}

	/**
	 * Gets the result from observing the NPC's mental state
	 *
	 * @param perception The perception value of the observing character
	 */
	private String observe(int perception) {
		for (var s : status) {
			if (s.flags().contains(Stsflag.unreadable)) {
				return "";
			}
		}

		var visible = new StringBuilder();

		// Determine arousal
		if (perception >= 9) {
			visible.append("Her arousal is at ")
					.append(getArousal().percent())
					.append("%.<br>");
		}
		else if (perception >= 7) {
			if (getArousal().percent() >= 75) {
				visible.append(
						"She's dripping with arousal and breathing heavily. She's at least 3/4 of the way to orgasm.<br>");
			}
			else if (getArousal().percent() >= 50) {
				visible.append("She's showing signs of arousal. She's at least halfway to orgasm.<br>");
			}
			else if (getArousal().percent() >= 25) {
				visible.append("She's starting to look noticeably aroused, maybe a quarter of her limit.<br>");
			}
		}
		else if (perception >= 3) {
			if (getArousal().percent() >= 50) {
				visible.append("She's showing clear sign of arousal. You're definitely getting to her.<br>");
			}
		}

		// Determine stamina
		if (perception >= 8) {
			visible.append("Her stamina is at ")
					.append(getStamina().percent())
					.append("%.<br>");
		}
		else if (perception >= 6) {
			if (getStamina().percent() <= 33) {
				visible.append("She looks a bit unsteady on her feet.<br>");
			}
			else if (getStamina().percent() <= 66) {
				visible.append("She's starting to look tired.<br>");
			}
		}
		else if (perception >= 4) {
			if (getStamina().percent() <= 50) {
				visible.append("She looks pretty tired.<br>");
			}
		}

		// Determine mood
		if (perception >= 5) {
			visible.append(mood.describe())
					.append("<br>");
		}

		return visible.toString();
	}

	/**
	 * Determine how effective this move is for winning combat
	 *
	 * @param skill The skill to check
	 * @param c     A reference to the ongoing combat
	 * @param fit1  Current fitness of character 1
	 * @param fit2  Current fitness of character 2
	 */
	private float rateMove(Skill skill, Combat c, float fit1, float fit2) {
		// Clone ourselves a new combat... This should clone our characters, too
		Combat combatSimulation;
		try {
			combatSimulation = c.clone();
		}
		catch (CloneNotSupportedException e) {
			return 0;
		}
		// Now do it!
		if (c.p1 == this) {
			skill.setSelf(combatSimulation.p1);
			skill.resolve(combatSimulation, combatSimulation.p2);
			skill.setSelf(c.p1);
		}
		else {
			skill.setSelf(combatSimulation.p2);
			skill.resolve(combatSimulation, combatSimulation.p1);
			skill.setSelf(c.p2);
		}
		// How close is the fight to finishing?
		var urgency = Math.min(combatSimulation.p1.getUrgency(), combatSimulation.p2.getUrgency());
		var dfit1 = combatSimulation.p1.getFitness(urgency, combatSimulation.stance) - fit1;
		var dfit2 = combatSimulation.p2.getFitness(urgency, combatSimulation.stance) - fit2;
		return (c.p1 == this ? dfit1 - dfit2 : dfit2 - dfit1);
	}
}
