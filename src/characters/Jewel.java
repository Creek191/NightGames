package characters;

import Comments.CommentGroup;
import Comments.CommentSituation;
import Comments.SkillComment;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Result;
import combat.Tag;
import daytime.Daytime;
import global.Flag;
import global.Global;
import global.Match;
import global.Modifier;
import items.Clothing;
import items.Toy;
import items.Trophy;
import scenes.SceneFlag;
import scenes.SceneManager;
import skills.*;
import stance.Stance;
import status.Stsflag;

import java.util.ArrayList;
import java.util.HashSet;

public class Jewel implements Personality {
	/**
	 *
	 */
	private static final long serialVersionUID = 6677748046858370216L;
	private final NPC character;

	public Jewel() {
		character = new NPC("Jewel", ID.JEWEL, 1, this);

		// Set clothing
		resetOutfit();
		character.closet.add(Clothing.bra);
		character.closet.add(Clothing.tanktop);
		character.closet.add(Clothing.panties);
		character.closet.add(Clothing.jeans);
		character.change(Modifier.normal);
		character.setUnderwear(Trophy.JewelTrophy);

		// Set base stats
		character.mod(Attribute.Power, 2);
		character.mod(Attribute.Speed, 1);
		character.getStamina().gainMax(20);

		// Set base traits
		character.add(Trait.female);
		character.add(Trait.direct);
		character.add(Trait.wrassler);
		character.add(Trait.insatiable);

		// Set AI logic
		character.plan = Emotion.hunting;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.sneaking, 0);
		character.strategy.put(Emotion.bored, 1);
		character.preferredSkills.add(FlashStep.class);
		character.preferredSkills.add(LowBlow.class);
		character.preferredSkills.add(Tackle.class);
		character.preferredSkills.add(HandjobEX.class);
		if (Global.checkFlag(Flag.PlayerButtslut)) {
			character.preferredSkills.add(AssFuck.class);
			character.preferredSkills.add(Strapon.class);
			character.preferredSkills.add(FingerAss.class);
		}

		Global.gainSkills(character);
	}

	@Override
	public Skill act(HashSet<Skill> available, Combat c) {
		var mandatory = new HashSet<Skill>();
		for (var skill : available) {
			if (skill.toString().equalsIgnoreCase("Ass Fuck")) {
				mandatory.add(skill);
			}
			if (character.is(Stsflag.orderedstrip)
					&& (skill.toString().equalsIgnoreCase("Undress")
					|| skill.toString().equalsIgnoreCase("Strip Tease"))) {
				mandatory.add(skill);
			}
			if (Global.checkFlag(Flag.PlayerButtslut) && character.has(Trait.strapped)) {
				if (skill.toString().equalsIgnoreCase("Mount") && character.has(Toy.Strapon2)) {
					if (Global.random(5) == 0) {
						mandatory.add(skill);
					}
				}
				if (skill.toString().equalsIgnoreCase("Turn Over")) {
					mandatory.add(skill);
				}
			}
		}

		if (!mandatory.isEmpty()) {
			var actions = mandatory.toArray(new Skill[0]);
			return actions[Global.random(actions.length)];
		}

		Skill chosen;
		var priority = character.parseSkills(available, c);
		if (Global.checkFlag(Flag.hardmode) && !c.hasModifier(Modifier.quiet)) {
			chosen = character.prioritizeSimulated(priority, c);
		}
		else {
			chosen = character.prioritizeRandom(priority);
		}

		if (chosen != null) {
			return chosen;
		}

		var actions = available.toArray(new Skill[0]);
		return actions[Global.random(actions.length)];
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		return character.parseMoves(available, radar, match);
	}

	@Override
	public void rest(int time, Daytime day) {
		if (character.rank >= 1) {
			if (character.money > 0) {
				day.visit("Dojo", character, Global.random(character.money));
				day.visit("Meditate", character, Global.random(character.money));
			}
		}
		if (Global.checkFlag(Flag.PlayerButtslut) // Special case for Buttslut starting mode
				&& !(character.has(Toy.Strapon) || character.has(Toy.Strapon2))
				&& character.money >= 200) {
			character.gain(Toy.Strapon);
			character.money -= 200;
		}
		if (!(character.has(Toy.Crop) || character.has(Toy.Crop2)) && character.money >= 200) {
			character.gain(Toy.Crop);
			character.money -= 200;
		}
		if (!(character.has(Toy.Strapon) || character.has(Toy.Strapon2)) && character.money >= 600 && character.getPure(
				Attribute.Seduction) >= 20) {
			character.gain(Toy.Strapon);
			character.money -= 600;
		}
		character.visit(4);
		var available = new ArrayList<String>();
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		if (character.rank > 0) {
			available.add("Dojo");
			available.add("Workshop");
		}
		available.add("Play Video Games");
		for (var i = 0; i < time - 5; i++) {
			var location = available.get(Global.random(available.size()));
			day.visit(location, character, Global.random(character.money));
		}
		if (character.getAffection(Global.getPlayer()) > 0) {
			Global.modCounter(Flag.JewelDWV, 1);
		}
	}

	@Override
	public String bbLiner() {
		if (character.getAffection(Global.getPlayer()) >= 25) {
			return "Jewel gives you a confident smirk. <i>\"You left yourself open again. Did you think I'd go easy on you just because I like you? If anything, the thought of "
					+ "busting your balls makes me extra wet.\"</i>";
		}
		switch (Global.random(3)) {
			case 1:
				return "Jewel lets out a hearty laugh.  <i>\"Hah!  Nut shot!\"</i>";
			case 2:
				return "<i>\"Come on little boy, get back up, take it like a man, and walk it off!\"</i>";
			default:
				return "Jewel gently pats your injured testicles. <br><i>\"These things are the reason I'm glad I was born a girl. If I had a pair of big dangling targets between my legs, "
						+ "I could never concentrate on fighting.\"</i>";
		}
	}

	@Override
	public String nakedLiner() {
		return "Jewel smiles and makes no effort to hide her nakedness. <i>\"Feel free to enjoy the view. I love fighting naked, it gives me so much freedom of movement.\"</i>";
	}

	@Override
	public String stunLiner() {
		return "Jewel takes several heaving breaths, looking beaten and exhausted. She suddenly grins ear to ear. <i>\"OK, I'm impressed.\"</i>";
	}

	@Override
	public String taunt() {
		return "Jewel glares at you and squeezes your dick tightly. <i>\"No matter how horny you are, you better give me your best fight. I don't like fucking weaklings.\"</i>";
	}

	@Override
	public void victory(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		character.clearGrudge(opponent);
		if (!opponent.human()) {
			return;
		}

		if (c.lastAction(character).toString().startsWith("Pleasure Bomb")) {
			SceneManager.play(SceneFlag.JewelUltVictory);
		}
		else if (flag == Result.anal && c.stance.penetration(character)) {
			if (Global.getValue(Flag.PlayerAssLosses) <= 0) {
				Global.setCounter(Flag.PlayerAssLosses, 1);
				SceneManager.play(SceneFlag.JewelPeggingVirginity);
			}
			else {
				Global.modCounter(Flag.PlayerAssLosses, 1);
				SceneManager.play(SceneFlag.JewelPeggingVictory);
			}
		}
		else if (c.stance.en == Stance.pin && c.stance.dom(character)) {
			SceneManager.play(SceneFlag.JewelPinVictory);
		}
		else if (flag == Result.intercourse) {
			SceneManager.play(SceneFlag.JewelSexVictory);
		}
		else if (opponent.is(Stsflag.horny)) {
			SceneManager.play(SceneFlag.JewelHornyVictory);
		}
		else if (character.has(Trait.fighter)
				&& character.getEffective(Attribute.Ki) >= 10
				&& Global.random(2) == 0) {
			SceneManager.play(SceneFlag.JewelFireVictory);
		}
		else if (character.getArousal().percent() > 50) {
			character.getArousal().empty();
			SceneManager.play(SceneFlag.JewelForeplayVictoryAlt);
		}
		else {
			SceneManager.play(SceneFlag.JewelForeplayVictory);
		}
	}

	@Override
	public void defeat(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		declareGrudge(opponent, c);
		if (!opponent.human()) {
			return;
		}
		if (flag == Result.anal && c.stance.penetration(opponent)) {
			SceneManager.play(SceneFlag.JewelAnalDefeat);
		}
		else if (flag == Result.intercourse && c.stance.dom(character)) {
			SceneManager.play(SceneFlag.JewelReversalDefeat);
		}
		else if (flag == Result.intercourse) {
			SceneManager.play(SceneFlag.JewelSexDefeat);
		}
		else if (character.is(Stsflag.masochism)) {
			SceneManager.play(SceneFlag.JewelMasochismDefeat);
		}
		else if (c.stance.en == Stance.pin && c.stance.sub(character)) {
			SceneManager.play(SceneFlag.JewelPinDefeat);
		}
		else if (character.is(Stsflag.horny)) {
			SceneManager.play(SceneFlag.JewelHornyDefeat);
		}
		else if (opponent.getArousal().percent() <= 30) {
			SceneManager.play(SceneFlag.JewelForeplayDefeatEasy);
		}
		else if (character.has(Trait.fighter) && Global.random(2) == 0) {
			SceneManager.play(SceneFlag.JewelChallengeDefeat);
		}
		else if (Global.random(2) == 0) {
			SceneManager.play(SceneFlag.JewelForeplayDefeatAlt);
		}
		else {
			SceneManager.play(SceneFlag.JewelForeplayDefeat);
		}
	}

	@Override
	public String describe() {
		if (character.has(Trait.fighter)) {
			return "Something has changed about Jewel's demeanor, though it's hard to put your finger on it. Her body has always been toned, but now she seems like a weapon "
					+ "in human shape. She carries a calm composure subtly different from her normal arrogance. Her movements are deliberate and fluid, like you imagine a "
					+ "martial arts master would look.";
		}
		return "Jewel has one of the most appropriate names you've ever heard. Her eyes are as bright green as emeralds and her long ponytailed hair is ruby red. The combination "
				+ "makes her strikingly beautiful despite not bothering with any make-up. Her body is fit and toned, with almost no fat, but somehow her breasts are at least a "
				+ "C cup. She practically radiates confidence. By her expression alone, it's like her victory is already assured.";
	}

	@Override
	public void draw(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		character.clearGrudge(opponent);
		if (!opponent.human()) {
			return;
		}

		if (flag == Result.intercourse) {
			SceneManager.play(SceneFlag.JewelSexDraw);
		}
		else {
			SceneManager.play(SceneFlag.JewelForeplayDraw);
		}
	}

	@Override
	public boolean fightFlight(Character opponent) {
		return true;
	}

	@Override
	public boolean attack(Character opponent) {
		return true;
	}

	@Override
	public void ding() {
		var numIncreases = (Global.random(3) / 2) + 1;
		var numOptions = 4;

		if (character.getPure(Attribute.Ki) >= 1) {
			character.mod(Attribute.Ki, 1);
		}
		else {
			character.mod(Attribute.Power, 1);

			// Early on, don't allow increasing Ki
			numOptions = 3;
		}

		for (var i = 0; i < numIncreases; i++) {
			switch (Global.random(numOptions)) {
				case 0:
					character.mod(Attribute.Power, 1);
					break;
				case 1:
					character.mod(Attribute.Seduction, 1);
					break;
				case 2:
					character.mod(Attribute.Cunning, 1);
					break;
				case 3:
					character.mod(Attribute.Ki, 1);
					break;
			}
		}

		character.getStamina().gainMax(6);
		character.getArousal().gainMax(3);
	}

	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if (target.human()) {
			return "Jewel grabs your cock firmly with one hand and your balls with the other. <i>\"I'll make you surrender with one of these. Which one do you want?\"</i> She sounds "
					+ "like she's joking, but her face tells you she's actually waiting for an answer. You don't want her to abuse your balls, so you swallow your pride and "
					+ "ask her to focus on your dick. She smiles, releases your ballsack and slowly pumps your dick. She maintains the slow pace until you're leaking pre-cum "
					+ "and suddenly stops. She wets her free hand with your liquid and begins polishing the sensitive head of your dick with her palm. The sensation is so strong that you try "
					+ "to pull away, but Jewel is holding your manhood securely and doesn't let it get away. She thoroughly tortures you with pure pleasure. You beg for mercy, but she doesn't let up until "
					+ "you cum in her hands.";
		}
		else {
			if (target.hasDick()) {
				return "Jewel looks over " + target.name() + ", trying to decide what to do with " + target.pronounTarget(
						false) + ". She stands up and presses her bare foot against "
						+ target.possessive(false) + " dick and balls. " + target.name() + " groans in pleasure and pain as Jewel roughly grinds her foot against the sensitive organs. "
						+ "<i>\"Do you like that? You can't help it, can you?\"</i> She grins sadistically. <i>\"I've stomped many boys into the ground, "
						+ "but no matter how much pride they have, they always end up moaning in pleasure. It's like penises exist just to be dominated.\"</i> You feel a chill run down your "
						+ "back, watching Jewel's display of dominance, but you're also rock hard. " + target.name() + " lets out a loud moan and covers Jewel's foot with cum.";
			}
			else {
				return "Jewel looks over " + target.name() + ", trying to decide what to do with her. She leans in and kisses the helpless girl firmly on the lips. Breaking the kiss, "
						+ "she starts to probe and inspect " + target.name() + "'s other lips, making her twitch and whimper with each touch. " + target.name() + " apparently passes the inspection, "
						+ "because Jewel slides her hips forward and presses her own wet pussy against " + target.name() + "'s. Both girls moan softly as Jewel begins moving her hips, grinding their lips and clits together. For a moment, "
						+ "you think Jewel's plan may backfire and she may cum first, but " + target.name() + " soon shudders to climax in your arms. Jewel doesn't stop until she reaches "
						+ "her own orgasm too.";
			}
		}
	}

	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if (target.human()) {
			return "You skillfully force " + assist.name() + " to the floor and begin pleasuring her into submission. You rub and finger her pussy until she's shivering and "
					+ "juices flow down her thighs. Before you finish her off, you hear a whistle behind you. You turn and see Jewel standing behind you. Before you can do "
					+ "anything, she sweeps your legs out from under you and deposits you on the floor next to " + assist.name() + ". She traps your arms in her thighs, leaving "
					+ "you defenseless. By this time, " + assist.name() + " has recovered and looks ready to take revenge.<br>";
		}
		else {
			return "You wrestle with " + target.name() + " until you're both naked and sweaty. You seem to have a slight advantage until she manages to get a free hand between "
					+ "your legs and slap your balls. You fall to the floor in pain, but " + target.name() + " doesn't have a chance to follow up. Jewel has arrived, seemingly out "
					+ "of nowhere, and before " + target.name() + " can react, Jewel slaps her on the pussy. She crumples in pain, almost mirroring you, and can't put up any defense "
					+ "when Jewel restrains her arms. You pull yourself back up so you can take advantage of your helpless opponent.<br>";
		}
	}

	@Override
	public void watched(Combat c, Character target, Character viewer) {
		if (viewer.human()) {
			SceneManager.play(SceneFlag.JewelWatch, target);
		}
	}

	@Override
	public String startBattle(Character opponent) {
		if (character.getGrudge() != null) {
			switch (character.getGrudge()) {
				case confidentdom:
					return "Jewel cracks her knuckles with a confident expression. "
							+ "She looks pretty sure of herself considering you've beaten her a couple times already.<p>"
							+ "<i>\"It's all about mind over matter. As long as I'm sure of my victory, there's no way I can lose\"</i><p>"
							+ "It sounds like you're going to have some trouble dealing with her this time, "
							+ "unless you can break her composure a bit.";
				case healing:
					return "Jewel closes her eyes and takes several deep breaths. When she opens her eyes, she takes her usual fighting stance.<p>"
							+ "<i>\"OK, I may have lost last time, but this time there's no way I'm going to run out of steam. Give me your best shot!\"</i>";
				case powerup:
					return "Jewel takes a deep breath, then lets out an intense <i>\"Osu!\"</i> She slowly, but deliberately plants her feet in "
							+ "a stable fighting stance. This is a much more focused and disciplined Jewel than you're used to. She practically "
							+ "radiates power.";
				case flash:
					return "You're preparing to fight Jewel, when she suddenly disappears from your sight. "
							+ "You instinctively throw out a kick behind you, hitting nothing but air.<p>"
							+ "You spin around and see that Jewel is in fact behind you, but is out of reach.<p>"
							+ "<i>\"Nice counter. That probably would have hit me if I wasn't using my Ki to enhance my reflexes. "
							+ "I can't keep it up for very long, but let's see if it's long enough to finish the fight.\"</i>";
				default:
					break;
			}
		}
		if (character.nude()) {
			return "Jewel shows no concern for her nakedness, as she takes up a loose fighting stance. This causes her bare breasts to bounce enticingly.<p>"
					+ "<i>\"Hey, try to keep your mind on the fight. I don't mind if you enjoy the view, but I'll be pissed if you're too distracted to fight properly.\"</i>";
		}
		if (opponent.pantsless()) {
			return "Jewel grins while looking over your state of undress. <i>\"I don't mind the view, but are you sure you don't want to cover up? "
					+ "If you give me such an obvious target, you can't complain if I take advantage.\"</i>";
		}
		if (character.getAffection(opponent) >= 30) {
			return "Jewel gives you an affectionate smile, but takes her usual fighting stance.<p>"
					+ "<i>\"You better not go easy on me just because we're lovers. I'm sure as hell gonna try to dominate you!\"</i>";
		}
		if (character.has(Trait.fighter)) {
			return "Jewel bows to you before taking a fighting stance.";
		}
		return "Jewel approaches, looking confident and ready to fight.";
	}

	@Override
	public boolean fit() {
		return true;
	}

	@Override
	public boolean night() {
		Global.gui().loadPortrait(Global.getPlayer(), this.character);
		SceneManager.play(SceneFlag.JewelAfterMatch);
		return true;
	}

	@Override
	public void advance(int rank) {
		if (rank >= 4 && !character.has(Trait.juggernaut)) {
			character.add(Trait.juggernaut);
		}
		if (rank >= 3 && !character.has(Trait.reflexes)) {
			character.add(Trait.reflexes);
		}
		if (rank >= 2 && !character.has(Trait.roughhandling)) {
			character.add(Trait.roughhandling);
		}
		if (rank >= 1 && !character.has(Trait.fighter)) {
			character.add(Trait.fighter);
			character.outfit[Character.OUTFITTOP].removeAllElements();
			character.outfit[Character.OUTFITBOTTOM].removeAllElements();

			character.outfit[Character.OUTFITTOP].add(Clothing.gi);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.panties);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.kungfupants);
			character.closet.add(Clothing.gi);
			character.closet.add(Clothing.kungfupants);
			character.clearSpriteImages();
			character.mod(Attribute.Ki, 1);
		}
	}

	@Override
	public NPC getCharacter() {
		return character;
	}

	@Override
	public boolean checkMood(Emotion mood, int value) {
		switch (mood) {
			case angry:
			case dominant:
				return value >= 30;
			case nervous:
				return value >= 80;
			default:
				return value >= 50;
		}
	}

	@Override
	public float moodWeight(Emotion mood) {
		switch (mood) {
			case angry:
			case dominant:
				return 1.2f;
			case nervous:
				return .7f;
			default:
				return 1f;
		}
	}

	@Override
	public String image() {
		return "assets/jewel_" + character.mood.name() + ".jpg";
	}

	@Override
	public void pickFeat() {
		var available = new ArrayList<Trait>();
		for (var feat : Global.getFeats()) {
			if (!character.has(feat) && feat.meetsRequirement(character)) {
				available.add(feat);
			}
		}

		if (!available.isEmpty()) {
			character.add((Trait) available.toArray()[Global.random(available.size())]);
		}
	}

	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		return null;
	}

	@Override
	public CommentGroup getComments() {
		var comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN,
				"<i>\"Ha! Your cock is already twitching inside me. Boys cum so easily.\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Not yet! Fuck, not yet! Cum for me now!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Even on top you can't handle me! Cum already!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Oh, yes! You might just deserve this!\"</i>");
		comments.put(CommentSituation.ANAL_PITCH_WIN,
				"<i>\"I love fucking a pretty ass! You love it too, right? I know you do. Are you actually going to cum like this now?\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"Fuck! I'm actually going to cum from my ass!\"</i>");
		comments.put(CommentSituation.OTHER_BOUND, "<i>\"Not going anywhere now, are you?\"</i>");
		comments.put(CommentSituation.SELF_BOUND,
				"<i>\"You cheap bastard! As soon as I get free, I'm gonna yank your balls off.\"</i>");
		comments.put(CommentSituation.OTHER_STUNNED,
				"<i>\"Aww, did I hurt you? Let me kiss you to make it better!\"</i>");
		comments.put(CommentSituation.PIN_DOM_WIN, "<i>\"I've got you! No escape now!\"</i>");
		comments.put(CommentSituation.BEHIND_DOM_WIN, "<i>\"Shh, just cum for me now...\"</i>");
		comments.put(CommentSituation.SELF_HORNY, "<i>\"More! Fuck me or hurt me, just give me more!\"</i>");
		comments.put(CommentSituation.MOUNT_SUB_LOSE,
				"<i>\"Are you gonna just sit on me all day or are you gonna man up and fuck me?\"</i>");
		comments.put(CommentSituation.PIN_DOM_LOSE,
				"<i>\"Did you think you had me beat? It's hard to get me off when you can't move.\"</i>");
		comments.put(CommentSituation.OTHER_SHAMED,
				"<i>\"Ah... A boy with a hard cock and broken pride... Brings back memories...\"</i>");
		comments.put(CommentSituation.SELF_BUSTED,
				"Jewel lets out a short burst of unintelligible profanity, covering her privates.");
		comments.put(new SkillComment(SkillTag.PET, true),
				"<i>\"Calling in reinforcements? I can take you both alone.\"</i>");
		comments.put(new SkillComment(Attribute.Ki, true),
				"<i>\"Show me that fighting spirit! I'm just getting fired up!\"</i>");
		comments.put(new SkillComment(Attribute.Arcane, true),
				"<i>\"Are you bringing magic tricks to a brawl? Don't think that's going to stop me!\"</i>");

		return comments;
	}

	@Override
	public CommentGroup getResponses() {
		return new CommentGroup();
	}

	@Override
	public int getCostumeSet() {
		return character.has(Trait.fighter) ? 2 : 1;
	}

	@Override
	public void declareGrudge(Character opponent, Combat c) {
		if ((character.getGrudge() == Trait.healing || character.getGrudge() == Trait.powerup)) {
			character.addGrudge(opponent, Trait.confidentdom);
		}
		else {
			switch (Global.random(3)) {
				case 0:
					if (character.has(Trait.fighter)) {
						character.addGrudge(opponent, Trait.flash);
						break;
					}
				case 2:
					character.addGrudge(opponent, Trait.healing);
					break;
				case 1:
					character.addGrudge(opponent, Trait.powerup);
					break;
				default:
					break;
			}
		}
	}

	@Override
	public void resetOutfit() {
		character.outfit[Character.OUTFITTOP].clear();
		character.outfit[Character.OUTFITBOTTOM].clear();

		if (character.has(Trait.fighter)) {
			character.outfit[Character.OUTFITTOP].add(Clothing.gi);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.panties);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.kungfupants);
		}
		else {
			character.outfit[Character.OUTFITTOP].add(Clothing.bra);
			character.outfit[Character.OUTFITTOP].add(Clothing.tanktop);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.panties);
			character.outfit[Character.OUTFITBOTTOM].add(Clothing.jeans);
		}
	}
}
