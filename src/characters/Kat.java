package characters;

import Comments.CommentGroup;
import Comments.CommentSituation;
import Comments.SkillComment;
import actions.Action;
import actions.Movement;
import combat.Combat;
import combat.Result;
import combat.Tag;
import daytime.Daytime;
import global.Flag;
import global.Global;
import global.Match;
import global.Modifier;
import items.Clothing;
import items.Toy;
import items.Trophy;
import scenes.SceneFlag;
import scenes.SceneManager;
import skills.*;
import stance.Stance;
import status.Stsflag;

import java.util.ArrayList;
import java.util.HashSet;

public class Kat implements Personality {
	/**
	 *
	 */
	private static final long serialVersionUID = -8169646189131720872L;
	private final NPC character;

	public Kat() {
		character = new NPC("Kat", ID.KAT, 10, this);

		// Set clothing
		resetOutfit();
		character.closet.add(Clothing.bra);
		character.closet.add(Clothing.Tshirt);
		character.closet.add(Clothing.panties);
		character.closet.add(Clothing.skirt);
		character.change(Modifier.normal);
		character.setUnderwear(Trophy.KatTrophy);

		// Set base stats
		character.mod(Attribute.Power, 5);
		character.mod(Attribute.Animism, 14);
		character.mod(Attribute.Cunning, 3);
		character.mod(Attribute.Speed, 3);
		character.mod(Attribute.Seduction, 2);
		character.getStamina().gainMax(45);
		character.getArousal().gainMax(55);
		character.getMojo().gainMax(25);

		// Set starting traits
		character.add(Trait.female);
		character.add(Trait.dexterous);
		character.add(Trait.pheromones);
		character.add(Trait.tailed);
		character.add(Trait.shy);
		character.add(Trait.sympathetic);

		// Set AI logic
		character.plan = Emotion.sneaking;
		character.mood = Emotion.confident;
		character.strategy.put(Emotion.hunting, 1);
		character.preferredSkills.add(Tackle.class);
		character.preferredSkills.add(Purr.class);
		character.preferredSkills.add(TailJob.class);
		character.preferredSkills.add(PheromoneOverdrive.class);
		character.preferredSkills.add(Carry.class);
		if (Global.checkFlag(Flag.PlayerButtslut)) {
			character.preferredSkills.add(AssFuck.class);
			character.preferredSkills.add(Strapon.class);
			character.preferredSkills.add(TailPeg.class);
			character.preferredSkills.add(FingerAss.class);
		}

		Global.gainSkills(character);
	}

	@Override
	public Skill act(HashSet<Skill> available, Combat c) {
		var mandatory = new HashSet<Skill>();
		for (var skill : available) {
			if (skill.toString().equalsIgnoreCase("Command")
					|| skill.toString().equalsIgnoreCase("Ass Fuck")) {
				mandatory.add(skill);
			}
			if (character.is(Stsflag.orderedstrip)
					&& (skill.toString().equalsIgnoreCase("Undress")
					|| skill.toString().equalsIgnoreCase("Strip Tease"))) {
				mandatory.add(skill);
			}
			if (Global.checkFlag(Flag.PlayerButtslut) && character.has(Trait.strapped)) {
				if (skill.toString().equalsIgnoreCase("Mount") && character.has(Toy.Strapon2)) {
					if (Global.random(5) == 0) {
						mandatory.add(skill);
					}
				}
				if (skill.toString().equalsIgnoreCase("Turn Over")) {
					mandatory.add(skill);
				}
			}
		}

		if (!mandatory.isEmpty()) {
			var actions = mandatory.toArray(new Skill[0]);
			return actions[Global.random(actions.length)];
		}

		Skill chosen;
		var priority = character.parseSkills(available, c);
		if (Global.checkFlag(Flag.hardmode) && !c.hasModifier(Modifier.quiet)) {
			chosen = character.prioritizeSimulated(priority, c);
		}
		else {
			chosen = character.prioritizeRandom(priority);
		}

		if (chosen != null) {
			return chosen;
		}

		var actions = available.toArray(new Skill[0]);
		return actions[Global.random(actions.length)];
	}

	@Override
	public Action move(HashSet<Action> available, HashSet<Movement> radar, Match match) {
		return character.parseMoves(available, radar, match);
	}

	@Override
	public void rest(int time, Daytime day) {
		if (!(character.has(Toy.Onahole) || character.has(Toy.Onahole2)) && character.money >= 300) {
			character.gain(Toy.Onahole);
			character.money -= 300;
		}
		if (!(character.has(Toy.Tickler) || character.has(Toy.Tickler2)) && character.money >= 300) {
			character.gain(Toy.Tickler);
			character.money -= 300;
		}
		if (Global.checkFlag(Flag.PlayerButtslut)
				&& !(character.has(Toy.Strapon) || character.has(Toy.Strapon2))
				&& character.money >= 600
				&& (character.getPure(Attribute.Seduction) >= 20 || Global.getValue(Flag.PlayerAssLosses) >= 10)) {
			character.gain(Toy.Strapon);
			character.money -= 600;
		}

		var available = new ArrayList<String>();
		available.add("Hardware Store");
		available.add("Black Market");
		available.add("XXX Store");
		available.add("Bookstore");
		available.add("Reference Room");
		available.add("Play Video Games");
		for (var i = 0; i < time - 3; i++) {
			var location = available.get(Global.random(available.size()));
			day.visit(location, character, Global.random(character.money));
			if (!location.equalsIgnoreCase("Exercise")
					&& !location.equalsIgnoreCase("Browse Porn Sites")) {
				available.remove(location);
			}
		}
		if (character.getAffection(Global.getPlayer()) > 0) {
			Global.modCounter(Flag.KatDWV, 1);
		}
		character.visit(3);
	}

	@Override
	public String bbLiner() {
		switch (Global.random(2)) {
			case 1:
				return "Kat's already large eyes widen even further as she looks at the pained expression on your face.  "
						+ "<i>\"Nya!  A swift shot to the nyutsack.  Works every time!\"</i>";
			default:
				return "Kat gives you a look of concern and sympathy. <i>\"Nya... Are you ok? I didn't mean to hit you that hard.\"</i>";
		}
	}

	@Override
	public String nakedLiner() {
		if (character.getArousal().percent() >= 50) {
			return "Kat makes no effort to hide the moisture streaming down her thighs. <i>\"You want my pussy? I'm nyot going to myake it easy for you.\"</i>";
		}
		else {
			return "Kat blushes deep red and bashfully tries to cover her girl parts with her tail. <i>\"Don't stare too much, ok?\"</i>";
		}
	}

	@Override
	public String stunLiner() {
		return "Kat mews pitifully on the floor. <i>\"Don't be so meaNya.\"</i>";
	}

	@Override
	public String taunt() {
		return "Kat smiles excitedly and bats at your cock. <i>\"Are you already close to cumming? Nya! I want to play with you more!\"</i>";
	}

	@Override
	public void victory(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		character.clearGrudge(opponent);
		character.getArousal().empty();
		if (!opponent.human()) {
			return;
		}

		if (flag == Result.anal && c.stance.penetration(character)) {
			Global.modCounter(Flag.PlayerAssLosses, 1);
			SceneManager.play(SceneFlag.KatPeggingVictory);
		}
		else if (flag == Result.intercourse) {
			if (Global.random(2) == 0) {
				SceneManager.play(SceneFlag.KatSexVictoryAlt);
			}
			else {
				SceneManager.play(SceneFlag.KatSexVictory);
			}
		}
		else if (c.lastAction(character).hasTag(Result.feet)) {
			SceneManager.play(SceneFlag.KatFootjobVictory);
		}
		else if (c.lastAction(character).hasTag(SkillTag.TAIL)) {
			SceneManager.play(SceneFlag.KatTailjobVictory);
		}
		else if (!character.is(Stsflag.feral)) {
			SceneManager.play(SceneFlag.KatForeplayVictoryEasy);
		}
		else {
			opponent.getArousal().set(opponent.getArousal().max() / 3);
			SceneManager.play(SceneFlag.KatForeplayVictory);
		}
	}

	@Override
	public void defeat(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		declareGrudge(opponent, c);
		if (!opponent.human()) {
			return;
		}

		if (flag == Result.anal && c.stance.penetration(character)) {
			SceneManager.play(SceneFlag.KatAnalDefeat);
		}
		else if (c.stance.en == Stance.standing) {
			SceneManager.play(SceneFlag.KatSexDefeatCarry);
		}
		else if (flag == Result.intercourse) {
			SceneManager.play(SceneFlag.KatSexDefeat);
		}
		else if (character.is(Stsflag.bound)) {
			SceneManager.play(SceneFlag.KatBoundDefeat);
		}
		else if (opponent.is(Stsflag.feral)) {
			SceneManager.play(SceneFlag.KatFeralDefeat);
		}
		else if (Global.random(2) == 0) {
			SceneManager.play(SceneFlag.KatForeplayDefeatAlt);
		}
		else {
			SceneManager.play(SceneFlag.KatForeplayDefeat);
		}
	}

	@Override
	public String describe() {
		return "It's easy to forget that Kat's older than you when she looks like she's about to start high school. She's a very short and slim, though you know she's "
				+ "stronger than she looks. Adorable cat ears poke through her short, strawberry blonde hair and a soft tail dangles from the top of her cute butt. She "
				+ "looks a bit timid, but there's a gleam of desire in her eyes.";
	}

	@Override
	public void draw(Combat c, Tag flag) {
		var opponent = c.getOther(character);
		character.clearGrudge(opponent);
		if (!opponent.human()) {
			return;
		}

		if (flag == Result.intercourse) {
			SceneManager.play(SceneFlag.KatSexDraw);
		}
		else {
			SceneManager.play(SceneFlag.KatForeplayDraw);
		}
	}

	@Override
	public boolean fightFlight(Character opponent) {
		return !character.nude() || opponent.nude();
	}

	@Override
	public boolean attack(Character opponent) {
		return true;
	}

	@Override
	public void ding() {
		var numIncreases = (Global.random(3) / 2) + 1;
		for (var i = 0; i < numIncreases; i++) {
			switch (Global.random(4)) {
				case 0:
					character.mod(Attribute.Power, 1);
					break;
				case 1:
					character.mod(Attribute.Seduction, 1);
					break;
				case 2:
					character.mod(Attribute.Cunning, 1);
					break;
				case 3:
					character.mod(Attribute.Animism, 1);
					break;
			}
		}

		character.getStamina().gainMax(4);
		character.getArousal().gainMax(4);
	}

	@Override
	public String victory3p(Combat c, Character target, Character assist) {
		character.clearGrudge(target);
		character.clearGrudge(assist);
		if (target.human()) {
			return "Kat crouches between your legs, watching your erect penis twitch and gently bob. She playfully bats it back and forth, clearly enjoying herself. That is "
					+ "most definitely not a toy, but she seems to disagree. As your boner wags from side to side, she catches it with her mouth, fortunately displaying the "
					+ "presence of mind to keep her teeth clear. She covers the head of your dick with her mouth and starts to lick it intently, while she teases your shaft "
					+ "with both hands. Oh God. You're not sure whether or not this is still a game to her, but she's being very effective. With the combined pleasure from her "
					+ "mouth and hands, it's not long before your hips start bucking in anticipation of release. She jerks you off with both hands and sucks firmly on your glans. "
					+ "You groan as you ejaculate into her mouth and she eagerly swallows every drop.";
		}
		else if (target.hasDick()) {
			return "Kat gives " + target.name() + " a distinctively feline smirk as she licks her lips, her hands wrapping around her opponent's throbbing "
					+ "member and pumping up and down as she leans in, swirling her tongue around the tip. " + target.name() + " shudders, struggling in "
					+ "your grip, but you hold her hands firmly. Kat's tail curls behind her as she sucks the girl's cock. You can see the veiny shaft "
					+ "glistening with saliva. By the way " + target.name() + "'s toes are clenching, you know she won't last much longer. She's already put "
					+ "up quite a fight. <i>\"S-Stop!\"</i> she cries, but her plea makes Kat's ears perk up. She pulls back, peering up at " + target.name()
					+ " with light eyes, purring softly as her tongue massages the underside of the glans.<p>"
					+ "<i>\"You want me to stop, nyaa?\"</i> Kat murmurs, lapping at the shiny precum that's appearing as she continues to furiously "
					+ "masturbate her foe. " + target.name() + " is at a loss for words, too overcome by the sensations to think coherently anymore. "
					+ "Kat plunges " + target.name() + "'s cock back into her mouth, taking it in as far as she can as she feels the girl begin to orgasm. "
					+ "You hold her steady as she bucks wildly, moaning loudly. Kat slurps down her load, cum dripping from the corners of her mouth. "
					+ "When she's finished, she sits up, wiping her mouth with a satisfied expression. It looks like victory is yours.";
		}
		else {
			return "Kat stalks over to " + target.name() + " like a cat. Very much like a cat. She leans toward the other girl's breasts and starts to softly lick her nipples. "
					+ target.name() + " stifles a moan of pleasure as Kat gradually covers her breasts in saliva, not missing an inch. Kat gradually works her way down, giving "
					+ "equal attention to " + target.name() + "'s bellybutton. By the helpless girl's shudders and stifled giggles, it clearly tickles, but also seems to heighten "
					+ "her arousal. You feel her body tense up in anticipation when Kat licks her way down towards her waiting pussy. Instead Kat veers off and starts kissing "
					+ "and licking her inner thighs. " + target.name() + " trembles in frustration and she must realize she has no chance to win, because she yells out: <i>\"Please! "
					+ "Stop teasing me and let me cum!\"</i> Kat blinks for a moment and looks surprised, like she had forgotten her original purpose, but she soon smiles and dives "
					+ "enthusiastically between " + target.name() + "'s legs. It doesn't take long before you feel her shudder in orgasm from Kat's intense licking, but even then, "
					+ "Kat shows no sign of stopping. " + target.name() + " stammers out a protest and you sympathetically release her hands, but she's too overwhelmed by pleasure "
					+ "to defend herself as Kat licks her to a second orgasm.";
		}
	}

	@Override
	public String intervene3p(Combat c, Character target, Character assist) {
		if (target.human()) {
			return "Your fight with " + assist.name() + " goes back and forth for several minutes, leaving you both naked and aroused, but without a clear winner. You back off "
					+ "a couple steps to catch your breath, when suddenly you're blindsided and knocked to the floor. You look up to see Kat straddling your chest in full cat-mode. "
					+ "She's clearly happy to see you, kissing and nuzzling your neck while mewing happily. She's very cute, but unfortunately you're unable to dislodge her from "
					+ "her perch on your upper body. More unfortunately, your completely defenseless lower body is exposed to " + assist.name() + ", who doesn't let the opportunity go "
					+ "to waste.<p>";
		}
		else {
			return "Your fight with " + target.name() + " goes back and forth for several minutes, leaving you both naked and aroused, but without a clear winner. You back off "
					+ "a couple steps to catch your breath, when you notice a blur of motion out of the corner of your eye. Distracted as you are, you aren't paying attention to "
					+ "your footing and stumble backwards. " + target.name() + " advances to take advantage of your mistake, but she soon realizes you aren't looking at her, but "
					+ "behind her. She turns around just in time to get pounced on by an exuberant catgirl. Kat starts to playfully tickle her prey, incapacitating the poor, "
					+ "naked girl with uncontrollable fits of laughter. You stand up, brush off your butt, and leisurely walk towards your helpless opponent to finish her off.<p>";
		}
	}

	@Override
	public void watched(Combat c, Character target, Character viewer) {
		if (!viewer.human()) {
			return;
		}

		if (target.hasDick()) {
			SceneManager.play(SceneFlag.KatWatchPenis);
		}
		else {
			SceneManager.play(SceneFlag.KatWatch);
		}
	}

	@Override
	public String startBattle(Character opponent) {
		if (character.getGrudge() != null) {
			switch (character.getGrudge()) {
				case feral:
					return "Kat approaches you with a confident swagger and her tail swaying slowly. She's clearly in cat mode already. "
							+ "It looks like someone already warmed her up for you.<p>"
							+ "<i>\"Nyot quite. I'm just myaking an effort to embrace my feline side. I'm gonnya go all out from the start.\"</i>";
				case predatorinstincts:
					return "Kat stalks you with a dangerous expression. Despite her cute features and small build, you start to feel like a prey animal faced "
							+ "by a predator.<p>"
							+ "<i>\"I'll catch you this time, and I won't let you go until you cum!\"</i>";
				case landsonfeet:
					return "Kat leaps toward you,  planting a light kiss on your lips. Before you can respond, she backflips "
							+ "out of reach, landing gracefully on one foot. She's even more nimble than you realized.<p>"
							+ "<i>\"Cats always land on their feet. I'll be the one on top this time.\"</i>";
				default:
					break;
			}
		}
		if (character.nude()) {
			if (character.getArousal().percent() >= 50) {
				return "Kat approaches you, naked and unashamed. She parts her slick nether lips to show you her arousal.<p>"
						+ "<i>\"You showed up at just the right time, nya. Nyow you're gonnya satisfy me.\"</i>";
			}
			else {
				return "Kat blushes deep red as she tries to preserve her modesty. <i>\"I was hoping to get dressed before you caught me. "
						+ "Well, let's hurry up and start before this gets any more embarrassing.\"</i>";
			}
		}
		if (opponent.pantsless()) {
			return "Kat stares at your dangling penis until you cough to get her attention. <i>\"Nya!? Oh, sorry.\"</i> She blushes a bit and "
					+ "regains her focus. <i>\"Cats are easily distracted by swinging things. Girls are also easily distracted by... certain things...\"</i>";
		}
		if (character.getAffection(opponent) >= 30) {
			return "Kat smiles and lets out a quiet purr when she sees you. She fidgets shyly with her tail, looking more like a girl waiting for a date "
					+ "than someone preparing to fight.<p>"
					+ "<i>\"Sorry, I was just looking forward to this. I almost don't care who wins.\"</i>";
		}
		return "Kat looks a bit nervous, but her tail wags slowly in anticipation. <i>\"Let's have some fun-nya.\"</i>";
	}

	@Override
	public boolean fit() {
		return (!character.nude() && character.getStamina().percent() >= 50)
				|| character.getArousal().percent() > 50;
	}

	@Override
	public boolean night() {
		Global.gui().loadPortrait(Global.getPlayer(), this.character);
		SceneManager.play(SceneFlag.KatAfterMatch);
		return true;
	}

	@Override
	public void advance(int rank) {
		if (rank >= 3 && !character.has(Trait.tailmastery)) {
			character.add(Trait.tailmastery);
		}
		if (rank >= 2 && !character.has(Trait.furaffinity)) {
			character.add(Trait.furaffinity);
		}
	}

	@Override
	public NPC getCharacter() {
		return character;
	}

	@Override
	public boolean checkMood(Emotion mood, int value) {
		if (character.is(Stsflag.feral)) {
			if (!character.has(Trait.shameless)) {
				character.add(Trait.shameless);
				character.remove(Trait.shy);
				character.strategy.put(Emotion.hunting, 5);
			}
			switch (mood) {
				case horny:
					return value >= 30;
				case nervous:
					return value >= 80;
				default:
					return value >= 50;
			}
		}
		else {
			if (!character.has(Trait.shy)) {
				character.add(Trait.shy);
				character.remove(Trait.shameless);
				character.strategy.put(Emotion.hunting, 1);
			}
			switch (mood) {
				case nervous:
				case horny:
					return value >= 30;
				case angry:
					return value >= 80;
				default:
					return value >= 50;
			}
		}
	}

	@Override
	public float moodWeight(Emotion mood) {
		if (character.is(Stsflag.feral)) {
			switch (mood) {
				case horny:
					return 1.2f;
				case nervous:
					return .7f;
				default:
					return 1f;
			}
		}
		else {
			switch (mood) {
				case nervous:
				case horny:
					return 1.2f;
				case angry:
					return .7f;
				default:
					return 1.2f;
			}
		}
	}

	@Override
	public String image() {
		return "assets/kat_" + character.mood.name() + ".jpg";
	}

	@Override
	public void pickFeat() {
		var available = new ArrayList<Trait>();
		for (var feat : Global.getFeats()) {
			if (!character.has(feat) && feat.meetsRequirement(character)) {
				available.add(feat);
			}
		}
		if (!available.isEmpty()) {
			character.add((Trait) available.toArray()[Global.random(available.size())]);
		}
	}

	@Override
	public String resist3p(Combat c, Character target, Character assist) {
		return null;
	}

	@Override
	public CommentGroup getComments() {
		var comments = new CommentGroup();
		comments.put(CommentSituation.VAG_DOM_CATCH_WIN, "<i>\"Nya! Cum in me now!\"</i>");
		comments.put(CommentSituation.VAG_DOM_CATCH_LOSE, "<i>\"Ooohnya! But I'm on top! I should win!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_WIN, "<i>\"Yes! Fill me! Breed me~!\"</i>");
		comments.put(CommentSituation.VAG_SUB_CATCH_LOSE, "<i>\"Nnnya! Oh! Fuck, breed, cum, NYAAA!\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_WIN, "<i>\"Ooh! Are you gonnya fill up my ass?\"</i>");
		comments.put(CommentSituation.ANAL_CATCH_LOSE, "<i>\"Nya! And they ca-AAH-ll me an animalnya!\"</i>");
		comments.put(CommentSituation.ANAL_PITCH_WIN,
				"<i>\"Yes! Come for me! There's nyot any shame in being the beta of a pack, nya~!\"</i>");
		comments.put(CommentSituation.ANAL_PITCH_LOSE,
				"<i>\"Nnnya?! Th-this is what it's like to be the one on top?\"</i> Kat has two fingers jammed between her strap-on harness and her clitoris, and is clearly enjoying herself.");
		comments.put(CommentSituation.MOUNT_DOM_WIN, "<i>\"Nyanyanya! I'm going to win now!\"</i>");
		comments.put(CommentSituation.SELF_BOUND, "<i>\"Please let me go! I'll do anyathing!\"</i>");
		comments.put(CommentSituation.MOUNT_SUB_LOSE, "<i>\"Ah! Put it in! Put it in!\"</i>");
		comments.put(CommentSituation.PIN_SUB_LOSE, "<i>\"Ah! Put it in! Put it in!\"</i>");
		comments.put(CommentSituation.OTHER_HORNY, "<i>\"Are you in heat nyow too? I can help with that!\"</i>");
		comments.put(CommentSituation.SELF_HORNY, "<i>\"Nyaa~... Please! I need it!\"</i>");
		comments.put(CommentSituation.SELF_SHAMED, "<i>\"Don't look! Don't look! Don't look!\"</i>");
		comments.put(CommentSituation.OTHER_CHARMED, "<i>\"Nya? Can I play with you a little? purretty please?\"</i>");
		comments.put(CommentSituation.SELF_BUSTED,
				"Kat goes cross-eyed and squeezes her knees together protectively.  She lets out a long, low growl from the back of her throat that slowly rises in pitch, until it becomes a pitiful whimper.");
		comments.put(new SkillComment(Attribute.Animism, true), "<i>\"Nyice! Let's go at it like anyamals!\"</i>");

		return comments;
	}

	@Override
	public CommentGroup getResponses() {
		return new CommentGroup();
	}

	@Override
	public int getCostumeSet() {
		return 1;
	}

	@Override
	public void declareGrudge(Character opponent, Combat c) {
		switch (Global.random(3)) {
			case 2:
				character.addGrudge(opponent, Trait.feral);
				break;
			case 1:
				character.addGrudge(opponent, Trait.predatorinstincts);
				break;
			case 0:
				character.addGrudge(opponent, Trait.landsonfeet);
				break;
			default:
				break;
		}
	}

	@Override
	public void resetOutfit() {
		character.outfit[Character.OUTFITTOP].clear();
		character.outfit[Character.OUTFITBOTTOM].clear();

		character.outfit[Character.OUTFITTOP].add(Clothing.bra);
		character.outfit[Character.OUTFITTOP].add(Clothing.Tshirt);
		character.outfit[Character.OUTFITBOTTOM].add(Clothing.panties);
		character.outfit[Character.OUTFITBOTTOM].add(Clothing.skirt);
	}
}
