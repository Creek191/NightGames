package characters;

import global.Constants;

/**
 * Defines the resource pools available to a character
 */
public enum Pool {
	STAMINA("Stamina", Constants.STARTINGSTAMINA),
	AROUSAL("Arousal", Constants.STARTINGAROUSAL),
	MOJO("Mojo", Constants.STARTINGMOJO),
	BATTERY("Battery", 20),
	TIME("Time", 12),
	ENIGMA("Enigma", 7),
	FOCUS("Focus", 12);
	private final String label;
	private final int baseCap;

	Pool(String label, int baseCap) {
		this.label = label;
		this.baseCap = baseCap;
	}

	/**
	 * Gets the name of the pool
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Gets the base cap of the pool
	 */
	public int getBaseCap() {
		return baseCap;
	}
}
